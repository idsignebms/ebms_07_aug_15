﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class ConsumerDal
    {
        #region Members
        CommonDAL objCommonDal = new CommonDAL();
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        int CommandTime = 300;

        //Stored Procedures Declaration
        private static string PROC_USP_GetInjectionSubStations = "USP_GetInjectionSubStations";
        private static string PROC_USP_GetServiceCenters = "USP_GetServiceCenters";
        private static string PROC_USP_GetServiceUnits = "USP_GetServiceUnits";
        private static string PROC_USP_GetBusinessUnits = "USP_GetBusinessUnits";
        private static string PROC_USP_GetIdentityTypes = "USP_GetIdentityTypes";
        private static string PROC_USP_GetFeeders = "USP_GetFeeders";
        private static string PROC_USP_GetTransformers = "USP_GetTransformers";
        private static string PROC_USP_GetPoles = "USP_GetPoles";
        private static string PROC_USP_GetConnectionReasons = "USP_GetConnectionReasons";
        private static string PROC_USP_GetPhases = "USP_GetPhases";
        private static string PROC_USP_GetBookNumbersByServiceDetails = "USP_GetBookNumbersByServiceDetails";
        private static string PROC_USP_GetMeterStatusList = "USP_GetMeterStatusList";
        private static string PROC_USP_GetMeterTypes = "USP_GetMeterTypes";
        private static string PROC_USP_GetAccountTypes = "USP_GetAccountTypes";
        private static string PROC_USP_GetReadCodes = "USP_GetReadCodes";
        private static string PROC_USP_GetCustomerTypesList = "USP_GetCustomerTypesList";
        private static string PROC_USP_GetAgenciesList = "USP_GetAgenciesList";
        private static string PROC_USP_InsertCustomer = "USP_InsertCustomer";
        private static string PROC_USP_GetProcessedByList = "USP_GetProcessedByList";
        private static string PROC_USP_GetEmployeesList = "USP_GetEmployeesList";
        private static string PROC_USP_GetCyclesList = "USP_GetCyclesList";
        private static string PROC_USP_GetUnMappedBookNoList = "USP_GetUnMappedBookNoList";
        private static string PROC_USP_GetCustomerDetails = "USP_GetCustomerDetails";
        private static string PROC_USP_GetCycleDetailsByCycleId = "USP_GetCycleDetailsByCycleId";
        private static string PROC_USP_GetBookDetailsByBookNo = "USP_GetBookDetailsByBookNo";
        private static string PROC_USP_GetCycleBookMappedList = "USP_GetCycleBookMappedList";
        private static string PROC_USP_MapBookToCycle = "USP_MapBookToCycle";
        private static string PROC_USP_UnMapBookFromCycle = "USP_UnMapBookFromCycle";
        private static string PROC_USP_CheckCustomerSequenceNoExists = "USP_CheckCustomerSequenceNoExists";
        private static string PROC_USP_CheckCustomerDocumentNoExists = "USP_CheckCustomerDocumentNoExists";
        private static string PROC_USP_SearchCustomer = "USP_SearchCustomer";
        private static string PROC_USP_UpdateCustomerDetails = "USP_UpdateCustomerDetails";
        private static string PROC_USP_GetCustomerDocumets = "USP_GetCustomerDocumets";
        private static string PROC_USP_DeleteCustomerDocuments = "USP_DeleteCustomerDocuments";
        private static string PROC_USP_IsBillGenerated_ByAccNo = "USP_IsBillGenerated_ByAccNo";


        private static string PROC_USP_CheckCustomerMeterNoExists = "USP_CheckCustomerMeterNoExists";
        private static string PROC_USP_CheckCustomerMeterSerialNoExists = "USP_CheckCustomerMeterSerialNoExists";
        private static string PROC_USP_CheckNeighbourAccNoExists = "USP_CheckNeighbourAccNoExists";
        private static string proc_USP_GetRoutes = "USP_GetRoutes";//Bhargav
        //private static string PROC_USP_GETSEARCHCUSTOMERS = "USP_SearchCustomers";//Rama
        private static string PROC_USP_GETSEARCHCUSTOMERS = "USP_SearchCustomers_Temp";//Karteek
        private static string PROC_USP_GETCONSUMERROUTEDETAILS = "USP_GetCustomerRouteSequence";//Rama
        private static string PROC_USP_UPDATECONSUMERROUTEDETAILS = "UpdateConsumerRouteSeq";//Rama  
        private static string PROC_USP_GETDIRECTCUSTOMERDETAILS = "USP_GETDIRECTCUSTOMERDETAILS";//NARESH
        private static string PROC_USP_INSERTDIRECTCUSTOMERENERGYCHARGES = "USP_INSERTDIRECTCUSTOMERENERGYCHARGES";//NARESH
        private static string PROC_USP_GetCustomerCount_BYTariffId = "USP_GetCustomerCount_BYTariffId";//NARESH
        private static string PROC_USP_GetCustomerCount_BYBUID = "USP_GetCustomerCount_BYBUID";//NARESH
        private static string PROC_USP_GetEnergyCharges_BYTariffId = "USP_GetEnergyCharges_BYTariffId";//NARESH
        private static string PROC_USP_InsertEstimatedCustomerDetails = "USP_InsertEstimatedCustomerDetails";//NARESH
        private static string PROC_USP_GetBUIDCountBYTariffId = "USP_GetBUIDCountBYTariffId";//NARESH
        private static string PROC_USP_UpdateEstRuleId = "USP_UpdateEstRuleId";//NARESH
        private static string PROC_USP_GetCustomerPaymentHistory = "USP_GetCustomerPaymentHistory";//NEERAJ
        //private static string PROC_USP_GetReportOnEstimatedCustomers = "USP_GetReportOnEstimatedCustomers";//NEERAJ
        private static string PROC_USP_GetReportOnEstimatedCustomers = "USP_GetReportOnEstimatedCustomers_New";//Karteek
        private static string PROC_USP_CheckOldAccNoExists = "USP_CheckOldAccNoExists";

        private static string PROC_USP_GetBookNoList = "USP_GetBookNoList";//Neeraj-ID077
        private static string PROC_USP_GetCustomersBy_Cycle_BookNo_CB = "USP_GetCustomersBy_Cycle_BookNo_CB";//Neeraj-ID077
        private static string PROC_USP_UpdateCustomerLatitude_Longiture = "USP_UpdateCustomerLatitude_Longiture";//Neeraj-ID077

        private static string PROC_USP_GETCUSTOMERDETAILS_ACCOUNTNO = "USP_GETCUSTOMERDETAILS_ACCOUNTNO";// Suresh

        private static string PROC_USP_GETMARKETERBYACCOUNTNO = "USP_GetMarketerByAccountNo";// Faiz

        private static string PROC_USP_BulkUload_Customers = "USP_BulkUload_Customers";// Suresh
        private static string PROC_USP_GetCustDetailBlkUpld = "USP_GetCustDetailBlkUpld";// Suresh
        private string PROC_USP_CustomerRegistration = "CUSTOMERS.USP_CustomerRegistration";
        private string PROC_USP_USP_BooknNoSearch = "MASTERS.USP_BooknNoSearch";
        private string PROC_USP_GetCustomerAddress = "USP_GetCustomerAddress";
        //private string PROC_USP_ChangeCustomersAddress = "USP_ChangeCustomersAddress";
        private string PROC_USP_ChangeCustomersAddress = "USP_ChangeCustomersAddress_New";//NEERAJ KANOJIYA --ID077
        private string PROC_USP_ChangeCustomersBookNo_New = "USP_ChangeCustomersBookNo_New";//NEERAJ KANOJIYA --ID077
        private string PROC_USP_ChangeCustomerBookNo = "USP_ChangeCustomerBookNo";
        private string PROC_USP_IsOldAccountNoExists = "USP_IsOldAccountNoExists";
        private string PROC_USP_IsEmailExists = "USP_IsEmailExists";
        private string PROC_USP_GetMeterDialsByMeterNo = "USP_GetMeterDialsByMeterNo";
        private string PROC_USP_AssignMeter = "MASTERS.USP_AssignMeter";
        private string PROC_USP_Re_AssignMeter = "MASTERS.USP_Re-AssignMeter";
        private string PROC_USP_GetCustDetailsForAssignMeter = "USP_GetCustDetailsForAssignMeter";
        private string PROC_USP_GetCustDetailsForBillGen = "USP_GetCustDetailsForBillGen";
        private string PROC_USP_GetIdentityDetails = "USP_GetIdentityDetails";//Neeraj ID077 28-MARCH-15
        private string PROC_USP_GetCustomerDocuments = "USP_GetCustomerDocuments";//Neeraj ID077 28-MARCH-15
        private string PROC_USP_GetCustomerUDFValues = "USP_GetCustomerUDFValues";//Neeraj ID077 28-MARCH-15
        private string PROC_USP_GetCustInfoForAdjustmentNoBill = "USP_GetCustInfoForAdjustmentNoBill";//Neeraj ID077 28-MARCH-15
        private string PROC_USP_GetCustomerAddress_Check = "USP_GetCustomerAddress_ByCheck";
        private string PROC_USP_GetCustomerBook_Check = "USP_GetCustomerBook_ByCheck";
        private string PROC_USP_GetCustDetailsForBillGen_ByCheck = "USP_GetCustDetailsForBillGen_ByCheck";
        private string PROC_USP_GetCustDetailsForAssignMeter_ByCheck = "USP_GetCustDetailsForAssignMeter_ByCheck";
        private string PROC_USP_USP_ApprovalRegistration = "CUSTOMERS.USP_ApprovalRegistration ";
        private string PROC_USP_GetApprovalRegistrationsByARID = "USP_GetApprovalRegistrationsByARID";

        #region Customer Import Procedures
        private string PROC_USP_InsertBatchCustomerBulkUpload = "USP_InsertBatchCustomerBulkUpload";// Neeraj
        private string PROC_USP_IsExistsBatchCustBulkUpload = "USP_IsExistsBatchCustBulkUpload";// Neeraj
        private string PROC_USP_InsertBatchVsAccontNo = "USP_InsertBatchVsAccontNo";// Neeraj
        #endregion

        #region Customer Bills

        private static string PROC_USP_GETCYCLES_BYSERVICEUNIT = "USP_GETCYCLES_BYSERVICEUNIT";
        private static string PROC_USP_GETFEEDERS_BYSERVICEUNIT = "USP_GETFEEDERS_BYSERVICEUNIT";
        private static string PROC_USP_GETUSERSERVICEUNITS_BUIDS = "USP_GETUSERSERVICEUNITS_BUIDS";
        private static string PROC_USP_GETUSERSERVICECENTERS_SUIDS = "USP_GETUSERSERVICECENTERS_SUIDS";

        #endregion

        #region SP_Reports
        //private string PROC_USP_GETACCOOUMTSWITHOUTCYCLES = "USP_GetAccountWithoutCycle"; //Old
        private string PROC_USP_GETACCOOUMTSWITHOUTCYCLES = "USP_GetAccountWithoutCycle_New"; // New
        //private string USP_GetAccountWithoutMeter = "USP_GetAccountWithoutMeter";//Old
        private string USP_GetAccountWithoutMeter = "USP_GetAccountWithoutMeter_New";//New
        //private string USP_GetTARIFFBUPIVOTREPORT = "USP_GetTariffBUPivotReport"; //Old
        private string USP_GetTARIFFBUPIVOTREPORT = "USP_GetTariffBUPivotReport_New"; //New
        private string PROC_USP_GetAccountWithOutFeeders = "USP_GetAccountWithOutFeeders";


        private string USP_RptGetCustomersNoNameOrAddList_New = "USP_RptGetCustomersNoNameOrAddList_New";
        #endregion

        #endregion

        #region Methods
        public XmlDocument Insert(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomer, SqlCon); break;
                    case Statement.Modify:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_MapBookToCycle, SqlCon); break;
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UnMapBookFromCycle, SqlCon); break;
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerDetails, SqlCon); break;
                    case Statement.Edit:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTDIRECTCUSTOMERENERGYCHARGES, SqlCon); break;
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerCount_BYTariffId, SqlCon); break;
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerCount_BYBUID, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertDetails(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertEstimatedCustomerDetails, SqlCon); break;
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateEstRuleId, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public XmlDocument GetDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBUIDCountBYTariffId, SqlCon);
                        break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetInjectionSubStations, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetServiceCenters, SqlCon); break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetServiceUnits, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBusinessUnits, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCycleDetailsByCycleId, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookDetailsByBookNo, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCycleBookMappedList, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerDetails, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetEnergyCharges_BYTariffId, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet GetSearchCustomers(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETSEARCHCUSTOMERS, SqlCon, CommandTime);
            //return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETSEARCHCUSTOMERS, SqlCon);
        }

        public DataSet GetSearchCustomers(string searchString, int startIndex, int endIndex)
        {
            SqlParameter[] parameters =
            {    
              new SqlParameter("@SearchString", SqlDbType.VarChar){ Value = searchString },
              new SqlParameter("@StartIndex", SqlDbType.Int){ Value = startIndex },
              new SqlParameter("@EndIndex", SqlDbType.Int){ Value = endIndex }
            };
            return objCommonDal.ExecuteReader_GetDataSet(PROC_USP_GETSEARCHCUSTOMERS, parameters, SqlCon, CommandTime);
        }

        public XmlDocument GetServiceDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetFeeders, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetTransformers, SqlCon); break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetPoles, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNumbersByServiceDetails, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckCustomerDocumentNoExists, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckCustomerSequenceNoExists, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_SearchCustomer, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAccountWithOutFeeders, SqlCon); break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, proc_USP_GetRoutes, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Get(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetIdentityTypes, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetPhases, SqlCon); break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetConnectionReasons, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetMeterStatusList, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetMeterTypes, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetAccountTypes, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetReadCodes, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetCustomerTypesList, SqlCon); break;
                    //case ReturnType.Fetch:
                    //    xml = objiDsignDAL.ExecuteXmlReader(proc_USP_GetRoutes, SqlCon);
                    //    break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Retrieve(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetAgenciesList, SqlCon); break;
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetProcessedByList, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetEmployeesList, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetCyclesList, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetUnMappedBookNoList, SqlCon); break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Retrieve(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckCustomerMeterNoExists, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckCustomerMeterSerialNoExists, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerDocumets, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteCustomerDocuments, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckNeighbourAccNoExists, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetReportOnEstimatedCustomers, SqlCon); break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsBillGenerated_ByAccNo, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckOldAccNoExists, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetMeterDialsByMeterNo, SqlCon); break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerRouteManagementDAL(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCONSUMERROUTEDETAILS, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATECONSUMERROUTEDETAILS, SqlCon); break;


                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetDirectCustomerDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETDIRECTCUSTOMERDETAILS, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerPaymentHistoryDAL(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerPaymentHistory, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerBillDeails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETFEEDERS_BYSERVICEUNIT, SqlCon);
                        break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCYCLES_BYSERVICEUNIT, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETUSERSERVICEUNITS_BUIDS, SqlCon);
                        break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETUSERSERVICECENTERS_SUIDS, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }


        public XmlDocument GetBookNoListDAL()//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetBookNoList, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomersBy_CB_DAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomersBy_Cycle_BookNo_CB, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument UpdateCustomerLongitudeAndLatitudeDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerLatitude_Longiture, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomersDetails(XmlDocument XmlDoc, ReturnType value)//Suresh
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCUSTOMERDETAILS_ACCOUNTNO, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetMarketerByAccountNo(XmlDocument XmlDoc, ReturnType value)//Suresh
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETMARKETERBYACCOUNTNO, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet BookNoSearchDAL(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_USP_BooknNoSearch, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #region Customer Import Methods

        public XmlDocument InsertBatchCustBulkUploadDAL(XmlDocument XmlDoc)//Neeraj
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertBatchCustomerBulkUpload, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument IsExistsBatchCustBulkUploadDAL(XmlDocument XmlDoc)//Neeraj
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsExistsBatchCustBulkUpload, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertBatchVsAccontNoDAL(XmlDocument XmlDoc)//Neeraj
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertBatchVsAccontNo, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument CustomerBulkUploadDAL(XmlDocument MultiXmlDoc)//Neeraj
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_BulkUload_Customers, MultiXmlDoc, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet GetCustDetailBlkUpldDAL(XmlDocument XmlDoc)//Neeraj
        {
            DataSet ds = new DataSet();
            try
            {
                CommonDAL objcd = new CommonDAL();
                ds = objcd.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustDetailBlkUpld, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        #endregion

        #region Customer Management With new table structure

        public XmlDocument CustomerRegistrationDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CustomerRegistration, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerAddress(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerAddress, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument ChangeCustomersAddress(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangeCustomersAddress, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument ChangeCustomersBookNo(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangeCustomersBookNo_New, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument IsOldAccountNoExists(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsOldAccountNoExists, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument IsEmailExists(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsEmailExists, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument AssignMeter(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_AssignMeter, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument ReAssignMeter(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_Re_AssignMeter, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerDetails(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerDetails, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerDetailsByApprovalID(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetApprovalRegistrationsByARID, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetIdentityDetails(XmlDocument XmlDoc)//Neeraj-ID077 --28-MARCH-2015
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetIdentityDetails, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public XmlDocument GetCustomerDocuments(XmlDocument XmlDoc)//Neeraj-ID077 --28-MARCH-2015
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerDocuments, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public XmlDocument GetCustomerUDFValues(XmlDocument XmlDoc)//Neeraj-ID077 --28-MARCH-2015
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerUDFValues, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public XmlDocument CustomerRegistrationByApproval(XmlDocument XmlDoc)//Neeraj-ID077 --23-July-2015
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_ApprovalRegistration, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        } 
        #endregion

        #region Reports
        public DataSet GetAccountswithoutCycleDAL(XmlDocument xmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(xmlDoc, PROC_USP_GETACCOOUMTSWITHOUTCYCLES, SqlCon, CommandTime);
            //return objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETACCOOUMTSWITHOUTCYCLES, SqlCon);
        }

        public DataSet GetAccountswithoutMeterDAL(XmlDocument xmlDoc)
        {
            //return objiDsignDAL.ExecuteXmlReader(xmlDoc, USP_GetAccountWithoutMeter, SqlCon);
            return objCommonDal.ExecuteReader_Get(xmlDoc, USP_GetAccountWithoutMeter, SqlCon, CommandTime);
        }
        //public DataSet GetTariffBUReportDAL()
        //{
        //    return objCommonDal.ExecuteReader_Get(USP_GetTARIFFBUPIVOTREPORT, SqlCon, CommandTime);
        //    //return objiDsignDAL.ExecuteXmlReader(USP_GetTARIFFBUPIVOTREPORT, SqlCon);
        //}
        public DataSet GetTariffBUReportDAL(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            try
            {
                ds = objCommonDal.ExecuteReader_Get(XmlDoc, USP_GetTARIFFBUPIVOTREPORT, SqlCon, CommandTime);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet GetAccountsWithNoNameOrAddress(XmlDocument xmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(xmlDoc, USP_RptGetCustomersNoNameOrAddList_New, SqlCon, CommandTime);
            //return objiDsignDAL.ExecuteXmlReader(USP_GetTARIFFBUPIVOTREPORT, SqlCon);
        }

        public XmlDocument GetCustomerForAssignMeter(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustDetailsForAssignMeter, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public XmlDocument GetCustomerForAdjustmentNoBill(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustInfoForAdjustmentNoBill, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public XmlDocument GetCustomerForBillGen(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustDetailsForBillGen, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerAddress_ByCheck(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerAddress_Check, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerBook_ByCheck(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerBook_Check, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerDetailsForMeter_ByCheck(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustDetailsForAssignMeter_ByCheck, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        #endregion


        #endregion
    }
}

