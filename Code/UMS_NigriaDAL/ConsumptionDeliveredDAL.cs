﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;

namespace UMS_NigriaDAL
{
    public class ConsumptionDeliveredDAL
    {
        #region Members
        CommonDAL objCommonDal = new CommonDAL();
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);

        private string PROC_USP_InsertConsumptionDelivered = "USP_InsertConsumptionDelivered";
        private string PROC_USP_UpdateConsumptionDelivered = "USP_UpdateConsumptionDelivered";
        private string PROC_USP_DeleteConsumptionDelivered = "USP_DeleteConsumptionDelivered";
        private string PROC_USP_GetConsumptionDelivered = "USP_GetConsumptionDelivered";
        #endregion

        public XmlDocument ConsumptionDelivered(XmlDocument XmlDoc, Statement value)
        {
                 XmlDocument xml = null;
                 try
                 {
                     switch (value)
                     {
                         case Statement.Insert:
                             xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertConsumptionDelivered, SqlCon); break;
                         case Statement.Update:
                             xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateConsumptionDelivered, SqlCon); break;
                         case Statement.Delete:
                             xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteConsumptionDelivered, SqlCon); break;
                     }
                 }
                 catch (Exception ex)
                 {
                     throw;
                 }
            return xml;
        }

        public XmlDocument ConsumptionDelivered(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetConsumptionDelivered, SqlCon); break;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return xml;
        }
    }
}
