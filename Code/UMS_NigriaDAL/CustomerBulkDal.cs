﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class CustomerBulkDal
    {
        #region Members
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        #endregion

        #region SP_Reports
        private string PROC_USP_RptGetAccountsWithDebitBal = "";
        #endregion

        #region Methods

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        #endregion
    }
}
