﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{

    public class CustomerDetailsDAL
    {
        public CustomerDetailsDAL()
        {
            InitializeProcedure();
        }

        public static string[] Procedures = new string[4];

        #region Member
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _ObjiDsignDAL = new iDsignDAL();
        #endregion

        #region Procedures

        public static string PROC_USP_GetCustomerBillDetails_ByAccountNo = "USP_GetCustomerBillDetails_ByAccountNo";
        public static string PROC_USP_GetCustomerBillReadings_ByAccountNo = "USP_GetCustomerBillReadings_ByAccountNo";
        public static string PROC_USP_UpdateCustomerReadings = "USP_UpdateCustomerReadings";
        public static string PROC_USP_GetCustomerReadCodes = "USP_GetCustomerReadCodes";

        public static string PROC_USP_GetExtraChargeNameAndCharges = "USP_GetExtraChargeNameAndCharges";
        public static string PROC_USP_UpdateEnergyCharges = "USP_UpdateEnergyCharges";
        public static string PROC_USP_UpdateExtraEnergyCharges = "USP_UpdateExtraEnergyCharges";
        public static string PROC_USP_GetEnergyChargesPerKW = "USP_GetEnergyChargesPerKW";

        public static string PROC_USP_CustomersCount = "USP_CustomersCount";

        public static string PROC_USP_InsertCustomerDetailsMulti = "USP_InsertCustomerDetailsMulti";

        public static string PROC_USP_GetBillAdjustmentDetails = "USP_GetBillAdjustmentDetails";//Neeraj

        public static string PROC_USP_GetCustomersByAccountNo_Serv = "USP_GetCustomersByAccountNo_Serv";//Neeraj

        public static string PROC_USP_GetCustomersDetailsForSPTBilling = "USP_GetCustomersDetailsForSPTBilling";//Neeraj

        public static string PROC_USP_InsertNewCustomerLocator = "USP_InsertNewCustomerLocator";//Neeraj

        public static string PROC_USP_GetCustLocatorForApproval = "USP_GetCustLocatorForApproval";//Neeraj

        public static string PROC_USP_GetCustLocator_Actioned = "USP_GetCustLocator_Actioned";//Neeraj

        public static string PROC_USP_ApproveCustLocator = "USP_ApproveCustLocator";//Neeraj

        public static string PROC_USP_UpdateCustomerOutstanding = "USP_UpdateCustomerOutstanding";//Neeraj

        public static string PROC_USP_GetCustomerBillDetailsByAccountNo = "USP_GetCustomerBillDetailsByAccountNo";//Karteek

        public static string PROC_USP_GetCustomerBillDetailsByAccountNo_ForAdjustment = "USP_GetCustomerBillDetailsByAccountNo_ForAdjustment";//Karteek

        public static string PROC_USP_GetCustomerPendingBillDetailsByAccountNo = "USP_GetCustomerPendingBillDetailsByAccountNo";//Karteek
                
        #endregion

        public XmlDocument Get(XmlDocument xml, ReturnType value)
        {
            XmlDocument xmldoc = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Bulk:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerBillDetailsByAccountNo, con); break;
                    //xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerBillDetails_ByAccountNo, con); break;
                    case ReturnType.Double:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerBillReadings_ByAccountNo, con); break;
                    case ReturnType.Fetch:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerReadCodes, con); break;
                    case ReturnType.Get:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetExtraChargeNameAndCharges, con); break;
                    case ReturnType.Group:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_UpdateEnergyCharges, con); break;
                    case ReturnType.List:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_UpdateExtraEnergyCharges, con); break;
                    case ReturnType.Multiple:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetEnergyChargesPerKW, con); break;
                    case ReturnType.Data:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerBillDetailsByAccountNo_ForAdjustment, con); break;
                        //xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerBillDetails_ByAccountNo, con); break;
                    case ReturnType.Retrieve:
                        xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerPendingBillDetailsByAccountNo, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xmldoc;
        }

        public XmlDocument Readings(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerReadings, con); break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument CountCustomersDAL(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Check:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CustomersCount, con); break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument InsertBulkCustomers(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomerDetailsMulti, con); break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument SendBillAdjustmentForApprovalDAL(XmlDocument XmlDoc)//Neeraj 
        {
            try
            {
                XmlDocument xml = null;
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, Procedures[1], con);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument SendMeterAdjustmentForApprovalDAL(XmlDocument XmlDoc)//Neeraj 
        {
            try
            {
                XmlDocument xml = null;
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, Procedures[2], con);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetBillAdjustmentDetailsDAL(XmlDocument XmlDoc)//Neeraj 
        {
            try
            {
                XmlDocument xml = null;
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, Procedures[3], con);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void InitializeProcedure()//Neeraj
        {
            Procedures[0] = "USP_GetBillAdjustmentDetails";
            Procedures[1] = "USP_Bill_Adjustment";
            Procedures[2] = "USP_Meter_Adjustment";
            Procedures[3] = "USP_Get_Bill_Adjustment_Details";
        }

        public XmlDocument GetCustomersByAccountNo_ServDAL(XmlDocument XmlDoc)//Neeraj --ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomersByAccountNo_Serv, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetCustomersDetailsForSPTBillingDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomersDetailsForSPTBilling, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertNewCustomerLocatorDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;

            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertNewCustomerLocator, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetCustLocatorForApprovalDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;

            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustLocatorForApproval, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetCustLocatorActionedDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;

            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustLocator_Actioned, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument ApproveCustLocatorDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;

            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ApproveCustLocator, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument UpdateCustomerOutstandingDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;

            try
            {
                xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerOutstanding, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

    }
}
