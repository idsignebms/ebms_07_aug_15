﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace UMS_NigriaDAL
{
    public class EmployeeDAL
    {
        #region constructor
        public EmployeeDAL()
        {
            //THIS CONSTRUCTOR USED TO INITIALISE ALL THE PROCEDURES IN METHOD InitializeProcedures()
            InitializeProcedures();
        }
        #endregion

        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        public static string[] Procedures = new string[1];
        #endregion

        #region Procedures
        public static void InitializeProcedures()//Neeraj
        {
            //Here maintain list for procedures:
            Procedures[0] = "[EMPLOYEE].[USP_EMPLOYEELIST]";
        }

        private static string PROC_USP_GetBEDCEmployeeList = "USP_GetBEDCEmployeeList";
        private static string PROC_USP_InsertBEDCEmployee = "USP_InsertBEDCEmployee";
        private static string PROC_USP_UpdateBEDCEmployee = "USP_UpdateBEDCEmployee";
        private static string PROC_USP_UpdateBEDCEmpActiveStatus = "USP_UpdateBEDCEmpActiveStatus";
        #endregion

        #region Bussiness Logic
        public XmlDocument GetEmployeeForListBindDAL()
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Procedures[0], con);
            }
            catch (Exception ex)
            {
                //write log
            }

            return xml;
        }
        #endregion

        #region AddBEDCEmmployee
        public XmlDocument InsertEmployee(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertBEDCEmployee, con); break; //Insert Employee
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateBEDCEmployee, con); break; //Update Employee
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateBEDCEmpActiveStatus, con); break; //Status Employee
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetEmployee(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBEDCEmployeeList, con); break;//Get Employees
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
