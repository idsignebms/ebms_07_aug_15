﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using UMS_NigeriaBE;

namespace UMS_NigriaDAL
{
    public class GeneralReportsDal
    {
        #region Members
        iDsignDAL _objiDsignDal = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Procedures
        private static string PROC_USP_RPTGETCUSTOMERLEDGERS = "USP_RptGetCustomerLedgers";//Ledger Report
        private static string PROC_USP_RPTGETACCOUNTSWITHDEBITBAL = "USP_RptGetAccountsWithDebitBal";//Debit Report
        private static string PROC_USP_RPTGETACCOUNTSWITHCREDITBAL = "USP_RptGetAccountsWithCreditBal";//Credit Report
        private static string PROC_USP_RPTGETPAYMETS = "USP_RptGetPaymets";//Payments Report
        private static string PROC_USP_GETACCOUNTWITHOUTMETER = "USP_GetAccountWithoutMeter";//Direct Customer Report
        private static string PROC_USP_GETACCOUNTWITHMETER = "USP_GetAccountWithMeter";//Read Customer Report
        private static string PROC_USP_RPTGETCUSTOMERSNONAMEORADDLIST = "USP_RptGetCustomersNoNameOrAddList";//No Name Or Address Customer Report
        private static string PROC_USP_GETCUSTOMERBILLSDETAILS = "USP_GetCustomerBillsDetails";//Customer Bills Report
        private static string PROC_USP_GETESTIMATIONDETAILSBYCYCLE = "USP_GetEstimationDetailsByCycle";//Usage Consumed Report
        private static string PROC_USP_RPTGETNOUSAGECUSTOMERSREPORT = "USP_RptGetNoUsageCustomersReport";//NoUsage Consumed Report

        #endregion

        #region Methods
        public DataSet GetLedgerReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETCUSTOMERLEDGERS, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetDebitReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETACCOUNTSWITHDEBITBAL, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetCreditReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETACCOUNTSWITHCREDITBAL, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetPaymentsReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETPAYMETS, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetDirectCustReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETACCOUNTWITHOUTMETER, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetReadCustReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETACCOUNTWITHMETER, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetNoNameOrAddressReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETCUSTOMERSNONAMEORADDLIST, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetBillsReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERBILLSDETAILS, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetUsageConsumedReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETESTIMATIONDETAILSBYCYCLE, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GetNoUsageConsumedReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETNOUSAGECUSTOMERSREPORT, SqlCon, Constants.CommandTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        #endregion
    }
}
