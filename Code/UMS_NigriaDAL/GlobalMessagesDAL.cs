﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class GlobalMessagesDAL
    {
        #region Members
        SqlConnection _sqlConnection = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _ObjiDsignDAL = new iDsignDAL();
        private string PROC_USP_ADDGLOBALMESSAGE = "USP_AddGlobalMessage";
        private string PROC_USP_GETSATES = "USP_GetStates";
        private string PROC_USP_GETBILLINGTYPES = "USP_GetBillingTypes";
        private string PROC_USP_DELETEGLOBALMESSAGE = "USP_DeleteGlobalMessage";
        private string PROC_USP_GETBILLINGMESSAGES = "USP_BindGlobalMessages";
        private string PROC_USP_GETDISTRICTLEVELMESSAGE = "USP_BindDistrictlevelMessages";
        private string PROC_USP_ADDDISTRICTLEVELMESSAGE = "USP_AddDistrictlevelMessage";
        private string PROC_USP_DELETEDISTRICTMESSAGE = "USP_DeleteDistrictMessage";
        private string PROC_USP_BINDVIEWGLOBALMSG = "USP_ViewGlobalMessages";
        //private string PROC_USP_GetBusinessUnitsForMsgs = "USP_GetBusinessUnitsForMsgs";
        private string PROC_USP_GetGlobalMessagesList = "USP_GetGlobalMessagesList";
        private string PROC_USP_UpdateGlobalMsgActiveStatus = "USP_UpdateGlobalMsgActiveStatus";
        
        #endregion

        #region Methods

        public XmlDocument InsertGlobalMessages(XmlDocument XmlDoc, Statement action)
        {
            try
            {
                XmlDocument xml = null;
                switch (action)
                {
                    case Statement.Insert:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ADDGLOBALMESSAGE, _sqlConnection);
                        break;                    
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetGlobalMessages(XmlDocument XmlDoc, ReturnType Value)
        {
            try
            {
                XmlDocument xml = null;
                switch (Value)
                {
                    case ReturnType.Get:
                        //xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBusinessUnitsForMsgs, _sqlConnection);
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetGlobalMessagesList, _sqlConnection);
                        break;
                    case ReturnType.Single:
                        //xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBusinessUnitsForMsgs, _sqlConnection);
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateGlobalMsgActiveStatus, _sqlConnection);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GlobalMessageActionDAL( XmlDocument xml, Statement action)
        {
            XmlDocument resultxml = null;
            switch (action)
            {
                case Statement.Insert:
               resultxml= _ObjiDsignDAL.ExecuteXmlReader(xml,PROC_USP_ADDGLOBALMESSAGE, _sqlConnection);
                break;
                case Statement.Delete:
                resultxml = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_DELETEGLOBALMESSAGE, _sqlConnection);
                break;
            }

            return resultxml;
        }
        public XmlDocument DistrictlevelMessageActionDAL(XmlDocument xml, Statement action)
        {
            XmlDocument resultxml = null;
            switch (action)
            {
                case Statement.Insert:
                    resultxml = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_ADDDISTRICTLEVELMESSAGE, _sqlConnection);
                    break;
                case Statement.Delete:
                    resultxml = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_DELETEDISTRICTMESSAGE, _sqlConnection);
                    break;
            }

            return resultxml;
        }
        public XmlDocument GetStatesDAL()
        {

            return _ObjiDsignDAL.ExecuteXmlReader(PROC_USP_GETSATES, _sqlConnection);
                   
        }
        public XmlDocument GetBillingMessagesDAL(XmlDocument xml)
        {

            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GETBILLINGMESSAGES, _sqlConnection);

        }
        public XmlDocument GetBillingMessagesBasedOnDistrictsDAL(XmlDocument xml)
        {

            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_BINDVIEWGLOBALMSG, _sqlConnection);

        }
        public XmlDocument GetDistrictlevelMessagesDAL(XmlDocument xml)
        {

            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GETDISTRICTLEVELMESSAGE, _sqlConnection);

        }
        public XmlDocument GetBillingTypesDAL()
        {

            return _ObjiDsignDAL.ExecuteXmlReader(PROC_USP_GETBILLINGTYPES, _sqlConnection);

        }
        #endregion

    }
}
