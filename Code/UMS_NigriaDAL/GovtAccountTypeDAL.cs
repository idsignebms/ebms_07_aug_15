﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class GovtAccountTypeDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Stored_Procedures

        private string PROC_USP_GET_NLEVELGOVTACCOUNTTYPES = "USP_Get_NLevelGovtAccountTypes";

        private string PROC_USP_GET_GOVTACCOUNTTYPESLIST = "USP_Get_GovtAccountTypesList";

        private string PROC_USP_InsertGovtAccountType = "USP_InsertGovtAccountType";

        private string PROC_USP_UPDATEGOVTACCOUNTTYPE = "USP_UpdateGovtAccountType";

        #endregion

        #region Methods

        public XmlDocument GovtAccoutnType(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertGovtAccountType, con);
                        break;//Insert  
                    case Statement.Delete:
                        //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETELGA, con); 
                        break;//Delete Country OR Active 
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEGOVTACCOUNTTYPE, con);
                        break;//
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetGovtAccountTypeList(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GET_NLEVELGOVTACCOUNTTYPES, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument GetGovtAccountTypeList()
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GET_GOVTACCOUNTTYPESLIST, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        #endregion
    }
}
