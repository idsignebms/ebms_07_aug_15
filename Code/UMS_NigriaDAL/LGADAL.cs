﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class LGADAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures
        //INSERT
        private string PROC_USP_INSERTLGA = "USP_InsertLGA";//Faiz - ID103

        //GET
        private string PROC_USP_GETLGALIST = "USP_GetLGAList";//Faiz - ID103
        private string PROC_USP_GETLGAS = "USP_GETLGAs";//Faiz - ID103


        //DELETE
        private string PROC_USP_DELETELGA = "USP_DeleteLGA";//Faiz - ID103

        //UPDATE
        private string PROC_USP_UPDATELGA = "USP_UpdateLGA";//Faiz - ID103
        #endregion


        #region Methods

        public XmlDocument LGA(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTLGA, con); break;//Insert Countries 
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETELGA, con); break;//Delete Country OR Active Country
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATELGA, con); break;//Update Country
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument LGA(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETLGALIST, con); break;//Get LGA  //--Faiz - ID103
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETLGAS, con); break;//Get LGA  //--Faiz - ID103
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
