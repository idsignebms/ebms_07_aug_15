﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;

namespace UMS_NigriaDAL
{
    public class LoginPageDAL
    {
        #region Members

        iDsignDAL _ObjiDsignDAL = new iDsignDAL();
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);

        #endregion
        # region Procedures
        //Get
        private string PROC_USP_GetUserLoginDetails = "USP_GetUserLoginDetails";
        private string PROC_USP_IsSuperAdmin = "USP_IsSuperAdmin";

        #endregion


        #region Methods
        //Is For Get Login Details
        public XmlDocument GetLoginDetails(XmlDocument XmlDoc, ReturnType value)  //Get List of SubType ClassNames
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUserLoginDetails, con); break;
                    case ReturnType.Single:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsSuperAdmin, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
