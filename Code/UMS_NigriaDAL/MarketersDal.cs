﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class MarketersDal
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures
        public static string PROC_USP_INSERTMARKETERS = "USP_InsertMarketers";
        public static string PROC_USP_UPDATEMARKETERS = "USP_UpdateMarketers";
        public static string PROC_USP_DELETEMARKETERS = "USP_DeleteMarketers";
        public static string PROC_USP_GETMARKETERS = "USP_GetMarketers";

        public static string PROC_USP_GETMARKETERSLIST = "USP_GetMarketersList";
        public static string PROC_USP_GetCustomerMarketers_ByBookNo = "USP_GetCustomerMarketers_ByBookNo";
        
        #endregion

        #region Methods
        public XmlDocument InsertMarketer(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTMARKETERS, con); break;//Insert Marketer 
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEMARKETERS, con); break;//Update Marketer
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEMARKETERS, con); break;//Update Marketer
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetMarketer(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETMARKETERS, con); break;//Get Marketer     
                    case ReturnType.List:
                        xml=objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETMARKETERSLIST, con); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerMarketers_ByBookNo, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        
        #endregion
    }
}
