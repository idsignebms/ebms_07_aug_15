﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace UMS_NigriaDAL
{
    public class MastersDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures

        #region Countries,States,Districts Pages
        //INSERT
        private static string PROC_USP_INSERTCOUNTRIES = "USP_InsertCountries";
        private static string PROC_USP_INSERTSTATES = "USP_InsertStates";
        //private static string PROC_USP_INSERTDISTRICTS = "USP_InsertDistricts";
        //GET
        private static string PROC_USP_GETCOUNTRIESLIST = "USP_GetCountriesList";
        private static string PROC_USP_GETSTATESLIST = "USP_GetStatesList";
        private static string PROC_USP_GETACTIVECOUNTRIESLIST = "USP_GetActiveCountriesList";
        private static string PROC_USP_GETACTIVESTATESLIST = "USP_GetActiveStatesList";
        private static string PROC_USP_GETACTIVEDISTRICTSLIST = "USP_GetActiveDistrictsList";
        private static string PROC_USP_GETSTATEDETAILSBYCOUNTRYCODE = "USP_GetStateDetailsByCountryCode";
        //private static string PROC_USP_GetStateDetailsByDistrict = "USP_GetStateDetailsByDistrict";


        //DELETE
        private static string PROC_USP_DELETECOUNTRY = "USP_DeleteCountry";
        private static string PROC_USP_DELETESTATE = "USP_DELETESTATE";
        //private static string PROC_USP_DELETEDISTRICT = "USP_DeleteDistrict";
        private static string PROC_USP_DELETEPAYMENTENTRYLIST = "USP_DeletePaymentEntryList";
        private static string PROC_USP_DeleteBatchWithPayments = "USP_DeleteBatchWithPayments";

        //UPDATE
        private static string PROC_USP_UPDATECOUNTRY = "USP_UpdateCountry";
        private static string PROC_USP_UPDATESTATE = "USP_UpdateState";
        //private static string PROC_USP_UPDATEDISTRICT = "USP_UpdateDistrict";


        #endregion

        #region BusinessUnits
        //GET
        //private static string PROC_USP_GETDISTRICTSBYSTATECODE = "USP_GetDistrictsByStateCode";
        private static string PROC_USP_GETBUSINESSUNITSLIST = "USP_GetBusinessUnitsList";
        private static string PROC_USP_GETDISTRICTSLIST = "USP_GetDistrictsList";
        private static string PROC_USP_GetBusinessUnitsByStateCode = "USP_GetBusinessUnitsByStateCode";
        private static string PROC_USP_GetStateDetailsByBusinessUnits = "USP_GetStateDetailsByBusinessUnits";
        private static string PROC_USP_CheckIsPageAccess = "USP_CheckIsPageAccess";
        private static string PROC_USP_GETBUSINESSUNITS_BYUSER = "USP_GETBUSINESSUNITS_BYUSER";//Suresh
        //INSERT
        private static string PROC_USP_USP_INSERTBUSINESSUNITS = "USP_InsertBusinessUnits";
        //Update
        private static string PROC_USP_UPDATEBUSSINESSUNITS = "USP_UpdateBussinessUnits";

        //Delete
        private static string PROC_USP_DELETEBUSINESSUNITS = "USP_DeleteBusinessUnits";
        #endregion

        #region ServiceUnits
        //GET
        public static string PROC_USP_GETSERVICEUNITSLIST = "USP_GetServiceUnitsList";
        //INSERT
        public static string PROC_USP_USP_INSERTSERVICEUNIT = "USP_InsertServiceUnits";
        //UPDATE
        public static string PROC_USP_USP_UPDATESERVICEUNIT = "USP_UpdateServiceUnit";
        //DELETE
        public static string PROC_USP_USP_DELETESERVICEUNIT = "USP_DeleteServiceUnits";
        #endregion

        #region ServiceCenters
        //GET
        public static string PROC_USP_GETSERVICECENTERSLIST = "USP_GetServiceCentersList";
        //INSERT
        public static string PROC_USP_INSERTSERVICECENTER = "USP_InsertServiceCenter";
        //UPDATE
        public static string PROC_USP_UPDATESERVICECENTER = "USP_UpdateServiceCenter";
        //DELETE
        public static string PROC_USP_DELETESERVICECENTER = "USP_DeleteServiceCenter";
        #endregion

        #region SubStations
        //GET
        public static string PROC_USP_GETSUBSTATIONSLIST = "USP_GetSubStationsList";
        //INSERT
        public static string PROC_USP_INSERTSUBSTATION = "USP_InsertSubStation";
        //UPDATE
        public static string PROC_USP_UPDATESUBSTATION = "USP_UpdateSubStation";
        //DELETE
        public static string PROC_USP_DELETESUBSTATION = "USP_DeleteSubStation";
        #endregion

        #region Feeders
        //GET
        public static string PROC_USP_GETFEEDERSLIST = "USP_GetFeedersList";
        //INSERT
        public static string PROC_USP_INSERTFEEDER = "USP_InsertFeeder";
        //UPDATE
        public static string PROC_USP_UPDATEFEEDER = "USP_UpdateFeeder";
        //DELETE
        public static string PROC_USP_DELETEFEEDER = "USP_DeleteFeeder";
        private static string PROC_USP_GETALLFEEDERSLIST = "USP_GETALLFEEDERSLIST";


        #endregion

        #region TransFormers
        //GET
        public static string PROC_USP_GETTRANSFORMERSLIST = "USP_GetTransFormersList";
        //INSERT
        public static string PROC_USP_INSERTTRANSFORMER = "USP_InsertTransformer";
        //UPDATE
        public static string PROC_USP_UPDATETRANSFORMER = "USP_UpdateTransFormer";
        //DELETE
        public static string PROC_USP_DELETETRANSFORMER = "USP_DeleteTransFormer";
        #endregion

        #region Poles
        //GET
        //public static string PROC_USP_GETPOLESLIST = "USP_GetPolesList";
        //INSERT
        //public static string PROC_USP_INSERTPOLE = "USP_InsertPole";
        //UPDATE
        //public static string PROC_USP_UPDATEPOLE = "USP_UpdatePole";
        //DELETE
        //public static string PROC_USP_DELETEPOLE = "USP_DeletePole";
        #endregion

        #region Cycle
        //GET
        public static string PROC_USP_GETCYCLES = "USP_GetCycles";
        public static string PROC_USP_GetCyclesByBUSUSC = "USP_GetCyclesByBUSUSC";
        public static string PROC_USP_GetCyclesBySC_HavingBooks = "USP_GetCyclesBySC_HavingBooks";

        //INSERT
        public static string PROC_USP_INSERTCYCLE = "USP_InsertCycle";
        //UPDATE
        public static string PROC_USP_UPDATECYCLEDETAILS = "USP_UpdateCycleDetails";
        //DELETE
        public static string PROC_USP_UPDATECYCLEACTIVESTATUS = "USP_UpdateCycleActiveStatus";


        #endregion

        #region BookNumber
        //GET
        public static string PROC_USP_GETBOOKNUMBERS = "USP_GetBookNumbers";
        public static string PROC_USP_GetAllBookNo_ByBU_ID = "USP_GetAllBookNo_ByBU_ID";

        //INSERT
        public static string PROC_USP_INSERTBOOKNUMBER = "USP_InsertBookNumber";
        //UPDATE
        public static string PROC_USP_UPDATEBOOKNUMBERDETAILS = "USP_UpdateBookNumberDetails";
        //DELETE
        public static string PROC_USP_UPDATEBOOKNUMBERACTIVESTATUS = "USP_UpdateBookNumberActiveStatus";
        #endregion

        #region Agency
        //GET
        public static string PROC_USP_GETAGENCIES = "USP_GetAgencies";
        //INSERT
        public static string PROC_USP_INSERTAGENCY = "USP_InsertAgency";
        //UPDATE
        public static string PROC_USP_UPDATEAGENCYDETAILS = "USP_UpdateAgencyDetails";
        //DELETE
        public static string PROC_USP_UPDATEAGENCYACTIVESTATUS = "USP_UpdateAgencyActiveStatus";
        #endregion

        #region MeterTypes
        //GET
        public static string PROC_USP_GetMeterTypesList = "USP_GetMeterTypesList";
        //INSERT
        public static string PROC_USP_InsertMeterType = "USP_InsertMeterType";
        //UPDATE
        public static string PROC_USP_UpdateMeterTypes = "USP_UpdateMeterTypes";
        //DELETE
        public static string PROC_USP_UpdateMeterTypeActiveStatus = "USP_UpdateMeterTypeActiveStatus";

        public string PROC_USP_GetMeterNoList = "MASTERS.[USP_GetMeterNoList]";
        #endregion

        #region CustomerTypes
        //GET
        public static string PROC_USP_GetCustomersTypesList = "USP_GetCustomersTypesList";
        //INSERT
        public static string PROC_USP_InsertCustomerType = "USP_InsertCustomerType";
        //UPDATE
        public static string PROC_USP_UpdateCustomerTypes = "USP_UpdateCustomerTypes";
        //DELETE
        public static string PROC_USP_UpdateCustomerTypeActiveStatus = "USP_UpdateCustomerTypeActiveStatus";

        #endregion

        #region CustomersIdentityTypes
        //GET
        public static string PROC_USP_GetCustomersIdentityTypesList = "USP_GetCustomersIdentityTypesList";
        //INSERT
        public static string PROC_USP_InsertCustomerIdentityType = "USP_InsertCustomerIdentityType";
        //UPDATE
        public static string PROC_USP_UpdateCustomerIdentityTypes = "USP_UpdateCustomerIdentityTypes";
        //DELETE
        public static string PROC_USP_UpdateCustomerIdentityTypeActiveStatus = "USP_UpdateCustomerIdentityTypeActiveStatus";

        #endregion

        #region Designation
        //GET
        public static string PROC_USP_GetDesignationList = "USP_GetDesignationList";
        //INSERT
        public static string PROC_USP_InsertDesignation = "USP_InsertDesignation";
        //UPDATE
        public static string PROC_USP_UpdateDesignation = "USP_UpdateDesignation";
        //DELETE
        public static string PROC_USP_UpdateDesignationActiveStatus = "USP_UpdateDesignationActiveStatus";

        #endregion

        #region Title
        //GET
        public static string PROC_USP_GetTitleList = "USP_GetTitleList";
        //INSERT
        public static string PROC_USP_InsertTitle = "USP_InsertTitle";
        //UPDATE
        public static string PROC_USP_UpdateTitle = "USP_UpdateTitle";
        //DELETE
        public static string PROC_USP_UpdateTitleActiveStatus = "USP_UpdateTitleActiveStatus";

        #endregion

        #region Account Types
        //GET
        public static string PROC_USP_GetAccountTypesList = "USP_GetAccountTypesList";
        //INSERT
        public static string PROC_USP_InsertAccountType = "USP_InsertAccountType";
        //UPDATE
        public static string PROC_USP_UpdateAccountTypes = "USP_UpdateAccountTypes";
        //DELETE
        public static string PROC_USP_UpdateAccountTypeActiveStatus = "USP_UpdateAccountTypeActiveStatus";

        public string PROC_USP_GetAccountNoList = "MASTERS.[USP_GetAccountNoList]";
        #endregion

        #region RouteMangement
        private static string PROC_USP_InsertRouteManagement = "USP_InsertRouteManagement";
        private static string PROC_USP_GetRouteManagement = "USP_GetRouteManagement";
        private static string PROC_USP_UpdateRouteManagement = "USP_UpdateRouteManagement";
        private static string PROC_USP_UpdateRouteActiveStatus = "USP_UpdateRouteActiveStatus";
        #endregion

        #region PaymentEntry

        private string PROC_USP_GETPAYMENTMODES = "USP_GetPaymentModes";
        private string PROC_USP_InsertCustomerPayments = "USP_InsertCustomerPayments";
        private static string PROC_USP_GetCustomerPendingBills = "USP_GetCustomerPendingBills";
        private static string PROC_USP_GetPaymentEntryListByBatchNo = "USP_GetPaymentEntryListByBatchNo";
        private static string PROC_USP_GetCustomerDetailsForPaymentEntry = "USP_GetCustomerDetailsForPaymentEntry";
        private string PROC_USP_InsertCustomerPaymentsApproval = "USP_InsertCustomerPaymentsApproval";
        private string PROC_USP_InsertCustomerPaymentsTemp = "USP_InsertCustomerPaymentsTemp";

        //private static string PROC_USP_INSERTBILLPAYMENTS = "USP_INSERTBILLPAYMENTS";//Suresh
        private static string PROC_USP_INSERTBILLPAYMENTS = "USP_InsertCustomerBillPayments";//Karteek

        private static string PROC_USP_INSERTBILLPAYMENTSAPPROVAL = "USP_INSERTBILLPAYMENTSAPPROVAL";
        private static string PROC_USP_InsertBillPaymentsByServ = "USP_InsertBillPaymentsByServ";//Neeraj
        private static string PROC_USP_ValidatePaymentUpload = "USP_ValidatePaymentUpload";//Neeraj
        private static string PROC_USP_GETPAYMENTMODEIDBYBATCHNO = "USP_GetPaymentmodeIdByBatchNo";//Neeraj

        private static string PROC_USP_ValidatePaymentUpload_WithDT = "USP_ValidatePaymentUpload_WithDT";//Karteek

        #endregion

        #region CashOffice
        private static string PROC_USP_InsertCashOffices = "USP_InsertCashOffices";
        private static string PROC_USP_GetCashOfficesList = "USP_GetCashOfficesList";
        private static string PROC_USP_UpdateCashOfficesActiveStatus = "USP_UpdateCashOfficesActiveStatus";
        private static string PROC_USP_UpdateCashOffices = "USP_UpdateCashOffices";
        #endregion

        #region MeterInformation

        private static string PROC_USP_InsertMeterInformation = "USP_InsertMeterInformation";
        private static string PROC_USP_GetMeterInformation = "USP_GetMeterInformation";
        private static string PROC_USP_UpdateMeterInformationActiveStatus = "USP_UpdateMeterInformationActiveStatus";
        private static string PROC_USP_UpdateMeterInformation = "USP_UpdateMeterInformation";
        public string PROC_USP_GetDisconnectedMeterDetials = "MASTERS.USP_GetDisconnectedMeterDetials";

        #endregion

        #region Account Type Information

        private static string PROC_USP_InsertAccountInformation = "USP_InsertAccountInformation";
        private static string PROC_USP_GetAccountTypeInformation = "USP_GetAccountTypeInformation";
        private static string PROC_USP_UpdateAccountTypeInformationActiveStatus = "USP_UpdateAccountTypeInformationActiveStatus";
        private static string PROC_USP_UpdateAccountTypeInformation = "USP_UpdateAccountTypeInformation";

        #endregion

        #region DocumentUploadPayments
        //private static string PROC_USP_INSERTCUSTOMERPAYMENTSDOCUMENT = "USP_INSERTCUSTOMERPAYMENTSDOCUMENT";//Naresh
        private static string PROC_USP_INSERTCUSTOMERPAYMENTSDOCUMENT = "USP_InsertBulkCustomerPayments";//Karteek
        private static string PROC_USP_InsertBulkCustomerPayments_WithDT = "USP_InsertBulkCustomerPayments_WithDT";//Karteek
        private static string PROC_USP_InsertBulkCustomerPayments_WithDT_Temp = "USP_InsertBulkCustomerPayments_WithDT_Temp";//Karteek

        #endregion

        #region Customers
        private static string PROC_USP_IsCustomerInBillProcess = "USP_IsCustomerInBillProcess";//Neeraj
        private static string PROC_USP_GetAccountNumbersInBillingProcess = "USP_GetAccountNumbersInBillingProcess";//Neeraj
        #endregion

        #region Approval
        private static string PROC_USP_GetApprovalsList = "USP_GetApprovalsList";
        #endregion

        #region EBill
        private static string Proc_UPS_GET_EBILL_PAYMENT_DETAILS = "UPS_GET_EBILL_PAYMENT_DETAILS";//Neeraj
        private static string Proc_UPS_CheckEBillExecutionTime = "UPS_CheckEBillExecutionTime";//Neeraj

        private static string Proc_USP_EBillPayment = "USP_EBillPayment";//Neeraj

        private static string Proc_USP_InsertEBillMailLogs = "USP_InsertEBillMailLogs";//Neeraj
        private static string Proc_USP_GetEBillMailsToSend = "USP_GetEBillMailsToSend";//Neeraj
        private static string Proc_USP_UpdateEBillMailsToSend = "USP_UpdateEBillMailsToSend";//Neeraj
        private static string Proc_USP_GetBuHeadOfficeEmails = "USP_GetBuHeadOfficeEmails";//Neeraj

        #endregion

        #region Key Authentication
        private static string Proc_USP_WS_AuthenticateKey = "USP_WS_AuthenticateKey";
        #endregion

        #region Estimation
        private string Proc_USP_GetEstimationDetails = "USP_GetEstimationDetails"; //Neeraj-ID077
        private string Proc_USP_InsertEstimation = "USP_InsertEstimation";//Neeraj-ID077
        private string Proc_USP_GetEstimationDetailsByCycle = "USP_GetEstimationDetailsByCycle";//Neeraj-ID077
        private string Proc_USP_UpdateEstimation = "USP_UpdateEstimation";//Neeraj-ID077
        private string Proc_USP_DeleteEstimationSetting = "USP_DeleteEstimationSetting";//Neeraj-ID077

        private static string PROC_USP_GETESTIMATIONLIST_CYCLE = "USP_GETESTIMATIONLIST_CYCLE";// Suresh
        //private static string PROC_USP_GETESTIMATEDCYCLES_SC = "USP_GETESTIMATEDCYCLES_SC";// Suresh

        //private static string PROC_USP_GETESTIMATEDCYCLES_SC = "USP_GetEstimationSettingsData_BySC";//Karteek 
        private static string PROC_USP_GETESTIMATEDCYCLES_SC = "USP_GetEstimationSettings_CycleWiseData_BySC";//Karteek 
        
        #endregion

        #region Spot Billing

        private static string PROC_USP_GetSpotBillingInputs = "USP_GetSpotBillingInputs"; //Neeraj-ID077

        private static string PROC_USP_InsertHHIputFileDetails = "USP_InsertHHIputFileDetails"; //Neeraj-ID077

        private static string PROC_USP_IsHHInputFileExists = "USP_IsHHInputFileExists"; //Neeraj-ID077

        private static string PROC_USP_GetHHInputFilesList = "USP_GetHHInputFilesList"; //Neeraj-ID077

        private static string PROC_USP_DeleteHHInputFile = "USP_DeleteHHInputFile"; //Neeraj-ID077

        #endregion

        #region Autocomplete

        private static string PROC_USP_AutoCompleteList = "USP_AutoCompleteList"; //Neeraj-ID077

        #endregion

        //#region Pole Management
        ////INSERT
        //public static string PROC_USP_INSERTPOLEMASTERDETAILS = "USP_InsertPoleMasterDetails";


        //#endregion

        #region Meter
        private static string PROC_USP_GetAvailableMeterList = "MASTERS.USP_GetAvailableMeterList"; //Neeraj-ID077
        #endregion

        #region cluster Category
        private string PROC_USP_GetClusterCategoriesListToBind = "MASTERS.USP_GetClusterCategoriesListToBind"; //Neeraj-ID077
        #endregion

        #region User Defined Controls
        private string PROC_USP_GetUserDefinedControls = "MASTERS.USP_GetUserDefinedControls"; //Neeraj-ID077
        #endregion

        #region Bind SC
        private string PROC_USP_GetCyclesByBUSUSCWithBillingStatus = "USP_GetCyclesByBUSUSCWithBillingStatus"; //Neeraj-ID077
        #endregion

        #region Bind SC
        private string PROC_USP_IsMeterAvailable = "MASTERS.USP_IsMeterAvailable_New"; //Neeraj-ID077
        private string PROC_USP_ChangeReadCustToDirect = "MASTERS.USP_ChangeReadCustToDirect"; //Neeraj-ID077
        #endregion

        #endregion

        #region Methods

        #region Methods For Countries,States,Districts Pages

        public XmlDocument Countries(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTCOUNTRIES, con); break;//Insert Countries 
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETECOUNTRY, con); break;//Delete Country OR Active Country
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATECOUNTRY, con); break;//Update Country
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument States(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTSTATES, con); break;//Insert States
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETESTATE, con); break;//Delete State
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATESTATE, con); break;//Update State
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        //public XmlDocument Districts(XmlDocument XmlDoc, Statement action)
        //{
        //    XmlDocument xml = null;
        //    try
        //    {
        //        switch (action)
        //        {
        //            //case Statement.Insert:
        //            //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTDISTRICTS, con); break;//Insert Districts
        //            //case Statement.Delete:
        //            //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEDISTRICT, con); break;//Delete District
        //            //case Statement.Update:
        //            //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEDISTRICT, con); break;//Update District
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return xml;
        //}

        public XmlDocument GetCountries(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETCOUNTRIESLIST, con); break;//Get Countries List For Binding DDL            

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetStates(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETSTATESLIST, con); break;//Get Countries List For Binding DDL
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Get(XmlDocument xmlDoc, ReturnType value)
        {
            XmlDocument xml = new XmlDocument();

            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETACTIVECOUNTRIESLIST, con);//Get Countries List For Grid
                        break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETACTIVESTATESLIST, con);//Get Countries List For Grid
                        break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETACTIVEDISTRICTSLIST, con);//Get Districts List For Grid
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETSTATEDETAILSBYCOUNTRYCODE, con);//Get States List For DDl After Selecting Countryddl
                        break;
                    //case ReturnType.Fetch:
                    //    xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetStateDetailsByDistrict, con);//Get States List For DDl After Selecting Countryddl
                    //    break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetCustomerPendingBills, con);//Get States List For DDl After Selecting Countryddl
                        break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetPaymentEntryListByBatchNo, con);//Get States List For DDl After Selecting Countryddl
                        break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetCustomerDetailsForPaymentEntry, con);//Get States List For DDl After Selecting Countryddl
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Business Units
        public XmlDocument GetBusinessUnits(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    //case ReturnType.Get:
                    //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETDISTRICTSBYSTATECODE, con); break; //Get Districts By StateCode
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBUSINESSUNITSLIST, con); break; //Get Business Units List
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBusinessUnitsByStateCode, con); break; //Get Business Units List
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetStateDetailsByBusinessUnits, con); break; //Get Business Units List
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CheckIsPageAccess, con); break; //Get Business Units List
                    case ReturnType.User:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBUSINESSUNITS_BYUSER, con); break; //Get Business Units List By Users-- Suresh

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetBusinessUnits(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETDISTRICTSLIST, con); break; //Get Districts List For DDL                    
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument BusinessUnits(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_INSERTBUSINESSUNITS, con); break;//Insert BusinessUnits
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEBUSINESSUNITS, con); break;//Delete BusinessUnits
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEBUSSINESSUNITS, con); break;//Update BusinessUnits
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region ServiceUnits
        public XmlDocument ServiceUnit(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_INSERTSERVICEUNIT, con); break;//Insert ServiceUnits
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_DELETESERVICEUNIT, con); break;//Delete ServiceUnits
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_UPDATESERVICEUNIT, con); break;//Update ServiceUnits
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument ServiceUnit(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETSERVICEUNITSLIST, con); break;//Get ServiceUnits
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region ServiceCenters
        public XmlDocument ServiceCenter(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTSERVICECENTER, con); break;//Insert ServiceCEnter
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETESERVICECENTER, con); break;//Delete ServiceCEnter
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATESERVICECENTER, con); break;//Update ServiceCEnter
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument ServiceCenter(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETSERVICECENTERSLIST, con); break;//Get ServiceCEnter
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region SubStations
        public XmlDocument SubStation(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTSUBSTATION, con); break;//Insert SubStation
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETESUBSTATION, con); break;//Delete SubStation
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATESUBSTATION, con); break;//Update SubStation
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument SubStation(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETSUBSTATIONSLIST, con); break;//Get SubStation
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Feeders

        public XmlDocument Feeder(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTFEEDER, con); break;//Insert Feeder
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEFEEDER, con); break;//Delete Feeder
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEFEEDER, con); break;//Update Feeder
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Feeder(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETFEEDERSLIST, con); break;//Get Feeder
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetFeeders(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETALLFEEDERSLIST, con); break;//Get Feeders
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region TransFormer
        public XmlDocument TransFormer(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTTRANSFORMER, con); break;//Insert TransFormer
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETETRANSFORMER, con); break;//Delete TransFormer
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATETRANSFORMER, con); break;//Update TransFormer
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument TransFormer(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETTRANSFORMERSLIST, con); break;//Get TransFormer
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Poles
        //public XmlDocument Pole(XmlDocument XmlDoc, Statement action)
        //{
        //    XmlDocument xml = null;
        //    try
        //    {
        //        switch (action)
        //        {
        //            //case Statement.Insert:
        //            //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPOLE, con); break;//Insert Pole
        //            //case Statement.Delete:
        //            //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEPOLE, con); break;//Delete Pole
        //            //case Statement.Update:
        //            //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEPOLE, con); break;//Update Pole
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return xml;
        //}

        //public XmlDocument Pole(XmlDocument XmlDoc, ReturnType value)
        //{
        //    XmlDocument xml = null;
        //    try
        //    {
        //        switch (value)
        //        {
        //            case ReturnType.Get:
        //                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPOLESLIST, con); break;//Get Pole
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return xml;
        //}
        #endregion

        #region Cycle
        public XmlDocument Cycle(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTCYCLE, con); break;//Insert Cycle
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATECYCLEACTIVESTATUS, con); break;//Delete Cycle
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATECYCLEDETAILS, con); break;//Update Cycle
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Cycle(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCYCLES, con); break;//Get Cycle
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCyclesByBUSUSC, con); break;//Get Cycles list by BU,SU,SC
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCyclesBySC_HavingBooks, con); break;//Get Cycles list by BU,SU,SC
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region BookNumber
        public XmlDocument BookNumber(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTBOOKNUMBER, con); break;//Insert BookNumber
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEBOOKNUMBERACTIVESTATUS, con); break;//Delete BookNumber
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEBOOKNUMBERDETAILS, con); break;//Update BookNumber
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument BookNumber(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBOOKNUMBERS, con); break;//Get BookNumber
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAllBookNo_ByBU_ID, con); break;//Get BookNumber
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Agency
        public XmlDocument Agency(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTAGENCY, con); break;//Insert Agency
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEAGENCYACTIVESTATUS, con); break;//Delete Agency
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEAGENCYDETAILS, con); break;//Update Agency
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Agency(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETAGENCIES, con); break;//Get Agency
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region MeterType
        public XmlDocument MeterType(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertMeterType, con); break; //Insert MeterType
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateMeterTypeActiveStatus, con); break;//Delete MeterType
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateMeterTypes, con); break;//Update MeterType
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertMeterInformation, con); break;//Insert MeterInformation
                    case Statement.Modify:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateMeterInformationActiveStatus, con); break;//Delete MeterInformation
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateMeterInformation, con); break;//UPDATE MeterInformation
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument MeterType(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetMeterTypesList, con); break;//Get MeterTypes
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetMeterInformation, con); break;//Get MeterInformation

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region CustomerTypes
        public XmlDocument CustomerType(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomerType, con); break; //Insert CustomerType
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerTypeActiveStatus, con); break;//Delete CustomerType
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerTypes, con); break;//Update CustomerType

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument CustomerType(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomersTypesList, con); break;//Get MeterTypes
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region CustomerIdentityTypes
        public XmlDocument CustomerIdentityType(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomerIdentityType, con); break; //Insert CustomerIdentityType
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerIdentityTypeActiveStatus, con); break;//Delete CustomerIdentityType
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerIdentityTypes, con); break;//Update CustomerIdentityType

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument CustomerIdentityType(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomersIdentityTypesList, con); break;//Get CustomerIdentityType
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Designation
        public XmlDocument Designation(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertDesignation, con); break; //Insert Designation
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateDesignationActiveStatus, con); break;//Delete Designation
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateDesignation, con); break;//Update Designation

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Designation(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetDesignationList, con); break;//Get Designation
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Title
        public XmlDocument Title(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertTitle, con); break; //Insert Title
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTitleActiveStatus, con); break;//Delete Title
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTitle, con); break;//Update Title

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Title(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetTitleList, con); break;//Get Title
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region AccountType
        public XmlDocument AccountType(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertAccountType, con); break; //Insert MeterType
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateAccountTypeActiveStatus, con); break;//Delete MeterType
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateAccountTypes, con); break;//Update MeterType
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertMeterInformation, con); break;//Insert MeterInformation
                    case Statement.Modify:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateAccountTypeInformationActiveStatus, con); break;//Delete MeterInformation
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateAccountTypeInformation, con); break;//UPDATE MeterInformation
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument AccountType(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAccountTypesList, con); break;//Get MeterTypes
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAccountTypeInformation, con); break;//Get MeterInformation

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region RouteManagement
        public XmlDocument RouteManagement(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertRouteManagement, con); break; //Insert RouteManagement
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateRouteActiveStatus, con); break;//Delete RouteManagement
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateRouteManagement, con); break;//Update RouteManagement
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument RouteManagement(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetRouteManagement, con); break;//Get RouteManagement
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region PaymentEntry

        public XmlDocument PaymentEntry(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTBILLPAYMENTS, con);//By Suresh
                        //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomerPayments, con);//Commmented By Suresh
                        break;//Insert PaymentEntry
                    case Statement.Service://For inserting payment from service
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertBillPaymentsByServ, con);//By Neeraj
                        break;
                    //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPAYMENTS, con); break; //Insert PaymentEntry
                    //case Statement.Delete:
                    //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, con); break;//Delete PaymentEntry
                    //case Statement.Update:
                    //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, con); break;//Update PaymentEntry
                    case Statement.Check://For inserting payment from service
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPAYMENTMODEIDBYBATCHNO, con);// For Get PaymentModeId By BatchNo
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument BatchEntry(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPAYMENTMODEIDBYBATCHNO, con);// For Get PaymentModeId By BatchNo
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument PaymentEntry(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETPAYMENTMODES, con);
                        break;//Get PaymentEntry
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument PaymentEntryTemporaryDAL(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:

                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTBILLPAYMENTSAPPROVAL, con); break; //By Suresh for Saving payments temporarly 
                    //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomerPaymentsTemp, con); break; //Insert PaymentEntry
                    //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPAYMENTS, con); break; //Insert PaymentEntry
                    //case Statement.Delete:
                    //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, con); break;//Delete PaymentEntry
                    //case Statement.Update:
                    //    xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, con); break;//Update PaymentEntry
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument PaymentUploadValidationDAL(XmlDocument xmlDoc, XmlDocument MultiXmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objCommonDal.ExecuteXmlReader(xmlDoc, MultiXmlDoc, PROC_USP_ValidatePaymentUpload, con, Constants.CommandTimeOut);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public DataSet GetValidatePaymentsUpload(string BU_ID, DataTable dtPayments)
        {
            SqlParameter[] parameters =
            {
              new SqlParameter("@Bu_Id", SqlDbType.VarChar,50){ Value = BU_ID },
              new SqlParameter("@TblPayments", SqlDbType.Structured){ Value = dtPayments }
            };
            return objCommonDal.ExecuteReader_GetDataSet(PROC_USP_ValidatePaymentUpload_WithDT, parameters, con, Constants.CommandTimeOut);
        }

        #endregion

        #region CashOffices
        public XmlDocument CashOffices(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCashOffices, con); break; //Insert CashOffices
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCashOfficesActiveStatus, con); break;//Delete CashOffices
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCashOffices, con); break;//Update CashOffices
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public DataSet GetCashOfficesList(XmlDocument XmlDoc)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCashOfficesList, con, 300);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region DocumentPayments
        public XmlDocument Insert(XmlDocument xmlDoc, XmlDocument multiXmlDoc, Statement action)
        {
            XmlDocument xml = new XmlDocument();

            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objCommonDal.ExecuteXmlReader(xmlDoc, multiXmlDoc, PROC_USP_INSERTCUSTOMERPAYMENTSDOCUMENT, con, Constants.CommandTimeOut);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertPaymentsDT(string CreatedBy, string DocumentPath, string DocumentName, int BatchNo, DataTable dtPayments, out string Status)
        {
            try
            {
                bool isSuccess = false;
                Status = string.Empty;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@DocumentPath", SqlDbType.VarChar){ Value = DocumentPath },
                  new SqlParameter("@DocumentName", SqlDbType.VarChar){ Value = DocumentName },
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@TempCustomer", SqlDbType.Structured){ Value = dtPayments }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertBulkCustomerPayments_WithDT, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                        Status = dr["StatusText"].ToString();
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        public int InsertPaymentsDT_Temp(string CreatedBy, string DocumentPath, string DocumentName, int BatchNo, DataTable dtPayments)
        {
            try
            {
                int isSuccess = 0;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@DocumentPath", SqlDbType.VarChar){ Value = DocumentPath },
                  new SqlParameter("@DocumentName", SqlDbType.VarChar){ Value = DocumentName },
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@TempCustomer", SqlDbType.Structured){ Value = dtPayments }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertBulkCustomerPayments_WithDT_Temp, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToInt32(dr["IsSuccess"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        #endregion

        #region PaymentList
        public XmlDocument PaymentList(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEPAYMENTENTRYLIST, con); break;//Delete Payment
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Batch

        public XmlDocument DeleteBatch(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteBatchWithPayments, con); break;//Delete Batch
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        #endregion

        #region Customers

        public XmlDocument IsCustomerInBillProcessDAL(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustomerInBillProcess, con); break;//Is customer in billing process
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetCustomersInBillingProcess()
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetAccountNumbersInBillingProcess, con);//Is customer in billing process
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        #endregion

        #region Approval


        public XmlDocument GetApprovalsListDAL(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetApprovalsList, con);//Get all approval list.
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        #endregion

        #region EBill
        public XmlDocument GetEBillDetailsDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_UPS_GET_EBILL_PAYMENT_DETAILS, con);
            return xml;
        }

        public XmlDocument CheckEBillExecutionTimeDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_UPS_CheckEBillExecutionTime, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument InsertEBillPaymentDAL(XmlDocument MultiXmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Proc_USP_EBillPayment, MultiXmlDoc, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument InsertEBillMailLogsDAL(XmlDocument MultiXmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Proc_USP_InsertEBillMailLogs, MultiXmlDoc, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetEBillMailToSendDAL()//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Proc_USP_GetEBillMailsToSend, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument UpdateEBillMailsToSendDAL(XmlDocument MultiXmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Proc_USP_UpdateEBillMailsToSend, MultiXmlDoc, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetBuHeadOfficeMailsDAL()//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Proc_USP_GetBuHeadOfficeEmails, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Key Authentication
        public XmlDocument AuthenticateKey(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_USP_WS_AuthenticateKey, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion

        #region Estimation
        public XmlDocument GetEstimatedCustomersDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_USP_GetEstimationDetails, con);
            }
            catch
            {
            }
            return xml;
        }

        public XmlDocument InsertEstimationDAL(XmlDocument XmlDoc, XmlDocument MultiXml)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, MultiXml, Proc_USP_InsertEstimation, con);
            }
            catch
            {
            }
            return xml;
        }

        public XmlDocument GetEstimatedCustomersByCycleDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_USP_GetEstimationDetailsByCycle, con);
            }
            catch
            {
            }
            return xml;
        }

        public XmlDocument UpdateEstimationDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_USP_UpdateEstimation, con);
            }
            catch
            {
            }
            return xml;
        }

        public XmlDocument DeleteEstimationSettingDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Proc_USP_DeleteEstimationSetting, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetEstimatedDetails(XmlDocument XmlDoc, ReturnType value)//Suresh
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETESTIMATIONLIST_CYCLE, con, 1000);
                        break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETESTIMATEDCYCLES_SC, con);
                        break;
                }
            }
            catch
            {
            }
            return xml;
        }

        #endregion

        #region Spot Billing

        public XmlDocument GetSpotBillingInputsDAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetSpotBillingInputs, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertHHIputFileDetailsDAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertHHIputFileDetails, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument IsHHInputFileExistsDAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsHHInputFileExists, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetHHInputFilesListDAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetHHInputFilesList, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument DeleteHHInputFileDAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteHHInputFile, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        #endregion

        #region Autocomplete
        public XmlDocument AutoCompleteListDAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_AutoCompleteList, con, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        #endregion

        //#region Pole Management

        //public XmlDocument AddPole(XmlDocument XmlDoc)
        //{
        //    XmlDocument xml = null;
        //    try
        //    {
        //        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPOLEMASTERDETAILS, con);//Insert Pole
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return xml;
        //}


        //#endregion

        public XmlDocument GetMeterNoList(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetMeterNoList, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetAvailableMeterList(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAvailableMeterList, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetClusterCategoriesListToBind()//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(PROC_USP_GetClusterCategoriesListToBind, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetUserDefinedControlsList()//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(PROC_USP_GetUserDefinedControls, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument BindCycleByBU(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCyclesByBUSUSCWithBillingStatus, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument IsMeterExist(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsMeterAvailable, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument ChangeReadCustToDirect(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangeReadCustToDirect, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetDisconnectedMeterDetails(XmlDocument XmlDoc)//NEERAJ --ID077
        {
            try
            {
                return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetDisconnectedMeterDetials, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}