﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class MenuDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Stored_Procedures

        private string PROC_USP_GET_NLEVELMENU = "USP_Get_NLevelMenu";

        private string PROC_USP_GET_MENULIST = "USP_Get_MenuList";

        private string PROC_USP_INSERTMENU = "USP_InsertMenu";

        private string PROC_USP_UPDATEMENU = "USP_UpdateMenus";

        //Edit List Report Summary
        private string PROC_USP_GETEDITLISTREPORTSUMMARY = "USP_GetEditListReportSummary";//Karteek ID044

        #endregion

        #region Methods

        public XmlDocument Menu(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTMENU, con);
                        break;//Insert  
                    case Statement.Delete:
                        //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETELGA, con); 
                        break;//Delete Country OR Active 
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEMENU, con);
                        break;//
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Menu(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GET_NLEVELMENU, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument GetMenuList()
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GET_MENULIST, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument Get(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETEDITLISTREPORTSUMMARY, con);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        #endregion
    }
}
