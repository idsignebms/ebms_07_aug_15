﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class NewReportsDal
    {
        #region Members
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region SP_Reports

        private string PROC_USP_RptGetBillingStatusForCustomerType = "USP_RptGetBillingStatusForCustomerType_New";
        private string PROC_USP_RPTGETCUSTOMERSSTATISTICSINFO = "USP_RptGetCustomersStatisticsInfo_New";//1
        private string PROC_USP_RptGetBillingStatusForCustomerServiceCenter = "USP_RptGetBillingStatusForCustomerServiceCenter_New";
        private string PROC_USP_RptGetCustomersStatisticsAndTariff = "USP_RptGetCustomersStatisticsAndTariff_New";
        private string PROC_USP_RptGetCustomersStatisticsAndTariffClasses = "USP_RptGetCustomersStatisticsAndTariffClasses_New";
        private string PROC_USP_RptGetCustomersBillingStatisticsInfo = "USP_RptGetCustomersBillingStatisticsInfo_New";

        #endregion

        #region Methods

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet GetData(XmlDocument XmlDoc, ReportType value)
        {
            DataSet ds = new DataSet();
            try
            {
                switch (value)
                {

                    case ReportType.RptBillingStatus:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetBillingStatusForCustomerType, SqlCon); break;
                    case ReportType.RptBillingStatistics:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RPTGETCUSTOMERSSTATISTICSINFO, SqlCon); break;
                    case ReportType.RptBillingStatusForSCByBUCustomerType:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetBillingStatusForCustomerServiceCenter, SqlCon); break;
                    case ReportType.RptCustomerStatisticsByTariffClass:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetCustomersStatisticsAndTariff, SqlCon); break;
                    case ReportType.RptCustomerStatisticsByTariffClasswithDescription:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetCustomersStatisticsAndTariffClasses, SqlCon); break;
                    case ReportType.RptGetCustomersBillingStatisticsInfo:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetCustomersBillingStatisticsInfo, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        #endregion
    }
}
