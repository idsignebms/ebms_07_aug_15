﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class NotificationsDAL
    {
        #region Member
        private static string PROC_USP_InsertNotificationsDetails = "USP_InsertNotificationsDetails";
        private static string PROC_USP_GetNotificationList = "USP_GetNotificationList";
        private static string PROC_USP_DeleteNotice = "USP_DeleteNotice";
        private static string PROC_USP_GetNotifications = "USP_GetNotifications";
        private static string PROC_USP_DeleteLatestNews = "USP_DeleteLatestNews";
        private static string PROC_USP_GetNotificationsBYID = "USP_GetNotificationsBYID";
        private static string PROC_USP_DeleteNews = "USP_DeleteNews";
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _ObjiDsignDAL = new iDsignDAL();
        #endregion
        #region Method
        public XmlDocument GetNotificationListDAL(XmlDocument xmlNotificationDetails)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_GetNotificationList, con);
        }
        public XmlDocument InsertNotificationDAL(XmlDocument xmlNotificationDetails, Statement Act)
        {
            XmlDocument ResultXml = new XmlDocument();
            switch (Act)
            {
                case Statement.Insert:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_InsertNotificationsDetails, con); break;
            }
            return ResultXml;
        }
        public XmlDocument DeleteNotificationDAL(XmlDocument xmlNotificationDetails, Statement Act)
        {
            XmlDocument ResultXml = new XmlDocument();
            switch (Act)
            {
                case Statement.Delete:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_DeleteNotice, con); break;
            }
            return ResultXml;
        }
        public XmlDocument GetNotificationsDAL(XmlDocument xmlNotificationDetails)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_GetNotifications, con);
        }
        public XmlDocument DeleteLatestNewsnDAL(XmlDocument xmlNotificationDetails, Statement Act)
        {
            XmlDocument ResultXml = new XmlDocument();
            switch (Act)
            {
                case Statement.Delete:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_DeleteLatestNews, con); break;
            }
            return ResultXml;
        }
        public XmlDocument GetNotificationBYIDsDAL(XmlDocument xmlNotificationDetails)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_GetNotificationsBYID, con);
        }
        public XmlDocument DeleteNewsDAL(XmlDocument xmlNotificationDetails, Statement Act)
        {
            XmlDocument ResultXml = new XmlDocument();
            switch (Act)
            {
                case Statement.Delete:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlNotificationDetails, PROC_USP_DeleteNews, con); break;
            }
            return ResultXml;
        }
        #endregion        
    }
}
