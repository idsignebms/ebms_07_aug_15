﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using iDSignHelper;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class PaidMeterDal
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _objiDsignDAL = new iDsignDAL();
        #endregion

        #region Sps
        private static string PROC_USP_GETCUSTDETFORPAIDMETERS_BYACCNO = "USP_GetCustDetForPaidMeters_ByAccno";
        private static string PROC_USP_INSERTPAIDMETERADJUSTMENTS = "USP_InsertPaidMeterAdjustments";
        
        #endregion

        #region Methods
        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get: //Get Details Of Paid Meter Customers
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCUSTDETFORPAIDMETERS_BYACCNO, con); break;

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert: //Insert Details Of Adjustment of paid meters cstomers 
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPAIDMETERADJUSTMENTS, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
