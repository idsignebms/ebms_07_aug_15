﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class PasswordStrengthDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures
        //Get
        private string PROC_USP_GETPASSWORDSTRENGTH = "USP_GetPasswordStrength";//Faiz - ID103
        private string PROC_USP_UPDATEPASSWORDSTRENGTH = "USP_UpdatePasswordStrength";//Faiz - ID103
        #endregion

        public XmlDocument GetPasswordStrenth()
        {
            XmlDocument xml = null;
            xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETPASSWORDSTRENGTH, con);
            return xml;
        }

        public XmlDocument UpdatePasswordStrength(XmlDocument MultiXmlDoc)
        {
            XmlDocument xml = null;
            xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_UPDATEPASSWORDSTRENGTH, MultiXmlDoc, con);
            return xml;
        }
    }
}
