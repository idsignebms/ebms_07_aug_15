﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class PaymentsDal
    {

        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDAL = new CommonDAL();

        private static string PROC_USP_INSERTPAYMENTS_BYACCOUNTNO = "USP_INSERTPAYMENTS_BYACCOUNTNO";
        private static string PROC_USP_GetCustDetailsForPaymentEntry_AccountWise = "USP_GetCustDetailsForPaymentEntry_AccountWise";
        private static string PROC_USP_GetPaymentUploadBatches = "USP_GetPaymentUploadBatches";
        private static string PROC_USP_InsertPaymentUploadBatch = "USP_InsertPaymentUploadBatch";
        private static string PROC_USP_ClosePaymentUploadBatch = "USP_ClosePaymentUploadBatch";
        private static string PROC_USP_GetPaymentBatchProcesFailures = "USP_GetPaymentBatchProcesFailures";
        private static string PROC_USP_GetPaymentBatchProcesSuccess = "USP_GetPaymentBatchProcesSuccess";


        public XmlDocument Insert(XmlDocument Xml, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_INSERTPAYMENTS_BYACCOUNTNO, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetDetails(XmlDocument Xml, ReturnType Value)
        {
            XmlDocument xml = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetCustDetailsForPaymentEntry_AccountWise, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument PaymentUploadBatches(XmlDocument Xml, ReturnType Value)
        {
            XmlDocument xml = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetPaymentUploadBatches, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument PaymentUploadBatches(XmlDocument Xml, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_InsertPaymentUploadBatch, SqlCon);
                        break;
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ClosePaymentUploadBatch, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet PaymentBatchProcesFailures(XmlDocument Xml, ReturnType Value)
        {
            DataSet ds = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        ds = objCommonDAL.ExecuteReader_Get(Xml, PROC_USP_GetPaymentBatchProcesFailures, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet PaymentBatchProcesSuccess(XmlDocument Xml, ReturnType Value)
        {
            DataSet ds = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        ds = objCommonDAL.ExecuteReader_Get(Xml, PROC_USP_GetPaymentBatchProcesSuccess, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

    }
}
