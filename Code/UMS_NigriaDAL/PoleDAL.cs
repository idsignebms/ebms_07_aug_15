﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigriaDAL
{
    public class PoleDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Pole Stored_Procedures
        //INSERT
        public static string PROC_USP_INSERTPOLEMASTERDETAILS = "USP_InsertPoleMasterDetails";

        //INSERT
        public static string PROC_USP_INSERTPOLEDESCRIPTIONDETAILS = "USP_InsertPoleDescription";

        //Update PoleDescription
        public static string PROC_USP_UPDATEPOLEDESCRIPTION = "USP_UpdatePoleDescription";

        //Update PoleMaster
        public static string PROC_USP_UPDATEPOLEMASTER = "USP_UpdatePoleMaster";

        //Delete/Deactivate/Activate
        public static string PROC_USP_DELETEPOLEDESCRIPTION = "USP_DeletePoleDescription";

        //Delete/Deactivate/Activate
        public static string PROC_USP_DELETEPOLEMASTER = "USP_DeletePoleMaster";

        //GETPOLELIST
        public static string PROC_USP_GETPOLELIST = "USP_GetPoleList";

        public static string PROC_USP_GETPOLES = "USP_GetPoles";

        public static string PROC_USP_GETPOLEMASTERS = "USP_GetPoleMasters";

        //GETPARENTPOLELIST
        public static string PROC_USP_GETPARENTPOLELIST = "USP_GetParentPoleList";

        public string PROC_USP_PoleNumberSearch = "MASTERS.USP_PoleNumberSearch";

        public string PROC_USP_USP_GetPoleNumber = "MASTERS.USP_GetPoleNumber";

        public string PROC_USP_IsPoleNoExists = "MASTERS.USP_IsPoleNoExists";

        public string PROC_USP_IsCustomerExistsPoleInfo = "USP_IsCustomerExistsPoleInfo";
        public string PROC_USP_GetMaxPoleOrderId = "USP_GetMaxPoleOrderId";
        #endregion

        #region Pole Management Methods

        public XmlDocument AddPole(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPOLEMASTERDETAILS, con);//Insert Pole
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument UpdatePole(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEPOLEDESCRIPTION, con);// Delete/Activate  Pole
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument UpdatePoleMaster(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEPOLEMASTER, con);// Delete/Activate  Pole
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument DeletePole(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEPOLEDESCRIPTION, con);// Delete/Activate  Pole
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument DeletePoleMaster(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEPOLEMASTER, con);// Delete/Activate  Pole
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument AddPoleDescription(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTPOLEDESCRIPTIONDETAILS, con);//Insert Pole
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetPoles(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETPOLELIST, con); break;//Get Poles
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPOLES, con); break;//Get Poles
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetMasterPoles(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPOLEMASTERS, con); break;//Get Poles
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetParentPoles(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPARENTPOLELIST, con); break;//Get Parent Poles
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetPoleByOrderId(XmlDocument XmlDoc)//neeraJ id077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_GetPoleNumber, con); //Get Parent Poles
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public DataSet PoleNumberSearch()
        {
            return objCommonDal.ExecuteReader_Get(PROC_USP_PoleNumberSearch, con);
        }

        public XmlDocument IsPoleNoExists(XmlDocument XmlDoc)//neeraJ id077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsPoleNoExists, con); //IS POLE EXISTS
            }
            catch (Exception ex)
            {
            }
            return xml;
        }
        public XmlDocument IsCustomerExistsExists()//neeraJ id077
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_IsCustomerExistsPoleInfo, con); //IS POLE EXISTS
            }
            catch (Exception ex)
            {
            }
            return xml;
        }
        public XmlDocument GetPoleOrderId()
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetMaxPoleOrderId, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }
        #endregion
    }
}
