﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class ReadingsDal
    {
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDAL = new CommonDAL();

        private static string PROC_USP_GetReadingUploadBatches = "USP_GetReadingUploadBatches";
        private static string PROC_USP_InsertReadingUploadBatch = "USP_InsertReadingUploadBatch";
        private static string PROC_USP_CloseReadingUploadBatch = "USP_CloseReadingUploadBatch";
        private static string PROC_USP_GetReadingBatchProcesFailures = "USP_GetReadingBatchProcesFailures";
        private static string PROC_USP_GetReadingBatchProcessSuccess = "USP_GetReadingBatchProcessSuccess";

        private static string PROC_USP_GetCustomerAverageUploadBatches = "USP_GetCustomerAverageUploadBatches";
        private static string PROC_USP_InsertAverageUploadTransactionDetails = "USP_InsertAverageUploadTransactionDetails";
        private static string PROC_USP_CloseCustomerAverageUploadBatch = "USP_CloseCustomerAverageUploadBatch";
        private static string PROC_USP_GetCustomerAverageBatchProcesFailures = "USP_GetCustomerAverageBatchProcesFailures";
        private static string PROC_USP_GetAvgerageConsumptionBatchSuccessTransactions = "USP_GetAvgConsumptionSuccess";

        public XmlDocument ReadingUploadBatches(XmlDocument Xml, ReturnType Value)
        {
            XmlDocument xml = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetReadingUploadBatches, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument ReadingUploadBatches(XmlDocument Xml, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_InsertReadingUploadBatch, SqlCon);
                        break;
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_CloseReadingUploadBatch, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet ReadingBatchProcesFailures(XmlDocument Xml, ReturnType Value)
        {
            DataSet ds = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        ds = objCommonDAL.ExecuteReader_Get(Xml, PROC_USP_GetReadingBatchProcesFailures, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet ReadingBatchProcessSuccess(XmlDocument Xml, ReturnType Value)
        {
            DataSet ds = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        ds = objCommonDAL.ExecuteReader_Get(Xml, PROC_USP_GetReadingBatchProcessSuccess, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public XmlDocument AvgerageConsumptionUploadBatches(XmlDocument Xml, ReturnType Value)
        {
            XmlDocument xml = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetCustomerAverageUploadBatches, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument AvgerageConsumptionUploadBatches(XmlDocument Xml, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_InsertAverageUploadTransactionDetails, SqlCon);
                        break;
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_CloseCustomerAverageUploadBatch, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet AvgerageConsumptionBatchProcesFailures(XmlDocument Xml, ReturnType Value)
        {
            DataSet ds = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        ds = objCommonDAL.ExecuteReader_Get(Xml, PROC_USP_GetCustomerAverageBatchProcesFailures, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet AvgerageConsumptionBatchSuccessTransactions(XmlDocument Xml, ReturnType Value)
        {
            DataSet ds = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        ds = objCommonDAL.ExecuteReader_Get(Xml, PROC_USP_GetAvgerageConsumptionBatchSuccessTransactions, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
    }
}
