﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class RegionsDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures
        //INSERT
        private string PROC_USP_INSERTREGION = "USP_InsertRegion";//Faiz - ID103

        //GET
        private string PROC_USP_GETREGIONLIST = "USP_GetRegions";//Faiz - ID103
        private string PROC_USP_GETREGIONSLIST = "USP_GetRegionsList";//Bhimaraju - ID065 Dropdown

        //DELETE
        private string PROC_USP_DELETEREGOION = "USP_DeleteRegion";//Faiz - ID103

        //UPDATE
        private string PROC_USP_UPDATEREGION = "USP_UpdateRegion";//Faiz - ID103
        #endregion


        #region Methods

        public XmlDocument Region(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTREGION, con); break;//Insert Countries 
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEREGOION, con); break;//Delete Region OR Active Country
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEREGION, con); break;//Update Country
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Regions(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETREGIONLIST, con); break;//Get Regions  //--Faiz - ID103
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETREGIONSLIST, con); break;//Get RegionsList DropDown  //--Bhimaraju - ID065

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
