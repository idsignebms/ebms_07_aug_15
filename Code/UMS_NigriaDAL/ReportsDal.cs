﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using UMS_NigeriaBE;

namespace UMS_NigriaDAL
{
    public class ReportsDal
    {
        #region Members
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDal = new CommonDAL();
        int ConnectionTimeOut = 300;
        #endregion

        #region SP_Reports

        //private string PROC_USP_RptGetAccountsWithDebitBal = "USP_RptGetAccountsWithDebitBal";
        private string PROC_USP_RptGetAccountsWithDebitBal = "USP_RptGetAccountsWithDebitBal_New";//Karteek 
        private string PROC_USP_RptGetAccountsWithDebitBal_New_Rajaiah = "USP_RptGetAccountsWithDebitBal_New_Rajaiah";//Karteek        
        //private string PROC_USP_RptGetAccountsStatusOfBill = "USP_RptGetAccountsStatusOfBill";
        private string PROC_USP_RptGetAccountsStatusOfBill = "USP_RptGetAccountsStatusOfBill_New";//Karteek        
        //private string PROC_USP_RptGetPaymets = "USP_RptGetPaymets";
        private string PROC_USP_RptGetPaymets = "USP_RptGetPaymets_New";//Karteek   
        private static string PROC_USP_GetAllAccountNo = "USP_GetAllAccountNo";
        //private static string PROC_USP_RptGetAccountsWithCreditBal = "USP_RptGetAccountsWithCreditBal";
        private static string PROC_USP_RptGetAccountsWithCreditBal = "USP_RptGetAccountsWithCreditBal_New";//Karteek
        private static string PROC_USP_RptGetAccountsWithCreditBal_New_Rajaiah = "USP_RptGetAccountsWithCreditBal_New_Rajaiah";//Karteek
        private static string PROC_USP_GetBookNoList_By_Cycle = "USP_GetBookNoList_By_Cycle";
        private static string PROC_USP_RptGetCustomersNoNameOrAddList = "USP_RptGetCustomersNoNameOrAddList";
        private static string PROC_USP_RptGetCheckMetersList = "USP_RptGetCheckMetersList";
        private static string PROC_USP_RPTGETCUSTOMERLEDGERS = "USP_RptGetCustomerLedgers";
        private static string PROC_USP_RPTGETCUSTOMERBILLDETAILS = "USP_RptGetCustomerBillDetails";
        //private string PROC_USP_GetBookNoList_By_Cycle_MeterReadin = "USP_GetBookNoList_By_Cycle_MeterReadin";
        private string PROC_USP_GetBookNoList_By_Cycle_MeterReadin = "USP_GetBookNoList_By_Cycle_MeterReading";//Karteek        
        private string PROC_USP_GetBookNoList_By_Cycle_BookwiseExport = "USP_GetBookNoList_By_Cycle_BookwiseExport";//Karteek        
        private string PROC_USP_GetBookNoList_By_Cycle_NotDisabled = "USP_GetBookNoList_By_Cycle_NotDisabled";//Karteek        

        private static string PROC_USP_GetAssignMetersLog = "USP_GetAssignMetersLog";
        private static string PROC_USP_GetAssignMetersWithLimit = "USP_GetAssignMetersWithLimit";
        private static string PROC_USP_GetReadToDirectLogs = "USP_GetReadToDirectLogs";
        private static string PROC_USP_GetReadToDirectWithLimit = "USP_GetReadToDirectWithLimit";
        private static string PROC_USP_GetTariffChangeLogsWithLimit = "USP_GetTariffChangeLogsWithLimit";
        private static string PROC_USP_GetBookNoChangeLogsWithLimit = "USP_GetBookNoChangeLogsWithLimit";
        private static string PROC_USP_GetBookNoChangeLogs = "USP_GetBookNoChangeLogs";
        private static string PROC_USP_GetTariffChangeLogs = "USP_GetTariffChangeLogs";
        private static string PROC_USP_GetCustomerAddressChangeLogs = "USP_GetCustomerAddressChangeLogs";
        private static string PROC_USP_GetCustomerAddressChangeLogsWithLimit = "USP_GetCustomerAddressChangeLogsWithLimit";
        private static string PROC_USP_GetCustomerNameChangeLogs = "USP_GetCustomerNameChangeLogs";
        private static string PROC_USP_GetCustomerNameChangeLogsWithLimit = "USP_GetCustomerNameChangeLogsWithLimit";
        private static string PROC_USP_GetCustomerMeterChangeLogs = "USP_GetCustomerMeterChangeLogs";
        private static string PROC_USP_GetCustomerMeterChangeLogsWithLimit = "USP_GetCustomerMeterChangeLogsWithLimit";
        private static string PROC_USP_GetCustomerChangeLogs = "USP_GetCustomerChangeLogs";
        private static string PROC_USP_GetCustomerChangeLogsWithLimit = "USP_GetCustomerChangeLogsWithLimit";

        //Prebilling Procedures
        private static string PROC_USP_RptPreBillingReadCustomer = "USP_RptPreBillingReadCustomer";
        private static string PROC_USP_RptPreBillingDirectCustomer = "USP_RptPreBillingDirectCustomer";
        private static string PROC_USP_RptPreBillingNonReadCustomer = "USP_RptPreBillingNonReadCustomer";



        private static string PROC_USP_RPTGETPREBILLINGCUSTOMERS = "USP_RPTGETPREBILLINGCUSTOMERS";
        private static string PROC_USP_GETOPENMONTHDETAILS = "USP_GETOPENMONTHDETAILS";
        private static string PROC_USP_RPTGETHIGHLOWREADING_CUSTOMERS = "USP_RPTGETHIGHLOWREADING_CUSTOMERS";
        private static string PROC_USP_RPTHIGHESTIMATION_CUSTOMERS = "USP_RPTHIGHESTIMATION_CUSTOMERS";

        private string PROC_IsCustExistsByBU = "IsCustExistsByBU";
        private string PROC_USP_IsCustExistsByBU_NEW = "USP_IsCustExistsByBU_NEW";
        private string PROC_USP_IsCustExistsByBUAndStatus = "USP_IsCustExistsByBUAndStatus_MeterReadings";

        private string PROC_USP_GetCustomersExportByBookWise = "USP_GetCustomersExportByBookWise";
        private string PROC_USP_RptAgedCustomerAccount_Receivable = "USP_RptAgedCustomerAccount_Receivable";
        private string PROC_USP_RptGetNewCustomersReport = "USP_RptGetNewCustomersReport";
        private string PROC_USP_RptGetNoUsageCustomersReport = "USP_RptGetNoUsageCustomersReport";
        private string PROC_USP_RptCustWithNoFxdChargesReport = "USP_RptCustWithNoFxdChargesReport";
        private string PROC_USP_RptGetUnBilledCustomers = "USP_RptGetUnBilledCustomers";
        private string PROC_USP_MasterSearch = "USP_MasterSearch";

        private string PROC_USP_CUSTOMERSTATISTICSBYTARIFF = "USP_CustomerStatisticsByTariff_New";

        private string PROC_USP_GetLastCloseMonth = "USP_GetLastCloseMonth";
        private string PROC_USP_GetCustomerTypeChangeLogs = "USP_GetCustomerTypeChangeLogs";
        private string PROC_USP_GetCustomerTypeChangeWithLimit = "USP_GetCustomerTypeChangeWithLimit";
        private string PROC_USP_GetCustomerPresentMeterReadingChangeLogs = "USP_GetCustomerPresentMeterReadingChangeLogs";//Bhimaraju
        private string PROC_USP_GetCustomerPresentMeterReadingChangeWithLimit = "USP_GetCustomerPresentMeterReadingChangeWithLimit";//Bhimaraju
        

        //Edit List Report
        private string PROC_USP_GETMETERREADINGSFROMLIST = "USP_GetMeterReadingsFromList";//Added By Karteek ID044
        private string PROC_USP_GETMETERREADINGSREPORT = "USP_GetMeterReadingsReport";//Added By Karteek ID044
        private string PROC_USP_GETAVERAGEUPLOADREPORT = "USP_GetAverageUploadReport";//Added By Karteek ID044
        private string PROC_USP_GETPAYMENTEDITLISTREPORT = "USP_GetPaymentEditListReport";//Added By Karteek ID044
        private string PROC_USP_GETADJUSTMENTTYPES = "USP_GetAdjustmentTypes";//Added By Karteek ID044
        private string PROC_USP_GETADJUSTMENTEDITLISTREPORT = "USP_GetAdjustmentEditListReport";//Added By Karteek ID044
        private string PROC_USP_GETPAYMENTTYPES = "USP_GetPaymentTypes";//Added By Karteek ID044
        private string PROC_USP_GETBATCHLISTEDITLISTREPORT = "USP_GetBatchListEditListReport";//Added By Karteek ID044
        private string PROC_USP_GETPAYMENTEDITLISTREPORT_BYBATCHNO = "USP_GetPaymentEditListReport_ByBatchNo";//Added By Karteek ID044
        private string PROC_USP_GETBATCHDETAILS_BYBATCHNO = "USP_GetBatchDetails_ByBatchNo";//Added By Karteek ID044
        private string PROC_USP_IsCustomerExistsInBU_Billing = "USP_IsCustomerExistsInBU_Billing";//NEERAJ KANOJIYA ID077
        private string PROC_USP_IsCustomerExistsInBU_AdjustmentNoBill = "USP_IsCustomerExistsInBU_AdjustmentNoBill";//NEERAJ KANOJIYA ID077
        private string PROC_USP_IsCustomerExistsInBU_Billing_INDV = "USP_IsCustomerExistsInBU_Billing_INDV";//NEERAJ KANOJIYA ID077

        private string PROC_USP_IsCustomerExistsInBU_ViewCustomer = "USP_IsCustomerExistsInBU_ViewCustomer";//Faiz-ID103

        #region PreBillingReports
        private string PROC_USP_RptPreBilling_GetReadCustomers = "USP_RptPreBilling_GetReadCustomers";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetNonReadCustomers = "USP_RptPreBilling_GetNonReadCustomers";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetZeroUsageCustomers = "USP_RptPreBilling_GetZeroUsageCustomers";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetNoBillsCustomers = "USP_RptPreBilling_GetNoBillsCustomers";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetHighLowEstimatedCustomers = "USP_RptPreBilling_GetHighLowEstimatedCustomers";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetPartialBillCustomers = "USP_RptPreBilling_GetPartialBillCustomers";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetAvgNotUploadedCustoemrs = "USP_RptPreBilling_GetAvgNotUploadedCustoemrs";//Added By Bhimaraju ID065
        private string PROC_USP_RptPreBilling_GetAvgUploadedCustoemrs = "USP_RptPreBilling_GetAvgUploadedCustoemrs";//Added By Bhimaraju ID065


        #endregion

        private string PROC_USP_GetCustDetForBookNoChangeByAccno_ByCheck = "USP_GetCustDetForBookNoChangeByAccno_ByCheck";//NEERAJ KANOJIYA ID077
        private string PROC_USP_GetCusotmerReading = "USP_GetCustomerReadings";//Faiz-ID103
        private string PROC_USP_GetApprovalRegistrationsAuditRay = "CUSTOMERS.USP_GetApprovalRegistrationsAuditRay";
        #endregion

        #region Methods

        public XmlDocument Get(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETMETERREADINGSFROMLIST, SqlCon); break;
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETADJUSTMENTTYPES, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETPAYMENTTYPES, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptGetAccountsWithDebitBal, SqlCon, 300); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptGetAccountsStatusOfBill, SqlCon, 300); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAllAccountNo, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptGetPaymets, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptGetAccountsWithCreditBal, SqlCon); break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoList_By_Cycle, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptGetCustomersNoNameOrAddList, SqlCon); break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerMeterChangeLogsWithLimit, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerChangeLogsWithLimit, SqlCon); break;
                    case ReturnType.User:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETMETERREADINGSREPORT, SqlCon, 300); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETAVERAGEUPLOADREPORT, SqlCon, 300); break;
                    case ReturnType.Data:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPAYMENTEDITLISTREPORT, SqlCon, 300); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBATCHLISTEDITLISTREPORT, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetBooksForBookwiseMeterReading(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoList_By_Cycle_MeterReadin, SqlCon);
                //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoList_By_Cycle, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetBooksForBookwiseCustomerExport(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoList_By_Cycle_BookwiseExport, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetNotDisabledBooks(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoList_By_Cycle_NotDisabled, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBATCHDETAILS_BYBATCHNO, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetList(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptGetCheckMetersList, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoChangeLogsWithLimit, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetTariffChangeLogsWithLimit, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetReadToDirectWithLimit, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAssignMetersWithLimit, SqlCon); break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerNameChangeLogsWithLimit, SqlCon); break;
                    case ReturnType.User:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerTypeChangeWithLimit, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETADJUSTMENTEDITLISTREPORT, SqlCon, 300); break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerPresentMeterReadingChangeWithLimit, SqlCon, 300); break;
                  

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public DataSet GetLogsList(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet dt = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Bulk:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustomerChangeLogs, SqlCon); break;//ActiveStatus Log
                    case ReturnType.Group:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetBookNoChangeLogs, SqlCon); break;//BookNo Logs
                    case ReturnType.Multiple:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetTariffChangeLogs, SqlCon); break;//Tariff Logs
                    case ReturnType.Single:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustomerNameChangeLogs, SqlCon); break;//Name Change Log
                    case ReturnType.Double:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustomerMeterChangeLogs, SqlCon); break;//Meter Change Log
                    case ReturnType.Data:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustomerTypeChangeLogs, SqlCon); break; // Customer Type Change Log
                    case ReturnType.Fetch:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetReadToDirectLogs, SqlCon); break; // ReadToDirect Change Log
                    case ReturnType.Get:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetAssignMetersLog, SqlCon); break; // AssignedMeter Change Log
                    case ReturnType.List:
                        dt = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustomerPresentMeterReadingChangeLogs, SqlCon); break; // Present Meter Reading Change Log
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return dt;

        }

        public XmlDocument GetAddressList(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerAddressChangeLogs, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerAddressChangeLogsWithLimit, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public XmlDocument GetLedger(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTGETCUSTOMERLEDGERS, SqlCon); break;
                    case ReturnType.Single:
                        //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_IsCustExistsByBU, SqlCon, Constants.CommandTimeOut); break;
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustExistsByBU_NEW, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, string.Empty, SqlCon); break;
                    case ReturnType.User:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTGETCUSTOMERBILLDETAILS, SqlCon); break;
                    case ReturnType.Check://Customer Bill Gen.
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustomerExistsInBU_Billing, SqlCon); break;
                    case ReturnType.Multiple://Adjustment No Bill
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustomerExistsInBU_AdjustmentNoBill, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustExistsByBUAndStatus, SqlCon); break;
                    case ReturnType.Retrieve://For View Customer Details
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustomerExistsInBU_ViewCustomer, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public XmlDocument GetPreBillingDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTGETPREBILLINGCUSTOMERS, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETOPENMONTHDETAILS, SqlCon); break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTGETHIGHLOWREADING_CUSTOMERS, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTHIGHESTIMATION_CUSTOMERS, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptPreBillingReadCustomer, SqlCon, Constants.CommandTimeOut);
                        break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptPreBillingDirectCustomer, SqlCon, Constants.CommandTimeOut);
                        break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RptPreBillingNonReadCustomer, SqlCon, Constants.CommandTimeOut);
                        break;


                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public DataSet GetPreBillingReports(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetReadCustomers, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Bulk:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetNonReadCustomers, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Check:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetZeroUsageCustomers, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Data:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetNoBillsCustomers, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Double:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetHighLowEstimatedCustomers, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Fetch:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetPartialBillCustomers, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.Group:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetAvgNotUploadedCustoemrs, SqlCon, Constants.CommandTimeOut); break;
                    case ReturnType.List:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptPreBilling_GetAvgUploadedCustoemrs, SqlCon, Constants.CommandTimeOut); break;


                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;

        }

        public DataSet GetCustomersExportByBookWise(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetCustomersExportByBookWise, SqlCon, ConnectionTimeOut);
        }

        public DataSet GetReport(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetAccountsStatusOfBill, SqlCon, ConnectionTimeOut); break;
                    case ReturnType.Bulk:
                        //ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetAccountsWithDebitBal, SqlCon, ConnectionTimeOut); break;
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetAccountsWithDebitBal_New_Rajaiah, SqlCon, ConnectionTimeOut); break;
                    case ReturnType.Group:
                        //ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetAccountsWithCreditBal, SqlCon, ConnectionTimeOut); break;
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetAccountsWithCreditBal_New_Rajaiah, SqlCon, ConnectionTimeOut); break;
                    case ReturnType.List:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetPaymets, SqlCon, ConnectionTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet GetAgedCustsList(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptAgedCustomerAccount_Receivable, SqlCon, ConnectionTimeOut);
        }

        public DataSet GetNewCustomersReport(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetNewCustomersReport, SqlCon, ConnectionTimeOut);
        }

        public DataSet GetNoUsageCustomersReport(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetNoUsageCustomersReport, SqlCon, ConnectionTimeOut);
        }

        public DataSet GetCustWithNoFxdChargesReportDAL(XmlDocument XmlDoc)//NEERAJ-ID077
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptCustWithNoFxdChargesReport, SqlCon, ConnectionTimeOut);
        }

        public DataSet GetUnBilledCustomersReport(XmlDocument XmlDoc)//NEERAJ-ID077
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_RptGetUnBilledCustomers, SqlCon, ConnectionTimeOut);
        }

        public DataSet SearchMasterDAL(XmlDocument XmlDoc)//NEERAJ-ID077
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_MasterSearch, SqlCon, ConnectionTimeOut);
        }

        public DataSet GetBatchWisePaymentsList(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETPAYMENTEDITLISTREPORT_BYBATCHNO, SqlCon, ConnectionTimeOut);
        }

        public DataSet RptCustStatisticsByTariff(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_CUSTOMERSTATISTICSBYTARIFF, SqlCon, ConnectionTimeOut); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet GetLastCloseMonth()
        {
            DataSet ds = null;
            try
            {

                ds = objCommonDal.ExecuteReader_Get(PROC_USP_GetLastCloseMonth, SqlCon, ConnectionTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public DataSet GetApprovalRegistrationsAuditRay(XmlDocument XmlDoc)
        {
            return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetApprovalRegistrationsAuditRay, SqlCon, ConnectionTimeOut);
        }
        public XmlDocument IsCustomerExistsInBU_Billing_INDV(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsCustomerExistsInBU_Billing_INDV, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerDetails_ByCheck(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustDetForBookNoChangeByAccno_ByCheck, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCusotmerReading(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCusotmerReading, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        #endregion
    }
}
