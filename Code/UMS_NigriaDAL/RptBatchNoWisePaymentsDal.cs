﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Xml;
using System.Configuration;

namespace UMS_NigriaDAL
{
    public class RptBatchNoWisePaymentsDal
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _objiDsignDAL = new iDsignDAL();
        #endregion

        #region Sps
        private static string PROC_USP_GETBATCHNO_BYOPENMONTH = "USP_GetBatchNo_ByOpenMonth";
        private static string PROC_USP_RPTGETBATCHWISEPAYMENTS = "USP_RptGetBatchWisePayments";
        private static string PROC_USP_GETADJUSTEDBATCHNO_BYOPENMONTH = "USP_GetAdjustedBatchNo_ByOpenMonth";
        private static string PROC_USP_RPTGETBATCHWISEBILLADJUSTMENTS = "USP_RptGetBatchWiseBillAdjustments";
        

        #endregion

        #region Methods
        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get: //Get Details OfBatchNos On Open Month Id basis
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBATCHNO_BYOPENMONTH, con); break;
                    case ReturnType.List: //Get BookNo Wise Payments List
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTGETBATCHWISEPAYMENTS, con); break;
                    case ReturnType.Fetch: //Get Details OfBatchNos On Open Month Id basis
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETADJUSTEDBATCHNO_BYOPENMONTH, con); break;
                    case ReturnType.Bulk: //Get Details OfBatchNos On Open Month Id basis
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_RPTGETBATCHWISEBILLADJUSTMENTS, con); break;

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert: //Insert Details Of Customer For BookNo Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, String.Empty, con); break;

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
