﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace UMS_NigriaDAL
{
   public class SecurityDongle
    {
        // Specify the COPYLOCK DLL to be used based on application mode (Single User or Multi-user)
        // you may also rename original default DLL file name and use this name here.
        const String dll_COPYLOCK = "CL32.DLL"; //Standalone application
        //        const String dll_COPYLOCK = "NETCL32.DLL"; //Multiuser application
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_login(string name);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_get_model([MarshalAs(UnmanagedType.LPArray)] byte[] dModel);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_lock_ok();
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_get_id([MarshalAs(UnmanagedType.LPArray)] byte[] dID, String rPass);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_get_batch([MarshalAs(UnmanagedType.LPArray)] byte[] dBatch, String rPass);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_set_sign(String dSign, String wPass);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_get_sign([MarshalAs(UnmanagedType.LPArray)] byte[] dSign, String rPass);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_set_osign(String dSign, String wPass);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_get_osign([MarshalAs(UnmanagedType.LPArray)] byte[] dSign, String rPass);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_write_block(String dBuf, String wPass, int BlockNo, int StPos, int Count);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_read_block([MarshalAs(UnmanagedType.LPArray)] byte[] dBuf, String rPass, int BlockNo, int StPos, int Count);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_write_word(int dWord, String wPass, int BlockNo, int StPos);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_read_word(ref int dWord, String rPass, int BlockNo, int StPos);
        [DllImport(dll_COPYLOCK)]
        public static extern int cl_logout();

        public string DongleMessage
        {
            get;
            set;
        }

       public SecurityDongle()
       {
           int dResult = cl_login("052740006750090128050201");
           DisplayResult(dResult, "");
        
       }
       private void DisplayResult(int dResult, string data)
       {
           switch (dResult)
           {
               case 1:
                   if (data == "")
                   {
                       DongleMessage = "Success";
                   }
                   else
                   {
                       DongleMessage = "Success" + ";   Dongle Data = " + data;
                   }
                   break;

               case -1:
                   DongleMessage = "Error#-1: Lock is missing";
                   break;

               case -2:
                   DongleMessage = "Error#-2: Driver load Error";
                   break;

               case -3:
                   DongleMessage = "Error#-3: Incorrect password";
                   break;

               case -4:
                   DongleMessage = "Error#-4: Fascility N.A.";
                   break;

               case -5:
                   DongleMessage = "Error#-5: Write failed";
                   break;

               case -6:
                   DongleMessage = "Error#-6: Login users limit is over";
                   break;

               case -7:
                   DongleMessage = "Error#-7: Link to the server is not active";
                   break;

               case -9:
                   DongleMessage = "Error#-9: Cannot establish connection with server";
                   break;

               case -12:
                   DongleMessage = "Error#-12: Time period expired";
                   break;

               default:

                   DongleMessage = "Error#nn: Internal Error";
                   break;
           }
       }
    }
}
