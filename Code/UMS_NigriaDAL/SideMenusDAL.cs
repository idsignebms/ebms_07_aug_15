﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;

namespace UMS_NigriaDAL
{
    public class SideMenusDAL
    {
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _ObjiDsignDAL = new iDsignDAL();

        #region ProcedureNames

        private static string PROC_USP_GETSIDEMENUS = "USP_GetSideMenus";
        private string PROC_USP_GETNEWMENUS = "USP_GetMenus";
        private string PROC_USP_GetMenusNW = "USP_GetMenusNW";
        private string PROC_USP_GetConfigurationSettingsMenu = "USP_GetConfigurationSettingsMenu";
        
        #endregion

        public XmlDocument GetMenus(XmlDocument xmlDoc)
        {
            try
            {
                return _ObjiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETSIDEMENUS, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetNewMenusDAL(XmlDocument xmlDoc)
        {
            try
            {
                return _ObjiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GETNEWMENUS, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetNewMenusNewDAL(XmlDocument xmlDoc)
        {
            try
            {
                return _ObjiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetMenusNW, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetConfigurationSettingsMenu(XmlDocument xmlDoc)
        {
            try
            {
                return _ObjiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetConfigurationSettingsMenu, con);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
