﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class SpotBillingOutputDal
    {
        iDsignDAL _objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);

        #region Procedure Names

        private static string PROC_USP_CHECKSPOTBILLINGOUTPUTS = "USP_CHECKSPOTBILLINGOUTPUTS";
        private static string PROC_USP_INSERTSPOTBILL_CHECKMETERDETAILS = "USP_INSERTSPOTBILL_CHECKMETERDETAILS";

        #endregion

        #region Methods

        public XmlDocument GetDetails(XmlDocument Xml, ReturnType value)
        {
            XmlDocument xml = null;
            switch (value)
            {
                case ReturnType.Get:
                    xml = _objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_CHECKSPOTBILLINGOUTPUTS, SqlCon);
                    break;
            }
            return xml;
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = new XmlDocument();

            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTSPOTBILL_CHECKMETERDETAILS, SqlCon);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
