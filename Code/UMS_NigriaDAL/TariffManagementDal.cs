﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace UMS_NigriaDAL
{
    public class TariffManagementDal
    {
        #region Members
        iDsignDAL objiDsignDal = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        #endregion

        #region Procedures
        //Get
        private static string PROC_USP_GetCharges = "USP_GetCharges";
        private static string PROC_USP_GetAdditionalClassCharges = "USP_GetAdditionalClassCharges";
        private static string PROC_USP_GetTariffEnergyClassYears = "USP_GetTariffEnergyClassYears";
        private static string PROC_USP_GetEnergyClassChargesList = "USP_GetEnergyClassChargesList";
        private static string PROC_USP_GetTariffAdditionalFixedChargesYear = "USP_GetTariffAdditionalFixedChargesYear";
        private static string PROC_USP_GetYearAndMonthForTariffEntry = "USP_GetYearAndMonthForTariffEntry";
        private static string PROC_USP_GetYearAndMonthForTariffFixed = "USP_GetYearAndMonthForTariffFixed";
        private static string PROC_USP_GetTariffCategoryList = "USP_GetTariffCategoryList";
        private static string PROC_USP_GetTariffSubCategoryList = "USP_GetTariffSubCategoryList";
        private static string PROC_USP_GetCustomerDetailsList = "USP_GetCustomerDetailsList";
        private static string PROC_USP_CheckTariffApprovalStatus = "USP_CheckTariffApprovalStatus";
        private static string PROC_USP_GetTariffApprovalList = "USP_GetTariffApprovalList";
        private static string PROC_USP_GetFixedChargesList = "USP_GetFixedChargesList";
        
        //Insert
        private static string PROC_USP_InsertTariffEntry = "USP_InsertTariffEntry";
        private static string PROC_USP_InsertTariffFixed = "USP_InsertTariffFixed";
        private static string PROC_USP_InsertTariffCategory = "USP_InsertTariffCategory";
        private static string PROC_USP_InsertTariffSubCategory = "USP_InsertTariffSubCategory";
        private static string PROC_USP_InsertFixedCharges = "USP_InsertFixedCharges";
        
        //Update
        private static string PROC_USP_UpdateAdditionalClassCharges = "USP_UpdateAdditionalClassCharges";
        private static string PROC_USP_UpdateTariffEntry = "USP_UpdateTariffEntry";
        private static string PROC_USP_UpdateTariffCategory = "USP_UpdateTariffCategory";
        private static string PROC_USP_UpdateTariffSubCategory = "USP_UpdateTariffSubCategory";
        //private static string PROC_USP_UpdateTariffDetails = "USP_UpdateTariffDetails";
        private static string PROC_USP_UpdateTariffDetails = "USP_UpdateTariffDetails_New";//Karteek        
        private static string PROC_USP_UpdateFixedCharge = "USP_UpdateFixedCharge";  

        private static string PROC_USP_GetTariffChangeLogsToApprove = "USP_GetTariffChangeLogsToApprove";//Karteek        
        
        //Active or Deactive
        private static string PROC_Usp_DeleteAdditionalClassCharges = "Usp_DeleteAdditionalClassCharges";
        private static string PROC_USP_DeleteTariffEntry = "USP_DeleteTariffEntry";
        private static string PROC_USP_UpdateTariffCategoryActiveStatus = "USP_UpdateTariffCategoryActiveStatus";
        #endregion

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetCharges, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetAdditionalClassCharges, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetEnergyClassChargesList, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetYearAndMonthForTariffEntry, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetYearAndMonthForTariffFixed, SqlCon); break;
                    case ReturnType.Double:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerDetailsList, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_CheckTariffApprovalStatus, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetTariffApprovalList, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetFixedChargesList, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet GetTariffApprovalList(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetTariffApprovalList, SqlCon);
            return ds;
        }

        public DataSet GetTariffChangesApproveList(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetTariffChangeLogsToApprove, SqlCon);
            return ds;
        }

        public XmlDocument Get(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDal.ExecuteXmlReader(PROC_USP_GetTariffEnergyClassYears, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDal.ExecuteXmlReader(PROC_USP_GetTariffAdditionalFixedChargesYear, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_InsertTariffFixed, SqlCon); break;
                    case Statement.Delete:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_Usp_DeleteAdditionalClassCharges, SqlCon); break;
                    case Statement.Check:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_InsertTariffEntry, SqlCon); break;
                    case Statement.Update:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateAdditionalClassCharges, SqlCon); break;
                    case Statement.Modify:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTariffEntry, SqlCon); break;
                    case Statement.Edit:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteTariffEntry, SqlCon); break;
                    case Statement.Change:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTariffDetails, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertTariff(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_InsertTariffCategory, SqlCon); break;
                    case Statement.Update:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTariffCategory, SqlCon); break;
                    case Statement.Delete:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTariffCategoryActiveStatus, SqlCon); break;
                    case Statement.Generate:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_InsertTariffSubCategory, SqlCon); break;
                    case Statement.Modify:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateTariffSubCategory, SqlCon); break;
                    case Statement.Change:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateFixedCharge, SqlCon); break;
                    case Statement.Edit:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_InsertFixedCharges, SqlCon); break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetTariff(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetTariffCategoryList, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDal.ExecuteXmlReader(XmlDoc, PROC_USP_GetTariffSubCategoryList, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
    }
}
