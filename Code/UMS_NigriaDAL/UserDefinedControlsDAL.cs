﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class UserDefinedControlsDAL
    {
        #region Members

        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();

        #endregion

        #region Stored Procedures

        public static string PROC_USP_ISCUSTOMERASSOCIATED = "USP_IsCustomerAssociated";

        public static string PROC_USP_GETCONTROLREFERENCES = "USP_GetControlReferences";
        public static string PROC_USP_INSERTCONTROLREFERENCES = "USP_InsertControlReferences";
        public static string PROC_USP_UPDATECONTROLREFERENCES = "USP_UpdateControlReferences";
        public static string PROC_USP_DELETECONTROLREFERENCE = "USP_DeleteControlReference";


        public static string PROC_USP_GETUSERDEFINEDCONTROLS = "USP_GetUserDefinedControls";
        public static string PROC_USP_GETUSERDEFINEDCONTROLSDETAILS = "USP_GetUserDefinedControlsDetails";
        public static string PROC_USP_INSERTUSERDEFINEDCONTROLSDETAILS = "USP_InsertUserDefinedControlsDetails";
        public static string PROC_USP_UPDATEUSERDEFINEDCONTROLSDETAILS = "USP_UpdateUserDefinedControlsDetails";
        public static string PROC_USP_DELETEUSERDEFINEDCONTROLSDETAILS = "USP_DeleteUserDefinedControlsDetails";

        #endregion

        public XmlDocument GetUserDefinedControlItems(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCONTROLREFERENCES, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument IsCustomerAssociated(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ISCUSTOMERASSOCIATED, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument GetUserDefinedControlsList()
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETUSERDEFINEDCONTROLS, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument GetUserDefinedControlsDetails(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc,PROC_USP_GETUSERDEFINEDCONTROLSDETAILS, con);
            }
            catch (Exception ex)
            {
            }
            return xml;
        }

        public XmlDocument UserDefinedControlsItems(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTCONTROLREFERENCES, con); break; //Insert
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATECONTROLREFERENCES, con); break; //Insert
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETECONTROLREFERENCE, con); break; //Insert

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UserDefinedControlsDetails(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTUSERDEFINEDCONTROLSDETAILS, con); break; //Insert
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEUSERDEFINEDCONTROLSDETAILS, con); break; //Insert
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEUSERDEFINEDCONTROLSDETAILS, con); break; //Insert

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
