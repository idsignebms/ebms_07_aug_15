﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class UserManagementDAL
    {
        #region Members
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        #endregion

        #region Procedures
        //Get
        private static string PROC_USP_GetGender = "USP_GetGender";
        private static string PROC_USP_GetRoles = "USP_GetRoles";
        private static string PROC_USP_GetRolesByBU = "USP_GetRolesByBU";
        private static string PROC_USP_GetDesignations = "USP_GetDesignations";
        private static string PROC_USP_GetUsersList = "USP_GetUsersList";
        private static string PROC_USP_GetUserPermissionsByRoleId = "USP_GetUserPermissionsByRoleId";
        private static string PROC_USP_GetUserDocuments = "USP_GetUserDocuments";
        private static string PROC_USP_GETUSERDETAILSBYUSERID = "USP_GetUserDetailsByUserId";
        private static string PROC_USP_ChangeUserDetails = "USP_ChangeUserDetails";
        private static string PROC_USP_GetUserDetails = "USP_GetUserDetails";
        
        //Insert
        private static string PROC_USP_InsertUserDetails = "USP_InsertUserDetails";
        //Update
        private static string PROC_USP_UpdateUserDetails = "USP_UpdateUserDetails";
        private static string PROC_USP_ResetUserPassword = "USP_ResetUserPassword";
        private static string PROC_USP_ChangePassword = "USP_ChangePassword";
        private static string PROC_USP_DeletePhoto = "USP_DeletePhoto";

        //Active or Deactive
        private static string PROC_USP_UpdateUserActiveStatus = "USP_UpdateUserActiveStatus";
        private static string PROC_USP_DeleteUserDocuments = "USP_DeleteUserDocuments";

        private static string PROC_USP_DisableUser = "USP_DisableUser"; //Neeraj-ID77

        //Meter Reading Report
        private static string PROC_USP_GETUSERSTOMETERREADINGREPORT = "USP_GetUsersToMeterReadingReport";//Karteek-ID044
        private static string PROC_USP_GETUSERSTOAVERAGEUPLOADREPORT = "USP_GetUsersToAverageUploadReport";//Karteek-ID044
        private static string PROC_USP_GETUSERSTOPAYMENTSREPORT = "USP_GetUsersToPaymentsReport";//Karteek-ID044
        private static string PROC_USP_GETUSERSTOADJUSTMENTREPORT = "USP_GetUsersToAdjustmentReport";//Karteek-ID044
        
        #endregion

        public XmlDocument Get(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETUSERSTOMETERREADINGREPORT, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETUSERSTOAVERAGEUPLOADREPORT, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETUSERSTOPAYMENTSREPORT, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETUSERSTOADJUSTMENTREPORT, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetGender, SqlCon); break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetRoles, SqlCon); break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetDesignations, SqlCon); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUsersList, SqlCon); break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUserPermissionsByRoleId, SqlCon); break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUserDocuments, SqlCon); break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETUSERDETAILSBYUSERID, SqlCon); break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUserDetails, SqlCon); break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetRolesByBU, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertUserDetails, SqlCon); break;
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateUserDetails, SqlCon); break;
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateUserActiveStatus, SqlCon); break;
                    case Statement.Modify:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ResetUserPassword, SqlCon); break;
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteUserDocuments, SqlCon); break;
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangePassword, SqlCon); break;
                    case Statement.Edit:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeletePhoto, SqlCon); break;
                    case Statement.Generate:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangeUserDetails, SqlCon); break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument DisableUserDAL(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DisableUser, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
    }
}
