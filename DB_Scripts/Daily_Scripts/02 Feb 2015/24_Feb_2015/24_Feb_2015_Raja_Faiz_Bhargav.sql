
GO

-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the Last Payment Date  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastPaymentDate]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
  
 -- SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,103) FROM Tbl_CustomerPayments -- comented by Faiz-ID103
 SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106) FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY RecievedDate DESC   
 --ORDER BY CreatedDate DESC-- Raja -ID065  
    
 RETURN @LastPaymentDate   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------
GO
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 18-03-2014  
-- Description: The purpose of this procedure is to update tariff entry
-- Modified By : Bhargav
-- Modified Date : 24th Feb, 2015
-- Modified Description : Restricting deactivation from the middle.
-- =============================================  
ALTER PROCEDURE [USP_DeleteTariffEntry]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @EnergyChargeId INT
			,@ModifiedBy VARCHAR(50)  
			,@MaxEnergyChargeId INT
			,@FromYear DATE
			,@MaxFromYear DATE
			,@MaxToYear DATE
			,@ToYear DATE
			,@ClassID INT
	
	SELECT @EnergyChargeId=C.value('(ClassID)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)

	-- Getting From Year and To Year of the given Energy Charge Id
	SELECT @FromYear = CONVERT(DATE,FromDate)
			,@ToYear = CONVERT(DATE,Todate)
			,@ClassID = ClassID
	FROM Tbl_LEnergyClassCharges
		WHERE EnergyClassChargeID = @EnergyChargeId

	-- Getting Max Energy Charge Id, From Year and To Year based on given Energy Charge Id
	SELECT @MaxEnergyChargeId = MAX(EnergyClassChargeID) 
			,@MaxFromYear = CONVERT(DATE,FromDate)
			,@MaxToYear = CONVERT(DATE,Todate)
	FROM Tbl_LEnergyClassCharges EC
		WHERE ClassID = @ClassID
		AND IsActive = 1
		AND FromDate = @FromYear
		AND Todate = @ToYear
	GROUP BY FromDate,Todate
				
	IF (@EnergyChargeId = @MaxEnergyChargeId) AND (@FromYear = @MaxFromYear) AND (@ToYear = @MaxToYear)
		BEGIN
			UPDATE Tbl_LEnergyClassCharges 
				SET IsActive=0
					,ModifiedBy=@ModifiedBy
					,ModifiedDate= dbo.fn_GetCurrentDateTime()
			WHERE EnergyClassChargeID=@EnergyChargeId
			
			IF EXISTS (SELECT 0 FROM Tbl_LEnergyClassCharges
							WHERE EnergyClassChargeID = @EnergyChargeId - 1
							AND IsActive = 1
							AND ClassID = @ClassID
							AND FromDate = @FromYear
							AND Todate = @ToYear)
				BEGIN
					UPDATE Tbl_LEnergyClassCharges
						SET ToKW = NULL
						WHERE EnergyClassChargeID = EnergyClassChargeID - 1
				END
			ELSE
				BEGIN
					UPDATE Tbl_LEnergyClassCharges
						SET ToKW = NULL
					WHERE EnergyClassChargeID = EnergyClassChargeID
				END
				
			SELECT 1 AS IsSuccess 
			FOR XML PATH('TariffManagementBE')
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
			FOR XML PATH('TariffManagementBE')
		END  
END
GO
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 27-DEC-2014  
-- Description: The purpose of this procedure is to get Book Details by BookNo  
-- Modified By: Padmini  
-- Modified Date:17/02/2015  
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order  
-- Modified By : Bhargav  
-- Modified Date : 23 Feb, 2015  
-- Modified Description : Changed Book No to Book Id and Book Code as ID + Book Code.  
-- Modified By : NEERAJ KANOJIYA  
-- Modified Date : 24 Feb, 2015  
-- Modified Description : ADDED CYCLE FILTER IN WHERE CONDITION. Modifiy SC_ID to ServiceCenterId in deserializtion.
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_BooknNoSearch]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @BookNo VARCHAR(50),  
   @BU_ID VARCHAR(50),  
   @SU_ID VARCHAR(50),  
   @SC_ID VARCHAR(50),
   @CycleId VARCHAR(50)  
 SELECT  
  @BookNo=C.value('(BookNo)[1]','VARCHAR(50)')  
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
  ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')  
  ,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(50)')  
  ,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)  
  
  SELECT BOOKNUMBER.ID AS BookNo  
   ,CASE   
     WHEN BOOKNUMBER.ID IS NULL   
      THEN '--'   
     ELSE BOOKNUMBER.ID + ' (' + BOOKNUMBER.BookCode + ')'   
   END AS BookCode  
   ,BUSINESSUNIT.BusinessUnitName  
   ,SERVICECUNIT.ServiceUnitName  
   ,SERVICECENTER.ServiceCenterName  
   ,CYCLE.CycleName  
  FROM Tbl_BussinessUnits AS BUSINESSUNIT   
  JOIN Tbl_ServiceUnits AS SERVICECUNIT ON BUSINESSUNIT.BU_ID=SERVICECUNIT.BU_ID  
  JOIN Tbl_ServiceCenter AS SERVICECENTER ON SERVICECENTER.SU_ID=SERVICECUNIT.SU_ID  
  JOIN Tbl_Cycles AS CYCLE ON CYCLE.ServiceCenterId=SERVICECENTER.ServiceCenterId --BOOKNUMBER.CycleId=CYCLE.CycleId  
  JOIN Tbl_BookNumbers AS BOOKNUMBER ON BOOKNUMBER.CycleId=CYCLE.CycleId --SERVICECENTER.ServiceCenterId=BOOKNUMBER.ServiceCenterId   
  WHERE (BUSINESSUNIT.BU_ID=@BU_ID OR @BU_ID='')  
  AND (SERVICECUNIT.SU_ID=@SU_ID OR @SU_ID='')  
  AND (SERVICECENTER.ServiceCenterId=@SC_ID OR @SC_ID='')  
  AND (CYCLE.CycleId=@CycleId OR @CycleId='')  
  AND (BOOKNUMBER.BookNo=@BookNo OR @BookNo='')  
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get Customer Name change logs based on search criteria
-- =============================================
ALTER PROCEDURE [USP_GetCustomerNameChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
		@CycleId VARCHAR(50)='',@TariffId INT=0,
		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
			,@TariffId=C.value('(TariffId)[1]','INT')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
		SELECT
		(
			SELECT CNL.AccountNo
					,Remarks
					--,OldName
					--,[NewName]
					--,OldSurName
					--,NewSurName
					,OldKnownAs
					,NewKnownAs
					,S.ApprovalStatus 
					,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate
			FROM Tbl_CustomerNameChangeLogs CNL
			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApproveStatusId
			JOIN [UDV_CustomerDescription] CD ON CNL.AccountNo=CD.GlobalAccountNumber
			AND (BU_ID=@BU_ID OR @BU_ID='')
			AND (SU_ID=@SU_ID OR @SU_ID='')
			--AND (ServiceCenterId=@SC_ID OR @SC_ID='')
			AND (CycleId=@CycleId OR @CycleId='')
			AND (TariffId=@TariffId OR @TariffId=0)
			AND (CONVERT(DATE,CNL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
			AND (CONVERT(DATE,CNL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
			ORDER BY CNL.CreatedDate DESC
			FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get Customer Address change logs based on search criteria
-- =============================================
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
		@CycleId VARCHAR(50)='',@TariffId INT=0,
		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
			,@TariffId=C.value('(TariffId)[1]','INT')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
		SELECT
		(
			SELECT CNL.AccountNo
					,Remarks
					--,OldPostalLandMark
					--,OldPostalStreet
					--,OldPostalCity
					--,OldPostalHouseNo +'<br/>'+OldPostalLandMark+'<br/>'+OldPostalStreet+
					--	'<br/>'+OldPostalCity+'<br/>'+OldPostalZipCode AS OldAddress
					--,OldPostalZipCode
					--,OldServiceLandMark
					--,OldServiceStreet
					--,OldServiceCity
					--,OldServiceHouseNo
					--,OldServiceZipCode
					--,OldServiceHouseNo +'<br/>'+OldServiceLandMark+'<br/>'+OldServiceStreet+
					--	'<br/>'+OldServiceCity+'<br/>'+OldServiceZipCode AS OldServiceAddress
					--,NewPostalLandMark
					--,NewPostalStreet
					--,NewPostalCity
					--,NewPostalHouseNo
					--,NewPostalZipCode
					--,NewPostalHouseNo +'<br/>'+NewPostalLandMark+'<br/>'+NewPostalStreet+
					--	'<br/>'+NewPostalCity+'<br/>'+NewPostalZipCode AS NewAddress
					--,NewServiceLandMark
					--,NewServiceStreet
					--,NewServiceCity
					--,NewServiceHouseNo
					--,NewServiceZipCode
					--,NewServiceHouseNo +'<br/>'+NewServiceLandMark+'<br/>'+NewServiceStreet+
					--	'<br/>'+NewServiceCity+'<br/>'+NewServiceZipCode AS NewServiceAddress
					,S.ApprovalStatus 
					,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					--,(SELECT Name FROM Tbl_CustomerDetails WHERE AccountNo=CNL.AccountNo) AS Name
					,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate
			FROM Tbl_CustomerAddressChangeLogs CNL
			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApproveStatusId
			JOIN [UDV_CustomerDescription] CD ON CNL.AccountNo=CD.GlobalAccountNumber
			AND (BU_ID=@BU_ID OR @BU_ID='')
			AND (SU_ID=@SU_ID OR @SU_ID='')
			AND (ServiceCenterId=@SC_ID OR @SC_ID='')
			AND (CycleId=@CycleId OR @CycleId='')
			AND (TariffId=@TariffId OR @TariffId=0)
			AND (CONVERT(DATE,CNL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
			AND (CONVERT(DATE,CNL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
			ORDER BY CNL.CreatedDate DESC
			FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 27-02-2014
-- Description:	The purpose of this procedure is to get Book Numbers by service details
-- =============================================
ALTER PROCEDURE [USP_GetBookNumbersByServiceDetails]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @BU_ID VARCHAR(20),@SU_ID VARCHAR(20),@ServiceCenterId VARCHAR(20)
	
	SELECT
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')
		,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')
	FROM @XmlDoc.nodes('ConsumerBe') as T(C)
	
	SELECT
	(
		SELECT B.BookNo
			FROM Tbl_BookNumbers B
			JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
			JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
			JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
			JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
			AND BU.BU_ID=@BU_ID
			AND SU.SU_ID=@SU_ID
			AND SC.ServiceCenterId=@ServiceCenterId
			AND B.ActiveStatusId=1
		--SELECT BookNo FROM Tbl_BookNumbers WHERE BU_ID=@BU_ID AND SU_ID=@SU_ID
		--AND ServiceCenterId=@ServiceCenterId AND ActiveStatusId=1
		FOR XML PATH('Consumer'),TYPE
	)
	FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 03-09-2014  
-- Description: The purpose of this procedure is to get Reordered Books list By Cycle   
-- =============================================  
ALTER PROCEDURE [USP_GetBookNoReOrderList]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE @CycleId VARCHAR(20)  
   ,@PageNo INT  
   ,@PageSize INT  
      
 SELECT   @CycleId=C.value('(CycleId)[1]','VARCHAR(20)')  
   ,@PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
 FROM @XmlDoc.nodes('BookNoReorderBe') AS T(C)  
   
   
 ;WITH PagedResults AS  
  (  
   --SELECT  
   --  ROW_NUMBER() OVER(ORDER BY B.ActiveStatusId ASC , B.SortOrder ASC ) AS RowNumber  
   -- ,B.SortOrder  
   -- ,B.BookNo  
   -- ,ISNULL(B.BookCode,'--') AS BookCode  
   -- --,(SELECT BusinessUnitName From Tbl_BussinessUnits WHERE BU_ID=B.BU_ID) AS BusinessUnitName  
   -- --,(SELECT ServiceUnitName From Tbl_ServiceUnits WHERE SU_ID=B.SU_ID) AS ServiceUnitName  
   -- --,(SELECT ServiceCenterName From Tbl_ServiceCenter WHERE ServiceCenterId=B.ServiceCenterId) AS ServiceCenterName  
   -- FROM Tbl_BookNumbers B  
   -- LEFT JOIN Tbl_BookNumbers BC ON B.BookNo=BC.BookNo  
   -- LEFT JOIN Tbl_Cycles C ON C.CycleId=BC.CycleId  
   -- WHERE (C.CycleId=@CycleId OR @CycleId='')
   -- AND B.ActiveStatusId=1
   SELECT  
     ROW_NUMBER() OVER(ORDER BY B.ActiveStatusId ASC , B.SortOrder ASC ) AS RowNumber  
    ,B.SortOrder  
    ,B.BookNo  
    ,ISNULL(B.BookCode,'--') AS BookCode
    ,BU.BusinessUnitName
    ,SU.ServiceUnitName
    ,SC.ServiceCenterName
   FROM Tbl_BookNumbers B
	JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
	AND B.ActiveStatusId=1
	AND (C.CycleId=@CycleId OR @CycleId='')
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('BookNoReorderBe'),TYPE  
  )  
  FOR XML PATH(''),ROOT('BookNoReorderBeInfoByXml')   
    
END  
--------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 04-03-2014
-- Description:	The purpose of this procedure is to get Books list
--Modified By	: Neeraj Kanojiya
--Modified Date	: 22-Aug-2014
--Modified By	: Suresh Kumar
--Modified Date	: Added BookNoWithDetails Field fot MeterReading page
-- =============================================
ALTER PROCEDURE [USP_GetBookNoList]
AS
BEGIN
	SELECT
	(
		SELECT
			B.BookNo
			,BU.BU_ID
			,(CASE WHEN ISNULL(B.Details,'') = '' THEN B.BookNo ELSE ( ISNULL(B.Details,'') + ' - '+B.BookNo) END) AS BookNoWithDetails
		--FROM Tbl_BookNumbers WHERE ActiveStatusId=1
		FROM Tbl_BookNumbers B
		JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
		JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
		JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
		JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
		ORDER BY BookNo ASC
		FOR XML PATH('Consumer'),TYPE
	)
	FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')
END
--------------------------------------------------------------------------------------------------------------------------
GO
  
  