GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================                  
 -- Author  : NEERAJ KANOJIYA                
 -- Create date  : 7 OCT 2014                  
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT #        
 -- Modified BY : Suresh Kumar Dasi 
 -- Modified By : Padmini
 -- Modified Date : 25-12-2014    
 -- =============================================                  
 ALTER PROCEDURE [USP_GetBillAdjustmentDetails]                  
 (                  
 @XmlDoc xml                  
 )                  
 AS                  
 BEGIN                  
  DECLARE	@SearchValue VARCHAR(50)
			,@AccountNo VARCHAR(20)                 
			,@BillNo VARCHAR(20)            
		--	,@OldAccountNo VARCHAR(20)            
		--	,@MeterNo VARCHAR(20)            
		--	,@CustomerBillId INT      
  SELECT       
	@SearchValue = C.value('(SearchValue)[1]','VARCHAR(50)')                                     
   --@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)'),          
   --@BillNo = C.value('(BillNo)[1]','VARCHAR(50)'),                          
   --@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(20)'),                          
   --@MeterNo = C.value('(MeterNo)[1]','VARCHAR(20)')                          
  FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
   
    
  --IF(@AccountNo = '' OR @AccountNo IS NULL)              
  -- BEGIN          
  -- SELECT TOP 1 @AccountNo = AccountNo FROM Tbl_CustomerBills WHERE BillNo=@BillNo OR MeterNo=@MeterNo    
  -- IF(@AccountNo = '' OR @AccountNo IS NULL)         
  --  SELECT @AccountNo=AccountNo FROM Tbl_CustomerDetails WHERE OldAccountNo=@OldAccountNo  
  -- END   
  --IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo=@SearchValue) 
	 -- BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.AccountNo)
		--				FROM Tbl_CustomerDetails AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.AccountNo=CB.AccountNo
		--				WHERE CD.OldAccountNo=@SearchValue OR CD.MeterNo=@SearchValue OR CB.BillNo=@SearchValue)					
	 -- END
    IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@SearchValue) 
	  BEGIN
		SET @AccountNo= (SELECT	TOP 1(CD.GlobalAccountNumber)
						FROM [CUSTOMERS].[Tbl_CustomerSDetail] AS CD
						JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
						JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber 			
						WHERE CD.OldAccountNo=@SearchValue OR CPD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue)					
	  END
  ELSE
	SET @AccountNo=@SearchValue
            
  SELECT                
  (                
  --Get customers details by account #                
  SELECT                
   A.GlobalAccountNumber AS CustomerID               
  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name             
  ,A.GlobalAccountNumber AS AccountNo              
  ,A.DocumentNo       
  ,A.MeterNumber AS MeterNo  
  ,A.OldAccountNo               
  ,A.RouteSequenceNo AS RouteSequenceNumber                 
  ,B.BusinessUnitName                  
  ,D.ServiceUnitName                  
  ,C.ServiceCenterName                  
  ,ReadCodeId                
  ,A.TariffId AS ClassID                
  ,E.ClassName               
  ,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings                  
  ,A.OutStandingAmount AS TotalDueAmount --Commented by Raja-ID065    
  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount --Commented by Raja-ID065    
  --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS TotalDueAmount --Raja-ID065    
  ,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate                  
  ,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount              
  ,dbo.fn_GetCustomerServiceAddress(GlobalAccountNumber) AS ServiceAddress              
  ,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate       
  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials                   
    FROM [UDV_CustomerDescription] AS A                 
    JOIN Tbl_BussinessUnits  AS B ON A.BU_ID = B.BU_ID                
    JOIN Tbl_ServiceCenter   AS C ON A.ServiceCenterId=C.ServiceCenterId                
    JOIN Tbl_ServiceUnits    AS D ON A.SU_ID=D.SU_ID                
    JOIN Tbl_MTariffClasses AS E ON A.TariffId = E.ClassID                   
    WHERE GlobalAccountNumber = @AccountNo                     
    FOR XML PATH('Customerdetails'),TYPE                  
 )              
 ,                
 (                
  --Get customers bill details by account #                
  SELECT TOP(3)                
   A.CustomerBillId AS CustomerBillID,                
  (SELECT BillAdjustmentId FROM Tbl_BillAdjustments AS B WHERE B.CustomerBillId = A.CustomerBillID) AS BAID,              
   A.CustomerId,                
   CONVERT(DECIMAL(18,2),A.Vat) AS Vat,              
   A.BillNo,              
   A.AccountNo,                        
   A.BillNo,                 
   A.PreviousReading AS PreviousReading,                  
   A.PresentReading AS PresentReading,                  
   A.Usage AS Consumption,                
   A.BillYear,                
   A.BillMonth,       A.ReadCodeId  
   ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(A.BillYear,A.BillMonth, A.AccountNo)) AS IsActiveMonth, --Raja-ID065 For getting month locked or not    
   (SELECT dbo.fn_IsCustomerBill_Latest(A.CustomerBillId,A.AccountNo)) AS IsSuccess, --For Checking Is this Bill Latest or NOT ?  
   A.NetEnergyCharges AS NetEnergyCharges,        
   --(SELECT dbo.fn_IsCustomerBilledMonthLocked(CONVERT(int, A.BillYear),CONVERT(int, A.BillMonth), A.AccountNo)) AS IsActiveMonth ,      
   --dbo.fn_GetBillPayments(A.CustomerBillId) AS TotaPayments,          
   dbo.fn_GetTotalPaidAmountOfBill(A.BillNo) AS TotaPayments,          
   --dbo.fn_GetDueBill(A.BillNo) AS DueBill,  --Commented Bu Raja-ID065    
   --CONVERT(DECIMAL(20,2),A.TotalBillAmountWithTax - dbo.fn_GetBillPayments(A.CustomerBillId)) AS DueBill, --Commented By Raja-ID065    
   (SELECT dbo.fn_IsBillAdjusted(A.CustomerBillId))AS IsAdjested,    
   (SELECT ApprovalStatusId FROM Tbl_BillAdjustments WHERE CustomerBillId = A.CustomerBillId) AS ApprovalStatusId,              
   CONVERT(DECIMAL(20,2),TotalBillAmount) AS TotalBillAmount,    
   (CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal, -- Raja-ID065 For GrandTotal    
   ((CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount))-ISNULL(dbo.fn_GetTotalPaidAmountOfBill(A.BillNo),0)) AS DueBill,-- Raja-ID065 For Due Bill    
   --(SELECT ReadCodeId FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS ReadCode,   
   --(SELECT OldAccountNo FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS OldAccountNo,
   (SELECT ReadCodeId FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber =@AccountNo) AS ReadCode,   
   (SELECT OldAccountNo FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber =@AccountNo) AS OldAccountNo,                               
   (SELECT top(1) CustomerReadingId FROM Tbl_CustomerReadings              
    WHERE GlobalAccountNumber=@AccountNo AND               
   ISBILLED=1               
    ORDER BY CustomerReadingId DESC) AS CustomerReadingId              
 --     A.CustomerReadingId                
   FROM Tbl_CustomerBills  AS A                       
  WHERE AccountNo = @AccountNo   
  AND BillNo = (CASE WHEN ISNULL(@BillNo,'') = '' THEN BillNo ELSE @BillNo END)   
  AND PaymentStatusID = 2                
  ORDER BY CustomerBillID DESC                      
  FOR XML PATH('CustomerBillDetails'),TYPE                  
 )                
 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                  
 END
 GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to get Approval settings
-- =============================================
ALTER PROCEDURE [USP_GetApprovalSettings]
AS
BEGIN
	SELECT 
		FAP.FunctionId
		,[Function]
		,AccessLevels
		,CONVERT(VARCHAR(20),FAR.CreatedDate,107) AS CreatedDate
		,FAR.CreatedBy
		,CASE FAP.IsActive WHEN 1 THEN 'true' ELSE 'false' END AS IsActive
		,'Level-'+CONVERT(VARCHAR(2),[Level]) AS Levels
		,(CASE WHEN UserIds IS NULL THEN 'All Users'
			  ELSE SUBSTRING((SELECT ', '+ UserId FROM Tbl_UserDetails 
					WHERE UserDetailsId IN(SELECT com FROM dbo.fn_Split(FAR.UserIds,',')) FOR XML PATH('')),2,100000000) END) AS Users
		,(CASE IsFinalApproval WHEN 1 THEN RoleName+' (Final Approval)' ELSE RoleName END) AS Roles
	 FROM TBL_FunctionalAccessPermission AS FAP
	JOIN TBL_FunctionApprovalRole AS FAR ON FAP.FunctionId=FAR.FunctionId
	JOIN Tbl_MRoles AS R ON FAR.RoleId=R.RoleId
	AND FAP.FunctionId IN(SELECT FunctionId FROM TBL_FunctionApprovalRole GROUP BY FunctionId)
	--ORDER BY [Function] ASC,[Level] ASC
	ORDER BY FAR.CreatedDate DESC,FAP.IsActive DESC
END
GO



