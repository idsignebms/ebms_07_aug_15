  GO
CREATE TABLE Tbl_DirectCustomersAvgReadings
(
	 AvgReadingId INT PRIMARY KEY IDENTITY(1,1)
	,GlobalAccountNumber VARCHAR(50)
	,AverageReading VARCHAR(50)	
	,ActiveStatusId INT DEFAULT 1
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)
GO
  GO
-- =============================================    
-- Author:  Faiz - ID103    
-- Create date: 05-02-2015  
-- Description: The purpose of this procedure is to get User Defined Controls Details   
-- =============================================    
  
ALTER PROCEDURE [dbo].[USP_GetUserDefinedControlsDetails]    
(  
@XmlDoc XML  
)  
AS  
BEGIN    
DECLARE  @PageNo INT    
  ,@PageSize INT    
    
 SELECT       
 @PageNo = C.value('(PageNo)[1]','INT')    
 ,@PageSize = C.value('(PageSize)[1]','INT')     
 FROM @XmlDoc.nodes('UserDefinedControlsBE') AS T(C)    
    
  
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY UDCD.IsActive DESC,CreatedDate DESC) AS RowNumber  
     ,UDCId  
     ,FieldName  
     ,UDCD.ControlTypeId  
     ,CM.ControlTypeName  
     ,UDCD.IsMandatory  
     ,IsActive  
     ,CM.IsHavingRef  
     FROM [MASTERS].[Tbl_USerDefinedControlDetails] as UDCD  
     Join [MASTERS].[Tbl_ControlRefMaster] as CM  
     on UDCD.ControlTypeId = CM.ControlTypeId  
 where CM.ActiveStatusId= 1  
  )SELECT    
   (    
  SELECT *    
    ,(Select COUNT(0) from PagedResults) as TotalRecords         
  FROM PagedResults    
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
  FOR XML PATH('UserDefinedControlsListBE_1'),TYPE    
   )    
   FOR XML PATH(''),ROOT('UserDefinedControlsInfoByXml')    
  END  
  
  
---------------------------------------------------------------------------------------------------
GO
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 18-03-2014  
-- Description: The purpose of this procedure is to update tariff entry  
-- Modified By : Bhargav
-- Modified Date : 26th Feb, 2015
-- Modified Description : Restricted amount to be in increasing order for that period.
-- =============================================  
ALTER PROCEDURE [USP_UpdateTariffEntry]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @EnergyChargeID INT
		,@Amount DECIMAL(18,2)
		,@ModifiedBy VARCHAR(50) 
		,@FromDate DATE
		,@ToDate DATE
		,@ClassId INT 
		,@IsSuccess BIT = 0
		,@IsAmtIncr BIT = 0

	SELECT @EnergyChargeID=C.value('(ClassID)[1]','INT')  
		  ,@Amount=C.value('(Amount)[1]','DECIMAL(18,2)')  
		  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)  

	SELECT @FromDate = CONVERT(DATE,FromDate)
			,@ToDate = CONVERT(DATE,Todate)
			,@ClassId = ClassID
	FROM Tbl_LEnergyClassCharges
	WHERE EnergyClassChargeID = @EnergyChargeId
	
	IF NOT EXISTS(SELECT 0 FROM Tbl_LEnergyClassCharges
					WHERE FromDate = @FromDate
					AND Todate = @ToDate
					AND ClassID = @ClassId
					AND IsActive = 1
					AND (Amount <= @Amount AND 
							EnergyClassChargeID > @EnergyChargeID)) -- Checking if the amount is less than further amounts.
		BEGIN
			IF NOT EXISTS(SELECT 0 FROM Tbl_LEnergyClassCharges
							WHERE FromDate = @FromDate
							AND Todate = @ToDate
							AND ClassID = @ClassId
							AND IsActive = 1
							AND (Amount >= @Amount 
									AND EnergyClassChargeID < @EnergyChargeID)) -- Checking if the amount is less than trailer amounts.
				BEGIN
					UPDATE Tbl_LEnergyClassCharges 
						SET Amount=@Amount
							,ModifiedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime()  
					WHERE EnergyClassChargeID=@EnergyChargeID
					
					SET @IsSuccess = 1
					SET @IsAmtIncr = 1
				END
		END

	SELECT @IsSuccess AS IsSuccess
			,@IsAmtIncr AS IsAmtIncr 
	FOR XML PATH('TariffManagementBE')  
END  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 26-Feb-2015
-- Description: AverageReading details are insertion if exists updation for those customers
-- =============================================
ALTER PROCEDURE [USP_InsertDirectCustomerBillReading]
	(
	@XmlDoc XML
	)
AS
BEGIN
	DECLARE @MinReading varchar(50)
		  ,@CreateBy varchar(50)  
		  ,@AccNum varchar(50)
	  
	 SELECT    @MinReading=C.value('(AverageReading)[1]','VARCHAR(50)')
			  ,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')  
			  ,@AccNum=C.value('(AccNum)[1]','varchar(50)')  
	 FROM @XmlDoc.nodes('BillingBE') AS T(C)
	 
	 
	 IF EXISTS(SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@AccNum)
		BEGIN
			UPDATE Tbl_DirectCustomersAvgReadings
			SET AverageReading=@MinReading
				,ModifiedBy=@CreateBy
				,ModifiedDate=dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber=@AccNum
			SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')			
		END
	ELSE
		BEGIN
			INSERT INTO Tbl_DirectCustomersAvgReadings
					(
						 GlobalAccountNumber
						,AverageReading
						,CreatedBy
						,CreatedDate
					)
			VALUES
					(
						@AccNum
						,@MinReading
						,@CreateBy
						,dbo.fn_GetCurrentDateTime()
					)
			SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		END								
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 26-02-2015
-- Description: The purpose of this Procedure is to MeterDials By Meter No
-- =============================================  
CREATE PROCEDURE [USP_GetMeterDialsByMeterNo]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
 DECLARE @MeterNo VARCHAR(100)
 SELECT  
  @MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)
	DECLARE @MeterDials INT
		
	SET @MeterDials=(SELECT ISNULL(MeterDials,0) 
	FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	SELECT @MeterDials AS MeterDials,1 As IsSuccess 
	
 FOR XML PATH('ConsumerBe')  
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change & MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     --,SurName
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     --,SAD.City AS ServiceCity
     --,SAD.HouseNo AS ServiceHouseNo  
     --,SAD.LandMark  AS ServiceLandMark
     --,SAD.StreetName  AS ServiceStreet
     --,SAD.ZipCode AS ServiceZipCode
     --,PAD.City AS PostalCity  
     --,PAD.HouseNo AS PostalHouseNo
     --,PAD.LandMark  AS PostalLandMark
     --,PAD.StreetName AS PostalStreet
     --,PAD.ZipCode AS PostalZipCode
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  --JOIN (SELECT * FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE IsServiceAddress=0) PAD ON CD.GlobalAccountNumber=PAD.GlobalAccountNumber
  --JOIN (SELECT * FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE IsServiceAddress=1) SAD ON CD.GlobalAccountNumber=SAD.GlobalAccountNumber
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	

;With FinalResult AS  
(  
SELECT     
	T1.AccountNo as AccNum
	,T1.MinReading
	,(SELECT ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=T1.AccountNo) AS ActiveStatusId
 FROM @TempCustomer T1   
 LEFT JOIN( SELECT  C.AccountNo AS AccNum
	,ActiveStatusId    
    FROM [CUSTOMERS].[Tbl_CustomerSDetail] C     
     ) B ON T1.AccountNo=B.AccNum    
)  
  
 SELECT  
  (       
  SELECT   
        AccNum
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo )AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)		
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper])
									
				VALUES				(@AccountNo
									,dbo.fn_GetCurrentDateTime()
									,@ModifiedBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,@Usage))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE @NewMeterDials INT
		
		SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		
		INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper])
									
				VALUES				(@AccountNo
									,dbo.fn_GetCurrentDateTime()
									,@ModifiedBy									
									,@NewMeterReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,@Usage))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0)
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)		
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper])
									
				VALUES				(@AccountNo
									,dbo.fn_GetCurrentDateTime()
									,@ModifiedBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,@Usage))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE @NewMeterDials INT
		
		SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		
		INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper])
									
				VALUES				(@AccountNo
									,dbo.fn_GetCurrentDateTime()
									,@ModifiedBy									
									,@NewMeterReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,@Usage))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0)
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 18-03-2014  
-- Description: The purpose of this procedure is to update tariff entry  
-- Modified By : Bhargav
-- Modified Date : 26th Feb, 2015
-- Modified Description : Restricted amount to be in increasing order for that period.
-- =============================================  
ALTER PROCEDURE [USP_UpdateTariffEntry]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @EnergyChargeID INT
		,@Amount DECIMAL(18,2)
		,@ModifiedBy VARCHAR(50) 
		,@FromDate DATE
		,@ToDate DATE
		,@ClassId INT 
		,@IsSuccess BIT = 0
		,@IsAmtIncr BIT = 0

	SELECT @EnergyChargeID=C.value('(ClassID)[1]','INT')  
		  ,@Amount=C.value('(Amount)[1]','DECIMAL(18,2)')  
		  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)  

	SELECT @FromDate = CONVERT(DATE,FromDate)
			,@ToDate = CONVERT(DATE,Todate)
			,@ClassId = ClassID
	FROM Tbl_LEnergyClassCharges
	WHERE EnergyClassChargeID = @EnergyChargeId
	
	IF NOT EXISTS(SELECT 0 FROM Tbl_LEnergyClassCharges
					WHERE FromDate = @FromDate
					AND Todate = @ToDate
					AND ClassID = @ClassId
					AND IsActive = 1
					AND (Amount <= @Amount AND 
							EnergyClassChargeID > @EnergyChargeID)) -- Checking if the amount is less than further amounts.
		BEGIN
			IF NOT EXISTS(SELECT 0 FROM Tbl_LEnergyClassCharges
							WHERE FromDate = @FromDate
							AND Todate = @ToDate
							AND ClassID = @ClassId
							AND IsActive = 1
							AND (Amount >= @Amount 
									AND EnergyClassChargeID < @EnergyChargeID)) -- Checking if the amount is less than trailer amounts.
				BEGIN
					UPDATE Tbl_LEnergyClassCharges 
						SET Amount=@Amount
							,ModifiedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime()  
					WHERE EnergyClassChargeID=@EnergyChargeID
					
					SET @IsSuccess = 1
					SET @IsAmtIncr = 1
				END
		END

	SELECT @IsSuccess AS IsSuccess
			,@IsAmtIncr AS IsAmtIncr 
	FOR XML PATH('TariffManagementBE')  
END  
GO
---------------------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 11-08-2014   
-- Modified date: 28-10-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get customer  Details By AccountNo,MeterNo  
-- =============================================    
ALTER PROCEDURE [USP_GetCustomerDetailsList]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
  ,@OldAccountNo VARCHAR(50)  
  ,@MeterNo VARCHAR(50)  
  ,@BUID VARCHAR(50)=''  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
  ,@OldAccountNo=C.value('(OldAccountNo)[1]','VARCHAR(50)')    
  ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')    
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)    
     
 IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE (BU_ID=@BUID OR @BUID='')  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
 BEGIN  
   SELECT    
      GlobalAccountNumber AS AccountNo  
     ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name  
     ,(SELECT dbo.fn_GetCustomerAddress(GlobalAccountNumber)) AS ServiceAddress  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber)),'--') AS LastBillDate  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber)),'--') As LastPaidDate  
     ,(SELECT dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber)) AS DueAmount  
     ,TariffId AS ClassID  
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff  
     ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CD.ClusterCategoryId) AS ClusterType
     ,(SELECT ClusterCategoryId FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CD.ClusterCategoryId) AS ClusterTypeId
     ,1 AS IsSuccess  
   FROM [UDV_CustomerDescription] AS CD    
   WHERE (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='')    
   AND (BU_ID=@BUID OR @BUID='')  
   FOR XML PATH('TariffManagementBE')    
 END  
 ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE BU_ID!=@BUID  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
  BEGIN  
   SELECT 1 AS IsCustExistsInOtherBU,1 AS IsSuccess FOR XML PATH('TariffManagementBE')   
  END  
 ELSE  
  SELECT 0 AS IsSuccess FOR XML PATH('TariffManagementBE')   
END   
GO  
---------------------------------------------------------------------------------------------  
