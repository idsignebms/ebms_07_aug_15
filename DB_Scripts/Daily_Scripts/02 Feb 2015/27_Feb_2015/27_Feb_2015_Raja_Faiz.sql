
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 07-JAN-2014  
-- Description: CUSTOMER REGISTRATION  
-- Modified date: 07-JAN-2014  
-- Description: Added Active Status Id Filter  
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_GetPoleNumber]  
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
DECLARE  
  @PoleMasterId INT  
  ,@Name VARCHAR(250)  
  ,@PoleOrder INT  
  ,@IsParentPole BIT=0  
    
SELECT   
  @PoleMasterId =C.value('(PoleMasterId)[1]','INT')  
  ,@Name=C.value('(Name)[1]','VARCHAR(250)')  
  ,@PoleOrder=C.value('(PoleOrder)[1]','INT')  
  ,@IsParentPole =C.value('(IsParentPole)[1]','bit')  
FROM @XmlDoc.nodes('PoleBE') as T(C)    
  
  if(@IsParentPole=0)  
  BEGIN  
   SELECT   
   (  
    SELECT   
      PoleId  
      ,NAME + '-'+ CODE AS Name   
      ,CODE AS Code  
    FROM Tbl_PoleDescriptionTable   
    WHERE   
     --CONVERT(INT,Code)=CONVERT(INT,SUBSTRING(@Name,LEN(@Name)-1,LEN(@Name))) AND   
     POLEORDER=@PoleMasterId AND ActiveStatusId=1  
    FOR XML PATH('PoleBE'),TYPE  
   )  
   FOR XML PATH(''),ROOT('PoleBEInfoByXml')  
  END  
  ELSE  
  BEGIN  
   SELECT   
    (  
     SELECT PoleId, NAME + '-'+ CODE AS Name 
     FROM Tbl_PoleDescriptionTable 
     WHERE PoleMasterId=1007  AND ActiveStatusId=1  -- Faiz-ID103 added AND ActiveStatusId=1 
     FOR XML PATH('PoleBE'),TYPE  
    )  
    FOR XML PATH(''),ROOT('PoleBEInfoByXml')  
  END  
  
END

--------------------------------------------------------------------------------------------------

GO

-- =============================================    
-- Author:  Faiz - ID103
-- Create date: 27-FEB-2015    
-- Description: Existence of Email ID.  
-- =============================================    
CREATE PROCEDURE [dbo].[USP_IsEmailExists]    
(  
@XmlDoc xml  
)  
AS    
BEGIN    
 DECLARE @EmailID VARCHAR(50),  
   @IsSuccessful bit=0;  
   
 SELECT  
 @EmailID=c.value('(EmailID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T (c)  
   
 IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerSDetail  WHERE EmailId =@EmailID) 
 BEGIN 
  SET @IsSuccessful=1;  
 END
 ELSE IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerTenentDetails  WHERE EmailID =@EmailID) 
 BEGIN
   SET @IsSuccessful=1;  
 END
 SELECT @IsSuccessful AS IsSuccessful  
 FOR XML PATH('CustomerRegistrationBE'),TYPE    
END    
GO
---------------------------------------------------------------------------------------------------
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
  
-- Author:  <Bhimaraju.V>  
  
-- Create date: <24-Feb-2014>  
  
-- ModifiedBy : T.Karthik  
  
-- Modified date: 29-11-2014  
  
-- Description: <For Insert ServiceCenter Data Into Tbl_ServiceCenter>  
  
-- ModifiedBy : NEERAJ KANOJIYA  
  
-- Modified date: 22-DEC-2014  
  
-- Description: GET AUTO GEN Service Center Code For Input.  
  
-- =============================================  
  
ALTER PROCEDURE [USP_InsertServiceCenter]  
  
(  
  
 @XmlDoc xml  
  
)  
  
AS  
  
BEGIN  
  
 DECLARE  @ServiceCenterName VARCHAR(300)  
  
   ,@Notes VARCHAR(MAX)  
  
   ,@SU_ID VARCHAR(20)  
  
   ,@CreatedBy VARCHAR(50)  
  
   ,@UniqueId VARCHAR(20)  
  
   ,@SCCode VARCHAR(10)  
  
   ,@Address1 VARCHAR(100)  
  
   ,@Address2 VARCHAR(100)  
  
   ,@City VARCHAR(20)  
  
   ,@ZIP VARCHAR(10)  
  
  
 SELECT   @ServiceCenterName=C.value('(ServiceCenterName)[1]','VARCHAR(300)')  
  
   ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')  
  
   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')  
  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')  
  
   ,@Address1=C.value('(Address1)[1]','VARCHAR(100)')    
     
     ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')    
     
     ,@City=C.value('(City)[1]','VARCHAR(20)')    
     
     ,@ZIP=C.value('(ZIP)[1]','VARCHAR(10)')    
  
  
   --,@SCCode=C.value('(SCCode)[1]','VARCHAR(10)')  
  
    
  
   FROM @XmlDoc.nodes('MastersBE') AS T(C)   
  
 SET @SCCode=dbo.fn_ServiceCenterCodeGenerate(@SU_ID)--Temp  
  
 SET @UniqueId=(SELECT dbo.fn_GenerateUniqueId(3));  
  IF(@SCCode IS NOT NULL)
  BEGIN
 IF NOT EXISTs(SELECT 0 FROM Tbl_ServiceCenter WHERE ServiceCenterName=@ServiceCenterName AND SU_ID=@SU_ID)  
  
 BEGIN  
  
   IF NOT EXISTs(SELECT 0 FROM Tbl_ServiceCenter WHERE SCCode=@SCCode AND SU_ID=@SU_ID)  
  
   BEGIN  
  
   IF(@SCCode IS NOT NULL)  
    BEGIN  
     INSERT INTO Tbl_ServiceCenter (  
  
             ServiceCenterId  
  
            ,ServiceCenterName  
  
            ,CreatedBy  
  
            ,CreatedDate  
  
            ,Notes  
  
            ,SU_ID  
  
            ,SCCode  
  
            ,Address1   
  
            ,Address2   
  
            ,City   
  
            ,ZIP   
            )  
  
          VALUES(  
  
             @UniqueId  
  
            ,@ServiceCenterName  
  
            ,@CreatedBy  
  
            ,DATEADD(SECOND,19800,GETUTCDATE())  
  
            ,CASE @Notes WHEN '' THEN NULL ELSE @Notes END  
  
            ,@SU_ID  
  
            ,@SCCode  
  
            ,@Address1   
          
            ,@Address2   
          
            ,@City   
          
            ,@ZIP   
            )  
    END  
              
  
     SELECT 1 AS IsSuccess,@UniqueId AS ServiceCenterId,@SCCode AS SCCode 
  
     FOR XML PATH ('MastersBE'),TYPE  
  
    END  
  
   ELSE   
  
   BEGIN  
  
     SELECT 1 AS IsSCCodeExists,@SCCode AS SCCode  
  
     FOR XML PATH ('MastersBE'),TYPE  
  
   END  
  
  END  
  
    
  
  ELSE  
  
  BEGIN  
  
    SELECT 0 AS IsSuccess,@SCCode AS SCCode   
  
    FOR XML PATH('MastersBE'),TYPE  
  
  END     
  END
END
--====================================================================================================
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)		
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper])
									
				VALUES				(@AccountNo
									,dbo.fn_GetCurrentDateTime()
									,@ModifiedBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,@Usage))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE @NewMeterDials INT
		
		SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		
		INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper])
									
				VALUES				(@AccountNo
									,dbo.fn_GetCurrentDateTime()
									,@ModifiedBy									
									,@NewMeterReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,@Usage))
									,0
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0)
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
