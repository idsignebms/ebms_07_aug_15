GO
ALTER TABLE Tbl_PoleDescriptionTable
ADD PoleId1 INT IDENTITY(1,1)
GO
ALTER TABLE Tbl_PoleDescriptionTable
ADD PoleOrder1 INT 
GO
CREATE TABLE Tbl_PoleOrderTest
(
 PoleId INT,
 PoleOrder INT
)
GO
INSERT INTO Tbl_PoleOrderTest(PoleId,PoleOrder)
SELECT PoleId,PoleOrder FROM Tbl_PoleDescriptionTable
ORDER BY PoleId
GO
UPDATE Tbl_PoleDescriptionTable 
SET PoleOrder1=PT.PoleOrder FROM Tbl_PoleOrderTest PT
WHERE Tbl_PoleDescriptionTable.PoleId1=PT.PoleId
GO
ALTER TABLE Tbl_PoleDescriptionTable
DROP COLUMN PoleId
GO
ALTER TABLE Tbl_PoleDescriptionTable
DROP COLUMN PoleOrder
GO
sp_RENAME 'Tbl_PoleDescriptionTable.[PoleId1]' , 'PoleId', 'COLUMN';
GO
sp_RENAME 'Tbl_PoleDescriptionTable.[PoleOrder1]' , 'PoleOrder', 'COLUMN'
GO
/****** Object:  StoredProcedure [MASTERS].[USP_BooknNoSearch]    Script Date: 03/03/2015 19:36:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 27-DEC-2014  
-- Description: The purpose of this procedure is to get Book Details by BookNo  
-- Modified By: Padmini  
-- Modified Date:17/02/2015  
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order  
-- Modified By : Bhargav  
-- Modified Date : 23 Feb, 2015  
-- Modified Description : Changed Book No to Book Id and Book Code as ID + Book Code.  
-- Modified By : NEERAJ KANOJIYA  
-- Modified Date : 24 Feb, 2015  
-- Modified Description : ADDED CYCLE FILTER IN WHERE CONDITION. Modifiy SC_ID to ServiceCenterId in deserializtion.
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_BooknNoSearch]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @BookNo VARCHAR(50),  
   @BU_ID VARCHAR(50),  
   @SU_ID VARCHAR(50),  
   @SC_ID VARCHAR(50),
   @CycleId VARCHAR(50)  
 SELECT  
  @BookNo=C.value('(BookNo)[1]','VARCHAR(50)')  
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
  ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')  
  ,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(50)')  
  ,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)  
  
  SELECT BOOKNUMBER.ID AS ID       -- Modified By- Padmini
   ,BOOKNUMBER.BookNo AS BookNo    -- Modified By- Padmini
   ,CASE   
     WHEN BOOKNUMBER.ID IS NULL   
      THEN '--'   
     ELSE BOOKNUMBER.ID + ' (' + BOOKNUMBER.BookCode + ')'   
   END AS BookCode  
   ,BUSINESSUNIT.BusinessUnitName  
   ,SERVICECUNIT.ServiceUnitName  
   ,SERVICECENTER.ServiceCenterName  
   ,CYCLE.CycleName  
  FROM Tbl_BussinessUnits AS BUSINESSUNIT   
  JOIN Tbl_ServiceUnits AS SERVICECUNIT ON BUSINESSUNIT.BU_ID=SERVICECUNIT.BU_ID  
  JOIN Tbl_ServiceCenter AS SERVICECENTER ON SERVICECENTER.SU_ID=SERVICECUNIT.SU_ID  
  JOIN Tbl_Cycles AS CYCLE ON CYCLE.ServiceCenterId=SERVICECENTER.ServiceCenterId --BOOKNUMBER.CycleId=CYCLE.CycleId  
  JOIN Tbl_BookNumbers AS BOOKNUMBER ON BOOKNUMBER.CycleId=CYCLE.CycleId --SERVICECENTER.ServiceCenterId=BOOKNUMBER.ServiceCenterId   
  WHERE (BUSINESSUNIT.BU_ID=@BU_ID OR @BU_ID='')  
  AND (SERVICECUNIT.SU_ID=@SU_ID OR @SU_ID='')  
  AND (SERVICECENTER.ServiceCenterId=@SC_ID OR @SC_ID='')  
  AND (CYCLE.CycleId=@CycleId OR @CycleId='')  
  AND (BOOKNUMBER.BookNo=@BookNo OR @BookNo='')  
END  
GO
GO
ALTER TABLE Tbl_CustomerReadings
ADD MeterNumber VARCHAR(50)
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [USP_GetCustomerDetailsForPaymentEntry]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
			,@BU_ID VARCHAR(50)
 SELECT  
  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 IF((SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))=1)  
	 BEGIN  
		  SELECT  
		   --dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		   dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
		   ,ClassName  
		   --,(SELECT DBO.fn_GetCustomerTotalDueAmount(@AccountNo)) AS DueAmount  --Commented By Raja-ID065
		   ,OutStandingAmount AS DueAmount  --Commented By Raja-ID065
		   --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS DueAmount --Raja-ID065
		   --,(SELECT CONVERT(DECIMAL(20,2),SUM(ISNULL(TotalBillAmountWithArrears,0))) FROM Tbl_CustomerBills WHERE ActiveStatusId=1 AND BillStatusId=2 AND AccountNo=@AccountNo) AS DueAmount  
		   ,1 AS IsCustomerExists  
		  FROM [UDV_CustomerDescription] 
		  WHERE GlobalAccountNumber=@AccountNo  
		  AND(BU_ID = @BU_ID OR @BU_ID = '')
		  FOR XML PATH('MastersBE')  
	 END  
	 ELSE  
		 BEGIN  
			SELECT 0 AS IsCustomerExists 
			FOR XML PATH('MastersBE')  
		 END  
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerDescription CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerDescription CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) <= CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum AND   
		CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC;  

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  ,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading   
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading   
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerDescription C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					 ,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						 CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate(C.GlobalAccountNumber,@ReadDate)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 ,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				 CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate(C.GlobalAccountNumber,@ReadDate)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
-----------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 18-10-2014
-- Description:	To Get FullName Of the Customer Title+Name+SurName
-- =============================================
CREATE FUNCTION [dbo].[fn_GetCustomerFullName_New]
(
	@Title VARCHAR(50),@FirstName VARCHAR(50) ,@MiddleName VARCHAR(50),@LastName VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Name VARCHAR(MAX)

		SELECT @Name =	LTRIM(RTRIM(
		CASE WHEN @Title IS NULL OR @Title = '' THEN '' ELSE LTRIM(RTRIM(@Title+'. ')) END + 
		CASE WHEN @FirstName IS NULL OR @FirstName = '' THEN '' ELSE  LTRIM(RTRIM(@FirstName)) END + 
		CASE WHEN @MiddleName IS NULL OR @MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@MiddleName)) END +
		CASE WHEN @LastName IS NULL OR @LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@LastName)) END))
 
	RETURN @Name
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 18-07-2014  
-- Description: Insert Bill Payments  
-- Modified By: Neeraj Kanojiya
-- Modified on: 4-Sep-2014
-- Modified By: Padmini
-- Modified Date : 27-12-2014
-- =============================================  
ALTER PROCEDURE [USP_InsertBillPayments]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
    DECLARE  @CustomerID VARCHAR(50)  
    ,@AccountNo VARCHAR(50)  
    ,@ReceiptNo VARCHAR(20)  
    ,@PaymentMode INT  
    ,@DocumentPath VARCHAR(MAX)  
    ,@DocumentName VARCHAR(MAX)  
    --,@BillNo VARCHAR(50)  
    ,@Cashier VARCHAR(50)  
    ,@CashOffice INT  
    ,@CreatedBy VARCHAR(50)  
    ,@BatchNo INT  
    ,@PaidAmount DECIMAL(20,4)  
    ,@PaymentRecievedDate VARCHAR(20)  
    ,@CustomerPaymentId INT  
    ,@EffectedRows INT  
  DECLARE @PaidBillTemp TABLE (AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),BillNo VARCHAR(50),BillStatus INT)    
     
     
  SELECT   @CustomerID=C.value('(CustomerID)[1]','VARCHAR(50)')  
    ,@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
    ,@ReceiptNo=C.value('(ReceiptNo)[1]','VARCHAR(20)')  
    ,@PaymentMode=C.value('(PaymentModeId)[1]','INT')  
    ,@DocumentPath=C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
    ,@DocumentName=C.value('(Document)[1]','VARCHAR(MAX)')  
    --,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')  
    ,@BatchNo=C.value('(BatchNo)[1]','INT')  
    ,@PaidAmount=C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
    ,@PaymentRecievedDate=C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
   
  --SET @CustomerID=(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo);  
    
  --IF(NOT EXISTS(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails where AccountNo=@AccountNo)) 
  
  IF(NOT EXISTS(SELECT GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] where GlobalAccountNumber=@AccountNo)) --Checking existence of customer befor payment
   SELECT 0 AS IsCustomerExists FOR XML PATH ('MastersBE')  
  ELSE  -- If customer is exists than do payment
   BEGIN     
   
    DELETE FROM @PaidBillTemp  --Flushing the temp table
    
    INSERT INTO @PaidBillTemp  --Inserting into temp table. If customer pay lumpsum amount for more than one bill then amount should be split according to bill.
    SELECT AccountNo,AmountPaid,BillNo,BillStatus FROM [dbo].[fn_GetBillPaymentDetails](@AccountNo,@PaidAmount)  --Called function, that returns table whic has splited amount according to bills by account no.
    WHERE AmountPaid>0  
         
    INSERT INTO Tbl_CustomerPayments(	--Inserting into Customer payment details table normal values.
         -- CustomerID  
           AccountNo  
         ,ReceiptNo  
         ,PaymentMode  
         ,DocumentPath  
         ,DocumentName  
         ,Cashier  
         ,CashOffice  
         ,CreatedBy  
         ,CreatedDate  
         ,BatchNo  
         ,RecievedDate  
         ,PaidAmount  
         ,ActivestatusId  
         )  
       VALUES(    
        -- @CustomerID  
          @AccountNo  
         ,@ReceiptNo  
         ,@PaymentMode  
         ,@DocumentPath  
         ,@DocumentName  
         ,@Cashier  
         ,@CashOffice  
         ,@CreatedBy  
         ,dbo.fn_GetCurrentDateTime()  
         ,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)   
         ,@PaymentRecievedDate  
         ,@PaidAmount  
         ,1  
         )  
       
    SELECT @CustomerPaymentId  = SCOPE_IDENTITY()       
      
    INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,PaidAmount,BillNo,CreatedBy,CreatedDate)  --Inserting into payment details table with splited details.    
    SELECT @CustomerPaymentId,AmountPaid,BillNo,@CreatedBy,dbo.fn_GetCurrentDateTime() FROM @PaidBillTemp  
         
    UPDATE CB   
     SET PaymentStatusID = PT.BillStatus  
    FROM Tbl_CustomerBills CB  
    JOIN @PaidBillTemp PT ON CB.BillNo = PT.BillNo  
    WHERE CB.AccountNo = @AccountNo  
    
  --  UPDATE Tbl_CustomerDetails 
		--SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)--(SELECT [dbo].[fn_GetCustomerTotalDueAmount] (@AccountNo))
  --  WHERE AccountNo = @AccountNo
	 
    UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
      SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)--(SELECT [dbo].[fn_GetCustomerTotalDueAmount] (@AccountNo))
    WHERE GlobalAccountNumber = @AccountNo
       
    SELECT @EffectedRows = @@ROWCOUNT  
      
    SELECT   
      1 AS IsCustomerExists  
     ,(CASE WHEN @EffectedRows > 0 THEN 1 ELSE 0 END)AS IsSuccess  
    -- ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount  
    --FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo  
    FOR XML PATH ('MastersBE')   
       
   END  
END
-------------------------------------------------------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Modified By: Neeraj Kanojiya
-- Modified on: 18-June-2014
-- Insert Batch Details
-- =============================================    
ALTER PROCEDURE [USP_InsertBatchDetails](@XmlDoc XML)          
          
AS          
BEGIN          
		DECLARE  @BatchNo int,          
				 @BatchName varchar(50) ,          
				 @BatchDate datetime ,          
				 @CashOffice INT ,          
				 @CashierID VARCHAR(50) ,          
				 @CreatedBy varchar(50),          
				 @BatchTotal NUMERIC(20,4),          
				 @Description VARCHAR(MAX),      
				 @Flag INT          
           
		 SELECT           
				 @BatchNo=C.value('(BatchNo)[1]','int')          
				,@BatchName=C.value('(BatchName)[1]','varchar(50)')          
				,@BatchDate=C.value('(BatchDate)[1]','DATETIME')          
				,@CashOffice = C.value('(OfficeId)[1]','INT')          
				,@CashierID = C.value('(CashierId)[1]','VARCHAR(50)')           
				,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')          
				,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')          
				,@Description=C.value('(Description)[1]','VARCHAR(MAX)')          
				,@Flag=C.value('(Flag)[1]','INT')          
		              
		 FROM @XmlDoc.nodes('BillingBE') AS T(C)       
 --TEMP INSERTING FOR APPROVAL         
    IF(@Flag=1)      
	  BEGIN      
		IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo))          
		BEGIN          
		  INSERT INTO Tbl_BatchDetailsApproval(          
												  BatchNo,          
												  --BatchName,          
												  BatchDate,          
												  CashOffice,          
												  CashierID,          
												  CreatedBy,          
												  CreatedDate,          
												  BatchTotal,          
												  BatchStatus          
												  --[Description]          
												 )          
										   VALUES(          
												  @BatchNo,          
												  --Case  @BatchName when '' then null else @BatchName end,
												  @BatchDate,
												  Case  @CashOffice when 0 then null else @CashOffice end,          
												  Case  @CashierID when '' then null else @CashierID end,           
												  @CreatedBy,
												  dbo.fn_GetCurrentDateTime(),             
												  @BatchTotal,
												  1          
												  --CASE @Description WHEN '' THEN NULL ELSE @Description END          
												 )          
	                
		  SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')          
		END          
		ELSE          
			  SELECT 0 as IsSuccess
					,BatchStatus AS BatchStatusId
					,BatchDate,BatchName
					,BatchNo
					,BatchTotal
					,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) 
							  FROM Tbl_CustomerPaymentsApproval          
							  WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount          
			  FROM Tbl_BatchDetailsApproval 
			  WHERE BatchNo=@BatchNo 
			  FOR XML PATH('BillingBE')          
	        
	  END    
	IF(@Flag=2)      
	  BEGIN      
		IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo))          
		BEGIN          
		  INSERT INTO Tbl_BatchDetails(          
									  BatchNo,          
									  --BatchName,          
									  BatchDate,          
									  CashOffice,          
									  CashierID,          
									  CreatedBy,          
									  CreatedDate,          
									  BatchTotal,          
									  BatchStatus          
									  --[Description]          
									 )          
								VALUES(          
									  @BatchNo,          
									  --@BatchName when '' then null else @BatchName end,          
									  @BatchDate,          
									  Case  @CashOffice when 0 then null else @CashOffice end,          
									  Case  @CashierID when '' then null else @CashierID end,           
									  @CreatedBy,              
									  dbo.fn_GetCurrentDateTime(),             
									  @BatchTotal,          
									  1          
									  --CASE @Description WHEN '' THEN NULL ELSE @Description END          
									 )          
	                
		  SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')          
		END          
		ELSE          
		  SELECT 0 as IsSuccess
				,BatchStatus AS BatchStatusId
				,BatchDate
				,BatchName
				,BatchNo
				,BatchTotal
				,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments          
						WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount          
		  FROM Tbl_BatchDetails 
		  WHERE BatchNo=@BatchNo 
		  FOR XML PATH('BillingBE')
	  END    
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 02-MAR-2015
-- Description:	The purpose of this procedure is to get Details of User
-- =============================================
CREATE PROCEDURE [USP_GetUserDetails]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @UserId VARCHAR(50)
				
	SELECT @UserId = C.value('(UserId)[1]','VARCHAR(50)')	
	FROM @XmlDoc.nodes('UserManagementBE') AS T(C)
		
	SELECT
		 D.UserId AS EmployeeId
		,D.Name
		,D.SurName
		,D.PrimaryContact AS ContactNo1
		,ISNULL(D.SecondaryContact,'---') AS ContactNo2
		,D.PrimaryEmailId
		,ISNULL(D.SecondaryEmailId,'---') AS SecondaryEmailId
		,D.[Address]
		,D.GenderId
		,(SELECT GenderName From Tbl_MGender Where GenderId=D.GenderId) As GenderName
		,ISNULL(D.Details,'---') AS Details
		,D.RoleId
		,(SELECT RoleName From Tbl_MRoles Where RoleId=D.RoleId) As RoleName
		,D.DesignationId
		,(SELECT DesignationName From Tbl_MDesignations Where DesignationId=D.DesignationId) As DesignationName
		,D.UserDetailsId
		,ISNULL(D.Photo,'--') AS Photo
		,ISNULL(D.FilePath,'--') AS FilePath
		,L.ActiveStatusId
		,D.IsMobileAccess
		,1 AS IsSuccess
	FROM Tbl_UserDetails D				
	JOIN Tbl_UserLoginDetails L ON D.UserId=L.UserId WHERE L.ActiveStatusId in (1,2)
	AND D.UserId =@UserId				
	FOR XML PATH('UserManagementBE'),TYPE
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-03-2015
-- Description:	The purpose of this function is to get customer
--				 Previous Reading based on meter billing read date 				
-- =============================================
CREATE FUNCTION [fn_GetCustPrevReadingByReadDate_New]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
	,@MeterNumber VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousReading VARCHAR(50)=''

	SET @PreviousReading=(SELECT CASE WHEN (SELECT TOP(1) PresentReading 
							FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber=@GlobalAccountNumber 
							AND CR.MeterNumber=@MeterNumber
							AND CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  
						 order by CustomerReadingId desc)  
					   IS NULL  
					   THEN  CONVERT(VARCHAR(50),ISNULL((SELECT InitialReading FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber=@GlobalAccountNumber),0))  
					   ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   
					  CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END 
					  )
	RETURN @PreviousReading	

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 13-10-2014
-- Description:	The purpose of this function is to get customer
--				 Previous Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [fn_GetCustPrevReadingByReadDate]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousReading VARCHAR(50)=''

	SET @PreviousReading=(SELECT CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   
						 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					   IS NULL  
					   THEN  CONVERT(VARCHAR(50),ISNULL((SELECT InitialReading FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber=@GlobalAccountNumber),0))  
					   ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   
					  CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END 
					  )
	RETURN @PreviousReading	

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-03-2015
-- Description:	The purpose of this function is to get customer
--				 Present Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [fn_GetCustPresentReadingByReadDate]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
	,@MeterNumber VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	
	DECLARE @PresentReading VARCHAR(50)
		
	SET @PresentReading=(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
									WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =@GlobalAccountNumber
									AND CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) 
												 AND CR.MeterNumber=@MeterNumber
												 order by CustomerReadingId desc)
					  
	RETURN @PresentReading
	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- =============================================
ALTER PROCEDURE [USP_InsertCustomerBillReading](@XmlDoc XML)
AS
BEGIN
DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@IsTamper BIT
		,@Multiple numeric(20,4);
SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@ReadBy=C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous=C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage= CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		
FROM @XmlDoc.nodes('BillingBE') AS T(C)

   
     --SET @CustUn=(SELECT TOP 1 CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccNum)

     --SET @Multiple=(SELECT TOP 1 MultiplicationFactor FROM Tbl_CustomerDetails WHERE AccountNo=@AccNum);
     
     
	SET @Multiple= (SELECT MI.MeterMultiplier FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	JOIN Tbl_MeterInformation MI ON MI.MeterNo=CPD.MeterNumber
	WHERE CPD.GlobalAccountNumber=@AccNum)
	
	DECLARE @MeterNumber VARCHAR(50)
	
	SET @MeterNumber=(SELECT MeterNumber FROM UDV_CustomerDescription WHERE GlobalAccountNumber=@AccNum)
       
  INSERT INTO Tbl_CustomerReadings (
									 --[CustomerUniqueNo]
									  GlobalAccountNumber
									  ,[ReadDate]
									  ,[ReadBy]
									  ,[PreviousReading]
									  ,[PresentReading]
									  ,[AverageReading]
									  ,[Usage]
									  ,[TotalReadingEnergies]
									  ,[TotalReadings]
									  ,[Multiplier]
									  ,[ReadType]
									  ,[CreatedBy]
									  ,[CreatedDate]
									  ,[IsTamper]
									  ,MeterNumber)
  
									VALUES(
									--@CustUn
									@AccNum
										   ,@ReadDate
										   ,@ReadBy
										   ,CONVERT(VARCHAR(50),@Previous)
										   ,CONVERT(VARCHAR(50),@Current)
										   --,(
										   -- (SELECT case when SUM( PresentReading) is NULL THEN 0 ELSE SUM( PresentReading) END FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Current)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1)
										   ,(SELECT dbo.fn_GetAverageReading(@AccNum,@Usage))
										 --,CONVERT(VARCHAR(50),((SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Usage)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1))
										   ,@Usage
										   ,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+@Usage
										   ,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+1)
										   ,@Multiple
										   ,2
										   ,@CreateBy
										   ,GETDATE()
										   ,@IsTamper
										   ,@MeterNumber)
										   
SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerDescription CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerDescription CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		--SELECT 
		--	TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC; 
		
		SELECT 
			TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum 
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  --,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading
				  ,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
				  --,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerDescription C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					--,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  ,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  --,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
-----------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 ,@OldMeterNo VARCHAR(50)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
		SET @ReadBy=(SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,(SELECT CONVERT(DATE,dbo.fn_GetCurrentDateTime()))
									,@ReadBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@OldMeterNo)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		--If New MeterNo assighned to that customer
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET InitialReading=@NewMeterReading
		WHERE GlobalAccountNumber=@AccountNo
		
		--DECLARE @NewMeterDials INT
		
		--SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		
		--INSERT INTO Tbl_CustomerReadings
		--							(GlobalAccountNumber
		--							,ReadDate
		--							,[ReadBy]
		--							,PreviousReading
		--							,PresentReading
		--							,[AverageReading]
		--							,Usage
		--							,[TotalReadingEnergies]
		--							,[TotalReadings]
		--							,Multiplier
		--							,ReadType
		--							,[CreatedBy]
		--							,CreatedDate
		--							,[IsTamper])
									
		--		VALUES				(@AccountNo
		--							,(SELECT CONVERT(DATE,dbo.fn_GetCurrentDateTime()))
		--							,@ReadBy									
		--							,@NewMeterReading
		--							,@NewMeterReading
		--							,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
		--							,0
		--							,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		--							,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
		--							,1
		--							,2
		--							,@ModifiedBy
		--							,dbo.fn_GetCurrentDateTime()
		--							,0)
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
/****** Object:  StoredProcedure [CUSTOMERS].[USP_CustomerRegistration]    Script Date: 03/03/2015 18:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	CUSTOMER REGISTRATION
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_CustomerRegistration]
(   
 @XmlDoc xml  
)  
AS  
BEGIN  
DECLARE
				@GlobalAccountNumber  Varchar(10)
				,@AccountNo  Varchar(50)
				,@TitleTanent varchar(10)
				,@FirstNameTanent	varchar(100)
				,@MiddleNameTanent	varchar(100)
				,@LastNameTanent	varchar(100)
				,@PhoneNumberTanent	varchar(20)
				,@AlternatePhoneNumberTanent 	varchar(20)
				,@EmailIdTanent	varchar(MAX)
				,@TitleLandlord	varchar(10)
				,@FirstNameLandlord	varchar(50)
				,@MiddleNameLandlord	varchar(50)
				,@LastNameLandlord	varchar(50)
				,@KnownAs	varchar(150)
				,@HomeContactNumberLandlord	varchar(20)
				,@BusinessPhoneNumberLandlord 	varchar(20)
				,@OtherPhoneNumberLandlord	varchar(20)
				,@EmailIdLandlord	varchar(MAX)
				,@IsSameAsService	BIT
				,@IsSameAsTenent	BIT
				,@DocumentNo	varchar(50)
				,@ApplicationDate	Datetime
				,@ConnectionDate	Datetime
				,@SetupDate	Datetime
				,@IsBEDCEmployee	BIT
				,@IsVIPCustomer	BIT
				,@OldAccountNo	Varchar(20)
				,@CreatedBy	varchar(50)
				,@CustomerTypeId	INT
				,@BookNo	varchar(30)
				,@PoleID	INT
				,@MeterNumber	Varchar(50)
				,@TariffClassID	INT
				,@IsEmbassyCustomer	BIT
				,@EmbassyCode	Varchar(50)
				,@PhaseId	INT
				,@ReadCodeID	INT
				,@IdentityTypeId	INT
				,@IdentityNumber	VARCHAR(100)
				,@UploadDocumentID	varchar(MAX)
				,@CertifiedBy	varchar(50)
				,@Seal1	varchar(50)
				,@Seal2	varchar(50)
				,@ApplicationProcessedBy	varchar(50)
				,@EmployeeName	varchar(100)
				,@InstalledBy 	INT
				,@AgencyId	INT
				,@IsCAPMI	BIT
				,@MeterAmount	Decimal(18,2)
				,@InitialBillingKWh	BigInt
				,@InitialReading	BigInt
				,@PresentReading	BigInt
				,@StatusText NVARCHAR(max)
				,@CreatedDate DATETIME
				,@PostalAddressID INT
				,@Bedcinfoid INT
				,@ContactName varchar(100)
				,@HouseNoPostal	varchar(50)
				,@StreetPostal Varchar(50)
				,@CityPostaL	varchar(50)
				,@AreaPostal	INT
				,@HouseNoService	varchar(50)
				,@StreetService	varchar(50)
				,@CityService	varchar(50)
				,@AreaService	INT
				,@TenentId INT
				,@ServiceAddressID INT
				,@IdentityTypeIdList	varchar(MAX)
				,@IdentityNumberList	varchar(MAX)
				,@DocumentName 	varchar(MAX)
				,@Path 	varchar(MAX)
				,@IsValid bit=1
				,@EmployeeCode INT
				,@PZipCode VARCHAR(50)
				,@SZipCode VARCHAR(50)
				,@AccountTypeId INT
				,@ClusterCategoryId INT
				,@RouteSequenceNumber INT
				,@Length INT
				,@UDFTypeIdList	varchar(MAX)
				,@UDFValueList	varchar(MAX)
				,@IsSuccessful BIT=1
				,@MGActTypeID INT
SELECT 
				 @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')
				,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')
				,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')
				,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')
				,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')
				,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')
				,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')
				,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')
				,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')
				,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')
				,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')
				,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')
				,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')
				,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')
				,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')
				,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')
				,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')
				,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')
				,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')
				,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')
				,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')
				,@SetupDate=C.value('(SetupDate)[1]','Datetime')
				,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')
				,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')
				,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')
				,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')
				,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
				,@BookNo=C.value('(BookNo)[1]','varchar(30)')
				,@PoleID=C.value('(PoleID)[1]','INT')
				,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')
				,@TariffClassID=C.value('(TariffClassID)[1]','INT')
				,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')
				,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')
				,@PhaseId=C.value('(PhaseId)[1]','INT')
				,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')
				,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')
				,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')
				,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')
				,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')
				,@Seal1=C.value('(Seal1)[1]','varchar(50)')
				,@Seal2=C.value('(Seal2)[1]','varchar(50)')
				,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')
				,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')
				,@InstalledBy =C.value('(InstalledBy )[1]','INT')
				,@AgencyId=C.value('(AgencyId)[1]','INT')
				,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')
				,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')
				,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')
				,@InitialReading=C.value('(InitialReading)[1]','BigInt')
				,@PresentReading=C.value('(PresentReading)[1]','BigInt')
				,@HouseNoPostal	=C.value('(	HouseNoPostal	)[1]','	varchar(50)	')
				,@StreetPostal=C.value('(StreetPostal)[1]','varchar(50)')
				,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')
				,@AreaPostal=C.value('(AreaPostal)[1]','INT')
				,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')
				,@StreetService=C.value('(StreetService)[1]','varchar(50)')
				,@CityService=C.value('(CityService)[1]','varchar(50)')
				,@AreaService=C.value('(AreaService)[1]','INT')
				,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')
				,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')
				,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')
				,@Path=C.value('(Path)[1]','varchar(MAX)')
				,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')
				,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')
				,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')
				,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')
				,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')
				,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')
				,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')
				,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')
				,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')
				
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  


--||***************************TEMP RESOURCE REMOVAL****************************||--
BEGIN TRY
	DROP TABLE #DocumentList
	DROP TABLE #UDFList
	DROP TABLE #IdentityList
END TRY
BEGIN CATCH

END CATCH
--||****************************************************************************||--
--===========================REGISTRATION STEPS===========================--

--STEP 1	:	GENERATE GLOBAL ACCOUNT NO

--STEP 2	:	GENERATE ACCOUNT NO.

--STEP 3	:	ADD Customer Postal AddressDetails

--STEP 4	:	ADD CustomerTenentDetails.

--STEP 5	:	ADD Customer Details

--STEP 6	:	ADD Customer Procedural Details

--STEP 7	:	ADD Customer Identity Details

--STEP 8	:	ADD Application Process Details

--STEP 9	:	ADD Application Process Person Details

--STEP 10	:	ADD Tbl_CustomerActiveDetails

--STEP 11	:	ADD Tbl_CustomerDocuments

--STEP 12	:	ADD Tbl_USerDefinedValueDetails

--STEP 13	:	ADD Tbl_GovtCustomers

--STEP 14	:	ADD Tbl_Paidmeterdetails

--===========================******************===========================--


--===========================MEMBERS STARTS===========================--+
SET @StatusText='SUCCESS!'

--GENERATE GLOBAL ACCOUNT NUMBER
--||***************************STEP 1****************************||--
BEGIN TRY
    SET @GlobalAccountNumber=CUSTOMERS.fn_GlobalAccountNumberGenerate();
END TRY
BEGIN CATCH
	SET @StatusText='Cannot generate global account no.';
	SET @IsValid=0;
END CATCH

--||***************************STEP 2****************************||--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		DECLARE @BU_ID VARCHAR(50)
				,@SU_ID VARCHAR(50)
				,@ServiceCenterId VARCHAR(50)
		SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId 
		FROM Tbl_BookNumbers BN 
		JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId
		JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
		JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
		JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
		 WHERE BookNo=@BookNo
		 
		SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate 
		--SET @AccountNo='00000001'
	END TRY
	BEGIN CATCH
	SET @IsValid=0;
		SET @StatusText='Cannot generate account no.';
	END CATCH
END
--GET DATE FROM UDF
SET @CreatedDate=dbo.fn_GetCurrentDateTime();

--==========================Preparation of Identity details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
	CREATE TABLE #IdentityList(
								value1 varchar(MAX)
								,value2 varchar(MAX)
								,GlobalAccountNumber VARCHAR(10)
								,CreatedDate DATETIME
								,CreatedBy VARCHAR(50)
								)
	INSERT INTO #IdentityList(
								value1
								,value2
								,GlobalAccountNumber
								,CreatedDate
								,CreatedBy
							)
	select	Value1
			,Value2
			,@GlobalAccountNumber AS GlobalAccountNumber
			,@CreatedDate  AS CreatedDate
			,@CreatedBy AS CreatedBy
	from  [dbo].[fn_splitTwo](@IdentityTypeIdList,@IdentityNumberList,'|')
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare Identity list.';
	END CATCH
END
--select * from  [dbo].[fn_splitTwo]('2|1|3|','456456|24245|645646|','|')
--==========================Preparation of Document details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@DocumentName)+ LEN(@Path)		
		CREATE TABLE #DocumentList(
									value1 varchar(MAX)
									,value2 varchar(MAX)
									,GlobalAccountNumber VARCHAR(10)
									,ActiveStatusId INT
									,CreatedDate DATETIME
									,CreatedBy VARCHAR(50)
							)
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #DocumentList(
										GlobalAccountNumber
										,value1
										,value2
										,ActiveStatusId
										,CreatedBy
										,CreatedDate
									)
			select	@GlobalAccountNumber AS GlobalAccountNumber
					,Value1
					,Value2
					,1
					,@CreatedBy AS CreatedBy
					,@CreatedDate  AS CreatedDate
			from  [dbo].[fn_splitTwo](@DocumentName,@Path,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare document list.';
	END CATCH
END
--==========================Preparation of User Define Controls details table============================--
	
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@UDFTypeIdList)+ LEN(@UDFValueList)	
		CREATE TABLE #UDFList(
									UDCId	INT
									,Value	VARCHAR(150)
									,GlobalAccountNumber VARCHAR(10)
							)	
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #UDFList(										
									UDCId
									,Value
									,GlobalAccountNumber
									)
			select	
									Value1
									,Value2
									,@GlobalAccountNumber AS GlobalAccountNumber
			from  [dbo].[fn_splitTwo](@UDFTypeIdList,@UDFValueList,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare User Define Control list.';
	END CATCH
END
--===========================MEMBERS ENDS===========================--

--===========================REGISTRATION STARTS===========================--
DECLARE @intErrorCode INT
IF(@IsValid=1)
BEGIN
BEGIN TRY
	BEGIN TRAN
		IF(@GlobalAccountNumber IS NOT NULL)
			BEGIN
				IF(@AccountNo IS NOT NULL)
					BEGIN
		--||***************************STEP 3****************************||--
				SET @StatusText='Postal Address';
				INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoPostal
														,@StreetPostal
														,@CityPostaL
														,@AreaPostal
														,@PZipCode
														,0
														,@CreatedDate
														,@CreatedBy
														)
				SET @PostalAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				
				IF(@IsSameAsService=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoService
														,@StreetService
														,@CityService
														,@AreaService
														,@SZipCode
														,1
														,@CreatedDate
														,@CreatedBy
														)
				SET @ServiceAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				END
				
				--||***************************STEP 4****************************||--
				SET @StatusText='Tenent Address';
				IF(@IsSameAsTenent=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerTenentDetails(
														GlobalAccountNumber
														,Title
														,FirstName
														,MiddleName
														,LastName
														,PhoneNumber
														,AlternatePhoneNumber
														,EmailID
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@TitleTanent
														,@FirstNameTanent
														,@MiddleNameTanent
														,@LastNameTanent
														,@PhoneNumberTanent
														,@AlternatePhoneNumberTanent
														,@EmailIdTanent
														,@CreatedDate
														,@CreatedBy

														)
					SET @TenentId=SCOPE_IDENTITY();--(SELECT TOP 1 TenentId FROM CUSTOMERS.Tbl_CustomerTenentDetails)
				END
				--||***************************STEP 5****************************||--
				SET @StatusText='Customer Details';
					INSERT INTO CUSTOMERS.Tbl_CustomersDetail (
														GlobalAccountNumber
														,AccountNo
														,Title
														,FirstName
														,MiddleName
														,LastName
														,KnownAs
														,EmailId
														,HomeContactNumber
														,BusinessContactNumber
														,OtherContactNumber
														,IsSameAsService
														,ServiceAddressID
														,PostalAddressID
														,IsSameAsTenent
														,DocumentNo
														,ApplicationDate
														,ConnectionDate
														,SetupDate
														,ActiveStatusId
														,IsBEDCEmployee
														,EmployeeCode
														,IsVIPCustomer
														,TenentId
														,OldAccountNo
														,CreatedDate
														,CreatedBy
														)
												values
														(
														@GlobalAccountNumber
														,@AccountNo
														,@TitleLandlord
														,@FirstNameLandlord
														,@MiddleNameLandlord
														,@LastNameLandlord
														,@KnownAs
														,@EmailIdLandlord
														,@HomeContactNumberLandlord
														,@BusinessPhoneNumberLandlord
														,@OtherPhoneNumberLandlord
														,@IsSameAsService 
														,@ServiceAddressID
														,@PostalAddressID
														,@IsSameAsTenent
														,@DocumentNo
														,@ApplicationDate
														,@ConnectionDate
														,@SetupDate
														,1
														,@IsBEDCEmployee
														,@EmployeeCode
														,@IsVIPCustomer
														,@TenentId
														,@OldAccountNo
														,@CreatedDate
														,@CreatedBy
												)
					--||***************************STEP 6****************************||--
					SET @StatusText='Customer Procedural Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerProceduralDetails(
														 GlobalAccountNumber
														,CustomerTypeId
														,AccountTypeId
														,PoleID
														,MeterNumber
														,TariffClassID
														,IsEmbassyCustomer
														,EmbassyCode
														,PhaseId
														,ReadCodeID
														,BookNo
														,ClusterCategoryId
														,RouteSequenceNumber
														,CreatedDate
														,CreatedBy
														,ActiveStatusID
														)
												VALUES	(
														@GlobalAccountNumber
														,@CustomerTypeId
														,@AccountTypeId
														,@PoleID
														,@MeterNumber
														,@TariffClassID
														,@IsEmbassyCustomer
														,@EmbassyCode
														,@PhaseId
														,@ReadCodeID
														,@BookNo
														,@ClusterCategoryId
														,@RouteSequenceNumber
														,@CreatedDate
														,@CreatedBy
														,1
														)
					--||***************************STEP 7****************************||--
					SET @StatusText='Customer identity Details';
					IF EXISTS(SELECT 0 FROM #IdentityList)
					BEGIN
						INSERT INTO CUSTOMERS.Tbl_CustomerIdentityDetails(
															IdentityTypeId
															,IdentityNumber
															,GlobalAccountNumber
															,CreatedDate
															,CreatedBy
															)
						SELECT								*
						FROM #IdentityList
					END
					--||***************************STEP 8****************************||--
					SET @StatusText='Application Process Details';
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessDetails(
														 GlobalAccountNumber
														,CertifiedBy
														,Seal1
														,Seal2
														,ApplicationProcessedBy
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@CertifiedBy
														,@Seal1
														,@Seal2
														,@ApplicationProcessedBy
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 9****************************||--
					SET @StatusText='Application Process Person Details';
					SET @Bedcinfoid=SCOPE_IDENTITY();--(SELECT TOP 1 Bedcinfoid FROM CUSTOMERS.Tbl_ApplicationProcessDetails ORDER BY Bedcinfoid DESC)
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessPersonDetails(
														GlobalAccountNumber
														,Bedcinfoid
														--,EmployeeName
														,InstalledBy
														,AgencyId
														,ContactName
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@Bedcinfoid
														--,@EmployeeName
														,@InstalledBy
														,@AgencyId
														,@ContactName
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 10****************************||--
					SET @StatusText='Customer Active Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerActiveDetails(
														GlobalAccountNumber
														,IsCAPMI
														,MeterAmount
														,InitialBillingKWh
														,PresentReading
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@IsCAPMI
														,@MeterAmount
														,@InitialBillingKWh
														,@PresentReading
														,@CreatedDate
														,@CreatedBy
														)
					--||***************************STEP 11****************************||--
					SET @StatusText='Document Details';
					if Exists(SELECT 0 FROM #DocumentList)
					BEGIN
						INSERT INTO Tbl_CustomerDocuments	
															(
															GlobalAccountNo
															,DocumentName
															,[Path]
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
															)
										SELECT				
															GlobalAccountNumber
															,value1
															,value2
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
										FROM  #DocumentList

				 						DROP TABLE #DocumentList
					 END
					--||***************************STEP 12****************************||--
					SET @StatusText='User Define List';
					if Exists(SELECT 0 FROM #UDFList)
					BEGIN
						INSERT INTO Tbl_USerDefinedValueDetails
															(
															AccountNo
															,UDCId
															,Value 
															)
										SELECT				
															GlobalAccountNumber
															,UDCId
															,Value
										FROM #UDFList

					 					
					 END
					 --||***************************STEP 13****************************||--
					SET @StatusText='Tbl_GovtCustomers';
					IF(@MGActTypeID > 0)
					BEGIN
						INSERT INTO Tbl_GovtCustomers
														(
														GlobalAccountNumber
														,MGActTypeID
														)
									values				(
														@GlobalAccountNumber
														,@MGActTypeID			
														)
					END
					 --||***************************STEP 14****************************||-- (Added By - Padmini(25th Feb 2015))
					SET @StatusText='Tbl_Paidmeterdetails';
					IF(@IsCAPMI=1)
					BEGIN
						INSERT INTO Tbl_PaidMeterDetails
														(
														AccountNo
														,MeterNo
														,MeterTypeId
														,MeterCost
														,ActiveStatusId
														,MeterAssignedDate
														,CreatedDate
														,CreatedBy
														)
									values
														(
														@GlobalAccountNumber
														,@MeterNumber
														,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)
														,@MeterAmount
														,1
														,@ConnectionDate
														,@CreatedDate
														,@CreatedBy
														)					
					END
				SET @StatusText	=''
			END
				ELSE
					BEGIN
						SET @GlobalAccountNumber = ''
						SET @IsSuccessful =0
						SET @StatusText='Problem in account number genrate.';
					END
			END
		ELSE
			BEGIN 
				SET @GlobalAccountNumber = ''
				SET @IsSuccessful =0
				SET @StatusText='Problem in global account number genrate.';
			END
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @GlobalAccountNumber = ''
	SET @IsSuccessful =0
END CATCH
END

SELECT 
	@IsSuccessful AS IsSuccessful
	,@StatusText AS StatusText 
	,@GlobalAccountNumber AS GolbalAccountNumber			
	FOR XML PATH('CustomerRegistrationBE'),TYPE
--===========================REGISTRATION ENDS===========================--
END
GO