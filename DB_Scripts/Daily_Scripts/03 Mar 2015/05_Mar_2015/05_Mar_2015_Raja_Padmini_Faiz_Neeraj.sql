GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerDescription CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerDescription CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum 
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  --,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading
				  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
				  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerDescription C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					--,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  ,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  --,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
-----------------------------------------------------------------------------------
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_DEMO]    Script Date: 03/05/2015 10:56:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  Suresh Kumar D          
-- Create date: 22-05-2014          
-- Description: The purpose of this procedure is to Calculate the Bill Generation        
-- ModifiedBy : T.Karthik      
-- ModifiedDate: 30-12-2014    
-- ModifiedBy : NEERAJ      
-- ModifiedDate: 28-FEB-2014        
-- DESC			: Billing rule condition for direct customers. approx line no. 317 to 362
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_DEMO]          
(          
@XmlDoc XML          
--@CycleId VARCHAR(50)    
--,@BillingQueuescheduleId INT  
)          
AS          
BEGIN    
 --BEGIN TRY  
 --BEGIN TRAN      
  DECLARE @AccountNumner VARCHAR(50)        
    ,@Month INT        
    ,@Year INT         
    ,@Date DATETIME         
    ,@MonthStartDate DATE     
    ,@PreviousReading VARCHAR(50)        
    ,@CurrentReading VARCHAR(50)        
    ,@Usage DECIMAL(20)        
    ,@RemaningUsage DECIMAL(20)        
    ,@TotalAmount DECIMAL(18,2)= 0        
    ,@Tax DECIMAL(18,2) = 5         
    ,@BillingQueuescheduleId INT        
    ,@PresentCharge INT        
    ,@FromUnit INT        
    ,@ToUnit INT        
    ,@Amount DECIMAL(18,2)    
    ,@TaxId INT        
    ,@CustomerBillId INT = 0        
    ,@BillGeneratedBY VARCHAR(50)        
    ,@LastDateOfBill DATETIME        
    ,@IsEstimatedRead BIT = 0        
    ,@ReadCodeId INT        
    ,@ReadType INT        
    ,@LastBillGenerated DATETIME      
    ,@FeederId VARCHAR(50)      
    ,@CycleId VARCHAR(50)      
    ,@BillNo VARCHAR(50)    
    ,@PrevCustomerBillId INT    
    ,@AdjustmentAmount DECIMAL(18,2)    
    ,@PreviousDueAmount DECIMAL(18,2)    
    ,@CustomerTariffId VARCHAR(50)    
    ,@BalanceUnits INT    
    ,@RemainingBalanceUnits INT    
    ,@IsHaveLatest BIT = 0    
    ,@ActiveStatusId INT    
    ,@IsDisabled BIT    
    ,@DisableType INT    
    ,@IsPartialBill BIT    
    ,@OutStandingAmount DECIMAL(18,2)    
    ,@IsActive BIT    
    ,@NoFixed BIT = 0    
    ,@NoBill BIT = 0     
    ,@ClusterCategoryId INT=NULL  
    ,@StatusText VARCHAR(50)    
    ,@BU_ID VARCHAR(50)  
    ,@BillingQueueScheduleIdList VARCHAR(MAX)  
    ,@RowsEffected INT
  DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     
        
   DECLARE @GeneratedCustomerBills TABLE(CustomerBillId INT)        
   DELETE FROM @GeneratedCustomerBills      
      
    SELECT    
   --@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')    
   @CycleId = C.value('(CycleId)[1]','VARCHAR(50)')   
   ,@BillingQueueScheduleIdList = C.value('(BillingQueueScheduleIdList)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)    
  
  --===========================Creating table for CycleID and BillingQueueSheduleID List=============================
  CREATE TABLE #inputList(  
         value1 varchar(MAX)  
         ,value2 varchar(MAX)  
         ,ID INT IDENTITY(1,1)  
           
       )    
   INSERT INTO #inputList(  
          value1  
          ,value2            
         )  
   select   
     Value1  
     ,Value2  
   from  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
   --===========================---------------------------------------------------------------=============================
   
 DECLARE @COUNTER INT=1  
 DECLARE @MaxInput INT  
 SET @MaxInput=(SELECT MAX(ID) FROM #inputList)  
  WHILE(@COUNTER<=@MaxInput) --loop
   BEGIN    
   SET @CycleId=(SELECT value1 FROM #inputList WHERE ID=@COUNTER)  
   SET @BillingQueueScheduleId=(SELECT value2 FROM #inputList WHERE ID=@COUNTER)  
   --BILL GEN LOGIC  
    
   --SELECT @BU_ID=BU_ID FROM Tbl_Cycles WHERE CycleId=@CycleId  
    SELECT @BU_ID=BU.BU_ID FROM
    Tbl_Cycles C 
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID   
   WHERE CycleId=@CycleId   
    --=======================Filtering customer data based on bu=============================  
    DECLARE @CustomerMaster TABLE(  
          GlobalAccountNumber VARCHAR(10)  
          ,TariffId INT  
          ,OutStandingAmount DECIMAL(18,2)  
          ,SortOrder INT  
          ,ActiveStatusId INT  
          ,MeterNumber  VARCHAR(50)  
          ,BU_ID VARCHAR(50)  
          ,SU_ID VARCHAR(50)  
          ,ServiceCenterId VARCHAR(50)  
          ,PoleId VARCHAR(50)  
          ,OldAccountNo VARCHAR(50)  
          ,BookNo VARCHAR(50)  
          )        
    INSERT INTO @CustomerMaster  
    (  
    GlobalAccountNumber  
    ,TariffId  
    ,OutStandingAmount  
    ,SortOrder  
    ,ActiveStatusId  
    ,MeterNumber  
    ,BU_ID  
    ,SU_ID  
    ,ServiceCenterId    
    ,PoleId  
    ,OldAccountNo  
    ,BookNo     
      
    )  
    SELECT GlobalAccountNumber  
    ,TariffId  
    ,OutStandingAmount  
    ,SortOrder  
    ,ActiveStatusId  
    ,MeterNumber  
    ,BU_ID  
    ,SU_ID  
    ,ServiceCenterId    
    ,PoleId  
    ,OldAccountNo  
    ,BookNo  
      
   FROM UDV_CustDetailsForBillGen  
   WHERE BU_ID=@BU_ID AND CycleId=@CycleId  
   --SELECT * FROM @CustomerMaster  
   -- =======================================================================================  
     
   DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
   DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
   --DECLARE @GeneratedCustomerBills TABLE(CustomerBillId INT)        
   --DELETE FROM @GeneratedCustomerBills        
           
   DELETE FROM @FilteredAccounts   
   INSERT INTO @FilteredAccounts        
   SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
   FROM Tbl_BillingQueeCustomers C WHERE C.BillingQueuescheduleId = @BillingQueuescheduleId    
   Order By AccountNo,[Month],[Year]    
    --C.BillGenarationStatusId = 5          
           
   SELECT TOP(1) @AccountNumner = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC        
   WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @AccountNumner ORDER BY AccountNo ASC))        
    BEGIN        
   SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0) FROM @CustomerMaster WHERE GlobalAccountNumber = @AccountNumner     
   SET @IsEstimatedRead = 0     
   SET @PrevCustomerBillId = 0        
   SET @AdjustmentAmount = 0    
                
   SELECT @Year = BillingYear,@Month = BillingMonth,@BillingQueuescheduleId = BillingQueuescheduleId FROM @FilteredAccounts WHERE AccountNo = @AccountNumner        
   SELECT @BillGeneratedBY = CreatedBy FROM Tbl_BillingQueueSchedule WHERE BillingQueueScheduleId = @BillingQueuescheduleId         
   SELECT TOP(1) @LastDateOfBill = CreatedDate FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner ORDER BY CreatedDate DESC        
   SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01')        
   SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(@MonthStartDate)))       
       
   --- Checking any Latest month bill already generated or not ?    
       
    SET @IsHaveLatest = 0   
    SET @StatusText ='IsHaveLatest Condition'   
    IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND CONVERT(DATE,(CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01')) > @MonthStartDate)    
     BEGIN    
   SET @IsHaveLatest = 1    
     END    
    ELSE    
   SET @IsHaveLatest = 0    
    -------- Checking CUstomer Book No Disable or not ?   START    
    SET @NoFixed  = 0    
    SET @NoBill = 0     
    SET @IsDisabled = 0    
    SET @DisableType = 0    
    SET @IsPartialBill = 0    
    SET @IsActive = 0    
          
    IF(@ActiveStatusId = 1)    
     BEGIN        
          
   SELECT     
    @IsDisabled = IsDisable, @DisableType = DisableTypeId    
    ,@IsPartialBill = IsPartialBill,@IsActive = IsActive    
   FROM dbo.fn_GetCustomerBookDisable_Details(@AccountNumner,@Month,@Year)    
          
   IF(@IsDisabled = 1)    
    BEGIN    
     IF(@IsActive = 1)    
      BEGIN    
    IF(@DisableType = 1)    
     SET @NoBill = 1    
    ELSE    
     SET @NoFixed = 1    
      END    
     ELSE    
      BEGIN    
    IF(@IsPartialBill = 1)    
     SET @NoFixed = 1    
      END    
    END    
     END    
    -------- Checking CUstomer Book No Disable or not ?   END    
       
       
   --IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND (CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01') > @MonthStartDate)    
       
   IF (@IsHaveLatest = 0)    
    BEGIN --- IF THE LATEST BILLS NOT EXISTS FOR THIS CUSTOMER    
   IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year AND BillingTypeId = 3 AND ReadType = 3)    
     BEGIN -- IF SPOT BILLING Not Happens in this months    
     IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year)    
    BEGIN    
     DECLARE @PrevBillId INT,@TotalBillAmountWithTax DECIMAL(20,4)=0     
     SELECT @BillNo = BillNo,@PrevBillId = CustomerBillId,@TotalBillAmountWithTax=TotalBillAmountWithTax    
      FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year    
                  
     Update Tbl_CustomerReadings SET IsBilled = 0 WHERE BillNo = @BillNo      
     DELETE FROM Tbl_CustomerBillPaymentsApproval WHERE BillNo = @BillNo    
     DELETE FROM Tbl_CustomerBillPayments WHERE BillNo  = @BillNo    
     DELETE FROM Tbl_BillAdjustmentDetails WHERE BillAdjustmentId IN(SELECT BillAdjustmentId FROM Tbl_BillAdjustments WHERE CustomerBillId = @PrevBillId)    
     DELETE FROM Tbl_BillAdjustments WHERE CustomerBillId = @PrevBillId    
     DELETE FROM Tbl_PaidMeterPaymentDetails WHERE BillNo = @BillNo    
     UPDATE Tbl_BillAdjustments  SET EffectedBillId = NULL WHERE EffectedBillId = @PrevBillId    
     DELETE FROM TBL_Customer_Additionalcharges WHERE CustomerBillId = @PrevBillId     
     DELETE FROM Tbl_CustomerBills WHERE CustomerBillId = @PrevBillId --AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year      
     --================================OLD STATEMENT FOR PREVIOUS BALANCE===================================================
     UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=OutStandingAmount-@TotalBillAmountWithTax WHERE GlobalAccountNumber=@AccountNumner-- {Modified Karthik}    
     --===================================================================================
     SELECT @OutStandingAmount = ISNULL(OutStandingAmount,0) FROM @CustomerMaster WHERE GlobalAccountNumber = @AccountNumner     
   
    END    
   IF(@NoBill = 0)    
    BEGIN    
      IF (@ActiveStatusId = 1) ---for Active customers energy units are calculated    
     BEGIN    
      IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date))--- For Readinf Customer which are have Readings        
		BEGIN         
			 DECLARE @MeterMultiplier INT    
		      
			 SELECT @MeterMultiplier=(SELECT MeterMultiplier FROM Tbl_MeterInformation    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD WHERE GlobalAccountNumber=@AccountNumner))    
		                      
			 --SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate FROM Tbl_CustomerBills    
			 --WHERE AccountNo = @AccountNumner     
			 --AND CONVERT(DATE,BillGeneratedDate) <= CONVERT(DATE,@Date)     
			 --ORDER BY BillGeneratedDate DESC    
		      
			 SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate FROM Tbl_CustomerBills    
			 WHERE AccountNo = @AccountNumner     
			 ORDER BY CustomerBillId DESC    
		               
			 SELECT TOP(1) @PreviousReading = PreviousReading FROM Tbl_CustomerReadings         
			 WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0     
			 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date)     
			 AND (CONVERT(DATE,ReadDate) >= CONVERT(DATE,@LastBillGenerated) OR ISNULL(@LastBillGenerated,'') = '')    
			 ORDER BY CONVERT(DECIMAL(18),CustomerReadingId) ASC        
		                  
			 SELECT TOP(1) @CurrentReading = PresentReading,@ReadType = ReadType FROM Tbl_CustomerReadings         
			 WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0     
			 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date)     
			 AND (CONVERT(DATE,ReadDate) >= CONVERT(DATE,@LastBillGenerated) OR ISNULL(@LastBillGenerated,'') = '')    
			 ORDER BY  CONVERT(DECIMAL(18),CustomerReadingId) DESC        
		                  
			 ---- NEW Code    
			 SELECT @Usage = SUM(Usage) FROM Tbl_CustomerReadings         
			 WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0     
			 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date)     
			 AND (CONVERT(DATE,ReadDate) >= CONVERT(DATE,@LastBillGenerated) OR ISNULL(@LastBillGenerated,'') = '')    
			 ---- NEW Code    
		      
			 SET @Usage=@Usage*@MeterMultiplier--Applying Multiply factor for Usage.    
		                  
			 --SELECT @Usage = CONVERT(DECIMAL,@CurrentReading) - CONVERT(DECIMAL,@PreviousReading)  --OLD      
			 SET @RemaningUsage = @Usage        
			 SET @TotalAmount = 0         
			 SET @ReadCodeId  = 2--if customer has reading                            
    END      
      ELSE    
		BEGIN        
     SET @IsEstimatedRead = 1        
     SET @Usage = 0    
     SET @PreviousReading = NULL    
     SET @CurrentReading = NULL                
               
     IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE ReadCodeId = 1 AND GlobalAccountNumber = @AccountNumner)--- For Direct Customers        
     BEGIN       
		  SET @ReadCodeId  = 3       
		  DECLARE @BillingRule INT; 
		  --Modified for avg reading from Tbl_DirectCustomersAvgReading --Neeraj 077
		  --Starts
		  SET @BillingRule=( SELECT      
							   BillingRule
									  FROM Tbl_EstimationSettings     
									  WHERE BillingMonth = @Month     
									  AND BillingYear = @Year     
									  AND TariffId = @CustomerTariffId     
									  AND CycleId = @CycleId     
									  AND ISNULL(ClusterCategoryId,0)=@ClusterCategoryId    
									  AND ActiveStatusId = 1 )
		  IF(@BillingRule=1)
		  BEGIN
			  SELECT @CustomerTariffId = TariffClassID, @ClusterCategoryId=ISNULL(ClusterCategoryId,0) FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber = @AccountNumner    
			  SELECT      
			   @Usage = EnergytoCalculate     
			  FROM Tbl_EstimationSettings     
			  WHERE BillingMonth = @Month     
			  AND BillingYear = @Year     
			  AND TariffId = @CustomerTariffId     
			  AND CycleId = @CycleId     
			  AND ISNULL(ClusterCategoryId,0)=@ClusterCategoryId    
			  AND ActiveStatusId = 1 AND BillingRule = 1      
          END   
          --Ends
          IF(@BillingRule=2)
		  BEGIN
			 SET @Usage=(SELECT TOP 1 AverageReading FROM Tbl_DirectCustomersAvgReadingS WHERE GlobalAccountNumber=@AccountNumner ORDER BY AvgReadingId DESC)     
          END   
		  IF(@Usage IS NULL OR @Usage = 0)    
			   BEGIN     
				 SELECT @Usage = ISNULL(InitialBillingKWh,0) FROM [CUSTOMERS].[Tbl_CustomerActiveDetails] WHERE GlobalAccountNumber = @AccountNumner             
				 SET @ReadCodeId  = 1        
			   END    
     END        
     ELSE IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date) )--- For Reading Customer who are not have Readings(Estimated Customers)        
     BEGIN     
     
     --Calculating Avg usage for non read customers
		 --starts 
		  DECLARE @TempBill TABLE (Usage DECIMAL(18,2))       
		  DELETE FROM @TempBill    
		  INSERT INTO @TempBill    
		  SELECT TOP(3) ISNULL(Usage,0) FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner     
		 AND CONVERT(DATE,(CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01')) < @MonthStartDate    
		 ORDER BY CustomerBillId DESC    
		  SELECT @Usage = SUM(Usage)/COUNT(0) FROM @TempBill    
		  --SELECT @Usage = (SELECT SUM(Usage) FROM Tbl_CustomerReadings WHERE AccountNumber = @AccountNumner AND IsBilled = 1 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date))    
		  --     /(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE AccountNumber = @AccountNumner AND IsBilled = 1 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date))        
		  --Ends       
	--If usage is zero than take reading from INKW.
		 --starts    
      IF(@Usage=0)
      BEGIN      
		  SET @Usage = (CASE WHEN @Usage IS NULL THEN (SELECT InitialBillingKWh FROM [CUSTOMERS].[Tbl_CustomerActiveDetails] where GlobalAccountNumber = @AccountNumner) ELSE @Usage END)     
		  SET @ReadCodeId  = 5              
      END
      --Ends
       
     END         
                  
    END     
     END    
            
    SELECT TOP(1) @PrevCustomerBillId  = CustomerBillId FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner ORDER BY CustomerBillId DESC    
    SELECT @AdjustmentAmount = TotalAmountEffected FROM Tbl_BillAdjustments WHERE AccountNo = @AccountNumner AND CustomerBillId = @PrevCustomerBillId    
     --  SELECT @AdjustmentAmount = ISNULL(@AdjustmentAmount,0) + dbo.fn_GetCustomerOverpayAmount(@AccountNumner)   --- Added Over Payment Details    
    SELECT @PreviousDueAmount = (CASE WHEN @OutStandingAmount = 0 THEN (dbo.fn_GetCustomerTotalDueAmount(@AccountNumner)) ELSE @OutStandingAmount END)    
    SELECT @BalanceUnits = ISNULL(BalanceUsage,0) FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND CustomerBillId = @PrevCustomerBillId    
    SET @BalanceUnits = ISNULL(@BalanceUnits,0)    
             
    -- This is for Estimation billing for read customers start    
     IF(@IsEstimatedRead = 1)    
      BEGIN    
    SET @RemainingBalanceUnits = @BalanceUnits + @Usage      
      END    
     ELSE    
      BEGIN    
    IF(@BalanceUnits > @Usage)    
     BEGIN    
      SET @Usage = 0    
      SET @RemainingBalanceUnits = @BalanceUnits - @Usage      
     END         
    ELSE    
     BEGIN    
      SET @Usage = @Usage - @BalanceUnits    
      SET @RemainingBalanceUnits = 0    
     END     
      END    
     -- This is for Estimation billing for read customers end    
             
    SELECT @TotalAmount = dbo.fn_CaluculateBill_Consumption(@AccountNumner,@Usage,@Month,@Year)    
              
    IF(@BalanceUnits > 0 AND @RemainingBalanceUnits >= 0 AND @Usage = 0 AND @ActiveStatusId = 1)    
      BEGIN    
    SET @ReadCodeId  = 4     
      END          
     IF (@ActiveStatusId = 2)--- for DeActive Customers Energy units are Zero    
     BEGIN    
      SET @PreviousReading = NULL       
      SET @CurrentReading    = NULL    
      SET @TotalAmount = 0    
     END       
           
     SET @StatusText ='Insert Tbl_CustomerBills'   
     INSERT INTO Tbl_CustomerBills([AccountNo]        
     --,[CustomerId]        
     ,[TotalBillAmount]        
     ,[ServiceAddress]        
     ,[MeterNo]        
     ,[Dials]        
     ,[NetArrears]        
     ,[NetEnergyCharges]        
     ,[NetFixedCharges]        
     ,[VAT]        
     ,[VATPercentage]        
     ,[Messages]        
     ,[BU_ID]        
     ,[SU_ID]        
     ,[ServiceCenterId]        
     --,[SubStationId]        
     --,[FeederId]        
     --,[TransFormerId]        
     ,[PoleId]        
     ,[BillGeneratedBy]        
     ,[BillGeneratedDate]        
     ,PaymentLastDate        
     ,[TariffId]        
     ,[BillYear]        
     ,[BillMonth]        
     ,[CycleId]        
     ,[TotalBillAmountWithArrears]        
     ,[ActiveStatusId]        
     ,[CreatedDate]        
     ,[CreatedBy]        
     ,[ModifedBy]        
     ,[ModifiedDate]        
     ,[BillNo]        
     ,PaymentStatusID        
     ,[PreviousReading]        
     ,[PresentReading]        
     ,[Usage]        
     ,[AverageReading]        
     ,[TotalBillAmountWithTax]        
     ,[EstimatedUsage]        
     ,[ReadCodeId]        
     ,[ReadType]    
     ,AdjustmentAmmount    
     ,BalanceUsage    
     ,BillingTypeId    
     )        
     SELECT GlobalAccountNumber    
      --,CustomerUniqueNo    
      ,0    
      ,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
      ,C.MeterNumber    
    ,(select mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials  --,M.MeterDials     
    ,0        
    ,@TotalAmount
    ,0
    ,0
    ,@Tax        
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
    ,BU_ID,SU_ID,ServiceCenterId    
    --,InjectionSubStationId        
    --,FeederId    
    --,TransformerId    
    ,PoleId        
    ,@BillGeneratedBY        
    ,(SELECT dbo.fn_GetCurrentDateTime())        
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
    ,TariffId,@Year,@Month,@CycleId
    ,0
    ,1        
    ,(SELECT dbo.fn_GetCurrentDateTime())        
    ,@BillGeneratedBY        
    ,NULL,NULL        
    ,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))        
    ,2    
    ,@PreviousReading        
    ,@CurrentReading        
    ,@Usage        
    ,0,0              
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)        
    ,@ReadCodeId,@ReadType        
    ,@AdjustmentAmount    
    ,@RemainingBalanceUnits    
    --,(CASE WHEN @ReadCodeId = 3 THEN (@RemainingBalanceUnits + @Usage) ELSE @RemainingBalanceUnits END)    
    ,2    
      FROM @CustomerMaster C        
      WHERE GlobalAccountNumber = @AccountNumner  
      
     
     -- AND C.ActiveStatusId IN(1,2) --AND M.ActiveStatusId = 1 --AND T.IsAcitive = 1        
                   
     SET @CustomerBillId = SCOPE_IDENTITY()         
               
     IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId )        
    BEGIN           
      IF(@NoFixed = 0)    
    BEGIN    
     DECLARE @PaidMeterBalance DECIMAL(18,2) = 0    
       ,@DiscountFixedCharges DECIMAL(18,2) = 0    
     SELECT @PaidMeterBalance = dbo.fn_GetPaidMeterCustomer_Balance(@AccountNumner)    
     IF(@PaidMeterBalance > 0)    
      BEGIN    
       DECLARE @ResultedTable TABLE(ChargeId INT,ChargeAmount DECIMAL(18,2),EffectedAmount DECIMAL(18,2)    
       --,CustomerUniqueNo VARCHAR(50)    
       ,AccountNo VARCHAR(50),ClassID INT)    
       DELETE FROM @ResultedTable     
       INSERT INTO @ResultedTable    
       SELECT     
     ChargeId,ChargeAmount,EffectedAmount    
     --,CustomerUniqueNo    
     ,AccountNo,ClassID    
       FROM dbo.fn_GetFixedCharges_PaidMeters(@AccountNumner,@MonthStartDate,@PaidMeterBalance)    
      
       INSERT INTO TBL_Customer_Additionalcharges(AccountNO,CustomerBillId,TariffId,Amount,CreatedBy,CreatedDate,ModifiedBY,ModifiedDate,ChargeId)         
       SELECT     
     --CustomerUniqueNo,    
     AccountNo,@CustomerBillId,ClassID,(A.ChargeAmount - A.EffectedAmount),NULL,GETDATE(),NULL,NULL,ChargeId     
       FROM @ResultedTable A        
                 
       SELECT @DiscountFixedCharges = SUM(EffectedAmount) FROM @ResultedTable    
                 
       INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)    
       SELECT     
     AccountNo,MeterNo    
     ,@DiscountFixedCharges    
     ,BillNo,@BillGeneratedBY,dbo.fn_GetCurrentDateTime()    
       FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId    
       UPDATE Tbl_CustomerBills SET AdjustmentAmmount = (ISNULL(AdjustmentAmmount,0)+ @DiscountFixedCharges) WHERE CustomerBillId = @CustomerBillId    
       UPDATE Tbl_PaidMeterDetails SET OutStandingAmount = (@PaidMeterBalance - @DiscountFixedCharges) WHERE AccountNo=@AccountNumner    
      END    
     ELSE    
      BEGIN               
       INSERT INTO TBL_Customer_Additionalcharges(AccountNO,CustomerBillId,TariffId,Amount,CreatedDate,ChargeId)         
       SELECT     
     --C.CustomerUniqueNo,    
     GlobalAccountNumber,@CustomerBillId,TariffId,A.Amount,GETDATE(),ChargeId     
       FROM Tbl_LAdditionalClassCharges A        
       JOIN @CustomerMaster C ON A.ClassID = C.TariffId        
       WHERE IsActive = 1 AND C.GlobalAccountNumber = @AccountNumner        
       AND ChargeId IN (SELECT ChargeId FROM Tbl_MChargeIds CI WHERE CI.IsAcitve = 1)    
       AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)      
      END    
    END            
      ---For Getting NetArrears        
      UPDATE Tbl_CustomerBills SET NetArrears = @PreviousDueAmount--(ISNULL(@PrevBillAmount,0) - ISNULL(@PaidAmount,0))     
       WHERE CustomerBillId = @CustomerBillId        
               
      ---For Getting NetFixedCharges         
      UPDATE Tbl_CustomerBills SET NetFixedCharges = (SELECT SUM(Amount) FROM TBL_Customer_Additionalcharges WHERE CustomerBillId = @CustomerBillId) WHERE CustomerBillId = @CustomerBillId        
      ---For Getting TotalBillAmount         
      UPDATE Tbl_CustomerBills SET TotalBillAmount = (ISNULL(NetEnergyCharges,0) + ISNULL(NetFixedCharges,0)) WHERE CustomerBillId = @CustomerBillId        
             
      DECLARE @IsRequireTax BIT=1    
             
      IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNumner AND IsEmbassyCustomer=1)    
      BEGIN            
    IF((SELECT IsEmbassyHaveTax FROM Tbl_CompanyDetails WHERE CompanyId=1)=0)    
     SET @IsRequireTax=0    
      END    
             
      ---For Getting VAT        
      IF(@IsRequireTax=1)    
    UPDATE Tbl_CustomerBills SET VAT = ((5*TotalBillAmount)/100) WHERE CustomerBillId = @CustomerBillId        
      ---For Getting TotalBillAmountWithTax         
      UPDATE Tbl_CustomerBills SET TotalBillAmountWithTax = (TotalBillAmount + VAT) WHERE CustomerBillId = @CustomerBillId        
      ---For Getting TotalBillAmountWithArrears         
      
      
       
       DECLARE @TotalBillWithArr DECIMAL(18,2)
       SELECT @TotalBillWithArr=(TotalBillAmountWithTax + NetArrears)FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
      UPDATE Tbl_CustomerBills SET TotalBillAmountWithArrears = @TotalBillWithArr WHERE CustomerBillId = @CustomerBillId         
       
       ----- Updating customer out standing amount for 1st time
       UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=@TotalBillWithArr WHERE GlobalAccountNumber=@AccountNumner      
      ---For Getting Average Reading        
      UPDATE Tbl_CustomerBills SET AverageReading = (SELECT SUM(B.Usage) FROM Tbl_CustomerBills B WHERE AccountNo = @AccountNumner)/(SELECT COUNT(0) FROM Tbl_CustomerBills B WHERE AccountNo = @AccountNumner)        
    WHERE CustomerBillId = @CustomerBillId        
                
     IF(@AdjustmentAmount IS NOT NULL)    
      BEGIN    
    UPDATE Tbl_BillAdjustments SET EffectedBillId = @CustomerBillId WHERE CustomerBillId = @PrevCustomerBillId AND  AccountNo = @AccountNumner     
      END    
             
      ------For Getting Adjustment Values    
      --UPDATE Tbl_CustomerBills SET AdjustmentAmmount = @AdjustmentAmount WHERE CustomerBillId = @CustomerBillId    
              
      ---If The Customer having Balance amount then the bill status is     
      IF((SELECT ISNULL(AdjustmentAmmount,0) - (ISNULL(NetArrears,0) + ISNULL(TotalBillAmountWithTax,0)) FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId) >= 0)    
    BEGIN    
     UPDATE Tbl_CustomerBills SET PaymentStatusID = 1 WHERE CustomerBillId = @CustomerBillId    
    END        
              
      ---For Update IsBilled Status For Bill Generation Customers        
     Update Tbl_CustomerReadings SET IsBilled = 1     
        ,BillNo = (SELECT BillNo FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId)    
        ,BilledDate = (SELECT dbo.fn_GetCurrentDateTime())    
        ,BilledBy = @BillGeneratedBY     
        ,UsageForBilling = @Usage        
     WHERE GlobalAccountNumber = @AccountNumner         
     AND CONVERT(DECIMAL,PreviousReading) >= CONVERT(DECIMAL,@PreviousReading)        
     AND CONVERT(DECIMAL,PresentReading) <= CONVERT(DECIMAL,@CurrentReading)         
             
      Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 3     
     WHERE AccountNo = @AccountNumner AND BillingQueuescheduleId = @BillingQueuescheduleId-- BillGenarationStatusId = 5        
               
     ---- Update  Highest Consumption Value for Customer  
     IF ((SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@AccountNumner)>=2 )
     BEGIN
		 Update [CUSTOMERS].[Tbl_CustomerActiveDetails]     
		 SET Highestconsumption = (dbo.fn_GetCustomerHighConsumption(@AccountNumner))    
		 ,OutStandingAmount = (SELECT (TotalBillAmountWithArrears-(AdjustmentAmmount-@DiscountFixedCharges)) FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId)    
		 WHERE GlobalAccountNumber = @AccountNumner            
     END     
  --    --================================OLD STATEMENT FOR PREVIOUS BALANCE=============================NEERAJ ID077 19-FEB-15======================
  --    DECLARE @LastBill VARCHAR(50)
		--		,@PreviousBillAmount DECIMAL(18,2)=0.00
		--SELECT TOP 1 @LastBill=CustomerbillID FROM Tbl_CustomerBills 
		--WHERE AccountNo=@AccountNumner
		--ORDER BY BILLGENERATEDDATE
		--IF(@LastBill!=0.00)
		--BEGIN	
		--SELECT @PreviousBillAmount= TotalBillAmountWithTax FROM Tbl_CustomerBills 
		--WHERE CustomerBillId=@LastBill
		--UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=@PreviousBillAmount WHERE GlobalAccountNumber=@AccountNumner-- {Modified Karthik}    
	
		--	--UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=-@TotalBillAmountWithTax WHERE GlobalAccountNumber=@AccountNumner
		--END
     
  --   --===================================================================================
               
               
               
      INSERT INTO @GeneratedCustomerBills(CustomerBillId)VALUES(@CustomerBillId)         
    END        
     END        
     END        
    END    
     IF(@AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
    BREAK        
     ELSE        
    BEGIN        
   SET @AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @AccountNumner ORDER BY AccountNo ASC)        
   IF(@AccountNumner IS NULL) break;        
     Continue        
    END        
    END         
    SET @COUNTER=@COUNTER+1  
   END  
  SET @RowsEffected=1
   
   --=================================Follwoing select statement required for bill prit==============================================
   --SELECT         
   --(        
    SELECT         
     CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     --,(CD.Name +SPACE(1)+ ISNULL(SurName,'')) AS Name     
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name      
     ,CB.CustomerId        
     ,CB.TotalBillAmount        
     --,(CASE WHEN CB.ServiceAddress IS NULL THEN 'N/A' ELSE CB.ServiceAddress END)AS ServiceAddress
     ,(dbo.fn_GetCustomerServiceAddress(CB.AccountNo)) AS ServiceAddress
     ,(CASE WHEN CB.MeterNo IS NULL THEN 'N/A' ELSE CB.MeterNo END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,(CB.NetArrears-CB.AdjustmentAmmount) AS NetArrears    -- Modified By Padmini
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN 0.00 ELSE CB.NetEnergyCharges END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN 0.00 ELSE CB.NetFixedCharges END)AS NetFixedCharges
     ,CB.VAT        
     ,CB.VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(10),CB.PaymentLastDate) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(10),CB.PaymentLastDate) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,CB.TotalBillAmountWithArrears        
     ,(CASE WHEN CB.PreviousReading IS NULL THEN 'N/A' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN 'N/A' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,CB.TotalBillAmountWithTax         
     ,B.BusinessUnitName        
     ,PAD.HouseNo AS ServiceHouseNo    
     ,PAD.StreetName AS ServiceStreet        
     ,PAD.City AS ServiceCity        
     ,PAD.ZipCode AS ServiceZipCode          
     ,CB.TariffId        
     ,CB.AdjustmentAmmount    
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = CB.TariffId AND IsActiveClass = 1) AS TariffName
     --,(SELECT ClassName FROM Tbl_CustomerBills CB JOIN Tbl_MTariffClasses TC ON CB.TariffId=TC.ClassID
     --  WHERE AccountNo=CB.AccountNo)AS TariffName
     ,ISNULL(CD.OldAccountNo,'----') AS OldAccountNo    
     ,CASE WHEN ((SELECT TOP(1) CONVERT(VARCHAR(30),ReadDate,103) FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC)) IS NULL THEN 'N/A' ELSE (SELECT TOP(1) CONVERT(VARCHAR(30),ReadDate,103) FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC) END  AS ReadDate        
     ,CASE WHEN ((SELECT TOP(1) Multiplier FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC)) IS NULL THEN 0 ELSE (SELECT TOP(1) Multiplier FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC) END  AS Multiplier         
     ,CASE WHEN ((SELECT TOP(1) CONVERT(VARCHAR(30),CreatedDate,103) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC))IS NULL THEN 'N/A' ELSE (SELECT TOP(1) CONVERT(VARCHAR(30),CreatedDate,103) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC)END AS LastPaymentDate        
     ,CASE WHEN ((SELECT TOP(1 )CONVERT(DECIMAL(18,2),PaidAmount) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC)) IS NULL THEN 0 ELSE ((SELECT TOP(1 )CONVERT(DECIMAL(18,2),PaidAmount) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC))END AS LastPaidAmount        
     --,CD.OutStandingAmount AS PreviousBalance 
     ,[dbo].[fn_GetCustomerLastBillAmountWithArrears](cb.AccountNo,cb.BillMonth,cb.BillYear) as PreviousBalance             
     --,(SELECT TOP(1 )CONVERT(DECIMAL(18,2),NetArrears) FROM Tbl_CustomerBills B WHERE B.AccountNo = CB.AccountNo AND B.BillNo < CB.BillNo ORDER BY CreatedDate DESC)AS PreviousBalance        
     ,C.CycleId
     ,1 AS RowsEffected
     ,CASE WHEN(SELECT TOP 1  dbo.fn_PaymentSumAfterLastBill(CD.GlobalAccountNumber,TBCB.BillMonth,TBCB.BillYear,CB.BillMonth,CB.BillYear))IS NULL THEN 0.00 ELSE (SELECT TOP 1 dbo.fn_PaymentSumAfterLastBill(CD.GlobalAccountNumber,TBCB.BillMonth,TBCB.BillYear,CB.BillMonth,CB.BillYear)) END AS TotalPayment
     --,'1005' AS PostalHouseNo        
     --,'Madhapur' AS PostalStreet        
     --,'500008' AS PostalZipCode
     ,(SELECT TOP 1 TCPA.ZipCode FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails AS TCPA WHERE TCPA.GlobalAccountNumber=CB.AccountNo AND IsServiceAddress=0 AND IsActive=1) AS PostalZipCode
     ,[dbo].[fn_GetCustomerPostalAddress](CB.AccountNo) AS PostalAddress 
     ,dbo.fn_GetCustomerTotalDueAmount(CB.AccountNo) AS TotalDueAmount 
    FROM Tbl_CustomerBills CB        
    JOIN @CustomerMaster CD ON CB.AccountNo = CD.GlobalAccountNumber        
    LEFT JOIN Tbl_BookNumbers BC ON CD.BookNo = BC.BookNo    
    LEFT JOIN Tbl_Cycles C ON BC.CycleId = C.CycleId    
    LEFT JOIN Tbl_BussinessUnits B ON CB.BU_ID = B.BU_ID    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD ON PAD.GlobalAccountNumber=CD.GlobalAccountNumber  
     LEFT JOIN Tbl_CustomerBills AS TBCB ON TBCB.AccountNo=CD.GlobalAccountNumber AND TBCB.BillMonth =DATEPART(MONTH, DATEADD(MONTH,-1,CONVERT(DATE,'2014-'+convert(VARCHAR(2),CB.BillMonth)+'-01')))  
    --LEFT JOIN Tbl_MeterInformation MTR ON CD.MeterNumber=MTR.MeterNo
    WHERE CB.CustomerBillId IN (SELECT CustomerBillId FROM @GeneratedCustomerBills) 
    Order by C.CycleName -- Modified because we are getting cyclename in view also    
    ,BC.SortOrder,CD.SortOrder ASC    
   -- FOR XML PATH('BillGenerationList'),TYPE        
   --)        
   --FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
   DROP TABLE #inputList     
--   COMMIT TRAN   
     
--END TRY   
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @RowsEffected=0
--END CATCH   

--SELECT @RowsEffected AS RowsEffected
--FOR XML PATH('BillGenerationBe'),TYPE
END    
GO
GO
INSERT INTO Tbl_PasswordStrength (StrengthName,MinLength,MaxLength,IsActive)
VALUES('No.of_CapitalLetters','1','6',1)
GO
INSERT INTO Tbl_PasswordStrength (StrengthName,MinLength,MaxLength,IsActive)
VALUES('No.of_SmallLetters','1','6',1)
GO
INSERT INTO Tbl_PasswordStrength (StrengthName,MinLength,MaxLength,IsActive)
VALUES('No.of_SpecialCharacters','1','6',1)
GO
INSERT INTO Tbl_PasswordStrength (StrengthName,MinLength,MaxLength,IsActive)
VALUES('No.of_NumericValues','1','6',1)
GO
INSERT INTO Tbl_PasswordStrength (StrengthName,MinLength,MaxLength,IsActive)
VALUES('PasswordLength','4','6',1)
GO
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- =============================================  
CREATE PROCEDURE [MASTERS].[USP_AssignMeter]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@ReadCodeID VARCHAR(50) 
		,@GlobalAccountNumber VARCHAR(50) 
		,@IsSuccessful BIT =1
		,@StatusText VARCHAR(200)

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	SET @StatusText='Deserialization'
	 SELECT  
	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')  
	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
	SET @StatusText='Update Statement'
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
	SET MeterNumber=@MeterNumber
		,ReadCodeID=@ReadCodeID
	WHERE GlobalAccountNumber = @GlobalAccountNumber
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('CustomerRegistrationBE'),TYPE

END  
------------------------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2014  
-- Description: The purpose of this procedure is to get list of MeterInformation  
-- =============================================  
CREATE PROCEDURE [MASTERS].[USP_IsMeterAvailable]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @MeterNo VARCHAR(100)  
   --,@MeterSerialNo VARCHAR(100)  
   ,@BUID VARCHAR(50)  
   ,@CustomerTypeId INT  
     
  SELECT     
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')  
   --,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')  
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  IF (@CustomerTypeId=3)  
   BEGIN  
    SELECT  
    (  
     SELECT  
       COUNT(0) AS EffectedRows
      FROM Tbl_MeterInformation AS M  
      WHERE M.ActiveStatusId IN(1,2)  
      AND (BU_ID=@BUID OR @BUID='')  
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails   
       WHERE MeterNumber !='' )  
      AND M.MeterType=1  
      AND M.MeterNo =@MeterNo   
      --AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'  
     FOR XML PATH('MastersBE'),TYPE  
    )  
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
   END  
  ELSE  
   BEGIN  
    SELECT  
    (  
     SELECT  
      COUNT(0) AS EffectedRows
      FROM Tbl_MeterInformation AS M  
      WHERE M.ActiveStatusId IN(1,2)  
      AND (BU_ID=@BUID OR @BUID='')  
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails   
       WHERE MeterNumber !='' )  
      AND M.MeterType !=1  
      AND M.MeterNo =@MeterNo   
      --AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'  
     FOR XML PATH('MastersBE'),TYPE  
    )  
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
   END  
     
END  
---------------------------------------------------------------------------------------------------------------
GO
 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 5-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- =============================================                    
 CREATE PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID                 
				  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo       
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNo    
				  ,A.OldAccountNo                 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                                       
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                                          
				  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful                  
		  FROM [UDV_CustomerDescription] AS A      
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END       
-------------------------------------------------------------------------------------------------------------------
GO
GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Assign Meter','../ConsumerManagement/AssignMeters.aspx',1,17) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================                  
 -- Author  : NEERAJ KANOJIYA                
 -- Create date  : 7 OCT 2014                  
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT #        
 -- Modified BY : Suresh Kumar Dasi 
 -- Modified By : Padmini
 -- Modified Date : 25-12-2014    
 -- =============================================                  
 ALTER PROCEDURE [USP_GetBillAdjustmentDetails]                  
 (                  
 @XmlDoc xml                  
 )                  
 AS                  
 BEGIN                  
  DECLARE	@SearchValue VARCHAR(50)
			,@AccountNo VARCHAR(20)                 
			,@BillNo VARCHAR(20)            
		--	,@OldAccountNo VARCHAR(20)            
		--	,@MeterNo VARCHAR(20)            
		--	,@CustomerBillId INT      
  SELECT       
	@SearchValue = C.value('(SearchValue)[1]','VARCHAR(50)')                                     
   --@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)'),          
   --@BillNo = C.value('(BillNo)[1]','VARCHAR(50)'),                          
   --@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(20)'),                          
   --@MeterNo = C.value('(MeterNo)[1]','VARCHAR(20)')                          
  FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
   
    
  --IF(@AccountNo = '' OR @AccountNo IS NULL)              
  -- BEGIN          
  -- SELECT TOP 1 @AccountNo = AccountNo FROM Tbl_CustomerBills WHERE BillNo=@BillNo OR MeterNo=@MeterNo    
  -- IF(@AccountNo = '' OR @AccountNo IS NULL)         
  --  SELECT @AccountNo=AccountNo FROM Tbl_CustomerDetails WHERE OldAccountNo=@OldAccountNo  
  -- END   
  --IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo=@SearchValue) 
	 -- BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.AccountNo)
		--				FROM Tbl_CustomerDetails AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.AccountNo=CB.AccountNo
		--				WHERE CD.OldAccountNo=@SearchValue OR CD.MeterNo=@SearchValue OR CB.BillNo=@SearchValue)					
	 -- END
    IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@SearchValue )
	  BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.GlobalAccountNumber)
		--				FROM [CUSTOMERS].[Tbl_CustomerSDetail] AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
		--				JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber 			
		--				WHERE CD.OldAccountNo=@SearchValue OR CPD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue)					
		
		SET @AccountNo= (SELECT GlobalAccountNumber 
							FROM UDV_CustomerDescription CD
							LEFT JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
							WHERE (CD.OldAccountNo=@SearchValue OR CD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue))
		
	  END
  ELSE
	SET @AccountNo=@SearchValue
            
  SELECT                
  (                
  --Get customers details by account #                
  SELECT                
   A.GlobalAccountNumber AS CustomerID               
  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name             
  ,A.GlobalAccountNumber AS AccountNo              
  ,A.DocumentNo       
  ,A.MeterNumber AS MeterNo  
  ,A.OldAccountNo               
  ,A.RouteSequenceNo AS RouteSequenceNumber                 
  ,B.BusinessUnitName                  
  ,D.ServiceUnitName                  
  ,C.ServiceCenterName                  
  ,ReadCodeId                
  ,A.TariffId AS ClassID                
  ,E.ClassName               
  ,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings                  
  ,A.OutStandingAmount AS TotalDueAmount --Commented by Raja-ID065    
  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount --Commented by Raja-ID065    
  --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS TotalDueAmount --Raja-ID065    
  ,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate                  
  ,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount              
  ,dbo.fn_GetCustomerServiceAddress(GlobalAccountNumber) AS ServiceAddress              
  ,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate       
  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials                   
    FROM [UDV_CustomerDescription] AS A                 
    JOIN Tbl_BussinessUnits  AS B ON A.BU_ID = B.BU_ID                
    JOIN Tbl_ServiceCenter   AS C ON A.ServiceCenterId=C.ServiceCenterId                
    JOIN Tbl_ServiceUnits    AS D ON A.SU_ID=D.SU_ID                
    JOIN Tbl_MTariffClasses AS E ON A.TariffId = E.ClassID                   
    WHERE GlobalAccountNumber = @AccountNo                     
    FOR XML PATH('Customerdetails'),TYPE                  
 )              
 ,                
 (                
  --Get customers bill details by account #                
  SELECT TOP(3)                
   A.CustomerBillId AS CustomerBillID,                
  (SELECT BillAdjustmentId FROM Tbl_BillAdjustments AS B WHERE B.CustomerBillId = A.CustomerBillID) AS BAID,              
   A.CustomerId,                
   CONVERT(DECIMAL(18,2),A.Vat) AS Vat,              
   A.BillNo,              
   A.AccountNo,                        
   A.BillNo,                 
   A.PreviousReading AS PreviousReading,                  
   A.PresentReading AS PresentReading,                  
   A.Usage AS Consumption,                
   A.BillYear,                
   A.BillMonth,       A.ReadCodeId  
   ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(A.BillYear,A.BillMonth, A.AccountNo)) AS IsActiveMonth, --Raja-ID065 For getting month locked or not    
   (SELECT dbo.fn_IsCustomerBill_Latest(A.CustomerBillId,A.AccountNo)) AS IsSuccess, --For Checking Is this Bill Latest or NOT ?  
   A.NetEnergyCharges AS NetEnergyCharges,        
   --(SELECT dbo.fn_IsCustomerBilledMonthLocked(CONVERT(int, A.BillYear),CONVERT(int, A.BillMonth), A.AccountNo)) AS IsActiveMonth ,      
   --dbo.fn_GetBillPayments(A.CustomerBillId) AS TotaPayments,          
   dbo.fn_GetTotalPaidAmountOfBill(A.BillNo) AS TotaPayments,          
   --dbo.fn_GetDueBill(A.BillNo) AS DueBill,  --Commented Bu Raja-ID065    
   --CONVERT(DECIMAL(20,2),A.TotalBillAmountWithTax - dbo.fn_GetBillPayments(A.CustomerBillId)) AS DueBill, --Commented By Raja-ID065    
   (SELECT dbo.fn_IsBillAdjusted(A.CustomerBillId))AS IsAdjested,    
   (SELECT ApprovalStatusId FROM Tbl_BillAdjustments WHERE CustomerBillId = A.CustomerBillId) AS ApprovalStatusId,              
   CONVERT(DECIMAL(20,2),TotalBillAmount) AS TotalBillAmount,    
   (CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal, -- Raja-ID065 For GrandTotal    
   ((CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount))-ISNULL(dbo.fn_GetTotalPaidAmountOfBill(A.BillNo),0)) AS DueBill,-- Raja-ID065 For Due Bill    
   --(SELECT ReadCodeId FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS ReadCode,   
   --(SELECT OldAccountNo FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS OldAccountNo,
   (SELECT ReadCodeId FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber =@AccountNo) AS ReadCode,   
   (SELECT OldAccountNo FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber =@AccountNo) AS OldAccountNo,                               
   (SELECT top(1) CustomerReadingId FROM Tbl_CustomerReadings              
    WHERE GlobalAccountNumber=@AccountNo AND               
   ISBILLED=1               
    ORDER BY CustomerReadingId DESC) AS CustomerReadingId              
 --     A.CustomerReadingId                
   FROM Tbl_CustomerBills  AS A                       
  WHERE AccountNo = @AccountNo   
  AND BillNo = (CASE WHEN ISNULL(@BillNo,'') = '' THEN BillNo ELSE @BillNo END)   
  AND PaymentStatusID = 2                
  ORDER BY CustomerBillID DESC                      
  FOR XML PATH('CustomerBillDetails'),TYPE                  
 )                
 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                  
 END     
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerDescription CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerDescription CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum 
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  --,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading
				  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
				  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerDescription C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					--,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  ,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  --,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
-----------------------------------------------------------------------------------
GO
   
    