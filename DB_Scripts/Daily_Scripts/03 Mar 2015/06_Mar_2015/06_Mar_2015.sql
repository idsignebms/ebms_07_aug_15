GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsList]    Script Date: 03/04/2015 20:45:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 11-08-2014   
-- Modified date: 28-10-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get customer  Details By AccountNo,MeterNo  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsList]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
  ,@OldAccountNo VARCHAR(50)  
  ,@MeterNo VARCHAR(50)  
  ,@BUID VARCHAR(50)=''  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
  ,@OldAccountNo=C.value('(OldAccountNo)[1]','VARCHAR(50)')    
  ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')    
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)    
     
 IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE (BU_ID=@BUID OR @BUID='')  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
 BEGIN  
   SELECT    
      GlobalAccountNumber AS AccountNo  
     ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name  
     ,(SELECT dbo.fn_GetCustomerAddress(GlobalAccountNumber)) AS ServiceAddress  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber)),'--') AS LastBillDate  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber)),'--') As LastPaidDate  
     ,(SELECT dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber)) AS DueAmount  
     ,TariffId AS ClassID  
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff  
     ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CD.ClusterCategoryId) AS ClusterType
     ,(SELECT ClusterCategoryId FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CD.ClusterCategoryId) AS ClusterTypeId
     ,1 AS IsSuccess  
   FROM [UDV_CustomerDescription] AS CD    
   WHERE (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='')    
   AND (BU_ID=@BUID OR @BUID='')  
   FOR XML PATH('TariffManagementBE')    
 END  
 ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE BU_ID!=@BUID  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
  BEGIN  
   SELECT 1 AS IsCustExistsInOtherBU,1 AS IsSuccess FOR XML PATH('TariffManagementBE')   
  END  
 ELSE  
  SELECT 0 AS IsSuccess FOR XML PATH('TariffManagementBE')   
END   
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsAccountNoExists_BU_Id]    Script Date: 03/06/2015 09:32:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
ALTER FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF NOT EXISTS(SELECT 0 FROM [UDV_CustomerDescription] 
                  WHERE (GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo ) AND ActiveStatusId=1 AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/06/2015 09:21:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')                       
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
    
    
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	 ,TempDetails.AccountNo  
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(TempDetails.AccountNo,TempDetails.ReceivedDate,TempDetails.AmountPaid) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/06/2015 11:24:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')                       
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
    
    
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	, CD.GlobalAccountNumber AS AccountNo
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT CD.GlobalAccountNumber FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=CD.GlobalAccountNumber) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
   INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END

GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllAccNoInfoXLMeterReadings]    Script Date: 03/06/2015 16:02:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	

;With FinalResult AS  
(  
SELECT     
	T1.AccountNo as AccNum
	,T1.MinReading
	,(SELECT ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=T1.AccountNo OR OldAccountNo=T1.AccountNo) AS ActiveStatusId
 FROM @TempCustomer T1   
 LEFT JOIN( SELECT  C.AccountNo AS AccNum
	,ActiveStatusId    
    FROM [CUSTOMERS].[Tbl_CustomerSDetail] C     
     ) B ON T1.AccountNo=B.AccNum    
)  
  
 SELECT  
  (       
  SELECT   
        AccNum
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
END
GO