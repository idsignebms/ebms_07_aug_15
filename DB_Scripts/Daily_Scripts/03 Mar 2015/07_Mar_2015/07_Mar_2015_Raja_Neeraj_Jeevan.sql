GO
 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 5-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID                 
				  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo       
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNo    
				  ,A.OldAccountNo                 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID        
				  ,A.MeterNumber                                  
				  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful                  
		  FROM [UDV_CustomerDescription] AS A    
		  JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END       
-------------------------------------------------------------------------------------------------------------------
GO
CREATE TABLE TBL_ReadToDirectCustomerActivity
											(
												RDID INT PRIMARY KEY IDENTITY
												,GlobalAccountNo VARCHAR(50)
												,MeterNumber VARCHAR(50)
												,IsLatest BIT DEFAULT 1
												,CreatedBy VARCHAR(50)
												,CreatedDate DATETIME
											)
-------------------------------------------------------------------------------------------------------------------			
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- =============================================  
CREATE PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy DATETIME
		,@StatusText VARCHAR(200)
		,@IsSuccessful BIT=1

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	SET @StatusText='Deserialization'
	
	 SELECT  
	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','DATETIME')  
	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
	SET @StatusText='Update Statement'
	
	UPDATE TBL_ReadToDirectCustomerActivity 
	SET IsLatest=0
	
	SET @StatusText='Insert Statement'
	INSERT INTO TBL_ReadToDirectCustomerActivity
												(
												GlobalAccountNo
												,MeterNumber 
												,CreatedBy 
												,CreatedDate 												
												)
										VALUES
												(
												@GlobalAccountNumber
												,@MeterNumber
												,@CreatedBy
												,DBO.fn_GetCurrentDateTime()
												)
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('CustomerRegistrationBE'),TYPE

END  							
---------------------------------------------------------------------------------------------------------------

GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Chnage Read To Direct','../ConsumerManagement/ReadToDirect.aspx',1,17) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO

-------------------------------------------------------------------------------------------------------------------
GO
 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 5-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID                 
				  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS FirstNameLandlord               
				  ,A.GlobalAccountNumber AS AccountNo       
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNo    
				  ,A.OldAccountNo                 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                                       
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                                          
				  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful                  
		  FROM [UDV_CustomerDescription] AS A      
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END       
-------------------------------------------------------------------------------------------------------------------
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsList]    Script Date: 03/04/2015 20:45:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 11-08-2014   
-- Modified date: 28-10-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get customer  Details By AccountNo,MeterNo  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsList]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
  ,@OldAccountNo VARCHAR(50)  
  ,@MeterNo VARCHAR(50)  
  ,@BUID VARCHAR(50)=''  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
  ,@OldAccountNo=C.value('(OldAccountNo)[1]','VARCHAR(50)')    
  ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')    
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)    
     
 IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE (BU_ID=@BUID OR @BUID='')  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
 BEGIN  
   SELECT    
      GlobalAccountNumber AS AccountNo  
     ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name  
     ,(SELECT dbo.fn_GetCustomerAddress(GlobalAccountNumber)) AS ServiceAddress  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber)),'--') AS LastBillDate  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber)),'--') As LastPaidDate  
     ,(SELECT dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber)) AS DueAmount  
     ,TariffId AS ClassID  
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff  
     ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CD.ClusterCategoryId) AS ClusterType
     ,(SELECT ClusterCategoryId FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CD.ClusterCategoryId) AS ClusterTypeId
     ,1 AS IsSuccess  
   FROM [UDV_CustomerDescription] AS CD    
   WHERE (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='')    
   AND (BU_ID=@BUID OR @BUID='')  
   FOR XML PATH('TariffManagementBE')    
 END  
 ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE BU_ID!=@BUID  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
  BEGIN  
   SELECT 1 AS IsCustExistsInOtherBU,1 AS IsSuccess FOR XML PATH('TariffManagementBE')   
  END  
 ELSE  
  SELECT 0 AS IsSuccess FOR XML PATH('TariffManagementBE')   
END   
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsAccountNoExists_BU_Id]    Script Date: 03/06/2015 09:32:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
ALTER FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF NOT EXISTS(SELECT 0 FROM [UDV_CustomerDescription] 
                  WHERE (GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo ) 
                  AND ActiveStatusId=1 AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/06/2015 09:21:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')                       
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
    
    
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	 ,TempDetails.AccountNo  
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(TempDetails.AccountNo,TempDetails.ReceivedDate,TempDetails.AmountPaid) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/06/2015 11:24:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')                       
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
    
    
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	, CD.GlobalAccountNumber AS AccountNo
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT CD.GlobalAccountNumber FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=CD.GlobalAccountNumber) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
   INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END

GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllAccNoInfoXLMeterReadings]    Script Date: 03/06/2015 16:02:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	

;With FinalResult AS  
(  
SELECT     
	T1.AccountNo as AccNum
	,T1.MinReading
	,(SELECT ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=T1.AccountNo OR OldAccountNo=T1.AccountNo) AS ActiveStatusId
 FROM @TempCustomer T1   
 LEFT JOIN( SELECT  C.AccountNo AS AccNum
	,ActiveStatusId    
    FROM [CUSTOMERS].[Tbl_CustomerSDetail] C     
     ) B ON T1.AccountNo=B.AccNum    
)  
  
 SELECT  
  (       
  SELECT   
        AccNum
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
END
GO
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs
ADD MeterChangedDate DATETIME
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				 ,PresentReading AS PreviousReading				 
				 ,AverageReading As AverageUsage
				 ,BT.BillingType AS LastReadType
				 ,CONVERT(VARCHAR(20),ReadDate,105) AS PreviousReadingDate				 
			  FROM UDV_CustomerDescription  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 ,@OldMeterNo VARCHAR(50)
				 ,@InitialBillingkWh INT
				 ,@OldMeterReadingDate DATETIME
				 ,@NewMeterReadingDate DATETIME
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
			,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
			,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
			,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
			
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading
														,MeterChangedDate)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET
				InitialBillingKWh=@InitialBillingkWh
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
		SET @ReadBy=(SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		
		
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@OldMeterReadingDate
									,@ReadBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@OldMeterNo)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		--If New MeterNo assighned to that customer
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET InitialReading=@NewMeterReading
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE @NewMeterDials INT
		
		SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		
		INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@NewMeterReadingDate
									,@ReadBy									
									,@NewMeterReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,0
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@MeterNo)
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				 ,PresentReading AS PreviousReading				 
				 ,AverageReading As AverageUsage
				 ,BT.BillingType AS LastReadType
				 ,CONVERT(VARCHAR(20),ReadDate,105) AS PreviousReadingDate				 
			  FROM UDV_CustomerDescription  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber
			  JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   
GO