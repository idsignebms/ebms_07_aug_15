GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M.RamaDevi
-- Create date: 07-04-2014
-- Modified date: 16-04-2014
-- ModifiedBy: NEERAJ
-- Description:	Insert Customer Payments in Tbl_CustomerPayments
-- Modified By : Padmini
-- Modified Date : 26-12-2014
-- =============================================
ALTER PROCEDURE [USP_InsertCustomerPayments]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @CustomerID VARCHAR(50)
			,@AccountNo VARCHAR(50)
			,@ReceiptNo VARCHAR(20)
			,@PaymentMode INT
			,@DocumentPath VARCHAR(MAX)
			,@DocumentName VARCHAR(MAX)
			,@BillNo VARCHAR(50)
			,@Cashier VARCHAR(50)
			,@CashOffice INT
			,@CreatedBy VARCHAR(50)
			,@BatchNo INT
			,@PaidAmount DECIMAL(20,4)
			,@PaymentRecievedDate VARCHAR(20)
			
	SELECT   @CustomerID=C.value('(CustomerID)[1]','VARCHAR(50)')
			,@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@ReceiptNo=C.value('(ReceiptNo)[1]','VARCHAR(20)')
			,@PaymentMode=C.value('(PaymentModeId)[1]','INT')
			,@DocumentPath=C.value('(DocumentPath)[1]','VARCHAR(MAX)')
			,@DocumentName=C.value('(Document)[1]','VARCHAR(MAX)')
			,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
			,@BatchNo=C.value('(BatchNo)[1]','INT')
			,@PaidAmount=C.value('(PaidAmount)[1]','DECIMAL(20,4)')
			,@PaymentRecievedDate=C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')
	FROM @XmlDoc.nodes('MastersBE') AS T(C)	
	
	----SET @CustomerID=(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo);
	--SET @CustomerID=(SELECT GlobalAccountNumber FROM [UDV_CustomerDescription] WHERE AccountNo=@AccountNo);
	
	--IF(NOT EXISTS(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails where AccountNo=@AccountNo))
	
	IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	BEGIN
		SET @AccountNo=(SELECT GlobalAccountNumber FROM UDV_CustomerDescription 
							WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	END 
	
	IF(NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] where GlobalAccountNumber=@AccountNo))
		SELECT 0 AS IsCustomerExists FOR XML PATH ('MastersBE')
	ELSE
	BEGIN

			DECLARE @BalanceAmount DECIMAL(20,4)=0
					,@BillAmount DECIMAL(20,4)
					,@BillPaymentModeId INT
					,@CustomerPaymentId INT
			SELECT @Cashier=CashierID,@CashOffice=CashOffice FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo			
			
			SELECT @BillAmount=TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE BillNo=@BillNo
			SELECT @BillPaymentModeId=BillPaymentModeId FROM Tbl_CompanyDetails Where CompanyId=1 
			IF(@BillPaymentModeId = 2)--FIFO
				BEGIN
					INSERT INTO Tbl_CustomerPayments(
														 --CustomerID
														AccountNo
														,ReceiptNo
														,PaymentMode
														,DocumentPath
														,DocumentName
														--,BillNo
														,Cashier
														,CashOffice
														,CreatedBy
														,CreatedDate
														,BatchNo
														,RecievedDate
														,PaidAmount
														,ActivestatusId
														)
												VALUES(
														--@CustomerID
														 @AccountNo
														,@ReceiptNo
														,@PaymentMode
														,@DocumentPath
														,@DocumentName
														--,@BillNo
														,@Cashier
														,@CashOffice
														,@CreatedBy
														,dbo.fn_GetCurrentDateTime()
														,@BatchNo
														,@PaymentRecievedDate
														,@PaidAmount
														,1
													)
					
					SELECT @BalanceAmount=SUM(PaidAmount)-(SELECT ISNULL(SUM(TotalBillAmountWithArrears),0) FROM Tbl_CustomerBills WHERE ActiveStatusId=1 AND PaymentStatusID=1)
					FROM Tbl_CustomerPayments WHERE AccountNo=@AccountNo AND ActivestatusId=1
					
			IF(@BalanceAmount>0)--Previous balance exists then Assigning to paid amount
						SET @PaidAmount=@BalanceAmount
						
			IF(@PaidAmount>=@BillAmount)
				BEGIN						
					INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,BillNo,PaidAmount,CreatedBy,CreatedDate)
					VALUES(
					(SELECT TOP(1) CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1 AND AccountNo=@AccountNo ORDER BY CustomerPaymentID DESC)
					,@BillNo,@BillAmount,@CreatedBy,dbo.fn_GetCurrentDateTime())
				END
						
			IF(@PaidAmount=@BillAmount)-- Paid amount is equal to bill amount then set bill to paid
				BEGIN
					UPDATE Tbl_CustomerBills SET PaymentStatusID=1 WHERE BillNo=@BillNo
				END
			ELSE IF(@PaidAmount>@BillAmount)-- Paid amount is more than bill amount then set bill to paid and update next bills
				BEGIN
					UPDATE Tbl_CustomerBills SET PaymentStatusID=1 WHERE BillNo=@BillNo
					SET @PaidAmount=@PaidAmount-@BillAmount
						
					IF(@PaidAmount>0)--Customer Amount exists more than bill amount then pay them to non paid bills
						BEGIN
								CREATE TABLE #CustomerBills(Sno INT IDENTITY(1,1),BillNo BIGINT)
								INSERT INTO #CustomerBills
								SELECT BillNo FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo AND ActiveStatusId=1 AND PaymentStatusID=2
								DECLARE @BillsCount INT=0
								SELECT @BillsCount=COUNT(0) FROM #CustomerBills
								
								IF(@BillsCount>0)-- Non Paid bills exists
									BEGIN
										DECLARE @I INT=1
										WHILE(@I<=@BillsCount)-- looping each bill to make payment
									BEGIN
										SELECT @BillNo=BillNo FROM #CustomerBills WHERE Sno=@I -- Assigns bill no
										SELECT @BillAmount=TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE BillNo=@BillNo-- Assgins Bill Amount
										
										IF(@PaidAmount>=@BillAmount)-- Customer amount is greater than equal to bill amount
											BEGIN
											
													UPDATE Tbl_CustomerBills SET PaymentStatusID=1 WHERE BillNo=@BillNo
													SET @PaidAmount=@PaidAmount-@BillAmount
													
																	
											INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,BillNo,PaidAmount,CreatedBy,CreatedDate)
											VALUES((SELECT TOP(1) CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1 AND AccountNo=@AccountNo ORDER BY CustomerPaymentID DESC)
											,@BillNo,@PaidAmount,@CreatedBy,dbo.fn_GetCurrentDateTime())
												
													IF(@PaidAmount<=0)
														BREAK;
											END
										ELSE
											BEGIN	
												INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,BillNo,PaidAmount,CreatedBy,CreatedDate)
												VALUES((SELECT top 1 CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1 AND AccountNo=@AccountNo ORDER BY CustomerPaymentID DESC)
												,@BillNo,@BillAmount,@CreatedBy,dbo.fn_GetCurrentDateTime())
																						
												BREAK;
											End
										SET @I=@I+1
									END
								END
						END
					END
					ELSE
					BEGIN
						
							INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,BillNo,PaidAmount,CreatedBy,CreatedDate)
							VALUES((SELECT top 1 CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1 AND AccountNo=@AccountNo ORDER BY CustomerPaymentID DESC),@BillNo,@BillAmount,@CreatedBy,dbo.fn_GetCurrentDateTime())
						

					END
					SELECT 
						1 AS IsCustomerExists
						,1 AS IsSuccess
						,(SELECT CONVERT(DECIMAL(20,2),
							BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount
						FROM Tbl_BatchDetails 
						WHERE BatchNo=@BatchNo
					FOR XML PATH ('MastersBE')
				END
			ELSE IF(@BillPaymentModeId=1)--BILL TO BILL
				BEGIN
					IF(@PaidAmount>@BillAmount)
					BEGIN
						SELECT 
							1 AS IsMorePaidAmount 
						FOR XML PATH('MastersBE')
					END
					ELSE
					BEGIN
					
								INSERT INTO Tbl_CustomerPayments(
									 --CustomerID
									AccountNo
									,ReceiptNo
									,PaymentMode
									,DocumentPath
									,DocumentName
									--,BillNo
									,Cashier
									,CashOffice
									,CreatedBy
									,CreatedDate
									,BatchNo
									,RecievedDate
									,PaidAmount
									,ActivestatusId
									)
							VALUES(
									--@CustomerID
									@AccountNo
									,@ReceiptNo
									,@PaymentMode
									,@DocumentPath
									,@DocumentName
									--,@BillNo
									,@Cashier
									,@CashOffice
									,@CreatedBy
									,dbo.fn_GetCurrentDateTime()
									,@BatchNo
									,@PaymentRecievedDate
									,@PaidAmount
									,1
								)
								IF(@PaidAmount=@BillAmount)
									UPDATE Tbl_CustomerBills SET PaymentStatusID=1 WHERE BillNo=@BillNo
								SELECT 
									 1 AS IsCustomerExists
									,1 AS IsSuccess
									,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount
									FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo
									FOR XML PATH ('MastersBE')
						
						IF(@PaidAmount=@BillAmount)
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,BillNo,PaidAmount,CreatedBy,CreatedDate)
							VALUES((SELECT TOP(1) CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1 AND AccountNo=@AccountNo ORDER BY CustomerPaymentID DESC)
							,@BillNo,@BillAmount,@CreatedBy,dbo.fn_GetCurrentDateTime())
							
						END
						ELSE
						BEGIN
							INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,BillNo,PaidAmount,CreatedBy,CreatedDate)
							VALUES((SELECT TOP(1) CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1 AND AccountNo=@AccountNo ORDER BY CustomerPaymentID DESC),@BillNo,@BillAmount,@CreatedBy,dbo.fn_GetCurrentDateTime())
							END						
						
						END
					END
		END
	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 18-07-2014  
-- Description: Insert Bill Payments  
-- Modified By: Neeraj Kanojiya
-- Modified on: 4-Sep-2014
-- Modified By: Padmini
-- Modified Date : 27-12-2014
-- =============================================  
ALTER PROCEDURE [USP_InsertBillPayments]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
    DECLARE  @CustomerID VARCHAR(50)  
    ,@AccountNo VARCHAR(50)  
    ,@ReceiptNo VARCHAR(20)  
    ,@PaymentMode INT  
    ,@DocumentPath VARCHAR(MAX)  
    ,@DocumentName VARCHAR(MAX)  
    --,@BillNo VARCHAR(50)  
    ,@Cashier VARCHAR(50)  
    ,@CashOffice INT  
    ,@CreatedBy VARCHAR(50)  
    ,@BatchNo INT  
    ,@PaidAmount DECIMAL(20,4)  
    ,@PaymentRecievedDate VARCHAR(20)  
    ,@CustomerPaymentId INT  
    ,@EffectedRows INT  
  DECLARE @PaidBillTemp TABLE (AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),BillNo VARCHAR(50),BillStatus INT)    
     
     
  SELECT   @CustomerID=C.value('(CustomerID)[1]','VARCHAR(50)')  
    ,@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
    ,@ReceiptNo=C.value('(ReceiptNo)[1]','VARCHAR(20)')  
    ,@PaymentMode=C.value('(PaymentModeId)[1]','INT')  
    ,@DocumentPath=C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
    ,@DocumentName=C.value('(Document)[1]','VARCHAR(MAX)')  
    --,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')  
    ,@BatchNo=C.value('(BatchNo)[1]','INT')  
    ,@PaidAmount=C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
    ,@PaymentRecievedDate=C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
   
  --SET @CustomerID=(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo);  
    
  --IF(NOT EXISTS(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails where AccountNo=@AccountNo)) 
  
  IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	BEGIN
		SET @AccountNo=(SELECT GlobalAccountNumber FROM UDV_CustomerDescription 
							WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	END 
  
  
  IF(NOT EXISTS(SELECT GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] where GlobalAccountNumber=@AccountNo)) --Checking existence of customer befor payment
   SELECT 0 AS IsCustomerExists FOR XML PATH ('MastersBE')  
  ELSE  -- If customer is exists than do payment
   BEGIN     
   
    DELETE FROM @PaidBillTemp  --Flushing the temp table
    
    INSERT INTO @PaidBillTemp  --Inserting into temp table. If customer pay lumpsum amount for more than one bill then amount should be split according to bill.
    SELECT AccountNo,AmountPaid,BillNo,BillStatus FROM [dbo].[fn_GetBillPaymentDetails](@AccountNo,@PaidAmount)  --Called function, that returns table whic has splited amount according to bills by account no.
    WHERE AmountPaid>0  
         
    INSERT INTO Tbl_CustomerPayments(	--Inserting into Customer payment details table normal values.
         -- CustomerID  
           AccountNo  
         ,ReceiptNo  
         ,PaymentMode  
         ,DocumentPath  
         ,DocumentName  
         ,Cashier  
         ,CashOffice  
         ,CreatedBy  
         ,CreatedDate  
         ,BatchNo  
         ,RecievedDate  
         ,PaidAmount  
         ,ActivestatusId  
         )  
       VALUES(    
        -- @CustomerID  
          @AccountNo  
         ,@ReceiptNo  
         ,@PaymentMode  
         ,@DocumentPath  
         ,@DocumentName  
         ,@Cashier  
         ,@CashOffice  
         ,@CreatedBy  
         ,dbo.fn_GetCurrentDateTime()  
         ,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)   
         ,@PaymentRecievedDate  
         ,@PaidAmount  
         ,1  
         )  
       
    SELECT @CustomerPaymentId  = SCOPE_IDENTITY()       
      
    INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,PaidAmount,BillNo,CreatedBy,CreatedDate)  --Inserting into payment details table with splited details.    
    SELECT @CustomerPaymentId,AmountPaid,BillNo,@CreatedBy,dbo.fn_GetCurrentDateTime() FROM @PaidBillTemp  
         
    UPDATE CB   
     SET PaymentStatusID = PT.BillStatus  
    FROM Tbl_CustomerBills CB  
    JOIN @PaidBillTemp PT ON CB.BillNo = PT.BillNo  
    WHERE CB.AccountNo = @AccountNo  
    
  --  UPDATE Tbl_CustomerDetails 
		--SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)--(SELECT [dbo].[fn_GetCustomerTotalDueAmount] (@AccountNo))
  --  WHERE AccountNo = @AccountNo
	 
    UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
      SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)--(SELECT [dbo].[fn_GetCustomerTotalDueAmount] (@AccountNo))
    WHERE GlobalAccountNumber = @AccountNo
       
    SELECT @EffectedRows = @@ROWCOUNT  
      
    SELECT   
      1 AS IsCustomerExists  
     ,(CASE WHEN @EffectedRows > 0 THEN 1 ELSE 0 END)AS IsSuccess  
    -- ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount  
    --FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo  
    FOR XML PATH ('MastersBE')   
       
   END  
END
-------------------------------------------------------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  T.Karthik      
-- Create date: 12-04-2014      
-- Modified date : 15-04-2014      
-- Description: The purpose of this procedure is to get Customer Pending bills      
-- =============================================      
ALTER PROCEDURE [USP_GetCustomerPendingBills]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
 SELECT      
  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')      
 FROM @XmlDoc.nodes('MastersBE') AS T(C)
 
 IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	BEGIN
		SET @AccountNo=(SELECT GlobalAccountNumber FROM UDV_CustomerDescription 
							WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	END 
       
 SELECT      
 (      
  SELECT      
   BillNo      
   ,CONVERT(VARCHAR(30),BillGeneratedDate,103) AS BillDate      
   --,CONVERT(DECIMAL(20,2),TotalBillAmountWithArrears) AS BillTotal    --Commented by Raja ID-065  
   ,CONVERT(DECIMAL(20,2),TotalBillAmountWithTax) AS BillTotal -- Raja ID-065  
   --,ISNULL((SELECT CONVERT(DECIMAL(20,2),TotalBillAmountWithArrears-SUM(PaidAmount)) FROM Tbl_CustomerPayments WHERE AccountNo=CB.AccountNo AND BillNo=CB.BillNo AND ActiveStatusId=1),CB.TotalBillAmountWithArrears) AS DueAmount      
   ,((CONVERT(DECIMAL(18,2),Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount))-ISNULL(dbo.fn_GetTotalPaidAmountOfBill(BillNo),0)) AS DueAmount-- Neeraj-ID077 For Due Bill    

  FROM Tbl_CustomerBills AS CB
  WHERE AccountNo=@AccountNo AND PaymentStatusID=2 AND ActiveStatusId=1      
  ORDER BY BillYear ASC,BillMonth ASC      
  FOR XML PATH('MastersBE'),TYPE      
 )      
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
END     
----------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [USP_GetCustomerDetailsForPaymentEntry]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
			,@BU_ID VARCHAR(50)
 SELECT  
  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 IF((SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))=1)  
	 BEGIN  
		  SELECT  
		   --dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		   dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
		   ,ClassName  
		   --,(SELECT DBO.fn_GetCustomerTotalDueAmount(@AccountNo)) AS DueAmount  --Commented By Raja-ID065
		   ,OutStandingAmount AS DueAmount  --Commented By Raja-ID065
		   --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS DueAmount --Raja-ID065
		   --,(SELECT CONVERT(DECIMAL(20,2),SUM(ISNULL(TotalBillAmountWithArrears,0))) FROM Tbl_CustomerBills WHERE ActiveStatusId=1 AND BillStatusId=2 AND AccountNo=@AccountNo) AS DueAmount  
		   ,1 AS IsCustomerExists  
		  FROM [UDV_CustomerDescription] 
		  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)		    
		  AND(BU_ID = @BU_ID OR @BU_ID = '')
		  FOR XML PATH('MastersBE')  
	 END  
	 ELSE  
		 BEGIN  
			SELECT 0 AS IsCustomerExists 
			FOR XML PATH('MastersBE')  
		 END  
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				 ,PresentReading AS PreviousReading				 
				 ,AverageReading As AverageUsage
				 ,BT.BillingType AS LastReadType
				 ,CONVERT(VARCHAR(20),ReadDate,103) AS PreviousReadingDate				 
			  FROM UDV_CustomerDescription  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 ,@OldMeterNo VARCHAR(50)
				 ,@InitialBillingkWh INT
				 ,@OldMeterReadingDate DATETIME
				 ,@NewMeterReadingDate DATETIME
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
			,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
			,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
			,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
			
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading
														,MeterChangedDate)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET
				InitialBillingKWh=@InitialBillingkWh
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
		SET @ReadBy=(SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		
		
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@OldMeterReadingDate
									,@ReadBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@OldMeterNo)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		--If New MeterNo assighned to that customer
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET InitialReading=@NewMeterReading
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE @NewMeterDials INT
		
		SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		
		INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@NewMeterReadingDate
									,@ReadBy									
									,@NewMeterReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,0
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@MeterNo)
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
ALTER FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF NOT EXISTS(SELECT 0 FROM [UDV_CustomerDescription] 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId=1 
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/09/2015 19:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO INT   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','INT')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
    
    
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	 ,(SELECT GlobalAccountNumber FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=TempDetails.AccountNo OR OldAccountNo=TempDetails.AccountNo)) AS AccountNo
	 ,(SELECT OldAccountNo FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=TempDetails.AccountNo OR OldAccountNo=TempDetails.AccountNo)) AS OldAccountNo
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(TempDetails.AccountNo,TempDetails.ReceivedDate,TempDetails.AmountPaid) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END 
GO
GO

-- =============================================    
-- Author:  Faiz-ID103 
-- Create date: 09-Mar-2015     
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For Customer Type Change 
-- =============================================      
CREATE PROCEDURE [USP_GetCustomerForCustTypeChange]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(50)
    
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)      
  


 IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))    
  BEGIN    
   SELECT  
       CD.GlobalAccountNumber AS GlobalAccountNumber  
      ,CD.AccountNo As AccountNo  
      ,CD.FirstName  
      ,CD.MiddleName  
      ,CD.LastName  
      ,CD.Title        
      ,CD.KnownAs  
      ,CD.CustomerTypeId
      ,CT.CustomerType
      ,ISNULL(MeterNumber,'--') AS MeterNo  
      ,CD.ClassName AS Tariff  
      ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo  
      FROM UDV_CustomerDescription  CD  
      join Tbl_MCustomerTypes CT
      on CD.CustomerTypeId=CT.CustomerTypeId
      WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)  
      AND CD.ActiveStatusId IN (1,2)    
      FOR XML PATH('ChangeCustomerTypeBE')      
  END    
 ELSE     
  BEGIN    
   SELECT 1 AS IsAccountNoNotExists   
   FOR XML PATH('ChangeCustomerTypeBE')    
  END  
END  

-----------------------------------------------------------------------------------------

GO
INSERT INTO TBL_FunctionalAccessPermission ([Function],AccessLevels,CreatedDate,CreatedBy,IsActive)
values('Customer Type Change',1,dbo.fn_GetCurrentDateTime(),'Admin',0)


-----------------------------------------------------------------------------------------
GO

-- =============================================  
-- Author:  Faiz-ID103      
-- Create date: 09-Mar-2015      
-- Description: The purpose of this procedure is to Update Customer Type 

-- =============================================  
CREATE PROCEDURE [USP_ChangeCustomerType]  
 (      
  @XmlDoc xml      
 )  
AS      
BEGIN  
       
 DECLARE    @GlobalAccountNumber VARCHAR(50)   
		   ,@CustomerTypeId INT 
		   ,@ModifiedBy VARCHAR(50)  
		   ,@Reason VARCHAR(MAX)  
		   ,@ApprovalStatusId INT  
    
 SELECT    @GlobalAccountNumber=C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		  ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
		  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')  
		  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
		  FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)  
   
   
		   UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails
		   SET CustomerTypeId=@CustomerTypeId
		   WHERE GlobalAccountNumber=@GlobalAccountNumber
		   
		   SELECT 1 AS IsSuccess FOR XML PATH('ChangeCustomerTypeBE')
   
   
 --IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
 -- WHERE GlobalAccountNumber=@GlobalAccountNumber  
 -- AND ApproveStatusId NOT IN (2,3)) = 0)  
 -- BEGIN  
 -- INSERT INTO Tbl_CustomerNameChangeLogs(  
 --      GlobalAccountNumber  
 --  ,AccountNo  
 --  ,OldTitle  
 --  ,OldFirstName  
 --  ,OldMiddleName  
 --  ,OldLastName  
 --  ,OldKnownas  
 --  ,NewTitle  
 --  ,NewFirstName  
 --  ,NewMiddleName  
 --  ,NewLastName  
 --  ,NewKnownas  
 --  ,CreatedBy  
 --  ,CreatedDate  
 --  ,ApproveStatusId  
 --  ,Remarks)  
     
 --SELECT @GlobalAccountNumber  
 --   ,AccountNo  
 --   ,CASE Title WHEN '' THEN NULL ELSE Title END   
 --   ,CASE FirstName WHEN '' THEN NULL ELSE FirstName END   
 --   ,CASE MiddleName WHEN '' THEN NULL ELSE MiddleName END  
 --   ,CASE LastName WHEN '' THEN NULL ELSE LastName END    
 --   ,CASE Knownas WHEN '' THEN NULL ELSE Knownas END    
 --   ,CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
 --   ,CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
 --   ,CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END   
 --   ,CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END     
 --   ,CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
 --   ,@ModifiedBy  
 --   ,dbo.fn_GetCurrentDateTime()  
 --   ,@ApprovalStatusId  
 --   ,@Details  
 --FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber  
    
 -- IF(@ApprovalStatusId = 2)--If Approved  
 -- BEGIN  
 --  UPDATE [CUSTOMERS].[Tbl_CustomerSDetail] SET  
 --  FirstName=CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
 --  ,MiddleName=CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END  
 --  ,LastName=CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END  
 --  ,KnownAs=CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
 --  ,ModifedBy=@ModifiedBy  
 --  ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
 --  WHERE GlobalAccountNumber=@GlobalAccountNumber    
 --  SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
 -- END  
 --ELSE  
 -- BEGIN  
 --  SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
 -- END  
 --END  
 --ELSE  
 --BEGIN  
 -- SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
 --END  
    
 END  
  
  
-----------------------------------------------------------------------------------------
GO