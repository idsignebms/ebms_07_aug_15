GO
GO

-- =============================================
-- Author:		Faiz-ID103---(Bhargav)
-- Create date: 10-Mar-2015
-- Description:	The purpose of this function is to get the Customer Service Address

-- =============================================
CREATE FUNCTION [dbo].[fn_GetCustomerServiceAddress_New]
(

@HouseNo VARCHAR(50)
,@StreetName VARCHAR(50)
,@Landmark VARCHAR(50)
,@City VARCHAR(50)
,@Details VARCHAR(50)
,@zipcode VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)


			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN @HouseNo IS NULL OR @HouseNo = '' THEN '' ELSE LTRIM(RTRIM(@HouseNo)) + ', '  END + 
				CASE WHEN @StreetName IS NULL OR @StreetName = '' THEN '' ELSE  LTRIM(RTRIM(@StreetName)) + ', ' END + 
				CASE WHEN @Landmark IS NULL OR @Landmark = '' THEN '' ELSE LTRIM(RTRIM(@Landmark)) + ', '  END + 
				CASE WHEN @City IS NULL OR @City = '' THEN '' ELSE  LTRIM(RTRIM(@City)) + ', ' END  + 
				CASE WHEN @Details IS NULL OR @Details = '' THEN '' ELSE  LTRIM(RTRIM(@Details))+ ', ' END   + 
				CASE WHEN @zipcode IS NULL OR @zipcode = '' THEN '' ELSE  LTRIM(RTRIM(@zipcode))+ ', ' END )) 
			 
	RETURN @ServiceAddress
END


---------------------------------------------------------------------------------------------------------


GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 18-10-2014
-- Description:	To Get FullName Of the Customer Title+Name+SurName

-- Modified By: Faiz-ID103-----(Bhargav)
-- Modified Date: 10-Mar-2015
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerFullName_New]
(
	@Title VARCHAR(50),@FirstName VARCHAR(50) ,@MiddleName VARCHAR(50),@LastName VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Name VARCHAR(MAX)

		SELECT @Name =	LTRIM(RTRIM(
		CASE WHEN @Title IS NULL OR @Title = '' THEN '' ELSE LTRIM(RTRIM(@Title+'. ')) END + 
		CASE WHEN @FirstName IS NULL OR @FirstName = '' THEN '' ELSE  LTRIM(RTRIM(@FirstName)) END + 
		CASE WHEN @MiddleName IS NULL OR @MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@MiddleName)) END +
		CASE WHEN @LastName IS NULL OR @LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@LastName)) END))
 
	RETURN @Name
END
----------------------------------------------------------------------------------------------------------
GO  

  
ALTER VIEW [dbo].[UDV_CustomerDescription]   
AS  
 SELECT  
   CD.GlobalAccountNumber   
  ,DocumentNo  
  ,AccountNo  
  ,OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,KnownAs  
  ,EmployeeCode  
  ,ISNULL(HomeContactNumber,'--') AS HomeContactNo      
  ,ISNULL(BusinessContactNumber,'--') AS BusinessContactNo      
  ,ISNULL(OtherContactNumber,'--') AS OtherContactNo  
  ,PhoneNumber  
  ,AlternatePhoneNumber  
  ,CD.ActiveStatusId  
  ,PD.PoleID  
  ,TariffClassID AS TariffId  
  ,TC.ClassName AS ClassName  
  ,IsBookNoChanged  
  ,ReadCodeID  
  ,PD.SortOrder  
  ,Highestconsumption  
  ,OutStandingAmount  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BU.BU_ID  
  ,BU.BusinessUnitName  
  ,BU.BUCode  
  ,SU.SU_ID  
  ,SU.ServiceUnitName  
  ,SU.SUCode  
  ,SC.ServiceCenterId  
  ,SC.ServiceCenterName  
  ,SC.SCCode  
  ,C.CycleId  
  ,C.CycleName  
  ,C.CycleCode  
  ,StatusName AS ActiveStatus  
  ,CD.EmailId  
  ,MeterNumber   
  ,MeterType AS MeterTypeId  
  ,PhaseId  
  ,ClusterCategoryId  
  ,InitialReading  
  ,InitialBillingKWh AS  MinimumReading  
  ,AvgReading  
  ,RouteSequenceNumber AS RouteSequenceNo  
  ,ConnectionDate  
  ,CD.CreatedDate  
  ,CD.CreatedBy  
  ,CustomerTypeId  
  ,C.ActiveStatusId AS CylceActiveStatusId  
  , CD.ServiceAddressID  
FROM CUSTOMERS.Tbl_CustomersDetail CD  
JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber=CD.GlobalAccountNumber  
LEFT JOIN  CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber  
LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails TD ON TD.TenentId=CD.TenentId  
JOIN  Tbl_BookNumbers BN ON BN.BookNo=PD.BookNo  
JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
--LEFT JOIN  Tbl_MActiveStatusDetails MS ON CD.ActiveStatusId=MS.ActiveStatusId   
LEFT JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
LEFT JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo  
--------------------------------------------------------------------------------------------------
  

GO
-- =============================================      
-- Author  : Suresh Kumar D      
-- Create date : 01-10-2014      
-- Description : The purpose of this procedure is to Get the Pre bill     
-- Modified By : Padmini    
-- Modified Date : 26-12-2014    
-- Modified By : Faiz-ID103    ---------(Bhargav)
-- Modified Date : 10-Mar-2015   
-- =============================================      
ALTER PROCEDURE [dbo].[USP_RptGetPreBillingCustomers]    
(      
@XmlDoc XML      
)      
AS      
BEGIN      
     
 Declare @ReadCode INT  
  ,@BU_Id VARCHAR(20)    
   --,@Month INT    
   --,@Year INT    
   --,@Date DATETIME         
   --,@MonthStartDate VARCHAR(50)        
       
 SELECT @ReadCode = C.value('(ReadCodeId)[1]','INT')      
  ,@BU_Id = C.value('(BU_ID)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('RptPreBillingBe') as T(C)      
     
 --SELECT     
 -- @Year = [Year]    
 -- ,@Month = [Month]     
 --FROM Tbl_BillingMonths WHERE OpenStatusId = 1    
     
 --SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'        
 --SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))       
    
 --  SELECT       
 --(    
 -- SELECT     
 --    CD.GlobalAccountNumber    
 --    --,KnownAs AS Name    
 --    ,(SELECT dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)) AS Name    
 --    ,CD.MeterNumber    
 --    --,(CASE WHEN @ReadCode = 2 THEN (dbo.fn_GetCustomerPreviousReadDate(AccountNo,@Year,@Month)) ELSE '' END) AS PreviousReadDate    
 --    --,(CASE WHEN @ReadCode = 2 THEN (dbo.fn_GetCustomerPreviousReading(AccountNo,@Year,@Month)) ELSE '' END) AS PreviousReading    
 --    ,(CASE WHEN @ReadCode = 2 THEN R.ReadDate ELSE '' END) AS PreviousReadDate    
 --    ,(CASE WHEN @ReadCode = 2 THEN ISNULL(R.PresentReading,CD.InitialReading) ELSE '' END) AS PreviousReading    
 --    ,OldAccountNo    
 --    ,TC.ClassName     
 --    ,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
 -- --FROM Tbl_CustomerDetails CD    
 -- FROM [UDV_CustomerDescription] CD    
 -- LEFT JOIN Tbl_MTariffClasses TC ON CD.TariffId = TC.ClassID    
 -- LEFT JOIN (SELECT GlobalAccountNumber,CONVERT(varchar,ReadDate,103) AS ReadDate,PresentReading FROM Tbl_CustomerReadings    
 --    WHERE CustomerReadingId IN (SELECT MAX(CustomerReadingId)     
 --         FROM Tbl_CustomerReadings     
 --         WHERE CONVERT(DATE,ReadDate) < CONVERT(DATE,@Date)    
 --         GROUP BY AccountNumber,ReadDate)) R ON CD.AccountNo = R.GlobalAccountNumber    
 -- WHERE CD.GlobalAccountNumber NOT IN(SELECT GlobalAccountNumber     
 --       FROM Tbl_CustomerReadings    
 --       WHERE CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date))    
 -- AND ActiveStatusId = 1 AND ReadCodeId = @ReadCode    
 -- FOR XML PATH('PreBillingList'),TYPE      
 -- )    
 -- FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')   
   
 IF(@ReadCode = 2) -- Read Customers  
  BEGIN  
   SELECT  
   (  
    
SELECT CD.GlobalAccountNumber  
     ,MIN(CR.PreviousReading) as PreviousReading  
     --,MAX(CR.PresentReading) as PresentReading  
     ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name  
     ,CD.ClassName AS ClassName
     ,CD.MeterNumber AS MeterNo
     ,OldAccountNo 
     ,MIN (CR.ReadDate) AS PreviousReadDate
     ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
    FROM Tbl_CustomerReadings CR  
    INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = Cr.GlobalAccountNumber   
    LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId  
    AND CD.BU_ID = @BU_Id-----
    AND CR.IsBilled=0  
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND 
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
    GROUP BY CD.GlobalAccountNumber  
     ,CD.FirstName  
     ,CD.Title  
     ,CD.MiddleName  
     ,CD.LastName  
     ,CD.MeterNumber  
     ,CD.ClassName  
     ,OldAccountNo 
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode
     ,Adress.AddressID 
    FOR XML PATH('PreBillingList'),TYPE  
   )     
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')  
  END    
 ELSE -- Direct / Estimated Customers  
  BEGIN  
   With Results AS  
   (  
   SELECT   
    CD.GlobalAccountNumber  
    ,MIN(CR.PreviousReading) AS PreviousReading  
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name   
    ,CD.ClassName  
    ,OldAccountNo  
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress  
   FROM UDV_CustomerDescription CD  
   LEFT JOIN Tbl_CustomerReadings CR ON (CD.GlobalAccountNumber != CR.GlobalAccountNumber AND IsBilled=0)  
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId   
   AND CD.BU_ID = @BU_Id-----
    AND CR.IsBilled=0  
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND 
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
   GROUP BY CD.GlobalAccountNumber  
     ,CD.Title  
     ,CD.FirstName  
     ,CD.MiddleName  
     ,CD.LastName  
     ,CD.ClassName  
     ,OldAccountNo  
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode
     ,Adress.AddressID 
   UNION   
   SELECT   
    CD.GlobalAccountNumber  
    ,MIN(CR.PreviousReading) AS PreviousReading  
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name   
    ,CD.ClassName  
    ,OldAccountNo  
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress  
   FROM UDV_CustomerDescription CD  
   LEFT JOIN Tbl_CustomerReadings CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber  
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId  
   AND CD.BU_ID = @BU_Id-----
    AND CR.IsBilled=0  
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND 
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
   WHERE CustomerTypeId = 1  
   GROUP BY CD.GlobalAccountNumber  
     ,CD.Title  
     ,CD.FirstName  
     ,CD.MiddleName  
     ,CD.LastName  
     ,CD.ClassName  
     ,OldAccountNo  
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode
     ,Adress.AddressID 
   )  
     
   SELECT  
   (  
   SELECT * FROM Results  
    FOR XML PATH('PreBillingList'),TYPE  
   )     
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')  
  END  
END    


---------------------------------------------------------------------------------------------------------  
GO 
-- =============================================    
-- Author  : Suresh Kumar D    
-- Create date : 08-10-2014    
-- Description : The purpose of this procedure is to Get High Estimation the Pre bill   
-- Modified By : Padmini  
-- Modified Date : 26-12-2014   
-- Modified By : T.Karthik  
-- Modified Date : 29-12-2014    
-- Modified By : Faiz-ID103  --------(Bhargav)
-- Modified Date : 10-Mar-2015
-- =============================================    
ALTER PROCEDURE [dbo].[USP_RptHighEstimation_Customers]  
(    
@XmlDoc XML    
)    
AS    
BEGIN    

DECLARE @BU_ID VARCHAR(20) 
   
  SELECT    
   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('RptPreBillingBe') as T(C) 


 ;with CustomersList As
(
select  Customers.GlobalAccountNumber  
   ,(SELECT dbo.fn_GetCustomerFullName_New(Customers.Title,Customers.FirstName,Customers.MiddleName,Customers.LastName)) AS Name   
   ,OldAccountNo  
   ,TC.ClassName   
   ,Cycles.CycleName  
   ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
   ,EnergytoCalculate AS EstimationValue
   ,Customers.ClusterCategoryId  
   ,CategoryName AS ClusterType
from dbo.UDV_CustomerDescription Customers 
INNER JOIN Tbl_MTariffClasses TC ON Customers.TariffId = TC.ClassID AND Customers.BU_ID=@BU_ID--'BEDC_BU_0032' 
and Customers.ReadCodeID=1  
INNER JOIN Tbl_Cycles Cycles On Cycles.CycleId = Customers.CycleId
LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=Customers.GlobalAccountNumber AND 
    Customers.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON MCC.ClusterCategoryId=Customers.ClusterCategoryId  
INNER JOIN Tbl_EstimationSettings ES ON ES.CycleId=Customers.CycleId and ES.BillingMonth=1
And ES.BillingYear=2015 and ES.ActiveStatusId=1 and Customers.ClusterCategoryId=Es.ClusterCategoryId
 AND Es.TariffId=Customers.TariffId And ES.BillingRule=1
 ) 
 select * from CustomersList
 where 
  CustomersList.EstimationValue > 
(
(select top 1 AverageReading = case  when  AverageReading IS NULL 
then (select InitialBillingKWh from CUSTOMERS.Tbl_CustomerActiveDetails where AccountNo = CustomersList.GlobalAccountNumber
) else AverageReading END
from  Tbl_CustomerBills  where AccountNo = CustomersList.GlobalAccountNumber 
Order  by CustomerBillId  Desc) * 30 ) /100
 FOR XML PATH('PreBillingList'),ROOT('RptPreBillingInfoByXml')   
 --)
 --FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')  
 END     
   
   

---------------------------------------------------------------------------------------------------------
GO
-- =============================================    
-- Author  : Suresh Kumar D    
-- Create date : 07-10-2014    
-- Description : The purpose of this procedure is to Get the Pre bill      
-- Modified By : Faiz-ID103  --------(Bhargav)
-- Modified Date : 10-Mar-2015
-- =============================================    
ALTER PROCEDURE [dbo].[USP_RptGetHighLowReading_Customers]  
(
@XmlDoc XML    
)
AS    
BEGIN    
   
 Declare @Month INT  
   ,@Year INT  
   ,@Date DATETIME       
   ,@MonthStartDate VARCHAR(50)   
   ,@BU_ID VARCHAR(20) 
   
    SELECT    
   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('RptPreBillingBe') as T(C)    
     
 SELECT   
  @Year = [Year]  
  ,@Month = [Month]   
 FROM Tbl_BillingMonths WHERE OpenStatusId = 1  
   
 SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
 SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))     
     
 ;WITH Result AS  
 (  
 SELECT   
  CR.GlobalAccountNumber AS AccountNo  
  ,CR.GlobalAccountNumber
  ,CR.AverageReading  
  ,CR.Usage  
  ,CR.PresentReading  
  ,CONVERT(DATE,CR.ReadDate) AS PreviousReadDate  
  ,dbo.fn_GetHugeMildReadingDetails(CR.Usage,CR.AverageReading,1) AS HighLowReadDetails  
  --,KnownAs AS Name  
  --,(SELECT dbo.fn_GetCustomerFullName(CR.GlobalAccountNumber)) AS Name  
  ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name   
  ,CD.MeterNumber  
  ,OldAccountNo  
  ,TC.ClassName   
  ,dbo.fn_GetCustomerServiceAddress(CR.GlobalAccountNumber) AS ServiceAddress 
  --,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress          
 FROM Tbl_CustomerReadings CR  
 --LEFT JOIN Tbl_CustomerDetails CD ON CR.AccountNumber = CD.AccountNo  
 LEFT JOIN [UDV_CustomerDescription] CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber  
 LEFT JOIN Tbl_MTariffClasses TC ON CD.TariffId = TC.ClassID  
 WHERE CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
 AND CD.BU_ID = @BU_ID 
 )  
   
   SELECT     
 (  
  SELECT   
   AccountNo  
   ,GlobalAccountNumber
   ,AverageReading  
   ,Usage  
   ,PresentReading  
   ,PreviousReadDate  
   ,HighLowReadDetails  
   ,Name  
   ,MeterNumber  AS MeterNo
   ,OldAccountNo  
   ,ClassName   
   ,ServiceAddress   
  FROM Result   
  WHERE HighLowReadDetails IS NOT NULL  
  FOR XML PATH('PreBillingList'),TYPE    
  )  
  FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')    
       
END    
  
  
---------------------------------------------------------------------------------------------------------  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [USP_GetCustomerDetailsForPaymentEntry]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
			,@BU_ID VARCHAR(50)
			,@Active INT=1
 SELECT  
  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
  IF NOT EXISTS(SELECT 0 FROM [UDV_CustomerDescription] 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId IN (1,2)
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
	BEGIN
		SET @Active=0
	END
	
   
 IF(@Active=1)  
	 BEGIN  
		  SELECT  
		   --dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		   dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
		   ,ClassName  
		   --,(SELECT DBO.fn_GetCustomerTotalDueAmount(@AccountNo)) AS DueAmount  --Commented By Raja-ID065
		   ,OutStandingAmount AS DueAmount  --Commented By Raja-ID065
		   --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS DueAmount --Raja-ID065
		   --,(SELECT CONVERT(DECIMAL(20,2),SUM(ISNULL(TotalBillAmountWithArrears,0))) FROM Tbl_CustomerBills WHERE ActiveStatusId=1 AND BillStatusId=2 AND AccountNo=@AccountNo) AS DueAmount  
		   ,1 AS IsCustomerExists  
		  FROM [UDV_CustomerDescription] 
		  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)		    
		  AND(BU_ID = @BU_ID OR @BU_ID = '')
		  FOR XML PATH('MastersBE')  
	 END  
	 ELSE  
		 BEGIN  
			SELECT 0 AS IsCustomerExists 
			FOR XML PATH('MastersBE')  
		 END  
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerDescription CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerDescription CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum 
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  --,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading
				  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
				  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerDescription C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
-----------------------------------------------------------------------------------
GO
GO
 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 5-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID                 
				  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo       
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber    
				  ,A.OldAccountNo                 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful                  
		  FROM [UDV_CustomerDescription] AS A    
		  JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END       
 --------------------------------------------------------------------------------------------------
 GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@StatusText VARCHAR(200)='Initiation'
		,@IsSuccessful BIT=1

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	SET @StatusText='Deserialization'
	
	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')  
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	SET @StatusText='Update Statement'
	
	UPDATE TBL_ReadToDirectCustomerActivity 
	SET IsLatest=0
	
	SET @StatusText='Insert Statement'
	INSERT INTO TBL_ReadToDirectCustomerActivity
												(
												GlobalAccountNo
												,MeterNumber 
												,CreatedBy 
												,CreatedDate 							
												)
										VALUES
												(
												@GlobalAccountNumber
												,@MeterNumber
												,@CreatedBy
												,DBO.fn_GetCurrentDateTime()
												)
	SET @StatusText='Update Statement'
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
		SET ReadCodeID=1
	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('MastersBE'),TYPE

END  	

---------------------------------------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA
-- Create date: 9 MARCH 2014  
-- Description: The purpose of this function is to get the GLOBAL messages Based on BU_ID  
-- =============================================  
CREATE FUNCTION [dbo].[fn_GetGlobalMessages_ByBu_Id]  
(  
@BU_ID VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @Message VARCHAR(MAX) 
		SET @Message=(SELECT TOP 1
									CONVERT(VARCHAR(50),GM1.[Message])
									+'|'+CONVERT(VARCHAR(50),GM2.[Message])
									+'|'+CONVERT(VARCHAR(50),GM3.[Message])
			FROM Tbl_GlobalMessages AS GM1  
			JOIN Tbl_GlobalMessages AS GM2 ON GM1.GlobalMessageId=GM2.GlobalMessageId-1
			JOIN Tbl_GlobalMessages AS GM3 ON GM1.GlobalMessageId=GM3.GlobalMessageId-2
			WHERE GM1.BU_ID=@BU_ID)
 RETURN @Message   
  
END  
--------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithArrears]    Script Date: 03/09/2015 19:10:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : M.Padmini  
-- Create date: 20-Feb-15
-- Description: This function is used to get customer previousbillamountwithArrears.
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithArrears]  
(  
@GlobalAccountNo VARCHAR(50)
,@BillMonth INT
,@BillYear  INT
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithTax DECIMAL(18,2)
  ,@MaxCustomerBillId INT 
  ,@BillCount INT
  --,@PresDate DATE
  --,@PrevDate DATE
  
  --      SET @PresDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		--SET @PrevDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		
	 --================ Getting 2nd heighest CustomerBillId===================
	 SET @BillCount= (SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
	 IF(@BillCount>=2)
	 BEGIN
		 SELECT  TOP 1 @MaxCustomerBillId= MAX(CustomerBillId)
		 FROM Tbl_CustomerBills   
		 WHERE AccountNo =@GlobalAccountNo 
		 AND CustomerBillId <(SELECT MAX(CustomerBillId) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
		 GROUP BY CustomerBillId
		 ORDER BY CustomerBillId DESC 
		 
		 --================= Getting Previous Bill Amount===============
		 
		 SELECT @BillAmountWithTax= TotalBillAmountWithArrears FROM Tbl_CustomerBills
		 WHERE CustomerBillId=@MaxCustomerBillId
		 AND AccountNo=@GlobalAccountNo
		 --AND BillMonth<DATEPART(MONTH, DATEADD(MONTH,+1,CONVERT(DATE, '2014-'+convert(VARCHAR(2),@BillMonth)+'-01'))) 
		 --AND BillYear=@BillYear
	 END
	 ELSE
	 BEGIN
		SET @BillAmountWithTax=(SELECT OpeningBalance FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber=@GlobalAccountNo)
	 END
 SET @BillAmountWithTax = ISNULL(@BillAmountWithTax,0) 
  
      
 RETURN @BillAmountWithTax   
  
END  
------------------------------------------------------------------------------------------------------------------------------------

GO
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya>
-- Create date: <10 MARCH 2015>
-- Description:	<TO GET TOTAL PAYMENT MADE BY CUSTOMER FROM LAST BIL GEN DATE TO FIX TEMP PURPOSE>
-- =============================================
CREATE FUNCTION fn_GetPreviuosTotalBillPayment
(
	@GlobalAccountNumber varchar(50) 
)
RETURNS  Decimal(18,2)
AS
BEGIN
	Declare @PaidAmount  Decimal(18,2) =0
	 Declare @BillCount int =0
	 SET @BillCount= (SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNumber)
	 IF(@BillCount>=2)
		BEGIN
	 
		 
	
		 
		 Declare @Prev_BillDate datetime

		 Select @Prev_BillDate= MAX (BillGeneratedDate) FROM Tbl_CustomerBills where CustomerBillId Not IN (
		  Select MAX (BillGeneratedDate) FROM Tbl_CustomerBills where  AccountNo=@GlobalAccountNumber) and  AccountNo=@GlobalAccountNumber
		 
		 
		  Select @PaidAmount=SUM(PaidAmount)  from Tbl_CustomerPayments where  
		   CreatedDate >=@Prev_BillDate  and AccountNo=@GlobalAccountNumber
		 
		 
		END
		ELSE 
		BEGIN
	 
		 
		 Select @PaidAmount=SUM(PaidAmount)   from Tbl_CustomerPayments where   CreatedDate >=
			(select top 1 LastBillGeneratedDate from Tbl_LastBillGeneratedDate) and AccountNo=@GlobalAccountNumber
		End
	
	
	
	RETURN @PaidAmount

END
GO

