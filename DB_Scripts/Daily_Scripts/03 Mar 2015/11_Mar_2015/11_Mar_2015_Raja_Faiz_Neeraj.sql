GO

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_DEMO]    Script Date: 03/11/2015 13:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  Suresh Kumar D          
-- Create date: 22-05-2014          
-- Description: The purpose of this procedure is to Calculate the Bill Generation        
-- ModifiedBy : T.Karthik      
-- ModifiedDate: 30-12-2014    
-- ModifiedBy : NEERAJ      
-- ModifiedDate: 28-FEB-2014        
-- DESC			: Billing rule condition for direct customers. approx line no. 317 to 362
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_DEMO]          
(          
@XmlDoc XML          
--@CycleId VARCHAR(50)    
--,@BillingQueuescheduleId INT  
)          
AS          
BEGIN    
 --BEGIN TRY  
 --BEGIN TRAN      
  DECLARE @AccountNumner VARCHAR(50)        
    ,@Month INT        
    ,@Year INT         
    ,@Date DATETIME         
    ,@MonthStartDate DATE     
    ,@PreviousReading VARCHAR(50)        
    ,@CurrentReading VARCHAR(50)        
    ,@Usage DECIMAL(20)        
    ,@RemaningUsage DECIMAL(20)        
    ,@TotalAmount DECIMAL(18,2)= 0        
    ,@Tax DECIMAL(18,2) = 5         
    ,@BillingQueuescheduleId INT        
    ,@PresentCharge INT        
    ,@FromUnit INT        
    ,@ToUnit INT        
    ,@Amount DECIMAL(18,2)    
    ,@TaxId INT        
    ,@CustomerBillId INT = 0        
    ,@BillGeneratedBY VARCHAR(50)        
    ,@LastDateOfBill DATETIME        
    ,@IsEstimatedRead BIT = 0        
    ,@ReadCodeId INT        
    ,@ReadType INT        
    ,@LastBillGenerated DATETIME      
    ,@FeederId VARCHAR(50)      
    ,@CycleId VARCHAR(50)      
    ,@BillNo VARCHAR(50)    
    ,@PrevCustomerBillId INT    
    ,@AdjustmentAmount DECIMAL(18,2)    
    ,@PreviousDueAmount DECIMAL(18,2)    
    ,@CustomerTariffId VARCHAR(50)    
    ,@BalanceUnits INT    
    ,@RemainingBalanceUnits INT    
    ,@IsHaveLatest BIT = 0    
    ,@ActiveStatusId INT    
    ,@IsDisabled BIT    
    ,@DisableType INT    
    ,@IsPartialBill BIT    
    ,@OutStandingAmount DECIMAL(18,2)    
    ,@IsActive BIT    
    ,@NoFixed BIT = 0    
    ,@NoBill BIT = 0     
    ,@ClusterCategoryId INT=NULL  
    ,@StatusText VARCHAR(50)    
    ,@BU_ID VARCHAR(50)  
    ,@BillingQueueScheduleIdList VARCHAR(MAX)  
    ,@RowsEffected INT
    ,@Netarrears Decimal(18,2) = 0
  DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     
        
   DECLARE @GeneratedCustomerBills TABLE(CustomerBillId INT)        
   DELETE FROM @GeneratedCustomerBills      
      
    SELECT    
   --@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')    
   @CycleId = C.value('(CycleId)[1]','VARCHAR(50)')   
   ,@BillingQueueScheduleIdList = C.value('(BillingQueueScheduleIdList)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)    
  
  --===========================Creating table for CycleID and BillingQueueSheduleID List=============================
  CREATE TABLE #inputList(  
         value1 varchar(MAX)  
         ,value2 varchar(MAX)  
         ,ID INT IDENTITY(1,1)  
           
       )    
   INSERT INTO #inputList(  
          value1  
          ,value2            
         )  
   select   
     Value1  
     ,Value2  
   from  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
   --===========================---------------------------------------------------------------=============================
   
 DECLARE @COUNTER INT=1  
 DECLARE @MaxInput INT  
 SET @MaxInput=(SELECT MAX(ID) FROM #inputList)  
  WHILE(@COUNTER<=@MaxInput) --loop
   BEGIN    
   SET @CycleId=(SELECT value1 FROM #inputList WHERE ID=@COUNTER)  
   SET @BillingQueueScheduleId=(SELECT value2 FROM #inputList WHERE ID=@COUNTER)  
   --BILL GEN LOGIC  
    
   --SELECT @BU_ID=BU_ID FROM Tbl_Cycles WHERE CycleId=@CycleId  
    SELECT @BU_ID=BU.BU_ID FROM
    Tbl_Cycles C 
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID   
   WHERE CycleId=@CycleId   
    --=======================Filtering customer data based on bu=============================  
    DECLARE @CustomerMaster TABLE(  
          GlobalAccountNumber VARCHAR(10)  
          ,TariffId INT  
          ,OutStandingAmount DECIMAL(18,2)  
          ,SortOrder INT  
          ,ActiveStatusId INT  
          ,MeterNumber  VARCHAR(50)  
          ,BU_ID VARCHAR(50)  
          ,SU_ID VARCHAR(50)  
          ,ServiceCenterId VARCHAR(50)  
          ,PoleId VARCHAR(50)  
          ,OldAccountNo VARCHAR(50)  
          ,BookNo VARCHAR(50)  
          )        
    INSERT INTO @CustomerMaster  
    (  
    GlobalAccountNumber  
    ,TariffId  
    ,OutStandingAmount  
    ,SortOrder  
    ,ActiveStatusId  
    ,MeterNumber  
    ,BU_ID  
    ,SU_ID  
    ,ServiceCenterId    
    ,PoleId  
    ,OldAccountNo  
    ,BookNo     
      
    )  
    SELECT GlobalAccountNumber  
    ,TariffId  
    ,OutStandingAmount  
    ,SortOrder  
    ,ActiveStatusId  
    ,MeterNumber  
    ,BU_ID  
    ,SU_ID  
    ,ServiceCenterId    
    ,PoleId  
    ,OldAccountNo  
    ,BookNo  
      
   FROM UDV_CustDetailsForBillGen  
   WHERE BU_ID=@BU_ID AND CycleId=@CycleId  
   --SELECT * FROM @CustomerMaster  
   -- =======================================================================================  
     
   DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
   DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
   --DECLARE @GeneratedCustomerBills TABLE(CustomerBillId INT)        
   --DELETE FROM @GeneratedCustomerBills        
           
   DELETE FROM @FilteredAccounts   
   INSERT INTO @FilteredAccounts        
   SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
   FROM Tbl_BillingQueeCustomers C WHERE C.BillingQueuescheduleId = @BillingQueuescheduleId    
   Order By AccountNo,[Month],[Year]    
    --C.BillGenarationStatusId = 5          
           
   SELECT TOP(1) @AccountNumner = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC        
   WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @AccountNumner ORDER BY AccountNo ASC))        
    BEGIN        
   SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0) FROM @CustomerMaster WHERE GlobalAccountNumber = @AccountNumner     
   SET @IsEstimatedRead = 0     
   SET @PrevCustomerBillId = 0        
   SET @AdjustmentAmount = 0    
                
   SELECT @Year = BillingYear,@Month = BillingMonth,@BillingQueuescheduleId = BillingQueuescheduleId FROM @FilteredAccounts WHERE AccountNo = @AccountNumner        
   SELECT @BillGeneratedBY = CreatedBy FROM Tbl_BillingQueueSchedule WHERE BillingQueueScheduleId = @BillingQueuescheduleId         
   SELECT TOP(1) @LastDateOfBill = CreatedDate FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner ORDER BY CreatedDate DESC        
   SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01')        
   SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(@MonthStartDate)))       
       
   --- Checking any Latest month bill already generated or not ?    
       
    SET @IsHaveLatest = 0   
    SET @StatusText ='IsHaveLatest Condition'   
    IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND CONVERT(DATE,(CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01')) > @MonthStartDate)    
     BEGIN    
   SET @IsHaveLatest = 1    
     END    
    ELSE    
   SET @IsHaveLatest = 0    
    -------- Checking CUstomer Book No Disable or not ?   START    
    SET @NoFixed  = 0    
    SET @NoBill = 0     
    SET @IsDisabled = 0    
    SET @DisableType = 0    
    SET @IsPartialBill = 0    
    SET @IsActive = 0    
          
    IF(@ActiveStatusId = 1)    
     BEGIN        
          
   SELECT     
    @IsDisabled = IsDisable, @DisableType = DisableTypeId    
    ,@IsPartialBill = IsPartialBill,@IsActive = IsActive    
   FROM dbo.fn_GetCustomerBookDisable_Details(@AccountNumner,@Month,@Year)    
          
   IF(@IsDisabled = 1)    
    BEGIN    
     IF(@IsActive = 1)    
      BEGIN    
    IF(@DisableType = 1)    
     SET @NoBill = 1    
    ELSE    
     SET @NoFixed = 1    
      END    
     ELSE    
      BEGIN    
    IF(@IsPartialBill = 1)    
     SET @NoFixed = 1    
      END    
    END    
     END    
    -------- Checking CUstomer Book No Disable or not ?   END    
       
       
   --IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND (CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01') > @MonthStartDate)    
       
   IF (@IsHaveLatest = 0)    
    BEGIN --- IF THE LATEST BILLS NOT EXISTS FOR THIS CUSTOMER    
   IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year AND BillingTypeId = 3 AND ReadType = 3)    
     BEGIN -- IF SPOT BILLING Not Happens in this months    
     IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year)    
    BEGIN    
     DECLARE @PrevBillId INT,@TotalBillAmountWithTax DECIMAL(20,4)=0     
     SELECT @BillNo = BillNo,@PrevBillId = CustomerBillId,@TotalBillAmountWithTax=TotalBillAmountWithTax    
      FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year    
                  
     Update Tbl_CustomerReadings SET IsBilled = 0 WHERE BillNo = @BillNo      
     DELETE FROM Tbl_CustomerBillPaymentsApproval WHERE BillNo = @BillNo    
     DELETE FROM Tbl_CustomerBillPayments WHERE BillNo  = @BillNo    
     DELETE FROM Tbl_BillAdjustmentDetails WHERE BillAdjustmentId IN(SELECT BillAdjustmentId FROM Tbl_BillAdjustments WHERE CustomerBillId = @PrevBillId)    
     DELETE FROM Tbl_BillAdjustments WHERE CustomerBillId = @PrevBillId    
     DELETE FROM Tbl_PaidMeterPaymentDetails WHERE BillNo = @BillNo    
     UPDATE Tbl_BillAdjustments  SET EffectedBillId = NULL WHERE EffectedBillId = @PrevBillId    
     DELETE FROM TBL_Customer_Additionalcharges WHERE CustomerBillId = @PrevBillId     
     DELETE FROM Tbl_CustomerBills WHERE CustomerBillId = @PrevBillId --AccountNo = @AccountNumner AND BillMonth = @Month AND BillYear = @Year      
     --================================OLD STATEMENT FOR PREVIOUS BALANCE===================================================
     UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=OutStandingAmount-@TotalBillAmountWithTax WHERE GlobalAccountNumber=@AccountNumner-- {Modified Karthik}    
     --===================================================================================
     SELECT @OutStandingAmount = ISNULL(OutStandingAmount,0) FROM @CustomerMaster WHERE GlobalAccountNumber = @AccountNumner     
   
    END    
   IF(@NoBill = 0)    
    BEGIN    
      IF (@ActiveStatusId = 1) ---for Active customers energy units are calculated    
     BEGIN   
      IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date))--- For Readinf Customer which are have Readings        
		BEGIN          --If customer has reading
			 DECLARE @MeterMultiplier INT    
		      
			 SELECT @MeterMultiplier=(SELECT MeterMultiplier FROM Tbl_MeterInformation    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD WHERE GlobalAccountNumber=@AccountNumner))    
		                      
			 --SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate FROM Tbl_CustomerBills    
			 --WHERE AccountNo = @AccountNumner     
			 --AND CONVERT(DATE,BillGeneratedDate) <= CONVERT(DATE,@Date)     
			 --ORDER BY BillGeneratedDate DESC    
		      
			 SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate FROM Tbl_CustomerBills    
			 WHERE AccountNo = @AccountNumner     
			 ORDER BY CustomerBillId DESC    
		               
			 SELECT TOP(1) @PreviousReading = PreviousReading FROM Tbl_CustomerReadings         
			 WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0     
			 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date)     
			 AND (CONVERT(DATE,ReadDate) >= CONVERT(DATE,@LastBillGenerated) OR ISNULL(@LastBillGenerated,'') = '')    
			 ORDER BY CONVERT(DECIMAL(18),CustomerReadingId) ASC        
		                  
			 SELECT TOP(1) @CurrentReading = PresentReading,@ReadType = ReadType FROM Tbl_CustomerReadings         
			 WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0     
			 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date)     
			 AND (CONVERT(DATE,ReadDate) >= CONVERT(DATE,@LastBillGenerated) OR ISNULL(@LastBillGenerated,'') = '')    
			 ORDER BY  CONVERT(DECIMAL(18),CustomerReadingId) DESC        
		                  
			 ---- NEW Code    
			 SELECT @Usage = SUM(Usage) FROM Tbl_CustomerReadings         
			 WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0     
			 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date)     
			 AND (CONVERT(DATE,ReadDate) >= CONVERT(DATE,@LastBillGenerated) OR ISNULL(@LastBillGenerated,'') = '')    
			 ---- NEW Code    
		      
			 SET @Usage=@Usage*@MeterMultiplier--Applying Multiply factor for Usage.    
		                  
			 --SELECT @Usage = CONVERT(DECIMAL,@CurrentReading) - CONVERT(DECIMAL,@PreviousReading)  --OLD      
			 SET @RemaningUsage = @Usage        
			 SET @TotalAmount = 0         
			 SET @ReadCodeId  = 2--if customer has reading                            
    END      
      ELSE    
		BEGIN        
     SET @IsEstimatedRead = 1        
     SET @Usage = 0    
     SET @PreviousReading = NULL    
     SET @CurrentReading = NULL                
               
     IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE ReadCodeId = 1 AND GlobalAccountNumber = @AccountNumner)--- For Direct Customers        
     BEGIN --If customer is direct
		  SET @ReadCodeId  = 3       
		  DECLARE @BillingRule INT; 
		  SELECT @CustomerTariffId = TariffClassID, @ClusterCategoryId=ISNULL(ClusterCategoryId,0) FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber = @AccountNumner    
		  --Modified for avg reading from Tbl_DirectCustomersAvgReading --Neeraj 077
		  --Starts
		  SET @BillingRule=( SELECT      
							   BillingRule
									  FROM Tbl_EstimationSettings     
									  WHERE BillingMonth = @Month     
									  AND BillingYear = @Year     
									  AND TariffId = @CustomerTariffId     
									  AND CycleId = @CycleId     
									  AND ISNULL(ClusterCategoryId,0)=@ClusterCategoryId    
									  AND ActiveStatusId = 1 )
		  IF(@BillingRule=1)
		  BEGIN
			  SELECT      
			   @Usage = EnergytoCalculate     
			  FROM Tbl_EstimationSettings     
			  WHERE BillingMonth = @Month     
			  AND BillingYear = @Year     
			  AND TariffId = @CustomerTariffId     
			  AND CycleId = @CycleId     
			  AND ISNULL(ClusterCategoryId,0)=@ClusterCategoryId    
			  AND ActiveStatusId = 1 AND BillingRule = 1      
          END   
          --Ends
          IF(@BillingRule=2)
		  BEGIN
			 SET @Usage=(SELECT TOP 1 AverageReading FROM Tbl_DirectCustomersAvgReadingS WHERE GlobalAccountNumber=@AccountNumner ORDER BY AvgReadingId DESC)     
          END   
		  IF(@Usage IS NULL OR @Usage = 0)    
			   BEGIN     
				 SELECT @Usage = ISNULL(InitialBillingKWh,0) FROM [CUSTOMERS].[Tbl_CustomerActiveDetails] WHERE GlobalAccountNumber = @AccountNumner             
				 SET @ReadCodeId  = 1        
			   END    
     END        
     ELSE IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date) )--- For Reading Customer who are not have Readings(Estimated Customers)        
     BEGIN     --If cusotmers is non read
     
     --Calculating Avg usage for non read customers
		 --starts 
		 SET @ReadCodeId  = 5
		  DECLARE @TempBill TABLE (Usage DECIMAL(18,2))       
		  DELETE FROM @TempBill    
		  INSERT INTO @TempBill    
		  SELECT TOP(3) ISNULL(Usage,0) FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner     
		 AND CONVERT(DATE,(CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01')) < @MonthStartDate    
		 ORDER BY CustomerBillId DESC    
		  SELECT @Usage = SUM(Usage)/COUNT(0) FROM @TempBill    
		  --SELECT @Usage = (SELECT SUM(Usage) FROM Tbl_CustomerReadings WHERE AccountNumber = @AccountNumner AND IsBilled = 1 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date))    
		  --     /(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE AccountNumber = @AccountNumner AND IsBilled = 1 AND CONVERT(DATE,ReadDate) <= CONVERT(DATE,@Date))        
		  --Ends       
	--If usage is zero than take reading from INKW.
		 --starts    
       IF(@Usage IS NULL OR @Usage = 0.00)
      BEGIN      
		  SET @Usage = (CASE WHEN @Usage IS NULL THEN (SELECT InitialBillingKWh FROM [CUSTOMERS].[Tbl_CustomerActiveDetails] where GlobalAccountNumber = @AccountNumner) ELSE @Usage END)     
		  --SET @ReadCodeId  = 5             
		  SET @ReadCodeId  = 4
      END
      --Ends
       
     END         
                  
    END     
     END    
            
    SELECT TOP(1) @PrevCustomerBillId  = CustomerBillId FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner ORDER BY CustomerBillId DESC    
    
    if @PrevCustomerBillId IS NULL
		BEGIN
			SELECT @AdjustmentAmount = TotalAmountEffected FROM Tbl_BillAdjustments WHERE AccountNo = @AccountNumner and CreatedDate >=
			(select top 1 LastBillGeneratedDate from Tbl_LastBillGeneratedDate)
		END
	ELSE
		BEGIN
			 SELECT @AdjustmentAmount = TotalAmountEffected FROM Tbl_BillAdjustments WHERE AccountNo = @AccountNumner AND CustomerBillId = isnull(@PrevCustomerBillId,0)
		END
		
		if @AdjustmentAmount is null
		 SET @AdjustmentAmount=0		
		Set @Netarrears = @OutStandingAmount - @AdjustmentAmount
    
   
     --  SELECT @AdjustmentAmount = ISNULL(@AdjustmentAmount,0) + dbo.fn_GetCustomerOverpayAmount(@AccountNumner)   --- Added Over Payment Details    
    SELECT @PreviousDueAmount = (CASE WHEN @OutStandingAmount = 0 THEN (dbo.fn_GetCustomerTotalDueAmount(@AccountNumner)) ELSE @OutStandingAmount END)    
    SELECT @BalanceUnits = ISNULL(BalanceUsage,0) FROM Tbl_CustomerBills WHERE AccountNo = @AccountNumner AND CustomerBillId = @PrevCustomerBillId    
    SET @BalanceUnits = ISNULL(@BalanceUnits,0)    
             
    -- This is for Estimation billing for read customers start    
     IF(@IsEstimatedRead = 1)    
      BEGIN    
    SET @RemainingBalanceUnits = @BalanceUnits + @Usage      
      END    
     ELSE    
      BEGIN    
    IF(@BalanceUnits > @Usage)    
     BEGIN    
      SET @Usage = 0    
      SET @RemainingBalanceUnits = @BalanceUnits - @Usage      
     END         
    ELSE    
     BEGIN    
      SET @Usage = @Usage - @BalanceUnits    
      SET @RemainingBalanceUnits = 0    
     END     
      END    
     -- This is for Estimation billing for read customers end    
             
    SELECT @TotalAmount = dbo.fn_CaluculateBill_Consumption(@AccountNumner,@Usage,@Month,@Year)    
              
    IF(@BalanceUnits > 0 AND @RemainingBalanceUnits >= 0 AND @Usage = 0 AND @ActiveStatusId = 1)    
      BEGIN    
    SET @ReadCodeId  = 4     
      END          
     IF (@ActiveStatusId = 2)--- for DeActive Customers Energy units are Zero    
     BEGIN    
      SET @PreviousReading = NULL       
      SET @CurrentReading    = NULL    
      SET @TotalAmount = 0    
     END       
           
     SET @StatusText ='Insert Tbl_CustomerBills'   
     INSERT INTO Tbl_CustomerBills([AccountNo]        
     --,[CustomerId]        
     ,[TotalBillAmount]        
     ,[ServiceAddress]        
     ,[MeterNo]        
     ,[Dials]        
     ,[NetArrears]        
     ,[NetEnergyCharges]        
     ,[NetFixedCharges]        
     ,[VAT]        
     ,[VATPercentage]        
     ,[Messages]        
     ,[BU_ID]        
     ,[SU_ID]        
     ,[ServiceCenterId]        
     --,[SubStationId]        
     --,[FeederId]        
     --,[TransFormerId]        
     ,[PoleId]        
     ,[BillGeneratedBy]        
     ,[BillGeneratedDate]        
     ,PaymentLastDate        
     ,[TariffId]        
     ,[BillYear]        
     ,[BillMonth]        
     ,[CycleId]        
     ,[TotalBillAmountWithArrears]        
     ,[ActiveStatusId]        
     ,[CreatedDate]        
     ,[CreatedBy]        
     ,[ModifedBy]        
     ,[ModifiedDate]        
     ,[BillNo]        
     ,PaymentStatusID        
     ,[PreviousReading]        
     ,[PresentReading]        
     ,[Usage]        
     ,[AverageReading]        
     ,[TotalBillAmountWithTax]        
     ,[EstimatedUsage]        
     ,[ReadCodeId]        
     ,[ReadType]    
     ,AdjustmentAmmount    
     ,BalanceUsage    
     ,BillingTypeId    
     )        
     SELECT GlobalAccountNumber    
      --,CustomerUniqueNo    
      ,0    
      ,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
      ,C.MeterNumber    
    ,(select mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials  --,M.MeterDials     
    ,0        
    ,@TotalAmount
    ,0
    ,0
    ,@Tax        
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
    ,BU_ID,SU_ID,ServiceCenterId    
    --,InjectionSubStationId        
    --,FeederId    
    --,TransformerId    
    ,PoleId        
    ,@BillGeneratedBY        
    ,(SELECT dbo.fn_GetCurrentDateTime())        
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
    ,TariffId,@Year,@Month,@CycleId
    ,0
    ,1        
    ,(SELECT dbo.fn_GetCurrentDateTime())        
    ,@BillGeneratedBY        
    ,NULL,NULL        
    ,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))        
    ,2    
    ,@PreviousReading        
    ,@CurrentReading        
    ,@Usage        
    ,0,0              
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)        
    ,@ReadCodeId,@ReadType        
    ,@AdjustmentAmount    
    ,@RemainingBalanceUnits    
    --,(CASE WHEN @ReadCodeId = 3 THEN (@RemainingBalanceUnits + @Usage) ELSE @RemainingBalanceUnits END)    
    ,2    
      FROM @CustomerMaster C        
      WHERE GlobalAccountNumber = @AccountNumner  
      
     
     -- AND C.ActiveStatusId IN(1,2) --AND M.ActiveStatusId = 1 --AND T.IsAcitive = 1        
                   
     SET @CustomerBillId = SCOPE_IDENTITY()         
               
     IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId )        
    BEGIN           
      IF(@NoFixed = 0)    
    BEGIN    
     DECLARE @PaidMeterBalance DECIMAL(18,2) = 0    
       ,@DiscountFixedCharges DECIMAL(18,2) = 0    
     SELECT @PaidMeterBalance = dbo.fn_GetPaidMeterCustomer_Balance(@AccountNumner)    
     IF(@PaidMeterBalance > 0)    
      BEGIN    
       DECLARE @ResultedTable TABLE(ChargeId INT,ChargeAmount DECIMAL(18,2),EffectedAmount DECIMAL(18,2)    
       --,CustomerUniqueNo VARCHAR(50)    
       ,AccountNo VARCHAR(50),ClassID INT)    
       DELETE FROM @ResultedTable     
       INSERT INTO @ResultedTable    
       SELECT     
     ChargeId,ChargeAmount,EffectedAmount    
     --,CustomerUniqueNo    
     ,AccountNo,ClassID    
       FROM dbo.fn_GetFixedCharges_PaidMeters(@AccountNumner,@MonthStartDate,@PaidMeterBalance)    
      
       INSERT INTO TBL_Customer_Additionalcharges(AccountNO,CustomerBillId,TariffId,Amount,CreatedBy,CreatedDate,ModifiedBY,ModifiedDate,ChargeId)         
       SELECT     
     --CustomerUniqueNo,    
     AccountNo,@CustomerBillId,ClassID,(A.ChargeAmount - A.EffectedAmount),NULL,GETDATE(),NULL,NULL,ChargeId     
       FROM @ResultedTable A        
                 
       SELECT @DiscountFixedCharges = SUM(EffectedAmount) FROM @ResultedTable    
                 
       INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)    
       SELECT     
     AccountNo,MeterNo    
     ,@DiscountFixedCharges    
     ,BillNo,@BillGeneratedBY,dbo.fn_GetCurrentDateTime()    
       FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId    
       --Commented below line. Not to save paid meter as adjustment NEERAJ-ID 077 11-MARCH-15
       --UPDATE Tbl_CustomerBills SET AdjustmentAmmount =(ISNULL(AdjustmentAmmount,0) - @DiscountFixedCharges) WHERE CustomerBillId = @CustomerBillId    
       UPDATE Tbl_PaidMeterDetails SET OutStandingAmount = (@PaidMeterBalance - @DiscountFixedCharges) WHERE AccountNo=@AccountNumner    
      END    
     ELSE    
      BEGIN               
       INSERT INTO TBL_Customer_Additionalcharges(AccountNO,CustomerBillId,TariffId,Amount,CreatedDate,ChargeId)         
       SELECT     
     --C.CustomerUniqueNo,    
     GlobalAccountNumber,@CustomerBillId,TariffId,A.Amount,GETDATE(),ChargeId     
       FROM Tbl_LAdditionalClassCharges A        
       JOIN @CustomerMaster C ON A.ClassID = C.TariffId        
       WHERE IsActive = 1 AND C.GlobalAccountNumber = @AccountNumner        
       AND ChargeId IN (SELECT ChargeId FROM Tbl_MChargeIds CI WHERE CI.IsAcitve = 1)    
       AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)      
      END    
    END            
      ---For Getting NetArrears        
      UPDATE Tbl_CustomerBills SET NetArrears = @Netarrears--(ISNULL(@PrevBillAmount,0) - ISNULL(@PaidAmount,0))     
       WHERE CustomerBillId = @CustomerBillId        
               
      ---For Getting NetFixedCharges         
      UPDATE Tbl_CustomerBills SET NetFixedCharges = (SELECT SUM(Amount) FROM TBL_Customer_Additionalcharges WHERE CustomerBillId = @CustomerBillId) WHERE CustomerBillId = @CustomerBillId        
      ---For Getting TotalBillAmount         
      UPDATE Tbl_CustomerBills SET TotalBillAmount = (ISNULL(NetEnergyCharges,0) + ISNULL(NetFixedCharges,0)) WHERE CustomerBillId = @CustomerBillId        
             
      DECLARE @IsRequireTax BIT=1    
             
      IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNumner AND IsEmbassyCustomer=1)    
      BEGIN            
    IF((SELECT IsEmbassyHaveTax FROM Tbl_CompanyDetails WHERE CompanyId=1)=0)    
     SET @IsRequireTax=0    
      END    
             
      ---For Getting VAT        
      IF(@IsRequireTax=1)    
    UPDATE Tbl_CustomerBills SET VAT = ((5*TotalBillAmount)/100) WHERE CustomerBillId = @CustomerBillId        
      ---For Getting TotalBillAmountWithTax         
      UPDATE Tbl_CustomerBills SET TotalBillAmountWithTax = (TotalBillAmount + VAT) WHERE CustomerBillId = @CustomerBillId        
      ---For Getting TotalBillAmountWithArrears         
       
       DECLARE @TotalBillWithArr DECIMAL(18,2)
       SELECT @TotalBillWithArr=(TotalBillAmountWithTax + NetArrears)FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
      UPDATE Tbl_CustomerBills SET TotalBillAmountWithArrears = @TotalBillWithArr WHERE CustomerBillId = @CustomerBillId         
       
       ----- Updating customer out standing amount for 1st time
       UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=@TotalBillWithArr WHERE GlobalAccountNumber=@AccountNumner      
      --For Getting Average Reading        
      UPDATE Tbl_CustomerBills SET AverageReading = (SELECT SUM(B.Usage) FROM Tbl_CustomerBills B WHERE AccountNo = @AccountNumner)/(SELECT COUNT(0) FROM Tbl_CustomerBills B WHERE AccountNo = @AccountNumner)        
    WHERE CustomerBillId = @CustomerBillId        
                
     IF(@AdjustmentAmount IS NOT NULL)    
      BEGIN    
    UPDATE Tbl_BillAdjustments SET EffectedBillId = @CustomerBillId WHERE CustomerBillId = @PrevCustomerBillId AND  AccountNo = @AccountNumner     
      END    
 
      
      ------For Getting Adjustment Values    
      --UPDATE Tbl_CustomerBills SET AdjustmentAmmount = @AdjustmentAmount WHERE CustomerBillId = @CustomerBillId    
              
      ---If The Customer having Balance amount then the bill status is     
      IF((SELECT ISNULL(AdjustmentAmmount,0) - (ISNULL(NetArrears,0) + ISNULL(TotalBillAmountWithTax,0)) FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId) >= 0)    
    BEGIN    
     UPDATE Tbl_CustomerBills SET PaymentStatusID = 1 WHERE CustomerBillId = @CustomerBillId    
    END        
              
      ---For Update IsBilled Status For Bill Generation Customers        
     Update Tbl_CustomerReadings SET IsBilled = 1     
        ,BillNo = (SELECT BillNo FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId)    
        ,BilledDate = (SELECT dbo.fn_GetCurrentDateTime())    
        ,BilledBy = @BillGeneratedBY     
        ,UsageForBilling = @Usage        
     WHERE GlobalAccountNumber = @AccountNumner         
     AND CONVERT(DECIMAL,PreviousReading) >= CONVERT(DECIMAL,@PreviousReading)        
     AND CONVERT(DECIMAL,PresentReading) <= CONVERT(DECIMAL,@CurrentReading)         
             
      Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 3     
     WHERE AccountNo = @AccountNumner AND BillingQueuescheduleId = @BillingQueuescheduleId-- BillGenarationStatusId = 5        
               
     ---- Update  Highest Consumption Value for Customer  
     IF ((SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@AccountNumner)>=2 )
     BEGIN
		 Update [CUSTOMERS].[Tbl_CustomerActiveDetails]     
		 SET Highestconsumption = (dbo.fn_GetCustomerHighConsumption(@AccountNumner))    
		 ,OutStandingAmount = (SELECT (TotalBillAmountWithArrears-(AdjustmentAmmount-@DiscountFixedCharges)) FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId)    
		 WHERE GlobalAccountNumber = @AccountNumner  
     END     
  --    --================================OLD STATEMENT FOR PREVIOUS BALANCE=============================NEERAJ ID077 19-FEB-15======================
  --    DECLARE @LastBill VARCHAR(50)
		--		,@PreviousBillAmount DECIMAL(18,2)=0.00
		--SELECT TOP 1 @LastBill=CustomerbillID FROM Tbl_CustomerBills 
		--WHERE AccountNo=@AccountNumner
		--ORDER BY BILLGENERATEDDATE
		--IF(@LastBill!=0.00)
		--BEGIN	
		--SELECT @PreviousBillAmount= TotalBillAmountWithTax FROM Tbl_CustomerBills 
		--WHERE CustomerBillId=@LastBill
		--UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=@PreviousBillAmount WHERE GlobalAccountNumber=@AccountNumner-- {Modified Karthik}    
	
		--	--UPDATE [CUSTOMERS].Tbl_CustomerActiveDetails SET OutStandingAmount=-@TotalBillAmountWithTax WHERE GlobalAccountNumber=@AccountNumner
		--END
     
  --   --===================================================================================
               
               SET @AdjustmentAmount = NULL 
               
      INSERT INTO @GeneratedCustomerBills(CustomerBillId)VALUES(@CustomerBillId)         
    END        
     END        
     END        
    END    
     IF(@AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
    BREAK        
     ELSE        
    BEGIN        
   SET @AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @AccountNumner ORDER BY AccountNo ASC)        
   IF(@AccountNumner IS NULL) break;        
     Continue        
    END        
    END         
    SET @COUNTER=@COUNTER+1  
   END  
  SET @RowsEffected=1
   
   --=================================Follwoing select statement required for bill prit==============================================
   --SELECT         
   --(        
    SELECT         
     CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     --,(CD.Name +SPACE(1)+ ISNULL(SurName,'')) AS Name     
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name      
     ,CB.CustomerId        
     ,CB.TotalBillAmount        
     --,(CASE WHEN CB.ServiceAddress IS NULL THEN 'N/A' ELSE CB.ServiceAddress END)AS ServiceAddress
     ,(dbo.fn_GetCustomerServiceAddress(CB.AccountNo)) AS ServiceAddress
     ,(CASE WHEN CB.MeterNo IS NULL THEN 'N/A' ELSE CB.MeterNo END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,CB.NetArrears AS NetArrears    -- Modified By Padmini
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN 0.00 ELSE CB.NetEnergyCharges END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN 0.00 ELSE CB.NetFixedCharges END)AS NetFixedCharges
     ,CB.VAT        
     ,CB.VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(10),CB.PaymentLastDate) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(10),CB.PaymentLastDate) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,CB.TotalBillAmountWithArrears        
     ,(CASE WHEN CB.PreviousReading IS NULL THEN 'N/A' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN 'N/A' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,CB.TotalBillAmountWithTax         
     ,B.BusinessUnitName        
     ,PAD.HouseNo AS ServiceHouseNo    
     ,PAD.StreetName AS ServiceStreet        
     ,PAD.City AS ServiceCity        
     ,PAD.ZipCode AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     , case when CB.AdjustmentAmmount  <0 then  CONVERT(varchar(20),CB.AdjustmentAmmount) + ' CR ' ELSE  CONVERT(varchar(20),CB.AdjustmentAmmount) + ' DR ' END  as AdjustmentAmmount
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = CB.TariffId AND IsActiveClass = 1) AS TariffName
     --,(SELECT ClassName FROM Tbl_CustomerBills CB JOIN Tbl_MTariffClasses TC ON CB.TariffId=TC.ClassID
     --  WHERE AccountNo=CB.AccountNo)AS TariffName
     ,ISNULL(CD.OldAccountNo,'----') AS OldAccountNo    
     ,CASE WHEN ((SELECT TOP(1) CONVERT(VARCHAR(30),ReadDate,103) FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC)) IS NULL THEN 'N/A' ELSE (SELECT TOP(1) CONVERT(VARCHAR(30),ReadDate,103) FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC) END  AS ReadDate        
     ,CASE WHEN ((SELECT TOP(1) Multiplier FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC)) IS NULL THEN 0 ELSE (SELECT TOP(1) Multiplier FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CB.AccountNo AND CONVERT(DECIMAL,CR.PresentReading) = CONVERT(DECIMAL,CB.PresentReading) ORDER BY ReadDate DESC) END  AS Multiplier         
     ,CASE WHEN ((SELECT TOP(1) CONVERT(VARCHAR(30),CreatedDate,103) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC))IS NULL THEN 'N/A' ELSE (SELECT TOP(1) CONVERT(VARCHAR(30),CreatedDate,103) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC)END AS LastPaymentDate        
     --,CASE WHEN ((SELECT TOP(1 )CONVERT(DECIMAL(18,2),PaidAmount) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC)) IS NULL THEN 0 ELSE ((SELECT TOP(1 )CONVERT(DECIMAL(18,2),PaidAmount) FROM Tbl_CustomerPayments CP WHERE CP.AccountNo = CB.AccountNo ORDER BY CreatedDate DESC))END AS LastPaidAmount        
    , (select dbo.fn_GetPreviuosTotalBillPayment(cb.AccountNo) )as LastPaidAmount
     --,CD.OutStandingAmount AS PreviousBalance 
     ,[dbo].[fn_GetCustomerLastBillAmountWithArrears](cb.AccountNo,cb.BillMonth,cb.BillYear) as PreviousBalance             
     --,(SELECT TOP(1 )CONVERT(DECIMAL(18,2),NetArrears) FROM Tbl_CustomerBills B WHERE B.AccountNo = CB.AccountNo AND B.BillNo < CB.BillNo ORDER BY CreatedDate DESC)AS PreviousBalance        
     ,C.CycleId
     ,1 AS RowsEffected
     --,CASE WHEN(SELECT TOP 1  dbo.fn_PaymentSumAfterLastBill(CD.GlobalAccountNumber,TBCB.BillMonth,TBCB.BillYear,CB.BillMonth,CB.BillYear))IS NULL THEN 0.00 ELSE (SELECT TOP 1 dbo.fn_PaymentSumAfterLastBill(CD.GlobalAccountNumber,TBCB.BillMonth,TBCB.BillYear,CB.BillMonth,CB.BillYear)) END AS TotalPayment
      ,(select dbo.fn_GetPreviuosTotalBillPayment(cb.AccountNo))AS TotalPayment
     --,'1005' AS PostalHouseNo        
     --,'Madhapur' AS PostalStreet        
     --,'500008' AS PostalZipCode
     ,(SELECT TOP 1 TCPA.ZipCode FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails AS TCPA WHERE TCPA.GlobalAccountNumber=CB.AccountNo AND IsServiceAddress=0 AND IsActive=1) AS PostalZipCode
     ,[dbo].[fn_GetCustomerPostalAddress](CB.AccountNo) AS PostalAddress 
     ,dbo.fn_GetCustomerTotalDueAmount(CB.AccountNo) AS TotalDueAmount 
    FROM Tbl_CustomerBills CB        
    JOIN @CustomerMaster CD ON CB.AccountNo = CD.GlobalAccountNumber        
    LEFT JOIN Tbl_BookNumbers BC ON CD.BookNo = BC.BookNo    
    LEFT JOIN Tbl_Cycles C ON BC.CycleId = C.CycleId    
    LEFT JOIN Tbl_BussinessUnits B ON CB.BU_ID = B.BU_ID    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD ON PAD.GlobalAccountNumber=CD.GlobalAccountNumber  
     LEFT JOIN Tbl_CustomerBills AS TBCB ON TBCB.AccountNo=CD.GlobalAccountNumber AND TBCB.BillMonth =DATEPART(MONTH, DATEADD(MONTH,-1,CONVERT(DATE,'2014-'+convert(VARCHAR(2),CB.BillMonth)+'-01')))  
    --LEFT JOIN Tbl_MeterInformation MTR ON CD.MeterNumber=MTR.MeterNo
    WHERE CB.CustomerBillId IN (SELECT CustomerBillId FROM @GeneratedCustomerBills) 
    Order by C.CycleName -- Modified because we are getting cyclename in view also    
    ,BC.SortOrder,CD.SortOrder ASC    
   -- FOR XML PATH('BillGenerationList'),TYPE        
   --)        
   --FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
   DROP TABLE #inputList     
--   COMMIT TRAN   
     
--END TRY   
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @RowsEffected=0
--END CATCH   

--SELECT @RowsEffected AS RowsEffected
--FOR XML PATH('BillGenerationBe'),TYPE
END 
-------------------------------------------------------------------------------------------------------------------------
GO
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				 ,ISNULL(PresentReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerDescription  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 ,@OldMeterNo VARCHAR(50)
				 ,@InitialBillingkWh INT
				 ,@OldMeterReadingDate DATETIME
				 ,@NewMeterReadingDate DATETIME
				 ,@NewMeterInitialReading VARCHAR(20)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
			,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
			,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
			,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
			
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading
														,MeterChangedDate)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET
				InitialReading=@InitialBillingkWh, PresentReading=@NewMeterReading
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
		SET @ReadBy=(SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		
		
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@OldMeterReadingDate
									,@ReadBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@OldMeterNo)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		--If New MeterNo assighned to that customer
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading=@NewMeterReading,
			InitialReading=@NewMeterInitialReading
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE @NewMeterUsage VARCHAR(50)
		SET @NewMeterUsage=	(ISNULL(CONVERT(BIGINT,@NewMeterReading),0)) - (ISNULL(CONVERT(BIGINT,@NewMeterInitialReading),0))
		
		IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
			BEGIN		
				DECLARE @NewMeterDials INT		
				SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@NewMeterReadingDate
									,@ReadBy									
									,@NewMeterInitialReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@NewMeterUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@NewMeterUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@MeterNo)
			END
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/11/2015 13:51:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
    
    
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	  ,CD.GlobalAccountNumber  AS AccountNo
	 , CD.OldAccountNo AS OldAccountNo
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(TempDetails.AccountNo,TempDetails.ReceivedDate,TempDetails.AmountPaid) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
    INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END 
GO
GO
-- =============================================    
-- Author  : Faiz-ID103  --------(Bhargav)
-- Create date : 11-Mar-2015    
-- Description : The purpose of this procedure is to Get Customer Statistics  by Tariff
-- =============================================    
CREATE PROCEDURE [dbo].[USP_CustomerStatisticsByTariff]  
(
@XmlDoc XML    
)
AS    
BEGIN    
   
 DECLARE	@BU_ID VARCHAR(20)
			,@Month INT  
			,@Year INT  
   
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@Month = C.value('(Month)[1]','INT')
		,@Year = C.value('(Year)[1]','INT')
FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)    
     
 CREATE TABLE #CustomerStatistics 
(	EnergyInkWh DECIMAL(18,2),
	EnergyCharges DECIMAL(18,2),
	RevenueBilled DECIMAL(18,2),
	Tariff VARCHAR(50),
	CustomerStatus VARCHAR(50)
 )
INSERT INTO #CustomerStatistics
SELECT	CBills.Usage,
		CBills.NetEnergyCharges,
		CBills.NetEnergyCharges + CBills.NetFixedCharges AS RevenueBilled,
		Cd.ClassName,CD.ActiveStatus 
FROM tbl_customerbills CBills  
INNER  JOIN UDV_CustomerDescription CD ON
CBills.BillMonth=1 AND CBills.BillYear=2015 AND CD.BU_ID='BEDC_BU_0024'
AND CD.GlobalAccountNumber=CBills.AccountNo
 
SELECT		Tariff,
			SUM(EnergyBilled) AS EnergyBilled ,
			SUM(RevenueBilled) As RevenueBilled  ,
			SUM(ISNULL(Active,0) ) AS Active ,
			SUM(ISNULL(InActive,0)) AS InActive,
			SUM (ISNULL(Hold,0) ) AS Hold,
			SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation
FROM
(
SELECT		count(CustomerStatus) AS TotalCustomerTariff ,
			CustomerStatus ,
			SUM(EnergyInkWh) AS EnergyBilled , 
			SUM(EnergyCharges) AS RevenueBilled,
			Tariff 
FROM #CustomerStatistics
GROUP BY	CustomerStatus,
			Tariff
 ) ListOfCustomers
 PIVOT ( sum(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
 ) as  ListOfCustomersT
GROUP BY Tariff

 ORDER BY Tariff
  
  
  --DROP table #CustomerStatistics
       
END    
  
  
---------------------------------------------------------------------------------------------------------  
    GO
-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 26-DEC-2014  
-- Description: The purpose of this procedure is to Close the batch (BatchPaymentStatus)  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CloseBatchPayment]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @BatchNo INT  
   ,@amount decimal(18,4)=0  
     
 SELECT  @BatchNo=C.value('(BatchNo)[1]','INT')        
 FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
  
 SET @amount=ISNULL((SELECT SUM(PaidAmount) as TotalPaidAmmount   
     FROM Tbl_CustomerPayments   
     WHERE BatchNo=@BatchNo   
     GROUP BY BatchNo),0)  
       
if(@amount IS NOT NULL)    
BEGIN    
 UPDATE Tbl_BatchDetails   
 SET  BatchTotal=@amount   
 WHERE BatchNo=@BatchNo  
  
 SELECT @@ROWCOUNT AS RowsEffected   
 FOR XML PATH('BatchStatusBE')  
END  
ELSE  
 BEGIN  
  SELECT -1 AS RowsEffected   
  FOR XML PATH('BatchStatusBE')  
 END  
END  
  
  
  -----------------------------------------------------------------------------------------------------
  
    GO
-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 27-DEC-2014  
-- Description: The purpose of this procedure is to Close the batch (BatchAdjustmentStatus)  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CloseBatchAdjustment]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @BatchID INT  
   ,@amount decimal(18,4)=0  
     
 SELECT  @BatchID=C.value('(BatchID)[1]','INT')        
 FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
  
 SET @amount=(ISNULL((SELECT SUM(ABS(TotalAmountEffected)) as TotalPaidAmmount   
     FROM Tbl_BillAdjustments   
     WHERE BatchNo=@BatchID   
     GROUP BY BatchNo),0))  
       
IF(@amount IS NOT NULL)    
BEGIN    
 UPDATE Tbl_BATCH_ADJUSTMENT   
 SET  BatchTotal=@amount   
 WHERE BatchID=@BatchID  
  
 SELECT @@ROWCOUNT AS RowsEffected   
 FOR XML PATH('BatchStatusBE')  
END  
ELSE  
 BEGIN  
  SELECT -1 AS RowsEffected   
  FOR XML PATH('BatchStatusBE')  
 END  
END  
GO
------------------------------------------------------------------------------------------