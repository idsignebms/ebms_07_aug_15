GO
-- =============================================          
-- CREATE By : NEERAJ      
-- CREATED Date: 13-MAR-2015
-- DESC			: Get last close month
-- =============================================          
ALTER PROCEDURE [dbo].[USP_GetLastCloseMonth]  
AS          
BEGIN 
	SELECT TOP 1 CONVERT(VARCHAR(5),[YEAR]) + ' ' +DateName( month , DateAdd( month , [MONTH] , 0 ) - 1 ) AS LastOpenMonth FROM Tbl_BillingMonths
	WHERE OpenStatusId=2
	ORDER BY ModifiedDate DESC
END	

SELECT * FROM CUSTOMERS.Tbl_CustomerActiveDetails
--SELECT  FROM CUSTOMERS.Tbl_CustomerSDetail
SELECT ClusterCategoryId,TariffClassID,BookNo FROM CUSTOMERS.Tbl_CustomerProceduralDetails
SELECT HouseNo,StreetName,City,Landmark,Details,AreaCode,IsServiceAddress FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------\  */
ALTER VIEW [UDV_CustomerDescription]
AS
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs, 
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo, 
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId, 
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID, 
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName, 
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading, 
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading, 
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo

GO



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerDescription  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				 ,ISNULL(CR.PresentReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerDescription  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------
-- =============================================  
-- Author:  Bhimaraju V 
-- Create date: 12-03-2014 5 
-- Description: Get Details For PaymentEntry-AccountWise
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)
	SELECT @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('PaymentsBe') as T(C)	

		  SELECT     CD.GlobalAccountNumber
					,CD.OldAccountNo
					,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
					,MeterNumber
					,ClassName
					,dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber)AS ServiceAddress 
					,OutStandingAmount
					,ISNULL(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber),'--') AS LastPaidDate
					,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS LastPaidAmount
					,ISNULL(dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber),'--') AS LastBillGeneratedDate		
					,dbo.fn_GetCustomerLastBillAmountWithoutArrears(CD.GlobalAccountNumber) AS LastBillAmount
					,1 AS IsExists
		FROM UDV_CustomerDescription CD
		WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo
		FOR XML PATH('PaymentsBe')
	 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBookNumbers]    Script Date: 03/13/2015 13:07:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Description: The purpose of this procedure is to get list of Book Numbers    
-- Modified By: V.BhimaRaju    
--Modified Date: 27-02-2014    
--Modified Date: 28-03-2014    
-- Modified By: T.Karthik  
-- Modified Date: 27-10-2014  
-- Description: To Wrong in Table and For --- in Details    
-- Modified By: Padmini  
-- Modified Date:17-Feb-2015  
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBookNumbers]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT      
   ,@BUID VARCHAR(50)  
      
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY BN.ActiveStatusId ASC , BN.CreatedDate DESC ) AS RowNumber    
    ,BN.ID AS BookNo  
    ,(SELECT dbo.fn_GetCycleIdByBook(BN.BookNo)) As CycleId  
    ,(SELECT CycleName From Tbl_Cycles WHERE CycleId=(SELECT dbo.fn_GetCycleIdByBook(BookNo))) CycleName  
    ,BN.ActiveStatusId    
    ,BN.CreatedBy    
    ,BN.NoOfAccounts    
    --,Details    
    ,ISNULL(BN.Details,'---')AS Details    
    ,BU.BU_ID    
    ,SU.SU_ID    
    ,SC.ServiceCenterId    
    ,ISNULL(BN.BookCode,'---')AS BookCode    
    ,BU.BusinessUnitName AS BusinessUnitName    
    ,SU.ServiceUnitName  AS ServiceUnitName    
    ,SC.ServiceCenterName AS ServiceCenterName  
 ,MarketerId  
 ,ISNULL(dbo.fn_GetMarketersFullName(MarketerId),'--') AS Marketer  
 ,ST.StateName     
 FROM Tbl_BookNumbers AS BN  
 JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
    JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
    JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
    JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
 LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode  
   WHERE (BU.BU_ID=@BUID OR @BUID='')  
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END    
-----------------------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerDescription CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerDescription CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum 
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading
				  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
				  --,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerDescription C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
-----------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 10-10-2014
-- Description  : This method is used to insert payment details by accountNo.
-- =============================================
ALTER PROCEDURE [USP_InsertPayments_ByAccountNo]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @AccountNo VARCHAR(50)
			,@CreatedBy VARCHAR(50)
			,@PaidAmount DECIMAL(20,4)
			,@PaymentRecievedDate VARCHAR(20)
			,@CustomerID VARCHAR(50)
			,@EffectedRows INT
			,@CurrentOutStanding DECIMAL(18,2)
			
	SELECT   @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
			,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
			,@PaymentRecievedDate = C.value('(PaymentReceivedDate)[1]','VARCHAR(20)')
			,@PaidAmount = C.value('(PaidAmount)[1]','DECIMAL(20,4)')
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)	
	
	--SELECT @AccountNo = AccountNo FROM Tbl_CustomerDetails WHERE ( AccountNo=@AccountNo OR OldAccountNo = @AccountNo)
	--SELECT @CustomerID = CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo

	SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE ( GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo)
	
	INSERT INTO Tbl_CustomerPayments(
					 --CustomerID
					 AccountNo
					,CreatedBy
					,CreatedDate
					,RecievedDate
					,PaidAmount
					,ActivestatusId
					)
			VALUES(
					--@CustomerID
					 @AccountNo
					,@CreatedBy
					,dbo.fn_GetCurrentDateTime()
					,@PaymentRecievedDate
					,@PaidAmount
					,1
				)
		
		SELECT @EffectedRows = @@ROWCOUNT	
		
		--SET @CurrentOutStanding=ISNULL((SELECT OutStandingAmount FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo),0)
		
		--UPDATE Tbl_CustomerDetails	
		--	SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @PaidAmount
		--		,ModifiedBy = @CreatedBy
		--		,ModifiedDate = dbo.fn_GetCurrentDateTime()
		--WHERE AccountNo=@AccountNo		
		
		UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]				
		SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @PaidAmount
			,ModifedBy = @CreatedBy
			,ModifiedDate = dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo	

		SELECT 
			(CASE WHEN @EffectedRows > 0 THEN 1 ELSE 0 END) AS EffectedRows
		FOR XML PATH ('PaymentsBe')
		
	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-03-2015
-- Description:	The purpose of this function is to get customer
--				 Previous Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [fn_GetCustPrevReadingByReadDate_New]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
	,@MeterNumber VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousReading VARCHAR(50)=''

	SET @PreviousReading=(SELECT CASE WHEN (SELECT TOP(1) PresentReading 
							FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber=@GlobalAccountNumber 
							AND CR.MeterNumber=@MeterNumber
							AND CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  
						 order by CustomerReadingId desc)  
					   IS NULL  
					   THEN  CONVERT(VARCHAR(50),ISNULL((SELECT PresentReading FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber=@GlobalAccountNumber),0))  
					   ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   
					  CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END 
					  )
	RETURN @PreviousReading	

END
GO
GO
/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 03/12/2015 12:52:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@ReadCodeID VARCHAR(50) 
		,@GlobalAccountNumber VARCHAR(50) 
		,@IsSuccessful BIT =1
		,@StatusText VARCHAR(200)
		,@InitialReading INT

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	SET @StatusText='Deserialization'
	 SELECT  
	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
	SET @StatusText='Update Statement'
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
	SET MeterNumber=@MeterNumber
		,ReadCodeID=@ReadCodeID
	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	SET InitialReading=@InitialReading
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('CustomerRegistrationBE'),TYPE

END  
GO

GO

-- =============================================        
-- Author:  Padmini        
-- Create date: 05/02/2015        
-- Description: This Procedure is used to Fetching details for Customer Address Change Log      
-- MODIFIED BY: NEERAJ    
-- DESCRIPTION: ADDED FILTERED ADDRESS BY ISACTIVE    
-- DATE    : 7-FEB-15     
-- MODIFIED BY: Bhimaraju    
-- DESCRIPTION: ADDED BookNo details    
-- DATE    : 7-FEB-15      
-- Modified By: Padmini    
-- Modified Date:17-Feb-2015    
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order    
-- Modified By: Faiz-ID103    
-- Modified Date: 13-Mar-2015    
-- Description: Searching with old account no also added 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerAddress]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN        
        
 DECLARE @GlobalAccountNo VARCHAR(50)        
               
    SELECT @GlobalAccountNo=C.value('GolbalAccountNumber[1]','VARCHAR(50)')             
    FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
                
   --         --1.Log Writing        
   --INSERT INTO Tbl_CustomerAddressChangeLogs( AccountNo                    
   --              ,OldHouseNo                     
   --             ,OldStreetName        
   --             ,OldCity        
   --             ,OldAreaCode        
   --             ,OldZipCode        
   --             ,OldPostalAddressID        
   --             ,IsServiceAddress         
   --            )        
   -- --- Fetching Postal Address Details For Insertion                    
   --SELECT        
   -- GlobalAccountNumber        
   -- ,CASE HouseNo WHEN '' THEN NULL ELSE HouseNo END         
   -- ,CASE StreetName WHEN '' THEN NULL ELSE StreetName END          
   -- ,CASE City WHEN '' THEN NULL ELSE City END         
   -- ,CASE AreaCode WHEN '' THEN NULL ELSE AreaCode END         
   -- ,CASE ZipCode WHEN '' THEN NULL ELSE ZipCode END         
   -- ,CASE AddressID WHEN '' THEN NULL ELSE AddressID END        
   -- ,IsServiceAddress        
   --FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]        
   --WHERE GlobalAccountNumber=@GlobalAccountNo         
           
 SELECT(     -- 2.Fetching Customer Basic List To Display --        
   SELECT         
    CD.GlobalAccountNumber  AS GolbalAccountNumber      
    ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)AS ContactName        
    ,CD.HomeContactNo AS HomeContactNumber        
    ,CD.BusinessContactNo AS BusinessContactNumber    
    ,CD.AccountNo AS AccountNo    
    ,CD.ClassName AS Tariff    
    ,CD.MeterNumber    
    ,CD.OldAccountNo    
    --,B.BookNo    
    --,BU.BU_ID    
    --,SU.SU_ID    
    --,SC.ServiceCenterId AS SC_ID    
    --,B.CycleId    
    --,BU.StateCode    
    ,CD.BookNo    
    ,CD.BU_ID    
    ,CD.SU_ID    
    ,CD.ServiceCenterId AS SC_ID    
    ,CD.CycleId    
    --,CD.StateCode    
    ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode    
   FROM UDV_CustomerDescription CD    
 --  JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber    
 --  JOIN Tbl_BookNumbers B ON B.BookNo=CPD.BookNo    
 --  JOIN Tbl_Cycles C ON C.CycleId=B.CycleId    
 --JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId    
 --JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID    
 --JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID    
   WHERE CD.GlobalAccountNumber=@GlobalAccountNo    
   or OldAccountNo=@GlobalAccountNo -- Faiz-ID103
   FOR XML PATH('CustomerRegistrationList_1BE'),TYPE        
 ),        
 (         
    --3.Fetching Customer Address Details        
   SELECT        
    CPAD.GlobalAccountNumber  AS GolbalAccountNumber      
    , HouseNo   AS HouseNoPostal      
    , StreetName     AS StreetPostal      
    , City   AS CityPostaL      
    , AreaCode   AS AreaPostal      
    , ZipCode   AS ZipCodePostal      
    , AddressID   AS PostalAddressID      
    ,IsServiceAddress  AS IsServiceAddress      
    ,IsCommunication      
   FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] as CPAD
   Join [CUSTOMERS].[Tbl_CustomerSDetail] as CSD on CSD.GlobalAccountNumber = CPAD.GlobalAccountNumber
   WHERE (CPAD.GlobalAccountNumber=@GlobalAccountNo or CSD.OldAccountNo=@GlobalAccountNo)AND IsActive=1   -- Faiz-ID103    
   ORDER BY IsServiceAddress ASC        
   FOR XML PATH('CustomerRegistrationList_2BE'),TYPE        
  )        
    FOR XML PATH(''),ROOT('CustomerRegistrationInfoByXml')          
END 
GO