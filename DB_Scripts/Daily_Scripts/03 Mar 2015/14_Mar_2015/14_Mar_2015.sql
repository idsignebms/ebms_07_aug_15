/****** Object:  StoredProcedure [dbo].[USP_GetAllAccNoInfoXLMeterReadings]    Script Date: 03/15/2015 05:33:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	

;With FinalResult AS  
(  
SELECT     
	CD.GlobalAccountNumber as AccNum
	,CD.OldAccountNo AS OldAccountNo
	,T1.MinReading
	,CD.ActiveStatusId AS ActiveStatusId
	--,(SELECT ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=T1.AccountNo OR OldAccountNo=T1.AccountNo) AS ActiveStatusId
 FROM @TempCustomer T1
INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON  T1.AccountNo=CD.GlobalAccountNumber OR  T1.AccountNo=CD.OldAccountNo
 LEFT JOIN( SELECT  C.AccountNo AS AccNum
	,ActiveStatusId    
    FROM [CUSTOMERS].[Tbl_CustomerSDetail] C     
     ) B ON T1.AccountNo=B.AccNum    
)  
  
 SELECT  
  (       
  SELECT   
        AccNum
        ,OldAccountNo
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsDuplicatePayment]    Script Date: 03/15/2015 14:38:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 30-10-2014
-- Description : to check the duplicate payment or not ?
-- =============================================
ALTER FUNCTION [dbo].[fn_IsDuplicatePayment]
(
@AccountNo VARCHAR(20)
,@PaidDate DATETIME
,@PaidAmount DECIMAL(18,2)
,@ReceiptNumber VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsPaymentExists BIT = 0
	DECLARE @GlobalAccountNo VARCHAR(50)

	IF EXISTS(SELECT 0 FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,@PaidDate) 
					AND PaidAmount = @PaidAmount AND ReceiptNo=@ReceiptNumber) 
		SET @IsPaymentExists  = 1
		
	RETURN @IsPaymentExists 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 03/15/2015 14:36:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <25-MAR-2014>        
-- Description:     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]        
 (
@XmlDoc XML
,@MultiXmlDoc XML
 )        
AS        
BEGIN        
        
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)   
								,ReceivedDate DATETIME,PaymentModeId INT)  
 DECLARE @Bu_Id VARCHAR(50)
 
 SELECT 
	@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)	   
       
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)    
 SELECT           
	 c.value('(SNO)[1]','INT')                       
	,c.value('(AccountNo)[1]','VARCHAR(50)')
	,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                       
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                       
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)  
    ,c.value('(PaymentMode)[1]','INT')  
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)      
        
  SELECT  
  (  
   SELECT   
	  ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber  
	  ,CD.GlobalAccountNumber  AS AccountNo
	 , CD.OldAccountNo AS OldAccountNo
     ,TempDetails.AmountPaid AS PaidAmount  
     ,TempDetails.ReceiptNO  
     ,TempDetails.ReceivedDate 
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate
	 ,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid, TempDetails.ReceiptNO) AS IsDuplicate
	 ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId) 
			 ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode  
	 ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId  
	 ,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers   
		  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess  
	-- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills   
   FROM @TempCustomer AS TempDetails   
    INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId  
 FOR XML PATH('PaymentsList'),TYPE    
 )  
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')      
END 

GO