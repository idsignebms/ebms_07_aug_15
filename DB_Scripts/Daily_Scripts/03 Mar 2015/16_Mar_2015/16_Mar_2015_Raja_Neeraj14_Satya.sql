GO
--SELECT * FROM Tbl_Menus WHERE ReferenceMenuId=1

GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Report Summary','../Reports/ReportsSummary.aspx',8,23) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
ALTER TABLE Tbl_CustomerBills
ADD ActualUsage DECIMAL(18,2)
GO
ALTER TABLE Tbl_CustomerBills
ADD LastPaymentTotal DECIMAL(18,2)
GO
ALTER TABLE Tbl_CustomerBills
ADD PreviousBalance DECIMAL(18,2)
GO
ALTER TABLE Tbl_CustomerBills
ADD PaidAmount DECIMAL(18,2)
GO
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Service_HouseNo VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Service_StreetName VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Service_City VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Service_Landmark VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Service_AreaCode VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Service_ZipCode VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Postal_HouseNo VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Postal_StreetName VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Postal_City VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Postal_Landmark VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Postal_AreaCode VARCHAR(100)
GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ADD Postal_ZipCode VARCHAR(100)
GO
/****** Object:  Table [dbo].[Tbl_BillDetails]    Script Date: 03/16/2015 00:14:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Tbl_BillDetails](
	[CustomerBillDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillID] [int] NULL,
	[CustomerFullName] [varchar](200) NULL,
	[BusinessUnitName] [varchar](200) NULL,
	[TariffName] [varchar](200) NULL,
	[ReadDate] [datetime] NULL,
	[Multiplier] [int] NULL,
	[Service_HouseNo] [varchar](200) NULL,
	[Service_Street] [varchar](200) NULL,
	[Service_City] [varchar](100) NULL,
	[ServiceZipCode] [varchar](50) NULL,
	[Postal_HouseNo] [varchar](50) NULL,
	[Postal_Street] [varchar](50) NULL,
	[Postal_City] [varchar](50) NULL,
	[Postal_ZipCode] [varchar](50) NULL,
	[Service_Landmark] [varchar](100) NULL,
	[Postal_LandMark] [varchar](100) NULL,
	[OldAccountNumber] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO
/****** Object:  View [dbo].[UDV_CustDetailsForBillGen]    Script Date: 03/16/2015 00:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[UDV_CustDetailsForBillGen] AS

SELECT		
 
			CD.GlobalAccountNumber
			--, CD.DocumentNo
			--, CD.AccountNo
			, CD.OldAccountNo
			, CD.Title
			, CD.FirstName
			, CD.MiddleName
			, CD.LastName
			--, CD.KnownAs
			--, CD.EmployeeCode
			--, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo
			, ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo
			--, TD.PhoneNumber
			--, TD.AlternatePhoneNumber
			, CD.ActiveStatusId
			, PD.PoleID
			, PD.TariffClassID AS TariffId
			--, TC.ClassName
			--, PD.IsBookNoChanged
			 , PD.ReadCodeID
			, PD.SortOrder
			--, CAD.Highestconsumption
			, CAD.OutStandingAmount
			, BN.BookNo
			--, BN.BookCode
			, BU.BU_ID
			, BU.BusinessUnitName
			--, BU.BUCode
			, SU.SU_ID
			--, SU.ServiceUnitName
			--, SU.SUCode
			, SC.ServiceCenterId
			--, SC.ServiceCenterName
			--, SC.SCCode
			, C.CycleId
			--, C.CycleName
			--, C.CycleCode
			--, MS.[Status] AS ActiveStatus
			, CD.EmailId
			, PD.MeterNumber
			--, MI.MeterType AS MeterTypeId
			--, PD.PhaseId
			--, PD.ClusterCategoryId
			--, CAD.InitialReading
			, CAD.InitialBillingKWh 
			--, CAD.AvgReading
			--, PD.RouteSequenceNumber AS RouteSequenceNo
			--, CD.ConnectionDate
			--, CD.CreatedDate
			--, CD.CreatedBy
			, PD.CustomerTypeId
			,PD.ClusterCategoryId
			,PD.IsEmbassyCustomer
			,CAD.Highestconsumption
			,CD.ServiceAddressID
			,CD.PostalAddressID
			 
			,CAD.OpeningBalance
			,CD.Service_HouseNo   
			,CD.Service_StreetName    
			,CD.Service_City   
			,CD.Service_ZipCode           
			,TC.ClassName  
			,TC.ClassID
			,CD.Postal_ZipCode
			,CD.Postal_HouseNo   
			,CD.Postal_StreetName    
			,CD.Postal_City
			,CD.Postal_Landmark
			,Cd.Service_Landmark   
			  
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId 
                      INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
                      INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
                      INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
                      INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
                      INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
                      INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
                      LEFT OUTER JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId 
                      LEFT OUTER JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo


GO



 GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 03/16/2015 00:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  
CREATE FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @CycleIdList VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(50),CB.PaymentLastDate) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(50),CB.PaymentLastDate) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),BD.ReadDate),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(50), CB.PaymentLastDate),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'--') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
    FROM Tbl_CustomerBills CB    
    INNER JOIN Tbl_BillDetails BD ON CB.CustomerBillId = BD.CustomerBillID 
    where CycleId in (
    select com from fn_Split(@CycleIdList,'|')) and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO    
-----------------------------------------------------------------------------
 
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAddressFormat]    Script Date: 03/16/2015 00:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya
-- Create date: 10-Mar-2015
-- Description:	The purpose of this function is to get the Customer Service Address

-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerAddressFormat]
(

@HouseNo VARCHAR(50)
,@StreetName VARCHAR(50)
--,@Landmark VARCHAR(50)
,@City VARCHAR(50)
--,@Details VARCHAR(50)
,@zipcode VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)


			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN @HouseNo IS NULL OR @HouseNo = '' THEN '' ELSE LTRIM(RTRIM(@HouseNo)) + ', '  END + 
				CASE WHEN @StreetName IS NULL OR @StreetName = '' THEN '' ELSE  LTRIM(RTRIM(@StreetName)) + ', ' END + 
				--CASE WHEN @Landmark IS NULL OR @Landmark = '' THEN '' ELSE LTRIM(RTRIM(@Landmark)) + ', '  END + 
				CASE WHEN @City IS NULL OR @City = '' THEN '' ELSE  LTRIM(RTRIM(@City)) + ', ' END  + 
				--CASE WHEN @Details IS NULL OR @Details = '' THEN '' ELSE  LTRIM(RTRIM(@Details))+ ', ' END   + 
				CASE WHEN @zipcode IS NULL OR @zipcode = '' THEN '' ELSE  LTRIM(RTRIM(@zipcode))+ ', ' END )) 
			 
	RETURN @ServiceAddress
END
GO
 
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Get_Customer_LastBill_TotalPayment]    Script Date: 03/16/2015 00:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_Get_Customer_LastBill_TotalPayment]
(
	 @GlobalAccountNumber Varchar(50)
	,@PrevBillID INT
	,@PrevBillDate Datetime
)
RETURNS   decimal(18,2)
AS
BEGIN
	
	Declare @PrevBillTotalPaidAmount Decimal(18,2) 
	If @PrevBillID IS NULL or @PrevBillID=0
		BEGIN
			--Print 'No Bills For Customer'
			
			   select @PrevBillTotalPaidAmount=SUM(PaidAmount) from Tbl_CustomerPayments
			   where RecievedDate >= (select MAX(LastBillGeneratedDate) 
									  from Tbl_LastBillGeneratedDate
									  ) and AccountNo=@GlobalAccountNumber
			    
		END
	ELSE
		Begin
			--Print 'Bills Avaiable'
			select @PrevBillTotalPaidAmount=SUM(PaidAmount) from Tbl_CustomerPayments 
			where RecievedDate >= @PrevBillDate and AccountNo=@GlobalAccountNumber
		END
		 
	if @PrevBillTotalPaidAmount IS NULL 
		SET @PrevBillTotalPaidAmount=0
	RETURN @PrevBillTotalPaidAmount

END
GO
 
/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges]    Script Date: 03/16/2015 00:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[fn_GetFixedCharges]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	--DECLARE @FixedCharges DECIMAL
	---- Add the SELECT statement with parameter references here
	--SELECT @FixedCharges=SUM(Amount) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	--WHERE  IsActive = 1   
	--AND  classID = @ClassID
	--AND @MonthStartDate BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)  
	
	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END
GO
 
/****** Object:  UserDefinedFunction [dbo].[fn_GetEnergyChanges]    Script Date: 03/16/2015 00:09:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 05-06-2014
-- Description:	The purpose of this function is to Calculate The Bill Amount Based on Consumption
-- Modified By : Padmini
-- Modified Date : 26-12-2014  

-- =============================================
ALTER FUNCTION [dbo].[fn_GetEnergyChanges]
(
	@AccountNumner VARCHAR(50)
	,@Consumption DECIMAL(18,2)
	,@Month INT 
	,@Year INT
	,@TariffClassID INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @Date VARCHAR(50) 
			,@RemaingUsage DECIMAL(20)
			,@TotalAmount DECIMAL(18,2)= 0
			,@PresentCharge INT
			,@FromUnit INT
			,@ToUnit INT
			,@Amount DECIMAL(18,2)
			
	SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'
	DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)

	DELETE FROM @ChargeUnits 
	
	INSERT INTO @ChargeUnits 
	SELECT 
		ClassID,FromKW,ToKW,Amount,TaxId FROM Tbl_LEnergyClassCharges 
	WHERE ClassID =@TariffClassID
	AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	ORDER BY FromKW ASC		
	
	SET @RemaingUsage = @Consumption
	SET @TotalAmount = 0
			
	SELECT TOP(1) @PresentCharge = Id FROM @ChargeUnits ORDER BY Id ASC

	WHILE(EXISTS(SELECT TOP(1) Id FROM @ChargeUnits WHERE Id >= @PresentCharge ORDER BY Id ASC))
		BEGIN
		
		SELECT @FromUnit = FromKW,@ToUnit = ToKW,@Amount = Amount
		FROM @ChargeUnits
		WHERE Id = @PresentCharge
		
		IF(@ToUnit IS NOT NULL)
			BEGIN
				IF(@RemaingUsage > (@ToUnit - @FromUnit)+1)
					BEGIN					
						SET @TotalAmount = @TotalAmount + ((@ToUnit - @FromUnit)+1)* @Amount	
						SET @RemaingUsage = @RemaingUsage - ((@ToUnit - @FromUnit)+1)
					END	
				ELSE
					BEGIN
						SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount
						SET @RemaingUsage = 0
					END	
			END
		ELSE
			BEGIN
				SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount	
				 SET @RemaingUsage = @RemaingUsage - @RemaingUsage
			END			
		
	IF(@PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits ORDER BY Id DESC))
		BREAK
	ELSE
		BEGIN
			SET @PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits WHERE  Id > @PresentCharge ORDER BY Id ASC)
			IF(@PresentCharge IS NULL) break;
				Continue
		END
	END

	RETURN @TotalAmount
END
GO
 
/****** Object:  UserDefinedFunction [dbo].[fn_GetDirectCustomerConsumption]    Script Date: 03/16/2015 00:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[fn_GetDirectCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50)	
	,@CycleId VARCHAR(100)
	,@ClusterCategoryId INT	
	,@Year INT
	,@Month INT
	,@InititalkWh INT
	,@TariffId INT
)
RETURNS DECIMAL
AS
BEGIN
	-- Declare the return variable here
	DECLARE @BillingRule INT, @Usage DECIMAL(20),@EstimationSettingId INT

	-- Add the T-SQL statements to compute the return value here
	
	SELECT @BillingRule=BillingRule,@EstimationSettingId=EstimationSettingId FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
		AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
		
	IF @BillingRule=1 -- As Per Settings Rule from table "tbl_EstimationSettings" and "tbl_MBillingRules"
	BEGIN
		SELECT @Usage=EnergytoCalculate FROM tbl_EstimationSettings(NOLOCK) WHERE @EstimationSettingId=EstimationSettingId
	END
	ELSE
	BEGIN
		SELECT @Usage=AverageReading FROM tbl_DirectCustomersAvgReadings(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber
		IF @Usage IS NULL
		BEGIN
			SELECT @Usage=@InititalkWh 
		END		
	END
	-- Return the result of the function
	RETURN @Usage

END
GO
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPaidMeterCustomer_Balance]    Script Date: 03/16/2015 00:10:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: <Suresh Kumar Dasi>
-- Create date	: <10-NOV-2014>
-- Description	: This function is used to get the paid meter customer balance 
-- =============================================
ALTER FUNCTION [dbo].[fn_GetPaidMeterCustomer_Balance]
(
@AccountNo VARCHAR(50)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
		DECLARE @Result DECIMAL(18,2)
				,@TotalMeterPrice DECIMAL(18,2)
				,@TotalDiscountedAmount DECIMAL(18,2)
		
		IF((SELECT dbo.fn_IsPaidMeterCustomer(@AccountNo)) = 1)
			BEGIN
				SELECT @TotalMeterPrice = ISNULL(MeterCost,0) FROM Tbl_PaidMeterDetails WHERE AccountNo = @AccountNo 
				SELECT @TotalDiscountedAmount = SUM(ISNULL(Amount,0)) FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo = @AccountNo 
				SET @Result = (ISNULL(@TotalMeterPrice,0) - ISNULL(@TotalDiscountedAmount,0))
			END
		ELSE
			BEGIN
				SET @Result = 0
			END	
			
	RETURN @Result

END
GO
 
/****** Object:  UserDefinedFunction [dbo].[fn_GetNormalCustomerConsumption]    Script Date: 03/16/2015 00:08:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,>
-- Description:	<Description,, Generate Normal customer consumption>
-- =============================================

--SELECT *	FROM fn_GetNormalCustomerConsumption('0001329260')
 
ALTER FUNCTION [dbo].[fn_GetNormalCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50),
	@OpeningBalance decimal(18,2)	
)
RETURNS @CustomerConsumption TABLE 
(
	-- Add the column definitions for the TABLE variable here
	 GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage DECIMAL(18,2)
	,LastBillGenerated DATETIME
	,BalanceUsage BIT
	,IsFromReading INT
	,CustomerBillId INT
	,PreviousBalance Decimal(18,2)
	,Multiplier INt
	,ReadDate datetime
)
AS
BEGIN
	DECLARE @ResultConsumption VARCHAR(1000),@LastBillGenerated DATETIME,@MeterMultiplier INT, @BalanceUsage INT,@CustomerBillId Int,@PreviousBalance decimal(18,2),@ReadDate Datetime
	
	-- Add the T-SQL statements to compute the return value here
	-- If reading available
	IF EXISTS(SELECT GlobalAccountNumber FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0)
	BEGIN
		
		 
		SELECT @MeterMultiplier=(SELECT top (1)  MeterMultiplier FROM Tbl_MeterInformation(NOLOCK)    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber))    
		
		SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@BalanceUsage=BalanceUsage ,@CustomerBillId=CustomerBillId,@PreviousBalance=PreviousBalance
		FROM Tbl_CustomerBills (NOLOCK)    
		WHERE AccountNo = @GlobalAccountNumber     
		ORDER BY CustomerBillId DESC   	
			
			IF @PreviousBalance IS NULL
				BEGIN
					select @PreviousBalance=@OpeningBalance
				END
			if @BalanceUsage IS NULL
				SET @BalanceUsage=0
			if @CustomerBillId IS NULL
				SET @CustomerBillId=0
		-- Fill the table variable with the rows for your result set
		INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate)
		SELECT GlobalAccountNumber
			,MIN(PreviousReading) AS PreviousReading
			,MAX(PresentReading) AS PresentReading
			,SUM(Usage)*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,MAX(ReadDate)
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		GROUP BY GlobalAccountNumber		
	
	END
	ELSE -- If reading not available
	BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 
		SELECT @IsFirstTimeCustomer=dbo.fn_IsFirstTimeCustomerForBilling(@GlobalAccountNumber)
		
		IF @IsFirstTimeCustomer=0
		BEGIN
		
		 
 		
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT TOP(1) AccountNo AS GlobalAccountNumber
				,'--' AS PreviousReading
				,'--' AS PresentReading
				,AverageReading AS Usage
				,BillGeneratedDate AS LastBillGenerated
				,isnull(BalanceUsage,0)
				,0 AS IsFromReading
				,CustomerBillId
				,PreviousBalance
				 
			FROM Tbl_CustomerBills (NOLOCK) 
			WHERE AccountNo = @GlobalAccountNumber 
			ORDER BY CustomerBillId DESC
		END
		ELSE
		BEGIN
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,
			PresentReading,Usage,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT GlobalAccountNumber
				,'--' AS PreviousReading
				,'--' AS PresentReading
				,InitialBillingKWh AS Usage
				,0 AS  BalanceUsage 
				,0 AS IsFromReading
				,0
				,@OpeningBalance
				
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
		END		
	
	END	
	
	RETURN 
END
GO
 
/****** Object:  UserDefinedFunction [dbo].[fn_IsFirstTimeCustomerForBilling]    Script Date: 03/16/2015 00:12:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================
-- Author:		<Rajaiah>
-- Create date: <30-JUL-2014>
-- Description	: This function is used to Check the Is First Time Customer For Billing.
-- =============================================
ALTER FUNCTION [dbo].[fn_IsFirstTimeCustomerForBilling]
(
	@GlobalAccountNumber VARCHAR(50)	
)
RETURNS BIT
AS
BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 		
				
		IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNumber)
		BEGIN
			SET @IsFirstTimeCustomer = 0
		END 		
			
	RETURN @IsFirstTimeCustomer

END
GO
 
/****** Object:  StoredProcedure [dbo].[USP_BillGenaraton]    Script Date: 03/16/2015 00:22:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya.K>
-- Create date: <10-Mar-2015>
-- Description:	<The purpose of this procedure is to generate the bills>
-- =============================================

--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(50)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	
	
	set @CycleId = 'BEDC_C_0848'
	set @BillingQueueScheduleIdList='580'
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (50)-- New Table   
			,ServiceStreet varchar (50) -- New Table         
			,ServiceCity varchar (50)       -- New Table   
			,ServiceZipCode varchar (50)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (50)	
			,Postal_HouseNo   varchar (50)	 
			,Postal_StreetName   varchar (50)	  
			,Postal_City   varchar (50)
			,Postal_LandMark VARCHAR(100)
			,Service_LandMark VARCHAR(100)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year] 
		
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		     
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			 
			 
			SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
			,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
			@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
			,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
			,@OpeningBalance=isnull(OpeningBalance,0)
			,@CustomerFullName=CustomerFullName
			,@BusinessUnitName =BusinessUnitName
			,@TariffName =TariffName
			--,@ReadDate =
			--,@Multiplier  
			,@Service_HouseNo =ServiceHouseNo
			,@Service_Street =ServiceStreet
			,@Service_City =ServiceCity
			,@ServiceZipCode  =ServiceZipCode
			,@Postal_HouseNo =ServiceHouseNo
			,@Postal_Street =Postal_StreetName
			,@Postal_City  =Postal_City
			,@Postal_ZipCode  =Postal_ZipCode
			,@Postal_LandMark =Postal_LandMark
			,@Service_LandMark =Service_LandMark
			,@OldAccountNumber=OldAccountNo
			FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
			
			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
			BEGIN
				
				SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
					,@RegenCustomerBillId=CustomerBillId	
				FROM Tbl_CustomerBills(NOLOCK)  
				WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
				DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
				
				DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
				SELECT @PaidAmount=SUM(PaidAmount)
				FROM Tbl_CustomerBillPayments(NOLOCK)  
				WHERE BillNo=@RegenCustomerBillId
				
				
			
				IF @PaidAmount IS NOT NULL
				BEGIN
									
					SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
					UPDATE Tbl_CustomerBillPayments
					SET BillNo=NULL
					WHERE BillNo=@RegenCustomerBillId
				END
				
				----------------------Update Readings as is billed =0 ----------------------------
				UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				
				
				-- Insert Paid Meter Payments
				DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				 
				update CUSTOMERS.Tbl_CustomerActiveDetails
				SET OutStandingAmount=@OutStandingAmount
				where GlobalAccountNumber=@GlobalAccountNumber
				----------------------------------------------------------------------------------
				
				
	 
				--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
				DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
				-------------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------------
				--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
				--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
				--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
				
				---------------------------------------------------------------------------------------
				--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
				---------------------------------------------------------------------------------------
			END

			IF @ReadCodeId=2 -- 
			BEGIN
	 			SELECT  @PreviousReading=PreviousReading,
						@CurrentReading = PresentReading,@Usage=Usage,
						@LastBillGenerated=LastBillGenerated,
						@PreviousBalance =Previousbalance,
						@BalanceUnits=BalanceUsage,
						@IsFromReading=IsFromReading,
						@PrevCustomerBillId=CustomerBillId,
						@PreviousBalance=PreviousBalance,
						@Multiplier=Multiplier,
						@ReadDate=ReadDate
				FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
				
				
				
				IF @IsFromReading=1
				BEGIN
						SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
						IF @Usage<@BalanceUnits
						BEGIN
							SET @ActualUsage=@Usage
							SET @Usage=0
							SET @RemaningBalanceUsage=@BalanceUnits-@Usage
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
						END
						ELSE
						BEGIN
							SET @ActualUsage=@Usage
							SET @Usage=@Usage-@BalanceUnits
							SET @RemaningBalanceUsage=0
						END
				END
				ELSE
				BEGIN
						SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
						SET @ActualUsage=@Usage
						SET @RemaningBalanceUsage=@Usage+@BalanceUnits
				END
			END
			ELSE --@CustomerTypeID=1 -- Direct customer
			BEGIN
				set @IsEstimatedRead =1
				 
				-- Get balance usage of the customer
				SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
							  @PreviousBalance=PreviousBalance
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber       
				ORDER BY CustomerBillId DESC 
				
				IF @PreviousBalance IS NULL
				BEGIN
					SET @PreviousBalance=@OpeningBalance
				END

			
		 
				      
				SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
				SET @ReadType=1 -- Direct
				SET @ActualUsage=@Usage
			END
			
					
 			
			
			
			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0
			 
			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
			DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
			
			-- =============================================
			-- If customer is disable 
			SELECT @BookDisableType=DisableTypeId FROM Tbl_BillingDisabledBooks WHERE BookNo=@BookNo AND YearId=@Year AND MonthId=@Month AND IsActive=1 AND ApproveStatusId=2
			 
			
			IF @BookDisableType IS NULL
				SET @BookDisableType=-1
			IF @BookDisableType<>-1 
			BEGIN 
				SET @FixedCharges=0
			END
			ELSE
			BEGIN
				INSERT INTO @tblFixedCharges(ClassID ,Amount)
				SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
				SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges					
			END 
			
			
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
			BEGIN
				
				IF @GetPaidMeterBalance<@FixedCharges
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=0
					SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
					SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
				END
				ELSE
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
					SET @FixedCharges=0
				END
			END
			------------------------
			-- Caluclate tax here
			
			SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			
			 
			
			SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0
			
			set @NetArrears=@OutStandingAmount-@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			
			
			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber
				Group BY CustomerBillId       
				ORDER BY CustomerBillId DESC 
			 
			if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage
			
			if @RemainingBalanceUnits IS NULL
			 set @RemainingBalanceUnits=0
			
			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
			
			 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			-----------------------------------------------------------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
			)        
			SELECT GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
				,BU_ID
				,SU_ID
				,ServiceCenterId
				,PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,ISNULL(@AverageUsageForNewBill,0)
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
			FROM @CustomerMaster C        
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL
			
			-----------------------------------------------------------------------------------------
			
			--While Loop continue checking
			IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
				BREAK        
			ELSE        
			BEGIN     
			   --BREAK
			   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
			   IF(@GlobalAccountNumber IS NULL) break;        
				Continue        
			END
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		SELECT * From dbo.fn_GetCustomerBillsForPrint(@CycleId,@Month,@Year)  
	END
 
END
GO

GO
 
/****** Object:  StoredProcedure [dbo].[USP_BillGenaraton]    Script Date: 03/16/2015 00:22:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya.K>
-- Create date: <10-Mar-2015>
-- Description:	<The purpose of this procedure is to generate the bills>
-- =============================================

--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New]
( @XmlDoc Xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	SELECT
			@FeederId = C.value('(FeederId)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BillingQueueScheduleIdList = C.value('(BillingQueueScheduleIdList)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('BillGenerationBe') as T(C)	
	
	
	
	--set @CycleId = 'BEDC_C_0848'
	--set @BillingQueueScheduleIdList='580'
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (50)-- New Table   
			,ServiceStreet varchar (50) -- New Table         
			,ServiceCity varchar (50)       -- New Table   
			,ServiceZipCode varchar (50)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (50)	
			,Postal_HouseNo   varchar (50)	 
			,Postal_StreetName   varchar (50)	  
			,Postal_City   varchar (50)
			,Postal_LandMark VARCHAR(100)
			,Service_LandMark VARCHAR(100)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year] 
		
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		     
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			 
			 
			SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
			,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
			@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
			,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
			,@OpeningBalance=isnull(OpeningBalance,0)
			,@CustomerFullName=CustomerFullName
			,@BusinessUnitName =BusinessUnitName
			,@TariffName =TariffName
			--,@ReadDate =
			--,@Multiplier  
			,@Service_HouseNo =ServiceHouseNo
			,@Service_Street =ServiceStreet
			,@Service_City =ServiceCity
			,@ServiceZipCode  =ServiceZipCode
			,@Postal_HouseNo =ServiceHouseNo
			,@Postal_Street =Postal_StreetName
			,@Postal_City  =Postal_City
			,@Postal_ZipCode  =Postal_ZipCode
			,@Postal_LandMark =Postal_LandMark
			,@Service_LandMark =Service_LandMark
			,@OldAccountNumber=OldAccountNo
			FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
			
			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
			BEGIN
				
				SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
					,@RegenCustomerBillId=CustomerBillId	
				FROM Tbl_CustomerBills(NOLOCK)  
				WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
				DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
				
				DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
				SELECT @PaidAmount=SUM(PaidAmount)
				FROM Tbl_CustomerBillPayments(NOLOCK)  
				WHERE BillNo=@RegenCustomerBillId
				
				
			
				IF @PaidAmount IS NOT NULL
				BEGIN
									
					SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
					UPDATE Tbl_CustomerBillPayments
					SET BillNo=NULL
					WHERE BillNo=@RegenCustomerBillId
				END
				
				----------------------Update Readings as is billed =0 ----------------------------
				UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				
				
				-- Insert Paid Meter Payments
				DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				 
				update CUSTOMERS.Tbl_CustomerActiveDetails
				SET OutStandingAmount=@OutStandingAmount
				where GlobalAccountNumber=@GlobalAccountNumber
				----------------------------------------------------------------------------------
				
				
	 
				--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
				DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
				-------------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------------
				--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
				--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
				--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
				
				---------------------------------------------------------------------------------------
				--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
				---------------------------------------------------------------------------------------
			END

			IF @ReadCodeId=2 -- 
			BEGIN
	 			SELECT  @PreviousReading=PreviousReading,
						@CurrentReading = PresentReading,@Usage=Usage,
						@LastBillGenerated=LastBillGenerated,
						@PreviousBalance =Previousbalance,
						@BalanceUnits=BalanceUsage,
						@IsFromReading=IsFromReading,
						@PrevCustomerBillId=CustomerBillId,
						@PreviousBalance=PreviousBalance,
						@Multiplier=Multiplier,
						@ReadDate=ReadDate
				FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
				
				
				
				IF @IsFromReading=1
				BEGIN
						SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
						IF @Usage<@BalanceUnits
						BEGIN
							SET @ActualUsage=@Usage
							SET @Usage=0
							SET @RemaningBalanceUsage=@BalanceUnits-@Usage
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
						END
						ELSE
						BEGIN
							SET @ActualUsage=@Usage
							SET @Usage=@Usage-@BalanceUnits
							SET @RemaningBalanceUsage=0
						END
				END
				ELSE
				BEGIN
						SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
						SET @ActualUsage=@Usage
						SET @RemaningBalanceUsage=@Usage+@BalanceUnits
				END
			END
			ELSE --@CustomerTypeID=1 -- Direct customer
			BEGIN
				set @IsEstimatedRead =1
				 
				-- Get balance usage of the customer
				SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
							  @PreviousBalance=PreviousBalance
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber       
				ORDER BY CustomerBillId DESC 
				
				IF @PreviousBalance IS NULL
				BEGIN
					SET @PreviousBalance=@OpeningBalance
				END

			
		 
				      
				SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
				SET @ReadType=1 -- Direct
				SET @ActualUsage=@Usage
			END
			
					
 			
			
			
			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0
			 
			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
			DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
			
			-- =============================================
			-- If customer is disable 
			SELECT @BookDisableType=DisableTypeId FROM Tbl_BillingDisabledBooks WHERE BookNo=@BookNo AND YearId=@Year AND MonthId=@Month AND IsActive=1 AND ApproveStatusId=2
			 
			
			IF @BookDisableType IS NULL
				SET @BookDisableType=-1
			IF @BookDisableType<>-1 
			BEGIN 
				SET @FixedCharges=0
			END
			ELSE
			BEGIN
				INSERT INTO @tblFixedCharges(ClassID ,Amount)
				SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
				SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges					
			END 
			
			
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
			BEGIN
				
				IF @GetPaidMeterBalance<@FixedCharges
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=0
					SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
					SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
				END
				ELSE
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
					SET @FixedCharges=0
				END
			END
			------------------------
			-- Caluclate tax here
			
			SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			
			 
			
			SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0
			
			set @NetArrears=@OutStandingAmount-@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			
			
			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber
				Group BY CustomerBillId       
				ORDER BY CustomerBillId DESC 
			 
			if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage
			
			if @RemainingBalanceUnits IS NULL
			 set @RemainingBalanceUnits=0
			
			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
			
			 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			-----------------------------------------------------------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
			)        
			SELECT GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
				,BU_ID
				,SU_ID
				,ServiceCenterId
				,PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,ISNULL(@AverageUsageForNewBill,0)
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
			FROM @CustomerMaster C        
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL
			
			-----------------------------------------------------------------------------------------
			
			--While Loop continue checking
			IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
				BREAK        
			ELSE        
			BEGIN     
			   --BREAK
			   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
			   IF(@GlobalAccountNumber IS NULL) break;        
				Continue        
			END
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		SELECT * From dbo.fn_GetCustomerBillsForPrint(@CycleId,@Month,@Year)  
	END
 
END
GO
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 03/16/2015 19:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @CycleIdList VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(50),CB.PaymentLastDate) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(50),CB.PaymentLastDate) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),BD.ReadDate),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(50), CB.PaymentLastDate),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'--') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
    FROM Tbl_CustomerBills CB    
    INNER JOIN Tbl_BillDetails BD ON CB.CustomerBillId = BD.CustomerBillID 
    where CycleId in (
    select com from fn_Split(@CycleIdList,'|')) and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@ReadCodeID VARCHAR(50) 
		,@GlobalAccountNumber VARCHAR(50) 
		,@IsSuccessful BIT =1
		,@StatusText VARCHAR(200)
		,@InitialReading INT

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	SET @StatusText='Deserialization'
	 SELECT  
	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
	SET @StatusText='Update Statement'
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
	SET MeterNumber=@MeterNumber
		,ReadCodeID=@ReadCodeID
	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	SET InitialReading=@InitialReading
		,PresentReading=@InitialReading
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('CustomerRegistrationBE'),TYPE

END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 ,@OldMeterNo VARCHAR(50)
				 ,@InitialBillingkWh INT
				 ,@OldMeterReadingDate DATETIME
				 ,@NewMeterReadingDate DATETIME
				 ,@NewMeterInitialReading VARCHAR(20)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
			,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
			,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
			,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
			
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading
														,MeterChangedDate)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET
		--		InitialReading=@InitialBillingkWh, 
		--		PresentReading=(CASE @NewMeterReading WHEN '' THEN @InitialBillingkWh ELSE @NewMeterInitialReading END)
		--WHERE GlobalAccountNumber=@AccountNo
			
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
		SET @ReadBy=(SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		
		
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@OldMeterReadingDate
									,@ReadBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@OldMeterNo)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		--If New MeterNo assighned to that customer
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading=@NewMeterInitialReading,
			PresentReading=CASE @NewMeterReading WHEN '' THEN @NewMeterInitialReading ELSE @NewMeterReading END
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE @NewMeterUsage VARCHAR(50)
		SET @NewMeterUsage=	(ISNULL(CONVERT(BIGINT,@NewMeterReading),0)) - (ISNULL(CONVERT(BIGINT,@NewMeterInitialReading),0))
		
		IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
			BEGIN		
				DECLARE @NewMeterDials INT		
				SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@NewMeterReadingDate
									,@ReadBy									
									,@NewMeterInitialReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@NewMeterUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@NewMeterUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@MeterNo)
			END
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
ALTER TABLE Tbl_BatchDetails ADD BU_ID VARCHAR(50)
-------------------------------------------------------------------------------
GO
-- =============================================  
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-03-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================            
ALTER PROCEDURE [USP_InsertBatchDetails](@XmlDoc XML)            
            
AS            
BEGIN            
  DECLARE  @BatchNo int,            
			 @BatchName varchar(50) ,            
			 @BatchDate datetime ,            
			 @CashOffice INT ,            
			 @CashierID VARCHAR(50) ,            
			 @CreatedBy varchar(50),            
			 @BatchTotal NUMERIC(20,4),            
			 @Description VARCHAR(MAX), 
			 @BU_ID VARCHAR(50),       
			 @Flag INT            
             
   SELECT             
     @BatchNo=C.value('(BatchNo)[1]','int')            
    ,@BatchName=C.value('(BatchName)[1]','varchar(50)')            
    ,@BatchDate=C.value('(BatchDate)[1]','DATETIME')            
    ,@CashOffice = C.value('(OfficeId)[1]','INT')            
    ,@CashierID = C.value('(CashierId)[1]','VARCHAR(50)')             
    ,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')            
    ,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')            
    ,@Description=C.value('(Description)[1]','VARCHAR(MAX)')            
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')            
    ,@Flag=C.value('(Flag)[1]','INT')            
                  
   FROM @XmlDoc.nodes('BillingBE') AS T(C)         
 --TEMP INSERTING FOR APPROVAL           
    IF(@Flag=1)        
   BEGIN        
  IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo))            
  BEGIN            
    INSERT INTO Tbl_BatchDetailsApproval(            
              BatchNo,            
              BatchName,            
              BatchDate,            
              CashOffice,            
              CashierID,            
              CreatedBy,            
              CreatedDate,            
              BatchTotal,            
              BatchStatus,            
              [Description]
              --BU_ID
             )            
             VALUES(            
              @BatchNo,            
              Case  @BatchName when '' then null else @BatchName end,  
              @BatchDate,  
              Case  @CashOffice when 0 then null else @CashOffice end,            
              Case  @CashierID when '' then null else @CashierID end,             
              Case  @CreatedBy when '' then null else @CreatedBy end,                
              dbo.fn_GetCurrentDateTime(),               
              @BatchTotal,            
              1,            
              CASE @Description WHEN '' THEN NULL ELSE @Description END
              --@BU_ID            
             )            
                   
    SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')            
  END            
  ELSE            
     SELECT 0 as IsSuccess  
     ,BatchStatus AS BatchStatusId  
     ,BatchDate,BatchName  
     ,BatchNo  
     ,BatchTotal  
     ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0))   
         FROM Tbl_CustomerPaymentsApproval            
         WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
     FROM Tbl_BatchDetailsApproval   
     WHERE BatchNo=@BatchNo   
     FOR XML PATH('BillingBE')            
           
   END      
 IF(@Flag=2)        
   BEGIN        
  IF(NOT EXISTS(SELECT BatchNo 
				FROM Tbl_BatchDetails 
				WHERE BatchNo=@BatchNo 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						AND BU_ID=@BU_ID))            
  BEGIN            
    INSERT INTO Tbl_BatchDetails(            
           BatchNo,            
           --BatchName,            
           BatchDate,            
           CashOffice,            
           CashierID,            
           CreatedBy,            
           CreatedDate,            
           BatchTotal,            
           BatchStatus,            
           --[Description] 
            BU_ID           
          )            
        VALUES(            
           @BatchNo,            
           --@BatchName when '' then null else @BatchName end,            
           @BatchDate,            
           Case  @CashOffice when 0 then null else @CashOffice end,            
           Case  @CashierID when '' then null else @CashierID end,             
           @CreatedBy,                
           dbo.fn_GetCurrentDateTime(),               
           @BatchTotal,            
           1,            
           @BU_ID
           --CASE @Description WHEN '' THEN NULL ELSE @Description END            
          )            
                   
    SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')            
  END            
  ELSE            
    SELECT 0 as IsSuccess  
    ,BatchStatus AS BatchStatusId  
    ,BatchDate  
    ,BatchName  
    ,BatchNo  
    ,BatchTotal  
    ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments            
      WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
    FROM Tbl_BatchDetails   
    WHERE BatchNo=@BatchNo 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						AND BU_ID=@BU_ID
    FOR XML PATH('BillingBE')  
   END      
END  
------------------------------------------------------------------------------------------
GO
-- =============================================    
-- Author:  T.Karthik    
-- Create date: 11-04-2014    
-- Modified Date:14-04-2014    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBatchDetailsByBatchNo]     
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @BatchNo INT    
 ,@Flag INT      
 ,@BU_ID VARCHAR(50)       
 ,@BatchDate DATE 
 SELECT    
  @BatchNo=C.value('(BatchNo)[1]','INT')    
  ,@Flag=C.value('(Flag)[1]','INT')        
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')        
  ,@BatchDate=C.value('(BatchDate)[1]','DATE') 
         
 FROM @XmlDoc.nodes('BillingBE') as T(C)    
 IF(@Flag=1)--GET DETAILS FROM TEMP TABLES    
  BEGIN    
   SELECT    
    CONVERT(VARCHAR(20),BatchDate,103) AS BatchDate    
    ,BatchName    
    ,BatchNo    
    ,CONVERT(DECIMAL(20,2),BatchTotal) AS BatchTotal    
    ,CashierID AS CashierId    
    ,CashOffice AS OfficeId    
    ,[Description]    
    ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPaymentsApproval WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount    
   FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo    
   FOR XML PATH('BillingBE')    
  END    
 ELSE IF(@Flag=2)--GET DETAILS FROM PERMANENT TABLE    
  BEGIN    
    SELECT     
    CONVERT(VARCHAR(20),BatchDate,103) AS BatchDate    
    ,BatchName    
    ,BatchNo    
    ,CONVERT(DECIMAL(20,2),BatchTotal) AS BatchTotal    
    ,CashierID AS CashierId    
    ,CashOffice AS OfficeId    
    ,[Description]    
    ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount    
   FROM Tbl_BatchDetails 
   WHERE BatchNo=@BatchNo   
		  AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120)
		AND BU_ID=@BU_ID 
   FOR XML PATH('BillingBE')    
  END    
END    
GO
-------------------------------------------------------------------------------------------------------------
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 27-09-2014  
-- Description: The purpose of this procedure is to get Payment Mode of Batch No  
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-03-2015    
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetPaymentmodeIdByBatchNo]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BatchNo INT    
			,@BU_ID VARCHAR(50)
			,@BatchDate DATE 
			,@IsSuccess BIT  
      
  SELECT     
   @BatchNo = C.value('(BatchNo)[1]','INT')  
   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
   ,@BatchDate = C.value('(CreatedDate)[1]','DATE')  
  FROM @XmlDoc.nodes('BatchDate') AS T(C)    
    
--IF EXISTS(SELECT 0 FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo)  
 IF(EXISTS(SELECT BatchNo 
				FROM Tbl_BatchDetails 
				WHERE BatchNo=@BatchNo 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						AND BU_ID=@BU_ID))             BEGIN   
  SELECT DISTINCT(PaymentMode) AS PaymentModeId   
  FROM Tbl_CustomerPayments   
  WHERE BatchNo=@BatchNo  
  FOR XML PATH('BatchEntryBe'),TYPE  
 END  
ELSE  
 BEGIN  
  SELECT 0 AS PaymentModeId FOR XML PATH('BatchEntryBe'),TYPE  
 END  
END    
----------------------------------------------------------------------------------------------------------------
GO
ALTER TABLE Tbl_Batch_CustBulkUpload ADD BU_ID VARCHAR(50)
----------------------------------------------------------------------------------------------------------------
GO
ALTER TABLE Tbl_BATCH_ADJUSTMENT ADD BU_ID VARCHAR(50)
------------------------------------------------------------------------------------------------------------------
GO
--=============================================================      
--AUTHOR : NEERAJ KANOJIYA      
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS      
--CREATED ON: 30-SEP-14    
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-MARCH-2015     
--=============================================================      
ALTER PROCEDURE [dbo].[USP_InsertAdjustmentBatch]      
(      
@XmlDoc XML      
)      
AS      
BEGIN      
 DECLARE @BatchNo INT,       
   @Reason VARCHAR(100),      
   @BatchTotal DECIMAL(18,2),      
   @BatchDate VARCHAR(50),      
   @CreatedBy VARCHAR(50),
   @BU_ID VARCHAR(50)      
       
 SELECT @BatchNo  = C.value('(BatchNo)[1]','INT'),      
     @Reason  = C.value('(ReasonCode)[1]','VARCHAR(100)'),      
     @BatchTotal = C.value('(BatchTotal)[1]','DECIMAL(18,2)'),      
     @BatchDate = C.value('(CreatedDate)[1]','VARCHAR(50)'),      
     @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)'),
     @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')        
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)      
       
       
 IF(NOT EXISTS(SELECT BatchNo 
				FROM Tbl_BATCH_ADJUSTMENT 
				WHERE BatchNo=@BatchNo 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						AND BU_ID=@BU_ID))  
				BEGIN
					 INSERT INTO Tbl_BATCH_ADJUSTMENT(      
							  BatchNo,       
							  Reason,      
							  BatchTotal,      
							  BatchDate,      
							  CreatedDate,       
							  CreatedBy,
							  BU_ID              
							 )      
						   VALUES      
							 (      
							  @BatchNo,      
							  @Reason,      
							  @BatchTotal,      
							  @BatchDate,      
							  dbo.fn_GetCurrentDateTime(),      
							  @CreatedBy,
							  @BU_ID
							 )      
					 SELECT @@ROWCOUNT AS [RowCount],      
					 (SELECT MAX(BatchID) FROM Tbl_BATCH_ADJUSTMENT) AS BatchID      
					 FOR XML PATH('BillAdjustmentsBe'),TYPE      
			END      
	ELSE
		BEGIN
		 SELECT 0 AS BatchID FOR XML PATH('BillAdjustmentsBe'),TYPE 
		END  
END  
GO
-------------------------------------------------------------------------------------------------------------------
ALTER TABLE Tbl_BATCH_ADJUSTMENT
DROP CONSTRAINT PK__Tbl_BATC__5D55CE382DB1C7EE
GO
-------------------------------------------------------------------------------------------------------------------
ALTER TABLE Tbl_BATCH_ADJUSTMENT
DROP CONSTRAINT UQ__Tbl_BATC__5D56EB96308E3499
GO