/****** Object:  StoredProcedure [dbo].[USP_RptGetPreBillingCustomers]    Script Date: 03/16/2015 20:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author  : Suresh Kumar D      
-- Create date : 01-10-2014      
-- Description : The purpose of this procedure is to Get the Pre bill     
-- Modified By : Padmini    
-- Modified Date : 26-12-2014    
-- Modified By : Faiz-ID103    ---------(Bhargav)
-- Modified Date : 10-Mar-2015   
-- =============================================      
ALTER PROCEDURE [dbo].[USP_RptGetPreBillingCustomers]    
(      
@XmlDoc XML      
)      
AS      
BEGIN      
     
 Declare @ReadCode INT  
  ,@BU_Id VARCHAR(20)    
   --,@Month INT    
   --,@Year INT    
   --,@Date DATETIME         
   --,@MonthStartDate VARCHAR(50)        
       
 SELECT @ReadCode = C.value('(ReadCodeId)[1]','INT')      
  ,@BU_Id = C.value('(BU_ID)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('RptPreBillingBe') as T(C)      
     
 IF(@ReadCode = 2) -- Read Customers  
  BEGIN  
   SELECT  
   (  
    
SELECT CD.GlobalAccountNumber  
     ,MIN(CR.PreviousReading) as PreviousReading  
     ,MAX(CR.PresentReading) as PresentReading  
     ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name  
     ,CD.ClassName AS ClassName
     ,CD.MeterNumber AS MeterNo
     ,OldAccountNo 
     ,MAX (CR.ReadDate) AS PreviousReadDate
     ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
   ,CR.ReadBy
   ,CR.CreatedBy
   ,CR.CreatedDate
    FROM Tbl_CustomerReadings CR  
    INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = Cr.GlobalAccountNumber   
    LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId  
    AND CD.BU_ID = @BU_Id-----
    AND CR.IsBilled=0  
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND 
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
    GROUP BY CD.GlobalAccountNumber  
     ,CD.FirstName  
     ,CD.Title  
     ,CD.MiddleName  
     ,CD.LastName  
     ,CD.MeterNumber  
     ,CD.ClassName  
     ,OldAccountNo 
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode
     ,Adress.AddressID 
      ,CR.ReadBy
   ,CR.CreatedBy
   ,CR.CreatedDate  
    FOR XML PATH('PreBillingList'),TYPE  
   )     
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')  
  END    
 ELSE -- Direct / Estimated Customers  
  BEGIN  
   With Results AS  
   (  
   SELECT   
    CD.GlobalAccountNumber  
    ,MIN(CR.PreviousReading) AS PreviousReading  
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name   
    ,CD.ClassName  
    ,OldAccountNo  
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress  
   FROM UDV_CustomerDescription CD  
   LEFT JOIN Tbl_CustomerReadings CR ON (CD.GlobalAccountNumber != CR.GlobalAccountNumber AND IsBilled=0)  
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId   
   AND CD.BU_ID = @BU_Id-----
    AND CR.IsBilled=0  
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND 
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
   GROUP BY CD.GlobalAccountNumber  
     ,CD.Title  
     ,CD.FirstName  
     ,CD.MiddleName  
     ,CD.LastName  
     ,CD.ClassName  
     ,OldAccountNo  
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode
     ,Adress.AddressID 
   UNION   
   SELECT   
    CD.GlobalAccountNumber  
    ,MIN(CR.PreviousReading) AS PreviousReading  
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name   
    ,CD.ClassName  
    ,OldAccountNo  
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress   
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress  
   FROM UDV_CustomerDescription CD  
   LEFT JOIN Tbl_CustomerReadings CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber  
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId  
   AND CD.BU_ID = @BU_Id-----
    AND CR.IsBilled=0  
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND 
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0
   WHERE CustomerTypeId = 1  
   GROUP BY CD.GlobalAccountNumber  
     ,CD.Title  
     ,CD.FirstName  
     ,CD.MiddleName  
     ,CD.LastName  
     ,CD.ClassName  
     ,OldAccountNo  
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode
     ,Adress.AddressID 
   )  
     
   SELECT  
   (  
   SELECT * FROM Results  
    FOR XML PATH('PreBillingList'),TYPE  
   )     
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')  
  END  
END    


---------------------------------------------------------------------------------------------------------  
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 03/16/2015 21:06:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date:	06-10-2014
-- Description:	The purpose of this procedure is to get Tariff change logs based on search criteria
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
		@CycleId VARCHAR(50)='',@TariffId INT=0,
		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
			,@TariffId=C.value('(TariffId)[1]','INT')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		SELECT
		(
			SELECT CT.AccountNo
					,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff
					,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff
					,Remarks
					,S.ApprovalStatus 
					,dbo.fn_GetCustomerFullName(cd.GlobalAccountNumber) AS Name
					,CONVERT(VARCHAR(30),CT.CreatedDate,107) AS TransactionDate
					,CT.CreatedBy
					,CT.CreatedDate
			FROM Tbl_LCustomerTariffChangeRequest CT
			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CT.ApprovalStatusId
			--JOIN UDV_CustomerDetails CD ON CT.AccountNo=CD.AccountNo
			JOIN [UDV_CustomerDescription] CD ON CT.AccountNo=CD.GlobalAccountNumber --Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]
			AND (BU_ID=@BU_ID OR @BU_ID='')
			AND (SU_ID=@SU_ID OR @SU_ID='')
			AND (ServiceCenterId=@SC_ID OR @SC_ID='')
			AND (CycleId=@CycleId OR @CycleId='')
			AND (TariffId=@TariffId OR @TariffId=0)
			AND (CONVERT(DATE,CT.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
			AND (CONVERT(DATE,CT.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
			ORDER BY CT.CreatedDate DESC
			FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
END
