GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading]    Script Date: 03/18/2015 14:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- ModiifiedBY: <Suresh Kumar Dasi>  
-- Description: to Solve the Date and DataType Convertions related Issues  
-- Modified By: T.Karthik
-- Modified Date: 27-12-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading]  
 (@MultiXmlDoc XML)  
AS  
BEGIN  
  
 DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),       
                             CurrentReading varchar(50),      
                             ReadDate DATETIME)     
                                
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)             
   
 INSERT INTO @TempCustomer(AccountNo ,CurrentReading,ReadDate)       
 SELECT       
	  c.value('(Account_Number)[1]','VARCHAR(50)')       
	  ,(CASE c.value('(Current_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Current_Reading)[1]','VARCHAR(50)') END)      
	  ,CONVERT(DATETIME,c.value('(Read_Date)[1]','varchar(50)'))       
--  , convert(VARCHAR(20),(  
-- substring(c.value('(Read_Date)[1]','VARCHAR(20)'),charindex('/',c.value('(Read_Date)[1]','VARCHAR(20)'))+1,2)+'/'+  
--left(c.value('(Read_Date)[1]','VARCHAR(20)'),charindex('/',c.value('(Read_Date)[1]','VARCHAR(20)'))-1)+'/'+  
--right(c.value('(Read_Date)[1]','VARCHAR(20)'),charindex('/',reverse(c.value('(Read_Date)[1]','VARCHAR(20)')))-1)))  
   
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)   

DELETE FROM @CustomersCount
INSERT INTO @CustomersCount
SELECT AccountNo,COUNT(0) As Total from @TempCustomer
GROUP BY AccountNo

;With FinalResult AS
(
SELECT   
  CD.GlobalAccountNumber as AccNum  
  ,B.Name  
  ,B.MeterNo  
  ,B.MeterDials  
  ,ReadCodeId  
  ,B.Decimals
  ,B.ActiveStatusId
  ,B.BU_ID  
  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(DATEPART(YY,T1.ReadDate),DATEPART(MM,T1.ReadDate), CD.GlobalAccountNumber)) AS IsActiveBillMonth
  ,(SELECT dbo.fn_IsBillMonthOpened(CONVERT(DATE, T1.ReadDate))) AS IsActiveMonth  
   ,(CASE WHEN (SELECT TOP(1) AverageReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate) < CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       IS NULL  
       THEN CONVERT(VARCHAR(50),ISNULL(AvgReading,0))   
       ELSE (SELECT TOP(1) AverageReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate) ORDER BY CustomerReadingId desc)  
       END) AS AverageReading  
  ,(CASE WHEN (SELECT TOP(1) TotalReadings FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
        CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       IS NULL  
       THEN '0'   
       ELSE (SELECT TOP(1) TotalReadings FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       END) AS TotalReadings  
  ,(CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       IS NULL  
       THEN CONVERT(VARCHAR(50),ISNULL(B.InitialReading,0))   
       ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       END)AS PreviousReading  
  ,T1.CurrentReading AS PresentReading  
  ,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate ) < CONVERT(DATE, T1.ReadDate)  ORDER BY CustomerReadingId DESC) AS BillNo
  ,CONVERT(VARCHAR(10),ISNULL((CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CONVERT(DATE,CR.ReadDate) = CONVERT(DATE,T1.ReadDate))  
      THEN 1 ELSE 0 END),0)) AS PrvExist
  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,T1.ReadDate))  
      THEN 1 ELSE 0 END) AS IsExists  
  ,CONVERT(VARCHAR(20),T1.ReadDate,103) AS BatchDate   
  ,CONVERT(VARCHAR(20),T1.ReadDate,106)AS ReadDate  
  --,B.CustomerUniqueNo
  --,CONVERT(VARCHAR(50),(CONVERT(DECIMAL(18,0),ISNULL(T1.CurrentReading,0))
		--				-(CASE WHEN (SELECT TOP(1) CONVERT(DECIMAL(18,0),PresentReading) FROM Tbl_CustomerReadings CR 
		--							 WHERE CR.AccountNumber = T1.AccountNo AND   
		--							 CONVERT(DATE,ReadDate ) < CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
		--				IS NULL  THEN 0   
		--				ELSE (SELECT TOP(1)  CONVERT(DECIMAL(18,0),PresentReading) FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = T1.AccountNo AND   
		--					   CONVERT(DATE,ReadDate ) < CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
		--				END)))AS Usage 
  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
			WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
			CONVERT(DATE,CR.ReadDate )=CONVERT(DATE, T1.ReadDate) ORDER BY CustomerReadingId DESC) AS IsBilled 
  ,(CASE WHEN EXISTS(SELECT AccountNo FROM @CustomersCount WHERE Total > 1 AND AccountNo = CD.GlobalAccountNumber) THEN 1 ELSE 0 END) AS IsDuplicate  
 FROM @TempCustomer T1 
INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON (CD.GlobalAccountNumber=T1.AccountNo OR CD.OldAccountNo=T1.AccountNo) 
 --LEFT JOIN( SELECT  C.CustomerUniqueNo AS CustomerUniqueNo  
	--			 ,C.AccountNo AS AccNum  
	--			 ,C.Name AS Name  
	--			 ,ReadCodeId  
	--			 ,C.MeterNo AS MeterNo  
	--			 ,M.MeterDials AS MeterDials  
	--			 ,M.Decimals AS Decimals  
	--			 ,C.InitialReading
	--			 ,C.AverageReading
	--			 ,(CASE WHEN T.AvgMaxLimit IS NULL THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
	--		 FROM Tbl_CustomerDetails C   
	--		LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNo  
	--		LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo   
	--	   ) B ON T1.AccountNo=B.AccNum  
	LEFT JOIN( SELECT 
				-- C.CustomerUniqueNo AS CustomerUniqueNo  
				 C.GlobalAccountNumber AS AccNum  
				 ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				 ,ReadCodeId  
				 ,C.MeterNumber AS MeterNo  
				 ,M.MeterDials AS MeterDials  
				 ,M.Decimals AS Decimals  
				 ,C.InitialReading
				 ,C.AvgReading
				 ,C.ActiveStatusId
				 ,C.BU_ID
				 ,(CASE WHEN T.AvgMaxLimit IS NULL THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			 FROM UDV_CustomerDescription C   
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
			LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo   
		   ) B ON CD.GlobalAccountNumber=B.AccNum  
)

	SELECT
		(     
		SELECT 
		      AccNum  
			  ,Name  
			  ,MeterNo  
			  ,MeterDials  
			  ,ReadCodeId  
			  ,Decimals  
			  ,IsActiveBillMonth
			  ,IsActiveMonth  
			  ,AverageReading  
			  ,TotalReadings  
			  ,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
			  ,PresentReading  
			  ,((CASE WHEN(PresentReading LIKE '%[^0-9]%') THEN 0 ELSE CONVERT(DECIMAL(18,0),PresentReading) END)- dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading)) AS Usage
			 -- ,((CASE WHEN ISNUMERIC(PresentReading) = 1 THEN CONVERT(DECIMAL(18,0),PresentReading) ELSE 0 END)- dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading)) AS Usage
			  ,PrvExist
			  ,IsExists  
			  ,BatchDate   
			  ,ReadDate  
			  --,CustomerUniqueNo
			  ,IsBilled 
			  ,IsDuplicate
			  ,ActiveStatusId
			  ,BU_ID
		 FROM FinalResult     
		FOR XML PATH('BillingBE'),TYPE  
		)  
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
END  
