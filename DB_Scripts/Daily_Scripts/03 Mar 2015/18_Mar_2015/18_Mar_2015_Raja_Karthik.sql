GO
GO
/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo_Rajaiah]    Script Date: 03/17/2015 11:05:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
-- EXEC USP_RptGetCustomersStatisticsInfo_Rajaiah
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo_Rajaiah]
(
@XmlDoc Xml=null
) 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(MAX)
	
	SELECT   @Month = C.value('(Month)[1]','INT')
			,@Year = C.value('(Year)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')			
		FROM @XmlDoc.nodes('NewReportsBe') AS T(C)
		
	--SET @Month=1
	--SET @Year=2015
	--SET @BU_ID='BEDC_BU_0024'
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CD.ClassName AS Tariff,
			--'' AS KWHSold,
			--'' AS KVASold,
			ISNULL(CBills.NetFixedCharges,0) AS FixedCharges,
			-- No Billed 
			ISNULL(CBills.TotalBillAmount,0) AS TotaAmountBilled,			
			ISNULL(CBills.PaidAmount,0) AS TotaAmountCollected,
			--'' AS NoOfStubs,
			-- Total population
			--'' AS WeightedAverage,
			CD.ActiveStatus AS CustomerStatus
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
	CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
	AND CD.GlobalAccountNumber=CBills.AccountNo

	SELECT Tariff,
		'' AS KWHSold,
		'' AS KVASold,
		SUM(FixedCharges) AS FixedCharges,
		COUNT(TotaAmountBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		'' AS WeightedAverage
		INTO #tmpCustomerStatisticsList
	FROM
	(
		SELECT	 Tariff,
			--KWHSold,
			--KVASold,
			COUNT(CustomerStatus) AS TotalCustomerTariff,		
			SUM(FixedCharges) AS FixedCharges,
			COUNT(TotaAmountBilled) AS NoBilled,
			SUM(TotaAmountBilled) AS TotaAmountBilled,
			SUM(TotaAmountCollected) AS TotaAmountCollected,
			--NoOfStubs,
			CustomerStatus
			--WeightedAverage		
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff
	ORDER BY Tariff
	
	DECLARE @tblCustomerStatisticsList AS TABLE
	(
		ID INT  IDENTITY(1,1),
		Tariff VARCHAR(200 ),
		KWHSold VARCHAR(500),
		KVASold VARCHAR(500),
		FixedCharges DECIMAL(18,2),
		NoBilled INT,
		TotaAmountBilled  DECIMAL(18,2),
		TotaAmountCollected  DECIMAL(18,2),
		NoOfStubs VARCHAR(500),
		TotalPopulation INT,
		WeightedAverage VARCHAR(500)
	)
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage)
	SELECT 	Tariff,
		KWHSold,
		KVASold,
		FixedCharges,
		NoBilled,
		TotaAmountBilled,
		TotaAmountCollected,
		NoOfStubs,
		TotalPopulation,
		WeightedAverage
	FROM #tmpCustomerStatisticsList
	
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage)
	SELECT 'Total' AS Tariff,
		'' AS KWHSold,
		'' AS KVASold,
		SUM(FixedCharges) AS FixedCharges,
		SUM(NoBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(TotalPopulation) AS TotalPopulation,
		'' AS WeightedAverage
	FROM #tmpCustomerStatisticsList
		
	SELECT	 ID AS SNo,Tariff
			,KWHSold
			,KVASold
			,FixedCharges
			,NoBilled
			,TotaAmountBilled AS TotalAmountBilled
			,TotaAmountCollected AS TotalAmountCollected
			,NoOfStubs
			,TotalPopulation
			,WeightedAverage 
	FROM @tblCustomerStatisticsList
	
	DROP TABLE #tmpCustomerStatistics
	DROP TABLE #tmpCustomerStatisticsList
	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Tariff change request logs
---- =============================================
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END		
	  
	  SELECT(
			  SELECT  TOP 10 TCR.AccountNo
					  ,TCR.ApprovalStatusId
					  ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff
					  ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff
					  ,TCR.Remarks
					  ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_LCustomerTariffChangeRequest  TCR
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId			  
			  GROUP BY TCR.AccountNo
					  ,TCR.ApprovalStatusId
					  ,PreviousTariffId
					  ,ChangeRequestedTariffId
					  ,TCR.Remarks
					  ,CONVERT(VARCHAR(30),TCR.CreatedDate,107)
					  ,ApproveSttus.ApprovalStatus
					  ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')	
 
END
--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsWithLimit]    Script Date: 03/17/2015 20:17:11 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Tariff change request logs
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit] 
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)

--		DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
--		INSERT INTO @Accounts
--			SELECT TCR.AccountNo FROM (
--				SELECT TOP 5 CT.AccountNo FROM Tbl_LCustomerTariffChangeRequest CT
--				JOIN [UDV_CustomerDescription] CD ON CT.AccountNo=CD.GlobalAccountNumber
--				AND (BU_ID=@BU_ID OR @BU_ID='')
--				AND (SU_ID=@SU_ID OR @SU_ID='')
--				AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--				AND (CycleId=@CycleId OR @CycleId='')
--				AND (TariffId=@TariffId OR @TariffId=0)
--				AND (CONVERT(DATE,CT.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--				AND (CONVERT(DATE,CT.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--				 ORDER BY CT.CreatedDate DESC
--				 ) TCR
--			 GROUP BY TCR.AccountNo
		 		 
--		CREATE TABLE #TariffLogs(TariffChangeRequestId INT)

--		DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
--		SELECT @Count=COUNT(0) FROM @Accounts
--		SELECT @TotalChanges=COUNT(0) FROM Tbl_LCustomerTariffChangeRequest

--		WHILE(@I<=@Count)
--		BEGIN
--			INSERT INTO #TariffLogs
--			SELECT TOP 10 TariffChangeRequestId FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
--			ORDER BY TariffChangeRequestId DESC
--			SET @I=@I+1
--		END 
		
--		SELECT
--		(
--			SELECT AccountNo
--					,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff
--					,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff
--					,Remarks
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(AccountNo) AS Name
--					,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
--					,@TotalChanges AS TotalChanges
--			FROM Tbl_LCustomerTariffChangeRequest TCR
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=TCR.ApprovalStatusId
--			AND TariffChangeRequestId IN(
--			SELECT TariffChangeRequestId FROM #TariffLogs
--			)
--			ORDER BY AccountNo DESC,CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- CREATE By : NEERAJ      
-- CREATED Date: 13-MAR-2015
-- DESC			: Get last close month
-- =============================================          
ALTER PROCEDURE [USP_GetLastCloseMonth]  
AS          
BEGIN 
	SELECT TOP 1 
		CONVERT(VARCHAR(5),[YEAR]) + ' ' +DateName( month , DateAdd( month , [MONTH] , 0 ) - 1 ) AS LastOpenMonth
		,[Month]
		,[Year]
	FROM Tbl_BillingMonths
	WHERE OpenStatusId=2
	ORDER BY ModifiedDate DESC
END	
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Customer Name change logs
---- ModifiedBy : Padmini
---- ModifiedDate : 22-01-2015
---- =============================================
ALTER PROCEDURE [USP_GetCustomerNameChangeLogsWithLimit] 
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END

	  SELECT(
			  SELECT  TOP 10 CNL.GlobalAccountNumber AS AccountNo
					  ,CNL.Remarks
					  ,CNL.OldTitle
					  ,CNL.NewTitle
					  ,CNL.OldFirstName AS OldName
					  ,CNL.NewFirstName AS [NewName]
					  ,CNL.OldMiddleName
					  ,CNL.NewMiddleName
					  ,CNL.OldLastName
					  ,CNL.NewLastName
					  ,CNL.OldKnownAs AS OldSurName
					  ,CNL.NewKnownAs AS NewSurName
					  ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_CustomerNameChangeLogs CNL
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId			  
			  GROUP BY CNL.GlobalAccountNumber
					  ,Remarks
					  ,OldTitle
					  ,NewTitle
					  ,OldFirstName
					  ,NewFirstName
					  ,OldMiddleName
					  ,NewMiddleName
					  ,OldLastName
					  ,NewLastName
					  ,OldKnownAs
					  ,NewKnownAs
					  ,CONVERT(VARCHAR(30),CNL.CreatedDate,107)
					  ,ApproveSttus.ApprovalStatus
					  ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsWithLimit]    Script Date: 03/17/2015 20:59:18 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Customer Name change logs
---- ModifiedBy : Padmini
---- ModifiedDate : 22-01-2015
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsWithLimit] 
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)

--		DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
--		INSERT INTO @Accounts
--			SELECT CNR.AccountNo FROM (
--				SELECT TOP 5 CN.AccountNo FROM Tbl_CustomerNameChangeLogs CN
--				JOIN [UDV_CustomerDescription] CD ON CN.AccountNo=CD.GlobalAccountNumber
--				AND (BU_ID=@BU_ID OR @BU_ID='')
--				AND (SU_ID=@SU_ID OR @SU_ID='')
--				AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--				AND (CycleId=@CycleId OR @CycleId='')
--				AND (TariffId=@TariffId OR @TariffId=0)
--				AND (CONVERT(DATE,CN.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--				AND (CONVERT(DATE,CN.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--				 ORDER BY CN.CreatedDate DESC
--				 ) CNR
--			 GROUP BY CNR.AccountNo
		 		 
--		CREATE TABLE #CustomerNameLogs(NameChangeLogId INT)

--		DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
--		SELECT @Count=COUNT(0) FROM @Accounts
--		SELECT @TotalChanges=COUNT(0) FROM Tbl_CustomerNameChangeLogs

--		WHILE(@I<=@Count)
--		BEGIN
--			INSERT INTO #CustomerNameLogs
--			SELECT TOP 10 NameChangeLogId FROM Tbl_CustomerNameChangeLogs WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
--			ORDER BY NameChangeLogId DESC
--			SET @I=@I+1
--		END 
		
--		SELECT
--		(
--			SELECT AccountNo            -- Changed fields as per new table
--					,Remarks
--					,OldTitle
--					,NewTitle
--					,OldFirstName
--					,NewFirstName
--					,OldMiddleName
--					,NewMiddleName
--					,OldLastName
--					,NewLastName
--					,OldKnownAs
--					,NewKnownAs
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(AccountNo) AS Name
--					,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
--					,@TotalChanges AS TotalChanges
--			FROM Tbl_CustomerNameChangeLogs CNL
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApproveStatusId
--			AND NameChangeLogId IN(
--			SELECT NameChangeLogId FROM #CustomerNameLogs
--			)
--			ORDER BY AccountNo DESC,CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get limited Meter change request logs
-- =============================================   
ALTER PROCEDURE [USP_GetCustomerMeterChangeLogsWithLimit]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
		
	  SELECT(
			  SELECT  TOP 10 CMI.AccountNo
			  		  ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType
					  ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType
					  ,CMI.Remarks
					  ,CMI.OldDials
					  ,CMI.NewDials
					  ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_CustomerMeterInfoChangeLogs  CMI
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId			  
			  GROUP BY CMI.AccountNo
			  		  ,OldMeterTypeId
					  ,NewMeterTypeId
					  ,CMI.Remarks
  					  ,CMI.OldDials
					  ,CMI.NewDials
					  ,CONVERT(VARCHAR(30),CMI.CreatedDate,107)
					  ,ApproveSttus.ApprovalStatus
					  ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Tariff change request logs
---- Modified By : Padmini
---- Modified Date : 26-12-2014
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogsWithLimit] 
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)

--		DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
--		INSERT INTO @Accounts
--			SELECT TCR.AccountNo FROM (
--				SELECT TOP 5 CT.AccountNo FROM Tbl_CustomerMeterInfoChangeLogs CT
--				JOIN [UDV_CustomerDescription] CD ON CT.AccountNo=CD.GlobalAccountNumber
--				AND (BU_ID=@BU_ID OR @BU_ID='')
--				AND (SU_ID=@SU_ID OR @SU_ID='')
--				AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--				AND (CycleId=@CycleId OR @CycleId='')
--				AND (TariffId=@TariffId OR @TariffId=0)
--				AND (CONVERT(DATE,CT.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--				AND (CONVERT(DATE,CT.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--				 ORDER BY CT.CreatedDate DESC
--				 ) TCR
--			 GROUP BY TCR.AccountNo
		 		 
--		CREATE TABLE #CustomerMeterLogs(MeterInfoChangeLogId INT)

--		DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
--		SELECT @Count=COUNT(0) FROM @Accounts
--		SELECT @TotalChanges=COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs

--		WHILE(@I<=@Count)
--		BEGIN
--			INSERT INTO #CustomerMeterLogs
--			SELECT TOP 10 MeterInfoChangeLogId FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
--			ORDER BY MeterInfoChangeLogId DESC
--			SET @I=@I+1
--		END 
		
--		SELECT
--		(
--			SELECT AccountNo
--					,Remarks
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(AccountNo) AS Name
--					,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
--					,@TotalChanges AS TotalChanges
--					,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=TCR.OldMeterTypeId) AS OldMeterType
--					,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=TCR.NewMeterTypeId) AS NewMeterType
--					,OldDials
--					,NewDials
--			FROM Tbl_CustomerMeterInfoChangeLogs TCR
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=TCR.ApproveStatusId
--			AND MeterInfoChangeLogId IN(
--			SELECT MeterInfoChangeLogId FROM #CustomerMeterLogs
--			)
--			ORDER BY AccountNo DESC,CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get limited Customer Address change logs
-- =============================================
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogsWithLimit] 
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
	

	  SELECT(
			  SELECT  TOP 10 CAL.GlobalAccountNumber AS AccountNo
						,OldHouseNo AS OldAddress
						,OldStreetName AS OldServiceAddress
						,OldCity
						,OldLandmark
						,OldAreaCode
						,OldZipCode
						,NewHouseNo AS NewAddress
						,NewStreetName AS NewServiceAddress
						,NewCity
						,NewLandmark
						,NewAreaCode
						,NewZipCode
						,Remarks
						,CONVERT(VARCHAR(30),CAL.CreatedDate,107) AS TransactionDate 
						,ApproveSttus.ApprovalStatus
						,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
						,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_CustomerAddressChangeLogs CAL
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CAL.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CAL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CAL.ApproveStatusId			  
			  GROUP BY CAL.GlobalAccountNumber
						,OldHouseNo
						,OldStreetName
						,OldCity
						,OldLandmark
						,OldAreaCode
						,OldZipCode
						,NewHouseNo
						,NewStreetName
						,NewCity
						,NewLandmark
						,NewAreaCode
						,NewZipCode
						,Remarks
						,CONVERT(VARCHAR(30),CAL.CreatedDate,107)
						,ApproveSttus.ApprovalStatus
						,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsWithLimit]    Script Date: 03/17/2015 21:11:33 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Customer Address change logs
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsWithLimit] 
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)

--		DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
--		INSERT INTO @Accounts
--			SELECT CNR.AccountNo FROM (
--				SELECT TOP 5 CN.AccountNo FROM Tbl_CustomerAddressChangeLogs CN
--				JOIN [UDV_CustomerDescription] CD ON CN.AccountNo=CD.GlobalAccountNumber
--				AND (BU_ID=@BU_ID OR @BU_ID='')
--				AND (SU_ID=@SU_ID OR @SU_ID='')
--				AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--				AND (CycleId=@CycleId OR @CycleId='')
--				AND (TariffId=@TariffId OR @TariffId=0)
--				AND (CONVERT(DATE,CN.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--				AND (CONVERT(DATE,CN.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--				 ORDER BY CN.CreatedDate DESC
--				 ) CNR
--			 GROUP BY CNR.AccountNo
		 		 
--		CREATE TABLE #CustomerAddressLogs(AddressChangeLogId INT)

--		DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
--		SELECT @Count=COUNT(0) FROM @Accounts
--		SELECT @TotalChanges=COUNT(0) FROM Tbl_CustomerAddressChangeLogs

--		WHILE(@I<=@Count)
--		BEGIN
--			INSERT INTO #CustomerAddressLogs
--			SELECT TOP 10 AddressChangeLogId FROM Tbl_CustomerAddressChangeLogs WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
--			ORDER BY AddressChangeLogId DESC
--			SET @I=@I+1
--		END 
		
--		SELECT
--		(
--			SELECT AccountNo
--					,Remarks
--					,OldPostalLandMark
--					,OldPostalStreet
--					,OldPostalCity
--					,OldPostalHouseNo
--					,OldPostalZipCode
--					,OldServiceLandMark
--					,OldServiceStreet
--					,OldServiceCity
--					,OldServiceHouseNo
--					,OldServiceZipCode
--					,NewPostalLandMark
--					,NewPostalStreet
--					,NewPostalCity
--					,NewPostalHouseNo
--					,NewPostalZipCode
--					,NewServiceLandMark
--					,NewServiceStreet
--					,NewServiceCity
--					,NewServiceHouseNo
--					,NewServiceZipCode
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(AccountNo) AS Name
--					,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
--					,@TotalChanges AS TotalChanges
--					,OldPostalHouseNo +'<br/>'+OldPostalLandMark+'<br/>'+OldPostalStreet+
--						'<br/>'+OldPostalCity+'<br/>'+OldPostalZipCode AS OldAddress
--					,OldServiceHouseNo +'<br/>'+OldServiceLandMark+'<br/>'+OldServiceStreet+
--						'<br/>'+OldServiceCity+'<br/>'+OldServiceZipCode AS OldServiceAddress
--					,NewPostalHouseNo +'<br/>'+NewPostalLandMark+'<br/>'+NewPostalStreet+
--						'<br/>'+NewPostalCity+'<br/>'+NewPostalZipCode AS NewAddress
--					,NewServiceHouseNo +'<br/>'+NewServiceLandMark+'<br/>'+NewServiceStreet+
--						'<br/>'+NewServiceCity+'<br/>'+NewServiceZipCode AS NewServiceAddress
--			FROM Tbl_CustomerAddressChangeLogs CNL
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApproveStatusId
--			AND AddressChangeLogId IN(
--			SELECT AddressChangeLogId FROM #CustomerAddressLogs
--			)
--			ORDER BY AccountNo DESC,CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get limited Book change request logs
-- =============================================   
ALTER PROCEDURE [USP_GetBookNoChangeLogsWithLimit]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
	  
	  --PRINT	 CONVERT(DATE,@FromDate)
	  --PRINT	 CONVERT(DATE,@Todate)

	  --select BookChange.CreatedDate,*
	  --  from Tbl_BookNoChangeLogs  BookChange
	  --LEFT JOIN [UDV_CustomerDescription]  CustomerView
	  --ON 
	  --BookChange.AccountNo=CustomerView.GlobalAccountNumber
	  
	  SELECT(
			  SELECT  TOP 10 BookChange.AccountNo
					  ,BookChange.ApproveStatusId
					  ,BookChange.OldBookNo
					  ,BookChange.NewBookNo
					  ,BookChange.Remarks
					  ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_BookNoChangeLogs  BookChange
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId			  
			  GROUP BY BookChange.AccountNo
					  ,BookChange.ApproveStatusId
					  ,BookChange.OldBookNo
					  ,BookChange.NewBookNo
					  ,BookChange.Remarks
					  ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107)
					  ,ApproveSttus.ApprovalStatus
					  ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
		
		--SELECT
		--	 @BU_ID='BEDC_BU_0024'	  --  BEDC_BU_0024
		--	,@SU_ID= 'BEDC_SU_0140' --BEDC_SU_0140,BEDC_SU_0153
		--	,@SC_ID='BEDC_SC_0611'   --BEDC_SC_0611,BEDC_SC_0626,BEDC_SC_0627
		--	,@CycleId= 'BEDC_C_0841'  --BEDC_C_0841,BEDC_C_0844,BEDC_C_0852,BEDC_C_0851
		--	,@TariffId=	 '3'--3,4,2,8
		--	,@FromDate=	 ''
		--	,@ToDate=	''
		--FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		--DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
		--INSERT INTO @Accounts
		--	SELECT BCL.AccountNo FROM (
		--	SELECT TOP 5 BL.AccountNo FROM Tbl_BookNoChangeLogs BL
		--		JOIN [UDV_CustomerDescription] CD ON BL.AccountNo=CD.GlobalAccountNumber
		--		AND (BU_ID=@BU_ID OR @BU_ID='')
		--		AND (SU_ID=@SU_ID OR @SU_ID='')
		--		AND (ServiceCenterId=@SC_ID OR @SC_ID='')
		--		AND (CycleId=@CycleId OR @CycleId='')
		--		AND (TariffId=@TariffId OR @TariffId=0)
		--		AND (CONVERT(DATE,BL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
		--		AND (CONVERT(DATE,BL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
		--	 ORDER BY BL.CreatedDate DESC
		--	 ) BCL
		--	 GROUP BY BCL.AccountNo
		 
		--CREATE TABLE #BookNoLogs(BookNoChangeLogId INT)

		--DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
		--SELECT @Count=COUNT(0) FROM @Accounts
		--SELECT @TotalChanges=COUNT(0) FROM Tbl_BookNoChangeLogs

		--WHILE(@I<=@Count)
		--BEGIN
		--	INSERT INTO #BookNoLogs
		--	SELECT TOP 10 BookNoChangeLogId FROM Tbl_BookNoChangeLogs WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
		--	ORDER BY BookNoChangeLogId DESC
		--	SET @I=@I+1
		--END 

		--SELECT
		--(
		--	SELECT AccountNo
		--			,OldBookNo
		--			,NewBookNo
		--			,Remarks
		--			,S.ApprovalStatus 
		--			,dbo.fn_GetCustomerFullName(AccountNo) AS Name
		--			,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
		--			,@TotalChanges AS TotalChanges
		--	FROM Tbl_BookNoChangeLogs BCL
		--	JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=BCL.ApproveStatusId
		--	AND BookNoChangeLogId IN(
		--	SELECT BookNoChangeLogId FROM #BookNoLogs
		--	)
		--	ORDER BY AccountNo DESC,CreatedDate DESC
		--	FOR XML PATH('AuditTrayList'),TYPE
		--)
		--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
 
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Padmini    
-- Create date: 05/02/2015    
-- Description: This Procedure is used to change customer address    
-- =============================================    
ALTER PROCEDURE [USP_ChangeCustomerBookNo]    
(    
 @XmlDoc xml       
)    
AS    
BEGIN    
 DECLARE    @GlobalAccountNo   VARCHAR(50)      
		   ,@NewPostalLandMark  VARCHAR(200)    
		   ,@NewPostalStreet   VARCHAR(200)    
		   ,@NewPostalCity   VARCHAR(200)    
		   ,@NewPostalHouseNo   VARCHAR(200)    
		   ,@NewPostalZipCode   VARCHAR(50)    
		   ,@NewServiceLandMark  VARCHAR(200)    
		   ,@NewServiceStreet   VARCHAR(200)    
		   ,@NewServiceCity   VARCHAR(200)    
		   ,@NewServiceHouseNo  VARCHAR(200)    
		   ,@NewServiceZipCode  VARCHAR(50)    
		   ,@NewPostalAreaCode  VARCHAR(50)    
		   ,@NewServiceAreaCode  VARCHAR(50)    
		   ,@NewPostalAddressID  INT    
		   ,@NewServiceAddressID  INT    
		   ,@IsSameAsServie           BIT    
		   ,@ModifiedBy    VARCHAR(50)    
		   ,@Details     VARCHAR(MAX)    
		   ,@RowsEffected    INT    
		   ,@AddressID INT   
		   ,@StatusText VARCHAR(50)
		   ,@IsCommunicationPostal BIT
		   ,@IsCommunicationService BIT
		   ,@BookNo VARCHAR(20)
		   ,@OldBookNo VARCHAR(50)
		   ,@Flag INT
     
       SELECT       
			 @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')    
		  ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(200)')        
		  ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')        
		  ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')       
		  ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')      
		  ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(200)')        
		  ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')        
		  ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')       
		  ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')    
		  ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')    
		  ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')    
		  ,@BookNo=C.value('(BookNo)[1]','VARCHAR(20)')    
		  ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')    
		  ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')    
		  ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')    
		  ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')    
		  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		  ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')    
		  ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')   
		  ,@Flag=C.value('(Flag)[1]','INT')
		  ,@OldBookNo=C.value('(OldBookNo)[1]','VARCHAR(50)')
		           
			 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
     
BEGIN
	BEGIN TRY
		BEGIN TRAN 
		
		SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)

		IF(@Flag = 1)
	 BEGIN
		
		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
										--,CustomerUniqueNo
										,OldBookNo
										,NewBookNo
										,CreatedBy
										,CreatedDate
										,ApproveStatusId
										,Remarks)
		VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),0,@Details)
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
										--,CustomerUniqueNo
										,OldBookNo
										,NewBookNo
										,CreatedBy
										,CreatedDate
										,ApproveStatusId
										,Remarks)
		VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),2,@Details)
	
	END	
		
		
		
		
		     IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE BookNo=@BookNo AND GlobalAccountNumber=@GlobalAccountNo)  
				BEGIN  
				
				DECLARE @BU_ID VARCHAR(50)
				,@SU_ID VARCHAR(50)
				,@ServiceCenterId VARCHAR(50)
				,@AccountNo VARCHAR(50)
				--Fetching ID's Of BU,SU,SC for acc no generation
				
				SELECT   @BU_ID=BU.BU_ID
						,@SU_ID=SU.SU_ID
						,@ServiceCenterId=SC.ServiceCenterId
				FROM Tbl_BookNumbers B
				JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
				JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
				JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
				JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
				WHERE B.BookNo=@BookNo
				
				SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate 
		
				  UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET     
						IsBookNoChanged=1
						,BookNo=@BookNo
				  WHERE GlobalAccountNumber=@GlobalAccountNo
				  
				  UPDATE [CUSTOMERS].[Tbl_CustomerSDetail] SET
						AccountNo=@AccountNo
				  WHERE GlobalAccountNumber=@GlobalAccountNo
				END
		  --UPDATE POSTAL ADDRESS
		  SET @StatusText='Postal Address Update.'
		   UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET    
			 LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END    
			,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END    
			,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END    
			,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
			,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END      
			,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END    
			,ModifedBy=@ModifiedBy    
			,ModifiedDate=dbo.fn_GetCurrentDateTime() 
			,IsCommunication=@IsCommunicationPostal   
			WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1
		   SET @RowsEffected=@@ROWCOUNT    
	              
		--IF CUSTOMER HAS SERVICE ADDRESS      
		IF(@IsSameAsServie=0)    
		 BEGIN   
			--IF CUSTOMER HAS SERVICE ADDRESS THEN UPDATE 
		  IF EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
			   WHERE IsServiceAddress=1 AND GlobalAccountNumber=@GlobalAccountNo AND IsActive=1)    
			   BEGIN      
					SET @StatusText='Service Address Update.'         
					UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET    
					 HouseNo=@NewServiceHouseNo     
					,LandMark=@NewServiceLandMark     
					,StreetName=@NewServiceStreet      
					,City=@NewServiceCity    
					,AreaCode=@NewServiceAreaCode    
					,ZipCode=@NewServiceZipCode      
					,ModifedBy=@ModifiedBy    
					,ModifiedDate=dbo.fn_GetCurrentDateTime()
					,IsCommunication=@IsCommunicationService       
					  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
					SET @RowsEffected=@@ROWCOUNT   
				END 
			ELSE
			
				BEGIN
					--CHECK IS CUSTOMER ALREADY HAVING SAME ADDRESS
					SET @AddressID =(SELECT  AddressID
					  FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]    
						  WHERE			GlobalAccountNumber=@GlobalAccountNo 
									  AND IsServiceAddress=1    
									  AND HouseNo=@NewServiceHouseNo 
									  --AND Landmark=@NewServiceLandMark 
									  AND StreetName=@NewServiceStreet     
									  AND City=@NewServiceCity 
									  --AND HouseNo=@NewServiceHouseNo 
									  AND ZipCode=@NewServiceZipCode    
									  AND AreaCode=@NewServiceAreaCode 
									  AND IsActive=0
						  )		
					--IF CUSTOMER HAVING SAME ADDRESS THEN CHANGE STATUS
					IF(@AddressID>0)			  
						 BEGIN  		
							SET @StatusText='Service Address IsActive Update.'                    
							  UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails    
							  SET IsActive=1,IsCommunication=@IsCommunicationService  
							  WHERE AddressID=@AddressID
							  SET @RowsEffected=@@ROWCOUNT    
						 END   
					-- IF CUSTOMER DOESN'T HAS SERVICE ADDRESS THEN INSERT SERVICE ADDRESS
					 ELSE
						 BEGIN  
							SET @StatusText='Service Address Insertion.'                     					 
							 INSERT INTO [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]    
																				  (    
																				   GlobalAccountNumber    
																				   ,HouseNo    
																				   ,Landmark    
																				   ,StreetName    
																				   ,City    
																				   ,AreaCode    
																				   ,ZipCode    
																				   ,CreatedDate    
																				   ,IsServiceAddress    
																				   ,IsCommunication
																				  )      
																				 VALUES    
																				 (    
																				   @GlobalAccountNo     
																				  ,@NewServiceHouseNo    
																				  ,@NewServiceLandMark    
																				  ,@NewServiceStreet    
																				  ,@NewServiceCity    
																				  ,@NewServiceAreaCode    
																				  ,@NewServiceZipCode    
																				  ,dbo.fn_GetCurrentDateTime()    
																				  ,1    
																				  ,@IsCommunicationService
																				 )   
											SET @RowsEffected=@@ROWCOUNT     
					 END
				
		   END   
	       
	        
		 END    
		ELSE
			BEGIN
				--DEACTIVATE ADDRESS IF CUSTOMER SELECT HIS POSTAL ADDRESS SAME AS SERVICE ADDRESS
				 IF EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
				   WHERE IsServiceAddress=1 AND GlobalAccountNumber=@GlobalAccountNo AND IsActive=1) 
				   BEGIN  
					  SET @StatusText='Service Address Logical Delettion.'                     					 
					  UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails    
					  SET IsActive=0 
					  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
					  SET @RowsEffected=@@ROWCOUNT   
					  
					 --IF CUSTOMER DELECT ALL HIS SERVICE ADDRESS, POSTAL ADDRESS HAS TO BE HIS COMMUNICATION ADDRESS
					IF NOT EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
									WHERE IsServiceAddress=1 
										AND GlobalAccountNumber=@GlobalAccountNo 
										AND IsActive=1)
										
					BEGIN
						UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails SET IsCommunication=1 
						WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1
					END										
				   
				   END
				   
				     --   UPDATE CUSTOMERS.Tbl_CustomerSDetail
					    --SET PostalAddress=
					    --[dbo].[fn_GetCustomerAddressFormat](@NewPostalHouseNo,@NewPostalStreet,@NewPostalCity,@NewPostalZipCode)
					    --WHERE GlobalAccountNumber=@GlobalAccountNo
					    
					    
					    --UPDATE CUSTOMERS.Tbl_CustomerSDetail
					    --SET ServiceAddress=
					    --[dbo].[fn_GetCustomerAddressFormat](@NewServiceHouseNo,@NewServiceStreet,@NewServiceCity,@NewServiceZipCode)
					    --WHERE GlobalAccountNumber=@GlobalAccountNo
				   
			END      
	COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN
		SET @RowsEffected =0
	END CATCH
		SELECT @RowsEffected AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')         
	END
END

--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerBookNo]    Script Date: 02/07/2015 14:31:52 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
------------------------------------------------------------------------------------------------  
---- =============================================    
---- Author:  V.Bhimaraju    
---- Create date: 29-09-2014    
---- Description: The purpose of this procedure is to Update Customer BookNo   
---- =============================================    
--ALTER PROCEDURE [dbo].[USP_ChangeCustomerBookNo]    
--(    
--@XmlDoc xml    
--)    
--AS    
--BEGIN    
-- DECLARE @AccountNo VARCHAR(50)  
--  --,@BU_ID VARCHAR(20)    
--  --      ,@SU_ID VARCHAR(20)    
--  --,@ServiceCenterId VARCHAR(20)    
--     ,@BookNo VARCHAR(20)  
--     ,@PostalLandMark VARCHAR(200)    
--     ,@PostalStreet VARCHAR(200)    
--     ,@PostalCity VARCHAR(200)    
--     ,@PostalHouseNo VARCHAR(200)  
--     ,@PostalZipCode VARCHAR(50)  
--     ,@ServiceLandMark VARCHAR(200)    
--     ,@ServiceStreet VARCHAR(200)    
--     ,@ServiceCity VARCHAR(200)    
--     ,@ServiceHouseNo VARCHAR(200)  
--     ,@ServiceZipCode VARCHAR(50)  
--     ,@ModifiedBy VARCHAR(50)  
--     ,@IsBookNoChanged BIT  
--     --,@ActiveStatusId INT
--     ,@Flag INT  
--     ,@Details VARCHAR(MAX)
       
       
--   SELECT     
--     @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
--    --,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')    
--    --,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')    
--    --,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')    
--    ,@BookNo=C.value('(BookNo)[1]','VARCHAR(20)')  
--    ,@PostalLandMark=C.value('(PostalLandMark)[1]','VARCHAR(200)')    
--    ,@PostalStreet=C.value('(PostalStreet)[1]','VARCHAR(200)')    
--    ,@PostalCity=C.value('(PostalCity)[1]','VARCHAR(200)')    
--    ,@PostalHouseNo=C.value('(PostalHouseNo)[1]','VARCHAR(200)')   
--    ,@PostalZipCode=C.value('(PostalZipCode)[1]','VARCHAR(50)')  
--    ,@ServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')    
--    ,@ServiceStreet=C.value('(ServiceStreet)[1]','VARCHAR(200)')    
--    ,@ServiceCity=C.value('(ServiceCity)[1]','VARCHAR(200)')    
--    ,@ServiceHouseNo=C.value('(ServiceHouseNo)[1]','VARCHAR(200)')   
--    ,@ServiceZipCode=C.value('(ServiceZipCode)[1]','VARCHAR(50)')  
--    --,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT') 
--    ,@Flag=C.value('(Flag)[1]','INT')  
--    ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
--    ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
-- FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
 
-- --DECLARE @CustUniqueno VARCHAR(50)   = (SELECT CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo)
-- DECLARE @OldBookNo VARCHAR(20)   = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
   
-- IF(@Flag = 1)
--	 BEGIN
		
--		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
--										--,CustomerUniqueNo
--										,OldBookNo
--										,NewBookNo
--										,CreatedBy
--										,CreatedDate
--										,ApproveStatusId
--										,Remarks)
--		VALUES(@AccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),0,@Details)
--	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
--	 END 
-- ELSE
--	BEGIN
	
--		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
--										--,CustomerUniqueNo
--										,OldBookNo
--										,NewBookNo
--										,CreatedBy
--										,CreatedDate
--										,ApproveStatusId
--										,Remarks)
--		VALUES(@AccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),2,@Details)
	
--		IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE BookNo=@BookNo AND GlobalAccountNumber=@AccountNo)  
--		BEGIN  
--			  UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET     
--					IsBookNoChanged=1  
--			  WHERE GlobalAccountNumber=@AccountNo  
    
--				UPDATE Tbl_Audit_CustomerDetails SET     
--					IsBookNoChanged=1  
--				WHERE AccountNo=@AccountNo  
--		END  
    
--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET  
--				--BU_ID=@BU_ID  
--			 --  ,SU_ID=@SU_ID  
--			 --  ,ServiceCenterId=@ServiceCenterId  
--			   BookNo=@BookNo  
--			    ,ModifedBy=@ModifiedBy  
--			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
--		WHERE GlobalAccountNumber=@AccountNo  

--		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET
--			   LandMark=CASE @PostalLandMark WHEN '' THEN NULL ELSE @PostalLandMark END      
--			   ,StreetName=CASE @PostalStreet WHEN '' THEN NULL ELSE @PostalStreet END      
--			   ,City=CASE @PostalCity WHEN '' THEN NULL ELSE @PostalCity END      
--			   ,HouseNo=CASE @PostalHouseNo WHEN '' THEN NULL ELSE @PostalHouseNo END    
--			   ,ZipCode=CASE @PostalZipCode WHEN '' THEN NULL ELSE @PostalZipCode END  
--			   ,ModifedBy=@ModifiedBy  
--			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
--		WHERE GlobalAccountNumber=@AccountNo  AND IsServiceAddress=0

--		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET
--			    LandMark=CASE @ServiceLandMark WHEN '' THEN NULL ELSE @ServiceLandMark END      
--			   ,StreetName=CASE @ServiceStreet WHEN '' THEN NULL ELSE @ServiceStreet END      
--			   ,City=CASE @ServiceCity WHEN '' THEN NULL ELSE @ServiceCity END      
--			   ,HouseNo=CASE @ServiceHouseNo WHEN '' THEN NULL ELSE @ServiceHouseNo END    
--			   ,ZipCode=CASE @ServiceZipCode WHEN '' THEN NULL ELSE @ServiceZipCode END  
--			   ,ModifedBy=@ModifiedBy  
--			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
--		WHERE GlobalAccountNumber=@AccountNo  AND IsServiceAddress=1
      
--      --Audit Table
--		UPDATE Tbl_Audit_CustomerDetails SET     
--			--BU_ID=@BU_ID  
--		  -- ,SU_ID=@SU_ID  
--		  -- ,ServiceCenterId=@ServiceCenterId  
--		    BookNo=@BookNo  
--		   ,PostalLandMark=CASE @PostalLandMark WHEN '' THEN NULL ELSE @PostalLandMark END      
--		   ,PostalStreet=CASE @PostalStreet WHEN '' THEN NULL ELSE @PostalStreet END      
--		   ,PostalCity=CASE @PostalCity WHEN '' THEN NULL ELSE @PostalCity END      
--		   ,PostalHouseNo=CASE @PostalHouseNo WHEN '' THEN NULL ELSE @PostalHouseNo END    
--		   ,PostalZipCode=CASE @PostalZipCode WHEN '' THEN NULL ELSE @PostalZipCode END  
--		   ,ServiceLandMark=CASE @ServiceLandMark WHEN '' THEN NULL ELSE @ServiceLandMark END      
--		   ,ServiceStreet=CASE @ServiceStreet WHEN '' THEN NULL ELSE @ServiceStreet END      
--		   ,ServiceCity=CASE @ServiceCity WHEN '' THEN NULL ELSE @ServiceCity END      
--		   ,ServiceHouseNo=CASE @ServiceHouseNo WHEN '' THEN NULL ELSE @ServiceHouseNo END    
--		   ,ServiceZipCode=CASE @ServiceZipCode WHEN '' THEN NULL ELSE @ServiceZipCode END
--		   --,IsBookNoChanged=1  
--		   ,ModifiedBy=@ModifiedBy  
--		   --,ActiveStatusId=@ActiveStatusId  
--		   ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
--		WHERE AccountNo=@AccountNo  
        
--		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
 
--	END
-- END
GO
GO
/****** Object:  View [dbo].[UDV_CustomerDescription]    Script Date: 03/18/2015 18:30:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------\  */
ALTER VIEW [dbo].[UDV_CustomerDescription]
AS
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs, 
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo, 
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId, 
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID, 
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName, 
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading, 
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading, 
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID,BN.SortOrder As BookSortOrder,
                      --cd.ServiceAddress
                      --CD.PostalAddress
                      TC.RefClassID
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date:	06-10-2014
-- Description:	The purpose of this procedure is to get Tariff change logs based on search criteria
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- =============================================
ALTER PROCEDURE [USP_GetTariffChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
			DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END		
	  
	  SELECT(
			  SELECT   TCR.AccountNo
					  ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff
					  ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff
					  ,TCR.Remarks
					  ,TCR.CreatedBy
					  ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			  FROM Tbl_LCustomerTariffChangeRequest  TCR
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId			  
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')		
			
		
		--SELECT
		--(
		--	SELECT CT.AccountNo
		--			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff
		--			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff
		--			,Remarks
		--			,S.ApprovalStatus 
		--			,dbo.fn_GetCustomerFullName(cd.GlobalAccountNumber) AS Name
		--			,CONVERT(VARCHAR(30),CT.CreatedDate,107) AS TransactionDate
		--	FROM Tbl_LCustomerTariffChangeRequest CT
		--	JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CT.ApprovalStatusId
		--	--JOIN UDV_CustomerDetails CD ON CT.AccountNo=CD.AccountNo
		--	JOIN [UDV_CustomerDescription] CD ON CT.AccountNo=CD.GlobalAccountNumber --Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]
		--	AND (BU_ID=@BU_ID OR @BU_ID='')
		--	AND (SU_ID=@SU_ID OR @SU_ID='')
		--	AND (ServiceCenterId=@SC_ID OR @SC_ID='')
		--	AND (CycleId=@CycleId OR @CycleId='')
		--	AND (TariffId=@TariffId OR @TariffId=0)
		--	AND (CONVERT(DATE,CT.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
		--	AND (CONVERT(DATE,CT.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
		--	ORDER BY CT.CreatedDate DESC
		--	FOR XML PATH('AuditTrayList'),TYPE
		--)
		--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Customer Name change logs
---- ModifiedBy : Padmini
---- ModifiedDate : 22-01-2015
---- =============================================
ALTER PROCEDURE [USP_GetCustomerNameChangeLogs] 
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END

	  SELECT(
			  SELECT   CNL.GlobalAccountNumber AS AccountNo
					  ,CNL.Remarks
					  ,CNL.OldTitle
					  ,CNL.NewTitle
					  ,CNL.OldFirstName
					  ,CNL.NewFirstName
					  ,CNL.OldMiddleName
					  ,CNL.NewMiddleName
					  ,CNL.OldLastName
					  ,CNL.NewLastName
					  ,CNL.OldKnownAs
					  ,CNL.NewKnownAs
					  ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,CNL.CreatedBy
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			  FROM Tbl_CustomerNameChangeLogs CNL
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId			  
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END

--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogs]    Script Date: 03/18/2015 12:52:37 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get Customer Name change logs based on search criteria
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogs]
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
--		SELECT
--		(
--			SELECT CNL.AccountNo
--					,Remarks
--					--,OldName
--					--,[NewName]
--					--,OldSurName
--					--,NewSurName
--					,OldKnownAs
--					,NewKnownAs
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
--					,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate
--			FROM Tbl_CustomerNameChangeLogs CNL
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApproveStatusId
--			JOIN [UDV_CustomerDescription] CD ON CNL.AccountNo=CD.GlobalAccountNumber
--			AND (BU_ID=@BU_ID OR @BU_ID='')
--			AND (SU_ID=@SU_ID OR @SU_ID='')
--			--AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--			AND (CycleId=@CycleId OR @CycleId='')
--			AND (TariffId=@TariffId OR @TariffId=0)
--			AND (CONVERT(DATE,CNL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--			AND (CONVERT(DATE,CNL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--			ORDER BY CNL.CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get limited Meter change request logs
-- =============================================   
ALTER PROCEDURE [USP_GetCustomerMeterChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
		
	  SELECT(
			  SELECT  CMI.AccountNo
			  		  ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType
					  ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType
					  ,CMI.Remarks
					  ,CMI.OldDials
					  ,CMI.NewDials
					  ,CMI.CreatedBy
					  ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,CMI.CreatedBy
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			  FROM Tbl_CustomerMeterInfoChangeLogs  CMI
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId			  
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogs]    Script Date: 03/18/2015 12:44:46 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get BookNo change logs based on search criteria
---- Modified By : Padmini
---- Modified Date :26-12-2014
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
--		SELECT
--		(
--			SELECT CML.AccountNo
--					,Remarks
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(cd.GlobalAccountNumber) AS Name
--					,CONVERT(VARCHAR(30),CML.CreatedDate,107) AS TransactionDate
--					,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CML.OldMeterTypeId) AS OldMeterType
--					,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CML.NewMeterTypeId) AS NewMeterType
--					,OldDials
--					,NewDials
--			FROM Tbl_CustomerMeterInfoChangeLogs CML
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CML.ApproveStatusId
--			JOIN [UDV_CustomerDescription] CD ON CML.AccountNo=CD.GlobalAccountNumber
--			AND (BU_ID=@BU_ID OR @BU_ID='')
--			AND (SU_ID=@SU_ID OR @SU_ID='')
--			AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--			AND (CycleId=@CycleId OR @CycleId='')
--			AND (TariffId=@TariffId OR @TariffId=0)
--			AND (CONVERT(DATE,CML.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--			AND (CONVERT(DATE,CML.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--			ORDER BY CML.CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get BookNo change logs based on search criteria
---- =============================================
ALTER PROCEDURE [USP_GetCustomerChangeLogsWithLimit]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END		
	  
	  SELECT(
			  SELECT  TOP 10 CCL.AccountNo
					  ,CCL.ApproveStatusId
					  ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus
					  ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus
					  ,CCL.Remarks
					  ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_CustomerActiveStatusChangeLogs CCL	
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId			  
			  GROUP BY CCL.AccountNo
					  ,CCL.ApproveStatusId
					  ,CCL.Remarks
					  ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) 
					  ,ApproveSttus.ApprovalStatus
					  ,OldStatus
					  ,NewStatus
					  ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')	
 
END
--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit]    Script Date: 03/18/2015 12:30:35 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Tariff change request logs
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit] 
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)

--		DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
--		INSERT INTO @Accounts
--			SELECT TCR.AccountNo FROM (
--				SELECT TOP 5 CT.AccountNo FROM Tbl_CustomerActiveStatusChangeLogs CT
--				JOIN [UDV_CustomerDescription] CD ON CT.AccountNo=CD.GlobalAccountNumber
--				AND (BU_ID=@BU_ID OR @BU_ID='')
--				AND (SU_ID=@SU_ID OR @SU_ID='')
--				AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--				AND (CycleId=@CycleId OR @CycleId='')
--				AND (TariffId=@TariffId OR @TariffId=0)
--				AND (CONVERT(DATE,CT.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--				AND (CONVERT(DATE,CT.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--				 ORDER BY CT.CreatedDate DESC
--				 ) TCR
--			 GROUP BY TCR.AccountNo
		 		 
--		CREATE TABLE #CustomerLogs(ActiveStatusChangeLogId INT)

--		DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
--		SELECT @Count=COUNT(0) FROM @Accounts
--		SELECT @TotalChanges=COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs

--		WHILE(@I<=@Count)
--		BEGIN
--			INSERT INTO #CustomerLogs
--			SELECT TOP 10 ActiveStatusChangeLogId FROM Tbl_CustomerActiveStatusChangeLogs WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
--			ORDER BY ActiveStatusChangeLogId DESC
--			SET @I=@I+1
--		END 
		
--		SELECT
--		(
--			SELECT AccountNo
--					,Remarks
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(AccountNo) AS Name
--					,(SELECT [Status] FROM Tbl_MActiveStatusDetails WHERE ActiveStatusId=TCR.OldStatus) AS OldStatus
--					,(SELECT [Status] FROM Tbl_MActiveStatusDetails WHERE ActiveStatusId=TCR.NewStatus) AS NewStatus
--					,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
--					,@TotalChanges AS TotalChanges
--			FROM Tbl_CustomerActiveStatusChangeLogs TCR
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=TCR.ApproveStatusId
--			AND ActiveStatusChangeLogId IN(
--			SELECT ActiveStatusChangeLogId FROM #CustomerLogs
--			)
--			ORDER BY AccountNo DESC,CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get BookNo change logs based on search criteria
---- =============================================
ALTER PROCEDURE [USP_GetCustomerChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END		
	  
	  SELECT(
			  SELECT  CCL.AccountNo
					  ,CCL.ApproveStatusId
					  ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus
					  ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus
					  ,CCL.Remarks
					  ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,CCL.CreatedBy
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			  FROM Tbl_CustomerActiveStatusChangeLogs CCL	
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId			  
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')	
 
END

--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogs]    Script Date: 03/17/2015 20:33:52 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get BookNo change logs based on search criteria
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
--		SELECT
--		(
--			SELECT CCL.AccountNo
--					,Remarks
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
--					,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate
--					--,(SELECT [Status] FROM Tbl_MActiveStatusDetails WHERE ActiveStatusId=CCL.OldStatus) AS OldStatus
--					--,(SELECT [Status] FROM Tbl_MActiveStatusDetails WHERE ActiveStatusId=CCL.NewStatus) AS NewStatus
--					,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus
--					,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus
--			FROM Tbl_CustomerActiveStatusChangeLogs CCL
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CCL.ApproveStatusId
--		  --JOIN UDV_CustomerDetails CD ON CCL.AccountNo=CD.AccountNo
--			JOIN [UDV_CustomerDescription] CD ON CCL.AccountNo=CD.GlobalAccountNumber
--			AND (BU_ID=@BU_ID OR @BU_ID='')
--			AND (SU_ID=@SU_ID OR @SU_ID='')
--			AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--			AND (CycleId=@CycleId OR @CycleId='')
--			AND (TariffId=@TariffId OR @TariffId=0)
--			AND (CONVERT(DATE,CCL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--			AND (CONVERT(DATE,CCL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--			ORDER BY CCL.CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get BookNo change logs based on search criteria
-- =============================================
ALTER PROCEDURE [USP_GetBookNoChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
				@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
				,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
				,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
				,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
				,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
				,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
				,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
		
		SELECT(
			  SELECT   BookChange.AccountNo
					  ,BookChange.OldBookNo
					  ,BookChange.NewBookNo
					  ,BookChange.Remarks
					  ,BookChange.CreatedBy
					  ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,CustomerView.BusinessUnitName
					  ,CustomerView.ServiceUnitName
					  ,CustomerView.ServiceCenterName
					  ,CustomerView.CycleName
					  ,CustomerView.BookCode
					  ,CustomerView.SortOrder AS CustomerSortOrder
					  ,CustomerView.BookSortOrder AS BookSortOrder
					  ,CustomerView.OldAccountNo
			  FROM Tbl_BookNoChangeLogs  BookChange
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId			  
			  
		FOR XML PATH('AuditTrayList'),TYPE
		)
		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
		
		--SELECT
		--(
		--	SELECT BCL.AccountNo
		--			,OldBookNo
		--			,NewBookNo
		--			,Remarks
		--			,S.ApprovalStatus 
		--			,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		--			,CONVERT(VARCHAR(30),BCL.CreatedDate,107) AS TransactionDate
		--	FROM Tbl_BookNoChangeLogs BCL
		--	JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=BCL.ApproveStatusId
		--	JOIN [UDV_CustomerDescription] CD ON BCL.AccountNo=CD.GlobalAccountNumber
		--	AND (BU_ID=@BU_ID OR @BU_ID='')
		--	AND (SU_ID=@SU_ID OR @SU_ID='')
		--	AND (ServiceCenterId=@SC_ID OR @SC_ID='')
		--	AND (CycleId=@CycleId OR @CycleId='')
		--	AND (TariffId=@TariffId OR @TariffId=0)
		--	AND (CONVERT(DATE,BCL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
		--	AND (CONVERT(DATE,BCL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
		--	ORDER BY BCL.CreatedDate DESC
		--	FOR XML PATH('AuditTrayList'),TYPE
		--)
		--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
GO

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  M.RamaDevi  
-- Create date: 12-04-2014
-- Author:  V.Bhimaraju
-- Create date: 07-08-2014
-- Modified By: T.Karthik
-- Modified Date: 29-10-2014
-- Modified By: Karteek.P
-- Modified Date: 18-03-2015
-- Description: To get Accounts without cycle  Customers
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAccountWithoutCycle]
(
	@xmlDoc xml
)
AS  
BEGIN  
		DECLARE    @BU_ID VARCHAR(MAX)   
				  ,@SU_ID VARCHAR(MAX)  
				  ,@ServiceCenterId VARCHAR(MAX)  
				  ,@Tariff VARCHAR(MAX)  
				  ,@PageNo INT  
				  ,@PageSize INT   
		SELECT	 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
				,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
				,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
				,@Tariff=C.value('(Tariff)[1]','VARCHAR(MAX)')  
				,@PageNo = C.value('(PageNo)[1]','INT')  
				,@PageSize = C.value('(PageSize)[1]','INT')   
		FROM @xmlDoc.nodes('ConsumerBe') AS T(C)  
  

		--OLD Code
		--select C.CustomerUniqueNo,C.Name,  
		-- ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo ASC) AS RowNumber,  
		--case when C.ServiceHouseNo is null then '' else C.ServiceHouseNo end  
		--+  
		--case when C.ServiceLandMark is null then '' else ','+C.ServiceLandMark end+  
		--case when C.ServiceStreet is null then '' else ','+C.ServiceStreet end+  
		--case when C.ServiceCity is null then '' else ','+C.ServiceCity end+  
		--case when C.ServiceZipCode is null then '' else ','+C.ServiceZipCode end as SetviceAddress  
		--,TC.ClassName  
		--,C.AccountNo AS AccountNo  
		--,convert(varchar(30),C.ConnectionDate,106)as ConnectionDate from Tbl_CustomerDetails C   
		--JOIN Tbl_MTariffClasses TC on C.TariffId=TC.ClassID and TC.IsActiveClass=1  
		--where C.CustomerUniqueNo not in(  
		--select D.CustomerUniqueNo  
		-- from Tbl_CustomerDetails D  
		-- join Tbl_Cycles T  
		--on T.BU_ID=D.BU_ID and T.SU_ID=D.SU_ID and T.ServiceCenterId=D.ServiceCenterId and t.ActiveStatusId=1)  
		--and (C.BU_ID=@BU or @BU='')and (C.SU_ID=@SU or @SU='')and (C.ServiceCenterId=@SC or @SC='')and (C.TariffId=@Tariff or @Tariff=''))  
		
		CREATE TABLE #CustWithoutCycle(Sno INT PRIMARY KEY IDENTITY(1,1),AccountNo VARCHAR(50)) -- COLLATE SQL_Latin1_General_CP1_CI_AS

		INSERT INTO #CustWithoutCycle(AccountNo)
						  SELECT	
								CD.GlobalAccountNumber
						FROM [UDV_CustomerDescription] CD
						WHERE (SELECT dbo.fn_GetCycleIdByBook(CD.BookNo)) IS NULL
						AND  (BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID,',')) OR @BU_ID = '')
						AND (SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID = '')
						AND (ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId = '')
						AND (TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,',')) OR @Tariff = '')
						ORDER BY CD.GlobalAccountNumber ASC

		DECLARE @Count INT=0
		SELECT @Count=COUNT(0) FROM #CustWithoutCycle

	  SELECT	
				Sno AS RowNumber
				,CD.AccountNo AS AccountNo
				,CD.GlobalAccountNumber AS GlobalAccountNumber
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				--,(SELECT dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)) AS Name
				,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,(SELECT dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber)) AS ServiceAddress
				--,CD.TariffId
				,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=CD.TariffId) AS ClassName
				,CD.ServiceCenterName
				,CD.ServiceUnitName
				,CD.BusinessUnitName
				,CD.SortOrder AS CustomerSortOrder
				--,CD.ConnectionDate
				,CD.BookNo AS BookNo
				,(SELECT dbo.fn_GetCycleIdByBook(CD.BookNo)) AS CycleId
				,ISNULL(CONVERT(VARCHAR(20),ConnectionDate,106),'--') AS ConnectionDate
				,@Count as TotalRecords
		FROM [UDV_CustomerDescription] CD
		JOIN #CustWithoutCycle AS LCWC ON LCWC.AccountNo COLLATE DATABASE_DEFAULT = CD.GlobalAccountNumber COLLATE DATABASE_DEFAULT
		ORDER BY Sno ASC 
END  
 ----------------------------------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M.RamaDevi
-- Create date: 12-04-2014
-- ModifiedBy: Bhimaraju Vanka
-- ModifiedDate: 16_10-2014
-- Description:	To get Accounts without Meter
-- Modified By: Padmini
--Modifed Date :04/02/2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithoutMeter](@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(ServiceCenterId)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('ConsumerBe') AS T(C)

CREATE TABLE #CustomersNoMeter(Sno INT PRIMARY KEY IDENTITY(1,1),AccountNo VARCHAR(50))

	INSERT INTO #CustomersNoMeter(AccountNo)
		  SELECT    
		   CD.GlobalAccountNumber  
		  FROM [UDV_CustomerDescription] CD
			LEFT JOIN Tbl_MTariffClasses TC on CD.TariffId=TC.ClassID
			WHERE CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU,','))
			AND CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU,','))
			AND CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@SC,','))
			AND CD.TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,','))
			AND ReadCodeId=1
			
			
		SELECT      
			Sno AS RowNumber
			,CD.GlobalAccountNumber
			,(Select COUNT(0) from #CustomersNoMeter) as TotalRecords    
			--,(SELECT dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)) AS Name
			,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,ROW_NUMBER() OVER(ORDER BY CD.SortOrder ASC) AS RowNumber
			,(SELECT dbo.fn_GetCustomerServiceAddress(SC.AccountNo)) As ServiceAddress
			,CD.ClassName
			,(ISNULL(OldAccountNo,'--')) AS OldAccountNo
			,CD.AccountNo AS AccountNo
			,CD.SortOrder AS CustomerSortOrder
			,BU.BusinessUnitName
			,SU.ServiceUnitName
			,SCC.ServiceCenterName	
		FROM #CustomersNoMeter AS SC
	JOIN [UDV_CustomerDescription] AS CD ON SC.AccountNo COLLATE DATABASE_DEFAULT=CD.GlobalAccountNumber COLLATE DATABASE_DEFAULT  
	JOIN Tbl_BussinessUnits AS BU ON CD.BU_ID=BU.BU_ID  
	JOIN Tbl_ServiceUnits AS SU ON CD.SU_ID=SU.SU_ID
	JOIN Tbl_ServiceCenter AS SCC ON CD.ServiceCenterId=SCC.ServiceCenterId
    AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
    ORDER BY Sno ASC 
 END
-------------------------------------------------------------------------------------------------------------------------
------------------Faiz-ID103---------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariff]    Script Date: 03/17/2015 06:55:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff>
-- =============================================
-- EXEC USP_RptGetCustomersStatisticsAndTariff
CREATE PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariff] 
	-- Add the parameters for the stored procedure here
	(
	@XmlDoc XML
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 --SET @Month=1    
 --SET @Year=2015    
 --SET @BU_ID='BEDC_BU_0024'    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('NewReportsBe') AS T(C)    
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			Cd.ClassName AS Tariff,
			CD.ActiveStatus AS CustomerStatus,
			ISNULL(CBills.LastPaymentTotal,0) AS RevenueCollected,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
	CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
	AND CD.GlobalAccountNumber=CBills.AccountNo

	SELECT Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(ClossingBalance) As ClossingBalance
	FROM
	(
		SELECT	Tariff, 
			COUNT(CustomerStatus) AS TotalCustomerTariff,
			CustomerStatus,
			SUM(EnergyBilled) AS EnergyBilled, 
			SUM(RevenueBilled) AS RevenueBilled,
			SUM(RevenueCollected) AS RevenueCollected,
			SUM(ClossingBalance) AS ClossingBalance
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff
	ORDER BY Tariff

	DROP TABLE #tmpCustomerStatistics
END
-------------------------------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses]    Script Date: 03/17/2015 06:55:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff with parent classes>
-- =============================================
-- EXEC USP_RptGetCustomersStatisticsAndTariffClasses_Rajaiah 
CREATE PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses] 
	-- Add the parameters for the stored procedure here
	(
	@XmlDoc XML
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 --SET @Month=1    
 --SET @Year=2015    
 --SET @BU_ID='BEDC_BU_0024'    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('NewReportsBe') AS T(C)  
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			Cd.ClassName AS Tariff,
			CD.ActiveStatus AS CustomerStatus,
			ISNULL(CBills.LastPaymentTotal,0) AS RevenueCollected,
			ISNULL(CBills.TotalBillAmountWithTax,0) AS AmountBilled,
			ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance,
			MTC.Description AS TariffDescription,
			CD.RefClassID 
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
						CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
						AND CD.GlobalAccountNumber=CBills.AccountNo
	INNER JOIN Tbl_MTariffClasses MTC(NOLOCK) ON MTC.ClassID=CD.RefClassID 

	SELECT RefClassID, 
		TariffDescription,
		Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(AmountBilled) AS AmountBilled,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) As ClossingBalance
		INTO #tmpCustomerStatisticsInfo
	FROM
	(
		SELECT	RefClassID,
			TariffDescription,
			Tariff, 
			COUNT(CustomerStatus) AS TotalCustomerTariff,
			CustomerStatus,
			SUM(EnergyBilled) AS EnergyBilled, 
			SUM(RevenueBilled) AS RevenueBilled,
			SUM(RevenueCollected) AS RevenueCollected,
			SUM(AmountBilled) AS AmountBilled,
			SUM(OpeningBalance) AS OpeningBalance,
			SUM(ClossingBalance) AS ClossingBalance
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff,TariffDescription,RefClassID 
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff,TariffDescription,RefClassID 
	ORDER BY Tariff

	
	SELECT ROW_NUMBER() OVER(ORDER BY RefClassID  ASC) AS ID,
		RefClassID ,
		TariffDescription,
		'SUB Total' AS Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(TotalPopulation,0)) AS TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(AmountBilled) AS AmountBilled,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) As ClossingBalance
		INTO #tmpCustomerStatisticsInfoSubTotal
	FROM #tmpCustomerStatisticsInfo
	GROUP BY TariffDescription,RefClassID 
	
	--==========================================================================================================
	DECLARE @tblCustomerStatistics AS TABLE
	(
		ID INT IDENTITY(1,1),
		RefClassID INT,
		TariffDescription VARCHAR(8000),
		Tariff VARCHAR(300),
		Active INT,
		InActive INT,
		--Hold INT,
		TotalPopulation INT,
		EnergyBilled DECIMAL(18,2),
		RevenueBilled DECIMAL(18,2),
		RevenueCollected DECIMAL(18,2),		
		AmountBilled DECIMAL(18,2),
		OpeningBalance DECIMAL(18,2),
		ClossingBalance DECIMAL(18,2)
	)
	DECLARE @index INT, @rwCount INT,@RefClassID INT 
	SET @index=0
	SELECT @rwCount=COUNT(ID) FROM #tmpCustomerStatisticsInfoSubTotal
	
	--SELECT * FROM #tmpCustomerStatisticsInfoSubTotal
	
	-- Loop for caluclating sub total
	WHILE @index<@rwCount
	BEGIN
		SET @index=@index+1
		
		SELECT @RefClassID=RefClassID FROM #tmpCustomerStatisticsInfoSubTotal WHERE ID=@index
		
		INSERT INTO @tblCustomerStatistics(RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance)
		SELECT RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance
		FROM #tmpCustomerStatisticsInfo WHERE RefClassID=@RefClassID
		
		INSERT INTO @tblCustomerStatistics(RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance)
		SELECT RefClassID,	'' AS TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance
		FROM #tmpCustomerStatisticsInfoSubTotal WHERE ID=@index

	END
	
	INSERT INTO @tblCustomerStatistics(RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance)
	SELECT 0 AS RefClassID ,
		'' AS TariffDescription,
		'GRAND Total' AS Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(TotalPopulation,0)) AS TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(AmountBilled) AS AmountBilled,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) As ClossingBalance		
	FROM #tmpCustomerStatisticsInfoSubTotal 
	

	SELECT ID,TariffDescription,Tariff AS TariffClass,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance
	FROM @tblCustomerStatistics
	ORDER BY ID
	--==========================================================================================================

	DROP TABLE #tmpCustomerStatistics	
	DROP TABLE #tmpCustomerStatisticsInfo
	DROP TABLE #tmpCustomerStatisticsInfoSubTotal

END

GO
-------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo_Rajaiah]    Script Date: 03/17/2015 06:55:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
-- EXEC USP_RptGetCustomersStatisticsInfo_Rajaiah
CREATE PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo] 
	-- Add the parameters for the stored procedure here
(  
@XmlDoc Xml=null  
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	--SET @Month=1
	--SET @Year=2015
	--SET @BU_ID='BEDC_BU_0024'
   
 SELECT   @Month = C.value('(Month)[1]','INT')  
   ,@Year = C.value('(Year)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('NewReportsBe') AS T(C)  
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CD.ClassName AS Tariff,
			--'' AS KWHSold,
			--'' AS KVASold,
			ISNULL(CBills.NetFixedCharges,0) AS FixedCharges,
			-- No Billed 
			ISNULL(CBills.TotalBillAmount,0) AS TotaAmountBilled,			
			ISNULL(CBills.PaidAmount,0) AS TotaAmountCollected,
			--'' AS NoOfStubs,
			-- Total population
			--'' AS WeightedAverage,
			CD.ActiveStatus AS CustomerStatus
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
	CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
	AND CD.GlobalAccountNumber=CBills.AccountNo

	SELECT Tariff,
		'' AS KWHSold,
		'' AS KVASold,
		SUM(FixedCharges) AS FixedCharges,
		COUNT(TotaAmountBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		'' AS WeightedAverage
		INTO #tmpCustomerStatisticsList
	FROM
	(
		SELECT	 Tariff,
			--KWHSold,
			--KVASold,
			COUNT(CustomerStatus) AS TotalCustomerTariff,		
			SUM(FixedCharges) AS FixedCharges,
			COUNT(TotaAmountBilled) AS NoBilled,
			SUM(TotaAmountBilled) AS TotaAmountBilled,
			SUM(TotaAmountCollected) AS TotaAmountCollected,
			--NoOfStubs,
			CustomerStatus
			--WeightedAverage		
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff
	ORDER BY Tariff
	
	DECLARE @tblCustomerStatisticsList AS TABLE
	(
		ID INT  IDENTITY(1,1),
		Tariff VARCHAR(200 ),
		KWHSold VARCHAR(500),
		KVASold VARCHAR(500),
		FixedCharges DECIMAL(18,2),
		NoBilled INT,
		TotaAmountBilled  DECIMAL(18,2),
		TotaAmountCollected  DECIMAL(18,2),
		NoOfStubs VARCHAR(500),
		TotalPopulation INT,
		WeightedAverage VARCHAR(500)
	)
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage)
	SELECT 	Tariff,
		KWHSold,
		KVASold,
		FixedCharges,
		NoBilled,
		TotaAmountBilled,
		TotaAmountCollected,
		NoOfStubs,
		TotalPopulation,
		WeightedAverage
	FROM #tmpCustomerStatisticsList
	
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage)
	SELECT 'Total' AS Tariff,
		'' AS KWHSold,
		'' AS KVASold,
		SUM(FixedCharges) AS FixedCharges,
		SUM(NoBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(TotalPopulation) AS TotalPopulation,
		'' AS WeightedAverage
	FROM #tmpCustomerStatisticsList
		
	SELECT ID AS [S/No],Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage 
	FROM @tblCustomerStatisticsList
	
	DROP TABLE #tmpCustomerStatistics
	DROP TABLE #tmpCustomerStatisticsList
	
END
GO
-------------------------------------------------------------------------------------------------------------------------
-- =============================================    
-- Author:  <Author,,Rajaiah>    
-- Create date: <Create Date,,03/14/2015>    
-- Description: <Description,,Billing status for customer service center>    
-- =============================================    
-- EXEC USP_RptGetBillingStatusForCustomerServiceCenter_Rajaiah    
CREATE PROCEDURE USP_RptGetBillingStatusForCustomerServiceCenter   
 -- Add the parameters for the stored procedure here   
 (  
 @XmlDoc XML=null    
 )   
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 DECLARE @Month INT,  
   @Year INT,  
   @BU_ID VARCHAR(100)    
 --SET @Month=1    
 --SET @Year=2015    
 --SET @BU_ID='BEDC_BU_0024'    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
  ,@Year = C.value('(Year)[1]','INT')    
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('NewReportsBe') AS T(C)    
    
    -- Insert statements for procedure here    
SELECT CT.CustomerType,    
   CD.GlobalAccountNumber,    
   CBills.Usage,    
   ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,    
   ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,    
   Cd.ServiceCenterName AS ServiceCenter,    
   CD.ActiveStatus AS CustomerStatus,    
   RC.ReadCode,    
   ISNULL(CBills.PaidAmount,0) AS Payment,    
   ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,    
   ISNULL(CD.OutStandingAmount,0) AS ClossingBalance    
   INTO #tmpCustomerServices    
 FROM tbl_customerbills CBills(NOLOCK)      
 INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON     
   CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID    
   AND CD.GlobalAccountNumber=CBills.AccountNo    
 INNER JOIN Tbl_MCustomerTypes CT(NOLOCK) ON CT.CustomerTypeId=CD.CustomerTypeId    
 INNER JOIN Tbl_MReadCodes RC(NOLOCK) ON RC.ReadCodeId=CBills.ReadCodeId     
    
 --SELECT * FROM #tmpCustomerServices    
    
 SELECT --ROW_NUMBER() OVER(ORDER BY CustomerType,ServiceCenter ASC) AS ID,    
  CustomerType,    
  ServiceCenter,    
  SUM(ISNULL(Active,0) ) AS Active,    
  SUM(ISNULL(InActive,0)) AS InActive,    
  --SUM (ISNULL(Hold,0) ) AS Hold,    
  SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,    
  SUM(EnergyBilled) AS EnergyBilled ,    
  SUM(RevenueBilled) As RevenueBilled,    
  SUM(ISNULL(Minimum,0) ) AS [MIN FC],    
  SUM(ISNULL([Read],0)) AS [Read],    
  SUM(ISNULL(Estimate,0) ) AS EST,    
  SUM(ISNULL(Direct,0)) AS Direct,    
  SUM(Payment) AS Payment,    
  SUM(OpeningBalance) AS OpeningBalance,    
  SUM(ClossingBalance) AS ClossingBalance    
  INTO #tmpCustomerServicesInfo    
 FROM    
 (    
  SELECT CustomerType,    
   ServiceCenter,     
   COUNT(CustomerStatus) AS TotalCustomerTariff,      
   CustomerStatus,    
   SUM(EnergyBilled) AS EnergyBilled,     
   SUM(RevenueBilled) AS RevenueBilled,    
   COUNT(ReadCode) AS ReadCodeType,    
   ReadCode,    
   SUM(Payment) AS Payment,    
   SUM(OpeningBalance) AS OpeningBalance,    
   SUM(ClossingBalance) AS ClossingBalance    
  FROM #tmpCustomerServices    
  GROUP BY CustomerStatus,ServiceCenter,CustomerType,ReadCode    
 ) ListOfCustomers    
 PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)    
 ) AS  ListOfCustomersT    
 PIVOT (SUM(ReadCodeType) FOR ReadCode IN(Direct,[Read],Estimate,Minimum)) AS  ListOfReadCodeType    
 GROUP BY ServiceCenter,CustomerType    
 ORDER BY CustomerType,ServiceCenter    
    
 --SELECT * FROM #tmpCustomerServicesInfo    
    
    
 SELECT ROW_NUMBER() OVER(ORDER BY CustomerType ASC) AS ID,    
  CustomerType,    
  'SUB Total' AS ServiceCenter,    
  SUM(ISNULL(Active,0) ) AS Active,    
  SUM(ISNULL(InActive,0)) AS InActive,    
  --SUM (ISNULL(Hold,0) ) AS Hold,    
  SUM(ISNULL(TotalPopulation,0)) as TotalPopulation,    
  SUM(EnergyBilled) AS EnergyBilled ,    
  SUM(RevenueBilled) As RevenueBilled,    
  SUM(ISNULL([MIN FC],0) ) AS [MIN FC],    
  SUM(ISNULL([Read],0)) AS [Read],    
 SUM(ISNULL(EST,0) ) AS EST,    
  SUM(ISNULL(Direct,0)) AS Direct,    
  SUM(Payment) AS Payment,    
  SUM(OpeningBalance) AS OpeningBalance,    
  SUM(ClossingBalance) AS ClossingBalance    
  INTO #tmpCustomerServicesInfoSubTotal    
 FROM #tmpCustomerServicesInfo    
 GROUP BY CustomerType    
    
 --==========================================================================================================    
 DECLARE @tblCustomerServices AS TABLE    
 (    
  ID INT IDENTITY(1,1),    
  CustomerType VARCHAR(100),    
  ServiceCenter VARCHAR(300),    
  Active INT,    
  InActive INT,    
  --Hold INT,    
  TotalPopulation INT,    
  EnergyBilled DECIMAL(18,2),    
  RevenueBilled DECIMAL(18,2),    
  [MIN FC] INT,    
  [Read] INT,    
  EST INT,     
  Direct INT,    
  Payment DECIMAL(18,2),    
  OpeningBalance DECIMAL(18,2),    
  ClossingBalance DECIMAL(18,2)    
 )    
 DECLARE @index INT, @rwCount INT,@CustomerType VARCHAR(100)     
 SET @index=0    
 SELECT @rwCount=COUNT(ID) FROM #tmpCustomerServicesInfoSubTotal    
    
 -- Loop for caluclating sub total    
 WHILE @index<@rwCount    
 BEGIN    
  SET @index=@index+1    
      
  SELECT @CustomerType=CustomerType FROM #tmpCustomerServicesInfoSubTotal WHERE ID=@index    
      
  INSERT INTO @tblCustomerServices(CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,Direct,Payment,    
  OpeningBalance,ClossingBalance)    
  SELECT CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,    
  Direct,Payment,OpeningBalance,ClossingBalance    
  FROM #tmpCustomerServicesInfo WHERE CustomerType=@CustomerType    
      
  INSERT INTO @tblCustomerServices(CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,Direct,Payment,    
  OpeningBalance,ClossingBalance)    
  SELECT CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,    
  Direct,Payment,OpeningBalance,ClossingBalance    
  FROM #tmpCustomerServicesInfoSubTotal WHERE ID=@index    
    
 END    
     
 INSERT INTO @tblCustomerServices(CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
 TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,Direct,Payment,    
 OpeningBalance,ClossingBalance)    
 SELECT '' AS CustomerType,    
  'GRAND Total' AS ServiceCenter,    
  SUM(ISNULL(Active,0) ) AS Active,    
  SUM(ISNULL(InActive,0)) AS InActive,    
  --SUM (ISNULL(Hold,0) ) AS Hold,    
  SUM(ISNULL(TotalPopulation,0)) as TotalPopulation,    
  SUM(EnergyBilled) AS EnergyBilled ,    
  SUM(RevenueBilled) As RevenueBilled,    
  SUM(ISNULL([MIN FC],0) ) AS [MIN FC],    
  SUM(ISNULL([Read],0)) AS [Read],    
  SUM(ISNULL(EST,0) ) AS EST,    
  SUM(ISNULL(Direct,0)) AS Direct,    
  SUM(Payment) AS Payment,    
  SUM(OpeningBalance) AS OpeningBalance,    
  SUM(ClossingBalance) AS ClossingBalance    
 FROM #tmpCustomerServicesInfoSubTotal     
     
    
 SELECT ID,CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,    
  Direct,Payment,OpeningBalance,ClossingBalance     
 FROM @tblCustomerServices    
 ORDER BY ID    
 --==========================================================================================================    
    
 DROP TABLE #tmpCustomerServices    
 DROP TABLE #tmpCustomerServicesInfo    
 DROP TABLE #tmpCustomerServicesInfoSubTotal    
    
     
END 
-------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersBillingStatisticsInfo]    Script Date: 03/17/2015 06:55:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
-- EXEC USP_RptGetCustomersBillingStatisticsInfo_Rajaiah
CREATE PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo] 
	-- Add the parameters for the stored procedure here
	(
	@XmlDoc XML
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,  
   @Year INT,  
   @BU_ID VARCHAR(100)    
 --SET @Month=1    
 --SET @Year=2015    
 --SET @BU_ID='BEDC_BU_0024'    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
  ,@Year = C.value('(Year)[1]','INT')    
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('NewReportsBe') AS T(C)    
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CD.ClassName AS Tariff,
			--'' AS KWHSold,
			ISNULL(CBills.NetFixedCharges,0) AS FixedCharges,
			-- No Billed 
			ISNULL(CBills.TotalBillAmount,0) AS TotaAmountBilled,			
			ISNULL(CBills.PaidAmount,0) AS TotaAmountCollected,
			--'' AS NoOfStubs,
			-- Total population
			--'' AS WeightedAverage,
			CD.ActiveStatus AS CustomerStatus
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
	CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
	AND CD.GlobalAccountNumber=CBills.AccountNo

	SELECT Tariff,
		'' AS KWHSold,
		SUM(FixedCharges) AS FixedCharges,
		COUNT(TotaAmountBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		'' AS WeightedAverage
		INTO #tmpCustomerStatisticsList
	FROM
	(
		SELECT	 Tariff,
			--KWHSold,
			COUNT(CustomerStatus) AS TotalCustomerTariff,		
			SUM(FixedCharges) AS FixedCharges,
			COUNT(TotaAmountBilled) AS NoBilled,
			SUM(TotaAmountBilled) AS TotaAmountBilled,
			SUM(TotaAmountCollected) AS TotaAmountCollected,
			--NoOfStubs,
			CustomerStatus
			--WeightedAverage		
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff
	ORDER BY Tariff
	
	DECLARE @tblCustomerStatisticsList AS TABLE
	(
		ID INT  IDENTITY(1,1),
		Tariff VARCHAR(200 ),
		KWHSold VARCHAR(500),
		FixedCharges DECIMAL(18,2),
		NoBilled INT,
		TotaAmountBilled  DECIMAL(18,2),
		TotaAmountCollected  DECIMAL(18,2),
		NoOfStubs VARCHAR(500),
		TotalPopulation INT,
		WeightedAverage VARCHAR(500)
	)
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage)
	SELECT 	Tariff,
		KWHSold,
		FixedCharges,
		NoBilled,
		TotaAmountBilled,
		TotaAmountCollected,
		NoOfStubs,
		TotalPopulation,
		WeightedAverage
	FROM #tmpCustomerStatisticsList
	
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage)
	SELECT 'Total' AS Tariff,
		'' AS KWHSold,
		SUM(FixedCharges) AS FixedCharges,
		SUM(NoBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(TotalPopulation) AS TotalPopulation,
		'' AS WeightedAverage
	FROM #tmpCustomerStatisticsList
		
	SELECT ID AS [S/No],Tariff,KWHSold,	FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage 
	FROM @tblCustomerStatisticsList
	
	DROP TABLE #tmpCustomerStatistics
	DROP TABLE #tmpCustomerStatisticsList
	
END
GO
-------------------------------------------------------------------------------------------------------------------------