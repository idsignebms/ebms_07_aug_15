GO
-- =============================================    
-- Author:    <Author>
-- Create date:   <Date>
  
-- Modified By: Faiz-ID103  
-- Modified Date: 16-Mar-2015  
-- Description: <To get master data in customer bulk upload validation>    
-- =============================================    
    
ALTER PROCEDURE [dbo].[USP_GetCustDetailBlkUpld]  
(
@XmlDoc XML    
)   
AS      
BEGIN     

DECLARE @BUID VARCHAR(20) 
   
    SELECT    
   @BUID = C.value('(BUID)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('CustomerBulkBe') as T(C)    
 
 --SELECT OldAccountNo,MeterNo FROM Tbl_CustomerDetails    
 SELECT OldAccountNo FROM CUSTOMERS.Tbl_CustomerSDetail where OldAccountNo is not null -- Faiz-ID103  
 SELECT PhaseId,Phase FROM Tbl_MPhases WHERE ActiveStatus=1    
 SELECT MeterStatusId,MeterStatus FROM Tbl_MMeterStatus WHERE ActiveStatusId=1    
 SELECT IdentityId,[Type] from Tbl_MIdentityTypes WHERE ActiveStatusId =1    
 SELECT AccountTypeId,AccountCode FROM Tbl_MAccountTypes WHERE ActiveStatusId=1    
 --SELECT TC.ClassID AS TariffClassId,TC.ClassName as Tariff,EC.Amount,AC.Amount FROM Tbl_MTariffClasses AS TC    
 --     INNER JOIN Tbl_LEnergyClassCharges AS EC ON TC.ClassID=EC.ClassID    
 --     INNER JOIN Tbl_LAdditionalClassCharges AC ON TC.ClassID=AC.ClassID    
 --     WHERE TC.IsActiveClass=1 AND EC.IsActive=1 AND AC.IsActive=1  
 
 SELECT TC.ClassID AS TariffClassId,TC.ClassName as Tariff FROM Tbl_MTariffClasses AS TC    
 WHERE TC.IsActiveClass=1 and RefClassID is not null
        
 SELECT MeterTypeId,MeterType from Tbl_MMeterTypes WHERE ActiveStatus=1          
 SELECT CustomerTypeId,CustomerType FROM Tbl_MCustomerTypes WHERE ActiveStatusId=1-- Faiz-ID103  
 SELECT MeterNumber from CUSTOMERS.Tbl_CustomerProceduralDetails where ActiveStatusId=1 and MeterNumber is not null-- Faiz-ID103  
 SELECT ReadCodeId,ReadCode as ReadType from Tbl_MReadCodes where ReadCodeId in (1,2)  
 select AreaCode from MASTERS.Tbl_MAreaDetails  
 
SELECT BookNo AS BookCodeId,ID AS BookCode
FROM Tbl_BookNumbers AS BN    
 JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId    
    JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId    
    JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID    
    JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID    
    JOIN Tbl_States ST ON ST.StateCode=BU.StateCode    
   WHERE (BU.BU_ID=@BUID OR @BUID='')  
    
 --FOR XML PATH('Consumer'),ROOT('ConsumerBeInfoByXml')    
End  
 GO
/****** Object:  View [dbo].[UDV_CustomerDescription]    Script Date: 03/19/2015 12:33:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[UDV_CustomerDescription]
AS
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs, 
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo, 
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId, 
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID, 
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName, 
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading, 
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading, 
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID
                      ,Service_HouseNo,Service_StreetName,Service_Landmark,Service_City,Service_ZipCode
                      ,Postal_HouseNo,Postal_StreetName,Postal_Landmark,Postal_City,Postal_ZipCode,BN.SortOrder AS BookSortOrder,TC.RefClassID
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
GO



 GO
	--- We Need to Give DropDowns For the customers & Front end has to check ----SET ANSI_NULLS ON
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya
-- 
-- =============================================
ALTER PROCEDURE [USP_GetAccountWithoutMeter_New](@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(ServiceCenterId)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('ConsumerBe') AS T(C)

			Declare @Count INT
				
				SELECT
						   IDENTITY (int, 1,1) As   RowNumber						   
						  ,'' TotlaRecordes
						  ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) as Name
						  ,CD.GlobalAccountNumber
						  ,CD.AccountNo						  
						  ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
																,CD.Service_Landmark
																,CD.Service_City,'',
																CD.Service_ZipCode) AS [ServiceAddress]
						  ,CD.ClassName
						  ,CD.OldAccountNo
						  ,CD.SortOrder AS CustomerSortOrder
						  ,CD.BookCode AS BookNo
						  ,CD.BusinessUnitName
						  ,CD.ServiceUnitName
						  ,CD.ServiceCenterName	
						  INTO #CustomersList  
						  FROM [UDV_CustomerDescription](NOLOCK) CD
						  WHERE CD.ReadCodeId=1
						  AND (CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU,',')) OR @BU='')
						  AND (CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU,',')) OR @SU='')
						  AND (CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@SC,',')) OR @SC='')
						  AND (CD.TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,',')) OR @Tariff='')
						  
			SET @Count=(select count(0) from #CustomersList)				

			 SELECT  *
					,@Count As TotalRecords 
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------
GO
 
/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithoutCycle]    Script Date: 03/18/2015 18:13:26 ******/
/** USP_GetAccountWithoutCycle Old Proc Name**/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  M.RamaDevi  
-- Create date: 12-04-2014
-- Author:  V.Bhimaraju
-- Create date: 07-08-2014
-- Modified By: T.Karthik
-- Modified Date: 29-10-2014
-- Description: To get Accounts without cycle  Customers
-- =============================================  
CREATE PROCEDURE [USP_GetAccountWithoutCycle_New]
(
	@xmlDoc xml
)
AS  
BEGIN  
		DECLARE    @BU_ID VARCHAR(MAX)   
				  ,@SU_ID VARCHAR(MAX)  
				  ,@ServiceCenterId VARCHAR(MAX)  
				  ,@Tariff VARCHAR(MAX)  
				  ,@PageNo INT  
				  ,@PageSize INT
				    
		SELECT	 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
				,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
				,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
				,@Tariff=C.value('(Tariff)[1]','VARCHAR(MAX)')  
				,@PageNo = C.value('(PageNo)[1]','INT')  
				,@PageSize = C.value('(PageSize)[1]','INT')   
		FROM @xmlDoc.nodes('ConsumerBe') AS T(C)  
  

				 		
						 		  
			SELECT	 GlobalAccountNumber,OldAccountNo,
					 (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) as FullName,
					 dbo.fn_GetCustomerServiceAddress_New(Cd.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode)   as [ServiceAdress]
					 ,CD.ClassName
					 ,CD.BookNo
					 ,CD.ConnectionDate
					 ,IDENTITY (int, 1,1) As   RowNumber
					 ,CD.BusinessUnitName
					 ,CD.ServiceUnitName
					 ,CD.ServiceCenterName
					 ,CD.SortOrder AS CustomerSortOrder
			INTO #CustomersList
			FROM
			[UDV_CustomerDescription] CD
			WHERE	CycleId IS NULL
			AND CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID,','))
			AND CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU_ID,','))
			AND CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@ServiceCenterId,','))
			AND CD.TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,','))


			Declare @TotlaRecords INT

			SET	   @TotlaRecords = (select COUNT(0) from  #CustomersList)

			Select	RowNumber
					,GlobalAccountNumber
					,ISNULL(OldAccountNo,'--') AS OldAccountNo
					,FullName   as Name
					,ServiceAdress as ServiceAddress
					,ClassName
					,BookNo
					,BusinessUnitName
					,ServiceUnitName
					,ServiceCenterName
					,CustomerSortOrder
					,ISNULL(CONVERT(VARCHAR(20),ConnectionDate,106),'--') AS ConnectionDate
					,@TotlaRecords As TotalRecords	 from #CustomersList
			  Where  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 

			--Select * from #CustomersList
			DROP TABLE	#CustomersList
		 
				   
END  
GO
 ----------------------------------------------------------------------------------------------------------------------------------  
  GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Satya  
--   
-- =============================================  
CREATE PROCEDURE [USP_GetTariffBUPivotReport_New]  
AS    
BEGIN   
--select Distinct TariffId , ClassName from   UDV_CustomerDescription   
--order by  TariffId,ClassName  
-------------------------------List OF Tariff In the DataBase ------------------------------  
--2  R1  
--3  R2  
--4  R3  
--5  R4  
--7  C1  
--8  C2  
--9  C3  
--11 D1  
--12 D2  
--13 D3  
--15 A1  
--16 A2  
--17 A4  
--19 S1  
-----------------------------------------------------------------------------------------  
  
--GROUP BY clause.  
  
Select BU_ID,BusinessUnitName  
 ,REPLACE(CONVERT(VARCHAR, (CAST(R1 AS MONEY)), 1), '.00', '') AS R1
 ,REPLACE(CONVERT(VARCHAR, (CAST(R2 AS MONEY)), 1), '.00', '') AS R2
 ,REPLACE(CONVERT(VARCHAR, (CAST(R3 AS MONEY)), 1), '.00', '') AS R3
 ,REPLACE(CONVERT(VARCHAR, (CAST(R4 AS MONEY)), 1), '.00', '') AS R4
 ,REPLACE(CONVERT(VARCHAR, (CAST(C1 AS MONEY)), 1), '.00', '') AS C1
 ,REPLACE(CONVERT(VARCHAR, (CAST(C2 AS MONEY)), 1), '.00', '') AS C2
 ,REPLACE(CONVERT(VARCHAR, (CAST(C3 AS MONEY)), 1), '.00', '') AS C3
 ,REPLACE(CONVERT(VARCHAR, (CAST(D1 AS MONEY)), 1), '.00', '') AS D1
 ,REPLACE(CONVERT(VARCHAR, (CAST(D2 AS MONEY)), 1), '.00', '') AS D2
 ,REPLACE(CONVERT(VARCHAR, (CAST(D3 AS MONEY)), 1), '.00', '') AS D3
 ,REPLACE(CONVERT(VARCHAR, (CAST(D4 AS MONEY)), 1), '.00', '') AS D4
 ,REPLACE(CONVERT(VARCHAR, (CAST(A1 AS MONEY)), 1), '.00', '') AS A1
 ,REPLACE(CONVERT(VARCHAR, (CAST(A2 AS MONEY)), 1), '.00', '') AS A2
 ,REPLACE(CONVERT(VARCHAR, (CAST(A3 AS MONEY)), 1), '.00', '') AS A3
 ,REPLACE(CONVERT(VARCHAR, (CAST(A4 AS MONEY)), 1), '.00', '') AS A4
 ,REPLACE(CONVERT(VARCHAR, (CAST(S1 AS MONEY)), 1), '.00', '') AS S1
 ,REPLACE(CONVERT(VARCHAR, (CAST(SUM(ISNULL(R1,0)+ISNULL(R2,0)+ISNULL(R3,0)+ISNULL(R4,0)+ISNULL(C1,0)+ISNULL(C2,0)+ISNULL(C3,0)+  
 ISNULL(D1,0)+ISNULL(D2,0)+ISNULL(D3,0)+ISNULL(D4,0)+ISNULL(A1,0)+ISNULL(A2,0)+ISNULL(A3,0)+ISNULL(A4,0)+ISNULL(S1,0)) AS MONEY)), 1), '.00', '') AS Total 
 FROM (  
Select BU.BU_ID, BU.BusinessUnitName,COUNT(0) As TotalCustomers,CD.ClassName   
 from UDV_CustomerDescription  CD  
inner join   
Tbl_BussinessUnits  BU  
On BU.BU_ID=CD.BU_ID  
Group By BU.BU_ID, BU.BusinessUnitName,CD.ClassName,CD.TariffId  
   
) PivotTable  
PIVOT  
(                      
 SUM(TotalCustomers) FOR PivotTable.ClassName IN (R1,R2,R3,R4,C1,C2,C3,D1,D2,D3,D4,A1,A2,A3,A4,S1)  
)      
 PivotTable  
 GROUP BY BU_ID,BusinessUnitName  
 ,R1,R2,R3,R4,C1,C2,C3,D1,D2,D3,D4,A1,A2,A3,A4,S1  
   
END  
GO
GO
-- =============================================  
-- Author:  <Author,,Rajaiah>  
-- Create date: <Create Date,,03/14/2015>  
-- Description: <Description,,Get customer statistics and tariff Info>  
-- =============================================  
-- EXEC USP_RptGetCustomersStatisticsInfo_Rajaiah  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo]   
 -- Add the parameters for the stored procedure here  
(    
@XmlDoc Xml=null    
)   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)  
 --SET @Month=1  
 --SET @Year=2015  
 --SET @BU_ID='BEDC_BU_0024'  
     
 SELECT   @Month = C.value('(Month)[1]','INT')    
   ,@Year = C.value('(Year)[1]','INT')    
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
  FROM @XmlDoc.nodes('NewReportsBe') AS T(C)    
    -- Insert statements for procedure here  
 SELECT CD.GlobalAccountNumber,  
   CD.ClassName AS Tariff,  
   --'' AS KWHSold,  
   --'' AS KVASold,  
   ISNULL(CBills.NetFixedCharges,0) AS FixedCharges,  
   -- No Billed   
   ISNULL(CBills.TotalBillAmount,0) AS TotaAmountBilled,     
   ISNULL(CBills.PaidAmount,0) AS TotaAmountCollected,  
   --'' AS NoOfStubs,  
   -- Total population  
   --'' AS WeightedAverage,  
   CD.ActiveStatus AS CustomerStatus  
   INTO #tmpCustomerStatistics  
 FROM tbl_customerbills CBills(NOLOCK)    
 INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON   
 CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID  
 AND CD.GlobalAccountNumber=CBills.AccountNo  
  
 SELECT Tariff,  
  '' AS KWHSold,  
  '' AS KVASold,  
  SUM(FixedCharges) AS FixedCharges,  
  COUNT(TotaAmountBilled) AS NoBilled,  
  SUM(TotaAmountBilled) AS TotaAmountBilled,  
  SUM(TotaAmountCollected) AS TotaAmountCollected,  
  '' AS NoOfStubs,  
  SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,  
  '' AS WeightedAverage  
  INTO #tmpCustomerStatisticsList  
 FROM  
 (  
  SELECT  Tariff,  
   --KWHSold,  
   --KVASold,  
   COUNT(CustomerStatus) AS TotalCustomerTariff,    
   SUM(FixedCharges) AS FixedCharges,  
   COUNT(TotaAmountBilled) AS NoBilled,  
   SUM(TotaAmountBilled) AS TotaAmountBilled,  
   SUM(TotaAmountCollected) AS TotaAmountCollected,  
   --NoOfStubs,  
   CustomerStatus  
   --WeightedAverage    
  FROM #tmpCustomerStatistics  
  GROUP BY CustomerStatus,Tariff  
 ) ListOfCustomers  
 PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)  
 ) AS  ListOfCustomersT  
 GROUP BY Tariff  
 ORDER BY Tariff  
   
 DECLARE @tblCustomerStatisticsList AS TABLE  
 (  
  ID INT  IDENTITY(1,1),  
  Tariff VARCHAR(200 ),  
  KWHSold VARCHAR(500),  
  KVASold VARCHAR(500),  
  FixedCharges DECIMAL(18,2),  
  NoBilled INT,  
  TotaAmountBilled  DECIMAL(18,2),  
  TotaAmountCollected  DECIMAL(18,2),  
  NoOfStubs VARCHAR(500),  
  TotalPopulation INT,  
  WeightedAverage VARCHAR(500)  
 )  
 INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold, KVASold,FixedCharges,NoBilled, TotaAmountBilled,  
  TotaAmountCollected, NoOfStubs,TotalPopulation,WeightedAverage)  
 SELECT  Tariff,  
  KWHSold,  
  KVASold,  
  FixedCharges,  
  NoBilled,  
  TotaAmountBilled,  
  TotaAmountCollected,  
  NoOfStubs,  
  TotalPopulation,  
  WeightedAverage  
 FROM #tmpCustomerStatisticsList  
   
 INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold, KVASold,FixedCharges,NoBilled, TotaAmountBilled,  
  TotaAmountCollected, NoOfStubs,TotalPopulation,WeightedAverage)  
 SELECT 'Total' AS Tariff,  
  '' AS KWHSold,  
  '' AS KVASold,  
  SUM(FixedCharges) AS FixedCharges,  
  SUM(NoBilled) AS NoBilled,  
  SUM(TotaAmountBilled) AS TotaAmountBilled,  
  SUM(TotaAmountCollected) AS TotaAmountCollected,  
  '' AS NoOfStubs,  
  SUM(TotalPopulation) AS TotalPopulation,  
  '' AS WeightedAverage  
 FROM #tmpCustomerStatisticsList  
    
 --SELECT ID AS [SNo],Tariff,KWHSold, KVASold,FixedCharges,NoBilled, TotaAmountBilled As TotalAmountBilled,  
 -- TotaAmountCollected, NoOfStubs,TotalPopulation,WeightedAverage   
 SELECT	 ID AS SNo,Tariff
			,KWHSold
			,KVASold
			,FixedCharges
			,NoBilled
			,TotaAmountBilled AS TotalAmountBilled
			,TotaAmountCollected AS TotalAmountCollected
			,NoOfStubs
			,TotalPopulation
			,WeightedAverage 
 FROM @tblCustomerStatisticsList  
   
 DROP TABLE #tmpCustomerStatistics  
 DROP TABLE #tmpCustomerStatisticsList  
   
END  
GO
---------------------------------------------------------------------------------------------------------

GO
-- =============================================  
-- Author:  <Author,,Rajaiah>  
-- Create date: <Create Date,,03/14/2015>  
-- Description: <Description,,Get customer statistics and tariff Info>  
-- =============================================  
-- EXEC USP_RptGetCustomersBillingStatisticsInfo_Rajaiah  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo]   
 -- Add the parameters for the stored procedure here  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 DECLARE @Month INT,    
   @Year INT,    
   @BU_ID VARCHAR(100)      
 --SET @Month=1      
 --SET @Year=2015      
 --SET @BU_ID='BEDC_BU_0024'      
    
SELECT   @Month = C.value('(Month)[1]','INT')      
  ,@Year = C.value('(Year)[1]','INT')      
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
FROM @XmlDoc.nodes('NewReportsBe') AS T(C)      
    -- Insert statements for procedure here  
 SELECT CD.GlobalAccountNumber,  
   CD.ClassName AS Tariff,  
   --'' AS KWHSold,  
   ISNULL(CBills.NetFixedCharges,0) AS FixedCharges,  
   -- No Billed   
   ISNULL(CBills.TotalBillAmount,0) AS TotaAmountBilled,     
   ISNULL(CBills.PaidAmount,0) AS TotaAmountCollected,  
   --'' AS NoOfStubs,  
   -- Total population  
   --'' AS WeightedAverage,  
   CD.ActiveStatus AS CustomerStatus  
   INTO #tmpCustomerStatistics  
 FROM tbl_customerbills CBills(NOLOCK)    
 INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON   
 CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID  
 AND CD.GlobalAccountNumber=CBills.AccountNo  
  
 SELECT Tariff,  
  '' AS KWHSold,  
  SUM(FixedCharges) AS FixedCharges,  
  COUNT(TotaAmountBilled) AS NoBilled,  
  SUM(TotaAmountBilled) AS TotaAmountBilled,  
  SUM(TotaAmountCollected) AS TotaAmountCollected,  
  '' AS NoOfStubs,  
  SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,  
  '' AS WeightedAverage  
  INTO #tmpCustomerStatisticsList  
 FROM  
 (  
  SELECT  Tariff,  
   --KWHSold,  
   COUNT(CustomerStatus) AS TotalCustomerTariff,    
   SUM(FixedCharges) AS FixedCharges,  
   COUNT(TotaAmountBilled) AS NoBilled,  
   SUM(TotaAmountBilled) AS TotaAmountBilled,  
   SUM(TotaAmountCollected) AS TotaAmountCollected,  
   --NoOfStubs,  
   CustomerStatus  
   --WeightedAverage    
  FROM #tmpCustomerStatistics  
  GROUP BY CustomerStatus,Tariff  
 ) ListOfCustomers  
 PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)  
 ) AS  ListOfCustomersT  
 GROUP BY Tariff  
 ORDER BY Tariff  
   
 DECLARE @tblCustomerStatisticsList AS TABLE  
 (  
  ID INT  IDENTITY(1,1),  
  Tariff VARCHAR(200 ),  
  KWHSold VARCHAR(500),  
  FixedCharges DECIMAL(18,2),  
  NoBilled INT,  
  TotaAmountBilled  DECIMAL(18,2),  
  TotaAmountCollected  DECIMAL(18,2),  
  NoOfStubs VARCHAR(500),  
  TotalPopulation INT,  
  WeightedAverage VARCHAR(500)  
 )  
 INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold, FixedCharges,NoBilled, TotaAmountBilled,  
  TotaAmountCollected, NoOfStubs,TotalPopulation,WeightedAverage)  
 SELECT  Tariff,  
  KWHSold,  
  FixedCharges,  
  NoBilled,  
  TotaAmountBilled,  
  TotaAmountCollected,  
  NoOfStubs,  
  TotalPopulation,  
  WeightedAverage  
 FROM #tmpCustomerStatisticsList  
   
 INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold, FixedCharges,NoBilled, TotaAmountBilled,  
  TotaAmountCollected, NoOfStubs,TotalPopulation,WeightedAverage)  
 SELECT 'Total' AS Tariff,  
  '' AS KWHSold,  
  SUM(FixedCharges) AS FixedCharges,  
  SUM(NoBilled) AS NoBilled,  
  SUM(TotaAmountBilled) AS TotaAmountBilled,  
  SUM(TotaAmountCollected) AS TotaAmountCollected,  
  '' AS NoOfStubs,  
  SUM(TotalPopulation) AS TotalPopulation,  
  '' AS WeightedAverage  
 FROM #tmpCustomerStatisticsList  
    
 --SELECT ID AS [S/No],Tariff,KWHSold, FixedCharges,NoBilled, TotaAmountBilled,  
 -- TotaAmountCollected, NoOfStubs,TotalPopulation,WeightedAverage   
 SELECT	 ID AS SNo,Tariff
			,KWHSold
			,FixedCharges
			,NoBilled
			,TotaAmountBilled AS TotalAmountBilled
			,TotaAmountCollected AS TotalAmountCollected
			,NoOfStubs
			,TotalPopulation
			,WeightedAverage 
 FROM @tblCustomerStatisticsList  
   
 DROP TABLE #tmpCustomerStatistics  
 DROP TABLE #tmpCustomerStatisticsList  
   
END  

GO
---------------------------------------------------------------------------------------------------------
