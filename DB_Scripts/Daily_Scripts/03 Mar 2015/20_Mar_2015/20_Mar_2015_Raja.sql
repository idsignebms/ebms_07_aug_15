GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Download Billing File','../Billing/DownloadBillingFile.aspx',4,20) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
GO
CREATE TABLE Tbl_CustomerTypeChangeLogs
(
	 CustomerTypeChangeLogId INT IDENTITY(1,1) PRIMARY KEY
	,GlobalAccountNumber VARCHAR(50)
	,OldCustomerTypeId INT
	,NewCustomerTypeId INT
	,Remarks VARCHAR(MAX)
	,ApprovalStatusId INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M.Padmini
-- Create date: 20-03-2015
-- Description:	The purpose of this procedure is to get limited CustomerType request logs
-- =============================================   
CREATE PROCEDURE [USP_GetCustomerTypeChangeWithLimit]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
	  
	  
	  SELECT(
			  SELECT  TOP 10 CC.GlobalAccountNumber AS AccountNo
					  ,CC.ApprovalStatusId
					  ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType
					  ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType
					  ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,CC.Remarks
					  ,CC.CreatedBy
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					  ,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_CustomerTypeChangeLogs  CC
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId			  
			  GROUP BY CC.GlobalAccountNumber
					  ,CC.ApprovalStatusId
					  ,OldCustomerTypeId
					  ,NewCustomerTypeId
					  ,CC.CreatedBy
					  ,CONVERT(VARCHAR(30),CC.CreatedDate,107)
					  ,ApproveSttus.ApprovalStatus
					  ,CC.Remarks
					  ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
				FOR XML PATH('AuditTrayList'),TYPE
				)
				FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		M.Padmini
-- Create date: 20-03-2015
-- Description:	The purpose of this procedure is to get limited CustomerType request logs
-- =============================================   
CREATE PROCEDURE [USP_GetCustomerTypeChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
	  
	  
	  SELECT(
			  SELECT  CC.GlobalAccountNumber AS AccountNo
					  ,CC.ApprovalStatusId
					  ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType
					  ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType
					  ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate 
					  ,ApproveSttus.ApprovalStatus
					  ,CC.Remarks
					  ,CC.CreatedBy
					  ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			  FROM Tbl_CustomerTypeChangeLogs  CC
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId			  
			FOR XML PATH('AuditTrayList'),TYPE
			)
			FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerType]    Script Date: 03/20/2015 14:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz-ID103      
-- Create date: 09-Mar-2015      
-- Description: The purpose of this procedure is to Update Customer Type 
-- Modified By - Padmini.M
-- Modified Date - 20-03-2015
-- =============================================  
ALTER PROCEDURE [USP_ChangeCustomerType]  
 (      
  @XmlDoc xml      
 )  
AS      
BEGIN  
       
 DECLARE    @GlobalAccountNumber VARCHAR(50)   
		   ,@CustomerTypeId INT 
		   ,@ModifiedBy VARCHAR(50)  
		   ,@Reason VARCHAR(MAX)  
		   ,@ApprovalStatusId INT  
    
 SELECT    @GlobalAccountNumber=C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		  ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
		  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')  
		  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
		  FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)  
  
		   
		   IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
	          WHERE GlobalAccountNumber=@GlobalAccountNumber AND ApprovalStatusId NOT IN(2,3))>0)
				BEGIN
					SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeCustomerTypeBE')
				END   
			ELSE
				BEGIN
						INSERT INTO Tbl_CustomerTypeChangeLogs( GlobalAccountNumber
																	,OldCustomerTypeId
																	,NewCustomerTypeId
																	,CreatedBy
																	,CreatedDate
																	,ApprovalStatusId
																	,Remarks)
						SELECT GlobalAccountNumber
						       ,CustomerTypeId
						       ,@CustomerTypeId
						       ,@ModifiedBy
						       ,dbo.fn_GetCurrentDateTime()
						       ,@ApprovalStatusId	
						       ,@Reason						   
						FROM CUSTOMERS.Tbl_CustomerProceduralDetails		
						WHERE GlobalAccountNumber=@GlobalAccountNumber		
				   
						IF(@ApprovalStatusId=2) -- If Approved
							BEGIN		
								
								   UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails
								   SET CustomerTypeId=@CustomerTypeId
								   WHERE GlobalAccountNumber=@GlobalAccountNumber
								   
								   SELECT 1 AS IsSuccess FOR XML PATH('ChangeCustomerTypeBE')	
							END
					    ELSE
							BEGIN
								SELECT 1 AS IsSuccess FOR XML PATH('ChangeCustomerTypeBE')
							END	
				END	
END
GO

--------------------------------------------------------------------------------------------------
-----------------------------Faiz-ID103---20-Mar-2015---------------------------------------------
--------------------------------------------------------------------------------------------------
  GO
-- =============================================      
-- Author:  <NEERAJ KANOJIYA>      
-- Create date: <19-March-2014>      
-- Description: <GET EBILL DAILY DETAILS>      
-- =============================================      
CREATE PROCEDURE  [dbo].[UPS_GetBillTextFiles]     
(    
@XmlDoc XML   
)     
AS      
BEGIN     
  
Declare @Month INT,    
  @Year INT  
    
SELECT    
   @Month = C.value('(Month)[1]','INT')    
   ,@Year = C.value('(Year)[1]','INT')    
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)     
     
  
 SELECT CY.ServiceCenterId ,  
   BS.BillingYear,  
   BillingMonth,  
   SCDetails.BusinessUnitName,  
   SCDetails.ServiceCenterName,    
   SCDetails.ServiceUnitName,  
   '~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,   
   --MAX(BS.BillingFile) As FilePath,    
   dbo.fn_GetMonthName(BillingMonth) as [MonthName]    
 FROM Tbl_BillingQueueSchedule BS      
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId      
 and BillGenarationStatusId=1  
 AND BS.BillingMonth=2--@Month   
 AND BS.BillingYear=2015--@Year    
 INNER JOIN    
 [UDV_ServiceCenterDetails] SCDetails    
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId    
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth     
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,    
 SCDetails.ServiceUnitName      
  
END  
GO
-----------------------------------------------------------------------------------------------------------
