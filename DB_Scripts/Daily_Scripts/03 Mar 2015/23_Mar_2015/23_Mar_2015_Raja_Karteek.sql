GO

CREATE TABLE Tbl_CustomerAddressChangeLog_New
(
   AddressChangeLogId INT IDENTITY(1,1) PRIMARY KEY
   ,GlobalAccountNumber VARCHAR(50)   
   ,OldPostal_HouseNo VARCHAR(100)
   ,OldPostal_StreetName VARCHAR(100)
   ,OldPostal_City VARCHAR(100)
   ,OldPostal_Landmark VARCHAR(100)
   ,OldPostal_AreaCode VARCHAR(100)
   ,OldPostal_ZipCode VARCHAR(100)
   ,NewPostal_HouseNo VARCHAR(100)
   ,NewPostal_StreetName VARCHAR(100)
   ,NewPostal_City VARCHAR(100)
   ,NewPostal_Landmark VARCHAR(100)
   ,NewPostal_AreaCode VARCHAR(100)
   ,NewPostal_ZipCode VARCHAR(100)
   ,OldService_HouseNo VARCHAR(100)
   ,OldService_StreetName VARCHAR(100)
   ,OldService_City VARCHAR(100)
   ,OldService_Landmark VARCHAR(100)
   ,OldService_AreaCode VARCHAR(100)
   ,OldService_ZipCode       VARCHAR(100)
   ,NewService_HouseNo VARCHAR(100)
   ,NewService_StreetName VARCHAR(100)
   ,NewService_City VARCHAR(100)
   ,NewService_Landmark VARCHAR(100)
   ,NewService_AreaCode VARCHAR(100)
   ,NewService_ZipCode VARCHAR(100)
   ,ApprovalStatusId INT
   ,Remarks VARCHAR(MAX)
   ,CreatedBy VARCHAR(50)
   ,CreatedDate DATETIME
   ,ModifiedBy VARCHAR(50)
   ,ModifiedDate DATETIME
   )
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get limited Customer Address change logs
-- =============================================
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogsWithLimit] 
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END
	

	  SELECT(
			  SELECT  TOP 10  CA.GlobalAccountNumber
					,OldPostal_HouseNo
					,OldPostal_StreetName 
					,OldPostal_City
					,OldPostal_Landmark
					,OldPostal_AreaCode
					,OldPostal_ZipCode
					,NewPostal_HouseNo
					,NewPostal_StreetName 
					,NewPostal_City
					,NewPostal_Landmark
					,NewPostal_AreaCode
					,NewPostal_ZipCode 
					,OldService_HouseNo
					,OldService_StreetName
					,OldService_City
					,OldService_Landmark
					,OldService_AreaCode
					,OldService_ZipCode 
					,NewService_HouseNo 
					,NewService_StreetName
					,NewService_City
					,NewService_Landmark
					,NewService_AreaCode
					,NewService_ZipCode
					,CA.Remarks
					,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate 
					,ApproveSttus.ApprovalStatus
					,CA.CreatedBy
					,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
					,COUNT(0) OVER() AS TotalChanges
			  FROM Tbl_CustomerAddressChangeLog_New CA						
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId			  
			  GROUP BY CA.GlobalAccountNumber
					,OldPostal_HouseNo
					,OldPostal_StreetName 
					,OldPostal_City
					,OldPostal_Landmark
					,OldPostal_AreaCode
					,OldPostal_ZipCode
					,NewPostal_HouseNo
					,NewPostal_StreetName 
					,NewPostal_City
					,NewPostal_Landmark
					,NewPostal_AreaCode
					,NewPostal_ZipCode 
					,OldService_HouseNo
					,OldService_StreetName
					,OldService_City
					,OldService_Landmark
					,OldService_AreaCode
					,OldService_ZipCode 
					,NewService_HouseNo 
					,NewService_StreetName
					,NewService_City
					,NewService_Landmark
					,NewService_AreaCode
					,NewService_ZipCode
					,CA.Remarks
					,CONVERT(VARCHAR(30),CA.CreatedDate,107)
					,ApprovalStatus
					,CA.CreatedBy
					,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName
					FOR XML PATH('AuditTrayAddressList'),TYPE
					)
					FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')
END
--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsWithLimit]    Script Date: 03/17/2015 21:11:33 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get limited Customer Address change logs
---- =============================================
--ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsWithLimit] 
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)

--		DECLARE @Accounts TABLE(Sno INT IDENTITY(1,1),AccountNo VARCHAR(30))
		
--		INSERT INTO @Accounts
--			SELECT CNR.AccountNo FROM (
--				SELECT TOP 5 CN.AccountNo FROM Tbl_CustomerAddressChangeLogs CN
--				JOIN [UDV_CustomerDescription] CD ON CN.AccountNo=CD.GlobalAccountNumber
--				AND (BU_ID=@BU_ID OR @BU_ID='')
--				AND (SU_ID=@SU_ID OR @SU_ID='')
--				AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--				AND (CycleId=@CycleId OR @CycleId='')
--				AND (TariffId=@TariffId OR @TariffId=0)
--				AND (CONVERT(DATE,CN.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--				AND (CONVERT(DATE,CN.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--				 ORDER BY CN.CreatedDate DESC
--				 ) CNR
--			 GROUP BY CNR.AccountNo
		 		 
--		CREATE TABLE #CustomerAddressLogs(AddressChangeLogId INT)

--		DECLARE @I INT=1,@Count INT=0,@TotalChanges INT=0
--		SELECT @Count=COUNT(0) FROM @Accounts
--		SELECT @TotalChanges=COUNT(0) FROM Tbl_CustomerAddressChangeLogs

--		WHILE(@I<=@Count)
--		BEGIN
--			INSERT INTO #CustomerAddressLogs
--			SELECT TOP 10 AddressChangeLogId FROM Tbl_CustomerAddressChangeLogs WHERE AccountNo=(SELECT AccountNo FROM @Accounts WHERE Sno=@I)
--			ORDER BY AddressChangeLogId DESC
--			SET @I=@I+1
--		END 
		
--		SELECT
--		(
--			SELECT AccountNo
--					,Remarks
--					,OldPostalLandMark
--					,OldPostalStreet
--					,OldPostalCity
--					,OldPostalHouseNo
--					,OldPostalZipCode
--					,OldServiceLandMark
--					,OldServiceStreet
--					,OldServiceCity
--					,OldServiceHouseNo
--					,OldServiceZipCode
--					,NewPostalLandMark
--					,NewPostalStreet
--					,NewPostalCity
--					,NewPostalHouseNo
--					,NewPostalZipCode
--					,NewServiceLandMark
--					,NewServiceStreet
--					,NewServiceCity
--					,NewServiceHouseNo
--					,NewServiceZipCode
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName(AccountNo) AS Name
--					,CONVERT(VARCHAR(30),CreatedDate,107) AS TransactionDate
--					,@TotalChanges AS TotalChanges
--					,OldPostalHouseNo +'<br/>'+OldPostalLandMark+'<br/>'+OldPostalStreet+
--						'<br/>'+OldPostalCity+'<br/>'+OldPostalZipCode AS OldAddress
--					,OldServiceHouseNo +'<br/>'+OldServiceLandMark+'<br/>'+OldServiceStreet+
--						'<br/>'+OldServiceCity+'<br/>'+OldServiceZipCode AS OldServiceAddress
--					,NewPostalHouseNo +'<br/>'+NewPostalLandMark+'<br/>'+NewPostalStreet+
--						'<br/>'+NewPostalCity+'<br/>'+NewPostalZipCode AS NewAddress
--					,NewServiceHouseNo +'<br/>'+NewServiceLandMark+'<br/>'+NewServiceStreet+
--						'<br/>'+NewServiceCity+'<br/>'+NewServiceZipCode AS NewServiceAddress
--			FROM Tbl_CustomerAddressChangeLogs CNL
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApproveStatusId
--			AND AddressChangeLogId IN(
--			SELECT AddressChangeLogId FROM #CustomerAddressLogs
--			)
--			ORDER BY AccountNo DESC,CreatedDate DESC
--			FOR XML PATH('AuditTrayList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get BookNo change logs based on search criteria
-- =============================================
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogs]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @BU_ID VARCHAR(MAX)=''
				,@SU_ID VARCHAR(MAX)=''
				,@SC_ID VARCHAR(MAX)=''
				,@CycleId VARCHAR(MAX)=''
				,@TariffId VARCHAR(MAX)=''
				,@BookNo VARCHAR(MAX)=''
				,@FromDate VARCHAR(20)=''
				,@ToDate VARCHAR(20)=''
		
		SELECT
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)
		
		
	IF (@FromDate ='' OR @FromDate IS NULL)
		BEGIN
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60
			SET	@Todate=dbo.fn_GetCurrentDateTime()
		END		
	  
	  SELECT(
		  SELECT     CA.GlobalAccountNumber
					,OldPostal_HouseNo
					,OldPostal_StreetName 
					,OldPostal_City
					,OldPostal_Landmark
					,OldPostal_AreaCode
					,OldPostal_ZipCode
					,NewPostal_HouseNo
					,NewPostal_StreetName 
					,NewPostal_City
					,NewPostal_Landmark
					,NewPostal_AreaCode
					,NewPostal_ZipCode 
					,OldService_HouseNo
					,OldService_StreetName
					,OldService_City
					,OldService_Landmark
					,OldService_AreaCode
					,OldService_ZipCode 
					,NewService_HouseNo 
					,NewService_StreetName
					,NewService_City
					,NewService_Landmark
					,NewService_AreaCode
					,NewService_ZipCode
					,CA.Remarks
					,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate 
					,ApproveSttus.ApprovalStatus
					,CA.CreatedBy
					,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			  FROM Tbl_CustomerAddressChangeLog_New CA	
			  INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber
			  AND  CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
			  AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))
			  AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))
			  AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))
			  AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))
			  AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))
			  AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
			  LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId			  
			FOR XML PATH('AuditTrayAddressList'),TYPE
			)
			FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')
	 
END
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		T.Karthik
---- Create date: 06-10-2014
---- Description:	The purpose of this procedure is to get Customer Address change logs based on search criteria
---- =============================================
--ALTER PROCEDURE [USP_GetCustomerAddressChangeLogs]
--(
--@XmlDoc xml
--)
--AS
--BEGIN
--		DECLARE @BU_ID VARCHAR(50)='',@SU_ID VARCHAR(50)='',@SC_ID VARCHAR(50)='',
--		@CycleId VARCHAR(50)='',@TariffId INT=0,
--		@FromDate VARCHAR(15)='',@ToDate VARCHAR(15)=''
		
--		SELECT
--			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
--			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
--			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(50)')
--			,@CycleId=C.value('(CycleId)[1]','VARCHAR(50)')
--			,@TariffId=C.value('(TariffId)[1]','INT')
--			,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
--			,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
--		FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)
		
		
--		SELECT
--		(
--			SELECT   CNL.AccountNo
--					,GlobalAccountNumber
--					,OldPostal_HouseNo
--					,OldPostal_StreetName 
--					,OldPostal_City
--					,OldPostal_Landmark
--					,OldPostal_AreaCode
--					,OldPostal_ZipCode
--					,NewPostal_HouseNo
--					,NewPostal_StreetName 
--					,NewPostal_City
--					,NewPostal_Landmark
--					,NewPostal_AreaCode
--					,NewPostal_ZipCode 
--					,OldService_HouseNo
--					,OldService_StreetName
--					,OldService_City
--					,OldService_Landmark
--					,OldService_AreaCode
--					,OldService_ZipCode 
--					,NewService_HouseNo 
--					,NewService_StreetName
--					,NewService_City
--					,NewService_Landmark
--					,NewService_AreaCode
--					,NewService_ZipCode					
--					,Remarks
--					,S.ApprovalStatus 
--					,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
--					,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate
--			FROM Tbl_CustomerAddressChangeLog_New CNL
--			JOIN Tbl_MApprovalStatus S ON S.ApprovalStatusId=CNL.ApprovalStatusId
--			JOIN [UDV_CustomerDescription] CD ON CNL.GlobalAccountNumber=CD.GlobalAccountNumber
--			AND (BU_ID=@BU_ID OR @BU_ID='')
--			AND (SU_ID=@SU_ID OR @SU_ID='')
--			AND (ServiceCenterId=@SC_ID OR @SC_ID='')
--			AND (CycleId=@CycleId OR @CycleId='')
--			AND (TariffId=@TariffId OR @TariffId=0)
--			AND (CONVERT(DATE,CNL.CreatedDate) >= CONVERT(DATE,@FromDate) OR @FromDate='')
--			AND (CONVERT(DATE,CNL.CreatedDate) <= CONVERT(DATE,@ToDate) OR @ToDate='')
--			ORDER BY CNL.CreatedDate DESC
--			FOR XML PATH('AuditTrayAddressList'),TYPE
--		)
--		FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')
--END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Padmini    
-- Create date: 05/02/2015    
-- Description: This Procedure is used to change customer address    
-- =============================================    
ALTER PROCEDURE [USP_ChangeCustomersAddress]    
(    
 @XmlDoc xml       
)    
AS    
BEGIN    
 DECLARE  @GlobalAccountNo   VARCHAR(50)      
   ,@NewPostalLandMark  VARCHAR(200)    
   ,@NewPostalStreet   VARCHAR(200)    
   ,@NewPostalCity   VARCHAR(200)    
   ,@NewPostalHouseNo   VARCHAR(200)    
   ,@NewPostalZipCode   VARCHAR(50)    
   ,@NewServiceLandMark  VARCHAR(200)    
   ,@NewServiceStreet   VARCHAR(200)    
   ,@NewServiceCity   VARCHAR(200)    
   ,@NewServiceHouseNo  VARCHAR(200)    
   ,@NewServiceZipCode  VARCHAR(50)    
   ,@NewPostalAreaCode  VARCHAR(50)    
   ,@NewServiceAreaCode  VARCHAR(50)    
   ,@NewPostalAddressID  INT    
   ,@NewServiceAddressID  INT    
   ,@IsSameAsServie           BIT    
   ,@ModifiedBy    VARCHAR(50)    
   ,@Details     VARCHAR(MAX)    
   ,@RowsEffected    INT    
   ,@AddressID INT   
   ,@StatusText VARCHAR(50)
   ,@IsCommunicationPostal BIT
   ,@IsCommunicationService BIT
     
       SELECT       
			 @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')    
		  --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')        
		  ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(200)')        
		  ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')        
		  ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')       
		  ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')      
		  --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')        
		  ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(200)')        
		  ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')        
		  ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')       
		  ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')    
		  ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')    
		  ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')    
		  ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')    
		  ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')    
		  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
		  ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')    
		  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		  ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')    
		  ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')            
			 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
     
BEGIN
	BEGIN TRY
		BEGIN TRAN 
		  --UPDATE POSTAL ADDRESS
		  SET @StatusText='Postal Address Update.'



    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
   ,ModifedBy=@ModifiedBy      
   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
   ,IsCommunication=@IsCommunicationPostal     
   WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1  
   
 IF (@IsSameAsServie =0) 
 
 BEGIN
   if EXISTS (select 0 from CUSTOMERS.Tbl_CustomerPostalAddressDetails where GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 )
BEGIN
 
   
   update CUSTOMERS.Tbl_CustomerPostalAddressDetails
	SET 
	  HouseNo=@NewServiceHouseNo       
     ,LandMark=@NewServiceLandMark       
     ,StreetName=@NewServiceStreet        
     ,City=@NewServiceCity      
     ,AreaCode=@NewServiceAreaCode      
     ,ZipCode=@NewServiceZipCode        
     ,ModifedBy=@ModifiedBy      
     ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
     ,IsCommunication=@IsCommunicationService         
       WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1 
   
   END
   ELSE
   
   BEGIn
   insert into 
     CUSTOMERS.Tbl_CustomerPostalAddressDetails(HouseNo,LandMark,StreetName,City,AreaCode,ZipCode,ModifedBy,ModifiedDate,IsCommunication,IsServiceAddress,IsActive)
     VALUES (@NewServiceHouseNo       
     ,@NewServiceLandMark       
     ,@NewServiceStreet        
     ,@NewServiceCity      
     ,@NewServiceAreaCode      
     ,@NewServiceZipCode        
     ,@ModifiedBy      
     ,dbo.fn_GetCurrentDateTime()  
     ,@IsCommunicationService         
       ,1,
       1)
       
       update CUSTOMERS.Tbl_CustomerSDetail  
         SET ServiceAddressID=SCOPE_IDENTITY()  
         WHERE GlobalAccountNumber=@GlobalAccountNo
         
              
   END
   END
   ELSE 
   BEGIN
    update CUSTOMERS.Tbl_CustomerSDetail  
         SET ServiceAddressID=PostalAddressID 
         WHERE GlobalAccountNumber=@GlobalAccountNo
    END
	COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN
		SET @RowsEffected =0
	END CATCH
		SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')         
	END
END
GO

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[UDV_CustomerDescription]
AS
	SELECT     
			 CD.GlobalAccountNumber
			,CD.DocumentNo
			,CD.AccountNo
			,CD.OldAccountNo
			,CD.Title
			,CD.FirstName
			,CD.MiddleName
			,CD.LastName
			,CD.KnownAs
			,CD.EmployeeCode
			,ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo
			,ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo
			,ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo
			,TD.PhoneNumber
			,TD.AlternatePhoneNumber
			,CD.ActiveStatusId
			,PD.PoleID
			,PD.TariffClassID AS TariffId
			,TC.ClassName
			,PD.IsBookNoChanged
			,PD.ReadCodeID
			,PD.SortOrder
			,CAD.Highestconsumption
			,CAD.OutStandingAmount
			,BN.BookNo
			,BN.BookCode
			,BU.BU_ID
			,BU.BusinessUnitName
			,BU.BUCode
			,SU.SU_ID
			,SU.ServiceUnitName
			,SU.SUCode
			,SC.ServiceCenterId
			,SC.ServiceCenterName
			,SC.SCCode
			,C.CycleId
			,C.CycleName
			,C.CycleCode
			,MS.StatusName AS ActiveStatus
			,CD.EmailId
			,PD.MeterNumber
			,MI.MeterType AS MeterTypeId
			,PD.PhaseId
			,PD.ClusterCategoryId
			,CAD.InitialReading
			,CAD.InitialBillingKWh AS MinimumReading
			,CAD.AvgReading
			,PD.RouteSequenceNumber AS RouteSequenceNo
			,CD.ConnectionDate
			,CD.CreatedDate
			,CD.CreatedBy
			,CAD.PresentReading
			,PD.CustomerTypeId
			,C.ActiveStatusId AS CylceActiveStatusId
			,CD.ServiceAddressID
			,Service_HouseNo
			,Service_StreetName
			,Service_Landmark
			,Service_City
			,Service_ZipCode
			,Postal_HouseNo
			,Postal_StreetName
			,Postal_Landmark
			,Postal_City
			,Postal_ZipCode
			,BN.SortOrder AS BookSortOrder
			,TC.RefClassID
		FROM CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
		CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
		CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN
		CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN
		dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN
		dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN
		dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN
		dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN
		dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN
		dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN
		dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN
		dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_RptCustomerDesc]
AS
	SELECT     
			 CD.GlobalAccountNumber
			,CD.DocumentNo
			,CD.AccountNo
			,CD.OldAccountNo
			,CD.Title
			,CD.FirstName
			,CD.MiddleName
			,CD.LastName
			,CD.KnownAs
			,CD.EmployeeCode
			,ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo
			,ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo
			,ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo
			,TD.PhoneNumber
			,TD.AlternatePhoneNumber
			,CD.ActiveStatusId
			,PD.PoleID
			,PD.TariffClassID AS TariffId
			,TC.ClassName
			,PD.IsBookNoChanged
			,PD.ReadCodeID
			,PD.SortOrder
			,CAD.Highestconsumption
			,CAD.OutStandingAmount
			,BN.BookNo
			,BN.BookCode
			,BU.BU_ID
			,BU.BusinessUnitName
			,BU.BUCode
			,SU.SU_ID
			,SU.ServiceUnitName
			,SU.SUCode
			,SC.ServiceCenterId
			,SC.ServiceCenterName
			,SC.SCCode
			,C.CycleId
			,C.CycleName
			,C.CycleCode
			,MS.StatusName AS ActiveStatus
			,CD.EmailId
			,PD.MeterNumber
			,MI.MeterType AS MeterTypeId
			,PD.PhaseId
			,PD.ClusterCategoryId
			,CAD.InitialReading
			,CAD.InitialBillingKWh AS MinimumReading
			,CAD.AvgReading
			,PD.RouteSequenceNumber AS RouteSequenceNo
			,CD.ConnectionDate
			,CD.CreatedDate
			,CD.CreatedBy
			,CAD.PresentReading
			,PD.CustomerTypeId
			,C.ActiveStatusId AS CylceActiveStatusId
			,CD.ServiceAddressID
			,Service_HouseNo
			,Service_StreetName
			,Service_Landmark
			,Service_City
			,Service_ZipCode
			,Postal_HouseNo
			,Postal_StreetName
			,Postal_Landmark
			,Postal_City
			,Postal_ZipCode
			,BN.SortOrder AS BookSortOrder
			,TC.RefClassID
		FROM CUSTOMERS.Tbl_CustomerSDetail AS CD 
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
		LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber 
		LEFT OUTER JOIN	CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId 
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
		LEFT JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
		LEFT JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
		LEFT JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
		LEFT JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
		INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
		LEFT OUTER JOIN	dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId 
		LEFT OUTER JOIN	dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Padmini    
-- Create date: 05/02/2015    
-- Description: This Procedure is used to change customer address    
-- =============================================    
ALTER PROCEDURE [USP_ChangeCustomersAddress]    
(    
 @XmlDoc xml       
)    
AS    
BEGIN    
 DECLARE  @GlobalAccountNo   VARCHAR(50)      
   ,@NewPostalLandMark  VARCHAR(200)    
   ,@NewPostalStreet   VARCHAR(200)    
   ,@NewPostalCity   VARCHAR(200)    
   ,@NewPostalHouseNo   VARCHAR(200)    
   ,@NewPostalZipCode   VARCHAR(50)    
   ,@NewServiceLandMark  VARCHAR(200)    
   ,@NewServiceStreet   VARCHAR(200)    
   ,@NewServiceCity   VARCHAR(200)    
   ,@NewServiceHouseNo  VARCHAR(200)    
   ,@NewServiceZipCode  VARCHAR(50)    
   ,@NewPostalAreaCode  VARCHAR(50)    
   ,@NewServiceAreaCode  VARCHAR(50)    
   ,@NewPostalAddressID  INT    
   ,@NewServiceAddressID  INT    
   ,@IsSameAsServie           BIT    
   ,@ModifiedBy    VARCHAR(50)    
   ,@Details     VARCHAR(MAX)    
   ,@RowsEffected    INT    
   ,@AddressID INT   
   ,@StatusText VARCHAR(50)
   ,@IsCommunicationPostal BIT
   ,@IsCommunicationService BIT
   ,@ApprovalStatusId INT=2
     
       SELECT       
			 @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')    
		  --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')        
		  ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(200)')        
		  ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')        
		  ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')       
		  ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')      
		  --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')        
		  ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(200)')        
		  ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')        
		  ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')       
		  ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')    
		  ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')    
		  ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')    
		  ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')    
		  ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')    
		  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
		  ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')    
		  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		  ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')    
		  ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')
		  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
			 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
     
BEGIN
	BEGIN TRY
		BEGIN TRAN 
		  --UPDATE POSTAL ADDRESS
		  SET @StatusText='Postal Address Update.'
		
	IF (@IsSameAsServie =0)
		BEGIN		
			INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
	ELSE
		BEGIN
			INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END

    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
   ,ModifedBy=@ModifiedBy      
   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
   ,IsCommunication=@IsCommunicationPostal     
   WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1  
   
 IF (@IsSameAsServie =0) 
 
 BEGIN
   if EXISTS (select 0 from CUSTOMERS.Tbl_CustomerPostalAddressDetails where GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 )
BEGIN
 
   
   update CUSTOMERS.Tbl_CustomerPostalAddressDetails
	SET 
	  HouseNo=@NewServiceHouseNo       
     ,LandMark=@NewServiceLandMark       
     ,StreetName=@NewServiceStreet        
     ,City=@NewServiceCity      
     ,AreaCode=@NewServiceAreaCode      
     ,ZipCode=@NewServiceZipCode        
     ,ModifedBy=@ModifiedBy      
     ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
     ,IsCommunication=@IsCommunicationService         
       WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1 
   
   END
   ELSE
   
   BEGIn
   insert into 
     CUSTOMERS.Tbl_CustomerPostalAddressDetails(HouseNo,LandMark,StreetName,City,AreaCode,ZipCode,ModifedBy,ModifiedDate,IsCommunication,IsServiceAddress,IsActive)
     VALUES (@NewServiceHouseNo       
     ,@NewServiceLandMark       
     ,@NewServiceStreet        
     ,@NewServiceCity      
     ,@NewServiceAreaCode      
     ,@NewServiceZipCode        
     ,@ModifiedBy      
     ,dbo.fn_GetCurrentDateTime()  
     ,@IsCommunicationService         
       ,1,
       1)
       
       update CUSTOMERS.Tbl_CustomerSDetail  
         SET ServiceAddressID=SCOPE_IDENTITY()  
         WHERE GlobalAccountNumber=@GlobalAccountNo
         
              
   END
   END
   ELSE 
   BEGIN
    update CUSTOMERS.Tbl_CustomerSDetail  
         SET ServiceAddressID=PostalAddressID 
         WHERE GlobalAccountNumber=@GlobalAccountNo
    END
	COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN
		SET @RowsEffected =0
	END CATCH
		SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')         
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Padmini    
-- Create date: 05/02/2015    
-- Description: This Procedure is used to change customer address    
-- =============================================    
ALTER PROCEDURE [USP_ChangeCustomerBookNo]    
(    
 @XmlDoc xml       
)    
AS    
BEGIN    
 DECLARE    @GlobalAccountNo   VARCHAR(50)      
		   ,@NewPostalLandMark  VARCHAR(200)    
		   ,@NewPostalStreet   VARCHAR(200)    
		   ,@NewPostalCity   VARCHAR(200)    
		   ,@NewPostalHouseNo   VARCHAR(200)    
		   ,@NewPostalZipCode   VARCHAR(50)    
		   ,@NewServiceLandMark  VARCHAR(200)    
		   ,@NewServiceStreet   VARCHAR(200)    
		   ,@NewServiceCity   VARCHAR(200)    
		   ,@NewServiceHouseNo  VARCHAR(200)    
		   ,@NewServiceZipCode  VARCHAR(50)    
		   ,@NewPostalAreaCode  VARCHAR(50)    
		   ,@NewServiceAreaCode  VARCHAR(50)    
		   ,@NewPostalAddressID  INT    
		   ,@NewServiceAddressID  INT    
		   ,@IsSameAsServie           BIT    
		   ,@ModifiedBy    VARCHAR(50)    
		   ,@Details     VARCHAR(MAX)    
		   ,@RowsEffected    INT    
		   ,@AddressID INT   
		   ,@StatusText VARCHAR(50)
		   ,@IsCommunicationPostal BIT
		   ,@IsCommunicationService BIT
		   ,@BookNo VARCHAR(20)
		   ,@OldBookNo VARCHAR(50)
		   ,@Flag INT
		   ,@ApprovalStatusId INT
     
       SELECT       
			 @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')    
		  ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(200)')        
		  ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')        
		  ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')       
		  ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')      
		  ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(200)')        
		  ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')        
		  ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')       
		  ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')    
		  ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')    
		  ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')    
		  ,@BookNo=C.value('(BookNo)[1]','VARCHAR(20)')    
		  ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')    
		  ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')    
		  ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')    
		  ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')    
		  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		  ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')    
		  ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')   
		  ,@Flag=C.value('(Flag)[1]','INT')
		  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		  ,@OldBookNo=C.value('(OldBookNo)[1]','VARCHAR(50)')
		           
			 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
     
BEGIN
	BEGIN TRY
		BEGIN TRAN 
		
		SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)

	--	IF(@Flag = 1)
	-- BEGIN
		
	--	INSERT INTO Tbl_BookNoChangeLogs( AccountNo
	--									--,CustomerUniqueNo
	--									,OldBookNo
	--									,NewBookNo
	--									,CreatedBy
	--									,CreatedDate
	--									,ApproveStatusId
	--									,Remarks)
	--	VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),0,@Details)	
		
		
	-- SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	-- END 
 --ELSE
	--BEGIN
	
	--	INSERT INTO Tbl_BookNoChangeLogs( AccountNo
	--									--,CustomerUniqueNo
	--									,OldBookNo
	--									,NewBookNo
	--									,CreatedBy
	--									,CreatedDate
	--									,ApproveStatusId
	--									,Remarks)
	--	VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),2,@Details)
			
	--END	
		
		IF (@IsSameAsServie =0)
		BEGIN		
			INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
	ELSE
		BEGIN
			INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
		
		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
										,OldBookNo
										,NewBookNo
										,CreatedBy
										,CreatedDate
										,ApproveStatusId
										,Remarks)
		VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),@ApprovalStatusId,@Details)	
		
		
		
		     IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE BookNo=@BookNo AND GlobalAccountNumber=@GlobalAccountNo)  
				BEGIN  
				
				DECLARE @BU_ID VARCHAR(50)
				,@SU_ID VARCHAR(50)
				,@ServiceCenterId VARCHAR(50)
				,@AccountNo VARCHAR(50)
				--Fetching ID's Of BU,SU,SC for acc no generation
				
				SELECT   @BU_ID=BU.BU_ID
						,@SU_ID=SU.SU_ID
						,@ServiceCenterId=SC.ServiceCenterId
				FROM Tbl_BookNumbers B
				JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
				JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
				JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
				JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
				WHERE B.BookNo=@BookNo
				
				SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate 
		
				  UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET     
						IsBookNoChanged=1
						,BookNo=@BookNo
				  WHERE GlobalAccountNumber=@GlobalAccountNo
				  
				  UPDATE [CUSTOMERS].[Tbl_CustomerSDetail] SET
						AccountNo=@AccountNo
				  WHERE GlobalAccountNumber=@GlobalAccountNo
				END
		  --UPDATE POSTAL ADDRESS
		  SET @StatusText='Postal Address Update.'
		   UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET    
			 LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END    
			,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END    
			,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END    
			,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
			,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END      
			,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END    
			,ModifedBy=@ModifiedBy    
			,ModifiedDate=dbo.fn_GetCurrentDateTime() 
			,IsCommunication=@IsCommunicationPostal   
			WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1
		   SET @RowsEffected=@@ROWCOUNT    
	              
		--IF CUSTOMER HAS SERVICE ADDRESS      
		IF(@IsSameAsServie=0)    
		 BEGIN   
			--IF CUSTOMER HAS SERVICE ADDRESS THEN UPDATE 
		  IF EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
			   WHERE IsServiceAddress=1 AND GlobalAccountNumber=@GlobalAccountNo AND IsActive=1)    
			   BEGIN      
					SET @StatusText='Service Address Update.'         
					UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET    
					 HouseNo=@NewServiceHouseNo     
					,LandMark=@NewServiceLandMark     
					,StreetName=@NewServiceStreet      
					,City=@NewServiceCity    
					,AreaCode=@NewServiceAreaCode    
					,ZipCode=@NewServiceZipCode      
					,ModifedBy=@ModifiedBy    
					,ModifiedDate=dbo.fn_GetCurrentDateTime()
					,IsCommunication=@IsCommunicationService       
					  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
					SET @RowsEffected=@@ROWCOUNT   
				END 
			ELSE
			
				BEGIN
					--CHECK IS CUSTOMER ALREADY HAVING SAME ADDRESS
					SET @AddressID =(SELECT  AddressID
					  FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]    
						  WHERE			GlobalAccountNumber=@GlobalAccountNo 
									  AND IsServiceAddress=1    
									  AND HouseNo=@NewServiceHouseNo 
									  --AND Landmark=@NewServiceLandMark 
									  AND StreetName=@NewServiceStreet     
									  AND City=@NewServiceCity 
									  --AND HouseNo=@NewServiceHouseNo 
									  AND ZipCode=@NewServiceZipCode    
									  AND AreaCode=@NewServiceAreaCode 
									  AND IsActive=0
						  )		
					--IF CUSTOMER HAVING SAME ADDRESS THEN CHANGE STATUS
					IF(@AddressID>0)			  
						 BEGIN  		
							SET @StatusText='Service Address IsActive Update.'                    
							  UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails    
							  SET IsActive=1,IsCommunication=@IsCommunicationService  
							  WHERE AddressID=@AddressID
							  SET @RowsEffected=@@ROWCOUNT    
						 END   
					-- IF CUSTOMER DOESN'T HAS SERVICE ADDRESS THEN INSERT SERVICE ADDRESS
					 ELSE
						 BEGIN  
							SET @StatusText='Service Address Insertion.'                     					 
							 INSERT INTO [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]    
																				  (    
																				   GlobalAccountNumber    
																				   ,HouseNo    
																				   ,Landmark    
																				   ,StreetName    
																				   ,City    
																				   ,AreaCode    
																				   ,ZipCode    
																				   ,CreatedDate    
																				   ,IsServiceAddress    
																				   ,IsCommunication
																				  )      
																				 VALUES    
																				 (    
																				   @GlobalAccountNo     
																				  ,@NewServiceHouseNo    
																				  ,@NewServiceLandMark    
																				  ,@NewServiceStreet    
																				  ,@NewServiceCity    
																				  ,@NewServiceAreaCode    
																				  ,@NewServiceZipCode    
																				  ,dbo.fn_GetCurrentDateTime()    
																				  ,1    
																				  ,@IsCommunicationService
																				 )   
											SET @RowsEffected=@@ROWCOUNT     
					 END
				
		   END   
	       
	        
		 END    
		ELSE
			BEGIN
				--DEACTIVATE ADDRESS IF CUSTOMER SELECT HIS POSTAL ADDRESS SAME AS SERVICE ADDRESS
				 IF EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
				   WHERE IsServiceAddress=1 AND GlobalAccountNumber=@GlobalAccountNo AND IsActive=1) 
				   BEGIN  
					  SET @StatusText='Service Address Logical Delettion.'                     					 
					  UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails    
					  SET IsActive=0 
					  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
					  SET @RowsEffected=@@ROWCOUNT   
					  
					 --IF CUSTOMER DELECT ALL HIS SERVICE ADDRESS, POSTAL ADDRESS HAS TO BE HIS COMMUNICATION ADDRESS
					IF NOT EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
									WHERE IsServiceAddress=1 
										AND GlobalAccountNumber=@GlobalAccountNo 
										AND IsActive=1)
										
					BEGIN
						UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails SET IsCommunication=1 
						WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1
					END										
				   
				   END
				   
				     --   UPDATE CUSTOMERS.Tbl_CustomerSDetail
					    --SET PostalAddress=
					    --[dbo].[fn_GetCustomerAddressFormat](@NewPostalHouseNo,@NewPostalStreet,@NewPostalCity,@NewPostalZipCode)
					    --WHERE GlobalAccountNumber=@GlobalAccountNo
					    
					    
					    --UPDATE CUSTOMERS.Tbl_CustomerSDetail
					    --SET ServiceAddress=
					    --[dbo].[fn_GetCustomerAddressFormat](@NewServiceHouseNo,@NewServiceStreet,@NewServiceCity,@NewServiceZipCode)
					    --WHERE GlobalAccountNumber=@GlobalAccountNo
				   
			END      
	COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN
		SET @RowsEffected =0
	END CATCH
		SELECT @RowsEffected AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')         
	END
END

--USE [EBMS]
--GO
--/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerBookNo]    Script Date: 02/07/2015 14:31:52 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
------------------------------------------------------------------------------------------------  
---- =============================================    
---- Author:  V.Bhimaraju    
---- Create date: 29-09-2014    
---- Description: The purpose of this procedure is to Update Customer BookNo   
---- =============================================    
--ALTER PROCEDURE [dbo].[USP_ChangeCustomerBookNo]    
--(    
--@XmlDoc xml    
--)    
--AS    
--BEGIN    
-- DECLARE @AccountNo VARCHAR(50)  
--  --,@BU_ID VARCHAR(20)    
--  --      ,@SU_ID VARCHAR(20)    
--  --,@ServiceCenterId VARCHAR(20)    
--     ,@BookNo VARCHAR(20)  
--     ,@PostalLandMark VARCHAR(200)    
--     ,@PostalStreet VARCHAR(200)    
--     ,@PostalCity VARCHAR(200)    
--     ,@PostalHouseNo VARCHAR(200)  
--     ,@PostalZipCode VARCHAR(50)  
--     ,@ServiceLandMark VARCHAR(200)    
--     ,@ServiceStreet VARCHAR(200)    
--     ,@ServiceCity VARCHAR(200)    
--     ,@ServiceHouseNo VARCHAR(200)  
--     ,@ServiceZipCode VARCHAR(50)  
--     ,@ModifiedBy VARCHAR(50)  
--     ,@IsBookNoChanged BIT  
--     --,@ActiveStatusId INT
--     ,@Flag INT  
--     ,@Details VARCHAR(MAX)
       
       
--   SELECT     
--     @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
--    --,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')    
--    --,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')    
--    --,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')    
--    ,@BookNo=C.value('(BookNo)[1]','VARCHAR(20)')  
--    ,@PostalLandMark=C.value('(PostalLandMark)[1]','VARCHAR(200)')    
--    ,@PostalStreet=C.value('(PostalStreet)[1]','VARCHAR(200)')    
--    ,@PostalCity=C.value('(PostalCity)[1]','VARCHAR(200)')    
--    ,@PostalHouseNo=C.value('(PostalHouseNo)[1]','VARCHAR(200)')   
--    ,@PostalZipCode=C.value('(PostalZipCode)[1]','VARCHAR(50)')  
--    ,@ServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')    
--    ,@ServiceStreet=C.value('(ServiceStreet)[1]','VARCHAR(200)')    
--    ,@ServiceCity=C.value('(ServiceCity)[1]','VARCHAR(200)')    
--    ,@ServiceHouseNo=C.value('(ServiceHouseNo)[1]','VARCHAR(200)')   
--    ,@ServiceZipCode=C.value('(ServiceZipCode)[1]','VARCHAR(50)')  
--    --,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT') 
--    ,@Flag=C.value('(Flag)[1]','INT')  
--    ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
--    ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
-- FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
 
-- --DECLARE @CustUniqueno VARCHAR(50)   = (SELECT CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo)
-- DECLARE @OldBookNo VARCHAR(20)   = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
   
-- IF(@Flag = 1)
--	 BEGIN
		
--		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
--										--,CustomerUniqueNo
--										,OldBookNo
--										,NewBookNo
--										,CreatedBy
--										,CreatedDate
--										,ApproveStatusId
--										,Remarks)
--		VALUES(@AccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),0,@Details)
--	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
--	 END 
-- ELSE
--	BEGIN
	
--		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
--										--,CustomerUniqueNo
--										,OldBookNo
--										,NewBookNo
--										,CreatedBy
--										,CreatedDate
--										,ApproveStatusId
--										,Remarks)
--		VALUES(@AccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),2,@Details)
	
--		IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE BookNo=@BookNo AND GlobalAccountNumber=@AccountNo)  
--		BEGIN  
--			  UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET     
--					IsBookNoChanged=1  
--			  WHERE GlobalAccountNumber=@AccountNo  
    
--				UPDATE Tbl_Audit_CustomerDetails SET     
--					IsBookNoChanged=1  
--				WHERE AccountNo=@AccountNo  
--		END  
    
--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET  
--				--BU_ID=@BU_ID  
--			 --  ,SU_ID=@SU_ID  
--			 --  ,ServiceCenterId=@ServiceCenterId  
--			   BookNo=@BookNo  
--			    ,ModifedBy=@ModifiedBy  
--			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
--		WHERE GlobalAccountNumber=@AccountNo  

--		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET
--			   LandMark=CASE @PostalLandMark WHEN '' THEN NULL ELSE @PostalLandMark END      
--			   ,StreetName=CASE @PostalStreet WHEN '' THEN NULL ELSE @PostalStreet END      
--			   ,City=CASE @PostalCity WHEN '' THEN NULL ELSE @PostalCity END      
--			   ,HouseNo=CASE @PostalHouseNo WHEN '' THEN NULL ELSE @PostalHouseNo END    
--			   ,ZipCode=CASE @PostalZipCode WHEN '' THEN NULL ELSE @PostalZipCode END  
--			   ,ModifedBy=@ModifiedBy  
--			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
--		WHERE GlobalAccountNumber=@AccountNo  AND IsServiceAddress=0

--		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET
--			    LandMark=CASE @ServiceLandMark WHEN '' THEN NULL ELSE @ServiceLandMark END      
--			   ,StreetName=CASE @ServiceStreet WHEN '' THEN NULL ELSE @ServiceStreet END      
--			   ,City=CASE @ServiceCity WHEN '' THEN NULL ELSE @ServiceCity END      
--			   ,HouseNo=CASE @ServiceHouseNo WHEN '' THEN NULL ELSE @ServiceHouseNo END    
--			   ,ZipCode=CASE @ServiceZipCode WHEN '' THEN NULL ELSE @ServiceZipCode END  
--			   ,ModifedBy=@ModifiedBy  
--			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
--		WHERE GlobalAccountNumber=@AccountNo  AND IsServiceAddress=1
      
--      --Audit Table
--		UPDATE Tbl_Audit_CustomerDetails SET     
--			--BU_ID=@BU_ID  
--		  -- ,SU_ID=@SU_ID  
--		  -- ,ServiceCenterId=@ServiceCenterId  
--		    BookNo=@BookNo  
--		   ,PostalLandMark=CASE @PostalLandMark WHEN '' THEN NULL ELSE @PostalLandMark END      
--		   ,PostalStreet=CASE @PostalStreet WHEN '' THEN NULL ELSE @PostalStreet END      
--		   ,PostalCity=CASE @PostalCity WHEN '' THEN NULL ELSE @PostalCity END      
--		   ,PostalHouseNo=CASE @PostalHouseNo WHEN '' THEN NULL ELSE @PostalHouseNo END    
--		   ,PostalZipCode=CASE @PostalZipCode WHEN '' THEN NULL ELSE @PostalZipCode END  
--		   ,ServiceLandMark=CASE @ServiceLandMark WHEN '' THEN NULL ELSE @ServiceLandMark END      
--		   ,ServiceStreet=CASE @ServiceStreet WHEN '' THEN NULL ELSE @ServiceStreet END      
--		   ,ServiceCity=CASE @ServiceCity WHEN '' THEN NULL ELSE @ServiceCity END      
--		   ,ServiceHouseNo=CASE @ServiceHouseNo WHEN '' THEN NULL ELSE @ServiceHouseNo END    
--		   ,ServiceZipCode=CASE @ServiceZipCode WHEN '' THEN NULL ELSE @ServiceZipCode END
--		   --,IsBookNoChanged=1  
--		   ,ModifiedBy=@ModifiedBy  
--		   --,ActiveStatusId=@ActiveStatusId  
--		   ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
--		WHERE AccountNo=@AccountNo  
        
--		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
 
--	END
-- END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  M.RamaDevi  
-- Create date: 12-04-2014
-- Author:  V.Bhimaraju
-- Create date: 07-08-2014
-- Modified By: T.Karthik
-- Modified Date: 29-10-2014
-- Description: To get Accounts without cycle  Customers
-- =============================================  
ALTER PROCEDURE [USP_GetAccountWithoutCycle_New]
(
	@xmlDoc xml
)
AS  
BEGIN  
		DECLARE    @BU_ID VARCHAR(MAX)   
				  ,@SU_ID VARCHAR(MAX)  
				  ,@ServiceCenterId VARCHAR(MAX)  
				  ,@Tariff VARCHAR(MAX)  
				  ,@PageNo INT  
				  ,@PageSize INT
				    
		SELECT	 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
				,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
				,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
				,@Tariff=C.value('(Tariff)[1]','VARCHAR(MAX)')  
				,@PageNo = C.value('(PageNo)[1]','INT')  
				,@PageSize = C.value('(PageSize)[1]','INT')   
		FROM @xmlDoc.nodes('ConsumerBe') AS T(C)  
  

				 		
						 		  
			SELECT	 GlobalAccountNumber,OldAccountNo,
					 (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) as FullName,
					 dbo.fn_GetCustomerServiceAddress_New(Cd.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode)   as [ServiceAdress]
					 ,CD.ClassName
					 ,CD.BookNo
					 ,CD.ConnectionDate
					 ,IDENTITY (int, 1,1) As   RowNumber
					 ,CD.BusinessUnitName
					 ,CD.ServiceUnitName
					 ,CD.ServiceCenterName
					 ,CD.SortOrder AS CustomerSortOrder
			INTO #CustomersList
			FROM [UDV_RptCustomerDesc] CD
			WHERE CycleId IS NULL
			--AND CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID,','))
			--AND CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU_ID,','))
			--AND CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@ServiceCenterId,','))
			AND (CD.TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,',')) OR @Tariff='')


			Declare @TotlaRecords INT

			SET	   @TotlaRecords = (select COUNT(0) from  #CustomersList)

			Select	RowNumber
					,GlobalAccountNumber AS AccountNo
					,ISNULL(OldAccountNo,'--') AS OldAccountNo
					,FullName   as Name
					,ServiceAdress as ServiceAddress
					,ClassName
					,BookNo
					,BusinessUnitName
					,ServiceUnitName
					,ServiceCenterName
					,CustomerSortOrder
					,ISNULL(CONVERT(VARCHAR(20),ConnectionDate,106),'--') AS ConnectionDate
					,@TotlaRecords As TotalRecords	 from #CustomersList
			  Where  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 

			--Select * from #CustomersList
			DROP TABLE	#CustomersList
		 
				   
END  
GO
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- =============================================  
ALTER PROCEDURE [USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
     
   SELECT(  
     SELECT   TCR.AccountNo  
       ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
       ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
       ,TCR.Remarks  
       ,TCR.CreatedBy  
       ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_LCustomerTariffChangeRequest  TCR  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
END  
GO
---------------------------------------------------------------------------
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014   
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
---- Description: The purpose of this procedure is to get limited Tariff change request logs  
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
     SELECT  TOP 10 TCR.AccountNo  
       ,TCR.ApprovalStatusId  
       ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
       ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
       ,TCR.Remarks  
       ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_LCustomerTariffChangeRequest  TCR  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId       
     GROUP BY TCR.AccountNo  
       ,TCR.ApprovalStatusId  
       ,PreviousTariffId  
       ,ChangeRequestedTariffId  
       ,TCR.Remarks  
       ,CONVERT(VARCHAR(30),TCR.CreatedDate,107)  
       ,ApproveSttus.ApprovalStatus  
       ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
GO
----------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
    ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
    ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
    ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
    ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
    ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
    ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
    ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
  IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END 
  
  -- start By Karteek
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
    
  SELECT(  
     SELECT   BookChange.AccountNo  
       ,BookChange.OldBookNo  
       ,BookChange.NewBookNo  
       ,BookChange.Remarks  
       ,BookChange.CreatedBy  
       ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,CustomerView.BusinessUnitName  
       ,CustomerView.ServiceUnitName  
       ,CustomerView.ServiceCenterName  
       ,CustomerView.CycleName  
       ,CustomerView.BookCode  
       ,CustomerView.SortOrder AS CustomerSortOrder  
       ,CustomerView.BookSortOrder AS BookSortOrder  
       ,CustomerView.OldAccountNo  
     FROM Tbl_BookNoChangeLogs  BookChange  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId       
       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
GO
----------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetBookNoChangeLogsWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
     SELECT  TOP 10 BookChange.AccountNo  
       ,BookChange.ApproveStatusId  
       ,BookChange.OldBookNo  
       ,BookChange.NewBookNo  
       ,BookChange.Remarks  
       ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_BookNoChangeLogs  BookChange  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId 
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId       
     GROUP BY BookChange.AccountNo  
       ,BookChange.ApproveStatusId  
       ,BookChange.OldBookNo  
       ,BookChange.NewBookNo  
       ,BookChange.Remarks  
       ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107)  
       ,ApproveSttus.ApprovalStatus  
       ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
GO
-------------------------------------------------------------------------------------
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
ALTER PROCEDURE [USP_GetCustomerChangeLogsWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek     
     
   SELECT(  
     SELECT  TOP 10 CCL.AccountNo  
       ,CCL.ApproveStatusId  
       ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
       ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
       ,CCL.Remarks  
       ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_CustomerActiveStatusChangeLogs CCL   
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId       
     GROUP BY CCL.AccountNo  
       ,CCL.ApproveStatusId  
       ,CCL.Remarks  
       ,CONVERT(VARCHAR(30),CCL.CreatedDate,107)   
       ,ApproveSttus.ApprovalStatus  
       ,OldStatus  
       ,NewStatus  
       ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
GO
----------------------------------------------------------------------------------
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
ALTER PROCEDURE [USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
   SELECT(  
     SELECT  CCL.AccountNo  
       ,CCL.ApproveStatusId  
       ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
       ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
       ,CCL.Remarks  
       ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CCL.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerActiveStatusChangeLogs CCL   
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
GO
----------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
   SELECT(  
     SELECT  CMI.AccountNo  
         ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
       ,CMI.Remarks  
       ,CMI.OldDials  
       ,CMI.NewDials  
       ,CMI.CreatedBy  
       ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CMI.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
GO
-----------------------------------------------------------------------
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014 
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetCustomerMeterChangeLogsWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
   SELECT(  
     SELECT  TOP 10 CMI.AccountNo  
         ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
       ,CMI.Remarks  
       ,CMI.OldDials  
       ,CMI.NewDials  
       ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId       
     GROUP BY CMI.AccountNo  
         ,OldMeterTypeId  
       ,NewMeterTypeId  
       ,CMI.Remarks  
         ,CMI.OldDials  
       ,CMI.NewDials  
       ,CONVERT(VARCHAR(30),CMI.CreatedDate,107)  
       ,ApproveSttus.ApprovalStatus  
       ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END 
GO
-----------------------------------------------------------------------------------
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- =============================================  
ALTER PROCEDURE [USP_GetCustomerNameChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
     SELECT  TOP 10 CNL.GlobalAccountNumber AS AccountNo  
       ,CNL.Remarks  
       ,CNL.OldTitle  
       ,CNL.NewTitle  
       ,CNL.OldFirstName AS OldName  
       ,CNL.NewFirstName AS [NewName]  
       ,CNL.OldMiddleName  
       ,CNL.NewMiddleName  
       ,CNL.OldLastName  
       ,CNL.NewLastName  
       ,CNL.OldKnownAs AS OldSurName  
       ,CNL.NewKnownAs AS NewSurName  
       ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_CustomerNameChangeLogs CNL  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId       
     GROUP BY CNL.GlobalAccountNumber  
       ,Remarks  
       ,OldTitle  
       ,NewTitle  
       ,OldFirstName  
       ,NewFirstName  
       ,OldMiddleName  
       ,NewMiddleName  
       ,OldLastName  
       ,NewLastName  
       ,OldKnownAs  
       ,NewKnownAs  
       ,CONVERT(VARCHAR(30),CNL.CreatedDate,107)  
       ,ApproveSttus.ApprovalStatus  
       ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
GO
--------------------------------------------------------------------------------
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- =============================================  
ALTER PROCEDURE [USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
     SELECT   CNL.GlobalAccountNumber AS AccountNo  
       ,CNL.Remarks  
       ,CNL.OldTitle  
       ,CNL.NewTitle  
       ,CNL.OldFirstName  
       ,CNL.NewFirstName  
       ,CNL.OldMiddleName  
       ,CNL.NewMiddleName  
       ,CNL.OldLastName  
       ,CNL.NewLastName  
       ,CNL.OldKnownAs  
       ,CNL.NewKnownAs  
       ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CNL.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerNameChangeLogs CNL  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END 
GO
-----------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END   
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
   SELECT(  
    SELECT     CA.GlobalAccountNumber  
     ,OldPostal_HouseNo  
     ,OldPostal_StreetName   
     ,OldPostal_City  
     ,OldPostal_Landmark  
     ,OldPostal_AreaCode  
     ,OldPostal_ZipCode  
     ,NewPostal_HouseNo  
     ,NewPostal_StreetName   
     ,NewPostal_City  
     ,NewPostal_Landmark  
     ,NewPostal_AreaCode  
     ,NewPostal_ZipCode   
     ,OldService_HouseNo  
     ,OldService_StreetName  
     ,OldService_City  
     ,OldService_Landmark  
     ,OldService_AreaCode  
     ,OldService_ZipCode   
     ,NewService_HouseNo   
     ,NewService_StreetName  
     ,NewService_City  
     ,NewService_Landmark  
     ,NewService_AreaCode  
     ,NewService_ZipCode  
     ,CA.Remarks  
     ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
     ,ApproveSttus.ApprovalStatus  
     ,CA.CreatedBy  
     ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerAddressChangeLog_New CA   
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId       
   FOR XML PATH('AuditTrayAddressList'),TYPE  
   )  
   FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
    
END 
GO
-------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014 
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get limited Customer Address change logs  
-- =============================================  
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
   
  
   SELECT(  
     SELECT  TOP 10  CA.GlobalAccountNumber  
     ,OldPostal_HouseNo  
     ,OldPostal_StreetName   
     ,OldPostal_City  
     ,OldPostal_Landmark  
     ,OldPostal_AreaCode  
     ,OldPostal_ZipCode  
     ,NewPostal_HouseNo  
     ,NewPostal_StreetName   
     ,NewPostal_City  
     ,NewPostal_Landmark  
     ,NewPostal_AreaCode  
     ,NewPostal_ZipCode   
     ,OldService_HouseNo  
     ,OldService_StreetName  
     ,OldService_City  
     ,OldService_Landmark  
     ,OldService_AreaCode  
     ,OldService_ZipCode   
     ,NewService_HouseNo   
     ,NewService_StreetName  
     ,NewService_City  
     ,NewService_Landmark  
     ,NewService_AreaCode  
     ,NewService_ZipCode  
     ,CA.Remarks  
     ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
     ,ApproveSttus.ApprovalStatus  
     ,CA.CreatedBy  
     ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_CustomerAddressChangeLog_New CA        
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId       
     GROUP BY CA.GlobalAccountNumber  
     ,OldPostal_HouseNo  
     ,OldPostal_StreetName   
     ,OldPostal_City  
     ,OldPostal_Landmark  
     ,OldPostal_AreaCode  
     ,OldPostal_ZipCode  
     ,NewPostal_HouseNo  
     ,NewPostal_StreetName   
     ,NewPostal_City  
     ,NewPostal_Landmark  
     ,NewPostal_AreaCode  
     ,NewPostal_ZipCode   
     ,OldService_HouseNo  
     ,OldService_StreetName  
     ,OldService_City  
     ,OldService_Landmark  
     ,OldService_AreaCode  
     ,OldService_ZipCode   
     ,NewService_HouseNo   
     ,NewService_StreetName  
     ,NewService_City  
     ,NewService_Landmark  
     ,NewService_AreaCode  
     ,NewService_ZipCode  
     ,CA.Remarks  
     ,CONVERT(VARCHAR(30),CA.CreatedDate,107)  
     ,ApprovalStatus  
     ,CA.CreatedBy  
     ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
     FOR XML PATH('AuditTrayAddressList'),TYPE  
     )  
     FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
END  
GO
------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
     SELECT  CC.GlobalAccountNumber AS AccountNo  
       ,CC.ApprovalStatusId  
       ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
       ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
       ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CC.Remarks  
       ,CC.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerTypeChangeLogs  CC  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId       
   FOR XML PATH('AuditTrayList'),TYPE  
   )  
   FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END
GO
-----------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetCustomerTypeChangeWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
     SELECT  TOP 10 CC.GlobalAccountNumber AS AccountNo  
       ,CC.ApprovalStatusId  
       ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
       ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
       ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CC.Remarks  
       ,CC.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,COUNT(0) OVER() AS TotalChanges  
     FROM Tbl_CustomerTypeChangeLogs  CC  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId       
     GROUP BY CC.GlobalAccountNumber  
       ,CC.ApprovalStatusId  
       ,OldCustomerTypeId  
       ,NewCustomerTypeId  
       ,CC.CreatedBy  
       ,CONVERT(VARCHAR(30),CC.CreatedDate,107)  
       ,ApproveSttus.ApprovalStatus  
       ,CC.Remarks  
       ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
    FOR XML PATH('AuditTrayList'),TYPE  
    )  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
GO
------------------------------------------------------------------------------------------
GO
