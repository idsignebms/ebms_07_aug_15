GO
ALTER TABLE CUSTOMERS.Tbl_CustomerPostalAddressDetails
ALTER COLUMN StreetName VARCHAR(255) 


GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ALTER COLUMN Service_StreetName VARCHAR(255) 

GO
ALTER TABLE CUSTOMERS.Tbl_CustomerSDetail
ALTER COLUMN Postal_StreetName VARCHAR(255) 

GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ALTER COLUMN OldPostal_StreetName VARCHAR(255) 

GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ALTER COLUMN NewPostal_StreetName VARCHAR(255) 

GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ALTER COLUMN OldService_StreetName VARCHAR(255) 

GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ALTER COLUMN NewService_StreetName VARCHAR(255) 


GO
ALTER TABLE Tbl_CustomerAddressChangeLogs
ALTER COLUMN OldStreetName VARCHAR(255) 

GO
ALTER TABLE Tbl_CustomerAddressChangeLogs
ALTER COLUMN NewStreetName VARCHAR(255) 


-------------------------------------------------------------------------------


GO
-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-Mar-2015>
-- Description:	<To get the report for Read Customers>
-- =============================================
CREATE PROCEDURE USP_RptPreBillingReadCustomer
(
@XmlDoc XML
)
AS
BEGIN
	Declare @ReadCode INT    
			,@BU_ID VARCHAR(20)
			,@SU_ID VARCHAR(20)
			,@ServiceCenterId VARCHAR(20)
			
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(20)')
		,@ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(20)')
FROM	@XmlDoc.nodes('RptPreBillingBe') as T(C)     
 
 
   SELECT    
   ( 
SELECT CD.GlobalAccountNumber    
     ,MIN(CR.PreviousReading) as PreviousReading    
     --,MAX(CR.PresentReading) as PresentReading    
     ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name    
     ,CD.ClassName AS ClassName  
     ,CD.MeterNumber AS MeterNo  
     ,OldAccountNo   
     ,MIN (CR.ReadDate) AS PreviousReadDate  
     ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    FROM Tbl_CustomerReadings CR    
    INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = Cr.GlobalAccountNumber     
    LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId    
    AND CD.BU_ID = @BU_Id-----  
    --AND CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID,','))
    --AND (CD.SU_ID IN (SELECT [com] FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')
    --AND (CD.ServiceCenterId IN (SELECT [com] FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
    GROUP BY CD.GlobalAccountNumber    
     ,CD.FirstName    
     ,CD.Title    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.MeterNumber    
     ,CD.ClassName    
     ,OldAccountNo   
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
    FOR XML PATH('PreBillingList'),TYPE    
   )       
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')    
  
  
END
GO
-------------------------------------------------------------------------------------------------------



GO
-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-Mar-2015>
-- Description:	<To get the report of PreBilling Direct Customers>
-- =============================================
CREATE PROCEDURE USP_RptPreBillingDirectCustomer
(
@XmlDoc XML
)
AS
BEGIN    

Declare @ReadCode INT    
			,@BU_ID VARCHAR(20)
			,@SU_ID VARCHAR(20)
			,@ServiceCenterId VARCHAR(20)
			
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(20)')
		,@ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(20)')
FROM	@XmlDoc.nodes('RptPreBillingBe') as T(C)     


   ;WITH Results AS    
   (    
   SELECT     
    CD.GlobalAccountNumber    
    ,MIN(CR.PreviousReading) AS PreviousReading    
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name     
    ,CD.ClassName    
    ,OldAccountNo    
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
   FROM UDV_CustomerDescription CD    
   LEFT JOIN Tbl_CustomerReadings CR ON (CD.GlobalAccountNumber != CR.GlobalAccountNumber AND IsBilled=0)    
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId     
   AND CD.BU_ID = @BU_Id-----  
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
   GROUP BY CD.GlobalAccountNumber    
     ,CD.Title    
     ,CD.FirstName    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.ClassName    
     ,OldAccountNo    
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
   UNION     
   SELECT     
    CD.GlobalAccountNumber    
    ,MIN(CR.PreviousReading) AS PreviousReading    
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name     
    ,CD.ClassName    
    ,OldAccountNo    
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
   FROM UDV_CustomerDescription CD    
   LEFT JOIN Tbl_CustomerReadings CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber    
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId    
   AND CD.BU_ID = @BU_Id-----  
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
   WHERE CustomerTypeId = 1    
   GROUP BY CD.GlobalAccountNumber    
     ,CD.Title    
     ,CD.FirstName    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.ClassName    
     ,OldAccountNo    
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
   )    
       
   SELECT    
   (    
   SELECT * FROM Results    
    FOR XML PATH('PreBillingList'),TYPE    
   )       
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')    
  END  
GO


--------------------------------------------------------------------------------------------------------


GO
-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-Mar-2015>
-- Description:	<To get the report of PreBilling Non Read Customers>
-- =============================================
CREATE PROCEDURE USP_RptPreBillingNonReadCustomer
(
@XmlDoc XML
)
AS
BEGIN    

Declare @ReadCode INT    
			,@BU_ID VARCHAR(20)
			,@SU_ID VARCHAR(20)
			,@ServiceCenterId VARCHAR(20)
			
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(20)')
		,@ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(20)')
FROM	@XmlDoc.nodes('RptPreBillingBe') as T(C)     


   ;WITH Results AS    
   (    
   SELECT     
    CD.GlobalAccountNumber    
    ,MIN(CR.PreviousReading) AS PreviousReading    
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name     
    ,CD.ClassName    
    ,OldAccountNo    
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
   FROM UDV_CustomerDescription CD    
   LEFT JOIN Tbl_CustomerReadings CR ON (CD.GlobalAccountNumber != CR.GlobalAccountNumber AND IsBilled=0)    
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId     
   AND CD.BU_ID = @BU_Id-----  
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
   GROUP BY CD.GlobalAccountNumber    
     ,CD.Title    
     ,CD.FirstName    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.ClassName    
     ,OldAccountNo    
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
   UNION     
   SELECT     
    CD.GlobalAccountNumber    
    ,MIN(CR.PreviousReading) AS PreviousReading    
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name     
    ,CD.ClassName    
    ,OldAccountNo    
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
   FROM UDV_CustomerDescription CD    
   LEFT JOIN Tbl_CustomerReadings CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber    
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId    
   AND CD.BU_ID = @BU_Id-----  
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
   WHERE CustomerTypeId = 1    
   GROUP BY CD.GlobalAccountNumber    
     ,CD.Title    
     ,CD.FirstName    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.ClassName    
     ,OldAccountNo    
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
   )    
       
   SELECT    
   (    
   SELECT * FROM Results    
    FOR XML PATH('PreBillingList'),TYPE    
   )       
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')    
  END  
GO
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- =============================================  
ALTER PROCEDURE [USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
     
   SELECT(  
     SELECT   --TCR.AccountNo  
        (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
       ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
       ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
       ,TCR.Remarks  
       ,TCR.CreatedBy  
       ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_LCustomerTariffChangeRequest  TCR  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
     SELECT  --CC.GlobalAccountNumber AS AccountNo
       (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
       ,CC.ApprovalStatusId  
       ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
       ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
       ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CC.Remarks  
       ,CC.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerTypeChangeLogs  CC  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId       
   FOR XML PATH('AuditTrayList'),TYPE  
   )  
   FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- =============================================  
ALTER PROCEDURE [USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
     SELECT   --CNL.GlobalAccountNumber AS AccountNo  
       (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
       ,CNL.Remarks  
       ,CNL.OldTitle  
       ,CNL.NewTitle  
       ,CNL.OldFirstName  
       ,CNL.NewFirstName  
       ,CNL.OldMiddleName  
       ,CNL.NewMiddleName  
       ,CNL.OldLastName  
       ,CNL.NewLastName  
       ,CNL.OldKnownAs  
       ,CNL.NewKnownAs  
       ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CNL.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerNameChangeLogs CNL  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END 
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
   SELECT(  
     SELECT  --CMI.AccountNo
             (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
         ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
       ,CMI.Remarks  
       ,CMI.OldDials  
       ,CMI.NewDials  
       ,CMI.CreatedBy  
       ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CMI.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
ALTER PROCEDURE [USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
   SELECT(  
     SELECT  --CCL.AccountNo 
        (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
       ,CCL.ApproveStatusId  
       ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
       ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
       ,CCL.Remarks  
       ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,CCL.CreatedBy  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerActiveStatusChangeLogs CCL   
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
    -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [USP_GetCustomerAddressChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
 IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END   
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
   SELECT(  
    SELECT    -- CA.GlobalAccountNumber 
      (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
     ,OldPostal_HouseNo  
     ,OldPostal_StreetName   
     ,OldPostal_City  
     ,OldPostal_Landmark  
     ,OldPostal_AreaCode  
     ,OldPostal_ZipCode  
     ,NewPostal_HouseNo  
     ,NewPostal_StreetName   
     ,NewPostal_City  
     ,NewPostal_Landmark  
     ,NewPostal_AreaCode  
     ,NewPostal_ZipCode   
     ,OldService_HouseNo  
     ,OldService_StreetName  
     ,OldService_City  
     ,OldService_Landmark  
     ,OldService_AreaCode  
     ,OldService_ZipCode   
     ,NewService_HouseNo   
     ,NewService_StreetName  
     ,NewService_City  
     ,NewService_Landmark  
     ,NewService_AreaCode  
     ,NewService_ZipCode  
     ,CA.Remarks  
     ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
     ,ApproveSttus.ApprovalStatus  
     ,CA.CreatedBy  
     ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
     FROM Tbl_CustomerAddressChangeLog_New CA   
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId       
   FOR XML PATH('AuditTrayAddressList'),TYPE  
   )  
   FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
    
END 
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @BU_ID VARCHAR(MAX)=''  
    ,@SU_ID VARCHAR(MAX)=''  
    ,@SC_ID VARCHAR(MAX)=''  
    ,@CycleId VARCHAR(MAX)=''  
    ,@TariffId VARCHAR(MAX)=''  
    ,@BookNo VARCHAR(MAX)=''  
    ,@FromDate VARCHAR(20)=''  
    ,@ToDate VARCHAR(20)=''  
    
  SELECT  
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
    ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
    ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
    ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
    ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
    ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
    ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
    ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
  IF (@FromDate ='' OR @FromDate IS NULL)  
  BEGIN  
   SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
   SET @Todate=dbo.fn_GetCurrentDateTime()  
  END 
  
  -- start By Karteek
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
    
  SELECT(  
     SELECT   --BookChange.AccountNo
	   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
       ,BookChange.OldBookNo  
       ,BookChange.NewBookNo  
       ,BookChange.Remarks  
       ,BookChange.CreatedBy  
       ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
       ,ApproveSttus.ApprovalStatus  
       ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
       ,CustomerView.BusinessUnitName  
       ,CustomerView.ServiceUnitName  
       ,CustomerView.ServiceCenterName  
       ,CustomerView.CycleName  
       ,CustomerView.BookCode  
       ,CustomerView.SortOrder AS CustomerSortOrder  
       ,CustomerView.BookSortOrder AS BookSortOrder  
       ,CustomerView.OldAccountNo  
     FROM Tbl_BookNoChangeLogs  BookChange  
     INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber  
     AND  CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
     -- changed by karteek start  
     --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
     --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
     --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
     --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
     --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
     --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
     INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
     INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
     INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
     INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
     INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
     INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
     -- changed by karteek end 
     LEFT JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId       
       
  FOR XML PATH('AuditTrayList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya
-- 
-- =============================================
ALTER PROCEDURE [USP_GetAccountWithoutMeter_New](@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(ServiceCenterId)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('ConsumerBe') AS T(C)

			Declare @Count INT
				
				SELECT
						   IDENTITY (int, 1,1) As   RowNumber						   
						  ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) as Name
						  --,CD.GlobalAccountNumber
						  --,CD.AccountNo	
						  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
						  ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
																,CD.Service_Landmark
																,CD.Service_City,'',
																CD.Service_ZipCode) AS [ServiceAddress]
						  ,CD.ClassName
						  ,CD.OldAccountNo
						  ,CD.SortOrder AS CustomerSortOrder
						  ,CD.BookCode AS BookNo
						  ,CD.BusinessUnitName
						  ,CD.ServiceUnitName
						  ,CD.ServiceCenterName	
						  INTO #CustomersList  
						  FROM [UDV_CustomerDescription](NOLOCK) CD
						  WHERE CD.ReadCodeId=1
						  AND (CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU,',')) OR @BU='')
						  AND (CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU,',')) OR @SU='')
						  AND (CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@SC,',')) OR @SC='')
						  AND (CD.TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,',')) OR @Tariff='')
						  
			SET @Count=(select count(0) from #CustomersList)				

			 SELECT  *
					,@Count As TotalRecords 
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  M.RamaDevi  
-- Create date: 12-04-2014
-- Author:  V.Bhimaraju
-- Create date: 07-08-2014
-- Modified By: T.Karthik
-- Modified Date: 29-10-2014
-- Description: To get Accounts without cycle  Customers
-- =============================================  
ALTER PROCEDURE [USP_GetAccountWithoutCycle_New]
(
	@xmlDoc xml
)
AS  
BEGIN  
		DECLARE    @BU_ID VARCHAR(MAX)   
				  ,@SU_ID VARCHAR(MAX)  
				  ,@ServiceCenterId VARCHAR(MAX)  
				  ,@Tariff VARCHAR(MAX)  
				  ,@PageNo INT  
				  ,@PageSize INT
				    
		SELECT	 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
				,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
				,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
				,@Tariff=C.value('(Tariff)[1]','VARCHAR(MAX)')  
				,@PageNo = C.value('(PageNo)[1]','INT')  
				,@PageSize = C.value('(PageSize)[1]','INT')   
		FROM @xmlDoc.nodes('ConsumerBe') AS T(C)  
  

				 		
						 		  
			SELECT	 OldAccountNo
					,(AccountNo+' - '+GlobalAccountNumber) AS GlobalAccountNumber
					,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) as FullName
					,dbo.fn_GetCustomerServiceAddress_New(Cd.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode)   as [ServiceAdress]
					 ,CD.ClassName
					 ,CD.BookNo
					 ,CD.ConnectionDate
					 ,IDENTITY (int, 1,1) As   RowNumber
					 ,CD.BusinessUnitName
					 ,CD.ServiceUnitName
					 ,CD.ServiceCenterName
					 ,CD.SortOrder AS CustomerSortOrder
			INTO #CustomersList
			FROM [UDV_RptCustomerDesc] CD
			WHERE CycleId IS NULL
			--AND CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID,','))
			--AND CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU_ID,','))
			--AND CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@ServiceCenterId,','))
			AND (CD.TariffId IN(SELECT [com] FROM dbo.fn_Split(@Tariff,',')) OR @Tariff='')


			Declare @TotlaRecords INT

			SET	   @TotlaRecords = (select COUNT(0) from  #CustomersList)

			Select	RowNumber
					,GlobalAccountNumber AS AccountNo
					,ISNULL(OldAccountNo,'--') AS OldAccountNo
					,FullName   as Name
					,ServiceAdress as ServiceAddress
					,ClassName
					,BookNo
					,BusinessUnitName
					,ServiceUnitName
					,ServiceCenterName
					,CustomerSortOrder
					,ISNULL(CONVERT(VARCHAR(20),ConnectionDate,106),'--') AS ConnectionDate
					,@TotlaRecords As TotalRecords	 from #CustomersList
			  Where  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 

			--Select * from #CustomersList
			DROP TABLE	#CustomersList
		 
				   
END  
GO

GO
CREATE TABLE Tbl_CustomerBillEnergyCharges( CusotmerNewBillID bigint identity(1,1), FromKW int,ToKW Int,Amount decimal(18,2))
GO
ALTER TABLE Tbl_CustomerBills ADD TariffRates VARCHAR(100)
GO
--------------------------------------------------------------------------------------------------------------------------------------

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 03/24/2015 00:09:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(50),CB.PaymentLastDate) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(50),CB.PaymentLastDate) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),BD.ReadDate),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(50), CB.PaymentLastDate),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'--') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,TariffRates As EnergyCharges
    FROM Tbl_CustomerBills CB    
    INNER JOIN Tbl_BillDetails BD ON CB.CustomerBillId = BD.CustomerBillID 
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
GO
-- =============================================          
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <25-MAR-2014>          
-- Description: Customer Payment from file upload   
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <24-MAR-2015>          
-- Description: Romove join to check customer bill in process
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]          
 (  
@XmlDoc XML  
,@MultiXmlDoc XML  
 )          
AS          
BEGIN          
          
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)     
        ,ReceivedDate DATETIME,PaymentModeId INT)    
 DECLARE @Bu_Id VARCHAR(50)  
   
 SELECT   
 @Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)      
         
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)      
 SELECT             
  c.value('(SNO)[1]','INT')                         
 ,c.value('(AccountNo)[1]','VARCHAR(50)')  
 ,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                         
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                         
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)    
    ,c.value('(PaymentMode)[1]','INT')    
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)        
          
  SELECT    
  (    
   SELECT     
   ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
   ,CD.GlobalAccountNumber  AS AccountNo  
  , CD.OldAccountNo AS OldAccountNo  
     ,TempDetails.AmountPaid AS PaidAmount    
     ,TempDetails.ReceiptNO    
     ,TempDetails.ReceivedDate   
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
  ,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid, TempDetails.ReceiptNO) AS IsDuplicate  
  ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists    
  ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId)   
    ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentModeId=PM.PaymentModeId) END) AS PaymentMode    
  ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE 1 END) AS PaymentModeId    
  --,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers     
  --  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess    
 -- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills     
   FROM @TempCustomer AS TempDetails     
    INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo  
  LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentModeId    
 FOR XML PATH('PaymentsList'),TYPE      
 )    
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')        
END   
------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateBillingQueueDetails]    Script Date: 03/24/2015 18:03:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 08 JULY 2014
-- Description  : To Update Bill Generation Details
-- Modified By  : Padmini
-- Modified Date : 26-12-2014  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBillingQueueDetails]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @BillingQueueScheduleId INT
			,@BillingMonth INT
			,@BillingYear INT
			,@BillSendingMode INT
			,@BillProcessTypeId INT
			--,@FeederId VARCHAR(50)
			,@CycleId VARCHAR(50)
			,@CreatedBy VARCHAR(50)
			,@CycleIds VARCHAR(MAX)
			,@Count INT = 0
			
	SELECT   
		@BillingMonth = C.value('(BillingMonth)[1]','INT')
		,@BillingYear = C.value('(BillingYear)[1]','INT')
		,@BillSendingMode = C.value('(BillSendingMode)[1]','INT')
		,@BillProcessTypeId = C.value('(BillProcessTypeId)[1]','INT')
		--,@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)		
	
	DECLARE @CycleTable TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))
	
	INSERT INTO @CycleTable
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
	
	SELECT TOP(1) @CycleId = CycleId FROM @CycleTable ORDER BY CycleId ASC    
     
	WHILE(EXISTS(SELECT TOP(1) CycleId FROM @CycleTable WHERE CycleId >= @CycleId ORDER BY CycleId ASC))    
		BEGIN 
			
			SET @BillingQueueScheduleId = 0
			SELECT @BillingQueueScheduleId = BillingQueueScheduleId FROM Tbl_BillingQueueSchedule WHERE BillingMonth = @BillingMonth
			AND BillingYear = @BillingYear
			AND BillProcessTypeId = @BillProcessTypeId
			AND CycleId = @CycleId
			--AND((CycleId = @CycleId AND ISNULL(FeederId,'') = '') OR (ISNULL(CycleId,'') = '' AND FeederId = @FeederId))
						
			IF(@BillingQueueScheduleId = 0)
				BEGIN
					INSERT INTO Tbl_BillingQueueSchedule 
						  (
							BillingMonth
							,BillingYear
							,BillSendingMode
							,BillProcessTypeId
							,TotalCustomersInQueue
							,ServiceStartDate
							--,FeederId
							,CycleId
							,BillGenarationStatusId
							,OpenStatusId
							,CreatedBy
							,CreatedOn
						  )
					VALUES( @BillingMonth
							,@BillingYear
							,@BillSendingMode
							,@BillProcessTypeId
							,(select dbo.fn_GetNumOfBillCustomersInQueue())
							,(select dbo.fn_GetBillServiceTime())
							--,NULL
							,@CycleId
							,5
							,1
							,@CreatedBy
							,(select dbo.fn_GetCurrentDateTime())
							)
					SET @BillingQueueScheduleId = SCOPE_IDENTITY()
				END
			ELSE
				BEGIN	        
				   Update Tbl_BillingQueueSchedule   
					SET ServiceStartDate = (SELECT dbo.fn_GetBillServiceTime())  
					 ,ModifiedDate = (SELECT dbo.fn_GetCurrentDateTime())  
					 ,ModifiedBy =  @CreatedBy  
					 ,BillSendingMode = @BillSendingMode  
					 ,OpenStatusId = 1
					 ,BillGenarationStatusId=5  
				   WHERE BillingQueueScheduleId =  @BillingQueueScheduleId
				END  
			--AND((CycleId = @CycleId AND ISNULL(FeederId,'') = '') OR (ISNULL(CycleId,'') = '' AND FeederId = @FeederId))
			
			IF EXISTS(SELECT 0 FROM dbo.fn_GetAccountNumbers_ByCycleFeeder(@CycleId) WHERE AccountNo NOT IN(SELECT AccountNo FROM Tbl_BillingQueeCustomers WHERE BillingQueuescheduleId = @BillingQueueScheduleId))
				BEGIN
					INSERT INTO Tbl_BillingQueeCustomers(BillingQueuescheduleId,AccountNo,[Month],[Year],BillGenarationStatusId)
					SELECT 
						@BillingQueueScheduleId
						,AccountNo 
						,@BillingMonth
						,@BillingYear
						,5
					FROM dbo.fn_GetAccountNumbers_ByCycleFeeder(@CycleId)
					WHERE AccountNo NOT IN(SELECT AccountNo FROM Tbl_BillingQueeCustomers WHERE BillingQueuescheduleId = @BillingQueueScheduleId)
					--AND AccountNo NOT IN(SELECT AccountNo FROM Tbl_CustomerDetails WHERE MeterTypeId = 1)
				END
			
			SET @Count = @Count + 1
			
		  IF(@CycleId = (SELECT TOP(1) CycleId FROM @CycleTable ORDER BY CycleId DESC))    
			   BREAK    
		  ELSE    
			   BEGIN    
					SET @CycleId = (SELECT TOP(1) CycleId FROM @CycleTable WHERE  CycleId > @CycleId ORDER BY CycleId ASC)    
					IF(@CycleId IS NULL) break;    
					  Continue    
			   END    
	    END     	
			
	SELECT 
		@Count AS TotalRecords
	FOR XML PATH ('BillGenerationBe'),TYPE
	
	
		
	--SELECT 
	--	TOP(1) BillingQueueScheduleId
	--FROM Tbl_BillingQueueSchedule	
	--WHERE BillingMonth = @BillingMonth
	--AND BillingYear = @BillingYear
	--AND BillProcessTypeId = @BillProcessTypeId
	--AND((CycleId = @CycleId AND ISNULL(FeederId,'') = '') OR (ISNULL(CycleId,'') = '' AND FeederId = @FeederId))
	--ORDER BY BillingQueueScheduleId DESC
	--FOR XML PATH ('BillGenerationBe'),TYPE
		
END
GO
