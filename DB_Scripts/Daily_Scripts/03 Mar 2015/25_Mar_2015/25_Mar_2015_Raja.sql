GO
CREATE TABLE MASTERS.Tbl_MMeterReadingsFrom
(
	 MeterReadingFromId INT IDENTITY(1,1) PRIMARY KEY
	,MeterReadingFrom VARCHAR(500)
)

GO
INSERT INTO MASTERS.Tbl_MMeterReadingsFrom(MeterReadingFrom)VALUES('AccountWise')
GO
INSERT INTO MASTERS.Tbl_MMeterReadingsFrom(MeterReadingFrom)VALUES('BookWise')
GO
INSERT INTO MASTERS.Tbl_MMeterReadingsFrom(MeterReadingFrom)VALUES('RouteWise')
GO
INSERT INTO MASTERS.Tbl_MMeterReadingsFrom(MeterReadingFrom)VALUES('BulkUpload')
GO
INSERT INTO MASTERS.Tbl_MMeterReadingsFrom(MeterReadingFrom)VALUES('MobileUpload')
GO
ALTER TABLE Tbl_CustomerReadings
ADD MeterReadingFrom INT
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	
;With FinalResult AS  
(  
	SELECT Temp.MinReading
		  ,ISNULL(CD.ActiveStatusId,0) AS ActiveStatusId
		  ,CD.OldAccountNo
		  --,CD.GlobalAccountNumber AS AccNum
		  ,Temp.AccountNo AS AccNum
	FROM @TempCustomer AS Temp
	LEFT JOIN UDV_CustomerDescription CD ON (CD.GlobalAccountNumber=Temp.AccountNo 
												OR Temp.AccountNo=CD.OldAccountNo)
)

 SELECT  
  (       
  SELECT   
        AccNum
        ,OldAccountNo
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')


--;With FinalResult AS  
--(  
--SELECT     
--	CD.GlobalAccountNumber as AccNum
--	,CD.OldAccountNo AS OldAccountNo
--	,T1.MinReading
--	,CD.ActiveStatusId AS ActiveStatusId
--	--,(SELECT ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=T1.AccountNo OR OldAccountNo=T1.AccountNo) AS ActiveStatusId
-- FROM @TempCustomer T1
--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON  T1.AccountNo=CD.GlobalAccountNumber OR  T1.AccountNo=CD.OldAccountNo
-- LEFT JOIN( SELECT  C.AccountNo AS AccNum
--	,ActiveStatusId    
--    FROM [CUSTOMERS].[Tbl_CustomerSDetail] C     
--     ) B ON T1.AccountNo=B.AccNum    
--)  
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Bhimaraju V
-- Create date: 26-Feb-2015
-- Description: AverageReading details are insertion if exists updation for those customers
-- =============================================
ALTER PROCEDURE [USP_InsertDirectCustomerBillReading]
	(
	@XmlDoc XML
	)
AS
BEGIN

		DECLARE @TempCustomer TABLE(AccNum VARCHAR(50),     
                              AverageReading VARCHAR(50),   
                             CreatedBy VARCHAR(50))
	 INSERT INTO @TempCustomer(AccNum ,AverageReading,CreatedBy)     
	 SELECT     
	   c.value('(AccNum)[1]','VARCHAR(50)')     
	  ,c.value('(AverageReading)[1]','VARCHAR(50)')     
	  ,c.value('(CreatedBy)[1]','VARCHAR(50)') 
	 FROM @XmlDoc.nodes('//NewDataSet/Table1') AS T(c)
	 
	 
	 UPDATE DC 
		SET DC.AverageReading=Tc.AverageReading
			,DC.ModifiedBy=TC.CreatedBy
			,DC.ModifiedDate=dbo.fn_GetCurrentDateTime()
	 FROM Tbl_DirectCustomersAvgReadings DC
	 INNER JOIN @TempCustomer TC ON TC.AccNum = DC.GlobalAccountNumber
	 WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM @TempCustomer)
	 
	 
	 INSERT INTO Tbl_DirectCustomersAvgReadings
					(
						 GlobalAccountNumber
						,AverageReading
						,CreatedBy
						,CreatedDate
					)
			SELECT		 AccNum
						,AverageReading
						,CreatedBy
						,dbo.fn_GetCurrentDateTime()
			FROM @TempCustomer AS TC
		WHERE TC.AccNum NOT IN (SELECT DISTINCT GlobalAccountNumber 
				FROM Tbl_DirectCustomersAvgReadings DC
				INNER JOIN @TempCustomer TC1 ON TC1.AccNum <> DC.GlobalAccountNumber)
	
	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

		
	--DECLARE @MinReading varchar(50)
	--	  ,@CreateBy varchar(50)  
	--	  ,@AccNum varchar(50)
	  
	-- SELECT    @MinReading=C.value('(AverageReading)[1]','VARCHAR(50)')
	--		  ,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')  
	--		  ,@AccNum=C.value('(AccNum)[1]','varchar(50)')  
	-- FROM @XmlDoc.nodes('BillingBE') AS T(C)
	 
	 
	-- IF EXISTS(SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@AccNum)
	--	BEGIN
	--		UPDATE Tbl_DirectCustomersAvgReadings
	--		SET AverageReading=@MinReading
	--			,ModifiedBy=@CreateBy
	--			,ModifiedDate=dbo.fn_GetCurrentDateTime()
	--		WHERE GlobalAccountNumber=@AccNum
	--		SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')			
	--	END
	--ELSE
	--	BEGIN
	--		INSERT INTO Tbl_DirectCustomersAvgReadings
	--				(
	--					 GlobalAccountNumber
	--					,AverageReading
	--					,CreatedBy
	--					,CreatedDate
	--				)
	--		VALUES
	--				(
	--					@AccNum
	--					,@MinReading
	--					,@CreateBy
	--					,dbo.fn_GetCurrentDateTime()
	--				)
	--		SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
	--	END								
END

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  :RamaDevi M  
-- Create date: 17-04-2014    
-- Description: Insert MeterReadings Through excel 
-- =============================================    
ALTER PROCEDURE [USP_InsertMeterReadingFromExcel]
 (    
@MultiXmlDoc XML    
 )    
AS    
BEGIN    
 
 DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),     
                              CurrentReading VARCHAR(50),   
                             ReadDate DATETIME)      
       
 
 INSERT INTO @TempCustomer(AccountNo ,CurrentReading,ReadDate)     
 SELECT     
   c.value('(AccNum)[1]','VARCHAR(50)')     
  ,c.value('(PresentReading)[1]','VARCHAR(50)')     
  --,c.value('(ReadDate)[1]','VARCHAR(50)')    
  ,c.value('(ReadingDate)[1]','DATETIME')    
 
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)      
    
  ;WITH Result AS
  (
  SELECT 
		--C.CustomerUniqueNo
         C.GlobalAccountNumber
        ,T.ReadDate
        ,(SELECT TOP(1) ISNULL(PresentReading,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=C.GlobalAccountNumber AND 
										CONVERT(DATE,ReadDate )<=CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc) AS PreviousReading
		,CONVERT(VARCHAR(50),T.CurrentReading)AS PresentReading
        ,(SELECT dbo.fn_GetCustomerAverageReading(C.GlobalAccountNumber,T.CurrentReading,T.ReadDate))  AS AverageReading
     --,CONVERT(VARCHAR(50),(CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=C.CustomerUniqueNo AND 
								--		CONVERT(DATE,ReadDate )<=CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc)IS NULL
								-- THEN '0' 
								-- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=C.CustomerUniqueNo AND 
								--		CONVERT(DATE,ReadDate )<=CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc)
								-- END)) AS PreviousReading
     --    ,CONVERT(VARCHAR(50),((SELECT  case when SUM( Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=C.CustomerUniqueNo)+(T.CurrentReading-(CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=C.CustomerUniqueNo AND 
				--CONVERT(DATE,ReadDate )<CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc)IS NULL
				--THEN 0 ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=C.CustomerUniqueNo AND 
				--CONVERT(DATE,ReadDate )<CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc)END)))/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=C.CustomerUniqueNo)+1)) AS AverageReading		
		,((SELECT  case when SUM( Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=C.GlobalAccountNumber)+(T.CurrentReading-(CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=C.GlobalAccountNumber AND 
					CONVERT(DATE,ReadDate )<CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc)IS NULL
					THEN 0 ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=C.GlobalAccountNumber AND 
					CONVERT(DATE,ReadDate )<CONVERT(DATE, T.ReadDate)  order by CustomerReadingId desc)END))) AS TotalReadingEnergies
		, ((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=C.GlobalAccountNumber)+1) AS TotalReadings
		,dbo.fn_GetCurrentDateTime() AS CreatedDate   
   FROM @TempCustomer T 
   JOIN [UDV_CustomerDescription] C ON T.AccountNo=C.GlobalAccountNumber
  )  
     
    
  INSERT INTO Tbl_CustomerReadings (    
         --CustomerUniqueNo    
         AccountNumber
        ,ReadDate    
        ,PreviousReading    
        ,PresentReading 
        ,Usage
        ,AverageReading   
        ,TotalReadingEnergies    
        ,TotalReadings    
      ,CreatedDate
      ,MeterReadingFrom
        )    
   SELECT 
	 --CustomerUniqueNo
	  GlobalAccountNumber 
     ,ReadDate    
      ,PreviousReading    
      ,PresentReading 
      ,(CONVERT(INT,PresentReading)- CONVERT(INT,PreviousReading))
      ,AverageReading   
      ,TotalReadingEnergies    
      ,TotalReadings    
      ,CreatedDate
      ,4--Bulk Upload MASTERS.Tbl_MMeterReadingsFrom --ID065-Raja
   FROM Result

  SELECT    
   @@ROWCOUNT   AS TotalRecords     
  FOR XML PATH('BillingBE'),TYPE     
     
END
GO
