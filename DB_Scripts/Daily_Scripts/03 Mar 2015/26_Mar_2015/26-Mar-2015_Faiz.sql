
GO 
Update [MASTERS].[Tbl_ControlRefMaster]  
SET ActiveStatusId = 1
WHERE ControlTypeId=1 AND ControlTypeName='TextBox'
GO
Update [MASTERS].[Tbl_ControlRefMaster]  
SET ActiveStatusId = 1
WHERE ControlTypeId=2 AND ControlTypeName='DropDownList'
GO
--------------------------------------------------------------------------------------------------------
GO
-- =============================================      
-- Author:  <Author,Ramaraju>      
-- Create date: <Create Date,03-02-2014>      
-- Description: <Description,GetCustomerSubType Details>    
-- ModifiedBy: Bhimaraju Vanka    
-- Create date: 27-Aug-2014    
-- Description: Modification is for getting remaining Tariffsubtypes without Customer having tariff id     
-- Create date: 27-Jan-2015    
-- Description: Deactivated category list belonging subcategories also we should allow to display    
-- ModifiedBy: Faiz  
-- Modified date: 26-Mar-2015 
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetCustomerSubTypes]      
(      
@XmlDoc xml=null      
)      
AS      
BEGIN      
  Declare @ClassId INT,@SubType VARCHAR(50)      
       
 SELECT    
     @ClassId=C.value('(ClassId)[1]','INT')    
     ,@SubType=C.value('(CustomerSubTypeNames)[1]','VARCHAR(50)')    
     FROM @XmlDoc.nodes('BillCalculatorBE')AS T(C)      
  IF(@SubType!='')    
 BEGIN    
  SELECT      
   (      
    SELECT ClassID AS ClassId      
     ,ClassName as CustomerSubTypeNames      
    FROM Tbl_MTariffClasses       
    WHERE RefClassID=(CASE @ClassId WHEN 0 THEN RefClassID ELSE @ClassId END)      
    AND isActiveclass=1    
    --AND ClassName NOT IN (@SubType) --commented as it was not allowing to change customer cluster type in Change Customer Tariff page   Faiz-ID103
   FOR XML PATH('BillCalculatorBE'),TYPE      
   )      
   FOR XML PATH(''),Root('BillCalculatorInfoByXml')    
 END    
 ELSE IF (@ClassId !='')    
 BEGIN    
   SELECT(    
   SELECT    
     SC.ClassName as CustomerSubTypeNames    
    ,SC.ClassID AS ClassId    
   FROM Tbl_MTariffClasses C    
   JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
   AND SC.IsActiveClass =1    
   AND C.IsActiveClass=1    
   AND C.ClassID=@ClassId --OR @ClassId=0    
   FOR XML PATH('BillCalculatorBE'),TYPE      
   )      
   FOR XML PATH(''),Root('BillCalculatorInfoByXml')    
 END    
 ELSE    
 BEGIN    
   SELECT(    
   SELECT    
     SC.ClassName as CustomerSubTypeNames    
    ,SC.ClassID AS ClassId    
   FROM Tbl_MTariffClasses C    
   JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
   AND SC.IsActiveClass =1    
   AND C.IsActiveClass=1    
   FOR XML PATH('BillCalculatorBE'),TYPE      
   )      
   FOR XML PATH(''),Root('BillCalculatorInfoByXml')    
 END     
END  
GO
-----------------------------------------------------------------------------------------------------------

GO
-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 29-09-2014      
-- Modified By: T.Karthik  
-- Modified Date: 27-12-2014  
-- Modified By: M.Padmini  
-- Modified Date: 21-01-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Bhimaraju.V  
-- Modified Date: 04-02-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- =============================================  
ALTER PROCEDURE [USP_ChangeCustomerName]  
 (      
  @XmlDoc xml      
 )  
AS      
BEGIN  
       
 DECLARE  @AccountNo VARCHAR(50)  
         ,@GlobalAccountNumber VARCHAR(50)  
   ,@NewTitle VARCHAR(10)  
   ,@NewFirstName VARCHAR(50)  
   ,@NewMiddleName VARCHAR(50)  
   ,@NewLastName VARCHAR(50)  
   ,@NewKnownAs VARCHAR(150)  
   ,@ModifiedBy VARCHAR(50)  
   ,@Details VARCHAR(MAX)  
   ,@ApprovalStatusId INT  
    
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
        ,@GlobalAccountNumber=C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
  ,@NewTitle=C.value('(Title)[1]','VARCHAR(10)')  
  ,@NewFirstName=C.value('(FirstName)[1]','VARCHAR(50)')  
  ,@NewMiddleName=C.value('(MiddleName)[1]','VARCHAR(50)')  
  ,@NewLastName=C.value('(LastName)[1]','VARCHAR(50)')  
  ,@NewKnownAs=C.value('(KnownAs)[1]','VARCHAR(150)')  
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
  WHERE GlobalAccountNumber=@GlobalAccountNumber  
  AND ApproveStatusId NOT IN (2,3)) = 0)  
  BEGIN  
  INSERT INTO Tbl_CustomerNameChangeLogs(  
       GlobalAccountNumber  
   ,AccountNo  
   ,OldTitle  
   ,OldFirstName  
   ,OldMiddleName  
   ,OldLastName  
   ,OldKnownas  
   ,NewTitle  
   ,NewFirstName  
   ,NewMiddleName  
   ,NewLastName  
   ,NewKnownas  
   ,CreatedBy  
   ,CreatedDate  
   ,ApproveStatusId  
   ,Remarks)  
     
 SELECT @GlobalAccountNumber  
    ,AccountNo  
    ,CASE Title WHEN '' THEN NULL ELSE Title END   -- title was missing --Faiz-ID103 
    ,CASE FirstName WHEN '' THEN NULL ELSE FirstName END   
    ,CASE MiddleName WHEN '' THEN NULL ELSE MiddleName END  
    ,CASE LastName WHEN '' THEN NULL ELSE LastName END    
    ,CASE Knownas WHEN '' THEN NULL ELSE Knownas END    
    ,CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
    ,CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
    ,CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END   
    ,CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END     
    ,CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime()  
    ,@ApprovalStatusId  
    ,@Details  
 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber  
    
  IF(@ApprovalStatusId = 2)--If Approved  
  BEGIN  
   UPDATE [CUSTOMERS].[Tbl_CustomerSDetail] SET  
   Title=CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
   ,FirstName=CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
   ,MiddleName=CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END  
   ,LastName=CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END  
   ,KnownAs=CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
   ,ModifedBy=@ModifiedBy  
   ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
   WHERE GlobalAccountNumber=@GlobalAccountNumber    
   SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
  END  
 ELSE  
  BEGIN  
   SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
  END  
 END  
 ELSE  
 BEGIN  
  SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
 END  
    
 END  
  
----------------------------------------------------------------------------------------------------------------