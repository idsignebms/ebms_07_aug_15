GO
CREATE TABLE Tbl_EditListReportSummary
(
	PageId INT IDENTITY(1,1),
	ReportCode VARCHAR(50), 
	ReportName VARCHAR(100), 
	PagePath VARCHAR(500), 
	PageOrder INT, 
	IsActive BIT DEFAULT 1, 
	CreatedBy VARCHAR(50), 
	CreatedDate DATETIME, 
	ModifiedBy VARCHAR(50), 
	ModifiedDate DATETIME
)
GO
INSERT INTO Tbl_EditListReportSummary(ReportCode,ReportName,PagePath,PageOrder,CreatedBy,CreatedDate)
VALUES('ELR-A001','Meter Reding Report','../Reports/MeterReadingReport.aspx',1,'admin',GETDATE())
GO
INSERT INTO Tbl_EditListReportSummary(ReportCode,ReportName,PagePath,PageOrder,CreatedBy,CreatedDate)
VALUES('ELR-A002','Average Upload Report','../Reports/AverageUploadReport.aspx',2,'admin',GETDATE())
GO
INSERT INTO Tbl_EditListReportSummary(ReportCode,ReportName,PagePath,PageOrder,CreatedBy,CreatedDate)
VALUES('ELR-A003','Payments Report','../Reports/PaymentEditListReport.aspx',3,'admin',GETDATE())
GO
INSERT INTO Tbl_EditListReportSummary(ReportCode,ReportName,PagePath,PageOrder,CreatedBy,CreatedDate)
VALUES('ELR-A004','Adjustment Report','../Reports/AdjustmentReport.aspx',4,'admin',GETDATE())
GO
---------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
CREATE PROCEDURE USP_GetMeterReadingsReport
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, U.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Users,',')) BU ON BU.BU_ID = CR.CreatedBy
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@TrxTypes,',')) SU ON SU.SU_Id = CR.MeterReadingFrom 
	  
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 26 Mar 2015  
-- Description: To get the Users from Tbl_DirectCustomersAvgReadings for Average Upload report  
-- =============================================  
CREATE PROCEDURE USP_GetUsersToAverageUploadReport  
   
AS  
BEGIN  
   
 SELECT  
 (  
  SELECT UserId, Name   
  FROM Tbl_UserDetails   
  WHERE UserId IN (SELECT DISTINCT CreatedBy FROM Tbl_DirectCustomersAvgReadings)  
  ORDER BY Name  
  FOR XML PATH('UserManagementBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
CREATE PROCEDURE USP_GetAverageUploadReport
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
	INTO #CustomerReadingsList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Users,',')) BU ON BU.BU_ID = CR.CreatedBy
	  
	SELECT
	(
		SELECT RowNumber
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 26 Mar 2015  
-- Description: To get the Users from Tbl_CustomerPayments for Payments report  
-- =============================================  
CREATE PROCEDURE USP_GetUsersToPaymentsReport  
   
AS  
BEGIN  
   
 SELECT  
 (  
  SELECT UserId, Name   
  FROM Tbl_UserDetails   
  WHERE UserId IN (SELECT DISTINCT CreatedBy FROM Tbl_CustomerPayments)  
  ORDER BY Name  
  FOR XML PATH('UserManagementBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
CREATE PROCEDURE USP_GetPaymentEditListReport
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	--IF(@TrxFrom = '')
	--	BEGIN
	--		SELECT @TrxFrom = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
	--				 FROM MASTERS.Tbl_MMeterReadingsFrom 
	--				 FOR XML PATH(''), TYPE)
	--				.value('.','NVARCHAR(MAX)'),1,1,'')
	--	END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, 'Upload' AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Users,',')) BU ON BU.BU_ID = CR.CreatedBy
    --INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.SU_Id = CR.MeterReadingFrom 
	  
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 26 Mar 2015  
-- Description: To get the List of Edit List Reports summary 
-- =============================================  
CREATE PROCEDURE USP_GetEditListReportSummary
   
AS  
BEGIN  
   
 SELECT  
 (  
  SELECT ReportCode,
		ReportName,
		PagePath
  FROM Tbl_EditListReportSummary   
  WHERE IsActive = 1
  ORDER BY PageOrder  
  FOR XML PATH('EditListReportsBEList'),TYPE  
 )  
 FOR XML PATH(''),ROOT('EditListReportsBEInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 26 Mar 2015  
-- Description: To get the Users from Tbl_BillAdjustments for Adjustment report  
-- =============================================  
CREATE PROCEDURE USP_GetUsersToAdjustmentReport  
   
AS  
BEGIN  
   
 SELECT  
 (  
  SELECT UserId, Name   
  FROM Tbl_UserDetails   
  WHERE UserId IN (SELECT DISTINCT ApprovedBy FROM Tbl_BillAdjustments)  
  ORDER BY Name  
  FOR XML PATH('UserManagementBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 26 Mar 2015  
-- Description: To get the Adjustment Types for Adjustment report  
-- =============================================  
CREATE PROCEDURE USP_GetAdjustmentTypes
   
AS  
BEGIN  
   
 SELECT  
 (  
	  SELECT BATID AS AdjustmentId, Name As AdjustmentName
	  FROM Tbl_BillAdjustmentType   
	  WHERE [Status] = 1  
	  ORDER BY Name  
  FOR XML PATH('AdjustmentEditList'),TYPE  
 )  
 FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
CREATE PROCEDURE USP_GetAdjustmentEditListReport
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
		, TotalAmountEffected
		, MR.Name AS AdjustmentName
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Users,',')) BU ON BU.BU_ID = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@AdjustmentTypes,',')) SU ON SU.SU_Id = CR.BillAdjustmentType 
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,AdjustmentName
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------
