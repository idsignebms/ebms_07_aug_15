GO
-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 15-Nov-2014  
-- Description: This is the proc about search masters using flag  
-- MODIFIED BY : NEERAJ KANOJIYA  
-- MODIFIED DATE: 17-NOV-2014  
-- Modified By: Padmini  
-- Modified Date:17-Feb-2015  
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order  
-- Modified By: Faiz  
-- Modified Date:27-Mar-2015
-- Description: Modified BookNo and Id for flag-5 for book no search
-- =============================================  
ALTER PROCEDURE [dbo].[USP_MasterSearch]  
 (@XmlDoc Xml=null)  
AS  
BEGIN  
 DECLARE  @BusinessUnit VARCHAR(50)  
   ,@BusinessUnitCode VARCHAR(50)  
   ,@ServiceUnit VARCHAR(50)  
   ,@ServiceUnitCode VARCHAR(50)  
   ,@ServiceCenter VARCHAR(50)  
   ,@ServiceCenterCode VARCHAR(50)  
   ,@CycleName VARCHAR(50)  
   ,@CycleCode VARCHAR(50)  
   ,@BookNo VARCHAR(50)  
   ,@BookCode VARCHAR(50)  
   ,@RouteName VARCHAR(50)  
   ,@RouteCode VARCHAR(50)  
   ,@MeterNo VARCHAR(50)  
   ,@MeterSerialNo VARCHAR(50)  
   ,@Brand VARCHAR(50)  
   ,@Name VARCHAR(50)  
   ,@ContactNo VARCHAR(50)  
   ,@RoleId INT  
   ,@DesignationId INT  
   ,@PageNo INT  
   ,@PageSize INT  
   ,@Flag INT  
   ,@BU_ID  VARCHAR(50)  
     
 SELECT   @BusinessUnit=C.value('(BusinessUnit)[1]','VARCHAR(50)')  
   ,@BusinessUnitCode=C.value('(BusinessUnitCode)[1]','VARCHAR(50)')  
   ,@ServiceUnit=C.value('(ServiceUnit)[1]','VARCHAR(50)')  
   ,@ServiceUnitCode=C.value('(ServiceUnitCode)[1]','VARCHAR(50)')  
   ,@ServiceCenter=C.value('(ServiceCenter)[1]','VARCHAR(50)')  
   ,@ServiceCenterCode=C.value('(ServiceCenterCode)[1]','VARCHAR(50)')  
   ,@CycleName=C.value('(CycleName)[1]','VARCHAR(50)')  
   ,@CycleCode=C.value('(CycleCode)[1]','VARCHAR(50)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')  
   ,@BookCode=C.value('(BookCode)[1]','VARCHAR(50)')  
   ,@RouteName=C.value('(RouteName)[1]','VARCHAR(50)')  
   ,@RouteCode=C.value('(RouteCode)[1]','VARCHAR(50)')  
   ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')  
   ,@MeterSerialNo=C.value('(MeterSerialNo)[1]','VARCHAR(50)')  
   ,@Brand=C.value('(Brand)[1]','VARCHAR(50)')  
   ,@Name=C.value('(Name)[1]','VARCHAR(50)')  
   ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(50)')  
   ,@RoleId=C.value('(RoleId)[1]','INT')  
   ,@DesignationId=C.value('(DesignationId)[1]','INT')  
   ,@PageNo=C.value('(PageNo)[1]','INT')  
   ,@PageSize=C.value('(PageSize)[1]','INT')  
   ,@Flag=C.value('(Flag)[1]','INT')     
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')     
  FROM @XmlDoc.nodes('MasterSearchBE') as T(C)  
    
  --============================== ** Note ** ==========================  
  --@Flag = 1 --Bussiness Unit Search  
  --@Flag = 2 --Service Unit Search  
  --@Flag = 3 --Service Center Search  
  --@Flag = 4 --Cycle Search  
  --@Flag = 5 --Book Search  
  --@Flag = 6 --Route Management Search  
  --@Flag = 7 --Meter Information Search  
  --@Flag = 8 --User Search  
  --@Flag = 9 --Change Customer BU  
  --============================== ** Note ** ==========================  
    
  IF (@Flag = 1)  
  BEGIN  
   ;WITH PagedResults AS  
  (  
    SELECT B.BU_ID  
      ,B.BusinessUnitName  
      ,ROW_NUMBER() OVER(ORDER BY B.BU_ID ASC) AS RowNumber  
      ,ISNULL(B.BUCode,'-') AS BusinessUnitCode  
      ,B.Notes  
      ,(S.StateName +' ( '+ S.StateCode +' )')  AS StateName  
      ,S.StateCode  
    FROM Tbl_BussinessUnits B  
    LEFT JOIN Tbl_States S ON S.StateCode=B.StateCode  
    WHERE   
    (ISNULL(BusinessUnitName,'') like '%'+@BusinessUnit+'%' OR @BusinessUnit='')  
    AND (ISNULL(BUCode,'') like '%'+@BusinessUnitCode+'%' OR @BusinessUnitCode='')  
    --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)  
    AND (B.BU_ID=@BU_ID OR @BU_ID='')  
  )    
   SELECT    
    *      
    ,(Select COUNT(0) from PagedResults) as TotalRecords  
    FROM PagedResults p  
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  END  
  ELSE IF(@Flag = 2)  
  BEGIN  
  ;WITH PagedResults AS  
  (  
   SELECT  --S.SU_ID  
     S.ServiceUnitName  
     ,ROW_NUMBER() OVER(ORDER BY S.SU_ID ASC) AS RowNumber  
     ,ISNULL(S.SUCode,'--') AS ServiceUnitCode  
     ,S.Notes  
     ,B.BusinessUnitName  
     ,B.StateCode  
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName  
   FROM Tbl_ServiceUnits S  
   LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode  
   WHERE (ISNULL(ServiceUnitName,'') like '%'+@ServiceUnit+'%' OR @ServiceUnit='')  
   AND (ISNULL(SUCode,'') like '%'+@ServiceUnitCode+'%' OR @ServiceUnitCode='')  
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)  
   AND (B.BU_ID=@BU_ID OR @BU_ID='')  
  )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
     
  END  
  ELSE IF(@Flag = 3)  
  BEGIN  
  ;WITH PagedResults AS  
  (  
   SELECT  --C.ServiceCenterId  
     C.ServiceCenterName  
     ,ROW_NUMBER() OVER(ORDER BY C.ServiceCenterId ASC) AS RowNumber  
     ,C.SCCode  
     ,C.Notes  
     ,S.ServiceUnitName  
     ,B.StateCode  
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName  
   FROM Tbl_ServiceCenter C  
   LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=C.SU_ID  
   LEFT JOIN Tbl_BussinessUnits B ON S.BU_ID=B.BU_ID  
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode  
   WHERE (ISNULL(ServiceCenterName,'') like '%'+@ServiceCenter+'%' OR @ServiceCenter='')  
   AND (ISNULL(SCCode,'') like '%'+@ServiceCenterCode+'%' OR @ServiceCenterCode='')  
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)  
   AND (B.BU_ID=@BU_ID OR @BU_ID='')  
  )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
     
  END  
  ELSE IF(@Flag = 4)  
  BEGIN  
   ;WITH PagedResults AS  
  (  
   SELECT  --C.CycleId  
     ROW_NUMBER() OVER(ORDER BY C.CycleId ASC) AS RowNumber  
     ,C.CycleCode  
     ,C.CycleName  
     ,C.ContactName  
     ,C.ContactNo  
     ,C.DetailsOfCycle  
     ,B.BusinessUnitName AS BusinessUnit  
     ,S.ServiceUnitName AS ServiceUnit  
     ,SC.ServiceCenterName AS ServiceCenter  
     ,B.StateCode  
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName  
   FROM Tbl_Cycles C  
   LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
   LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID  
   LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode  
   WHERE (ISNULL(CycleCode,'') like '%'+@CycleCode+'%' OR @CycleCode='')  
   AND (ISNULL(CycleName,'') like '%'+@CycleName+'%' OR @CycleName='')  
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)  
   AND (B.BU_ID=@BU_ID OR @BU_ID='')  
   )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
     
  END  
  ELSE IF(@Flag = 5)  
  BEGIN  
  ;WITH PagedResults AS  
  (  
   SELECT  --B.BookNo --Faiz-ID103
   B.ID AS BookNo 
     ,B.BookCode  
     ,B.Details  
     ,B.NoOfAccounts  
     ,ROW_NUMBER() OVER(ORDER BY B.BookNo ASC) AS RowNumber  
     ,C.CycleName  
     ,BU.BusinessUnitName AS BusinessUnit  
     ,SU.ServiceUnitName AS ServiceUnitName  
     ,SC.ServiceCenterName AS ServiceCenterName  
     ,BU.StateCode  
     ,BU.BusinessUnitName  
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName  
   FROM Tbl_BookNumbers B  
   LEFT JOIN Tbl_Cycles C ON C.CycleId=B.CycleId  
   LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
   LEFT JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
   LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode  
   WHERE (ISNULL(BookCode,'') like '%'+@BookCode+'%' OR @BookCode='')  
   AND (ISNULL(B.ID,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103
   --AND (ISNULL(BookNo,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)  
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')  
   )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  END  
  ELSE IF(@Flag = 6)  
  BEGIN  
   ;WITH PagedResults AS  
  (  
   SELECT  R.RouteName  
     ,R.RouteCode  
     ,R.Details  
     ,ROW_NUMBER() OVER(ORDER BY R.RouteName ASC) AS RowNumber  
     ,BU.BusinessUnitName AS BusinessUnit  
     ,BU.StateCode  
   FROM Tbl_MRoutes R  
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=R.BU_ID  
   WHERE (ISNULL(RouteName,'') like '%'+@RouteName+'%' OR @RouteName='')  
   AND (ISNULL(RouteCode,'') like '%'+@RouteCode+'%' OR @RouteCode='')  
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)  
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')  
   )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  END  
  ELSE IF(@Flag = 7)  
  BEGIN  
   ;WITH PagedResults AS  
  (  
   SELECT  M.MeterNo  
     ,M.MeterSerialNo  
     ,MT.MeterType  
     ,M.MeterSize  
     ,M.MeterBrand  
     ,M.MeterRating  
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber  
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate  
     ,M.MeterDials  
     ,M.MeterDetails  
     ,M.MeterMultiplier  
     ,M.MeterId  
     ,M.BU_ID  
    ,B.BusinessUnitName  
   FROM Tbl_MeterInformation M  
   LEFT JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType  
   LEFT JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID   
   WHERE (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')  
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')  
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')  
   AND (M.BU_ID=@BU_ID OR @BU_ID='')  
   )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  END  
  ELSE IF(@Flag = 8)  
  BEGIN  
   ;WITH PagedResults AS  
  (  
   SELECT  U.UserId AS EmployeeId  
     ,U.Name  
     ,ROW_NUMBER() OVER(ORDER BY U.UserId ASC) AS RowNumber  
     ,U.SurName  
     ,U.PrimaryContact AS ContactNo  
     ,U.PrimaryEmailId  
     ,D.DesignationName AS Designation  
     ,R.RoleName  
     ,A.Status   
   FROM Tbl_UserDetails U  
   JOIN Tbl_MDesignations D ON D.DesignationId=U.DesignationId  
   JOIN Tbl_MRoles R ON R.RoleId=U.RoleId  
   JOIN Tbl_UserLoginDetails L ON L.UserId=U.UserId  
   JOIN Tbl_MActiveStatusDetails A ON L.ActiveStatusId=A.ActiveStatusId  
   AND L.ActiveStatusId IN (1,2)  
   AND ((ISNULL(Name,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(SurName,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(PrimaryEmailId,'') like '%'+@Name+'%' OR @Name=''))  
   AND (ISNULL(PrimaryContact,'') like '%'+@ContactNo+'%' OR @ContactNo='')  
   AND (ISNULL(R.RoleId,0)=@RoleId OR @RoleId=0)  
   AND (ISNULL(D.DesignationId,0)=@DesignationId OR @DesignationId=0)     
   )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  END  
  ELSE IF(@Flag = 9)  
  BEGIN  
   ;WITH PagedResults AS  
  (  
   SELECT  M.MeterNo  
     ,M.MeterSerialNo  
     ,MT.MeterType  
     ,M.MeterSize  
     ,M.MeterBrand  
     ,M.MeterRating  
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber  
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate  
     ,M.MeterDials  
     ,M.MeterDetails  
     ,M.MeterMultiplier  
     ,M.MeterId  
     ,M.BU_ID  
    ,B.BusinessUnitName  
   FROM Tbl_MeterInformation M  
   LEFT JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType  
   JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID  
   AND M.MeterNo NOT IN (SELECT CPD.MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD WHERE CPD.MeterNumber IS Not NULL)  
   AND M.ActiveStatusId IN(1,2)  
   AND (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')  
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')  
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')  
   AND (M.BU_ID=@BU_ID OR @BU_ID='')  
   )  
    
  SELECT    
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   FROM PagedResults p  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  END  
 END  
 
 ---------------------------------------------------------------------------------------------------------------------
 GO
-- =============================================    
-- Author:  V.Bhimaraju    
-- Create date: 14-03-2014    
-- Description: The purpose of this procedure is to update active status of MeterType   
-- Modified By : Bhargav G  
-- Modified Date : 19th Feb, 2015  
-- Modified Description :  Restricting user to deactivate the meter type when any customers are associated.  
-- =============================================    
ALTER PROCEDURE [USP_UpdateMeterTypeActiveStatus]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterTypeId INT  
   ,@ActiveStatus INT    
   ,@ModifiedBy VARCHAR(50)        
   ,@MeterCount int
   
 SELECT @MeterTypeId=C.value('(MeterTypeId)[1]','INT')    
  ,@ActiveStatus=C.value('(ActiveStatusId)[1]','INT')    
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)    
  
  set @MeterCount=(SELECT count(0) FROM UDV_CustomerDescription WHERE MeterTypeId = @MeterTypeId);
  
 --IF NOT EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE MeterTypeId = @MeterTypeId)  
 IF(@MeterCount=0)
  BEGIN  
   UPDATE Tbl_MMeterTypes   
    SET ActiveStatus=@ActiveStatus    
     ,ModifiedBy=@ModifiedBy    
     ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
   WHERE MeterTypeId=@MeterTypeId  
  
   SELECT 1 AS IsSuccess   
   FOR XML PATH('MastersBE')    
  END  
 ELSE  
  BEGIN  
   SELECT   
    COUNT(0) AS [Count]  
    ,0 AS IsSuccess  
   FROM UDV_CustomerDescription  
   WHERE MeterTypeId = @MeterTypeId  
   FOR XML PATH('MastersBE')  
  END  
END   
GO
 ---------------------------------------------------------------------------------------------------------------------
 
 GO
ALTER TABLE Tbl_PoleDescriptionTable
ADD CONSTRAINT DF_PoleDescriptionTable
DEFAULT 1 FOR ActiveStatusId
GO
-------------------------------------------------------------------------------------------------------
