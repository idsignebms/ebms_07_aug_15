GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 27 Mar 2015  
-- Description: To get the Payment Types for Payment report  
-- =============================================  
CREATE PROCEDURE USP_GetPaymentTypes
   
AS  
BEGIN  
   
 SELECT  
 (  
	  SELECT PaymentTypeID AS TransactionFromId, PaymentType AS TransactionFrom
	  FROM Tbl_MPaymentType   
	  WHERE ActiveStatusID = 1 
  FOR XML PATH('RptPaymentsEditList'),TYPE  
 )  
 FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report

-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, U.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
		CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS ReadingFrom FROM dbo.fn_Split(@TrxTypes,',')) SU ON SU.ReadingFrom = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	
	DECLARE	@TotalRecords INT
	SET @TotalRecords = (SELECT COUNT(0) FROM #CustomerReadingsList)  
		  
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,CONVERT(VARCHAR(20),ReadDate,106) AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,@TotalRecords AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAverageUploadReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
	INTO #AverageUploadList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	
	DECLARE	@TotalRecords INT
	SET @TotalRecords = (SELECT COUNT(0) FROM #AverageUploadList)  
	 
	SELECT
	(
		SELECT RowNumber
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,@TotalRecords AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
		FROM #AverageUploadList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#AverageUploadList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  		
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName
		, dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,
					 CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress
		, TotalAmountEffected
		, MR.Name AS AdjustmentName
	INTO #CustomerAdjustmentList
	FROM Tbl_BillAdjustments CR
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS AdjustmentType FROM dbo.fn_Split(@AdjustmentTypes,',')) SU ON SU.AdjustmentType = CR.BillAdjustmentType 
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.AccountNo
	  
	DECLARE	@TotalRecords INT
	SET @TotalRecords = (SELECT COUNT(0) FROM #CustomerAdjustmentList)  
	 
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,AdjustmentName
			,@TotalRecords AS TotalRecords 
		FROM #CustomerAdjustmentList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerAdjustmentList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek.P  
-- Create date: 25 Mar 2015  
-- Description: To get the List of customers for Payment report  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]  
(  
 @XmlDoc XML = NULL  
)  
AS  
BEGIN  
  
 DECLARE @Users VARCHAR(MAX)   
   ,@TrxFrom VARCHAR(MAX)  
   ,@FromDate DATE  
   ,@ToDate DATE  
   ,@PageSize INT    
   ,@PageNo INT    
    
 SELECT  
   @Users=C.value('(UserName)[1]','VARCHAR(MAX)')  
  ,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')  
  ,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')  
  ,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')  
  ,@PageNo = C.value('(PageNo)[1]','INT')    
  ,@PageSize = C.value('(PageSize)[1]','INT')      
 FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)  
   
 IF(@Users = '')  
  BEGIN  
   SELECT @Users = STUFF((SELECT ',' + CreatedBy  
      FROM Tbl_CustomerPayments   
      GROUP BY CreatedBy   
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@TrxFrom = '')  
  BEGIN  
   SELECT @TrxFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50))   
      FROM Tbl_MPaymentType 
      WHERE ActiveStatusID = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
   
 SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, PT.PaymentType AS TransactionFrom  
  , RecievedDate, PaidAmount  
  , U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName  
  , (dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerName  
  , dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,  
      CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAdress  
 INTO #CustomerPaymentList  
 FROM Tbl_CustomerPayments CR    
 INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
  AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
 INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy  
 INNER JOIN (SELECT [com] AS PaymentType FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.PaymentType = CR.PaymentType   
 INNER JOIN Tbl_MPaymentType PT ON PT.PaymentTypeID = CR.PaymentType 
 INNER JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber = CR.AccountNo  
     
 DECLARE @TotalRecords INT
 SET @TotalRecords = (SELECT COUNT(0) FROM #CustomerPaymentList)  
	 
 SELECT  
 (  
  SELECT RowNumber  
   ,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount  
   ,UserId  
   ,Name AS UserName  
   ,TransactionFrom  
   ,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate  
   ,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate  
   ,@TotalRecords AS TotalRecords   
   ,GlobalAccountNumber  
   ,OldAccountNo  
   ,CycleName AS BookGroup  
   ,CustomerName  
   ,ServiceAdress  
  FROM #CustomerPaymentList  
  WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
  FOR XML PATH('RptPaymentsEditList'),TYPE  
 )  
 FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')  
   
 DROP TABLE #CustomerPaymentList  
   
END  
------------------------------------------------------------------------------------------------
