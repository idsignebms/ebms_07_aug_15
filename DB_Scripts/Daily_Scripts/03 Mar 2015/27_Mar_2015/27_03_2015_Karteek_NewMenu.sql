--SELECT * FROM Tbl_Menus WHERE ReferenceMenuId=8 ORDER BY PAge_Order DESC

-- Here we need to check the ReferenceMenuId and Page Order in Tbl_Menus
GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Edit List','../Reports/EditListReportSummay.aspx',8,23) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
-- Here we need to update the Page Order for Report Summary to increase in Tbl_Menus
UPDATE Tbl_Menus
SET Page_Order = 24
WHERE MenuId = 149
GO
