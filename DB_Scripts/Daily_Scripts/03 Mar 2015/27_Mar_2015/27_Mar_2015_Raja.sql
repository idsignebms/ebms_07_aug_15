GO
UPDATE Tbl_CustomerReadings SET MeterReadingFrom=1
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <4-APR-2014>
-- Description:	<Update BillReading details on customerreading table>
-- Modified By : Padmini
-- Modified Date : 26-12-2014 
-- =============================================
ALTER PROCEDURE [USP_UpdateBillPreviousReading](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		@ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@Count=C.value('(TotalReadings)[1]','INT')
		,@Avg=C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage=CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')

FROM @XmlDoc.nodes('BillingBE') AS T(C)

   
     --SET @CustUn=(SELECT TOP 1 CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccNum)
	 
     SET @CustUn=(SELECT TOP 1 GlobalAccountNumber FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber=@AccNum)
	 --Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]
    UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
									,AverageReading = (SELECT dbo.fn_GetCustomerAverageReading(GlobalAccountNumber,CONVERT(NUMERIC,@Current),@ReadDate)) --CONVERT(VARCHAR(50),(CONVERT(NUMERIC(20,4),(CONVERT(DECIMAL(18,2),@Avg)*@Count)+@Usage))/[TotalReadings])
									,TotalReadingEnergies = ISNULL((SELECT SUM(Usage)FROM Tbl_CustomerReadings C WHERE GlobalAccountNumber = @CustUn AND C.CustomerReadingId < CR.CustomerReadingId),0)+@Usage--CONVERT(NUMERIC(20,4),(CONVERT(DECIMAL(18,2),@Avg)*@Count)+@Usage)
									,Usage = @Usage
									,ModifiedBy = @Modified
									,IsTamper=@IsTamper
									,MeterReadingFrom=@MeterReadingFrom
									,ModifiedDate = GETDATE()
								FROM Tbl_CustomerReadings CR	
									 WHERE GlobalAccountNumber = @CustUn 
									 AND CONVERT(VARCHAR(10),ReadDate,121)=CONVERT(VARCHAR(10),@ReadDate,121)
									 AND CustomerReadingId IN(SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum ORDER BY CustomerReadingId DESC)  
SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- Modified By -- Bhimaraju Vanka
-- Modified Date -- 27-03-2015
-- Reason : Added/Inserting one more field MeterReading From
-- =============================================
ALTER PROCEDURE [USP_InsertCustomerBillReading](@XmlDoc XML)
AS
BEGIN
DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@IsTamper BIT
		,@Multiple numeric(20,4)
		,@MeterReadingFrom INT
SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@ReadBy=C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous=C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage= CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')
		
FROM @XmlDoc.nodes('BillingBE') AS T(C)

   
     --SET @CustUn=(SELECT TOP 1 CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccNum)

     --SET @Multiple=(SELECT TOP 1 MultiplicationFactor FROM Tbl_CustomerDetails WHERE AccountNo=@AccNum);
     
     
	SET @Multiple= (SELECT MI.MeterMultiplier FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	JOIN Tbl_MeterInformation MI ON MI.MeterNo=CPD.MeterNumber
	WHERE CPD.GlobalAccountNumber=@AccNum)
	
	DECLARE @MeterNumber VARCHAR(50)
	
	SET @MeterNumber=(SELECT MeterNumber FROM UDV_CustomerDescription WHERE GlobalAccountNumber=@AccNum)
       
  INSERT INTO Tbl_CustomerReadings (
									 --[CustomerUniqueNo]
									  GlobalAccountNumber
									  ,[ReadDate]
									  ,[ReadBy]
									  ,[PreviousReading]
									  ,[PresentReading]
									  ,[AverageReading]
									  ,[Usage]
									  ,[TotalReadingEnergies]
									  ,[TotalReadings]
									  ,[Multiplier]
									  ,[ReadType]
									  ,[CreatedBy]
									  ,[CreatedDate]
									  ,[IsTamper]
									  ,MeterNumber
									  ,[MeterReadingFrom])
  
									VALUES(
									--@CustUn
									@AccNum
										   ,@ReadDate
										   ,@ReadBy
										   ,CONVERT(VARCHAR(50),@Previous)
										   ,CONVERT(VARCHAR(50),@Current)
										   --,(
										   -- (SELECT case when SUM( PresentReading) is NULL THEN 0 ELSE SUM( PresentReading) END FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Current)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1)
										   ,(SELECT dbo.fn_GetAverageReading(@AccNum,@Usage))
										 --,CONVERT(VARCHAR(50),((SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Usage)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1))
										   ,@Usage
										   ,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+@Usage
										   ,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+1)
										   ,@Multiple
										   ,2
										   ,@CreateBy
										   ,GETDATE()
										   ,@IsTamper
										   ,@MeterNumber
										   ,@MeterReadingFrom)
										   
SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- CREATE By : NEERAJ      
-- CREATED Date: 13-MAR-2015
-- DESC			: Get last close month
-- ModifiedBy : Bhimaraju Vanka
--ModifiedDate : 03-Mar-2015
--Desc : More Columns are fetching
-- =============================================          
ALTER PROCEDURE [USP_GetLastCloseMonth]  
AS          
BEGIN 
	SELECT TOP 1 
		CONVERT(VARCHAR(5),[YEAR]) + ' ' +DateName( month , DateAdd( month , [MONTH] , 0 ) - 1 ) AS LastOpenMonth
		,[Month]
		,[Year]
	FROM Tbl_BillingMonths
	WHERE OpenStatusId=2
	ORDER BY ModifiedDate DESC
END	
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
CREATE PROCEDURE [USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

IF(@Business_Units = '')
	BEGIN
		SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Units = '')
	BEGIN
		SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
				 FROM Tbl_ServiceUnits 
				 WHERE ActiveStatusId = 1
				 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Centers = '')
	BEGIN
		SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
				 FROM Tbl_ServiceCenter
				 WHERE ActiveStatusId = 1
				 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END

SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
	(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name 
	,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,
	CD.Service_Landmark,CD.Service_City,'',
	CD.Service_ZipCode) AS ServiceAddress
	,CR.PreviousReading  AS PreviousReading 
	,CR.ReadDate AS PreviousReadDate
	,CR.PresentReading  AS	 PresentReading
	,CD.InitialReading  AS Usage
	,CR.CreatedDate
	,CR.CreatedBy
	,CR.ReadDate
	,CD.BusinessUnitName
	,CD.ServiceUnitName
	,CD.ServiceCenterName
	,CD.SortOrder AS CustomerSortOrder
into #ReadCustomersList
from Tbl_CustomerReadings CR
INNER JOIN UDV_CustomerDescription CD ON CR.GlobalAccountNumber=CD.GlobalAccountNumber AND CR.IsBilled = 0
INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId

SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
	,Tem.MeterNumber AS MeterNo
	,Tem.ClassName
	,Tem.Name
	,Tem.ServiceAddress
	,MIN(Tem.PreviousReading) AS PreviousReading 
	,MAX (Tem.ReadDate) AS PreviousReadDate
	,MAX(Tem.PresentReading) AS	 PresentReading
	,SUM(usage) AS Usage
	,COUNT(0) AS TotalReadings
	,MAX(Tem.CreatedDate) AS LatestTransactionDate
	,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
		ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
		ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
		FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
		FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
	,Tem.BusinessUnitName
	,Tem.ServiceUnitName
	,Tem.ServiceCenterName
	,Tem.CustomerSortOrder
FROM #ReadCustomersList Tem
GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
	Tem.MeterNumber,Tem.ClassName,
	Tem.Name,
	Tem.ServiceAddress,Tem.BusinessUnitName
	,Tem.ServiceUnitName
	,Tem.ServiceCenterName
	,Tem.CustomerSortOrder
	
DROP TABLE #ReadCustomersList

END
GO
