 GO
 -- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 5-MARCHAR-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)             
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name                   
      ,A.GlobalAccountNumber AS AccountNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                      
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    AND A.ActiveStatusId=1                              
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END           
 GO
 -------------------------------------------------------------------------------------------------- 