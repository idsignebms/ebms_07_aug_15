  
    
/*--------------------------------------------------------------------------\  */    
ALTER VIEW [dbo].[UDV_CustomerDescription]    
AS    
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs,     
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo,     
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading,     
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID    
                      ,CD.Service_HouseNo       
   ,CD.Service_StreetName        
   ,CD.Service_City       
   ,CD.Service_ZipCode               
        
   ,CD.Postal_ZipCode    
   ,CD.Postal_HouseNo       
   ,CD.Postal_StreetName        
   ,CD.Postal_City    
   ,CD.Postal_Landmark    
   ,Cd.Service_Landmark    
   ,Cd.ApplicationDate    
   ,Cd.SetupDate    
   ,CD.TenentId    
   ,CD.IsVIPCustomer    
   ,IsCAPMI    
   ,CD.IsBEDCEmployee    
   ,MeterAmount    
   ,MS.StatusId as CustomerStatusID    
   ,PD.IsEmbassyCustomer    
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
          
       
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN    
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo    
    
    
    
    
----------------------------------------------------------------------------------------------------
GO
CREATE VIEW [dbo].[UDV_RptCustomerDesc]  
AS  
 SELECT       
    CD.GlobalAccountNumber  
   ,CD.DocumentNo  
   ,CD.AccountNo  
   ,CD.OldAccountNo  
   ,CD.Title  
   ,CD.FirstName  
   ,CD.MiddleName  
   ,CD.LastName  
   ,CD.KnownAs  
   ,CD.EmployeeCode  
   ,ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo  
   ,ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo  
   ,ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo  
   ,TD.PhoneNumber  
   ,TD.AlternatePhoneNumber  
   ,CD.ActiveStatusId  
   ,PD.PoleID  
   ,PD.TariffClassID AS TariffId  
   ,TC.ClassName  
   ,PD.IsBookNoChanged  
   ,PD.ReadCodeID  
   ,PD.SortOrder  
   ,CAD.Highestconsumption  
   ,CAD.OutStandingAmount  
   ,BN.BookNo  
   ,BN.BookCode
   ,BN.ID AS BookId    
   ,BU.BU_ID  
   ,BU.BusinessUnitName  
   ,BU.BUCode  
   ,SU.SU_ID  
   ,SU.ServiceUnitName  
   ,SU.SUCode  
   ,SC.ServiceCenterId  
   ,SC.ServiceCenterName  
   ,SC.SCCode  
   ,C.CycleId  
   ,C.CycleName  
   ,C.CycleCode  
   ,MS.StatusName AS ActiveStatus  
   ,CD.EmailId  
   ,PD.MeterNumber  
   ,MI.MeterType AS MeterTypeId  
   ,PD.PhaseId  
   ,PD.ClusterCategoryId  
   ,CAD.InitialReading  
   ,CAD.InitialBillingKWh AS MinimumReading  
   ,CAD.AvgReading  
   ,PD.RouteSequenceNumber AS RouteSequenceNo  
   ,CD.ConnectionDate  
   ,CD.CreatedDate  
   ,CD.CreatedBy  
   ,CAD.PresentReading  
   ,PD.CustomerTypeId  
   ,C.ActiveStatusId AS CylceActiveStatusId  
   ,CD.ServiceAddressID  
   ,Service_HouseNo  
   ,Service_StreetName  
   ,Service_Landmark  
   ,Service_City  
   ,Service_ZipCode  
   ,Postal_HouseNo  
   ,Postal_StreetName  
   ,Postal_Landmark  
   ,Postal_City  
   ,Postal_ZipCode  
   ,BN.SortOrder AS BookSortOrder  
   ,TC.RefClassID  
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
  LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber   
  LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId   
  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  LEFT JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  LEFT JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  LEFT JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  LEFT JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID   
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID   
  LEFT OUTER JOIN dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId   
  LEFT OUTER JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo  
------------------------------------------------------------------------------------------------------
GO
  
CREATE VIEW [dbo].[UDV_RptCustomerAndBookInfo]    
AS    
 SELECT CD.GlobalAccountNumber  
 ,CD.AccountNo  
 ,CD.OldAccountNo  
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode) AS [ServiceAddress]   
   ,PD.SortOrder  
   ,BN.SortOrder AS BookSortOrder  
   ,CD.ConnectionDate  
   ,PD.PoleID    
   ,PD.TariffClassID AS TariffId    
   ,TC.ClassName    
   ,PD.ReadCodeID    
   ,BN.BookNo    
   ,BN.BookCode  
   ,BN.ID AS BookId    
   ,BU.BU_ID    
   ,BU.BusinessUnitName    
   ,BU.BUCode    
   ,SU.SU_ID    
   ,SU.ServiceUnitName    
   ,SU.SUCode    
   ,SC.ServiceCenterId    
   ,SC.ServiceCenterName    
   ,SC.SCCode    
   ,C.CycleId    
   ,C.CycleName    
   ,C.CycleCode  
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID  
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID   
----------------------------------------------------------------------------------------------------  
  GO
    
CREATE VIEW [dbo].[UDV_RptCustomerBookInfo]    
AS    
 SELECT         
    PD.GlobalAccountNumber    
   ,PD.PoleID    
   ,PD.TariffClassID AS TariffId    
   ,TC.ClassName    
   ,PD.ReadCodeID    
   ,BN.BookNo    
   ,BN.BookCode  
   ,BN.ID AS BookId    
   ,BU.BU_ID    
   ,BU.BusinessUnitName    
   ,BU.BUCode    
   ,SU.SU_ID    
   ,SU.ServiceUnitName    
   ,SU.SUCode    
   ,SC.ServiceCenterId    
   ,SC.ServiceCenterName    
   ,SC.SCCode    
   ,C.CycleId    
   ,C.CycleName    
   ,C.CycleCode  
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS PD  
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID  
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID   
----------------------------------------------------------------------------------------------------  
  