GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


IF(@Business_Units = '')
	BEGIN
		SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Units = '')
	BEGIN
		SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
				 FROM Tbl_ServiceUnits 
				 WHERE ActiveStatusId = 1
				 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Centers = '')
	BEGIN
		SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
				 FROM Tbl_ServiceCenter
				 WHERE ActiveStatusId = 1
				 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END


SELECT TOP 1000 * FROM UDV_CustomerDescription	CD
INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeID = 1 AND CD.MinimumReading IS NULL
INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
INNER JOIN Tbl_DirectCustomersAvgReadings DAVG ON CD.GlobalAccountNumber != DAVG.GlobalAccountNumber 
INNER JOIN Tbl_MeterInformation	MI ON MI.MeterNo = CD.MeterNumber AND isnull(CONVERT(DECIMAL(16,2), MI.MeterMultiplier),0) != 0


END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

IF(@Business_Units = '')
	BEGIN
		SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Units = '')
	BEGIN
		SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
				 FROM Tbl_ServiceUnits 
				 WHERE ActiveStatusId = 1
				 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Centers = '')
	BEGIN
		SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
				 FROM Tbl_ServiceCenter
				 WHERE ActiveStatusId = 1
				 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END

DECLARE @Count INT
SET @Count = 10000

SELECT TOP(@Count) CD.GlobalAccountNumber,CD.OldAccountNo
	,CD.MeterNumber AS MeterNo
	,CD.ClassName
	,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name 
	,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress
	,CR.PreviousReading AS PreviousReading 
	,CR.ReadDate AS PreviousReadDate
	,CR.PresentReading AS PresentReading
	,[usage] AS Usage
	, CR.CreatedDate
	,CR.CreatedBy
	,CR.ReadDate
	,CD.BusinessUnitName
	,CD.ServiceUnitName
	,CD.ServiceCenterName
	,CD.SortOrder AS CustomerSortOrder
INTO #NonReadCustomersList
FROM  UDV_CustomerDescription CD
INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeID = 2 -- Read Customer 
INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId 
LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0 AND CR.CustomerReadingId IS NULL

SELECT * FROM #NonReadCustomersList

DROP TABLE #NonReadCustomersList

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- Hold, Closed Customers
	SET	@BookDisableType = '1'

IF(@Business_Units = '')
	BEGIN
		SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Units = '')
	BEGIN
		SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
				 FROM Tbl_ServiceUnits 
				 WHERE ActiveStatusId = 1
				 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END
IF(@Service_Centers = '')
	BEGIN
		SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
				 FROM Tbl_ServiceCenter
				 WHERE ActiveStatusId = 1
				 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
	END

--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
SELECT   
	CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,CD.ServiceUnitName,CD.ServiceCenterName,
	(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name ,
	dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress
	,'Pre- Paid Meters' AS Comments
FROM UDV_CustomerDescription CD
INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
INNER JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
UNION ALL
SELECT   
	CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,CD.ServiceUnitName,CD.ServiceCenterName,
	(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name ,
	dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress
	,'HOLD / Closed Customers' AS Comments
FROM UDV_CustomerDescription CD
INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
INNER JOIN (SELECT [com] AS MeterTypes FROM dbo.fn_Split(@MeterTypes,',')) CustomerStatus ON CustomerStatus.MeterTypes = CD.MeterTypeId
UNION ALL 
SELECT  
	CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,CD.ServiceUnitName,CD.ServiceCenterName,
	(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name ,
	dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress
	,'BookDisable No Power' AS Comments
FROM UDV_CustomerDescription CD
INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
INNER JOIN Tbl_BillingDisabledBooks BD ON ISNULL(CD.BookNo,0) = BD.BookNo
INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId   


END
GO