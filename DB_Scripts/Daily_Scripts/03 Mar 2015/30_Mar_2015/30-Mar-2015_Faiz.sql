
GO
/****** Object:  Trigger [dbo].[TR_INSERTAUDITAGENCIES]    Script Date: 03/30/2015 15:43:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[TR_INSERTAUDITAGENCIES] ON [dbo].[Tbl_Agencies]
INSTEAD OF UPDATE
AS
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <18-MAR-2014>
-- Description:	<Insert Previous data of Agencies into Audit table Before Update>
-- =============================================
DECLARE @ID INT
    , @AgencyName  VARCHAR(300)
      , @Details  VARCHAR(MAX)
      , @ContactNo1   VARCHAR(20)
      , @ContactNo2   VARCHAR(20)
      , @ContactPersonName   VARCHAR(50)
      ,@ActiveStatusId  INT
      , @ModifedBy   VARCHAR(50);
      

SELECT @ID=I.AgencyId FROM inserted I;
SELECT @AgencyName=I.AgencyName FROM inserted I;

SELECT @Details=I.Details FROM inserted I;

SELECT @ContactNo1=I.ContactNo1 FROM inserted I;

SELECT @ContactNo2=I.ContactNo2 FROM inserted I;

SELECT @ContactPersonName=I.ContactPersonName FROM inserted I;

SELECT @ActiveStatusId=I.ActiveStatusId FROM inserted I;
SELECT @ModifedBy=I.ModifedBy FROM inserted I;


INSERT INTO Tbl_Audit_Agencies
(
AgencyId
,AgencyName
,Details
,ContactNo1
,ContactNo2
,ContactPersonName
,ActiveStatusId
,CreatedDate
,CreatedBy
,ModifedBy
,ModifiedDate
)
SELECT 
		AgencyId
      ,[AgencyName]
      ,[Details]
      ,[ContactNo1]
      ,[ContactNo2]
      ,[ContactPersonName]
      ,[ActiveStatusId]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ModifedBy]
      ,[ModifiedDate] FROM Tbl_Agencies WHERE AgencyId=@ID
      
      
 UPDATE Tbl_Agencies SET [AgencyName]=@AgencyName
      ,[Details]=@Details
      ,[ContactNo1]=@ContactNo1
      ,[ContactNo2]=@ContactNo2
      ,[ContactPersonName]=@ContactPersonName
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifedBy]=@ModifedBy
      ,[ModifiedDate]=GETDATE() WHERE AgencyId=@ID


GO
-------------------------------------------------------------------------------------------------------


GO
/****** Object:  Trigger [dbo].[TR_InsertAuditStates]    Script Date: 03/30/2015 15:49:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of States into Audit table Before Update>
-- =============================================

ALTER TRIGGER [dbo].[TR_InsertAuditStates]   ON  [dbo].[Tbl_States]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE 
	 @StateCode   varchar (20)   ,
	 @StateName   varchar (50)  ,
	 @DisplayCode   varchar (20)  ,
	 @CountryCode   varchar (20)  ,
	 @StateDetails   varchar (max)  ,
	 @Notes   varchar (max)  ,
	 @ModifiedBy   varchar (50)  ,
	 @IsActive   bit   ;

SELECT
 @StateCode =I.StateCode
 ,@StateName=I.StateName
 ,@DisplayCode=I.DisplayCode
 ,@CountryCode=I.CountryCode
 ,@StateDetails=I.StateDetails
 ,@Notes=I.Notes
 ,@ModifiedBy=I.ModifiedBy
 ,@IsActive=I.IsActive
FROM inserted I;

INSERT INTO Tbl_Audit_States
(
StateCode
,StateName
,DisplayCode
,CountryCode
,StateDetails
,Notes
,CreatedBy
,CreatedDate
,ModifiedBy
,ModifiedDate
,IsActive
)
SELECT [StateCode]
      ,[StateName]
      ,[DisplayCode]
      ,[CountryCode]
      ,[StateDetails]
      ,[Notes]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[IsActive]
       FROM Tbl_States WHERE StateCode=@StateCode;
UPDATE Tbl_States SET
[StateName]=@StateName
      ,[DisplayCode]=@DisplayCode
      ,[CountryCode]=@CountryCode
      ,[StateDetails]=@StateDetails
      ,[Notes]=@Notes
      ,[ModifiedBy]=@ModifiedBy
      ,[ModifiedDate]=GETDATE()
      ,[IsActive]=@IsActive WHERE StateCode=@StateCode;

END
GO
------------------------------------------------------------------------------------------


GO
/****** Object:  Trigger [dbo].[TR_InsertAuditServiceUnits]    Script Date: 03/30/2015 15:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceUnits into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
ALTER TRIGGER [dbo].[TR_InsertAuditServiceUnits]  ON  [dbo].[Tbl_ServiceUnits]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE  
	 @SU_ID   varchar (20)   ,
	 @ServiceUnitName   varchar (300)  ,
	 @Notes   varchar (max)  ,
	 @BU_ID   varchar (20)  ,
	 @SUCode   varchar (10)  ,
	 @ActiveStatusId   int   ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ModifiedBy   varchar (50) ;
	 
	 SELECT 
	 @SU_ID =I.SU_ID
	 ,@ServiceUnitName=I.ServiceUnitName
	 ,@Notes=I.Notes
	 ,@BU_ID=I.BU_ID
	 ,@SUCode=I.SUCode
	 ,@Address1=I.ActiveStatusId
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 ,@ActiveStatusId=I.ActiveStatusId
	 ,@ModifiedBy=I.ModifiedBy
	 FROM inserted I; 
	 
	 INSERT INTO Tbl_Audit_ServiceUnits
	 (
		SU_ID
		,ServiceUnitName
		,Notes
		,BU_ID
		,ActiveStatusId
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate
		,SUCode
		,Address1
		,Address2
		,City
		,ZIP

	 )
	 SELECT [SU_ID]
      ,[ServiceUnitName]
      ,[Notes]
      ,[BU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SUCode] 
      ,Address1
      ,Address2
      ,City
      ,ZIP
      FROM Tbl_ServiceUnits WHERE SU_ID=@SU_ID;
	 
	 UPDATE Tbl_ServiceUnits SET
							   [ServiceUnitName]=@ServiceUnitName
							  ,[Notes]=@Notes
							  ,[BU_ID]=@BU_ID
							  ,[SUCode]=@SUCode
							  ,[ActiveStatusId]=@ActiveStatusId
							  ,[Address1]=@Address1
							  ,Address2=@Address2
						       ,City=@City
							   ,ZIP=@ZIP
							  ,[ModifiedBy]=@ModifiedBy
							  ,[ModifiedDate]=GETDATE() 
	 WHERE SU_ID=@SU_ID;

END
GO
------------------------------------------------------------------------------------------


GO
/****** Object:  Trigger [dbo].[TR_InsertAuditServiceCenter]    Script Date: 03/30/2015 15:59:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceCenter into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
ALTER TRIGGER [dbo].[TR_InsertAuditServiceCenter]   ON  [dbo].[Tbl_ServiceCenter]
   INSTEAD OF UPDATE
AS 
BEGIN

DECLARE  
     @ServiceCenterId   varchar(20)   ,
	 @ServiceCenterName   varchar(300)  ,
	 @Notes   varchar(max)  ,
	 @SU_ID   varchar(20)  ,
	 @SCCode   varchar(10)  ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ActiveStatusId   int   ,
	 @ModifiedBy   varchar(50) ;
	 
	 SELECT
	  @ServiceCenterId =I.ServiceCenterId
	  ,@ServiceCenterName=I.ServiceCenterName
	  ,@Notes=I.Notes
	  ,@SU_ID=I.SU_ID
	  ,@ActiveStatusId=I.ActiveStatusId
	  ,@ModifiedBy=I.ModifiedBy
	  ,@SCCode=I.SCCode
	  ,@Address1=I.ActiveStatusId
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 FROM inserted I;
	 
	 INSERT INTO Tbl_Audit__ServiceCenter
	 (
		ServiceCenterId
		,ServiceCenterName
		,Notes
		,SU_ID
		,ActiveStatusId
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate
		,SCCode
		,Address1
		,Address2
		,City
		,ZIP
	 )
	 SELECT [ServiceCenterId]
      ,[ServiceCenterName]
      ,[Notes]
      ,[SU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SCCode]
      ,Address1
      ,Address2
      ,City
      ,ZIP
       FROM Tbl_ServiceCenter WHERE ServiceCenterId=@ServiceCenterId;
	 
	 UPDATE Tbl_ServiceCenter SET
	 [ServiceCenterName]=@ServiceCenterName
      ,[Notes]=@Notes
      ,[SU_ID]=@SU_ID
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifiedBy]=@ModifiedBy
      ,[SCCode]=@SCCode
      ,[Address1]=@Address1
      ,Address2=@Address2
      ,City=@City
      ,ZIP=@ZIP
      ,[ModifiedDate]=dbo.fn_GetCurrentDateTime() 
     WHERE ServiceCenterId=@ServiceCenterId  
      
END
GO
-----------------------------------------------------------------------------------------------
GO
ALTER TABLE dbo.Tbl_PoleDescriptionTable
ADD CONSTRAINT ActiveStatusId_Default_Value
DEFAULT 1 FOR ActiveStatusId ;
GO
-----------------------------------------------
GO
ALTER TABLE dbo.Tbl_MPoleMasterDetails
ADD CONSTRAINT MPoleMasterDetails_ActiveStatusId_Default_Value
DEFAULT 1 FOR ActiveStatusId ;
GO
-----------------------------------------------------------------------------------------------

GO
-- =============================================      
-- Author:  Bhimaraju.Vanka      
-- Create date: 27-06-2014      
-- Description: Update Status of Tariff Categories     
-- Modified By : Bhargav G  
-- Modified Date : 19th Feb, 2015  
-- Modified Descrtiption : Restricted user to deactive if there are any customers assigned to it.  

-- Modified By : Faiz-ID103
-- Modified Date : 30-Mar-2015
-- Modified Descrtiption : Restricted user to deactive if there are any customers assigned to it.  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_UpdateTariffCategoryActiveStatus]      
(      
 @XmlDoc XML      
)      
AS      
BEGIN      
 DECLARE  @ClassID INT      
 ,@ActiveStatusId BIT      
 ,@ModifiedBy VARCHAR(50)    
 ,@TariffCustomerCount INT
  
 SELECT @ClassID = C.value('(ClassID)[1]','INT')      
   ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')      
   ,@ActiveStatusId = C.value('(ActiveStatusId)[1]','BIT')      
 FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)      
 
 
 --Faiz-ID103
 set @TariffCustomerCount  = (select Count(0) from CUSTOMERS.Tbl_CustomerProceduralDetails 
								where TariffClassID IN(select ClassID from Tbl_MTariffClasses 
								where RefClassID=@ClassID or ClassID=@ClassID))
  
 IF(@ActiveStatusId = 1) --For Activation    
  BEGIN    
   UPDATE Tbl_MTariffClasses    
    SET IsActiveClass = @ActiveStatusId  
     ,ModifiedBy = @ModifiedBy  
     ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
   WHERE  ClassID = @ClassID      
  
   SELECT 1 AS IsSuccess   
   FOR XML PATH('TariffManagementBE')     
  END    
 ELSE --For Deactivation    
  BEGIN    
   --IF NOT EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE TariffId = @ClassID)  --Faiz-ID103
   IF(@TariffCustomerCount<=0)
    BEGIN  
     IF ((SELECT Count(0) FROM Tbl_MTariffClasses WHERE IsActiveClass = 1 AND RefClassID IS NULL)> 1)    
      BEGIN    
       UPDATE Tbl_MTariffClasses    
        SET IsActiveClass = @ActiveStatusId  
         ,ModifiedBy = @ModifiedBy  
         ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
       WHERE  ClassID = @ClassID      
  
       SELECT 1 AS IsSuccess   
       FOR XML PATH('TariffManagementBE')     
      END      
     ELSE    
      BEGIN    
       SELECT 0 AS IsSuccess  
        ,1 AS IsLastActiveRecord   
       FOR XML PATH('TariffManagementBE')    
      END    
    END  
   ELSE  
    BEGIN  
     SELECT 0 AS IsSuccess  
       ,@TariffCustomerCount AS [Count]  
     --FROM UDV_CustomerDescription  --Faiz-ID103
     --WHERE TariffId = @ClassID  
     FOR XML PATH('TariffManagementBE')  
    END  
  END    
END      
GO
-----------------------------------------------------------------------------------------------
    GO
-- =============================================      
-- Author:  T.Karthik      
-- Create date: 16-03-2014      
-- Modified date : 25-03-2014      
-- Modified By:  V.Bhimaraju      
-- Modified date : 10-04-2014      
-- Modified date : 27-06-2014      
-- Description: The purpose of this procedure is to get Energy class charges list     
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetEnergyClassChargesList]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @Year INT, @PageNo INT      
   ,@PageSize INT      
 SELECT      
   @PageNo=C.value('PageNo[1]','INT')      
   ,@PageSize=C.value('PageSize[1]','INT')      
   ,@Year=C.value('(Year)[1]','INT')      
 FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)      
       
 ;WITH PagedResults AS      
  (      
   SELECT      
    ROW_NUMBER() OVER(ORDER BY TC.ClassID ASC , FromDate ASC) AS RowNumber      
    ,Amount          
    --,CONVERT(VARCHAR(30),FromDate,103) AS FromDate      
    --,CONVERT(VARCHAR(30),Todate,103) AS ToDate      
    --,SUBSTRING(CONVERT(VARCHAR(30), FromDate, 113), 4, 8) AS FromDate      
    --,SUBSTRING(CONVERT(VARCHAR(30), Todate, 113), 4, 8) AS ToDate      
    --,REPLACE(RIGHT(CONVERT(VARCHAR(30),FromDate,6),6),' ','-') AS FromDate      
    --,REPLACE(RIGHT(CONVERT(VARCHAR(30),ToDate,6),6),' ','-') AS ToDate      
    --,CONVERT(VARCHAR(50),FromDate,106) AS FromDate -- Correct      
    --,CONVERT(VARCHAR(50),ToDate,106) AS ToDate -- Correct      
    ,(CONVERT(VARCHAR(50),FromDate,106) +' - '+ CONVERT(VARCHAR(50),ToDate,106)) AS FromDate          
    --,(CASE (SELECT IsActiveClass FROM Tbl_MTariffClasses where ClassID= TC.ClassID) WHEN 0 THEN 'Inactive' ELSE 'Active' END) AS ClassStatus    
    ,TC.ClassName      
    ,FromKW      
    ,ISNULL(CONVERT(VARCHAR(50),ToKW),'&#8734;') AS ToKWs      
    ,EnergyClassChargeID     
    ,ECC.IsActive  
    ,(SELECT (case when (t.IsActiveClass = 0) then 'Inactive'     
    when (tt.IsActiveClass = 0) then 'Inactive' when (ECC.IsActive=0) then 'Inactive' else 'Active' end )     
    from Tbl_MTariffClasses t    
    LEFT join Tbl_MTariffClasses tt    
    on tt.RefClassID=t.ClassID     
    where tt.ClassName is not null AND tt.ClassID=TC.ClassID) as ClassStatus     
   FROM Tbl_LEnergyClassCharges AS ECC      
   JOIN Tbl_MTariffClasses TC ON ECC.ClassID=TC.ClassID      
   WHERE       
    (CASE @YEAR WHEN 0 THEN YEAR(GETDATE()) ELSE @YEAR END)      
     BETWEEN CASE @YEAR WHEN 0 THEN YEAR(GETDATE()) ELSE YEAR(FromDate) END      
     AND CASE @YEAR WHEN 0 THEN YEAR(getdate()) ELSE YEAR(ToDate) END      
   AND IsActive=1 --AND IsActiveClass=1      
  )      
       
 SELECT      
 (      
  SELECT      
   *      
   ,(Select COUNT(0) from PagedResults) as TotalRecords           
   FROM PagedResults      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  FOR XML PATH('TariffManagement'),TYPE      
 )      
 FOR XML PATH(''),ROOT('TariffManagementBEInfoByXml')      
END      
GO
-----------------------------------------------------------------------------------------------      
      
      
      
      