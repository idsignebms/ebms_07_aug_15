   
GO
  
CREATE VIEW [dbo].[UDV_EditListReport]    
AS         
  
 SELECT   
	 CD.GlobalAccountNumber  
	 ,CD.AccountNo  
	 ,CD.OldAccountNo  
	 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
	 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
				 ,CD.Service_Landmark  
				 ,CD.Service_City,'',  
				CD.Service_ZipCode) AS [ServiceAddress]   
	 ,PD.SortOrder  
	 ,BN.SortOrder AS BookSortOrder  
	 ,PD.TariffClassID AS TariffId    
	 ,C.CycleId    
	 ,C.CycleName    
	 ,C.CycleCode  
	 ,BU.BU_ID
	 ,BU.BusinessUnitName
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID  
   
 ----------------------------------------------------------------------------------------------------  
  
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[UDV_PrebillingRpt]  

AS 
 SELECT CD.GlobalAccountNumber
	,CD.AccountNo
	,CD.OldAccountNo
	,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName
	,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS [ServiceAddress] 
   ,PD.SortOrder
   ,BN.SortOrder AS BookSortOrder
   ,CD.ConnectionDate
   ,PD.PoleID  
   ,PD.TariffClassID AS TariffId  
   ,TC.ClassName  
   ,PD.ReadCodeID  
   ,BN.BookNo  
   ,BN.BookCode
   ,BN.ID AS BookId  
   ,BU.BU_ID  
   ,BU.BusinessUnitName  
   ,BU.BUCode  
   ,SU.SU_ID  
   ,SU.ServiceUnitName  
   ,SU.SUCode  
   ,SC.ServiceCenterId  
   ,SC.ServiceCenterName  
   ,SC.SCCode  
   ,C.CycleId  
   ,C.CycleName  
   ,C.CycleCode
   ,Pd.MeterNumber
   ,CAD.InitialBillingKWh as DeafaultUsage
   ,CustomerStatus.StatusId as CustomerStatusID
   FROM CUSTOMERS.Tbl_CustomerSDetail AS CD 
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
  INNER Join CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID
  INNER JOIn Tbl_MCustomerStatus AS CustomerStatus ON   CD.ActiveStatusId = CustomerStatus.StatusId 
----------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
			AND CD.ReadCodeID = 1 AND CD.DeafaultUsage IS NULL
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	INNER JOIN Tbl_DirectCustomersAvgReadings DAVG ON CD.GlobalAccountNumber != DAVG.GlobalAccountNumber

END
----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,[usage] AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeID = 2 -- Read Customer 
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId 
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE CR.CustomerReadingId IS NULL

END
--------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- PrepaidCustomers
	SET	@BookDisableType = '1'

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
	SELECT   
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress AS ServiceAddress
		,CASE WHEN ISNULL(CustomerStatus.CustomerStatusID,0)=0 THEN 'HOLD / Closed Customers' ELSE 'Pre- Paid Meters' END AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	LEFT JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
	LEFT JOIN Tbl_MeterInformation MI ON CD.MeterNumber = MI.MeterNo and MI.MeterType=1
	WHERE CustomerStatus.CustomerStatusID IS NUll OR  MI.MeterType IS NULL 
	
	UNION ALL 
	
	SELECT  
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress  AS ServiceAddress
		,'BookDisable No Power' AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM UDV_PrebillingRpt CD
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	INNER JOIN Tbl_BillingDisabledBooks BD ON ISNULL(CD.BookNo,0) = BD.BookNo
	INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId   

END
--------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,ISNULL(CR.Usage,0)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber=CD.GlobalAccountNumber AND CR.IsBilled = 0
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	
	DROP TABLE #ReadCustomersList

END
----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,Cr.Usage  AS Usage
		,CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PrebillingRpt CD ON CR.GlobalAccountNumber=CD.GlobalAccountNumber AND CR.IsBilled = 0
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	 HAVING (CAST(SUM(usage) AS DECIMAL(18,2)) > CAST(AverageReading AS DECIMAL(18,2))  + ((30/100)*100))
		 OR	(CAST(SUM(usage) AS DECIMAL(18,2)) < 250  + ((30/100)*100))
		 
	DROP TABLE #ReadCustomersList
	
END
----------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 06-June-2014  
-- Description: Get The Details of Individual Bill Balance Of The Customers  
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================  
CREATE PROCEDURE [dbo].[USP_RptGetAccountsStatusOfBill_New]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  

	DECLARE @PageSize INT  
		,@PageNo INT  
		,@BU_ID VARCHAR(MAX)  
		,@SU_ID VARCHAR(MAX)  
		,@SC_ID VARCHAR(MAX)    
		,@CycleId VARCHAR(MAX)  
		,@BookNo VARCHAR(MAX)  
		,@YearId VARCHAR(MAX)  
		,@MonthId VARCHAR(MAX)  
     
	SELECT @PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@YearId=C.value('(YearName)[1]','VARCHAR(MAX)')  
		,@MonthId=C.value('(MonthName)[1]','VARCHAR(MAX)')  
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)  
   
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
   
	;WITH PagedResults AS  
	(  
		SELECT  
			ROW_NUMBER() OVER(ORDER BY CD.SortOrder ) AS RowNumber  
			,CD.AccountNo AS AccountNo
			,CD.GlobalAccountNumber AS GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,ISNULL(CONVERT(VARCHAR(50),BillGeneratedDate,106),'--') AS BillDate  
			,ISNULL(TotalBillAmountWithTax,0) AS BillAmount
			,ISNULL(CONVERT(VARCHAR(50),(SELECT dbo.fn_GetCustomerLastPaymentDate(CB.AccountNo)) ,106),'--') AS LastPaidDate  
			,ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0) AS TotalPaidAmount  
			,(CB.TotalBillAmountWithTax - ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0)) AS DueAmount
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.OldAccountNo AS OldAccountNo
		FROM Tbl_CustomerBills CB
		INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CB.AccountNo 
				AND (CB.TotalBillAmountWithTax - ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0)) > 0
				AND BillMonth IN (SELECT [com] FROM dbo.fn_Split(@MonthId,','))
				AND BillYear IN (SELECT [com] FROM dbo.fn_Split(@YearId,','))
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
	)  
   
	SELECT   
	(  
		SELECT    
			 RowNumber  
			,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,BillDate  
			,BillAmount
			,LastPaidDate  
			,TotalPaidAmount  
			,DueAmount
			,BusinessUnitName
			,ServiceCenterName
			,ServiceUnitName
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,OldAccountNo
			,(SELECT SUM(DueAmount) FROM PagedResults ) AS TotalDueAmount  
			,(Select COUNT(0) from PagedResults) AS TotalRecords  
		FROM PagedResults p  
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize  
		FOR XML PATH('Reports'),TYPE  
	)  
	FOR XML PATH(''),ROOT('ReportsBeInfoByXml')  
	
END  
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 05-June-2014
-- Description:	Get The Details of Debit Balence Of The Customers
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetAccountsWithDebitBal_New]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)			
			,@BookNo VARCHAR(MAX)
			,@MinAmt VARCHAR(50)
			,@MaxAmt VARCHAR(50)
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@MinAmt=C.value('(MinAmt)[1]','VARCHAR(50)')
			,@MaxAmt=C.value('(MaxAmt)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
			
	;WITH PagedResults AS
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY CD.SortOrder ) AS RowNumber
				,A.AccountNo AS AccountNo
				,CD.GlobalAccountNumber AS GlobalAccountNumber
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
				,OutStandingAmount AS TotalDueAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(A.AccountNo)),0) AS LastPaidAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(A.AccountNo)),'--') AS LastPaidDate
				,CD.OldAccountNo
				,CD.CycleName
				,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
				,CD.SortOrder AS CustomerSortOrder
				,CD.BookSortOrder
				,CD.BusinessUnitName AS BusinessUnitName
				,CD.ServiceUnitName AS ServiceUnitName
				,CD.ServiceCenterName AS ServiceCenterName
		 FROM Tbl_CustomerBills A
		 INNER JOIN [UDV_CustomerDescription] CD ON A.AccountNo = CD.GlobalAccountNumber AND OutStandingAmount >0
				AND OutStandingAmount BETWEEN (CASE WHEN @MaxAmt='' AND @MinAmt='' THEN (OutStandingAmount) ELSE @MinAmt END) 
									AND (CASE @MaxAmt WHEN '' THEN (OutStandingAmount) ELSE @MaxAmt END)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
	)
	
	SELECT 
	(
		SELECT	 
			 RowNumber
			,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,TotalDueAmount
			,LastPaidAmount
			,LastPaidDate
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(SELECT SUM(TotalDueAmount) FROM PagedResults ) AS DueAmount
			,(Select COUNT(0) from PagedResults) as TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		FOR XML PATH('Reports'),TYPE
	)
	FOR XML PATH(''),ROOT('ReportsBeInfoByXml')

END
--------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetPaymets_New]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
		
	IF (@FromDate ='' OR @FromDate IS NULL)    
		BEGIN    
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
		END 
	IF (@Todate ='' OR @Todate IS NULL)    
		BEGIN    
			SET @Todate = dbo.fn_GetCurrentDateTime()    
		END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,CD.GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,100),'--') AS RecievedDate
			,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD 	
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN Tbl_CustomerPayments CP ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
				--AND (DATEDIFF(DAY,@FromDate,CP.RecievedDate) >= 0 OR @FromDate = '')
				--AND (DATEDIFF(DAY,@ToDate,CP.RecievedDate) <= 0 OR @ToDate = '')
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		INNER JOIN Tbl_MBillingTypes BT ON CP.ReceivedDevice = BT.BillingTypeId
	)
	
	SELECT 
		(
			SELECT	 
				 RowNumber
				,GlobalAccountNumber
				,Name
				,ServiceAddress
				,BusinessUnitName
				,ServiceCenterName
				,ServiceUnitName
				,CycleName
				,BookNumber
				,CustomerSortOrder
				,BookSortOrder
				,ReceivedDevice
				,PaidAmount
				,RecievedDate
				,TotalDueAmount
				,(SELECT SUM(TotalDueAmount) FROM PagedResults ) AS TotalDue
				,(Select COUNT(0) from PagedResults) as TotalRecords
				,(SELECT SUM(PaidAmount) FROM PagedResults) AS TotalReceivedAmount
			FROM PagedResults p
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			FOR XML PATH('Reports'),TYPE
		)
	FOR XML PATH(''),ROOT('ReportsBeInfoByXml')
	
END
--------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 23-Aug-2014
-- Description:	Get The Details of Credit Balance Of The Customers
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetAccountsWithCreditBal_New]
(
	@XmlDoc xml
)
AS
BEGIN

		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(		
		SELECT ROW_NUMBER() OVER(ORDER BY CD.SortOrder ) AS RowNumber				
			,CD.GlobalAccountNumber AS GlobalAccountNumber
			,CD.AccountNo AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
			,(SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)) AS LastPaidAmount		
			,ISNULL(CONVERT(VARCHAR(50),(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)) ,106),'--') AS LastPaidDate
			,CD.OutStandingAmount As OverPayAmount
			,(SELECT dbo.fn_GetCustomerTotalBillsAmount(CD.GlobalAccountNumber)) As TotalBillsAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalPaidAmount(CD.GlobalAccountNumber)),0) As TotalPaidAmount
			,CD.BookCode AS BookCode
			,CD.OldAccountNo
			,CD.CycleName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.BusinessUnitName
			,CD.ServiceUnitName
			,CD.ServiceCenterName
		FROM [UDV_CustomerDescription] CD 
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.OutStandingAmount < 0
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
	)
	
	SELECT 
	(
		SELECT
			 RowNumber
			,GlobalAccountNumber
			,AccountNo
			,Name
			,ServiceAddress
			,LastPaidAmount		
			,LastPaidDate
			,OverPayAmount
			,TotalBillsAmount
			,TotalPaidAmount
			,BookCode
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(SELECT SUM(OverPayAmount) FROM PagedResults) AS TotalCreditBalance				
			,(Select COUNT(0) from PagedResults) as TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		FOR XML PATH('Reports'),TYPE
	)
	FOR XML PATH(''),ROOT('ReportsBeInfoByXml')

END
----------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
    
	--SELECT(  
		 SELECT   --BookChange.AccountNo
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
		   ,BookChange.OldBookNo  
		   ,BookChange.NewBookNo  
		   ,BookChange.Remarks  
		   ,BookChange.CreatedBy  
		   ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		   ,CustomerView.BusinessUnitName  
		   ,CustomerView.ServiceUnitName  
		   ,CustomerView.ServiceCenterName  
		   ,CustomerView.CycleName  
		   ,CustomerView.BookCode  
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.BookSortOrder AS BookSortOrder  
		   ,CustomerView.OldAccountNo
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder    
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_BookNoChangeLogs  BookChange  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId    
		 -- changed by karteek end    	       
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
----------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
     
	--SELECT(  
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy  
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 -- changed by karteek end    
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
END  
-----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END    
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
	SELECT(  
		SELECT    -- CA.GlobalAccountNumber 
			  (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
			 ,OldPostal_HouseNo  
			 ,OldPostal_StreetName   
			 ,OldPostal_City  
			 ,OldPostal_Landmark  
			 ,OldPostal_AreaCode  
			 ,OldPostal_ZipCode  
			 ,NewPostal_HouseNo  
			 ,NewPostal_StreetName   
			 ,NewPostal_City  
			 ,NewPostal_Landmark  
			 ,NewPostal_AreaCode  
			 ,NewPostal_ZipCode   
			 ,OldService_HouseNo  
			 ,OldService_StreetName  
			 ,OldService_City  
			 ,OldService_Landmark  
			 ,OldService_AreaCode  
			 ,OldService_ZipCode   
			 ,NewService_HouseNo   
			 ,NewService_StreetName  
			 ,NewService_City  
			 ,NewService_Landmark  
			 ,NewService_AreaCode  
			 ,NewService_ZipCode  
			 ,CA.Remarks  
			 ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
			 ,ApproveSttus.ApprovalStatus  
			 ,CA.CreatedBy
			 ,CustomerView.BusinessUnitName
			 ,CustomerView.ServiceUnitName
			 ,CustomerView.ServiceCenterName
			 ,CustomerView.SortOrder AS CustomerSortOrder  
			 ,CustomerView.CycleName
		     ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		     ,CustomerView.BookSortOrder     
			 ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		     ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerAddressChangeLog_New CA   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId     
		 -- changed by karteek end  
		FOR XML PATH('AuditTrayAddressList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
    
END 
---------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
	--SELECT(  
		 SELECT  --CCL.AccountNo 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,CCL.ApproveStatusId  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			   ,CCL.Remarks  
			   ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CCL.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder
			   ,CustomerView.CycleName
		       ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		       ,CustomerView.BookSortOrder      
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		-- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		 -- changed by karteek end       
	--	FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
-------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
	--SELECT(  
		 SELECT   --CNL.GlobalAccountNumber AS AccountNo  
			   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,CNL.Remarks  
			   ,CNL.OldTitle  
			   ,CNL.NewTitle  
			   ,CNL.OldFirstName  
			   ,CNL.NewFirstName  
			   ,CNL.OldMiddleName  
			   ,CNL.NewMiddleName  
			   ,CNL.OldLastName  
			   ,CNL.NewLastName  
			   ,CNL.OldKnownAs  
			   ,CNL.NewKnownAs  
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CNL.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId   
		 -- changed by karteek end    
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END 
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	--SELECT(  
		 SELECT  --CC.GlobalAccountNumber AS AccountNo
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,CC.Remarks  
		   ,CC.CreatedBy
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder      
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		-- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 -- changed by karteek end    
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END
----------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
	--SELECT(  
		 SELECT  --CMI.AccountNo
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
			   ,CMI.Remarks  
			   ,CMI.OldDials  
			   ,CMI.NewDials  
			   ,CMI.CreatedBy  
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CMI.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		       ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name 
			   ,CustomerView.OldAccountNo   
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		-- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId   
		 -- changed by karteek end    
	--	FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
-----------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 16-Sep-2014
-- Description:	Get The Details of The Customers With out Having No Name Or Address
-- Modified By : Karthik
-- Modified Date : 27-12-2014
-- Modified By : Padmini
-- Modified Date : 01/12/2015
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList_New]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)			
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')			
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber
				,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
				,CD.GlobalAccountNumber AS GlobalAccountNumber
				,CD.AccountNo AS AccountNo
				,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
				,ISNULL(OldAccountNo,'--') AS OldAccountNo
				,CD.CycleName
				,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
				,CD.SortOrder AS CustomerSortOrder
				,CD.BookSortOrder
				,CD.BusinessUnitName
				,CD.ServiceUnitName
				,CD.ServiceCenterName
				FROM UDV_CustomerDescription CD
				INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID 
						AND CD.ActiveStatusId=1 AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName='')
				INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
				INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
				INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
				INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
				INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber 
						AND PAD.IsServiceAddress = 0 AND (PAD.StreetName IS NULL OR PAD.StreetName = '' 
														 OR PAD.HouseNo IS NULL OR PAD.HouseNo = '' 
														 OR PAD.City IS NULL OR PAD.City = '')
				INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber 
						AND SAD.IsServiceAddress = 1 AND (SAD.StreetName IS NULL OR SAD.StreetName = '' 
														 OR SAD.HouseNo IS NULL OR SAD.HouseNo = '' 
														 OR SAD.City IS NULL OR SAD.City = '') 
	)
	
	SELECT 
	(
		SELECT
			 RowNumber
			,Name
			,GlobalAccountNumber
			,AccountNo
			,ServiceAddress
			,BusinessUnitName
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(Select COUNT(0) from PagedResults) as TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		FOR XML PATH('Reports'),TYPE
	)
	FOR XML PATH(''),ROOT('ReportsBeInfoByXml')
END
-------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 17-Sep-2014
-- Description:	Get The Details of The Customers Having Check Meters
-- Modified By : Padmini
-- Modified Date : 26-12-2014 
-- Modified By: Karteek.P
-- Modified Date: 30-03-2015  
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetCheckMetersList_New]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)			
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')			
		FROM @XmlDoc.nodes('RptCheckMetersBe') AS T(C)
		
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber
				,CD.GlobalAccountNumber
				,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
				,ISNULL(CMD.InitialReading,'--') AS InitialReading
				,ISNULL(CMR.CheckMeterCurrentReading,'--') AS CheckMeterCurrentReading
				,ISNULL(CMR.MeterCurrentReading,'--') AS MeterCurrentReading
				,CD.CycleName
				,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
				,CD.SortOrder AS CustomerSortOrder
				,CD.BookSortOrder
				,CD.BusinessUnitName
				,CD.ServiceUnitName
				,CD.ServiceCenterName
				FROM [UDV_CustomerDescription] CD 
				INNER JOIN Tbl_CheckMeterReadings CMR ON CD.GlobalAccountNumber = CMR.AccountNo AND CD.ActiveStatusId = 1
				INNER JOIN Tbl_CheckMeterDetails CMD ON CMR.AccountNo = CMD.AccountNo AND CMD.IsActive = 1
				INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID 
				INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
				INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
				INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
				INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo				
	)
	
	SELECT 
	(
		SELECT
			 RowNumber
			,GlobalAccountNumber
			,Name
			,InitialReading
			,CheckMeterCurrentReading
			,MeterCurrentReading
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(Select COUNT(0) from PagedResults) as TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		FOR XML PATH('RptCheckMeters'),TYPE
	)
	FOR XML PATH(''),ROOT('RptCheckMetersInfoByXml')
END
---------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Oct-2014
-- Description:	Get The Details of BatchNo Wise Payments list
-- Modified By: Karteek.P
-- Modified Date: 30-03-2015  
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetBatchWisePayments]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @PageSize INT
			,@PageNo INT
			,@BatchNo VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			
	SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BatchNo=C.value('(BatchNo)[1]','VARCHAR(MAX)')
			,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('RptBatchNoWisePaymentsBe') AS T(C)
		
	IF (@FromDate ='' OR @FromDate IS NULL)    
		BEGIN    
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
		END 
	IF (@Todate ='' OR @Todate IS NULL)    
		BEGIN    
			SET @Todate = dbo.fn_GetCurrentDateTime()    
		END 
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY BD.BatchNo) AS RowNumber
			,BD.BatchNo
			,BD.BatchName
			,ISNULL(CONVERT(VARCHAR(50),BD.BatchDate,106),'--') AS BatchDate
			,CONVERT(DECIMAL(18,2),BD.BatchTotal) AS TotalBatchAmount
			,ISNULL(CP.AccountNo,'--') AS AccountNo
			,ISNULL(CBP.BillNo,'--') AS BillNo
			,ISNULL(CP.ReceiptNo,'--') AS ReceiptNo
			,(SELECT ISNULL(PaymentMode,'--') FROM Tbl_MPaymentMode WHERE PaymentModeId = CP.PaymentMode) AS PaymentMode
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,(SELECT SUM(CP.PaidAmount)FROM Tbl_CustomerPayments CP WHERE BatchNo = BD.BatchNo) AS BatchTotal
		FROM Tbl_BatchDetails BD
		LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchNo
		LEFT JOIN Tbl_CustomerBillPayments CBP ON CBP.CustomerPaymentId = CP.CustomerPaymentID
		WHERE (BD.BatchNo IN (SELECT [com] FROM dbo.fn_Split(@BatchNo,',')))
			AND (BD.BatchDate BETWEEN @FromDate AND @ToDate)
	)
	
	SELECT 
	(
		SELECT	 
			 RowNumber
			,BatchNo		
			,BatchName
			,BatchDate
			,TotalBatchAmount
			,AccountNo
			,BillNo
			,ReceiptNo
			,PaymentMode
			,PaidAmount
			,BatchTotal
			,(Select COUNT(0) from PagedResults) as TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		FOR XML PATH('RptBatchNoWisePaymentsBe'),TYPE
	)
	FOR XML PATH(''),ROOT('RptBatchNoWisePaymentsBeInfoByXml')
END
--------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, U.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@TrxTypes,',')) TU ON TU.TypeId = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	  
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress AS ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAverageUploadReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')		 		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
	INTO #CustomerReadingsList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	  
	SELECT
	(
		SELECT RowNumber
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')				
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	--IF(@TrxFrom = '')
	--	BEGIN
	--		SELECT @TrxFrom = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
	--				 FROM MASTERS.Tbl_MMeterReadingsFrom 
	--				 FOR XML PATH(''), TYPE)
	--				.value('.','NVARCHAR(MAX)'),1,1,'')
	--	END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, 'Upload' AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
    --INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.SU_Id = CR.MeterReadingFrom 
	  
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT 
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')	
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, TotalAmountEffected
		, MR.Name AS AdjustmentName
		, CD.BusinessUnitName
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@AdjustmentTypes,',')) TU ON TU.TypeId = CR.BillAdjustmentType 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,AdjustmentName
			,(SELECT COUNT(0) FROM #CustomerReadingsList) AS TotalRecords
			,BusinessUnitName 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------
