
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, U.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@TrxTypes,',')) TU ON TU.TypeId = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	  
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress AS ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAverageUploadReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')		 		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	  
	SELECT
	(
		SELECT RowNumber
			,CAST(ROUND(AverageReading,0) AS VARCHAR(50)) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')				
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	--IF(@TrxFrom = '')
	--	BEGIN
	--		SELECT @TrxFrom = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
	--				 FROM MASTERS.Tbl_MMeterReadingsFrom 
	--				 FOR XML PATH(''), TYPE)
	--				.value('.','NVARCHAR(MAX)'),1,1,'')
	--	END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, 'Upload' AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
    --INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.SU_Id = CR.MeterReadingFrom 
	  
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT 
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')	
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, TotalAmountEffected
		, MR.Name AS AdjustmentName
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@AdjustmentTypes,',')) TU ON TU.TypeId = CR.BillAdjustmentType 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,AdjustmentName
			,TotalRecords
			,BusinessUnitName 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------
