GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 03/30/2015 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author		:  NEERAJ KANOJIYA
-- Create date	:  27-MARCH-2015
-- Description	:  GET CUSTOMERS FULL DETAILS
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  

    DECLARE  @GlobalAccountNumber VARCHAR(50)  
    SELECT            
	   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)     
		SELECT top 1
		
		CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord
		,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord
		,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord
		,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord
		,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs
		,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord
		,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord
		,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord
		,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord
		,CD.DocumentNo	
		,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate	
		,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate	
		,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate	
		,CD.OldAccountNo	
		,CT.CustomerType
		,BookCode
		,CD.PoleID	
		,CD.MeterNumber	
		,TC.ClassName as Tariff	
		,CPD.IsEmbassyCustomer	
		,CPD.EmbassyCode	
		,CD.PhaseId	
		,RC.ReadCode as ReadType
		,CD.IsVIPCustomer
		,CD.IsBEDCEmployee
		,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService	
		,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService	
		,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService
		,CASE PAD.AreaCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.AreaCode END AS AreaService
		,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode			
		,PAD.IsCommunication AS IsCommunicationService			
		,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal	
		,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal	
		,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL	
		,CASE PAD1.AreaCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.AreaCode END AS  AreaPostal		
		,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode
		,PAD.IsCommunication AS IsCommunicationPostal				
		,CD.ServiceAddressID		
		,CAD.IsCAPMI
		,InitialBillingKWh	
		,CAD.InitialReading	
		--,PresentReading	
		,CD.AvgReading as AverageReading	
		,CD.Highestconsumption	
		,CD.OutStandingAmount	
		,OpeningBalance
		,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1
		,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2
		,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType
		,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName
		,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName
		,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase
		,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName
		,CTD.TenentId
		,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent
		,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent
		,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent
		,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent
		,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent
		,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent
		,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent
		,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName
		,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy
		,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber
		,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount
		,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy
		,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy  
		from UDV_CustomerDescription CD
		left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId
		left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId
		left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0
		left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber 
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber
		LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId
		LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId
		LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId
		LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo
		LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId
		LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId
		Where CD.GlobalAccountNumber=@GlobalAccountNumber
		FOR XML PATH('CustomerRegistrationBE'),TYPE
END

------------------------------------------------------------------------------------------------------------
 
/****** Object:  StoredProcedure [dbo].[USP_UpdateBillingQueueDetails_Satya]    Script Date: 03/30/2015 17:47:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 08 JULY 2014
-- Description  : To Update Bill Generation Details
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBillingQueueDetails]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @BillingQueueScheduleId INT
			,@BillingMonth INT
			,@BillingYear INT
			,@BillSendingMode INT
			,@BillProcessTypeId INT
			--,@FeederId VARCHAR(50)
			,@CycleId VARCHAR(50)
			,@CreatedBy VARCHAR(50)
			,@CycleIds VARCHAR(MAX)
			,@Count INT = 0
			
	SELECT   
		@BillingMonth = C.value('(BillingMonth)[1]','INT')
		,@BillingYear = C.value('(BillingYear)[1]','INT')
		,@BillSendingMode = C.value('(BillSendingMode)[1]','INT')
		,@BillProcessTypeId = C.value('(BillProcessTypeId)[1]','INT')
		--,@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)		
	
	DECLARE @CycleTable TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))
	
	INSERT INTO @CycleTable
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
	
	SELECT TOP(1) @CycleId = CycleId FROM @CycleTable ORDER BY CycleId ASC  
	
	-------------------------------------------------------------------------
	delete from Tbl_BillingQueeCustomers 
	where BillingQueuescheduleId in (select BillingQueuescheduleId
	from Tbl_BillingQueueSchedule where	CycleId in  
	(SELECT [com] FROM dbo.fn_Split(@CycleIds,',') )
	and BillingMonth= @BillingMonth and BillingYear=@BillingYear  )
	
	delete from Tbl_BillingQueueSchedule
	where	CycleId in  (SELECT [com] FROM dbo.fn_Split(@CycleIds,',') )
	and BillingMonth= @BillingMonth and BillingYear=@BillingYear 
	----------------------------------------------------------------------------
     
	WHILE(EXISTS(SELECT TOP(1) CycleId FROM @CycleTable WHERE CycleId >= @CycleId ORDER BY CycleId ASC))    
		BEGIN 
			SET @BillingQueueScheduleId = 0
			SELECT @BillingQueueScheduleId = BillingQueueScheduleId 
			FROM Tbl_BillingQueueSchedule WHERE BillingMonth = @BillingMonth
			AND BillingYear = @BillingYear
			AND BillProcessTypeId = @BillProcessTypeId
			AND CycleId = @CycleId
			--AND((CycleId = @CycleId AND ISNULL(FeederId,'') = '') OR (ISNULL(CycleId,'') = '' AND FeederId = @FeederId))
			
			INSERT INTO Tbl_BillingQueueSchedule 
						  (
							BillingMonth
							,BillingYear
							,BillSendingMode
							,BillProcessTypeId
							,TotalCustomersInQueue
							,ServiceStartDate
							--,FeederId
							,CycleId
							,BillGenarationStatusId
							,OpenStatusId
							,CreatedBy
							,CreatedOn
						  )
					VALUES( @BillingMonth
							,@BillingYear
							,@BillSendingMode
							,@BillProcessTypeId
							,(select dbo.fn_GetNumOfBillCustomersInQueue())
							,(select dbo.fn_GetBillServiceTime())
							--,NULL
							,@CycleId
							,5
							,1
							,@CreatedBy
							,(select dbo.fn_GetCurrentDateTime())
							)
					SET @BillingQueueScheduleId = SCOPE_IDENTITY()
					
			 INSERT INTO Tbl_BillingQueeCustomers
					(BillingQueuescheduleId,AccountNo,[Month],[Year],BillGenarationStatusId)
			 select @BillingQueueScheduleId,GlobalAccountNumber,@BillingMonth,@BillingYear,5
			 from UDV_CustDetailsForBillGen   where CycleId=@CycleId
			 
			 					
		 
 			
 			SET @Count = @Count+ @@ROWCOUNT + 1
			
		  IF(@CycleId = (SELECT TOP(1) CycleId FROM @CycleTable ORDER BY CycleId DESC))    
			   BREAK    
		  ELSE    
			   BEGIN    
					SET @CycleId = (SELECT TOP(1) CycleId FROM @CycleTable WHERE  CycleId > @CycleId ORDER BY CycleId ASC)    
					IF(@CycleId IS NULL) break;    
					  Continue    
			   END    
	    END     	
			
	SELECT 
		@Count AS TotalRecords
	FOR XML PATH ('BillGenerationBe'),TYPE
	
	

		
END
