
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 03/30/2015 20:13:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(50),CB.PaymentLastDate) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(50),CB.PaymentLastDate) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),BD.ReadDate),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(50), CB.PaymentLastDate),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'--') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
    FROM Tbl_CustomerBills CB    
    INNER JOIN Tbl_BillDetails BD ON CB.CustomerBillId = BD.CustomerBillID 
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO


-----------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_CheckScheduledBillTime]    Script Date: 03/30/2015 20:04:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <19-March-2014>  
-- Description: <GET EBILL DAILY DETAILS>  
-- =============================================  
ALTER PROCEDURE [dbo].[UPS_CheckScheduledBillTime]  

AS  
BEGIN 
 DECLARE @IsExists BIT=0
 
 IF EXISTS(SELECT TOP 1 0 FROM Tbl_BillingQueueSchedule   
    WHERE (DATEADD(MI,DATEDIFF(MI,0,ServiceStartDate),0))=(DATEADD(MI,DATEDIFF(MI,0,
    (SELECT dbo.fn_GetCurrentDateTime())),0))  AND   BillGenarationStatusId=5   )   
 SET @IsExists=1;  
 
 
 SELECT @IsExists AS IsExists      
 FOR XML PATH('BillGenerationBe')  
END  
GO
--------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 03/30/2015 20:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya.K>
-- Create date: <10-Mar-2015>
-- Description:	<The purpose of this procedure is to generate the bills>
-- =============================================

--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges Decimal(18,2)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	--SELECT
	--		@FeederId = C.value('(FeederId)[1]','VARCHAR(MAX)')
	--		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
	--		,@BillingQueueScheduleIdList = C.value('(BillingQueueScheduleIdList)[1]','VARCHAR(MAX)')
	--	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)	
	
	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET 	@BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
	
	--set @CycleId = 'BEDC_C_0848'
	--set @BillingQueueScheduleIdList='580'
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		     
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
		
			 
			SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
			,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
			@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
			,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
			,@OpeningBalance=isnull(OpeningBalance,0)
			,@CustomerFullName=CustomerFullName
			,@BusinessUnitName =BusinessUnitName
			,@TariffName =TariffName
			--,@ReadDate =
			--,@Multiplier  
			,@Service_HouseNo =ServiceHouseNo
			,@Service_Street =ServiceStreet
			,@Service_City =ServiceCity
			,@ServiceZipCode  =ServiceZipCode
			,@Postal_HouseNo =ServiceHouseNo
			,@Postal_Street =Postal_StreetName
			,@Postal_City  =Postal_City
			,@Postal_ZipCode  =Postal_ZipCode
			,@Postal_LandMark =Postal_LandMark
			,@Service_LandMark =Service_LandMark
			,@OldAccountNumber=OldAccountNo
			FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
			 
			 
			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
			BEGIN
				
				SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
					,@RegenCustomerBillId=CustomerBillId	
				FROM Tbl_CustomerBills(NOLOCK)  
				WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
				DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
				
				DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
				SELECT @PaidAmount=SUM(PaidAmount)
				FROM Tbl_CustomerBillPayments(NOLOCK)  
				WHERE BillNo=@RegenCustomerBillId
				
				
			
				IF @PaidAmount IS NOT NULL
				BEGIN
									
					SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
					UPDATE Tbl_CustomerBillPayments
					SET BillNo=NULL
					WHERE BillNo=@RegenCustomerBillId
				END
				
				----------------------Update Readings as is billed =0 ----------------------------
				UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				
				
				-- Insert Paid Meter Payments
				DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				 
				update CUSTOMERS.Tbl_CustomerActiveDetails
				SET OutStandingAmount=@OutStandingAmount
				where GlobalAccountNumber=@GlobalAccountNumber
				----------------------------------------------------------------------------------
				
				
	 
				--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
				DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
				-------------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------------
				--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
				--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
				--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
				
				---------------------------------------------------------------------------------------
				--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
				---------------------------------------------------------------------------------------
			END
			
			
				IF @ReadCodeId=2 -- 
				BEGIN
					 
					SELECT  @PreviousReading=PreviousReading,
							@CurrentReading = PresentReading,@Usage=Usage,
							@LastBillGenerated=LastBillGenerated,
							@PreviousBalance =Previousbalance,
							@BalanceUnits=BalanceUsage,
							@IsFromReading=IsFromReading,
							@PrevCustomerBillId=CustomerBillId,
							@PreviousBalance=PreviousBalance,
							@Multiplier=Multiplier,
							@ReadDate=ReadDate
					FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
					
					
					
					IF @IsFromReading=1
					BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							IF @Usage<@BalanceUnits
							BEGIN
								SET @ActualUsage=@Usage
								SET @Usage=0
								SET @RemaningBalanceUsage=@BalanceUnits-@Usage
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @ActualUsage=@Usage
								SET @Usage=@Usage-@BalanceUnits
								SET @RemaningBalanceUsage=0
							END
					END
					ELSE
					BEGIN
							SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
					END
				END
				ELSE --@CustomerTypeID=1 -- Direct customer
				BEGIN
					set @IsEstimatedRead =1
					 
					-- Get balance usage of the customer
					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
								  @PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 
					
					IF @PreviousBalance IS NULL
					BEGIN
						SET @PreviousBalance=@OpeningBalance
					END

				
			 
					      
					SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
					SET @ReadType=1 -- Direct
					SET @ActualUsage=@Usage
				END
			IF  @ActiveStatusId !=2 -- In Active Customers
			BEGIN
				SET	 @Usage=0
			END
			
					
 			
			
			
			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0
			 
			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
			DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
			
			-- =============================================
			-- If customer is disable
			 
				--SELECT @BookDisableType=DisableTypeId 
				--FROM Tbl_BillingDisabledBooks 
				--WHERE BookNo=@BookNo AND YearId=@Year 
				--		AND MonthId=@Month AND 
				--		IsActive=1 AND ApproveStatusId=2
				 
				
				--IF @BookDisableType IS NULL
				--	SET @BookDisableType=-1
				--IF @BookDisableType<>-1 
				--BEGIN 
				--	SET @FixedCharges=0
				--END
				--ELSE
				--BEGIN
				--	INSERT INTO @tblFixedCharges(ClassID ,Amount)
				--	SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
				--	SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges					
				--END 
			----------------------------------------Disable Customer Verification---------------------------------------------------------------
		
			select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
					from Tbl_BillingDisabledBooks	  
					where BookNo=@BookNo and YearId=@Year and MonthId=@Month
				
			IF @IspartialClose IS NULL 
				BEGIN 
					SET @IspartialClose=-1
				END
			IF	@BookDisableType IS NULL
				BEGIN
					SET @BookDisableType=-1
				END
			IF  @IspartialClose =   1
				BEGIN
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					 
					IF @BookDisableType = 2  -- Temparary Close
						BEGIN
							SET	 @FixedCharges=0
						END
					ELSE
						BEGIN
							INSERT INTO @tblFixedCharges(ClassID ,Amount)
							SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
							SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
						END
				END
						  
			
			------------------------------------------------------------------------------------------------------------------------------------
			
			
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
			BEGIN
				
				IF @GetPaidMeterBalance<@FixedCharges
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=0
					SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
					SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
				END
				ELSE
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
					SET @FixedCharges=0
				END
			END
			------------------------
			-- Caluclate tax here
			
			SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			
			 
			IF	 @PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
							WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
							WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				
				END
			
			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0
			
			set @NetArrears=@OutStandingAmount-@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			
			
			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber
				Group BY CustomerBillId       
				ORDER BY CustomerBillId DESC 
			 
			if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage
			
			if @RemainingBalanceUnits IS NULL
			 set @RemainingBalanceUnits=0
			 
			 -------------------------------------------------------------------------------------------------------
			SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(Amount,'--') +' \n '  AS VARCHAR(50))

			FROM Tbl_LEnergyClassCharges 
			WHERE ClassID =@TariffId
			AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	
				FOR XML PATH(''), TYPE)
			 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
			 
			
			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
			
			 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			-----------------------------------------------------------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
				,TariffRates
			)        
			SELECT GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
				,BU_ID
				,SU_ID
				,ServiceCenterId
				,PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,ISNULL(@AverageUsageForNewBill,0)
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
				,@TariffCharges
			FROM @CustomerMaster C        
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL
			SET @IspartialClose=NULL
			SET @BookDisableType =NULL
			SET @TariffCharges=NULL
			
			-----------------------------------------------------------------------------------------
			
			--While Loop continue checking
			IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
				BREAK        
			ELSE        
			BEGIN     
			   --BREAK
			   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
			   IF(@GlobalAccountNumber IS NULL) break;        
				Continue        
			END
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		--SELECT * From dbo.fn_GetCustomerBillsForPrint(@CycleId,@Month,@Year)  
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[UPS_GetServiceCentersForBillPrint]    Script Date: 03/30/2015 20:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Satya.K>    
-- Create date: <10-Mar-2015>    
-- Description: <GET SERVICE CENTER LIST>      
-- =============================================    
    
ALTER PROCEDURE  [dbo].[UPS_GetServiceCentersForBillPrint]      
AS      
BEGIN     
     
SELECT  
(     
 SELECT CY.ServiceCenterId as ServiceCenterID ,BS.BillingYear as [Year],BillingMonth   as [Month]  
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,  
 SCDetails.ServiceUnitName,  
 dbo.fn_GetMonthName(BillingMonth)   as [MonthName]  
  from Tbl_BillingQueueSchedule BS    
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId    
 and BillGenarationStatusId=1   
 INNER JOIN  
 [UDV_ServiceCenterDetails] SCDetails  
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId  
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth   
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,  
 SCDetails.ServiceUnitName  
 FOR XML PATH('BillGenerationList'),TYPE      
)      
FOR XML PATH(''),ROOT('BillGenerationInfoByXml')       
        
END     
--------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerBillForFileCreation]    Script Date: 03/30/2015 20:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                          
-- Author  : Suresh kumar                     
-- Create date  : 31 OCT 2014                          
-- Description  : THIS function is use to get customers under the selecting cycle  
-- =======================================================================      
ALTER Procedure [dbo].[GetCustomerBillForFileCreation]  
  (
	@XmlDoc XML
  )        
AS      
BEGIN      
  DECLARE  @ServiceCenter VARCHAR(50)    
			,@Month VARCHAR(50)   
			,@Year INT  
  SELECT     
	@ServiceCenter = C.value('(ServiceCenterID)[1]','VARCHAR(50)')  
	,@Month = C.value('(Month)[1]','VARCHAR(50)')  
	,@Year = C.value('(Year)[1]','INT')  
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)           
	SELECT * from fn_GetCustomerBillsForPrint(@ServiceCenter,@Month,@Year)    
END  
-------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_UpdateBillingStatusAsBillGenarted]    Script Date: 03/30/2015 20:08:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <NEERAJ KANOJIYA>    
-- Create date: <19-March-2014>    
-- Description: <GET EBILL DAILY DETAILS>    
-- =============================================    
ALTER PROCEDURE  [dbo].[UPS_UpdateBillingStatusAsBillGenarted]   
(  
	@XmlDoc XML
)    
AS    
BEGIN   
    DECLARE @Month INT,  
			@Year INT,  
			@ServiceCenterID varchar(100)
	 SELECT     
			@ServiceCenterID = C.value('(ServiceCenterID)[1]','VARCHAR(50)')  
			,@Month = C.value('(Month)[1]','INT')  
			,@Year = C.value('(Year)[1]','INT')  
			FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)      
			
			 UPDATE  Tbl_BillingQueueSchedule  
			 SET  BillGenarationStatusId=1  
			 WHERE BillingMonth=@Month and BillingYear=@Year and  CycleId in ( SELECT CycleId 
																				FROM  Tbl_Cycles 
																				WHERE  ServiceCenterId =@ServiceCenterID  
																				)        
			SELECT @@ROWCOUNT AS RowsEffected
			FOR XML PATH('BillGenerationBe'),TYPE																				
END   
-------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_UpdateBillingStatusAsBillFileCreated]    Script Date: 03/30/2015 20:08:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <SATYA>    
-- Create date: <19-March-2014>    
-- Description: <GET EBILL DAILY DETAILS>    
-- Modified By:  <NEERAJ>    
-- Create date: <ADDED SERIALIZATION AND DESERIALI>  
-- =============================================    
ALTER PROCEDURE  [dbo].[UPS_UpdateBillingStatusAsBillFileCreated]   
(  
	@XmlDoc XML
)   
AS    
BEGIN   
  DECLARE @Month INT,  
			@Year INT,  
			@ServiceCenterID varchar(100),  
			@Filepath   varchar(MAx)  

 SELECT     
	@ServiceCenterID = C.value('(ServiceCenterID)[1]','VARCHAR(50)')  
	,@Month = C.value('(Month)[1]','INT')  
	,@Year = C.value('(Year)[1]','INT')  
	,@Filepath = C.value('(FilePath)[1]','VARCHAR(MAx)')  
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)      

	 UPDATE  Tbl_BillingQueueSchedule  
	 SET  BillGenarationStatusId=2,BillingFile=@Filepath  
	 WHERE  BillingMonth=@Month and BillingYear=@Year and  CycleId in ( SELECT CycleId 
																		from  Tbl_Cycles 
																		where  ServiceCenterId =@ServiceCenterID  
																		)  
	SELECT @@ROWCOUNT AS RowsEffected
	FOR XML PATH('BillGenerationBe'),TYPE																	
END   
-----------------------------------------------------------------------------------------------------    
GO

/****** Object:  StoredProcedure [dbo].[UPS_GetEmailList]    Script Date: 03/30/2015 20:09:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <SATYA>    
-- Create date: <20-March-2014>       
-- DESC: <GET CYCLE EMAIL LSIT BY SERVIC3 CENTER.>  
-- =============================================    
ALTER PROCEDURE  [dbo].[UPS_GetEmailList]   
(  
	@XmlDoc XML
)   
AS    
BEGIN   
  DECLARE 
		@ServiceCenterID varchar(100)
		,@CycleEmailList VARCHAR(MAX)
			
	 SELECT     
		@ServiceCenterID = C.value('(ServiceCenterID)[1]','VARCHAR(50)')   
		FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)      
	
				
	SELECT @CycleEmailList= (SELECT STUFF((SELECT '|' + CAST(PrimaryEmailId AS VARCHAR(50))  
	FROM Tbl_UserDetails 
	WHERE UserId in (
					 SELECT BS.CreatedBy  from Tbl_BillingQueueSchedule  BS
					 inner join Tbl_Cycles   CY
					 ON BS.CycleId=cy.CycleId      
					  where ServiceCenterId=@ServiceCenterID
					) 
	FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,1,'')  )
	SELECT @CycleEmailList AS CycleEmailList
	FOR XML PATH('BillGenerationBe'),TYPE	
	
END      
-----------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_UpdateBillingStatusAsEmailSent]    Script Date: 03/30/2015 20:09:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <SATYA>    
-- Create date: <19-March-2014>    
-- Description: <GET EBILL DAILY DETAILS>    
-- Modified By:  <NEERAJ>    
-- Create date: <ADDED SERIALIZATION AND DESERIALI>  
-- =============================================    
ALTER PROCEDURE  [dbo].[UPS_UpdateBillingStatusAsEmailSent]   
(  
	@XmlDoc XML
)   
AS    
BEGIN   
  DECLARE @Month INT,  
			@Year INT,  
			@ServiceCenterID varchar(100),  
			@Filepath   varchar(MAx)  

 SELECT     
	@ServiceCenterID = C.value('(ServiceCenterID)[1]','VARCHAR(50)')  
	,@Month = C.value('(Month)[1]','INT')  
	,@Year = C.value('(Year)[1]','INT')  
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)      

	 UPDATE  Tbl_BillingQueueSchedule  
	 SET  BillGenarationStatusId=3  
	 WHERE  BillingMonth=@Month and BillingYear=@Year and  CycleId in ( SELECT CycleId 
																		from  Tbl_Cycles 
																		where  ServiceCenterId =@ServiceCenterID  
																		)  
	SELECT @@ROWCOUNT AS RowsEffected
	FOR XML PATH('BillGenerationBe'),TYPE																	
END 
---------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_UpdateBillingStatusAsComplete]    Script Date: 03/30/2015 20:10:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <SATYA>    
-- Create date: <19-March-2014>    
-- Description: <GET EBILL DAILY DETAILS>    
-- Modified By:  <NEERAJ>    
-- Create date: <ADDED SERIALIZATION AND DESERIALI>  
-- =============================================    
ALTER PROCEDURE  [dbo].[UPS_UpdateBillingStatusAsComplete]   
(  
	@XmlDoc XML
)   
AS    
BEGIN   
  DECLARE @Month INT,  
			@Year INT,  
			@ServiceCenterID varchar(100),  
			@Filepath   varchar(MAx)  

 SELECT     
	@ServiceCenterID = C.value('(ServiceCenterID)[1]','VARCHAR(50)')  
	,@Month = C.value('(Month)[1]','INT')  
	,@Year = C.value('(Year)[1]','INT')  
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)      

	 UPDATE  Tbl_BillingQueueSchedule  
	 SET  BillGenarationStatusId=1  
	 WHERE  BillingMonth=@Month and BillingYear=@Year and  CycleId in ( SELECT CycleId 
																		from  Tbl_Cycles 
																		where  ServiceCenterId =@ServiceCenterID  
																		)  
	SELECT @@ROWCOUNT AS RowsEffected
	FOR XML PATH('BillGenerationBe'),TYPE																	
END 
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillingQueueDetails]    Script Date: 03/30/2015 20:11:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 08 JULY 2014
-- Description  : To Update Bill Generation Details
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBillingQueueDetails]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @BillingQueueScheduleId INT
			,@BillingMonth INT
			,@BillingYear INT
			,@BillSendingMode INT
			,@BillProcessTypeId INT
			--,@FeederId VARCHAR(50)
			,@CycleId VARCHAR(50)
			,@CreatedBy VARCHAR(50)
			,@CycleIds VARCHAR(MAX)
			,@Count INT = 0
			
	SELECT   
		@BillingMonth = C.value('(BillingMonth)[1]','INT')
		,@BillingYear = C.value('(BillingYear)[1]','INT')
		,@BillSendingMode = C.value('(BillSendingMode)[1]','INT')
		,@BillProcessTypeId = C.value('(BillProcessTypeId)[1]','INT')
		--,@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)		
	
	DECLARE @CycleTable TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))
	
	INSERT INTO @CycleTable
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
	
	SELECT TOP(1) @CycleId = CycleId FROM @CycleTable ORDER BY CycleId ASC  
	
	-------------------------------------------------------------------------
	delete from Tbl_BillingQueeCustomers 
	where BillingQueuescheduleId in (select BillingQueuescheduleId
	from Tbl_BillingQueueSchedule where	CycleId in  
	(SELECT [com] FROM dbo.fn_Split(@CycleIds,',') )
	and BillingMonth= @BillingMonth and BillingYear=@BillingYear  )
	
	delete from Tbl_BillingQueueSchedule
	where	CycleId in  (SELECT [com] FROM dbo.fn_Split(@CycleIds,',') )
	and BillingMonth= @BillingMonth and BillingYear=@BillingYear 
	----------------------------------------------------------------------------
     
	WHILE(EXISTS(SELECT TOP(1) CycleId FROM @CycleTable WHERE CycleId >= @CycleId ORDER BY CycleId ASC))    
		BEGIN 
			SET @BillingQueueScheduleId = 0
			SELECT @BillingQueueScheduleId = BillingQueueScheduleId 
			FROM Tbl_BillingQueueSchedule WHERE BillingMonth = @BillingMonth
			AND BillingYear = @BillingYear
			AND BillProcessTypeId = @BillProcessTypeId
			AND CycleId = @CycleId
			--AND((CycleId = @CycleId AND ISNULL(FeederId,'') = '') OR (ISNULL(CycleId,'') = '' AND FeederId = @FeederId))
			
			INSERT INTO Tbl_BillingQueueSchedule 
						  (
							BillingMonth
							,BillingYear
							,BillSendingMode
							,BillProcessTypeId
							,TotalCustomersInQueue
							,ServiceStartDate
							--,FeederId
							,CycleId
							,BillGenarationStatusId
							,OpenStatusId
							,CreatedBy
							,CreatedOn
						  )
					VALUES( @BillingMonth
							,@BillingYear
							,@BillSendingMode
							,@BillProcessTypeId
							,(select dbo.fn_GetNumOfBillCustomersInQueue())
							,(select dbo.fn_GetBillServiceTime())
							--,NULL
							,@CycleId
							,5
							,1
							,@CreatedBy
							,(select dbo.fn_GetCurrentDateTime())
							)
					SET @BillingQueueScheduleId = SCOPE_IDENTITY()
					
			 INSERT INTO Tbl_BillingQueeCustomers
					(BillingQueuescheduleId,AccountNo,[Month],[Year],BillGenarationStatusId)
			 select @BillingQueueScheduleId,GlobalAccountNumber,@BillingMonth,@BillingYear,5
			 from UDV_CustDetailsForBillGen   where CycleId=@CycleId
			 
			 					
		 
 			
 			SET @Count = @Count+ @@ROWCOUNT + 1
			
		  IF(@CycleId = (SELECT TOP(1) CycleId FROM @CycleTable ORDER BY CycleId DESC))    
			   BREAK    
		  ELSE    
			   BEGIN    
					SET @CycleId = (SELECT TOP(1) CycleId FROM @CycleTable WHERE  CycleId > @CycleId ORDER BY CycleId ASC)    
					IF(@CycleId IS NULL) break;    
					  Continue    
			   END    
	    END     	
			
	SELECT 
		@Count AS TotalRecords
	FOR XML PATH ('BillGenerationBe'),TYPE
	
END

