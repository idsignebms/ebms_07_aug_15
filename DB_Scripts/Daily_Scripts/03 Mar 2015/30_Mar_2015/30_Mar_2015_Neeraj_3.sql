GO
CREATE TABLE Tbl_MPaymentType(PaymentTypeID INT PRIMARY KEY IDENTITY(1,1),PaymentType VARCHAR(50),ActiveStatusID INT DEFAULT 1)

GO
INSERT INTO Tbl_MPaymentType(PaymentType) VALUES('Batch Payment')
GO
INSERT INTO Tbl_MPaymentType (PaymentType) VALUES('Bulk Upload')
GO
INSERT INTO Tbl_MPaymentType (PaymentType) VALUES('Customer Payment')
---------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillPayments]    Script Date: 03/27/2015 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 18-07-2014  
-- Description: Insert Bill Payments  
-- Modified By: Neeraj Kanojiya
-- Modified on: 4-Sep-2014
-- Modified By: Padmini
-- Modified Date : 27-12-2014
-- Updated By :NEERAJ KANOJIYA
-- Updated date: 27-MARCH-2014         
-- Description: ADDED PAYMENT TYPE
-- =============================================  
ALTER PROCEDURE [dbo].[USP_InsertBillPayments]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
    DECLARE  @CustomerID VARCHAR(50)  
    ,@AccountNo VARCHAR(50)  
    ,@ReceiptNo VARCHAR(20)  
    ,@PaymentMode INT  
    ,@DocumentPath VARCHAR(MAX)  
    ,@DocumentName VARCHAR(MAX)  
    --,@BillNo VARCHAR(50)  
    ,@Cashier VARCHAR(50)  
    ,@CashOffice INT  
    ,@CreatedBy VARCHAR(50)  
    ,@BatchNo INT  
    ,@PaidAmount DECIMAL(20,4)  
    ,@PaymentRecievedDate VARCHAR(20)  
    ,@CustomerPaymentId INT  
    ,@EffectedRows INT  
    ,@PaymentType INT
  DECLARE @PaidBillTemp TABLE (AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),BillNo VARCHAR(50),BillStatus INT)    
     
     
  SELECT   @CustomerID=C.value('(CustomerID)[1]','VARCHAR(50)')  
    ,@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
    ,@ReceiptNo=C.value('(ReceiptNo)[1]','VARCHAR(20)')  
    ,@PaymentMode=C.value('(PaymentModeId)[1]','INT')  
    ,@DocumentPath=C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
    ,@DocumentName=C.value('(Document)[1]','VARCHAR(MAX)')  
    --,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')  
    ,@BatchNo=C.value('(BatchNo)[1]','INT')  
    ,@PaidAmount=C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
    ,@PaymentRecievedDate=C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
    ,@PaymentType=C.value('(PaymentType)[1]','INT')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
   
  --SET @CustomerID=(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo);  
    
  --IF(NOT EXISTS(SELECT CustomerUniqueNo FROM Tbl_CustomerDetails where AccountNo=@AccountNo)) 
  
  IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	BEGIN
		SET @AccountNo=(SELECT GlobalAccountNumber FROM UDV_CustomerDescription 
							WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	END 
  
  
  IF(NOT EXISTS(SELECT GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] where GlobalAccountNumber=@AccountNo)) --Checking existence of customer befor payment
   SELECT 0 AS IsCustomerExists FOR XML PATH ('MastersBE')  
  ELSE  -- If customer is exists than do payment
   BEGIN     
   
    DELETE FROM @PaidBillTemp  --Flushing the temp table
    
    INSERT INTO @PaidBillTemp  --Inserting into temp table. If customer pay lumpsum amount for more than one bill then amount should be split according to bill.
    SELECT AccountNo,AmountPaid,BillNo,BillStatus FROM [dbo].[fn_GetBillPaymentDetails](@AccountNo,@PaidAmount)  --Called function, that returns table whic has splited amount according to bills by account no.
    WHERE AmountPaid>0  
         
    INSERT INTO Tbl_CustomerPayments(	--Inserting into Customer payment details table normal values.
         -- CustomerID  
           AccountNo  
         ,ReceiptNo  
         ,PaymentMode  
         ,DocumentPath  
         ,DocumentName  
         ,Cashier  
         ,CashOffice  
         ,CreatedBy  
         ,CreatedDate  
         ,BatchNo  
         ,RecievedDate  
         ,PaidAmount  
         ,ActivestatusId  
         ,PaymentType
         )  
       VALUES(    
        -- @CustomerID  
          @AccountNo  
         ,@ReceiptNo  
         ,@PaymentMode  
         ,@DocumentPath  
         ,@DocumentName  
         ,@Cashier  
         ,@CashOffice  
         ,@CreatedBy  
         ,dbo.fn_GetCurrentDateTime()  
         ,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)   
         ,@PaymentRecievedDate  
         ,@PaidAmount  
         ,1
         ,@PaymentType
         )  
       
    SELECT @CustomerPaymentId  = SCOPE_IDENTITY()       
      
    INSERT INTO Tbl_CustomerBillPayments(CustomerPaymentId,PaidAmount,BillNo,CreatedBy,CreatedDate)  --Inserting into payment details table with splited details.    
    SELECT @CustomerPaymentId,AmountPaid,BillNo,@CreatedBy,dbo.fn_GetCurrentDateTime() FROM @PaidBillTemp  
         
    UPDATE CB   
     SET PaymentStatusID = PT.BillStatus  
    FROM Tbl_CustomerBills CB  
    JOIN @PaidBillTemp PT ON CB.BillNo = PT.BillNo  
    WHERE CB.AccountNo = @AccountNo  
    
  --  UPDATE Tbl_CustomerDetails 
		--SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)--(SELECT [dbo].[fn_GetCustomerTotalDueAmount] (@AccountNo))
  --  WHERE AccountNo = @AccountNo
	 
    UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
      SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)--(SELECT [dbo].[fn_GetCustomerTotalDueAmount] (@AccountNo))
    WHERE GlobalAccountNumber = @AccountNo
       
    SELECT @EffectedRows = @@ROWCOUNT  
      
    SELECT   
      1 AS IsCustomerExists  
     ,(CASE WHEN @EffectedRows > 0 THEN 1 ELSE 0 END)AS IsSuccess  
    -- ,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount  
    --FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo  
    FOR XML PATH ('MastersBE')   
       
   END  
END
-------------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerPaymentsDocument]    Script Date: 03/27/2015 12:22:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  :Naresh T          
-- Create date: 07-04-2014    
-- Updated By :Suresh Kumar D
-- Updated date: 1-NOV-2014         
-- Description: Insert Customer Bulk Payments based on flag.  
-- Updated By :NEERAJ KANOJIYA
-- Updated date: 27-MARCH-2014         
-- Description: ADDED PAYMENT TYPE  
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertCustomerPaymentsDocument]          
 (          
  @XmlDoc XML           
 ,@MultiXmlDoc XML          
 )          
AS          
BEGIN          
  DECLARE @CreatedBy VARCHAR(50)          
    ,@EffectedRows INT = 0         
    ,@DocumentPath VARCHAR(MAX)          
    ,@DocumentName VARCHAR(MAX)         
    ,@BatchNo   VARCHAR(50)   
    ,@Flag INT
 
  DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),ReceiptNo VARCHAR(20),           
         PaymentMode VARCHAR(20),PaymentModeId INT, AmountPaid DECIMAL(18,2),          
         RecievedDate DATETIME)            
              
  SELECT            
   @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')          
   ,@DocumentPath=C.value('(DocumentPath)[1]','VARCHAR(MAX)')          
   ,@DocumentName=C.value('(DocumentName)[1]','VARCHAR(MAX)')        
   ,@BatchNo=C.value('(BatchNo)[1]','INT')   
   ,@Flag=C.value('(Flag)[1]','INT') 
  FROM @XmlDoc.nodes('PaymentsBe') AS T(C)     
    
    
   INSERT INTO @TempCustomer(AccountNo ,ReceiptNo,PaymentMode,PaymentModeId,AmountPaid,RecievedDate)           
  SELECT           
    T.c.value('(AccountNo)[1]','VARCHAR(50)')           
   ,T.c.value('(ReceiptNO)[1]','VARCHAR(20)')           
   ,T.c.value('(PaymentMode)[1]','VARCHAR(20)')          
   ,T.c.value('(PaymentModeId)[1]','VARCHAR(20)')          
   ,T.c.value('(PaidAmount)[1]','DECIMAL(18,2)')          
   ,T.c.value('(PaymentDate)[1]','DATETIME')          
  FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Table1') AS T(c)   
    
  DECLARE @ResultedTable TABLE(Id INT IDENTITY(1,1),AccountNo VARCHAR(50),ReceiptNo VARCHAR(20),          
          PaymentMode VARCHAR(20),PaymentModeId INT,AmountPaid DECIMAL(18,2),            
         BillNo VARCHAR(50),RecievedDate DATETIME,BillStatus INT,PaidDate DATETIME)              
           
  DECLARE @PresentAccountNo VARCHAR(50)  
   ,@TotalPaidAmount DECIMAL(18,2)  
    
  --looping for accurate payment start  
  SELECT TOP(1) @PresentAccountNo = AccountNo FROM @TempCustomer ORDER BY AccountNo ASC    
     
  WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @TempCustomer WHERE AccountNo >= @PresentAccountNo ORDER BY AccountNo ASC))    
		BEGIN     
     
			  SELECT @EffectedRows =  @EffectedRows + 1
		
			  SELECT @TotalPaidAmount = SUM(AmountPaid) FROM @TempCustomer WHERE AccountNo = @PresentAccountNo  
			   
			   --- Split the Paid Amount to Customer Pending Bills  START
			  INSERT INTO @ResultedTable(AccountNo,AmountPaid,BillNo,PaymentMode,PaymentModeId,ReceiptNo,RecievedDate,BillStatus)   
			  SELECT A.AccountNo,A.AmountPaid,BillNo,PaymentMode,PaymentModeId,ReceiptNo,RecievedDate,BillStatus   
			  FROM( (SELECT AccountNo,AmountPaid,BillNo,BillStatus 
						FROM dbo.fn_GetBillPaymentDetails(@PresentAccountNo,@TotalPaidAmount)) AS A  
						LEFT JOIN(SELECT TOP(1) 
										ReceiptNo,AccountNo,PaymentMode,PaymentModeId,RecievedDate  
								  FROM @TempCustomer WHERE AccountNo = @PresentAccountNo  
								  ORDER BY ReceiptNo DESC) AS B
								   ON A.AccountNo = B.AccountNo )   
		     
			   --- Split the Paid Amount to Customer Pending Bills   END
		     
				IF(@Flag=1)--INSERT IN LOG TABLE FOR APPROVAL  
					 BEGIN  
						INSERT INTO Tbl_CustomerPaymentsApproval 
							(	
							--CustomerID,
							AccountNo,ReceiptNo,PaymentMode,PaidAmount
								,RecievedDate,DocumentPath,DocumentName,CreatedBy          
								,CreatedDate,BatchNo         
							 )          
						SELECT           
						--	(Select CustomerUniqueNo  from Tbl_CustomerDetails where AccountNo=C.AccountNo) 
							(Select GlobalAccountNumber from [CUSTOMERS].[Tbl_CustomerSDetail] where GlobalAccountNumber=C.AccountNo)           
							  -- ,AccountNo          
							   ,ReceiptNo          
							   ,PaymentModeId
							   ,AmountPaid          
							   ,RecievedDate          
							   ,@DocumentPath          
							   ,@DocumentName          
							   ,@CreatedBy          
							   ,dbo.fn_GetCurrentDateTime()         
							   ,@BatchNo         
						FROM @TempCustomer C WHERE AccountNo = @PresentAccountNo          
					END       
				ELSE IF(@Flag=2)--INSERT IN MAIN TABLE IF NO APPROVAL  
					BEGIN  
						DECLARE @CustomerPaymentId INT  	
							
						INSERT INTO Tbl_CustomerPayments
							(
							--CustomerID,
							 AccountNo,ReceiptNo
							 ,PaymentMode          
							 ,PaidAmount
							 ,RecievedDate
							 ,DocumentPath
							 ,DocumentName          
							 ,CreatedBy
							 ,CreatedDate
							 ,BatchNo     
							 ,PaymentType    
							 )          
						SELECT           
							 --(Select CustomerUniqueNo  from Tbl_CustomerDetails where AccountNo = C.AccountNo)   
                              (Select GlobalAccountNumber from [CUSTOMERS].[Tbl_CustomerSDetail] where GlobalAccountNumber=C.AccountNo)                     
							 --,AccountNo          
							 ,ReceiptNo          
							 ,PaymentModeId      
							 ,AmountPaid          
							 ,RecievedDate          
							 ,@DocumentPath          
							 ,@DocumentName          
							 ,@CreatedBy          
							 ,dbo.fn_GetCurrentDateTime()            
							 ,@BatchNo   
							 ,2--Bulk Upload     
						   FROM @TempCustomer C WHERE AccountNo = @PresentAccountNo          
						   
							SELECT @CustomerPaymentId = SCOPE_IDENTITY()  
						   
						    INSERT INTO Tbl_CustomerBillPayments(  
								 CustomerPaymentId  
								 ,BillNo  
								 ,CreatedBy  
								 ,CreatedDate
								 ,PaidAmount)  
							 SELECT           
								 @CustomerPaymentId  
								 ,BillNo  
								 ,@CreatedBy          
								 ,dbo.fn_GetCurrentDateTime() 
								 ,AmountPaid           
							FROM @ResultedTable C WHERE AccountNo = @PresentAccountNo						   
					END	
				
				 -- UPDATE Tbl_CustomerDetails 
					--SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@TotalPaidAmount,0)
				 -- WHERE AccountNo = @PresentAccountNo
				 
				  UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@TotalPaidAmount,0)
				  WHERE GlobalAccountNumber = @PresentAccountNo
		      
		   IF(@PresentAccountNo = (SELECT TOP(1) AccountNo FROM @TempCustomer ORDER BY AccountNo DESC))    
			 BREAK    
		   ELSE    
			BEGIN    
			  SET @PresentAccountNo = (SELECT TOP(1) AccountNo FROM @TempCustomer WHERE  AccountNo > @PresentAccountNo ORDER BY AccountNo ASC)    
			  IF(@PresentAccountNo IS NULL) break;    
			   Continue    
			 END    
		  END    
  --looping for accurate payment end    
   
	  SELECT   
		@EffectedRows  As EffectedRows
	  FOR XML PATH('PaymentsBe'),TYPE     
                       
           
END
-------------------------------------------------------------------------------------------------------------------------
GO
UPDATE Tbl_CustomerPayments SET PaymentType=3 WHERE BatchNo IS NULL
GO
UPDATE Tbl_CustomerPayments SET PaymentType=1 WHERE BatchNo IS NOT NULL
GO
GO
/****** Object:  View [dbo].[UDV_PrebillingRpt]    Script Date: 03/30/2015 20:32:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[UDV_PrebillingRpt]  

AS 
 SELECT CD.GlobalAccountNumber
	,CD.AccountNo
	,CD.OldAccountNo
	,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName
	,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS [ServiceAddress] 
   ,PD.SortOrder
   ,BN.SortOrder AS BookSortOrder
   ,CD.ConnectionDate
   ,PD.PoleID  
   ,PD.TariffClassID AS TariffId  
   ,TC.ClassName  
   ,PD.ReadCodeID  
   ,BN.BookNo  
   ,BN.BookCode
   ,BN.ID AS BookId  
   ,BU.BU_ID  
   ,BU.BusinessUnitName  
   ,BU.BUCode  
   ,SU.SU_ID  
   ,SU.ServiceUnitName  
   ,SU.SUCode  
   ,SC.ServiceCenterId  
   ,SC.ServiceCenterName  
   ,SC.SCCode  
   ,C.CycleId  
   ,C.CycleName  
   ,C.CycleCode
   ,Pd.MeterNumber
   ,CAD.InitialBillingKWh as DeafaultUsage
   ,CustomerStatus.StatusId as CustomerStatusID
   ,PD.IsEmbassyCustomer 
   FROM CUSTOMERS.Tbl_CustomerSDetail AS CD 
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
  INNER Join CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID
  INNER JOIn Tbl_MCustomerStatus AS CustomerStatus ON   CD.ActiveStatusId = CustomerStatus.StatusId 
----------------------------------------------------------------------------------------------------


GO

