GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Customer Bill Generation','../ConsumerManagement/GenerateBillByCustomer.aspx',1,18) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Adjustment (No Bill)','../ConsumerManagement/Adjustment_NoBill.aspx',1,19) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Get Customer Details','../ConsumerManagement/SearchCustomer.aspx',1,20) 
 GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@@IDENTITY,1,0,'Admin',GETDATE(),1)
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE   @AccountNo VARCHAR(50)
				 ,@ModifiedBy VARCHAR(50)
				 ,@MeterNo VARCHAR(100)
				 ,@MeterTypeId INT
				 ,@MeterDials INT
				 ,@Flag INT  
				 ,@Details VARCHAR(MAX)
				 ,@FunctionId INT
				 ,@PresentRoleId INT
				 ,@NextRoleId INT
				 ,@OldMeterReading VARCHAR(20)
				 ,@NewMeterReading VARCHAR(20)
				 ,@BU_ID VARCHAR(50)
				 ,@OldMeterNo VARCHAR(50)
				 ,@InitialBillingkWh INT
				 ,@OldMeterReadingDate DATETIME
				 ,@NewMeterReadingDate DATETIME
				 ,@NewMeterInitialReading VARCHAR(20)
				 
       
       
	   SELECT
			 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
			,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
			,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
			,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
			,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
			,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
			,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
			,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
			
			
		FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		 BEGIN  
		  SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		 BEGIN  
		  SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		 BEGIN  
		  SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		 END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
	BEGIN  
		SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
	END
	ELSE IF(@Flag = 1)
	 BEGIN
		
		SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
		FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)
		
		DECLARE @PrvMeterNo VARCHAR(50)
		SET @PrvMeterNo=(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber=@AccountNo)
		
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,PresentApprovalRole
														,NextApprovalRole
														,OldMeterReading
														,NewMeterReading)
		SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,0
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
		
	 SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	 END 
 ELSE
	BEGIN
	
			
		INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
														,OldMeterNo
														,NewMeterNo
														,OldMeterTypeId
														,NewMeterTypeId
														,OldDials
														,NewDials
														,CreatedBy
														,CreatedDate
														,ApproveStatusId
														,Remarks
														,OldMeterReading
														,NewMeterReading
														,MeterChangedDate)
		SELECT   GlobalAccountNumber
				--,CASE CD.MeterNumber WHEN '' THEN NULL ELSE CD.MeterNumber END
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
		WHERE GlobalAccountNumber=@AccountNo
    
		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET
				MeterNumber=@MeterNo
			   ,ModifedBy=@ModifiedBy
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		--START Old MeterREading Taken and insert in to Customer Bills Table
		
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET
				InitialReading=@InitialBillingkWh, PresentReading=@NewMeterReading
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
		SET @Usage=	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
		SET @ReadBy=(SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)
		SET @PrvMeterDials=(SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo=@PrvMeterNo)
		
		
		IF (@Usage > 0)
			BEGIN
				
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@OldMeterReadingDate
									,@ReadBy
									,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
									,@OldMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@Usage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@OldMeterNo)
			END
		-- END Old MeterREading Taken and insert in to Customer Bills Table
		
		-- START NEW MeterREading Taken and insert in to Customer Bills Table
		
		--If New MeterNo assighned to that customer
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading=@NewMeterReading,
			InitialReading=@NewMeterInitialReading
		WHERE GlobalAccountNumber=@AccountNo
		
		DECLARE @NewMeterUsage VARCHAR(50)
		SET @NewMeterUsage=	(ISNULL(CONVERT(BIGINT,@NewMeterReading),0)) - (ISNULL(CONVERT(BIGINT,@NewMeterInitialReading),0))
		
		IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
			BEGIN		
				DECLARE @NewMeterDials INT		
				SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
				INSERT INTO Tbl_CustomerReadings
									(GlobalAccountNumber
									,ReadDate
									,[ReadBy]
									,PreviousReading
									,PresentReading
									,[AverageReading]
									,Usage
									,[TotalReadingEnergies]
									,[TotalReadings]
									,Multiplier
									,ReadType
									,[CreatedBy]
									,CreatedDate
									,[IsTamper]
									,MeterNumber)
									
				VALUES				(@AccountNo
									,@NewMeterReadingDate
									,@ReadBy									
									,@NewMeterInitialReading
									,@NewMeterReading
									,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
									,@NewMeterUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@NewMeterUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
									,1
									,2
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime()
									,0
									,@MeterNo)
			END
		-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
		SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 17 OCT 2014
-- Description  : To Check the Readings are Exists or not to Cycles based on Month
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- =============================================
ALTER PROCEDURE [USP_IsCycleReadingsExists_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@CycleIds VARCHAR(MAX)
			,@MonthStart DATETIME
			,@MonthEnd DATETIME
			,@FromDate DATETIME
			,@ToDate DATETIME
			,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
			,@CycleId VARCHAR(50)
			,@PresentId INT
			
	DECLARE @TempCycles TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))		
	DECLARE @NotEstimatedCycles TABLE(CycleId VARCHAR(50))	
	DECLARE @CycleLastBilledDates TABLE(CycleId VARCHAR(50),LastBilledDate Datetime)
	DECLARE @ReadingCycles TABLE(CycleId VARCHAR(50))	
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	DELETE FROM @TempCycles
	
	INSERT INTO @TempCycles
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
	
	INSERT INTO  @CycleLastBilledDates
	SELECT C.CycleId,MAX(BillGeneratedDate) AS LastBilledDate  
	FROM @TempCycles C
	LEFT JOIN Tbl_CustomerBills CB ON CB.CycleId=C.CycleId 
	GROUP BY C.CycleId 	


	--Checking for Reading from last billed date to from date 
	INSERT INTO @ReadingCycles
	SELECT DISTINCT B.CycleId FROM Tbl_CustomerReadings CR
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers B ON B.BookNo=CPD.BookNo
	INNER JOIN @CycleLastBilledDates C ON B.CycleId=C.CycleId 
	WHERE CR.CreatedDate> C.LastBilledDate OR CR.CreatedDate<GETDATE()
	
	 
	--Getting the cycle ids doesn't have readings
	INSERT INTO @NotEstimatedCycles 
	SELECT CycleId FROM @TempCycles
	EXCEPT 
	SELECT CycleId FROM @ReadingCycles
	 
	IF EXISTS (SELECT 0 FROM @NotEstimatedCycles)
		BEGIN
			SELECT
			(
				SELECT 
					0 AS IsExists
					,R.CycleId
					,C.CycleName
				FROM @NotEstimatedCycles R
				LEFT JOIN Tbl_Cycles C ON R.CycleId = C.CycleId	
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					1 AS IsExists
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
		END		
END
GO