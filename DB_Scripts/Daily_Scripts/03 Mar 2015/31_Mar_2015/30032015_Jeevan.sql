GO
/****** Object:  StoredProcedure [dbo].[USP_IsCycleReadingsExists_Month]    Script Date: 03/30/2015 20:03:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 17 OCT 2014
-- Description  : To Check the Readings are Exists or not to Cycles based on Month
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015  
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCycleReadingsExists_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@CycleIds VARCHAR(MAX)
			,@MonthStart DATETIME
			,@MonthEnd DATETIME
			,@FromDate DATETIME
			,@ToDate DATETIME
			,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
			,@CycleId VARCHAR(50)
			,@PresentId INT
			
	DECLARE @TempCycles TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))		
	DECLARE @NotEstimatedCycles TABLE(CycleId VARCHAR(50))	
	DECLARE @CycleLastBilledDates TABLE(CycleId VARCHAR(50),LastBilledDate Datetime)

		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	DELETE FROM @TempCycles
	
	INSERT INTO @TempCycles
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
	
	INSERT INTO  @CycleLastBilledDates
	SELECT C.CycleId,MAX(BillGeneratedDate) AS LastBilledDate  
	FROM @TempCycles C
	LEFT JOIN Tbl_CustomerBills CB ON CB.CycleId=C.CycleId 
	GROUP BY C.CycleId 	

	--Getting the cycle ids doesn't have readings
	INSERT INTO @NotEstimatedCycles 
		SELECT CycleId FROM @TempCycles
		
		EXCEPT 
		--Checking for Reading from last billed date to from date 
		SELECT DISTINCT B.CycleId FROM Tbl_CustomerReadings CR WITH (NOLOCK)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber
		INNER JOIN Tbl_BookNumbers B ON B.BookNo=CPD.BookNo
		INNER JOIN @CycleLastBilledDates C ON B.CycleId=C.CycleId 
		WHERE CR.CreatedDate> C.LastBilledDate OR CR.CreatedDate<= dbo.fn_GetCurrentDateTime()
	 
	IF EXISTS (SELECT 0 FROM @NotEstimatedCycles)
		BEGIN
			SELECT
			(
				SELECT 
					0 AS IsExists
					,R.CycleId
					,C.CycleName
				FROM @NotEstimatedCycles R
				LEFT JOIN Tbl_Cycles C ON R.CycleId = C.CycleId	
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					1 AS IsExists
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
		END		
END

GO
/****** Object:  StoredProcedure [dbo].[USP_IsCycleEstimationExists_Month]    Script Date: 03/30/2015 19:59:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 20 OCT 2014
-- Description  : To Check the Estimations are Exists or not to Cycles based on Month
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015  
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCycleEstimationExists_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@CycleIds VARCHAR(MAX)
			,@MonthStart DATETIME
			,@MonthEnd DATETIME
			,@FromDate DATETIME
			,@ToDate DATETIME
			,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
			,@CycleId VARCHAR(50)
			,@PresentId INT

	DECLARE @TempCycles TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))		
	DECLARE @NotEstimatedCycles TABLE(CycleId VARCHAR(50))		
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	DELETE FROM @TempCycles
	DELETE FROM @NotEstimatedCycles
	
	INSERT INTO @TempCycles
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
		
	INSERT INTO 	@NotEstimatedCycles
		SELECT CycleId FROM @TempCycles
		EXCEPT
		SELECT ES.CycleId FROM Tbl_EstimationSettings ES WITH (NOLOCK)
		INNER JOIN @TempCycles C  ON C.CycleId=ES.CycleId
		WHERE BillingMonth=@Month AND BillingYear=@Year

	IF EXISTS (SELECT 0 FROM @NotEstimatedCycles)
		BEGIN
			SELECT
			(
				SELECT 
					0 AS IsExists
					,R.CycleId
					,C.CycleName
				FROM @NotEstimatedCycles R
				LEFT JOIN Tbl_Cycles C ON R.CycleId = C.CycleId	
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					1 AS IsExists
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
		END	
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_IsAdjustmentBacthesClose_Month]    Script Date: 03/30/2015 20:08:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 18 OCT 2014
-- Description  : To Check the Adjustment related old batches are close not to on Month
-- Modified By: T.karthik
-- Modified date: 30-12-2014
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsAdjustmentBacthesClose_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
	DECLARE @TempTable TABLE(BatchID INT,BatchNo INT,BatchTotal DECIMAL(18,2),UsedBatchTotal DECIMAL(18,2))
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	

	INSERT INTO @TempTable(BatchID,BatchNo,BatchTotal,UsedBatchTotal)			
	SELECT
		 BatchID
		 ,BatchNo
		,BatchTotal
		,(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments CP  WITH (NOLOCK) WHERE BatchNo IS NOT NULL AND CP.BatchNo = BD.BatchID)
	FROM Tbl_BATCH_ADJUSTMENT BD WITH (NOLOCK)

	IF EXISTS(SELECT 0 FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0))
		BEGIN
			SELECT 1 AS IsExists,SUBSTRING(
					(SELECT ','+CONVERT(VARCHAR(MAX),BatchNo) FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0) FOR XML PATH('')
						),2,10000000000) AS OpenBatches
			FOR XML PATH('BillGenerationBe'),TYPE			
		END
	ELSE
		BEGIN
			SELECT 0 AS IsExists
			FOR XML PATH('BillGenerationBe'),TYPE
		END	
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_IsPaymentsBacthesClose_Month]    Script Date: 03/30/2015 20:13:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 18 OCT 2014
-- Description  : To Check the Payments related old batches are close not to on Month
-- Modified By: T.karthik
-- Modified date: 30-12-2014
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsPaymentsBacthesClose_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
	DECLARE @TempTable TABLE(BatchNo INT,BatchTotal DECIMAL(18,2),UsedBatchTotal DECIMAL(18,2))
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	

	INSERT INTO @TempTable(BatchNo,BatchTotal,UsedBatchTotal)			
	SELECT
		 BatchNo
		,BatchTotal
		,(SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments CP WITH (NOLOCK) WHERE BatchNo IS NOT NULL AND CP.BatchNo = BD.BatchNo)
	FROM Tbl_BatchDetails BD WITH (NOLOCK)
	
	IF EXISTS(SELECT 0 FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0))
		BEGIN
			SELECT 
				1 AS IsExists
				,SUBSTRING(
					(SELECT ','+CONVERT(VARCHAR(MAX),BatchNo) FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0) FOR XML PATH('')
						),2,10000000000) AS OpenBatches
			FOR XML PATH('BillGenerationBe'),TYPE			
		END
	ELSE
		BEGIN
			SELECT 0 AS IsExists
			FOR XML PATH('BillGenerationBe'),TYPE
		END	
END
GO