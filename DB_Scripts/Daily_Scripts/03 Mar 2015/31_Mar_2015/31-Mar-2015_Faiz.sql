GO
-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 10-JAN-2014    
-- Description: GET UDF LIST    
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_GetUserDefinedControls]    
AS      
BEGIN      
 SELECT    
 (    
  SELECT     
    UDCD.UDCId    
    ,UDCD.FieldName    
    ,UDCD.ControlTypeId    
    ,UDCD.IsMandatory    
    ,UDCD.IsActive    
    ,CRM.ControlTypeName    
  FROM [MASTERS].[Tbl_USerDefinedControlDetails] AS UDCD     
  LEFT JOIN [MASTERS].[Tbl_ControlRefMaster] AS CRM ON UDCD.UDCId= CRM.ControlTypeId AND UDCD.IsActive=1    
  FOR XML PATH('UserDefinedControlsListBE_1'),TYPE    
 ),    
 (    
  SELECT     
    MRefId    
    ,VText     
    ,UDCId    
  FROM MASTERS.[Tbl_MControlRefMaster]      
  WHERE IsActive=1    
  FOR XML PATH('UserDefinedControlsListBE_2'),TYPE    
 )    
 FOR XML PATH(''),ROOT('UserDefinedControlsInfoByXml')    
END 
GO
---------------------------------------------------------------------------------------------------
GO
-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Modified By: V.Bhimaraju    
-- Modified Date:28-Mar-2014    
-- ModifiedBy : T.Karthik    
-- Modified date: 03-12-2014    
-- Description: The purpose of this procedure is to insert Book Number    
--- Modified By: Padmini    
-- Modified Date:17/02/2015    
-- Description: Inserting only Cycle Id    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_InsertBookNumber]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
 @ID VARCHAR(50)  
 ,@BookNo VARCHAR(20)  
 ,@CreatedBy VARCHAR(50)    
 --,@BU_ID VARCHAR(20)    
 ,@CycleId VARCHAR(20)    
 ,@MarketerId INT    
 --,@SU_ID VARCHAR(20)    
 ,@ServiceCenterId VARCHAR(20)    
 ,@NoOfAccouns INT,@Details VARCHAR(MAX),@BookCode VARCHAR(10),@SortOrder INT    
      
 SELECT    
  @ID=C.value('(BookNo)[1]','VARCHAR(20)')    
  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')    
  --,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')    
  --,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')    
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')    
  ,@NoOfAccouns=C.value('(NoOfAccounts)[1]','INT')    
  ,@MarketerId=C.value('(MarketerId)[1]','INT')    
  ,@CycleId=C.value('(CycleId)[1]','VARCHAR(20)')    
  --,@BookCode=C.value('(BookCode)[1]','VARCHAR(10)')    
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)    
   
  
     
 SET @SortOrder = (SELECT MAX(SortOrder) FROM Tbl_BookNumbers)    
 SET @SortOrder = CASE WHEN @SortOrder IS NULL  THEN 0 ELSE @SortOrder END     
     
 SET @BookCode=MASTERS.fn_BookCodeGenerate(@ServiceCenterId);    
 SET @BookNo=dbo.fn_GenerateUniqueId(11);  
   
  
   
 IF(@BookCode IS NOT NULL)    
 BEGIN    
 IF EXISTS(SELECT 0 FROM Tbl_BookNumbers WHERE ID=@ID AND CycleId=@CycleId)    
 BEGIN    
  SELECT 1 AS IsBookNumberExists,@BookCode AS BookCode FOR XML PATH('MastersBE')    
 END    
 --ELSE IF EXISTS(SELECT 0 FROM Tbl_BookNumbers WHERE BookCode=@BookCode AND ServiceCenterId=@ServiceCenterId)    
 ELSE IF EXISTS(SELECT 0 FROM Tbl_BookNumbers WHERE BookCode=@BookCode AND CycleId=@CycleId)    
 BEGIN    
  SELECT 1 AS IsBookCodeExists,@BookCode AS BookCode FOR XML PATH('MastersBE')    
 END    
 ELSE    
 BEGIN    
  INSERT INTO Tbl_BookNumbers(ID,BookNo,CreatedBy,CreatedDate    
       --,BU_ID    
       --,SU_ID    
       --,ServiceCenterId    
       ,NoOfAccounts,Details,BookCode    
   ,CycleId,SortOrder,MarketerId)    
  VALUES(@ID,@BookNo,@CreatedBy,DATEADD(SECOND,19800,GETUTCDATE())    
   --    ,@BU_ID    
   --    ,@SU_ID,    
   --@ServiceCenterId    
   ,CASE @NoOfAccouns WHEN 0 THEN NULL ELSE @NoOfAccouns END,       
   CASE @Details WHEN '' THEN NULL ELSE @Details END,@BookCode,    
   CASE @CycleId WHEN '' THEN NULL ELSE @CycleId END    
   ,@SortOrder+1 ,@MarketerId)    
    
  SELECT 1 AS IsSuccess,@BookCode AS BookCode FOR XML PATH('MastersBE')    
 END    
 END    
END  

GO
---------------------------------------------------------------------------------------------------

GO
-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 03-JAN-2014    
-- Description: CUSTOMER REGISTRATION    
-- =============================================  
  
ALTER PROCEDURE [EMPLOYEE].[USP_EMPLOYEELIST]    
AS      
BEGIN      
 SELECT    
 (    
 SELECT BEDCEmpId,(EmployeeName + ' - ' + convert(varchar(20),BEDCEmpId)) AS EmployeeName 
 FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails 
 WHERE ActiveStatusId=1    
 FOR  XML PATH('EmployeeBE'),TYPE    
 )     
 FOR  XML PATH(''),ROOT('EmployeeListBEInfoByXml')  
END  
GO
---------------------------------------------------------------------------------------------------



