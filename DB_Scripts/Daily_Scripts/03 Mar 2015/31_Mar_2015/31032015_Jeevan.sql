GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyUsage_ByCycle]    Script Date: 03/31/2015 07:09:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 29 July 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By  : Padmini
-- Modified Date: 25-12-2014
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyUsage_ByCycle]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalUsage INT
	
	SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers AS BC ON CPD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID 
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <= dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
 	RETURN @TotalUsage;
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyReadingCustomers_ByCycle]    Script Date: 03/31/2015 07:17:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 03 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By   : Padmini
-- Modified Date : 25-12-2014
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyReadingCustomers_ByCycle]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
		
	SELECT @TotalCount = COUNT(Distinct CR.GlobalAccountNumber) FROM Tbl_CustomerReadings CR
	JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo	
	WHERE  BC.CycleId = @CycleID
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <=dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalCount;
END
GO

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetReadCustomers_ByCycle]    Script Date: 03/31/2015 07:20:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READ Customers BY TARIFF Id
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetReadCustomers_ByCycle]
(
	@CycleID VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
   
	SELECT 
		@TotalCount = COUNT(0) 
	FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD 
	INNER JOIN Tbl_BookNumbers AS BC ON CD.BookNo = BC.BookNo
	WHERE  BC.CycleId = @CycleID
	AND CD.ActiveStatusId =1 AND ReadCodeId = 2
		 
	RETURN @TotalCount
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetZeroMinimumCustomers_ByCycle]    Script Date: 03/31/2015 07:21:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READ Customers BY TARIFF Id
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetZeroMinimumCustomers_ByCycle]
(
	@CycleID VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
   
	SELECT 
		@TotalCount = COUNT(0) 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD 
	JOIN CUSTOMERS.Tbl_CustomerActiveDetails CA ON CD.GlobalAccountNumber=CA.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers AS BC ON CD.BookNo = BC.BookNo
	WHERE  BC.CycleId = @CycleID
	AND CD.ActiveStatusId = 1 AND ReadCodeId = 1 AND ISNULL(InitialBillingKWh,0) = 0
		 
	RETURN @TotalCount
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetEstimatedCycles_SC]    Script Date: 03/31/2015 07:06:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 06-10-2014
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- Modified By: T.Karhtik
-- Modified Date: 24-12-2014
-- Modified Desc: Cross join with Tbl_MClusterCategories
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimatedCycles_SC]
(
@XmlDoc xml
)
AS
BEGIN

DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(20)
        ,@TotalReadCustomers INT 
		
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)')		
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	
	;WITH Result AS
	(
		SELECT
			CycleId
			,CycleName
			,CC.CategoryName
			,CC.ClusterCategoryId
			,(CASE WHEN CycleCode IS NULL THEN CycleName ELSE CycleName+' ('+CycleCode+')' END) AS Cycle
			,([dbo].[fn_GetMonthlyUsage_ByCycle](CycleId,@Year,@Month)) AS TotalMetersUsage
			,([dbo].fn_GetMonthlyReadingCustomers_ByCycle(CycleId,@Year,@Month)) AS TotalReadCustomers
			,([dbo].fn_GetReadCustomers_ByCycle(CycleId)) AS TotalCustomers
			,([dbo].fn_GetZeroMinimumCustomers_ByCycle(CycleId)) AS Total
		FROM Tbl_Cycles 
		--CROSS JOIN Customers.Tbl_MClusterCategories CC
		CROSS JOIN MASTERS.Tbl_MClusterCategories CC
		WHERE ActiveStatusId=1
		AND ServiceCenterId = @ServiceCenterId
	)
	
	SELECT
	(
		SELECT 
			CycleId
			,CycleName
			,Cycle
			,CategoryName
			,ClusterCategoryId
			,TotalMetersUsage
			,TotalReadCustomers
			,(TotalCustomers - TotalReadCustomers) AS TotalNonReadCustomers 
			,Total
		FROM Result
		FOR XML PATH('EstimationDetails'),TYPE
	)
	FOR XML PATH(''),ROOT('EstimationInfoByXml')
END
GO
GO
ALTER TABLE Tbl_DirectCustomersAvgReadings ALTER COLUMN AverageReading DECIMAL(18,2)
GO
/****** Object:  View [dbo].[UDV_RptCustomerBookInfo]    Script Date: 03/31/2015 18:15:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[UDV_RptCustomerBookInfo]  
AS  
 SELECT       
    PD.GlobalAccountNumber  
   ,PD.PoleID  
   ,PD.TariffClassID AS TariffId  
   ,PD.ClusterCategoryId 
   ,TC.ClassName  
   ,PD.ReadCodeID  
   ,CD.ActiveStatusId 
   ,BN.BookNo  
   ,BN.BookCode
   ,BN.ID AS BookId  
   ,BU.BU_ID  
   ,BU.BusinessUnitName  
   ,BU.BUCode  
   ,SU.SU_ID  
   ,SU.ServiceUnitName  
   ,SU.SUCode  
   ,SC.ServiceCenterId  
   ,SC.ServiceCenterName  
   ,SC.SCCode  
   ,C.CycleId  
   ,C.CycleName  
   ,C.CycleCode
  FROM CUSTOMERS.Tbl_CustomerSDetail CD
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON CD.GlobalAccountNumber=PD.GlobalAccountNumber
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDirectCustomersCount]    Script Date: 03/31/2015 17:41:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 06 OCT 2014                        
-- Description  : THIS function is use to get direct customers under the selecting cycle
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetDirectCustomersCount]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
,@ClusterCategoryId INT
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @MonthStartDate VARCHAR(50)         
    ,@Date VARCHAR(50)           
    ,@Result INT = 0    
    ,@ReadCustomersCount INT     
    
 IF(@ClusterCategoryId!=0)
	 BEGIN
			SELECT 
			@Result = COUNT(0) 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
			JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
			WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
			AND CD.ActiveStatusId IN(1,2) AND ReadCodeID = 1 AND ClusterCategoryId=@ClusterCategoryId
	 END
 ELSE
	 BEGIN
			SELECT 
			@Result = COUNT(0) 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
			JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
			WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
			AND CD.ActiveStatusId IN(1,2) AND ReadCodeID = 1 
	 END 
       
 RETURN @Result    
    
END 
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyReadCustomers_ByTariff]    Script Date: 03/31/2015 17:43:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyReadCustomers_ByTariff]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
    ,@TariffId INT
	,@ClusterCategoryId INT)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
	--		,@Date DATETIME     
	--		,@MonthStartDate VARCHAR(50)     
   
	--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
 --   SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

	SELECT @TotalCount = COUNT(Distinct CR.GlobalAccountNumber) FROM Tbl_CustomerReadings CR
	JOIN UDV_RptCustomerBookInfo CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID AND CD.TariffId = @TariffId
	AND CD.ClusterCategoryId = @ClusterCategoryId
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <=dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalCount;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyConsumption_ByTariff]    Script Date: 03/31/2015 17:51:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTAL COnsumptin BY TARIFF ID
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyConsumption_ByTariff]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
    ,@TariffId INT
	,@ClusterCategoryId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalUsage INT
			,@Date DATETIME     
			,@MonthStartDate VARCHAR(50)     
   
	SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
    SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

	SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	JOIN UDV_RptCustomerBookInfo CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID AND CD.TariffId = @TariffId
	AND CD.ClusterCategoryId = @ClusterCategoryId
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <=dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalUsage;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalMinimumUsage_Tariff]    Script Date: 03/31/2015 18:13:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 06 OCT 2014                        
-- Description  : THIS function is use to get direct customers under the selecting cycle
-- Modified date: 24-12-2014
-- Modified By : T.Karthik
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetTotalMinimumUsage_Tariff]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
,@ClusterCategoryId INT
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @Result DECIMAL(18,2) = 0    

SELECT 
		@Result =SUM(AverageReading) 
  FROM Tbl_DirectCustomersAvgReadings DCA
  INNER JOIN UDV_RptCustomerBookInfo AS CD  ON CD.GlobalAccountNumber= DCA.GlobalAccountNumber
  JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
  WHERE CD.TariffId  = @TariffId AND BC.CycleId = @CycleID   
  AND CD.ClusterCategoryId = @ClusterCategoryId
  AND CD.ActiveStatusId IN(1,2) AND ReadCodeId = 1
  GROUP BY BC.CycleId
 RETURN @Result    
    
END 

GO
