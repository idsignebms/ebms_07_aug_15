
GO

SET QUOTED_IDENTIFIER ON
GO

  
    
/*--------------------------------------------------------------------------\  */    
ALTER VIEW [dbo].[UDV_CustomerDescription]    
AS    
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs,     
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo,     
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading,     
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID    
                      ,CD.Service_HouseNo       
   ,CD.Service_StreetName        
   ,CD.Service_City       
   ,CD.Service_ZipCode               
        
   ,CD.Postal_ZipCode    
   ,CD.Postal_HouseNo       
   ,CD.Postal_StreetName        
   ,CD.Postal_City    
   ,CD.Postal_Landmark    
   ,Cd.Service_Landmark    
   ,Cd.ApplicationDate    
   ,Cd.SetupDate    
   ,CD.TenentId    
   ,CD.IsVIPCustomer    
   ,IsCAPMI    
   ,CD.IsBEDCEmployee    
   ,MeterAmount    
   ,MS.StatusId as CustomerStatusID    
   ,PD.IsEmbassyCustomer    
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
   ,BN.Details AS BookDetails  
       
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN    
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo    
    
    
    
    
----------------------------------------------------------------------------------------------------

GO



GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 03-11-2014
-- Modified date: 04-11-2014
-- Description:	The purpose of this procedure is to get New Customers Report
-- Modified By	: Neeraj Kanojiya
-- Modified Date: 11-Nov-14
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:31-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetNewCustomersReport] 
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @BUID VARCHAR(MAX)
		,@SUID VARCHAR(MAX)		
		,@SCID VARCHAR(MAX)
		,@Cycles VARCHAR(MAX)
		,@BookNos VARCHAR(MAX)
		,@FromDate VARCHAR(50)
		,@ToDate VARCHAR(50)
		,@PageSize INT  
		,@PageNo INT  
		
	SELECT
		 @BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')
	FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
	
	IF (@FromDate = '' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60 
		END 
	IF (@Todate = '' OR @Todate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH LIST AS
	(
		SELECT
			 ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber) AS RowNumber 
			,CD.GlobalAccountNumber AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) AS ServiceAddress 
			,CD.HomeContactNo
			,ISNULL(MeterNumber,'-') AS MeterNo
			,CD.ClassName AS Tariff
			,dbo.fn_GetCustomerLastBillGeneratedDate(cd.GlobalAccountNumber) AS CreatedDate
			,ISNULL(CD.AvgReading,0) AS AverageReading
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.CycleName 
			,CD.BusinessUnitName
			,CD.ServiceUnitName
			,CD.ServiceCenterName
			,CD.BookCode
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,COUNT(0) OVER() AS TotalRecords
		FROM [UDV_CustomerDescription] AS CD
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID  
					AND CD.ActiveStatusId = 1 AND CD.CylceActiveStatusId = 1 AND CD.CustomerTypeId = 1
					AND (CD.CreatedDate BETWEEN @FromDate AND @ToDate)
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
	)

	SELECT 
			 RowNumber
			,AccountNo
			,Name
			,ServiceAddress
			,HomeContactNo
			,MeterNo
			,Tariff
			,CreatedDate
			,AverageReading
			,CustomerSortOrder
			,BookSortOrder
			,CycleName 
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,BookCode
			,BookNumber
			,TotalRecords
	FROM LIST
	WHERE RowNumber BETWEEN (@PageNo-1) * @PageSize + 1 AND @PageNo * @PageSize

END
-------------------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 04-11-2014
-- Description:	The purpose of this procedure is to get No Usage Customers Report
-- Modified By	: Neeraj Kanojiya
-- Modified Date: 11-Nov-14
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:31-Mar-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetNoUsageCustomersReport]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @BUID VARCHAR(MAX)
		,@SUID VARCHAR(MAX)
		,@SCID VARCHAR(MAX)
		,@Cycles VARCHAR(MAX)
		,@BookNos VARCHAR(MAX)
		,@FromDate VARCHAR(50)
		,@ToDate VARCHAR(50)
		,@PageSize INT  
		,@PageNo INT  
		
	SELECT
		@BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
	
	IF (@FromDate = '' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60 
		END 
	IF (@Todate = '' OR @Todate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH LIST AS
	(	
		SELECT 
			 ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber) AS RowNumber 
			,GlobalAccountNumber
			,CD.AccountNo
			,CD.OldAccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) AS ServiceAddress
			,MeterNumber AS MeterNo
			,CONVERT(VARCHAR(25),CB.BillGeneratedDate,107) AS BillGeneratedDate
			,CD.ClassName AS Tariff
			,ISNULL(CB.NetFixedCharges,0) AS AdditionalCharges
			,CB.TotalBillAmountWithArrears AS TotalBilledAmount
			,RC.ReadCode
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.CycleName 
			,CD.BusinessUnitName
			,CD.ServiceUnitName
			,CD.ServiceCenterName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.BookCode
			,ISNULL(CD.OutStandingAmount,0) AS OutStandingAmount
			,COUNT(0) OVER() AS TotalRecords
		FROM [UDV_CustomerDescription] AS CD
		INNER JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber = CB.AccountNo AND CD.ActiveStatusId IN(1,2) AND ISNULL(CB.Usage,0) = 0
				AND CD.ActiveStatusId = 1 AND CD.CylceActiveStatusId = 1 
				AND (CD.CreatedDate BETWEEN @FromDate AND @ToDate)
		INNER JOIN Tbl_MReadCodes AS RC ON CB.ReadCodeId = RC.ReadCodeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
	)

	SELECT 
		 RowNumber
		,GlobalAccountNumber
		,AccountNo
		,OldAccountNo
		,Name
		,ServiceAddress 
		,MeterNo
		,BillGeneratedDate
		,Tariff
		,AdditionalCharges
		,TotalBilledAmount
		,ReadCode
		,CustomerSortOrder
		,BookSortOrder
		,CycleName 
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,BookNumber
		,BookCode
		,OutStandingAmount
		,TotalRecords
	FROM LIST
	WHERE RowNumber BETWEEN (@PageNo-1) * @PageSize + 1 AND @PageNo * @PageSize

END
------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 07-NOV-2014
-- Description:	The purpose of this procedure is to get Customers with no Energy charges.
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:31-Mar-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptCustWithNoFxdChargesReport] 
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @BUID VARCHAR(MAX)=''
		,@SUID VARCHAR(MAX)=''
		,@SCID VARCHAR(MAX)=''
		,@Cycles VARCHAR(MAX)=''
		,@BookNos VARCHAR(MAX)=''
		,@FromDate VARCHAR(50)=''
		,@ToDate VARCHAR(50)=''
		,@PageSize INT  
		,@PageNo INT  
		
	SELECT
		 @BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
	FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
	
	IF (@FromDate = '' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60 
		END 
	IF (@Todate = '' OR @Todate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;with List AS	
	(
		SELECT 
			 ROW_NUMBER() OVER (ORDER BY CB.CustomerBillId desc) AS RowNumber
			,CD.GlobalAccountNumber AS AccountNo
			,ISNULL(CD.OldAccountNo,'') AS OldAccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) AS ServiceAddress
			,ISNULL(CD.MeterNumber,'') AS MeterNo
			,ClassName AS Tariff
			,RC.ReadCode
			,CONVERT(DECIMAL(18,2),CB.Usage) AS Usage
			,CONVERT(DECIMAL(18,2),CB.NetEnergyCharges) AS NetEnergyCharges
			,CONVERT(DECIMAL(18,2),CB.VAT)AS VAT
			,CONVERT(DECIMAL(18,2),CB.NetEnergyCharges+CB.VAT) AS TOTAL
			,CONVERT(VARCHAR(11),CB.CreatedDate,100) AS BillGeneratedDate
			,CB.CreatedDate
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.CycleName 
			,CD.BusinessUnitName
			,CD.ServiceUnitName
			,CD.ServiceCenterName
			,CD.BookCode
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CB.NetFixedCharges			
			,COUNT(0) OVER() AS TotalRecords	
		FROM Tbl_CustomerBills AS CB
		INNER JOIN [UDV_CustomerDescription] AS CD ON CB.AccountNo = CD.GlobalAccountNumber AND CD.ReadCodeId IN(1,2)
				AND ISNULL(CB.NetFixedCharges,0) = 0 AND CD.ActiveStatusId=1
				AND (CD.CreatedDate BETWEEN @FromDate AND @ToDate)	
		INNER JOIN Tbl_MReadCodes AS RC ON CB.ReadCodeId = RC.ReadCodeId		
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
	)

	SELECT 
		 RowNumber
		,AccountNo
		,OldAccountNo
		,Name
		,ServiceAddress
		,MeterNo
		,Tariff
		,ReadCode
		,Usage
		,NetEnergyCharges
		,VAT
		,TOTAL
		,BillGeneratedDate
		,CreatedDate
		,CustomerSortOrder
		,BookSortOrder
		,CycleName 
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,BookCode
		,BookNumber
		,NetFixedCharges			
		,TotalRecords
	FROM List L
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 

END
-------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 10-11-2014
-- Modified date: 12-11-2014
-- Description:	The purpose of this procedure is to get Unbilled Customers Report
-- Modified By: Karteek
-- Modified Date:31-Mar-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetUnBilledCustomers_New]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @FromMonth INT 
		,@FromYear INT 
		,@ToMonth INT 
		,@ToYear INT 
		,@FromDate VARCHAR(50)
		,@ToDate VARCHAR(50)

	DECLARE @BUID VARCHAR(MAX)
		,@SUID VARCHAR(MAX)
		,@SCID VARCHAR(MAX)
		,@Cycles VARCHAR(MAX)
		,@BookNos VARCHAR(MAX)
		,@PageSize INT  
		,@PageNo INT  
		
	SELECT
		 @BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@FromMonth=C.value('(FromMonth)[1]','INT')
		,@FromYear=C.value('(FromYear)[1]','INT')
		,@ToMonth=C.value('(ToMonth)[1]','INT')
		,@ToYear=C.value('(ToYear)[1]','INT')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')
	FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
		
	SET @FromYear = (SELECT CASE @FromYear WHEN 0 THEN (SELECT MIN(BillYear) FROM Tbl_CustomerBills) ELSE @FromYear END)
	SET @ToYear = (SELECT CASE @ToYear WHEN 0 THEN (SELECT MAX(BillYear) FROM Tbl_CustomerBills) ELSE @ToYear END)
	SET @FromMonth = (SELECT CASE @FromMonth WHEN 0 THEN (SELECT MIN(BillMonth) FROM Tbl_CustomerBills) ELSE @FromMonth END)
	SET @ToMonth = (SELECT CASE @ToYear WHEN 0 THEN (SELECT MAX(BillMonth) FROM Tbl_CustomerBills) ELSE @ToMonth END)

	SELECT @FromDate = CONVERT(DATE, (CONVERT(VARCHAR(4),CASE @FromYear WHEN 0 THEN YEAR(dbo.fn_GetCurrentDateTime()) ELSE @FromYear END) + '-'+ CONVERT(VARCHAR(2),CASE @FromMonth WHEN 0 THEN MONTH(dbo.fn_GetCurrentDateTime()) ELSE @FromMonth END) + '-01'))
		,@ToDate = CONVERT(DATE, (CONVERT(VARCHAR(4),CASE @ToYear WHEN 0 THEN YEAR(dbo.fn_GetCurrentDateTime()) ELSE @ToYear END) +'-'+ CONVERT(VARCHAR(2),CASE @ToMonth WHEN 0 THEN MONTH(dbo.fn_GetCurrentDateTime()) ELSE @ToMonth END) + '-01'))

	DECLARE @DatesRange TABLE(Sno INT,[Year] INT,[Month] INT)
	
	INSERT INTO @DatesRange(Sno, [Year],[Month])
	SELECT Id,[Year],[Month] FROM dbo.fn_GetLast_N_MonthsByFromAndToDates(@FromDate,@ToDate)

	--DROP TABLE #UnBilledCustomers
	CREATE TABLE #UnBilledCustomers(Sno INT PRIMARY KEY IDENTITY(1,1),AccountNo VARCHAR(50),[Month] VARCHAR(10),[Year] INT,MonthNo INT)

	DECLARE @BUS TABLE(BU_ID VARCHAR(50))
	DECLARE @SUS TABLE(SU_ID VARCHAR(50))
	DECLARE @SCS TABLE(SC_ID VARCHAR(50))
	DECLARE @BOOKS TABLE(BOOK_ID VARCHAR(50))
	DECLARE @CYCLS TABLE(CYCLE_ID VARCHAR(50))
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	DECLARE @I INT = 1, @Count INT = 0
	SET @Count = (SELECT COUNT(0) FROM @DatesRange)
		
	WHILE(@I <= @Count)
	BEGIN
		DECLARE @BillYear INT,@BillMonth INT
		SELECT @BillMonth = [Month],@BillYear = [Year] FROM @DatesRange where Sno=@I
		
		INSERT INTO #UnBilledCustomers(AccountNo,[Month],[Year],MonthNo)
		SELECT CD.GlobalAccountNumber
				,(SELECT DATENAME(MONTH,DATEADD(MONTH,@BillMonth,0)-1))
				,@BillYear 
				,@BillMonth
		FROM [UDV_CustomerDescription] AS CD
		INNER JOIN Tbl_BillingDisabledBooks AS BDB ON CD.BookNo = BDB.BookNo
				AND CD.ActiveStatusId IN(1,2)
				AND BDB.DisableTypeId = 1 AND BDB.ApproveStatusId = 2 AND BDB.IsActive = 1
		AND CD.ActiveStatusId = 1 
		AND CD.GlobalAccountNumber NOT IN (SELECT AccountNo FROM Tbl_CustomerBills WHERE BillYear = @BillYear AND BillMonth = @BillMonth)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
		GROUP BY BDB.BookNo,CD.GlobalAccountNumber,CD.CycleName,CD.BookSortOrder,CD.SortOrder
		ORDER BY CD.CycleName,CD.BookSortOrder,CD.SortOrder ASC 
			
		SET @I = @I + 1
		
	END
	
	DECLARE @CustCount INT = 0
	SET @CustCount = (SELECT COUNT(0) FROM #UnBilledCustomers)

	SELECT 
		 Sno AS RowNumber 
		,CD.GlobalAccountNumber AS AccountNo
		,CD.OldAccountNo
		,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
		,dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber) AS [Address]
		,CD.BookCode
		,(SELECT TOP(1) NetArrears FROM Tbl_CustomerBills WHERE AccountNo=CD.GlobalAccountNumber
				AND BillMonth <= UBC.MonthNo AND BillYear <= UBC.[Year] ORDER BY CustomerBillId DESC) AS OutStanding
		,CD.ClassName AS Tariff
		,(SELECT TOP(1) CONVERT(VARCHAR(25),BillGeneratedDate,107) FROM Tbl_CustomerBills WHERE AccountNo = CD.GlobalAccountNumber
				AND BillMonth <= UBC.MonthNo AND BillYear <= UBC.[Year] ORDER BY CustomerBillId DESC) AS LastBilledDate
		,(SELECT TOP(1) TotalBillAmount FROM Tbl_CustomerBills WHERE AccountNo = CD.GlobalAccountNumber
				AND BillMonth <= UBC.MonthNo AND BillYear <= UBC.[Year] ORDER BY CustomerBillId DESC) AS BillAmountWithoutArrears
		,(SELECT TOP(1) PaidAmount FROM Tbl_CustomerPayments WHERE AccountNo = CD.GlobalAccountNumber AND Month(RecievedDate) <= UBC.MonthNo
				AND YEAR(RecievedDate) <= UBC.[Year] ORDER BY CustomerPaymentID DESC) AS LastPaymentAmount
		,(SELECT TOP(1) CONVERT(VARCHAR(25),RecievedDate,107) FROM Tbl_CustomerPayments WHERE AccountNo = CD.GlobalAccountNumber AND Month(RecievedDate) <= UBC.MonthNo
				AND YEAR(RecievedDate) <= UBC.[Year] ORDER BY CustomerPaymentID DESC) AS LastPaymentDate
		,(SELECT TOP(1) CONVERT(VARCHAR(25),CreatedDate,107) FROM Tbl_BillAdjustments WHERE AccountNo = CD.GlobalAccountNumber AND Month(CreatedDate) <= UBC.MonthNo
				AND YEAR(CreatedDate) <= UBC.[Year] AND ApprovalStatusId = 2 ORDER BY BillAdjustmentId DESC) AS LastAdjustmentDate
		,(SELECT Name FROM Tbl_BillAdjustmentType WHERE BATID = (SELECT TOP(1) BillAdjustmentType FROM Tbl_BillAdjustments WHERE AccountNo = CD.GlobalAccountNumber 
																	AND Month(CreatedDate) <= UBC.MonthNo
																	AND YEAR(CreatedDate) <= UBC.[Year] 
																	AND ApprovalStatusId = 2 
																	ORDER BY BillAdjustmentId DESC)) AS AdjustmentType
		,(SELECT TOP(1) TotalAmountEffected FROM Tbl_BillAdjustments WHERE AccountNo = CD.GlobalAccountNumber AND Month(CreatedDate) <= UBC.MonthNo
				AND YEAR(CreatedDate) <= UBC.[Year] AND ApprovalStatusId = 2 ORDER BY BillAdjustmentId DESC) AS LastAdjustmentAmount
		,CD.SortOrder AS CustomerSortOrder
		,CD.BookSortOrder
		,CD.CycleName 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,UBC.[Month]
		,UBC.[Year]
		,UBC.MonthNo
		,@CustCount AS TotalRecords
	 FROM [UDV_CustomerDescription] AS CD
	 INNER JOIN #UnBilledCustomers AS UBC ON CD.GlobalAccountNumber COLLATE DATABASE_DEFAULT = UBC.AccountNo COLLATE DATABASE_DEFAULT
	 AND Sno BETWEEN (@PageNo-1) * @PageSize + 1 AND @PageNo * @PageSize
	 
END
-----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju.Vanka  
-- Create date: 02-06-2014  
-- Description: Toget the list of Estimated,Read Customers  
-- Modified By: Neeraj Kanojiya
-- Modified date: 31-11-2014  
-- Modified By: Neeraj Kanojiya
-- Modified date: 4-11-2014  
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetReportOnEstimatedCustomers_New]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
  
	DECLARE    
		 @PageSize INT  
		,@PageNo INT  
		,@BU_ID VARCHAR(MAX)  
		,@SU_ID VARCHAR(MAX)  
		,@SC_ID VARCHAR(MAX)    
		,@CycleId VARCHAR(MAX)
		,@Year INT  
		,@Month INT  
		,@ReadCodeId VARCHAR(MAX)  
  
	SELECT @PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@Year = C.value('(Year)[1]','INT')  
		,@Month = C.value('(Month)[1]','INT')  
		,@ReadCodeId = C.value('(ReadCode)[1]','VARCHAR(MAX)')  
	FROM @XmlDoc.nodes('ConsumerBe') as T(C)  
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS  
	( 
		SELECT  
			 ROW_NUMBER() OVER(ORDER BY CB1.BillNo DESC ) AS RowNumber  
			,CB1.AccountNo
			,CD.BusinessUnitName
			,CD.BookNo
			,CB1.TotalBillAmountWithArrears
			,(Case WHEN CD.BookDetails IS NULL OR CD.BookDetails = ''  THEN CD.BookCode ELSE CD.BookDetails + '-' + CD.BookCode END) AS BookCode
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,CD.CycleName  
			,CB1.ServiceAddress 
			,RCD.ReadCode AS ReadCode
			,DATENAME(MONTH,CB1.CreatedDate) + ' ' + DATENAME(YY,CB1.CreatedDate) AS MonthYear
			,CB1.TotalBillAmountWithArrears AS Amount
			,BS.BillStatus
			,dbo.fn_GetRegEstimationCount(CB1.AccountNo,@Month,@Year) AS ContinusEstimations
			,(SELECT dbo.fn_GetMonthName(CB1.BillMonth) + '-' + CONVERT(VARCHAR(50),CB1.BillYear)) AS MonthYear1  
			,CB1.EstimatedUsage AS Usage1
			,CB1.NetEnergyCharges AS Amount1
			,TC1.ClassName AS ClassName1
			,(SELECT dbo.fn_GetMonthName(CB2.BillMonth) + '-' + CONVERT(VARCHAR(50),CB2.BillYear)) AS MonthYear2
			,CB2.EstimatedUsage AS Usage2
			,CB2.NetEnergyCharges AS Amount2
			,CASE WHEN CB2.EstimatedUsage IS NULL THEN '' ELSE TC2.ClassName END AS ClassName2
			,(SELECT dbo.fn_GetMonthName(CB3.BillMonth) + '-' + CONVERT(VARCHAR(50),CB3.BillYear)) AS MonthYear3 
			,CB3.EstimatedUsage AS Usage3
			,CB3.NetEnergyCharges AS Amount3
			,CASE WHEN CB3.EstimatedUsage IS NULL THEN '' ELSE TC3.ClassName END AS ClassName3
			,COUNT(0) OVER() AS TotalRecords
		FROM Tbl_CustomerBills CB1
		INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CB1.AccountNo AND CB1.BillMonth = @Month AND CB1.BillYear = @Year
					AND CD.MeterNumber IS NOT NULL
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		LEFT JOIN Tbl_CustomerBills CB2 ON CB1.AccountNo = CB2.AccountNo AND CB2.BillMonth = DATEPART(MONTH, DATEADD(MONTH,-1,CONVERT(DATE, convert(VARCHAR(4),@Year) + '-' + convert(VARCHAR(2),@Month) + '-01')))
		LEFT JOIN Tbl_CustomerBills CB3 ON CB2.AccountNo = CB3.AccountNo AND CB3.BillMonth = DATEPART(MONTH, DATEADD(MONTH,-2,CONVERT(DATE, convert(VARCHAR(4),@Year) + '-' + convert(VARCHAR(2),@Month) + '-01')))
		LEFT JOIN Tbl_MTariffClasses TC1 ON CB1.TariffId = TC1.ClassID
		LEFT JOIN Tbl_MTariffClasses TC2 ON CB2.TariffId = TC2.ClassID
		LEFT JOIN Tbl_MTariffClasses TC3 ON CB3.TariffId = TC3.ClassID
		LEFT JOIN Tbl_MReadCodes AS RCD ON CB1.ReadCodeId=RCD.ReadCodeId
		LEFT JOIN Tbl_MBillStatus AS BS ON CB1.PaymentStatusID=bs.BillStatusId
		WHERE (CB1.ReadCodeId IN (3,5) OR 
		(CB1.ReadCodeId IN (3,5) AND CB2.ReadCodeId IN (3,5)) OR
		(CB1.ReadCodeId IN (3,5) AND CB2.ReadCodeId IN (3,5) AND CB3.ReadCodeId IN (3,5)))
	)
	   
	SELECT   
	(  
		SELECT    
			 RowNumber  
			,AccountNo
			,BusinessUnitName
			,BookNo
			,TotalBillAmountWithArrears
			,BookCode
			,Name
			,CycleName  
			,ServiceAddress 
			,ReadCode
			,MonthYear
			,Amount
			,BillStatus
			,ContinusEstimations
			,MonthYear1  
			,Usage1
			,Amount1
			,ClassName1
			,MonthYear2
			,Usage2
			,Amount2
			,ClassName2
			,MonthYear3 
			,Usage3
			,Amount3
			,ClassName3
			,TotalRecords  
		FROM PagedResults p  
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		FOR XML PATH('Consumer'),TYPE  
	)  
	FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
	  
END
---------------------------------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------    
-- =============================================      
-- Author  : Suresh Kumar D      
-- Create date  : 31-10-2014      
-- Description  : The purpose of this procedure is to Get the Aged Customers Account Receivable    
-- Modified By : Bimaraju  
-- Modified date: 11-NOV-2014  
-- Modified By : NEERAJ KANOJIYA (Modified for paging pupose.)  
-- Modified date: 12-NOV-2014  
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================      
CREATE PROCEDURE [dbo].[USP_RptAgedCustomerAccount_Receivable_New]    
(      
	@XmlDoc XML  
)      
AS      
BEGIN      
	
	DECLARE @CurrentDate DATE = CONVERT(DATE,dbo.fn_GetCurrentDateTime())
	    
	DECLARE @Month INT, @Year INT, @Month1 INT, @Year1 INT, @Month2 INT, @Year2 INT, @Month3 INT, @Year3 INT  
		,@BU_ID VARCHAR(50), @SU_ID VARCHAR(MAX), @SC_ID VARCHAR(MAX), @PageSize INT, @PageNo INT    
		,@RecentBillDate DATETIME, @PrevBillDate DATETIME  
  
	SELECT @Month = DATEPART(MONTH,@CurrentDate), @Year = DATEPART(YEAR,@CurrentDate)   
  
	SELECT @RecentBillDate = MAX(CONVERT(DATETIME,CONVERT(VARCHAR(4),BillYear) + '-'+ CONVERT(VARCHAR(2),BillMonth) + '-01')) FROM Tbl_CustomerBills  
                 
	SELECT @Month = DATEPART(MONTH,@RecentBillDate), @Year = DATEPART(YEAR,@RecentBillDate)  
	SELECT @Month1 = DATEPART(MONTH,DATEADD(MONTH,-1,@RecentBillDate)), @Year1 = DATEPART(YEAR,DATEADD(MONTH,-1,@RecentBillDate))    
	SELECT @Month2 = DATEPART(MONTH,DATEADD(MONTH,-2,@RecentBillDate)), @Year2 = DATEPART(YEAR,DATEADD(MONTH,-2,@RecentBillDate))    
	SELECT @Month3 = DATEPART(MONTH,DATEADD(MONTH,-3,@RecentBillDate)), @Year3 = DATEPART(YEAR,DATEADD(MONTH,-3,@RecentBillDate))   
    
	SELECT @PrevBillDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),@Year3) + '-' + CONVERT(VARCHAR(2),@Month3) + '-01')  
	
	SELECT @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')    
	FROM @XmlDoc.nodes('RptAgedCustsBe') AS T(C)  
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT CD.GlobalAccountNumber AS AccountNo    
		,CD.OldAccountNo    
		,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name    
		--,(SELECT TOP 1 OutStandingAmount FROM [CUSTOMERS].[Tbl_CustomerActiveDetails](NOLOCK) 
		--		WHERE GlobalAccountNumber = CD.GlobalAccountNumber) AS OutStandingAmount  
		,CD.OutStandingAmount
		,CD.BusinessUnitName  
		,CD.ServiceUnitName  
		,CD.ServiceCenterName  
		,ISNULL(CONVERT(VARCHAR(50),B.BillGeneratedDate,106),'--') AS BillGeneratedDate  
		,ISNULL(B.TotalBillAmountWithTax,0) AS TotalBillAmountWithTax  
		,ISNULL(B.TotalBillAmountWithArrears,0) AS TotalBillAmountWithArrears   
		,CD.BookSortOrder AS BookSortOrder  
		,CD.SortOrder AS CustomerSortOrder  
		,B.BillMonth  
		,B.BillYear  
	INTO #tmpCustomerBillInfo  
	FROM [UDV_CustomerDescription] CD (NOLOCK)      
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ActiveStatusId = 1   			
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId    
	INNER JOIN Tbl_CustomerBills B (NOLOCK) ON CD.GlobalAccountNumber = B.AccountNo
		AND CONVERT(DATETIME,CONVERT(VARCHAR(4),b.BillYear) + '-' + CONVERT(VARCHAR(2),B.BillMonth) + '-' + '01',101) BETWEEN @PrevBillDate AND @RecentBillDate
       
	;WITH List AS   
	(  
		SELECT    
			 ROW_NUMBER() OVER (ORDER BY B.BookSortOrder,B.CustomerSortOrder ASC) AS RowNumber  
			,B.AccountNo    
			,B.OldAccountNo    
			,B.Name    
			,B.OutStandingAmount  
			,B.BusinessUnitName  
			,B.ServiceUnitName  
			,B.ServiceCenterName  
			--30days  
			,ISNULL(CONVERT(VARCHAR(50),B1.BillGeneratedDate,106),'--') AS BillGeneratedDate30 
			,ISNULL(B1.TotalBillAmountWithTax,0)   AS TotalBillAmountWithTax30  
			,ISNULL(B1.TotalBillAmountWithArrears,0) AS TotalBillAmountWithArrears30   
			--60days  
			,ISNULL(CONVERT(VARCHAR(50),B2.BillGeneratedDate,106),'--') AS BillGeneratedDate60  
			,ISNULL(B2.TotalBillAmountWithTax,0)  AS TotalBillAmountWithTax60  
			,ISNULL(B2.TotalBillAmountWithArrears,0) AS TotalBillAmountWithArrears60  
			--90days  
			,ISNULL(CONVERT(VARCHAR(50),B3.BillGeneratedDate,106),'--') AS BillGeneratedDate90  
			,ISNULL(B3.TotalBillAmountWithTax,0)  AS TotalBillAmountWithTax90  
			,ISNULL(B3.TotalBillAmountWithArrears,0) AS TotalBillAmountWithArrears90  
			--120days  
			,ISNULL(CONVERT(VARCHAR(50),B4.BillGeneratedDate,106),'--') AS BillGeneratedDate120  
			,ISNULL(B4.TotalBillAmountWithTax,0)  AS TotalBillAmountWithTax120  
			,ISNULL(B4.TotalBillAmountWithArrears,0) AS TotalBillAmountWithArrears120  
			,B.BookSortOrder  
			,B.CustomerSortOrder
			,COUNT(0) OVER() AS TotalRecords 
		FROM #tmpCustomerBillInfo B  
		LEFT JOIN #tmpCustomerBillInfo B1 ON B.AccountNo = B1.AccountNo AND B1.BillMonth = @Month AND B1.BillYear = @Year1    
		LEFT JOIN #tmpCustomerBillInfo B2 ON B1.AccountNo = B2.AccountNo AND B2.BillMonth = @Month1 AND B2.BillYear = @Year1    
		LEFT JOIN #tmpCustomerBillInfo B3 ON B2.AccountNo = B3.AccountNo AND B3.BillMonth = @Month2 AND B3.BillYear = @Year2    
		LEFT JOIN #tmpCustomerBillInfo B4 ON B3.AccountNo = B4.AccountNo AND B4.BillMonth = @Month3 AND B4.BillYear = @Year3 
	)  
	
	SELECT RowNumber
		,AccountNo
		,OldAccountNo
		,Name
		,OutStandingAmount
		,BusinessUnitName
		,ServiceUnitName 
		,ServiceCenterName  
		--30days  
		,BillGeneratedDate30
		,TotalBillAmountWithTax30
		,TotalBillAmountWithArrears30   
		--60days  
		,BillGeneratedDate60
		,TotalBillAmountWithTax60
		,TotalBillAmountWithArrears60  
		--90days  
		,BillGeneratedDate90
		,TotalBillAmountWithTax90
		,TotalBillAmountWithArrears90  
		--120days  
		,BillGeneratedDate120
		,TotalBillAmountWithTax120
		,TotalBillAmountWithArrears120  
		,BookSortOrder
		,CustomerSortOrder  
		,(SELECT COUNT(0) FROM List) AS TotalRecords  
	FROM List L  
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
	
	DROP TABLE #tmpCustomerBillInfo           
   
END      
---------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name,CD.ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID	AND CD.ReadCodeID = 1 
		AND CD.GlobalAccountNumber NOT IN(SELECT DAVG.GlobalAccountNumber FROM Tbl_DirectCustomersAvgReadings DAVG)
	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId 
	--INNER JOIN Tbl_DirectCustomersAvgReadings DAVG ON CD.GlobalAccountNumber != DAVG.GlobalAccountNumber

END
------------------------------------------------------------------------------------

