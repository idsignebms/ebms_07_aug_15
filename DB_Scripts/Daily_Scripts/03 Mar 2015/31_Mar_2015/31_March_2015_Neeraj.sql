GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment    
--===================================         
CREATE PROCEDURE [dbo].[USP_AdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId			 INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)    
   ,@TotalAmountEffected DECIMAL(18,2)    
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT     
       
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','INT')    
   ,@TotalAmountEffected = C.value('(AmountEffected)[1]','INT') 
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
  
       
   INSERT INTO Tbl_BillAdjustments(    
     AccountNo    
     ,CustomerId    
     ,AmountEffected   
     ,BillAdjustmentType    
     ,TotalAmountEffected   
     )           
    VALUES(         
      @AccountNo    
     ,@CustomerId    
     ,@AmountEffected   
     ,@BillAdjustmentType   
     ,@TotalAmountEffected  
     )      
  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
       
  INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)    
  SELECT           
   @BillAdjustmentId     
   ,@AmountEffected    
     
  SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
 -------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--=====================================================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : CHECK IS CUSTOMER HAVING ANY BILL 
--=====================================================================         
CREATE PROCEDURE [dbo].[IsBillExists]         
(        
@XmlDoc xml        
)         
AS        
BEGIN 
 DECLARE @GlobalAccountNo VARCHAR(50) 
		 ,@IsSuccess BIT=0   
 SELECT     
	@GlobalAccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)  
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
	BEGIN
		SET @IsSuccess=1 
	END	
	SELECT @IsSuccess AS IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE
END
GO
-------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[USP_AdjustmentForNoBill]    Script Date: 03/31/2015 16:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment    
--===================================         
CREATE PROCEDURE [dbo].[USP_UpdateAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId			 INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)    
   ,@TotalAmountEffected DECIMAL(18,2)    
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT     
       
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','INT')    
   ,@TotalAmountEffected = C.value('(AmountEffected)[1]','INT') 
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
  
       
   UPDATE Tbl_BillAdjustments 
	SET AmountEffected=@AmountEffected
		,TotalAmountEffected=TotalAmountEffected 
	WHERE BillAdjustmentId=@BillAdjustmentId
	
  UPDATE Tbl_BillAdjustmentDetails 
	SET EnergryCharges=@AmountEffected 
	WHERE BillAdjustmentId=@BillAdjustmentId
     
  SELECT @@ROWCOUNT As IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
 -------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Delete No Bill adjustment    
--===================================         
CREATE PROCEDURE [dbo].[USP_DeleteAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId INT   
   
       
       
  SELECT     
	@BillAdjustmentId = C.value('(BillAdjustmentId)[1]','INT') 
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      
       
	DELETE FROM  Tbl_BillAdjustments WHERE BillAdjustmentId=@BillAdjustmentId	
	DELETE Tbl_BillAdjustmentDetails WHERE BillAdjustmentId=@BillAdjustmentId
     
	SELECT @@ROWCOUNT As IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
 -------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Get No Bill adjustment Details
--===================================         
CREATE PROCEDURE [dbo].[USP_GetAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId INT 
  SELECT     
	@BillAdjustmentId = C.value('(BillAdjustmentId)[1]','INT') 
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)   
     
     SELECT AmountEffected
			,BillAdjustmentId 
	 FROM Tbl_BillAdjustments WHERE BillAdjustmentId=@BillAdjustmentId 
	 
	 FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
----------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--=====================================================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : CHECK IS CUSTOMER HAVING ANY BILL 
--=====================================================================         
CREATE PROCEDURE [dbo].[USP_IsNoBillAdjustmentExists]         
(        
@XmlDoc xml        
)         
AS        
BEGIN 
 DECLARE @GlobalAccountNo VARCHAR(50) 
		 ,@IsSuccess BIT=0   
 SELECT     
	@GlobalAccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)  
	
	IF EXISTS(SELECT 0 FROM Tbl_BillAdjustments WHERE AccountNo=@GlobalAccountNo and CustomerBillId=Null)
	BEGIN
		SET @IsSuccess=1 
	END	
	SELECT @IsSuccess AS IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE
END
GO