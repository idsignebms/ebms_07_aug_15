GO
/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSCWitlBillingStaus]    Script Date: 04/01/2015 17:39:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  NEERAJ    
-- Create date: 10-FEB-2015           
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC with billing status.    
-- Modified date: 16-FEB-2015           
-- Description: Added new field total customers  
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSCWitlBillingStaus]        
(        
@XmlDoc xml        
)        
AS        
BEGIN        
        
 DECLARE @BU_ID VARCHAR(20)    
   ,@SU_ID VARCHAR(MAX)    
   ,@ServiceCenterId VARCHAR(MAX)     
   ,@BillYear VARCHAR(MAX)    
   ,@BillMonth VARCHAR(MAX)       
          
 SELECT        
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')        
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')        
   ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')        
   ,@BillYear=C.value('(BillYear)[1]','VARCHAR(20)')          
   ,@BillMonth=C.value('(BillMonth)[1]','VARCHAR(20)')          
     
     
 FROM @XmlDoc.nodes('MastersBE') as T(C)        
         
 SELECT        
 (        
    SELECT        
   CycleId        
   ,(CycleName + ' ('+CycleCode+') - ' + SC.ServiceCenterName ) as CycleName
   ,(select COUNT(0)     
     from Tbl_CustomerBills as cb     
     where  cb.BillMonth=@BillMonth     
       and cb.BillYear=@BillYear     
       AND cb.BU_ID=BU.BU_ID    
       AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
       AND CB.CycleId=CY.CycleId) AS BillGenCount    
   ,(select COUNT(0)     
     from Tbl_CustomerBillPayments as bp JOIN Tbl_CustomerBills AS CB ON bp.BillNo=cb.BillNo    
     where  cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS PaymentCount       
  ,(select COUNT(0)     
     from Tbl_CustomerBills AS CB     
     JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId    
     where cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS BillAdjustmentCount      
   ,(SELECT COUNT(0)    
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=cy.CycleId) AS TotalCustomers  
  FROM Tbl_Cycles cy    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
  WHERE CY.ActiveStatusId=1        
  AND (BU.BU_ID=@BU_ID OR @BU_ID='')        
  AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')        
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')        
  ORDER BY CycleName ASC       
  FOR XML PATH('MastersBE'),TYPE        
 )        
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
END 

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertUserDetails]    Script Date: 04/01/2015 17:56:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju .v  
-- Create date: 03-MAR-2014  
-- Modified date : 09-04-2014  
-- Modified By : T.Karthik  
-- Modified date : 19-03-2014  
-- Description: Insert Data into Tbl_UserDetails  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_InsertUserDetails]  
(  
 @XmlDoc Xml  
)  
AS  
BEGIN  
 DECLARE  @UserId VARCHAR(50)  
   ,@Name VARCHAR(300)  
   ,@SurName VARCHAR(300)  
   ,@PrimaryContact VARCHAR(20)  
   ,@SecondaryContact VARCHAR(20)  
   ,@PrimaryEmailId VARCHAR(500)  
   ,@SecondaryEmailId VARCHAR(500)  
   ,@Address VARCHAR(MAX)  
   ,@Photo VARCHAR(MAX)  
   ,@FilePath VARCHAR(MAX)  
   ,@Password VARCHAR(MAX)  
   ,@GenderId INT   
   ,@Details VARCHAR(MAX)  
   ,@RoleId INT  
   ,@DesignationId INT  
   ,@CreatedBy VARCHAR(50)  
   ,@DocumentName VARCHAR(MAX)  
   ,@Path VARCHAR(MAX)  
   --,@BudgetSegmentWiseMargin BIT  
   --,@CustomerDetailsModification BIT  
   --,@BillAdjustment BIT  
   --,@TariffAdjustments BIT  
   --,@PaymentsAdjustments BIT  
   --,@Disconnection BIT  
   --,@ReConnections BIT  
   --,@BillGeneration BIT  
   ,@BusinessUnits VARCHAR(MAX)  
   ,@ServiceCenters VARCHAR(MAX)  
   ,@ServiceUnits VARCHAR(MAX)  
   ,@Districts VARCHAR(MAX)  
   ,@BU_ID VARCHAR(20)  
   ,@SU_ID VARCHAR(20)  
   ,@CashOffices VARCHAR(MAX)  
   ,@CurrentDate DATETIME
   ,@IsMobileAccess BIT 
 SELECT @CurrentDate=dbo.fn_GetCurrentDateTime();  
     
 SELECT   @UserId =C.value('(EmployeeId)[1]','VARCHAR(50)')  
   ,@Name =C.value('(Name)[1]','VARCHAR(300)')  
   ,@SurName =C.value('(SurName)[1]','VARCHAR(300)')  
   ,@PrimaryContact =C.value('(ContactNo1)[1]','VARCHAR(20)')  
   ,@SecondaryContact =C.value('(ContactNo2)[1]','VARCHAR(20)')  
   ,@PrimaryEmailId =C.value('(PrimaryEmailId)[1]','VARCHAR(500)')  
   ,@SecondaryEmailId =C.value('(SecondaryEmailId)[1]','VARCHAR(500)')  
   ,@Address =C.value('(Address)[1]','VARCHAR(MAX)')  
   ,@Photo =C.value('(Photo)[1]','VARCHAR(MAX)')  
   ,@FilePath=C.value('(FilePath)[1]','VARCHAR(MAX)')  
   ,@Password =C.value('(Password)[1]','VARCHAR(MAX)')  
   ,@GenderId  =C.value('(GenderId)[1]','INT')  
   ,@Details =C.value('(Details)[1]','VARCHAR(MAX)')  
   ,@RoleId =C.value('(RoleId)[1]','INT')  
   ,@DesignationId =C.value('(DesignationId)[1]','INT')  
   ,@CreatedBy =C.value('(CreatedBy)[1]','VARCHAR(20)')  
   ,@DocumentName =C.value('(DocumentName)[1]','VARCHAR(MAX)')  
   ,@Path =C.value('(Path)[1]','VARCHAR(MAX)')  
   --,@BudgetSegmentWiseMargin=C.value('(BudgetSegmentWiseMargin)[1]','BIT')  
   --,@CustomerDetailsModification=C.value('(CustomerDetailsModification)[1]','BIT')  
   --,@BillAdjustment=C.value('(BillAdjustment)[1]','BIT')  
   --,@TariffAdjustments=C.value('(TariffAdjustments)[1]','BIT')  
   --,@PaymentsAdjustments=C.value('(PaymentsAdjustments)[1]','BIT')  
   --,@Disconnection=C.value('(Disconnection)[1]','BIT')  
   --,@ReConnections=C.value('(ReConnections)[1]','BIT')  
   --,@BillGeneration=C.value('(BillGeneration)[1]','BIT')  
   ,@BusinessUnits=C.value('(BusinessUnits)[1]','VARCHAR(MAX)')  
   ,@ServiceCenters=C.value('(ServiceCenters)[1]','VARCHAR(MAX)')  
   ,@ServiceUnits=C.value('(ServiceUnits)[1]','VARCHAR(MAX)')  
   ,@Districts=C.value('(Districts)[1]','VARCHAR(MAX)')  
   ,@CashOffices=C.value('(CashOffices)[1]','VARCHAR(MAX)')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')
   ,@IsMobileAccess=C.value('(IsMobileAccess)[1]','BIT')
   FROM @XmlDoc.nodes('UserManagementBE')AS T(C)  
     
 IF NOT EXISTS(SELECT 0 FROM Tbl_UserDetails WHERE UserID=@UserId)  
  BEGIN  
  IF NOT EXISTS(SELECT 0 FROM Tbl_UserDetails WHERE PrimaryEmailId=@PrimaryEmailId AND @PrimaryEmailId != '')   
		BEGIN  
     
  INSERT INTO Tbl_UserLoginDetails(  UserId  
           ,[PassWord]  
           ,CreatedBy  
           ,CreatedDate  
          )  
        VALUES(  @UserId  
          ,@Password  
          ,@CreatedBy  
          ,GETDATE()  
            )  
              
   INSERT INTO Tbl_UserDetails(  
            UserId  
           ,Name  
           ,SurName  
           ,PrimaryContact  
           ,SecondaryContact  
           ,PrimaryEmailId  
           ,SecondaryEmailId  
           ,[Address]  
           ,Photo  
           ,GenderId  
           ,Details  
           ,RoleId  
           ,DesignationId  
           ,CreatedBy  
           ,CreatedDate  
           ,FilePath
           ,IsMobileAccess
           )  
         VALUES(@UserId   
          ,@Name  
          ,@SurName  
          ,@PrimaryContact  
          ,(CASE @SecondaryContact WHEN '' THEN NULL ELSE @SecondaryContact END)  
          ,(CASE @PrimaryEmailId WHEN '' THEN NULL ELSE @PrimaryEmailId END)
          ,(CASE @SecondaryEmailId WHEN '' THEN NULL ELSE @SecondaryEmailId END)  
          ,@Address  
          ,CASE @Photo WHEN '' THEN NULL ELSE @Photo END            
          ,@GenderId  
          ,(CASE @Details WHEN '' THEN NULL ELSE @Details END)  
          ,@RoleId  
          ,@DesignationId  
          ,@CreatedBy  
          ,DATEADD(SECOND,19800,GETUTCDATE())  
          ,(CASE @FilePath WHEN '' THEN NULL ELSE @FilePath END)
          ,@IsMobileAccess
          )  
  IF(@DocumentName!='')  
  BEGIN            
  INSERT INTO Tbl_UserDocuments(  
         UserId  
        ,DocumentName  
        ,[Path]  
        ,CreatedBy  
        ,CreatedDate)  
      SELECT  
         @UserId  
        ,com  
        ,@Path  
        ,@CreatedBy  
        ,DATEADD(SECOND,19800,GETUTCDATE())  
      FROM dbo.fn_Split(@DocumentName,',')  
  END  
    
  --INSERT INTO Tbl_UserSpecialPermissions(  
  -- UserId   
  -- ,BudgetSegmentWiseMargin   
  -- ,CustomerDetailsModification   
  -- ,BillAdjustment   
  -- ,TariffAdjustments   
  -- ,PaymentsAdjustments   
  -- ,Disconnection   
  -- ,ReConnections   
  -- ,BillGeneration   
  -- ,CreatedBy   
  -- ,CreatedDate   
  -- )  
  --VALUES(  
  --  @UserId  
  -- ,@BudgetSegmentWiseMargin  
  -- ,@CustomerDetailsModification  
  -- ,@BillAdjustment  
  -- ,@TariffAdjustments  
  -- ,@PaymentsAdjustments  
  -- ,@Disconnection  
  -- ,@ReConnections  
  -- ,@BillGeneration  
  -- ,@CreatedBy  
  -- ,GETDATE()  
  -- )  
     
  IF(@RoleId=2 OR @RoleId=3)  
  BEGIN  
   INSERT INTO Tbl_UserBusinessUnits(  
    BU_ID  
    ,UserId  
    ,CreatedDate  
    ,CreatedBy  
   )  
   SELECT  
    com  
    ,@UserId  
    ,GETDATE()  
    ,@CreatedBy  
   FROM dbo.fn_Split(@BusinessUnits,',')  
  END  
  ELSE IF(@RoleId=4)  
  BEGIN  
   INSERT INTO Tbl_UserServiceCenters(  
    BU_ID  
    ,SU_ID  
    ,ServiceCenterId  
    ,UserId  
    ,CreatedDate  
    ,CreatedBy  
   )  
   SELECT  
    @BU_ID  
    ,@SU_ID  
    ,com  
    ,@UserId  
    ,GETDATE()  
    ,@CreatedBy  
   FROM dbo.fn_Split(@ServiceCenters,',')  
   INSERT INTO Tbl_UserCashOffices(  
     CashOfficeId  
    ,UserId  
    ,CreatedDate  
    ,CreatedBy  
   )  
   SELECT  
    com  
    ,@UserId  
    ,@CurrentDate  
    ,@CreatedBy  
   FROM dbo.fn_Split(@CashOffices,',')     
  END  
  ELSE IF(@RoleId=5)  
  BEGIN     
     
   INSERT INTO Tbl_UserBusinessUnits(  
    BU_ID  
    ,UserId  
    ,CreatedDate  
    ,CreatedBy  
   )  
   SELECT  
    com  
    ,@UserId  
    ,GETDATE()  
    ,@CreatedBy  
   FROM dbo.fn_Split(@BusinessUnits,',')  
     
  END  
  ELSE IF(@RoleId=6)  
  BEGIN  
   INSERT INTO Tbl_UserServiceUnits(  
    BU_ID  
    ,SU_ID  
    ,UserId  
    ,CreatedDate  
    ,CreatedBy  
   )  
   SELECT  
    @BU_ID  
    ,com  
    ,@UserId  
    ,GETDATE()  
    ,@CreatedBy  
   FROM dbo.fn_Split(@ServiceUnits,',')  
  END  
  ELSE
	BEGIN
	--For all other roles
  INSERT INTO Tbl_UserBusinessUnits(  
    BU_ID  
    ,UserId  
    ,CreatedDate  
    ,CreatedBy  
   )  
   SELECT  
    com  
    ,@UserId  
    ,GETDATE()  
    ,@CreatedBy  
   FROM dbo.fn_Split(@BusinessUnits,',')  
  END
    SELECT 1 As IsSuccess  
    FOR Xml PATH('UserManagementBE'),TYPE  
      
   END  
  ELSE  
		BEGIN  
    SELECT 1 AS IsEmailIdExists  
    FOR Xml PATH('UserManagementBE'),TYPE  
   END  
  END  
 ELSE  
		BEGIN  
   SELECT 1 AS IsUserIdExists  
   FOR Xml PATH('UserManagementBE'),TYPE  
  END  
END
GO


GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDirectCustomersCount]    Script Date: 04/01/2015 20:10:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 06 OCT 2014                        
-- Description  : THIS function is use to get direct customers under the selecting cycle
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetDirectCustomersCount]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
,@ClusterCategoryId INT
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @MonthStartDate VARCHAR(50)         
    ,@Date VARCHAR(50)           
    ,@Result INT = 0    
    ,@ReadCustomersCount INT     
    
 IF(@ClusterCategoryId!=0)
	 BEGIN
			SELECT 
			@Result = COUNT(0) 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
			JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
			WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
			AND CD.ActiveStatusId IN(1,2) AND ReadCodeID = 1 AND ClusterCategoryId=@ClusterCategoryId
	 END
 ELSE
	 BEGIN
			SELECT 
			@Result = COUNT(0) 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
			JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
			WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
			AND CD.ActiveStatusId IN(1,2) AND ReadCodeID = 1 
	 END 
       
 RETURN @Result    
    
END 
GO

