GO
SET QUOTED_IDENTIFIER ON
GO
--=====================================================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : CHECK IS CUSTOMER HAVING ANY BILL 
--=====================================================================         
CREATE PROCEDURE [dbo].[IsBillExists]         
(        
@XmlDoc xml        
)         
AS        
BEGIN 
 DECLARE @GlobalAccountNo VARCHAR(50) 
		 ,@IsSuccess BIT=0   
 SELECT     
	@GlobalAccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)  
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
	BEGIN
		SET @IsSuccess=1 
	END	
	SELECT @IsSuccess AS IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE
END
GO
--------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--=====================================================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : CHECK IS CUSTOMER HAVING ANY BILL 
--=====================================================================         
CREATE PROCEDURE [dbo].[USP_IsNoBillAdjustmentExists]         
(        
@XmlDoc xml        
)         
AS        
BEGIN 
 DECLARE @GlobalAccountNo VARCHAR(50) 
		 ,@IsSuccess BIT=0   
 SELECT     
	@GlobalAccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)  
	
	IF EXISTS(SELECT 0 FROM Tbl_BillAdjustments WHERE AccountNo=@GlobalAccountNo and CustomerBillId IS Null)
	BEGIN
		SET @IsSuccess=1 
	END	
	SELECT @IsSuccess AS IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE
END
GO
-------------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Get No Bill adjustment Details
--===================================         
CREATE PROCEDURE [dbo].[USP_GetAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @AccountNo INT 
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','INT') 
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)   
     
     SELECT AmountEffected
			,BillAdjustmentId 
			,1 AS IsSuccess
			,AccountNo
	 FROM Tbl_BillAdjustments WHERE AccountNo=@AccountNo
	 
	 FOR XML PATH('BillAdjustmentsBe'),TYPE    
END

--------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment    
--===================================         
CREATE PROCEDURE [dbo].[USP_UpdateAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
  BEGIN     
  DECLARE    
   @BillAdjustmentId			 INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)        
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT     
       
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')    
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
   ,@BillAdjustmentId = C.value('(BillAdjustmentId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
  
       
   UPDATE Tbl_BillAdjustments 
	SET AmountEffected=@AmountEffected
		,TotalAmountEffected=@AmountEffected 
	WHERE BillAdjustmentId=@BillAdjustmentId
	
  UPDATE Tbl_BillAdjustmentDetails 
	SET EnergryCharges=@AmountEffected 
	WHERE BillAdjustmentId=@BillAdjustmentId
     
  SELECT @@ROWCOUNT As IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
----------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Delete No Bill adjustment    
--===================================         
CREATE PROCEDURE [dbo].[USP_DeleteAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId INT  
   ,@AccountNo VARCHAR(50) 
   
       
       
  SELECT     
	@BillAdjustmentId = C.value('(BillAdjustmentId)[1]','INT') 
	,@AccountNo= C.value('(AccountNo)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      
       
    SET @BillAdjustmentId=(SELECT BillAdjustmentId FROM Tbl_BillAdjustments WHERE AccountNo=@AccountNo AND CustomerBillId IS NULL)
	DELETE FROM  Tbl_BillAdjustments WHERE BillAdjustmentId=@BillAdjustmentId	
	DELETE Tbl_BillAdjustmentDetails WHERE BillAdjustmentId=@BillAdjustmentId
     
	SELECT @@ROWCOUNT As IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
