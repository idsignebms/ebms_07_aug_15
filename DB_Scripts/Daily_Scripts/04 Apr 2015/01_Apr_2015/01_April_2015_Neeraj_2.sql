
GO
/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress]    Script Date: 04/01/2015 14:50:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 1/APRIL/2015    
-- Description: This Procedure is used to change customer address    
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]    
(    
 @XmlDoc xml       
)    
AS    
BEGIN  
	DECLARE @TotalAddress INT	
			,@IsSameAsServie BIT
			,@GlobalAccountNo   VARCHAR(50)      
		   ,@NewPostalLandMark  VARCHAR(200)    
		   ,@NewPostalStreet   VARCHAR(200)    
		   ,@NewPostalCity   VARCHAR(200)    
		   ,@NewPostalHouseNo   VARCHAR(200)    
		   ,@NewPostalZipCode   VARCHAR(50)    
		   ,@NewServiceLandMark  VARCHAR(200)    
		   ,@NewServiceStreet   VARCHAR(200)    
		   ,@NewServiceCity   VARCHAR(200)    
		   ,@NewServiceHouseNo  VARCHAR(200)    
		   ,@NewServiceZipCode  VARCHAR(50)    
		   ,@NewPostalAreaCode  VARCHAR(50)    
		   ,@NewServiceAreaCode  VARCHAR(50)    
		   ,@NewPostalAddressID  INT    
		   ,@NewServiceAddressID  INT   
		   ,@ModifiedBy    VARCHAR(50)    
		   ,@Details     VARCHAR(MAX)    
		   ,@RowsEffected    INT    
		   ,@AddressID INT   
		   ,@StatusText VARCHAR(50)
		   ,@IsCommunicationPostal BIT
		   ,@IsCommunicationService BIT
		   ,@ApprovalStatusId INT=2  
		   ,@PostalAddressID INT
		   ,@ServiceAddressID INT
       SELECT       
			 @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')    
		  --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')        
			  ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(200)')        
			  ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')        
			  ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')       
			  ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')      
			  --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')        
			  ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(200)')        
			  ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')        
			  ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')       
			  ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')    
			  ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')    
			  ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')    
			  ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')    
			  ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')    
			  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
			  ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')    
			  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			  ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')    
			  ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')
			  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
BEGIN TRY
		BEGIN TRAN 
		  --UPDATE POSTAL ADDRESS
	SET @StatusText='Total address count.'			
	SET @TotalAddress=(SELECT COUNT(0) 
						FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails 
						WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1
					   )
	--=====================================================================================					   
	--Customer has only one addres and wants to update the address. //CONDITION 1//
	--=====================================================================================	
	IF(@TotalAddress =1 AND @IsSameAsServie = 1)
	BEGIN
		SET @StatusText='CONDITION 1'			
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewPostalLandMark
				,Service_StreetName=@NewPostalStreet
				,Service_City=@NewPostalCity
				,Service_HouseNo=@NewPostalHouseNo
				,Service_ZipCode=@NewPostalZipCode
				,Service_AreaCode=@NewPostalAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@PostalAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=1
		WHERE GlobalAccountNumber=@GlobalAccountNo
	END	   
	--=====================================================================================
	--Customer has one addres and wants to add new address. //CONDITION 2//
	--=====================================================================================
	ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)
	BEGIN
		SET @StatusText='CONDITION 2'	
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		
		INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
																HouseNo
																,LandMark
																,StreetName
																,City
																,AreaCode
																,ZipCode
																,ModifedBy
																,ModifiedDate
																,IsCommunication
																,IsServiceAddress
																,IsActive
																,GlobalAccountNumber
																)
														 VALUES (@NewServiceHouseNo       
																,@NewServiceLandMark       
																,@NewServiceStreet        
																,@NewServiceCity      
																,@NewServiceAreaCode      
																,@NewServiceZipCode        
																,@ModifiedBy      
																,dbo.fn_GetCurrentDateTime()  
																,@IsCommunicationService         
																,1
																,1
																,@GlobalAccountNo
																)	
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewServiceLandMark
				,Service_StreetName=@NewServiceStreet
				,Service_City=@NewServiceCity
				,Service_HouseNo=@NewServiceHouseNo
				,Service_ZipCode=@NewServiceZipCode
				,Service_AreaCode=@NewServiceAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@ServiceAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=0
		WHERE GlobalAccountNumber=@GlobalAccountNo															
	END	  
	
	--=====================================================================================
	--Customer alrady has tow address and wants to in-active service. //CONDITION 3//
	--=====================================================================================
	ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)	
	BEGIN
		SET @StatusText='CONDITION 3'			
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				IsActive=0
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewPostalLandMark
				,Service_StreetName=@NewPostalStreet
				,Service_City=@NewPostalCity
				,Service_HouseNo=@NewPostalHouseNo
				,Service_ZipCode=@NewPostalZipCode
				,Service_AreaCode=@NewPostalAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@PostalAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=1
		WHERE GlobalAccountNumber=@GlobalAccountNo
	END	 
	--=====================================================================================
	--Customer alrady has tow address and wants to update both address. //CONDITION 4//
	--=====================================================================================
	ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)
	BEGIN
		SET @StatusText='CONDITION 4'			
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END      
			   ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END      
			   ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END      
			   ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END      
			   ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END        
			   ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END         
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationService
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewServiceLandMark
				,Service_StreetName=@NewServiceStreet
				,Service_City=@NewServiceCity
				,Service_HouseNo=@NewServiceHouseNo
				,Service_ZipCode=@NewServiceZipCode
				,Service_AreaCode=@NewServiceAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@ServiceAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=0
		WHERE GlobalAccountNumber=@GlobalAccountNo			
	END	 
	COMMIT TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SET @RowsEffected=0
END CATCH
	SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')         
END