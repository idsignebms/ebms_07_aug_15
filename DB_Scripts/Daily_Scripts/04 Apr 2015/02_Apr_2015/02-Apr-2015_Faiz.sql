GO
-- =============================================  
-- Author:  Neeraj Kanojiya  
-- Create date: 21-April-2014  
-- Description: Get All NEws  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetNotificationsBYID]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE  @PageNo INT  
   ,@PageSize INT,  
   @CreatedBy varchar(20),  
   @BU_ID varchar(50),  
   @NoticeID INT   
    
  SELECT   
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
   ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(20)')  
   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')     
   ,@NoticeID = C.value('(NoticeID)[1]','INT')  
   FROM @XmlDoc.nodes('NotificationsBE') AS T (C)  
  ;WITH PagedResults AS  
  (    
    select ROW_NUMBER() OVER(ORDER BY NoticeID) AS Srno,* FROM Tbl_Notifications WHERE activestatus=1 AND NoticeID=@NoticeID  
    UNION  
    select ROW_NUMBER() OVER(ORDER BY NoticeID)+1 AS Srno,* FROM Tbl_Notifications WHERE Activestatus=1 and CreatedBy=@CreatedBy and NoticeID!=@NoticeID  
  )  
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults P  
   LEFT JOIN Tbl_BUNotice N ON N.Notice_ID=P.NoticeID     
   WHERE Srno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   AND N.BU_ID=@BU_ID   
   order by Srno
   FOR XML PATH('NotificationsBE'),type  
   )   
   FOR XML PATH(''),root('NotificationsBEXML')  
END  
  GO
----------------------------------------------------------------------------------------------------------------------  


GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateRole]    Script Date: 04/02/2015 18:35:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Faiz Mohammed>    
-- Create date: <19-01-2015>    
-- Description: <For Updateing Role Data in Tbl_NewPagePermissions>    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_UpdateRole]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @RoleId INT
		 ,@Permissions VARCHAR(MAX)  
		 ,@ModifiedBy VARCHAR(50)
		 ,@ModifiedDate DATETIME  
		 ,@ExistedRoleId INT
		 ,@ExistedCount INT
		 ,@IsValid BIT  
		 ,@StatusText VARCHAR(MAX)  
       
 SELECT  @RoleId=C.value('(RoleId)[1]','INT')   
		,@Permissions=C.value('(Permissions)[1]','VARCHAR(MAX)')     
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')   
 FROM	@XmlDoc.nodes('AdminBE') AS T(C)   

--Start Validating if Role wtih the same permisssions already exists in the Tbl_NewPagePermissions
--If Role Name is doesn't Exist
		  --Start Validating if Role wtih the same permisssions already exists in the Tbl_NewPagePermissions
			DECLARE @TempPagePermissions TABLE(RoleId INT, PagePermissions VARCHAR(MAX))		
			
			DELETE @TempPagePermissions
			
			INSERT INTO @TempPagePermissions(RoleId,PagePermissions)
			SELECT DISTINCT P.Role_Id,
			STUFF((SELECT  ',' + CONVERT(VARCHAR(MAX), P1.MenuId)
				 FROM  Tbl_NewPagePermissions P1
				 WHERE P.Role_Id = P1.Role_Id  ORDER BY P1.MenuId
					FOR XML PATH(''), TYPE
					).value('.', 'NVARCHAR(MAX)')
				,1,1,'') MenuId 
			FROM Tbl_NewPagePermissions P;
			
			--SELECT * FROM @TempPagePermissions
			
			--Arranging the permission id in increasing order to verify
			DECLARE @PermissionsSortder Table (PermissionId INT)
			DELETE @PermissionsSortder
			INSERT INTO @PermissionsSortder SELECT * FROM dbo.fn_Split (@Permissions,',')
			
			SELECT @Permissions = STUFF((SELECT  ',' + CONVERT(VARCHAR(MAX), PermissionId)
				 FROM  @PermissionsSortder ORDER BY PermissionId FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')  
			--End of sorting permission id's

			PRINT @Permissions

			--Validating the permission id's exist for other roles
			IF EXISTS( SELECT RoleId FROM @TempPagePermissions WHERE PagePermissions=@Permissions and RoleId!=@RoleId )
			BEGIN
				--If Same Role Permissones Already Exist
				SELECT	-1 AS RowsEffected
				,(SELECT RoleName FROM Tbl_MRoles 
					WHERE RoleId=(SELECT RoleId FROM @TempPagePermissions WHERE PagePermissions=@Permissions)) AS ExistingRoleName  
				FOR XML PATH ('AdminBE'),TYPE 
			END
			ELSE
			BEGIN
					   DELETE FROM Tbl_NewPagePermissions WHERE Role_Id = @RoleId --Faiz-ID103 02-Apr-2015
					 --GET DATE FROM UDF  
					 SET @ModifiedDate = dbo.fn_GetCurrentDateTime();  

						BEGIN TRY  
							CREATE TABLE #PermissionList(  
								RoleId INT  
								,MenuId INT  
								,CreatedDate DATETIME  
								,CreatedBy VARCHAR(50)  
								)  
							INSERT INTO #PermissionList(  
								RoleId  
								,MenuId  
								,CreatedDate  
								,CreatedBy  
							   )  
							SELECT @RoleId  
							,MenuId  
							,@ModifiedDate  AS CreatedDate  
							,@ModifiedBy AS CreatedBy  
							from  [dbo].[fn_splitMenuId](@Permissions,',')  
						END TRY  
					 BEGIN CATCH  
						  SET @IsValid=0;  
						  SET @StatusText='Cannot prepare MenuId list.';  
					 END CATCH  
					     
					   IF EXISTS(SELECT 0 FROM #PermissionList)  
						BEGIN  
							INSERT INTO Tbl_NewPagePermissions(  
								  Role_Id  
								  ,MenuId  
								  ,Created_Date  
								  ,Created_By  
								  )  
							SELECT * FROM #PermissionList  
					     END 
					    
					   SELECT @@ROWCOUNT AS RowsEffected, 0 AS IsRolePermissionsExists    
					   FOR XML PATH ('AdminBE'),TYPE    
					     
					   DROP TABLE #PermissionList  
			END
END
GO
----------------------------------------------------------------------------------------------------------------------  
