GO
ALTER TABLE Tbl_CustomerNameChangeLogs
ADD PresentApprovalRole INT, NextApprovalRole INT
GO
ALTER TABLE Tbl_CustomerNameChangeLogs
ALTER COLUMN OldKnownAs VARCHAR(150)
GO
CREATE TABLE Tbl_Audit_CustomerNameChangeLogs
(
	 NameChangeLogId INT
	,AccountNo VARCHAR(50)
	,GlobalAccountNumber VARCHAR(50)
	,OldTitle VARCHAR(10)
	,NewTitle VARCHAR(10)
	,OldFirstName VARCHAR(50)
	,NewFirstName VARCHAR(50)
	,OldMiddleName VARCHAR(50)
	,NewMiddleName VARCHAR(50)
	,OldLastName VARCHAR(50)
	,NewLastName VARCHAR(50)
	,OldKnownAs VARCHAR(150)
	,NewKnownAs VARCHAR(150)
	,ApproveStatusId INT
	,Remarks VARCHAR(500)
	,CreatedDate DATETIME
	,CreatedBy VARCHAR(50)
	,ModifedBy VARCHAR(50)
	,ModifiedDate DATETIME 
	,PresentApprovalRole INT
	,NextApprovalRole INT
)
GO
----------------------------------------------------------------------------------------
