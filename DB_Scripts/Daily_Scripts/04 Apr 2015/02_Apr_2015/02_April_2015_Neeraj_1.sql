GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/02/2015 11:39:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author		:  NEERAJ KANOJIYA
-- Create date	:  27-MARCH-2015
-- Description	:  GET CUSTOMERS FULL DETAILS
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  

    DECLARE  @GlobalAccountNumber VARCHAR(50)  
    SELECT            
	   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)     
		SELECT top 1
		
		CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord
		,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord
		,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord
		,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord
		,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs
		,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord
		,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord
		,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord
		,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord
		,CD.DocumentNo	
		,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate	
		,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate	
		,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate	
		,CD.OldAccountNo	
		,CT.CustomerType
		,BookCode
		,CD.PoleID	
		,CD.MeterNumber	
		,TC.ClassName as Tariff	
		,CPD.IsEmbassyCustomer	
		,CPD.EmbassyCode	
		,CD.PhaseId	
		,RC.ReadCode as ReadType
		,CD.IsVIPCustomer
		,CD.IsBEDCEmployee
		,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService	
		,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService	
		,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService
		,CASE PAD.AreaCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.AreaCode END AS AreaService
		,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode			
		,PAD.IsCommunication AS IsCommunicationService			
		,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal	
		,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal	
		,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL	
		,CASE PAD1.AreaCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.AreaCode END AS  AreaPostal		
		,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode
		,PAD.IsCommunication AS IsCommunicationPostal				
		,CD.ServiceAddressID		
		,CAD.IsCAPMI
		,InitialBillingKWh	
		,CAD.InitialReading	
		--,PresentReading	
		,CD.AvgReading as AverageReading	
		,CD.Highestconsumption	
		,CD.OutStandingAmount	
		,OpeningBalance
		,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1
		,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2
		,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType
		,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName
		,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName
		,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase
		,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName
		,CTD.TenentId
		,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent
		,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent
		,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent
		,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent
		,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent
		,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent
		,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent
		,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName
		,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy
		,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber
		,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount
		,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy
		,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy  
		from UDV_CustomerDescription CD
		left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId
		left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId
		left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0
		left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber 
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber
		LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId
		LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId
		LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId
		LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo
		LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId
		LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId
		Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber
		FOR XML PATH('CustomerRegistrationBE'),TYPE
END

------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress]    Script Date: 04/01/2015 14:50:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 2/APRIL/2015    
-- Description: This Procedure is used to change customer address and book no.   
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]    
(    
 @XmlDoc xml       
)    
AS    
BEGIN  
	DECLARE @TotalAddress INT	
			,@IsSameAsServie BIT
			,@GlobalAccountNo   VARCHAR(50)      
		   ,@NewPostalLandMark  VARCHAR(200)    
		   ,@NewPostalStreet   VARCHAR(200)    
		   ,@NewPostalCity   VARCHAR(200)    
		   ,@NewPostalHouseNo   VARCHAR(200)    
		   ,@NewPostalZipCode   VARCHAR(50)    
		   ,@NewServiceLandMark  VARCHAR(200)    
		   ,@NewServiceStreet   VARCHAR(200)    
		   ,@NewServiceCity   VARCHAR(200)    
		   ,@NewServiceHouseNo  VARCHAR(200)    
		   ,@NewServiceZipCode  VARCHAR(50)    
		   ,@NewPostalAreaCode  VARCHAR(50)    
		   ,@NewServiceAreaCode  VARCHAR(50)    
		   ,@NewPostalAddressID  INT    
		   ,@NewServiceAddressID  INT   
		   ,@ModifiedBy    VARCHAR(50)    
		   ,@Details     VARCHAR(MAX)    
		   ,@RowsEffected    INT    
		   ,@AddressID INT   
		   ,@StatusText VARCHAR(50)
		   ,@IsCommunicationPostal BIT
		   ,@IsCommunicationService BIT
		   ,@ApprovalStatusId INT=2  
		   ,@PostalAddressID INT
		   ,@ServiceAddressID INT
		   ,@BookNo VARCHAR(50)
       SELECT       
			 @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')    
		  --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')        
			  ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(200)')        
			  ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')        
			  ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')       
			  ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')      
			  --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')        
			  ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(200)')        
			  ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')        
			  ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')       
			  ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')    
			  ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')    
			  ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')    
			  ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')    
			  ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')    
			  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
			  ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')    
			  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			  ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')    
			  ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')
			  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
			  ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
BEGIN TRY
		BEGIN TRAN 
		  --UPDATE POSTAL ADDRESS
	SET @StatusText='Total address count.'			
	SET @TotalAddress=(SELECT COUNT(0) 
						FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails 
						WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1
					   )
					   
	--=====================================================================================					   
	--Update book number of customer.
	--=====================================================================================	
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
	SET BookNo=@BookNo 
	WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1			   
	--=====================================================================================					   
	--Customer has only one addres and wants to update the address. //CONDITION 1//
	--=====================================================================================				
	IF(@TotalAddress =1 AND @IsSameAsServie = 1)
	BEGIN
		SET @StatusText='CONDITION 1'			
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewPostalLandMark
				,Service_StreetName=@NewPostalStreet
				,Service_City=@NewPostalCity
				,Service_HouseNo=@NewPostalHouseNo
				,Service_ZipCode=@NewPostalZipCode
				,Service_AreaCode=@NewPostalAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@PostalAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=1
		WHERE GlobalAccountNumber=@GlobalAccountNo
	END	   
	--=====================================================================================
	--Customer has one addres and wants to add new address. //CONDITION 2//
	--=====================================================================================
	ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)
	BEGIN
		SET @StatusText='CONDITION 2'	
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		
		INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
																HouseNo
																,LandMark
																,StreetName
																,City
																,AreaCode
																,ZipCode
																,ModifedBy
																,ModifiedDate
																,IsCommunication
																,IsServiceAddress
																,IsActive
																,GlobalAccountNumber
																)
														 VALUES (@NewServiceHouseNo       
																,@NewServiceLandMark       
																,@NewServiceStreet        
																,@NewServiceCity      
																,@NewServiceAreaCode      
																,@NewServiceZipCode        
																,@ModifiedBy      
																,dbo.fn_GetCurrentDateTime()  
																,@IsCommunicationService         
																,1
																,1
																,@GlobalAccountNo
																)	
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewServiceLandMark
				,Service_StreetName=@NewServiceStreet
				,Service_City=@NewServiceCity
				,Service_HouseNo=@NewServiceHouseNo
				,Service_ZipCode=@NewServiceZipCode
				,Service_AreaCode=@NewServiceAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@ServiceAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=0
		WHERE GlobalAccountNumber=@GlobalAccountNo															
	END	  
	
	--=====================================================================================
	--Customer alrady has tow address and wants to in-active service. //CONDITION 3//
	--=====================================================================================
	ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)	
	BEGIN
		SET @StatusText='CONDITION 3'			
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				IsActive=0
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewPostalLandMark
				,Service_StreetName=@NewPostalStreet
				,Service_City=@NewPostalCity
				,Service_HouseNo=@NewPostalHouseNo
				,Service_ZipCode=@NewPostalZipCode
				,Service_AreaCode=@NewPostalAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@PostalAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=1
		WHERE GlobalAccountNumber=@GlobalAccountNo
	END	 
	--=====================================================================================
	--Customer alrady has tow address and wants to update both address. //CONDITION 4//
	--=====================================================================================
	ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)
	BEGIN
		SET @StatusText='CONDITION 4'			
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END      
			   ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END      
			   ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END      
			   ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END      
			   ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END        
			   ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END      
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationPostal     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1	
		
		UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET      
				LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END      
			   ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END      
			   ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END      
			   ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END      
			   ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END        
			   ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END         
			   ,ModifedBy=@ModifiedBy      
			   ,ModifiedDate=dbo.fn_GetCurrentDateTime()   
			   ,IsCommunication=@IsCommunicationService
		WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1
		SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)
		SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)
		UPDATE CUSTOMERS.Tbl_CustomersDetail SET
				Service_Landmark=@NewServiceLandMark
				,Service_StreetName=@NewServiceStreet
				,Service_City=@NewServiceCity
				,Service_HouseNo=@NewServiceHouseNo
				,Service_ZipCode=@NewServiceZipCode
				,Service_AreaCode=@NewServiceAreaCode
				,Postal_Landmark=@NewPostalLandMark
				,Postal_StreetName=@NewPostalStreet
				,Postal_City=@NewPostalCity
				,Postal_HouseNo=@NewPostalHouseNo
				,Postal_ZipCode=@NewPostalZipCode
				,Postal_AreaCode=@NewPostalAreaCode
				,ServiceAddressID=@ServiceAddressID
				,PostalAddressID=@PostalAddressID
				,IsSameAsService=0
		WHERE GlobalAccountNumber=@GlobalAccountNo			
	END	 
	COMMIT TRAN
END TRY
BEGIN CATCH
	ROLLBACK TRAN
	SET @RowsEffected=0
END CATCH
	SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')         
END