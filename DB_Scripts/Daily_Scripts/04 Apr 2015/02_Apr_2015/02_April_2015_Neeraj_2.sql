
GO
/****** Object:  StoredProcedure [dbo].[USP_AdjustmentForNoBill]    Script Date: 04/02/2015 17:49:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment    
--===================================         
ALTER PROCEDURE [dbo].[USP_AdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId			 INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)    
   ,@TotalAmountEffected DECIMAL(18,2)    
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT     
       
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')    
   ,@TotalAmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
  
       
   INSERT INTO Tbl_BillAdjustments(    
     AccountNo    
     ,CustomerId    
     ,AmountEffected   
     ,BillAdjustmentType    
     ,TotalAmountEffected   
     )           
    VALUES(         
      @AccountNo    
     ,@CustomerId    
     ,@AmountEffected   
     ,@BillAdjustmentType   
     ,@TotalAmountEffected  
     )      
  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
       
  INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)    
  SELECT           
   @BillAdjustmentId     
   ,@AmountEffected    
     
  SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
 -------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentForNoBill]    Script Date: 04/02/2015 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Get No Bill adjustment Details
--===================================         
ALTER PROCEDURE [dbo].[USP_GetAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @AccountNo VARCHAR(50) 
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)   
     IF NOT EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber=@AccountNo)
		SET @AccountNo=(SELECT GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail WHERE OldAccountNo=@AccountNo)
		
     SELECT AmountEffected
			,BillAdjustmentId 
			,1 AS IsSuccess
			,AccountNo
	 FROM Tbl_BillAdjustments WHERE AccountNo=@AccountNo
	 
	 FOR XML PATH('BillAdjustmentsBe'),TYPE    
END

--------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteAdjustmentForNoBill]    Script Date: 04/02/2015 18:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Delete No Bill adjustment    
--===================================         
ALTER PROCEDURE [dbo].[USP_DeleteAdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
   @BillAdjustmentId INT  
   ,@AccountNo VARCHAR(50) 
   
       
       
  SELECT     
	@BillAdjustmentId = C.value('(BillAdjustmentId)[1]','INT') 
	,@AccountNo= C.value('(AccountNo)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      
       
   IF NOT EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber=@AccountNo)
	SET @AccountNo=(SELECT GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail WHERE OldAccountNo=@AccountNo)
		
		
    SET @BillAdjustmentId=(SELECT BillAdjustmentId FROM Tbl_BillAdjustments WHERE AccountNo=@AccountNo AND CustomerBillId IS NULL)
	DELETE FROM  Tbl_BillAdjustments WHERE BillAdjustmentId=@BillAdjustmentId	
	DELETE Tbl_BillAdjustmentDetails WHERE BillAdjustmentId=@BillAdjustmentId
     
	SELECT @@ROWCOUNT As IsSuccess FOR XML PATH('BillAdjustmentsBe'),TYPE    
END
------------------------------------------------------------------------------------------
GO

/****** Object:  View [dbo].[UDV_CustomerDescription]    Script Date: 04/02/2015 18:54:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[UDV_CustomerDescription]    
AS    
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs,     
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo,     
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading,     
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID    
                      ,CD.Service_HouseNo       
   ,CD.Service_StreetName        
   ,CD.Service_City       
   ,CD.Service_ZipCode               
        
   ,CD.Postal_ZipCode    
   ,CD.Postal_HouseNo       
   ,CD.Postal_StreetName        
   ,CD.Postal_City    
   ,CD.Postal_Landmark    
   ,Cd.Service_Landmark    
   ,Cd.ApplicationDate    
   ,Cd.SetupDate    
   ,CD.TenentId    
   ,CD.IsVIPCustomer    
   ,IsCAPMI    
   ,CD.IsBEDCEmployee    
   ,MeterAmount    
   ,MS.StatusId as CustomerStatusID    
   ,PD.IsEmbassyCustomer    
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
   ,BN.Details AS BookDetails  
   ,CD.IsSameAsService
       
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN    
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo    
    

------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/02/2015 18:54:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author		:  NEERAJ KANOJIYA
-- Create date	:  27-MARCH-2015
-- Description	:  GET CUSTOMERS FULL DETAILS
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  

    DECLARE  @GlobalAccountNumber VARCHAR(50)  
    SELECT            
	   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)     
		SELECT top 1
		
		CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord
		,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord
		,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord
		,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord
		,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs
		,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord
		,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord
		,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord
		,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord
		,CD.DocumentNo	
		,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate	
		,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate	
		,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate	
		,CD.OldAccountNo	
		,CT.CustomerType
		,BookCode
		,CD.PoleID	
		,CD.MeterNumber	
		,TC.ClassName as Tariff	
		,CPD.IsEmbassyCustomer	
		,CPD.EmbassyCode	
		,CD.PhaseId	
		,RC.ReadCode as ReadType
		,CD.IsVIPCustomer
		,CD.IsBEDCEmployee
		,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService	
		,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService	
		,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService
		,PAD.AreaCode AS AreaService 
		,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode			
		,PAD.IsCommunication AS IsCommunicationService			
		,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal	
		,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal	
		,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL	
		,PAD1.AreaCode AS  AreaPostal		
		,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode
		,PAD.IsCommunication AS IsCommunicationPostal				
		,CD.ServiceAddressID		
		,CAD.IsCAPMI
		,InitialBillingKWh	
		,CAD.InitialReading	
		--,PresentReading	
		,CD.AvgReading as AverageReading	
		,CD.Highestconsumption	
		,CD.OutStandingAmount	
		,OpeningBalance
		,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1
		,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2
		,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType
		,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName
		,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName
		,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase
		,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName
		,CTD.TenentId
		,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent
		,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent
		,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent
		,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent
		,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent
		,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent
		,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent
		,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName
		,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy
		,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber
		,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount
		,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy
		,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy  
		,CD.IsSameAsService
		from UDV_CustomerDescription CD
		left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId
		left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId
		left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0
		left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber 
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber
		LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId
		LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId
		LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId
		LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo
		LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId
		LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId
		Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber
		FOR XML PATH('CustomerRegistrationBE'),TYPE
END

------------------------------------------------------------------------------------------------------------
