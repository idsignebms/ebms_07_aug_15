GO
ALTER TABLE Tbl_LCustomerTariffChangeRequest
ADD OldClusterCategoryId INT, NewClusterCategoryId INT
GO
ALTER TABLE Tbl_Audit_CustomerTariffChangeRequest
ADD OldClusterCategoryId INT, NewClusterCategoryId INT
GO
----------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')				
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	--IF(@TrxFrom = '')
	--	BEGIN
	--		SELECT @TrxFrom = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
	--				 FROM MASTERS.Tbl_MMeterReadingsFrom 
	--				 FOR XML PATH(''), TYPE)
	--				.value('.','NVARCHAR(MAX)'),1,1,'')
	--	END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, PT.PaymentType AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy 
	INNER JOIN (SELECT [com] AS PaymentType FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.PaymentType = CR.PaymentType
	INNER JOIN Tbl_MPaymentType PT ON PT.PaymentTypeID = CR.PaymentType  
	  
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 14-08-2014  
-- Modified date : 06-12-2014
-- Modified By : T.Karthik
-- Description: The purpose of this procedure is to get list of Tariff Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffApprovalList]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
		 ,@PageSize INT
		 ,@BU_ID VARCHAR(MAX)
		 ,@BookNo VARCHAR(MAX)
		 ,@UserId VARCHAR(50)
		 ,@RoleId INT
		 ,@UserIds VARCHAR(MAX)=''
      
  SELECT     
		    @PageNo = C.value('(PageNo)[1]','INT')  
		   ,@PageSize = C.value('(PageSize)[1]','INT')
		   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		   ,@UserId=C.value('(UserId)[1]','VARCHAR(MAX)')
  FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)   
  
  SET @RoleId=(SELECT RoleId FROM Tbl_UserDetails WHERE UserId=@UserId)
  SET @UserIds=(SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId=3 AND RoleId=@RoleId)
  
  SET @UserId=(SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId=@UserId)
  
  SELECT @UserIds=(CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
  CREATE TABLE #TariffApprovalList(Sno INT PRIMARY KEY IDENTITY(1,1), TariffChangeRequestId INT)
    
  INSERT INTO #TariffApprovalList(TariffChangeRequestId)
	SELECT
		TariffChangeRequestId
	FROM Tbl_LCustomerTariffChangeRequest T
		JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber=T.AccountNo
		WHERE T.ApprovalStatusId IN(1,4)
		AND (CD.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID,',')) OR @BU_ID = '')  
		AND (CD.BookNo IN(SELECT [com] FROM dbo.fn_Split(@BookNo,',')) OR @BookNo = '')
		
	DECLARE @Count INT
	SET @Count=(Select COUNT(0) from #TariffApprovalList) 

	SELECT  
		  Sno AS RowNumber
		 ,T.TariffChangeRequestId
		 ,T.AccountNo
		 ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) As Name
		 ,(SELECT dbo.fn_GetCustomerAddress(T.AccountNo)) As ServiceAddress
		 ,PreviousTariffId AS OldClassID
		 ,ChangeRequestedTariffId AS NewClassID
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=PreviousTariffId) AS PreviousTariffName
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=ChangeRequestedTariffId) AS ChangeRequestedTariffName
		 ,T.OldClusterCategoryId AS OldClusterTypeId
		 ,T.NewClusterCategoryId AS NewClusterTypeId
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.OldClusterCategoryId) AS PreviousClusterName
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.NewClusterCategoryId) AS ChangeRequestedClusterName
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From '+NR.RoleName ELSE APS.ApprovalStatus+' By '+NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole=@RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds='')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,@Count AS TotalRecords
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_LCustomerTariffChangeRequest T
	JOIN #TariffApprovalList LT ON T.TariffChangeRequestId=LT.TariffChangeRequestId
	JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber=T.AccountNo
	JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId=APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole=NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole=CR.RoleId
	AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	ORDER BY IsPerformAction DESC 
	
	SELECT
		 MessageId
		,[Message]
		,TRM.TariffChangeRequestId
		,CreatedBy
		,CONVERT(VARCHAR,CreatedDate,100) AS CreatedDate
	FROM Tbl_TariffRequestMessages AS TRM
	JOIN #TariffApprovalList AS LTL ON TRM.TariffChangeRequestId=LTL.TariffChangeRequestId
	AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		  
END
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- =============================================  
CREATE PROCEDURE [dbo].[USP_UpdateTariffDetails_New]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
  
	DECLARE  
		 @AccountNo VARCHAR(50)   
		,@CreatedBy VARCHAR(50)  
		,@Remarks VARCHAR(50)  
		,@Flag INT  
		,@ApprovalStatusId INT  
		,@OldClassID INT  
		,@NewClassID INT  
		,@TariffChangeRequestId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@OldClusterTypeId INT  
		,@NewClusterTypeId INT  

	SELECT   
		 @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@Flag = C.value('(Flag)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@OldClassID = C.value('(OldClassID)[1]','INT')  
		,@NewClassID = C.value('(NewClassID)[1]','INT')  
		,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@OldClusterTypeId = C.value('(OldClusterTypeId)[1]','INT')  
		,@NewClusterTypeId = C.value('(ClusterTypeId)[1]','INT') 
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
	IF(@Flag = 1)  -- Approval Permission Required
	BEGIN  
		IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo = @AccountNo AND ApprovalStatusId = 1)  
			BEGIN  -- Pending Request already exists
				SELECT 1 AS ApprovalProcess  
			END  
		ELSE  -- New Request
			BEGIN  
				DECLARE @RoleId INT,@IsFinalApproval BIT=0
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
				SET @CurrentLevel = 0
				
				--SELECT 
				--	 @CurrentLevel = [Level] 
				--	,@IsFinalApproval = (CASE IsFinalApproval
				--						 WHEN 1 THEN (CASE WHEN UserIds IS NOT NULL 
				--											THEN (CASE WHEN EXISTS(SELECT 0 FROM dbo.fn_Split(UserIds,',') WHERE @CreatedBy = com) 
				--														THEN 1 END)
				--						 ELSE 1 END)
				--						 END)
				--FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId 
				--	AND	RoleId = @RoleId

				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

				INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					 AccountNo  
					,PreviousTariffId  
					,ChangeRequestedTariffId 
					,OldClusterCategoryId
					,NewClusterCategoryId 
					,CreatedBy  
					,CreatedDate  
					,Remarks  
					,ApprovalStatusId
					,PresentApprovalRole
					,NextApprovalRole)  
				VALUES(  
					 @AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@OldClusterTypeId
					,@NewClusterTypeId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,1 -- Process
					,@PresentRoleId
					,@NextRoleId)  

				--IF(@IsFinalApproval = 1)
				--	BEGIN
				--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
				--		SET       
				--			 TariffClassID = @NewClassID
				--			,ClusterCategoryId = @NewClusterTypeId  
				--			,ModifedBy = @CreatedBy  
				--			,ModifiedDate = dbo.fn_GetCurrentDateTime()  
				--		WHERE GlobalAccountNumber = @AccountNo 
						
				--		INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				--			 AccountNo  
				--			,PreviousTariffId  
				--			,ChangeRequestedTariffId 
				--			,OldClusterCategoryId
				--			,NewClusterCategoryId 
				--			,CreatedBy  
				--			,CreatedDate  
				--			,Remarks  
				--			,ApprovalStatusId
				--			,PresentApprovalRole
				--			,NextApprovalRole)  
				--		VALUES(  
				--			 @AccountNo  
				--			,@OldClassID  
				--			,@NewClassID  
				--			,@OldClusterTypeId
				--			,@NewClusterTypeId
				--			,@CreatedBy  
				--			,dbo.fn_GetCurrentDateTime()  
				--			,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				--			,2 --Approved
				--			,@PresentRoleId
				--			,NULL)
				--	END

				SELECT 1 As IsSuccess,@IsFinalApproval AS IsFinalApproval  
				FOR XML PATH('TariffManagementBE'),TYPE		
			END  
		END  
	ELSE IF(@Flag = 2)  -- Tariff Approval Page
		BEGIN  
			IF(@ApprovalStatusId = 2)  -- Request Approved
				BEGIN  
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
						BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
											RoleId = (SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
													WHERE TariffChangeRequestId = @TariffChangeRequestId))

							SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

							IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApprovalStatusId = @ApprovalStatusId
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId  

									UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
									SET       
										 TariffClassID = @NewClassID
										,ClusterCategoryId = @NewClusterTypeId  
										,ModifedBy = @CreatedBy  
										,ModifiedDate = dbo.fn_GetCurrentDateTime()  
									WHERE GlobalAccountNumber = @AccountNo  
									
									INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
										 TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId  
										,ChangeRequestedTariffId 
										,OldClusterCategoryId
										,NewClusterCategoryId 
										,CreatedBy  
										,CreatedDate  
										,Remarks  
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole)  
									SELECT   
										 @TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId
										,ChangeRequestedTariffId  
										,OldClusterCategoryId
										,NewClusterCategoryId
										,@CreatedBy  
										,dbo.fn_GetCurrentDateTime()  
										,Remarks 
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole
									FROM Tbl_LCustomerTariffChangeRequest
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId 
								END
							ELSE -- Not a final approval
								BEGIN
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId
										,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
									WHERE AccountNo = @AccountNo 
									AND TariffChangeRequestId = @TariffChangeRequestId
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							UPDATE Tbl_LCustomerTariffChangeRequest 
							SET  
								 ApprovalStatusId = @ApprovalStatusId   
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE AccountNo = @AccountNo  
							AND TariffChangeRequestId = @TariffChangeRequestId  

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET       
								 TariffClassID = @NewClassID  
								,ClusterCategoryId = @NewClusterTypeId 
								,ModifedBy = @CreatedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
								 TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId  
								,ChangeRequestedTariffId 
								,OldClusterCategoryId
								,NewClusterCategoryId 
								,CreatedBy  
								,CreatedDate  
								,Remarks  
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT   
								 @TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId
								,ChangeRequestedTariffId  
								,OldClusterCategoryId
								,NewClusterCategoryId
								,@CreatedBy  
								,dbo.fn_GetCurrentDateTime()  
								,Remarks 
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole
							FROM Tbl_LCustomerTariffChangeRequest
							WHERE AccountNo = @AccountNo   
							AND TariffChangeRequestId = @TariffChangeRequestId 
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
			ELSE  
				BEGIN  -- Request Not Approved
					IF(@ApprovalStatusId = 3) -- Request is Rejected
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
								BEGIN -- Approval levels are there and status is rejected
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
															RoleId = (SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
																		WHERE TariffChangeRequestId = @TariffChangeRequestId))

									SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
									FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
								END
						END
					ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
								SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
								WHERE TariffChangeRequestId = @TariffChangeRequestId
						END

					-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
					UPDATE Tbl_LCustomerTariffChangeRequest SET 
						 ApprovalStatusId = @ApprovalStatusId   
						,ModifiedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId
						,Remarks = @Remarks
					WHERE AccountNo = @AccountNo   
					AND TariffChangeRequestId = @TariffChangeRequestId  

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
		END  
	ELSE  -- Approval Permission Not Required
		BEGIN  

			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
				 AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId
				,OldClusterCategoryId
				,NewClusterCategoryId
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId)  
			VALUES(  
				 @AccountNo  
				,@OldClassID  
				,@NewClassID
				,@OldClusterTypeId
				,@NewClusterTypeId  
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId) 

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				 TariffClassID = @NewClassID
				,ClusterCategoryId = @NewClusterTypeId  
				,ModifedBy = @CreatedBy  
				,ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
			
			INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				 TariffChangeRequestId
				,AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId 
				,OldClusterCategoryId
				,NewClusterCategoryId 
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @TariffChangeRequestId
				,@AccountNo  
				,@OldClassID  
				,@NewClassID  
				,@OldClusterTypeId
				,@NewClusterTypeId
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId
				,@PresentRoleId
				,@NextRoleId)
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('TariffManagementBE'),TYPE  
		END  
		
END  
-----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 01-04-2015
-- Description: The purpose of this procedure is to get list of Tariff Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetTariffChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 3 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.TariffChangeRequestId
		 ,T.AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
				 ,CD.Service_Landmark    
				 ,CD.Service_City,'',    
				 CD.Service_ZipCode) As ServiceAddress
		 ,PreviousTariffId AS OldClassID
		 ,ChangeRequestedTariffId AS NewClassID
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=PreviousTariffId) AS PreviousTariffName
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=ChangeRequestedTariffId) AS ChangeRequestedTariffName
		 ,T.OldClusterCategoryId AS OldClusterTypeId
		 ,T.NewClusterCategoryId AS NewClusterTypeId
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.OldClusterCategoryId) AS PreviousClusterName
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.NewClusterCategoryId) AS ChangeRequestedClusterName
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_LCustomerTariffChangeRequest T 
	INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApprovalStatusId IN(1,4)
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTariffDetails_New]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
  
	DECLARE  
		 @AccountNo VARCHAR(50)   
		,@CreatedBy VARCHAR(50)  
		,@Remarks VARCHAR(50)  
		,@Flag INT  
		,@ApprovalStatusId INT  
		,@OldClassID INT  
		,@NewClassID INT  
		,@TariffChangeRequestId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@OldClusterTypeId INT  
		,@NewClusterTypeId INT  

	SELECT   
		 @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@Flag = C.value('(Flag)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@OldClassID = C.value('(OldClassID)[1]','INT')  
		,@NewClassID = C.value('(NewClassID)[1]','INT')  
		,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@OldClusterTypeId = C.value('(OldClusterTypeId)[1]','INT')  
		,@NewClusterTypeId = C.value('(ClusterTypeId)[1]','INT') 
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
	IF(@Flag = 1)  -- Approval Permission Required
	BEGIN  
		IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo = @AccountNo AND ApprovalStatusId = 1)  
			BEGIN  -- Pending Request already exists
				SELECT 1 AS ApprovalProcess  
			END  
		ELSE  -- New Request
			BEGIN  
				DECLARE @RoleId INT,@IsFinalApproval BIT=0
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
				SET @CurrentLevel = 0
				
				--SELECT 
				--	 @CurrentLevel = [Level] 
				--	,@IsFinalApproval = (CASE IsFinalApproval
				--						 WHEN 1 THEN (CASE WHEN UserIds IS NOT NULL 
				--											THEN (CASE WHEN EXISTS(SELECT 0 FROM dbo.fn_Split(UserIds,',') WHERE @CreatedBy = com) 
				--														THEN 1 END)
				--						 ELSE 1 END)
				--						 END)
				--FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId 
				--	AND	RoleId = @RoleId

				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

				INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					 AccountNo  
					,PreviousTariffId  
					,ChangeRequestedTariffId 
					,OldClusterCategoryId
					,NewClusterCategoryId 
					,CreatedBy  
					,CreatedDate  
					,Remarks  
					,ApprovalStatusId
					,PresentApprovalRole
					,NextApprovalRole)  
				VALUES(  
					 @AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@OldClusterTypeId
					,@NewClusterTypeId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,1 -- Process
					,@PresentRoleId
					,@NextRoleId)  

				--IF(@IsFinalApproval = 1)
				--	BEGIN
				--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
				--		SET       
				--			 TariffClassID = @NewClassID
				--			,ClusterCategoryId = @NewClusterTypeId  
				--			,ModifedBy = @CreatedBy  
				--			,ModifiedDate = dbo.fn_GetCurrentDateTime()  
				--		WHERE GlobalAccountNumber = @AccountNo 
						
				--		INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				--			 AccountNo  
				--			,PreviousTariffId  
				--			,ChangeRequestedTariffId 
				--			,OldClusterCategoryId
				--			,NewClusterCategoryId 
				--			,CreatedBy  
				--			,CreatedDate  
				--			,Remarks  
				--			,ApprovalStatusId
				--			,PresentApprovalRole
				--			,NextApprovalRole)  
				--		VALUES(  
				--			 @AccountNo  
				--			,@OldClassID  
				--			,@NewClassID  
				--			,@OldClusterTypeId
				--			,@NewClusterTypeId
				--			,@CreatedBy  
				--			,dbo.fn_GetCurrentDateTime()  
				--			,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				--			,2 --Approved
				--			,@PresentRoleId
				--			,NULL)
				--	END

				SELECT 1 As IsSuccess,@IsFinalApproval AS IsFinalApproval  
				FOR XML PATH('TariffManagementBE'),TYPE		
			END  
		END  
	ELSE IF(@Flag = 2)  -- Tariff Approval Page
		BEGIN  
			IF(@ApprovalStatusId = 2)  -- Request Approved
				BEGIN  
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
						BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
											RoleId = (SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
													WHERE TariffChangeRequestId = @TariffChangeRequestId))

							SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

							IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApprovalStatusId = @ApprovalStatusId
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId  

									UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
									SET       
										 TariffClassID = @NewClassID
										,ClusterCategoryId = @NewClusterTypeId  
										,ModifedBy = @ModifiedBy  
										,ModifiedDate = dbo.fn_GetCurrentDateTime()  
									WHERE GlobalAccountNumber = @AccountNo  
									
									INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
										 TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId  
										,ChangeRequestedTariffId 
										,OldClusterCategoryId
										,NewClusterCategoryId 
										,CreatedBy  
										,CreatedDate  
										,Remarks  
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole)  
									SELECT   
										 @TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId
										,ChangeRequestedTariffId  
										,OldClusterCategoryId
										,NewClusterCategoryId
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,Remarks 
										,ApprovalStatusId
										,@PresentRoleId
										,@NextRoleId
									FROM Tbl_LCustomerTariffChangeRequest
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId 
								END
							ELSE -- Not a final approval
								BEGIN
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId
										,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
									WHERE AccountNo = @AccountNo 
									AND TariffChangeRequestId = @TariffChangeRequestId
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							UPDATE Tbl_LCustomerTariffChangeRequest 
							SET  
								 ApprovalStatusId = @ApprovalStatusId   
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE AccountNo = @AccountNo  
							AND TariffChangeRequestId = @TariffChangeRequestId  

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET       
								 TariffClassID = @NewClassID  
								,ClusterCategoryId = @NewClusterTypeId 
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
								 TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId  
								,ChangeRequestedTariffId 
								,OldClusterCategoryId
								,NewClusterCategoryId 
								,CreatedBy  
								,CreatedDate  
								,Remarks  
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT   
								 @TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId
								,ChangeRequestedTariffId  
								,OldClusterCategoryId
								,NewClusterCategoryId
								,@ModifiedBy  
								,dbo.fn_GetCurrentDateTime()  
								,Remarks 
								,ApprovalStatusId
								,@PresentRoleId
								,@NextRoleId
							FROM Tbl_LCustomerTariffChangeRequest
							WHERE AccountNo = @AccountNo   
							AND TariffChangeRequestId = @TariffChangeRequestId 
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
			ELSE  
				BEGIN  -- Request Not Approved
					IF(@ApprovalStatusId = 3) -- Request is Rejected
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
								BEGIN -- Approval levels are there and status is rejected
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
															RoleId = (SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
																		WHERE TariffChangeRequestId = @TariffChangeRequestId))

									SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
									FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
								END
						END
					ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
								SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
								WHERE TariffChangeRequestId = @TariffChangeRequestId
						END

					-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
					UPDATE Tbl_LCustomerTariffChangeRequest SET 
						 ApprovalStatusId = @ApprovalStatusId   
						,ModifiedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId
						,Remarks = @Remarks
					WHERE AccountNo = @AccountNo   
					AND TariffChangeRequestId = @TariffChangeRequestId  

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
		END  
	ELSE  -- Approval Permission Not Required
		BEGIN  

			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
				 AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId
				,OldClusterCategoryId
				,NewClusterCategoryId
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId)  
			VALUES(  
				 @AccountNo  
				,@OldClassID  
				,@NewClassID
				,@OldClusterTypeId
				,@NewClusterTypeId  
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId) 

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				 TariffClassID = @NewClassID
				,ClusterCategoryId = @NewClusterTypeId  
				,ModifedBy = @CreatedBy  
				,ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
			
			INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				 TariffChangeRequestId
				,AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId 
				,OldClusterCategoryId
				,NewClusterCategoryId 
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @TariffChangeRequestId
				,@AccountNo  
				,@OldClassID  
				,@NewClassID  
				,@OldClusterTypeId
				,@NewClusterTypeId
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId
				,@PresentRoleId
				,@NextRoleId)
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('TariffManagementBE'),TYPE  
		END  
		
END  
-----------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 29-09-2014      
-- Modified By: T.Karthik  
-- Modified Date: 27-12-2014  
-- Modified By: M.Padmini  
-- Modified Date: 21-01-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Bhimaraju.V  
-- Modified Date: 04-02-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Karteek
-- Modified Date: 02-04-2015  
-- Description: The purpose i have changed this to inserting changes in Tbl_CustomerNameChangeLogs only
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerName]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @AccountNo VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50)  
		,@NewTitle VARCHAR(10)  
		,@NewFirstName VARCHAR(50)  
		,@NewMiddleName VARCHAR(50)  
		,@NewLastName VARCHAR(50)  
		,@NewKnownAs VARCHAR(150)  
		,@ModifiedBy VARCHAR(50)  
		,@Details VARCHAR(MAX)  
		,@ApprovalStatusId INT = 1
    
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@GlobalAccountNumber = C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@NewTitle = C.value('(Title)[1]','VARCHAR(10)')  
		,@NewFirstName = C.value('(FirstName)[1]','VARCHAR(50)')  
		,@NewMiddleName = C.value('(MiddleName)[1]','VARCHAR(50)')  
		,@NewLastName = C.value('(LastName)[1]','VARCHAR(50)')  
		,@NewKnownAs = C.value('(KnownAs)[1]','VARCHAR(150)')  
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
		--,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
	IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApproveStatusId IN (1,4)) = 0)  
		BEGIN  		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT, @FunctionId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level
			SET @FunctionId = 8 -- For Custumer Name Change
			
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
		
			INSERT INTO Tbl_CustomerNameChangeLogs(  
				 GlobalAccountNumber  
				,AccountNo  
				,OldTitle  
				,OldFirstName  
				,OldMiddleName  
				,OldLastName  
				,OldKnownas  
				,NewTitle  
				,NewFirstName  
				,NewMiddleName  
				,NewLastName  
				,NewKnownas  
				,CreatedBy  
				,CreatedDate  
				,ApproveStatusId  
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole) 
			SELECT @GlobalAccountNumber  
				,AccountNo  
				,CASE Title WHEN '' THEN NULL ELSE Title END   -- title was missing --Faiz-ID103 
				,CASE FirstName WHEN '' THEN NULL ELSE FirstName END   
				,CASE MiddleName WHEN '' THEN NULL ELSE MiddleName END  
				,CASE LastName WHEN '' THEN NULL ELSE LastName END    
				,CASE Knownas WHEN '' THEN NULL ELSE Knownas END    
				,CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
				,CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
				,CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END   
				,CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END     
				,CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()  
				,@ApprovalStatusId  
				,@Details  
				,@PresentRoleId
				,@NextRoleId
			FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @GlobalAccountNumber  
  
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
    
END  
  
---------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju.Vanka  
-- Create date: 02-06-2014  
-- Description: Toget the list of Estimated,Read Customers  
-- Modified By: Neeraj Kanojiya
-- Modified date: 31-11-2014  
-- Modified By: Neeraj Kanojiya
-- Modified date: 4-11-2014  
-- Modified By : Padmini
-- Modified Date : 26-12-2014  
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetReportOnEstimatedCustomers_New]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
  
	DECLARE    
		 @PageSize INT  
		,@PageNo INT  
		,@BU_ID VARCHAR(MAX)  
		,@SU_ID VARCHAR(MAX)  
		,@SC_ID VARCHAR(MAX)    
		,@CycleId VARCHAR(MAX)
		,@Year INT  
		,@Month INT  
		,@ReadCodeId VARCHAR(MAX)  
  
	SELECT @PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@Year = C.value('(Year)[1]','INT')  
		,@Month = C.value('(Month)[1]','INT')  
		,@ReadCodeId = C.value('(ReadCode)[1]','VARCHAR(MAX)')  
	FROM @XmlDoc.nodes('ConsumerBe') as T(C)  
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS  
	( 
		SELECT  
			 ROW_NUMBER() OVER(ORDER BY CB1.BillNo DESC ) AS RowNumber  
			,CB1.AccountNo
			,CB1.TotalBillAmountWithArrears
			,(Case WHEN CD.BookDetails IS NULL OR CD.BookDetails = ''  THEN CD.BookCode ELSE CD.BookDetails + '-' + CD.BookCode END) AS BookCode
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,CB1.ServiceAddress 
			,RCD.ReadCode AS ReadCode
			,DATENAME(MONTH,CB1.CreatedDate) + ' ' + DATENAME(YY,CB1.CreatedDate) AS MonthYear
			,CB1.TotalBillAmountWithArrears AS Amount
			,BS.BillStatus
			,dbo.fn_GetRegEstimationCount(CB1.AccountNo,@Month,@Year) AS ContinusEstimations
			,(SELECT dbo.fn_GetMonthName(CB1.BillMonth) + '-' + CONVERT(VARCHAR(50),CB1.BillYear)) AS MonthYear1  
			,CB1.EstimatedUsage AS Usage1
			,CB1.NetEnergyCharges AS Amount1
			,TC1.ClassName AS ClassName1
			,(SELECT dbo.fn_GetMonthName(CB2.BillMonth) + '-' + CONVERT(VARCHAR(50),CB2.BillYear)) AS MonthYear2
			,CB2.EstimatedUsage AS Usage2
			,CB2.NetEnergyCharges AS Amount2
			,CASE WHEN CB2.EstimatedUsage IS NULL THEN '' ELSE TC2.ClassName END AS ClassName2
			,(SELECT dbo.fn_GetMonthName(CB3.BillMonth) + '-' + CONVERT(VARCHAR(50),CB3.BillYear)) AS MonthYear3 
			,CB3.EstimatedUsage AS Usage3
			,CB3.NetEnergyCharges AS Amount3
			,CASE WHEN CB3.EstimatedUsage IS NULL THEN '' ELSE TC3.ClassName END AS ClassName3
			,COUNT(0) OVER() AS TotalRecords
			,CD.SortOrder AS CustomerSortOrder
			,CD.BusinessUnitName
			,CD.ServiceCenterName
			,CD.CycleName  
			,CD.BookCode AS BookNo
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.BookSortOrder
		FROM Tbl_CustomerBills CB1
		INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CB1.AccountNo AND CB1.BillMonth = @Month AND CB1.BillYear = @Year
					AND CD.MeterNumber IS NOT NULL
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		LEFT JOIN Tbl_CustomerBills CB2 ON CB1.AccountNo = CB2.AccountNo AND CB2.BillMonth = DATEPART(MONTH, DATEADD(MONTH,-1,CONVERT(DATE, convert(VARCHAR(4),@Year) + '-' + convert(VARCHAR(2),@Month) + '-01')))
		LEFT JOIN Tbl_CustomerBills CB3 ON CB2.AccountNo = CB3.AccountNo AND CB3.BillMonth = DATEPART(MONTH, DATEADD(MONTH,-2,CONVERT(DATE, convert(VARCHAR(4),@Year) + '-' + convert(VARCHAR(2),@Month) + '-01')))
		LEFT JOIN Tbl_MTariffClasses TC1 ON CB1.TariffId = TC1.ClassID
		LEFT JOIN Tbl_MTariffClasses TC2 ON CB2.TariffId = TC2.ClassID
		LEFT JOIN Tbl_MTariffClasses TC3 ON CB3.TariffId = TC3.ClassID
		LEFT JOIN Tbl_MReadCodes AS RCD ON CB1.ReadCodeId=RCD.ReadCodeId
		LEFT JOIN Tbl_MBillStatus AS BS ON CB1.PaymentStatusID=bs.BillStatusId
		WHERE (CB1.ReadCodeId IN (3,5) OR 
		(CB1.ReadCodeId IN (3,5) AND CB2.ReadCodeId IN (3,5)) OR
		(CB1.ReadCodeId IN (3,5) AND CB2.ReadCodeId IN (3,5) AND CB3.ReadCodeId IN (3,5)))
	)
	   
	SELECT   
	(  
		SELECT    
			 RowNumber  
			,AccountNo
			,BusinessUnitName
			,BookNo
			,TotalBillAmountWithArrears
			,BookCode
			,Name
			,CycleName  
			,ServiceAddress 
			,ReadCode
			,MonthYear
			,Amount
			,BillStatus
			,ContinusEstimations
			,MonthYear1  
			,Usage1
			,Amount1
			,ClassName1
			,MonthYear2
			,Usage2
			,Amount2
			,ClassName2
			,MonthYear3 
			,Usage3
			,Amount3
			,ClassName3
			,TotalRecords 
			,CustomerSortOrder
			,BusinessUnitName
			,ServiceCenterName
			,CycleName  
			,BookNo
			,BookNumber
			,BookSortOrder 
		FROM PagedResults p 
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		FOR XML PATH('Consumer'),TYPE  
	)  
	FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
	  
END
---------------------------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 06-June-2014  
-- Description: Get The Details of Individual Bill Balance Of The Customers  
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetAccountsStatusOfBill_New]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  

	DECLARE @PageSize INT  
		,@PageNo INT  
		,@BU_ID VARCHAR(MAX)  
		,@SU_ID VARCHAR(MAX)  
		,@SC_ID VARCHAR(MAX)    
		,@CycleId VARCHAR(MAX)  
		,@BookNo VARCHAR(MAX)  
		,@YearId VARCHAR(MAX)  
		,@MonthId VARCHAR(MAX)  
     
	SELECT @PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@YearId=C.value('(YearName)[1]','VARCHAR(MAX)')  
		,@MonthId=C.value('(MonthName)[1]','VARCHAR(MAX)')  
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)  
   
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
   
	;WITH PagedResults AS  
	(  
		SELECT  
			 ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber  
			,CD.AccountNo AS AccountNo
			,CD.GlobalAccountNumber AS GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,ISNULL(CONVERT(VARCHAR(50),BillGeneratedDate,106),'--') AS BillDate  
			,ISNULL(TotalBillAmountWithTax,0) AS BillAmount
			,ISNULL(CONVERT(VARCHAR(50),(SELECT dbo.fn_GetCustomerLastPaymentDate(CB.AccountNo)) ,106),'--') AS LastPaidDate  
			,ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0) AS TotalPaidAmount  
			,(CB.TotalBillAmountWithTax - ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0)) AS DueAmount
			,BU.BusinessUnitName AS BusinessUnitName
			,SC.ServiceCenterName AS ServiceCenterName
			,SU.ServiceUnitName AS ServiceUnitName
			,C.CycleName
			,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
			,PD.SortOrder AS CustomerSortOrder
			,BN.SortOrder AS BookSortOrder
			,BN.BookCode
			,CD.OldAccountNo AS OldAccountNo
			,COUNT(0) OVER() AS TotalRecords
		FROM Tbl_CustomerBills CB
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = CB.AccountNo
				AND (CB.TotalBillAmountWithTax - ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0)) > 0
				AND BillMonth IN (SELECT [com] FROM dbo.fn_Split(@MonthId,','))
				AND BillYear IN (SELECT [com] FROM dbo.fn_Split(@YearId,','))
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
					AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
		
		--INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CB.AccountNo 
		--		AND (CB.TotalBillAmountWithTax - ISNULL((SELECT dbo.fn_GetTotalPaidAmountOfBill(CB.BillNo)),0)) > 0
		--		AND BillMonth IN (SELECT [com] FROM dbo.fn_Split(@MonthId,','))
		--		AND BillYear IN (SELECT [com] FROM dbo.fn_Split(@YearId,','))
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
	)  
   
	--SELECT   
	--(  
		SELECT    
			 RowNumber  
			,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,BillDate  
			,BillAmount
			,LastPaidDate  
			,TotalPaidAmount  
			,DueAmount
			,BusinessUnitName
			,ServiceCenterName
			,ServiceUnitName
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BookCode
			,OldAccountNo
			,(SELECT SUM(DueAmount) FROM PagedResults) AS TotalDueAmount  
			,TotalRecords
		FROM PagedResults p  
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize  
	--	FOR XML PATH('Reports'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')  
	
END  
--------------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 05-June-2014
-- Description:	Get The Details of Debit Balence Of The Customers
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithDebitBal_New]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)			
			,@BookNo VARCHAR(MAX)
			,@MinAmt VARCHAR(50)
			,@MaxAmt VARCHAR(50)
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@MinAmt=C.value('(MinAmt)[1]','VARCHAR(50)')
			,@MaxAmt=C.value('(MaxAmt)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	IF(@MinAmt = '')
		SET @MinAmt = (SELECT MIN(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails)
	IF(@MaxAmt = '')
		SET @MaxAmt = (SELECT MAX(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
			
	;WITH PagedResults AS
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
				,A.AccountNo AS AccountNo
				,CD.GlobalAccountNumber AS GlobalAccountNumber
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
				,OutStandingAmount AS TotalDueAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(A.AccountNo)),0) AS LastPaidAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(A.AccountNo)),'--') AS LastPaidDate
				,CD.OldAccountNo
				,C.CycleName
				,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
				,PD.SortOrder AS CustomerSortOrder
				,BN.SortOrder AS BookSortOrder
				,BU.BusinessUnitName AS BusinessUnitName
				,SU.ServiceUnitName AS ServiceUnitName
				,SC.ServiceCenterName AS ServiceCenterName
				,COUNT(0) OVER() AS TotalRecords
		 FROM Tbl_CustomerBills A
		 INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = A.AccountNo
		 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
				AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		 INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
				AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		 INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
				AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		 INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
				AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		 INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
				AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		 INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID
		  
		  --INNER JOIN [UDV_CustomerDescription] CD ON A.AccountNo = CD.GlobalAccountNumber AND OutStandingAmount >0
			--	AND OutStandingAmount BETWEEN (CASE WHEN @MaxAmt='' AND @MinAmt='' THEN (OutStandingAmount) ELSE @MinAmt END) 
			--						AND (CASE @MaxAmt WHEN '' THEN (OutStandingAmount) ELSE @MaxAmt END)
		 --INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		 --INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		 --INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		 --INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		 --INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
	)
	
	--SELECT 
	--(
		SELECT	 
			 RowNumber
			,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,TotalDueAmount
			,LastPaidAmount
			,LastPaidDate
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(SELECT SUM(TotalDueAmount) FROM PagedResults ) AS DueAmount
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	--	FOR XML PATH('Reports'),TYPE
	--)
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')

END
--------------------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 23-Aug-2014
-- Description:	Get The Details of Credit Balance Of The Customers
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithCreditBal_New]
(
	@XmlDoc xml
)
AS
BEGIN

		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(		
		SELECT ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber				
			,CD.GlobalAccountNumber AS GlobalAccountNumber
			,CD.AccountNo AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
			,(SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)) AS LastPaidAmount		
			,ISNULL(CONVERT(VARCHAR(50),(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)) ,106),'--') AS LastPaidDate
			,CAD.OutStandingAmount As OverPayAmount
			,(SELECT dbo.fn_GetCustomerTotalBillsAmount(CD.GlobalAccountNumber)) As TotalBillsAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalPaidAmount(CD.GlobalAccountNumber)),0) As TotalPaidAmount
			,BN.BookCode AS BookCode
			,CD.OldAccountNo
			,C.CycleName
			,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
			,PD.SortOrder AS CustomerSortOrder
			,BN.SortOrder AS BookSortOrder
			,BU.BusinessUnitName
			,SU.ServiceUnitName
			,SC.ServiceCenterName
			,COUNT(0) OVER() AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomersDetail CD 
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			AND CAD.OutStandingAmount < 0
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
			AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
			AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
			AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
		--[UDV_CustomerDescription] CD 
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.OutStandingAmount < 0
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo
	)
	
	--SELECT 
	--(
		SELECT
			 RowNumber
			,GlobalAccountNumber
			,AccountNo
			,Name
			,ServiceAddress
			,LastPaidAmount		
			,LastPaidDate
			,OverPayAmount
			,TotalBillsAmount
			,TotalPaidAmount
			,BookCode
			,OldAccountNo
			,CycleName
			,BookNumber 
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(SELECT SUM(OverPayAmount) FROM PagedResults) AS TotalCreditBalance				
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	--	FOR XML PATH('Reports'),TYPE
	--)
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')

END
------------------------------------------------------------------------------

