GO

/****** Object:  View [dbo].[UDV_BookNumberDetails]    Script Date: 04/02/2015 05:02:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[UDV_BookNumberDetails]
AS
	SELECT 
	BU.BU_ID,BU.BUCode,BU.BusinessUnitName
	,SU.SU_ID,SU.SUCode,SU.ServiceUnitName
	,SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName
	,C.CycleId,C.CycleCode,C.CycleName
	,B.BookNo,B.BookCode,B.ID
	FROM Tbl_BookNumbers B
	JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
GO
GO

/****** Object:  View [dbo].[UDV_CustomerDetails]    Script Date: 04/02/2015 04:51:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[UDV_CustomerMeterInformation] 
AS
	SELECT
	  CD.GlobalAccountNumber 
	 ,CD.AccountNo
	 ,CD.OldAccountNo
	 ,CD.Title
	 ,CD.FirstName
	 ,CD.MiddleName
	 ,CD.LastName
	 ,CD.KnownAs
	 ,CD.ActiveStatusId
	 ,PD.TariffClassID AS TariffId
	 ,TC.ClassName
	 ,PD.ReadCodeID
	 ,PD.SortOrder
	 ,AD.Highestconsumption
	 ,BN.BookNo
	 ,BN.BookCode
	 ,BN.CycleId
	 ,BN.CycleCode
	 ,BN.CycleName
	 ,BN.SCCode
	 ,BN.ServiceCenterId
	 ,BN.ServiceCenterName
	 ,BN.SUCode
	 ,BN.ServiceUnitName
	 ,BN.SU_ID
	 ,BN.BUCode
	 ,BN.BU_ID
	 ,BN.BusinessUnitName
	 ,MS.[StatusName] AS ActiveStatus
	 ,CD.EmailId
	 ,PD.MeterNumber 
	 ,MI.MeterType AS MeterTypeId
	 ,PD.PhaseId
	 ,AD.InitialReading
	 ,AD.AvgReading
	 ,PD.RouteSequenceNumber AS RouteSequenceNo
	 ,PD.CustomerTypeId
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD 
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber 
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo 
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId	
INNER JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading]    Script Date: 04/02/2015 04:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerMeterInformation CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerMeterInformation CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum 
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				  C.GlobalAccountNumber AS AccNum  
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @present END AS PresentReading
				  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
				  --,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerMeterInformation C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
GO
/****** Object:  View [dbo].[UDV_CustomerMeterInformation]    Script Date: 04/03/2015 11:18:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[UDV_CustomerMeterInformation] 
AS
	SELECT
	  CD.GlobalAccountNumber 
	 ,CD.AccountNo
	 ,CD.OldAccountNo
	 ,CD.Title
	 ,CD.FirstName
	 ,CD.MiddleName
	 ,CD.LastName
	 ,CD.KnownAs
	 ,CD.ActiveStatusId
	 ,PD.TariffClassID AS TariffId
	 ,TC.ClassName
	 ,PD.ReadCodeID
	 ,PD.SortOrder
	 ,AD.Highestconsumption
	 ,BN.BookNo
	 ,BN.BookCode
	 ,BN.CycleId
	 ,BN.CycleCode
	 ,BN.CycleName
	 ,BN.SCCode
	 ,BN.ServiceCenterId
	 ,BN.ServiceCenterName
	 ,BN.SUCode
	 ,BN.ServiceUnitName
	 ,BN.SU_ID
	 ,BN.BUCode
	 ,BN.BU_ID
	 ,BN.BusinessUnitName
	 ,MS.[StatusName] AS ActiveStatus
	 ,CD.EmailId
	 ,PD.MeterNumber 
	 ,MI.MeterType AS MeterTypeId
	 ,PD.PhaseId
	 ,AD.InitialReading 
	,AD.PresentReading 
	 ,AD.AvgReading
	 ,PD.RouteSequenceNumber AS RouteSequenceNo
	 ,PD.CustomerTypeId
	,CD.CreatedDate
	,CD.CreatedBy
	,CD.ModifiedDate
	,CD.ModifedBy 
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD 
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber 
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo 
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId	
INNER JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustPrevReadingByReadDate_New]    Script Date: 04/03/2015 14:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-03-2015
-- Description:	The purpose of this function is to get customer
--				 Previous Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustPrevReadingByReadDate_New]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
	,@MeterNumber VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousReading VARCHAR(50)=''

	SET @PreviousReading=(SELECT CASE WHEN (SELECT TOP(1) PreviousReading 
							FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber=@GlobalAccountNumber 
							AND CR.MeterNumber=@MeterNumber
							AND CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  
						 order by CustomerReadingId desc)  
					   IS NULL  
					   THEN  CONVERT(VARCHAR(50),ISNULL((SELECT InitialReading FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber=@GlobalAccountNumber),0))  
					   ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   CR.MeterNumber=@MeterNumber AND
					  CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END 
					  )
	RETURN @PreviousReading	

END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 04/03/2015 15:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerMeterInformation CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerMeterInformation  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CD.InitialReading,'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   

GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading]    Script Date: 04/03/2015 17:34:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerMeterInformation CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerMeterInformation CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum AND MeterNumber=@MeterNo
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum  AND MeterNumber=@MeterNo
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				   C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo  AS AccountNo
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   Cr.MeterNumber=@MeterNo AND
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND  Cr.MeterNumber=@MeterNo AND
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.PresentReading,0)) ELSE @present END AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerMeterInformation C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  AND C.MeterTypeId=2
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  
GO

GO
/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 04/03/2015 19:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 08-JAN-2014
-- Description:	The purpose of this procedure is to get list of MeterInformation
-- =============================================
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @MeterNo VARCHAR(100)
			,@MeterSerialNo VARCHAR(100)
			,@BUID VARCHAR(50)
			,@CustomerTypeId INT
			
		SELECT 	 
			@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
			,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')
			,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)	
		
		IF (@CustomerTypeId=3)
			BEGIN
				SELECT
				(
					SELECT
							MeterId
							,MeterNo
							,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType
							,MeterSize
							,MeterMultiplier
							,MeterBrand
							,MeterSerialNo
						FROM Tbl_MeterInformation AS M
						WHERE M.ActiveStatusId IN(1,2)
						AND (BU_ID=@BUID OR @BUID='')
						AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails 
							WHERE MeterNumber !='' )
							AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)
						AND M.MeterType=1
						AND M.MeterNo LIKE '%'+@MeterNo+'%' 
						AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'
					FOR XML PATH('MastersBE'),TYPE
				)
				FOR XML PATH(''),ROOT('MastersBEInfoByXml')
			END
		ELSE
			BEGIN
				SELECT
				(
					SELECT
							MeterId
							,MeterNo
							,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType
							,MeterSize
							,MeterMultiplier
							,MeterBrand
							,MeterSerialNo
						FROM Tbl_MeterInformation AS M
						WHERE M.ActiveStatusId IN(1,2)
						AND (BU_ID=@BUID OR @BUID='')
						AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails 
							WHERE MeterNumber !='' )
							AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)
						AND M.MeterType !=1
						AND M.MeterNo LIKE '%'+@MeterNo+'%' 
						AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'
					FOR XML PATH('MastersBE'),TYPE
				)
				FOR XML PATH(''),ROOT('MastersBEInfoByXml')
			END
			
END

