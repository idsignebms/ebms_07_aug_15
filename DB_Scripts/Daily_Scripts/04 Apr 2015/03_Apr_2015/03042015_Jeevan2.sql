/****** Object:  StoredProcedure [dbo].[USP_UpdateBillPreviousReading]    Script Date: 04/03/2015 12:48:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <4-APR-2014>
-- Description:	<Update BillReading details on customerreading table>
-- Modified By : Padmini
-- Modified Date : 26-12-2014 
-- Modified By -- Jeevan Amunuri
-- Modified Date -- 03-04-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBillPreviousReading](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		@ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@Count=C.value('(TotalReadings)[1]','INT')
		,@Avg=C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage=CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')

FROM @XmlDoc.nodes('BillingBE') AS T(C)

     SET @CustUn=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber=@AccNum)
	
    UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
									,AverageReading = (SELECT dbo.fn_GetCustomerAverageReading(GlobalAccountNumber,CONVERT(NUMERIC,@Current),@ReadDate)) --CONVERT(VARCHAR(50),(CONVERT(NUMERIC(20,4),(CONVERT(DECIMAL(18,2),@Avg)*@Count)+@Usage))/[TotalReadings])
									,TotalReadingEnergies = ISNULL((SELECT SUM(Usage)FROM Tbl_CustomerReadings C WHERE GlobalAccountNumber = @CustUn AND C.CustomerReadingId < CR.CustomerReadingId),0)+CONVERT(NUMERIC(20,4),@Current) - CONVERT(NUMERIC(20,4), CR.PreviousReading)--CONVERT(NUMERIC(20,4),(CONVERT(DECIMAL(18,2),@Avg)*@Count)+@Usage)
									,Usage =  CONVERT(NUMERIC(20,4),@Current) - CONVERT(NUMERIC(20,4), CR.PreviousReading)
									,ModifiedBy = @Modified
									,IsTamper=@IsTamper
									,MeterReadingFrom=@MeterReadingFrom
									,ModifiedDate = GETDATE()
								FROM Tbl_CustomerReadings CR	
									 WHERE GlobalAccountNumber = @CustUn 
									 AND CONVERT(VARCHAR(10),ReadDate,121)=CONVERT(VARCHAR(10),@ReadDate,121)
									 AND CustomerReadingId IN(SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum ORDER BY CustomerReadingId DESC)  
									
									UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET PresentReading = @Current 
									,AvgReading=CONVERT(NUMERIC,(SELECT dbo.fn_GetCustomerAverageReading(GlobalAccountNumber,CONVERT(NUMERIC,@Current),@ReadDate)))
									WHERE GlobalAccountNumber=@AccNum 
SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading]    Script Date: 04/03/2015 12:56:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- Modified By -- Bhimaraju Vanka
-- Modified Date -- 27-03-2015
-- Reason : Added/Inserting one more field MeterReading From
-- Modified By -- Jeevan Amunuri
-- Modified Date -- 03-04-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading](@XmlDoc XML)
AS
BEGIN
DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@IsTamper BIT
		,@Multiple numeric(20,4)
		,@MeterReadingFrom INT
SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@ReadBy=C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous=C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage= CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')
		
FROM @XmlDoc.nodes('BillingBE') AS T(C)
     
	SET @Multiple= (SELECT MI.MeterMultiplier FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	JOIN Tbl_MeterInformation MI ON MI.MeterNo=CPD.MeterNumber
	WHERE CPD.GlobalAccountNumber=@AccNum)
	
	DECLARE @MeterNumber VARCHAR(50)
	
	SET @MeterNumber=(SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber=@AccNum)
	
	IF(@IsTamper=0)
      BEGIN
		SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
      END
       
  INSERT INTO Tbl_CustomerReadings (
									 --[CustomerUniqueNo]
									  GlobalAccountNumber
									  ,[ReadDate]
									  ,[ReadBy]
									  ,[PreviousReading]
									  ,[PresentReading]
									  ,[AverageReading]
									  ,[Usage]
									  ,[TotalReadingEnergies]
									  ,[TotalReadings]
									  ,[Multiplier]
									  ,[ReadType]
									  ,[CreatedBy]
									  ,[CreatedDate]
									  ,[IsTamper]
									  ,MeterNumber
									  ,[MeterReadingFrom])
  
									VALUES(
									--@CustUn
									@AccNum
										   ,@ReadDate
										   ,@ReadBy
										   ,CONVERT(VARCHAR(50),@Previous)
										   ,CONVERT(VARCHAR(50),@Current)
										   --,(
										   -- (SELECT case when SUM( PresentReading) is NULL THEN 0 ELSE SUM( PresentReading) END FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Current)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1)
										   ,(SELECT dbo.fn_GetAverageReading(@AccNum,@Usage))
										 --,CONVERT(VARCHAR(50),((SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Usage)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1))
										   ,@Usage
										   ,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+@Usage
										   ,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+1)
										   ,@Multiple
										   ,2
										   ,@CreateBy
										   ,GETDATE()
										   ,@IsTamper
										   ,@MeterNumber
										   ,@MeterReadingFrom)
										  
UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET InitialReading=@Previous, PresentReading=@Current 
,AvgReading=CONVERT(NUMERIC,(SELECT dbo.fn_GetAverageReading(@AccNum,@Usage))) WHERE GlobalAccountNumber= @AccNum

										   
SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
END

GO