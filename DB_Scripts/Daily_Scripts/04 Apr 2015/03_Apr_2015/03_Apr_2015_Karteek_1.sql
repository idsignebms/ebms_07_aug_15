GO
UPDATE Tbl_Menus
SET IsActive = 0
WHERE Name='Customer Tariff Approval'
GO
-------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetPaymets_New]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
		
	IF (@FromDate ='' OR @FromDate IS NULL)    
		BEGIN    
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
		END 
	IF (@Todate ='' OR @Todate IS NULL)    
		BEGIN    
			SET @Todate = dbo.fn_GetCurrentDateTime()    
		END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,CD.GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,100),'--') AS RecievedDate
			,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD 	
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN Tbl_CustomerPayments CP ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
				--AND (DATEDIFF(DAY,@FromDate,CP.RecievedDate) >= 0 OR @FromDate = '')
				--AND (DATEDIFF(DAY,@ToDate,CP.RecievedDate) <= 0 OR @ToDate = '')
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		INNER JOIN Tbl_MBillingTypes BT ON CP.ReceivedDevice = BT.BillingTypeId
	)
	
	--SELECT 
	--	(
			SELECT	 
				 RowNumber
				,GlobalAccountNumber
				,Name
				,ServiceAddress
				,BusinessUnitName
				,ServiceCenterName
				,ServiceUnitName
				,CycleName
				,BookNumber
				,CustomerSortOrder
				,BookSortOrder
				,ReceivedDevice
				,PaidAmount
				,RecievedDate
				,TotalDueAmount
				,(SELECT SUM(TotalDueAmount) FROM PagedResults ) AS TotalDue
				,(Select COUNT(0) from PagedResults) as TotalRecords
				,(SELECT SUM(PaidAmount) FROM PagedResults) AS TotalReceivedAmount
			FROM PagedResults p
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	--		FOR XML PATH('Reports'),TYPE
	--	)
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')
	
END
--------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTariffDetails_New]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
  
	DECLARE  
		 @AccountNo VARCHAR(50)   
		,@CreatedBy VARCHAR(50)  
		,@Remarks VARCHAR(50)  
		,@Flag INT  
		,@ApprovalStatusId INT  
		,@OldClassID INT  
		,@NewClassID INT  
		,@TariffChangeRequestId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@OldClusterTypeId INT  
		,@NewClusterTypeId INT 
		,@IsFinalApproval BIT = 0 

	SELECT   
		 @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@Flag = C.value('(Flag)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@OldClassID = C.value('(OldClassID)[1]','INT')  
		,@NewClassID = C.value('(NewClassID)[1]','INT')  
		,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@OldClusterTypeId = C.value('(OldClusterTypeId)[1]','INT')  
		,@NewClusterTypeId = C.value('(ClusterTypeId)[1]','INT') 
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT') 
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
	IF(@Flag = 1)  -- Approval Permission Required
	BEGIN  
		IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo = @AccountNo AND ApprovalStatusId = 1)  
			BEGIN  -- Pending Request already exists
				SELECT 1 AS ApprovalProcess  
			END  
		ELSE  -- New Request
			BEGIN  
				DECLARE @RoleId INT--,@IsFinalApproval BIT=0
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
				SET @CurrentLevel = 0
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
					BEGIN
						SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
					END
				
				--SELECT 
				--	 @CurrentLevel = [Level] 
				--	,@IsFinalApproval = (CASE IsFinalApproval
				--						 WHEN 1 THEN (CASE WHEN UserIds IS NOT NULL 
				--											THEN (CASE WHEN EXISTS(SELECT 0 FROM dbo.fn_Split(UserIds,',') WHERE @CreatedBy = com) 
				--														THEN 1 END)
				--						 ELSE 1 END)
				--						 END)
				--FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId 
				--	AND	RoleId = @RoleId

				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

				INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					 AccountNo  
					,PreviousTariffId  
					,ChangeRequestedTariffId 
					,OldClusterCategoryId
					,NewClusterCategoryId 
					,CreatedBy  
					,CreatedDate  
					,Remarks  
					,ApprovalStatusId
					,PresentApprovalRole
					,NextApprovalRole)  
				VALUES(  
					 @AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@OldClusterTypeId
					,@NewClusterTypeId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,1 -- Process
					,@PresentRoleId
					,@NextRoleId)
					
				--SET @TariffChangeRequestId = SCOPE_IDENTITY()

				--IF(@IsFinalApproval = 1)
				--	BEGIN
				--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
				--		SET       
				--			 TariffClassID = @NewClassID
				--			,ClusterCategoryId = @NewClusterTypeId  
				--			,ModifedBy = @CreatedBy  
				--			,ModifiedDate = dbo.fn_GetCurrentDateTime()  
				--		WHERE GlobalAccountNumber = @AccountNo 
						
				--		INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				--			 TariffChangeRequestId
				--			,AccountNo  
				--			,PreviousTariffId  
				--			,ChangeRequestedTariffId 
				--			,OldClusterCategoryId
				--			,NewClusterCategoryId 
				--			,CreatedBy  
				--			,CreatedDate  
				--			,Remarks  
				--			,ApprovalStatusId
				--			,PresentApprovalRole
				--			,NextApprovalRole)  
				--		VALUES(  
				--			 @TariffChangeRequestId
				--			,@AccountNo  
				--			,@OldClassID  
				--			,@NewClassID  
				--			,@OldClusterTypeId
				--			,@NewClusterTypeId
				--			,@CreatedBy  
				--			,dbo.fn_GetCurrentDateTime()  
				--			,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				--			,@ApprovalStatusId
				--			,@PresentRoleId
				--			,@NextRoleId)
				--	END

				SELECT 1 As IsSuccess--,@IsFinalApproval AS IsFinalApproval  
				FOR XML PATH('TariffManagementBE'),TYPE		
			END  
		END  
	ELSE IF(@Flag = 2)  -- Tariff Approval Page
		BEGIN  
			IF(@ApprovalStatusId = 2)  -- Request Approved
				BEGIN  
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
						BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
											RoleId = (SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
													WHERE TariffChangeRequestId = @TariffChangeRequestId))

							SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

							IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApprovalStatusId = @ApprovalStatusId
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId  

									UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
									SET       
										 TariffClassID = @NewClassID
										,ClusterCategoryId = @NewClusterTypeId  
										,ModifedBy = @ModifiedBy  
										,ModifiedDate = dbo.fn_GetCurrentDateTime()  
									WHERE GlobalAccountNumber = @AccountNo  
									
									INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
										 TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId  
										,ChangeRequestedTariffId 
										,OldClusterCategoryId
										,NewClusterCategoryId 
										,CreatedBy  
										,CreatedDate  
										,Remarks  
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole)  
									SELECT   
										 @TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId
										,ChangeRequestedTariffId  
										,OldClusterCategoryId
										,NewClusterCategoryId
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,Remarks 
										,ApprovalStatusId
										,@PresentRoleId
										,@NextRoleId
									FROM Tbl_LCustomerTariffChangeRequest
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId 
								END
							ELSE -- Not a final approval
								BEGIN
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId
										,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
									WHERE AccountNo = @AccountNo 
									AND TariffChangeRequestId = @TariffChangeRequestId
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							UPDATE Tbl_LCustomerTariffChangeRequest 
							SET  
								 ApprovalStatusId = @ApprovalStatusId   
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE AccountNo = @AccountNo  
							AND TariffChangeRequestId = @TariffChangeRequestId  

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET       
								 TariffClassID = @NewClassID  
								,ClusterCategoryId = @NewClusterTypeId 
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
								 TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId  
								,ChangeRequestedTariffId 
								,OldClusterCategoryId
								,NewClusterCategoryId 
								,CreatedBy  
								,CreatedDate  
								,Remarks  
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT   
								 @TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId
								,ChangeRequestedTariffId  
								,OldClusterCategoryId
								,NewClusterCategoryId
								,@ModifiedBy  
								,dbo.fn_GetCurrentDateTime()  
								,Remarks 
								,ApprovalStatusId
								,@PresentRoleId
								,@NextRoleId
							FROM Tbl_LCustomerTariffChangeRequest
							WHERE AccountNo = @AccountNo   
							AND TariffChangeRequestId = @TariffChangeRequestId 
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
			ELSE  
				BEGIN  -- Request Not Approved
					IF(@ApprovalStatusId = 3) -- Request is Rejected
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
								BEGIN -- Approval levels are there and status is rejected
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
															RoleId = (SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
																		WHERE TariffChangeRequestId = @TariffChangeRequestId))

									SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
									FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
								END
						END
					ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
								SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
								WHERE TariffChangeRequestId = @TariffChangeRequestId
						END

					-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
					UPDATE Tbl_LCustomerTariffChangeRequest SET 
						 ApprovalStatusId = @ApprovalStatusId   
						,ModifiedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId
						,Remarks = @Remarks
					WHERE AccountNo = @AccountNo   
					AND TariffChangeRequestId = @TariffChangeRequestId  

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
		END  
	ELSE  -- Approval Permission Not Required
		BEGIN  

			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
				 AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId
				,OldClusterCategoryId
				,NewClusterCategoryId
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId)  
			VALUES(  
				 @AccountNo  
				,@OldClassID  
				,@NewClassID
				,@OldClusterTypeId
				,@NewClusterTypeId  
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId) 
				
			SET @TariffChangeRequestId = SCOPE_IDENTITY()

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				 TariffClassID = @NewClassID
				,ClusterCategoryId = @NewClusterTypeId  
				,ModifedBy = @CreatedBy  
				,ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
			
			INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				 TariffChangeRequestId
				,AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId 
				,OldClusterCategoryId
				,NewClusterCategoryId 
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @TariffChangeRequestId
				,@AccountNo  
				,@OldClassID  
				,@NewClassID  
				,@OldClusterTypeId
				,@NewClusterTypeId
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId
				,@PresentRoleId
				,@NextRoleId)
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('TariffManagementBE'),TYPE  
		END  
		
END  
-----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author		: Karteek        
-- Create date  : 3 Apr 2015        
-- Description  : This is to get the Approval Status by Role
-- =============================================              
CREATE PROCEDURE [dbo].[USP_IsFinalApproval]
(              
	@XmlDoc xml              
)              
AS              
BEGIN  
            
	DECLARE @FunctionId INT, @RoleId INT
			,@IsFinalApproval BIT = 0             

	SELECT  
		 @FunctionId = C.value('(FunctionId)[1]','INT')
		,@RoleId = C.value('(RoleId)[1]','INT')        
		FROM @XmlDoc.nodes('ApprovalBe') as T(C)          
		
	IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId and IsActive = 1)
		BEGIN		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @IsFinalApproval = IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
			ELSE
				BEGIN
					SET @IsFinalApproval = 0
				END
		END
	ELSE
		BEGIN
			--IF(@RoleId = 1)
			--	BEGIN
					SET @IsFinalApproval = 1
				--END
		END
		
	SELECT @IsFinalApproval AS IsFinalApproval             
	FOR XML PATH('ApprovalBe'),TYPE      
          
END
------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 29-09-2014      
-- Modified By: T.Karthik  
-- Modified Date: 27-12-2014  
-- Modified By: M.Padmini  
-- Modified Date: 21-01-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Bhimaraju.V  
-- Modified Date: 04-02-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Karteek
-- Modified Date: 02-04-2015  
-- Description: The purpose i have changed this to inserting changes in Tbl_CustomerNameChangeLogs only
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerName]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @AccountNo VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50)  
		,@NewTitle VARCHAR(10)  
		,@NewFirstName VARCHAR(50)  
		,@NewMiddleName VARCHAR(50)  
		,@NewLastName VARCHAR(50)  
		,@NewKnownAs VARCHAR(150)  
		,@ModifiedBy VARCHAR(50)  
		,@Details VARCHAR(MAX)  
		,@ApprovalStatusId INT 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
    
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@GlobalAccountNumber = C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@NewTitle = C.value('(Title)[1]','VARCHAR(10)')  
		,@NewFirstName = C.value('(FirstName)[1]','VARCHAR(50)')  
		,@NewMiddleName = C.value('(MiddleName)[1]','VARCHAR(50)')  
		,@NewLastName = C.value('(LastName)[1]','VARCHAR(50)')  
		,@NewKnownAs = C.value('(KnownAs)[1]','VARCHAR(150)')  
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
	IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApproveStatusId IN (1,4)) = 0)  
		BEGIN  		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
		
			INSERT INTO Tbl_CustomerNameChangeLogs(  
				 GlobalAccountNumber  
				,AccountNo  
				,OldTitle  
				,OldFirstName  
				,OldMiddleName  
				,OldLastName  
				,OldKnownas  
				,NewTitle  
				,NewFirstName  
				,NewMiddleName  
				,NewLastName  
				,NewKnownas  
				,CreatedBy  
				,CreatedDate  
				,ApproveStatusId  
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole) 
			SELECT @GlobalAccountNumber  
				,AccountNo  
				,CASE Title WHEN '' THEN NULL ELSE Title END   -- title was missing --Faiz-ID103 
				,CASE FirstName WHEN '' THEN NULL ELSE FirstName END   
				,CASE MiddleName WHEN '' THEN NULL ELSE MiddleName END  
				,CASE LastName WHEN '' THEN NULL ELSE LastName END    
				,CASE Knownas WHEN '' THEN NULL ELSE Knownas END    
				,CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
				,CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
				,CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END   
				,CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END     
				,CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()  
				,@ApprovalStatusId  
				,@Details  
				,@PresentRoleId
				,@NextRoleId
			FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			SET @NameChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 Title = (CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END) 
						,FirstName = (CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END)  
						,MiddleName = (CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END)  
						,LastName = (CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END)  
						,KnownAs = (CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END)  
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerNameChangeLogs(  
						 NameChangeLogId
						,GlobalAccountNumber  
						,AccountNo  
						,OldTitle  
						,OldFirstName  
						,OldMiddleName  
						,OldLastName  
						,OldKnownas  
						,NewTitle  
						,NewFirstName  
						,NewMiddleName  
						,NewLastName  
						,NewKnownas  
						,CreatedBy  
						,CreatedDate  
						,ApproveStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 NameChangeLogId
						,GlobalAccountNumber  
						,AccountNo  
						,OldTitle  
						,OldFirstName  
						,OldMiddleName  
						,OldLastName  
						,OldKnownas  
						,NewTitle  
						,NewFirstName  
						,NewMiddleName  
						,NewLastName  
						,NewKnownas  
						,CreatedBy  
						,CreatedDate  
						,ApproveStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_CustomerNameChangeLogs WHERE NameChangeLogId = @NameChangeRequestId
					
				END
  
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
    
END  
  
---------------------------------------------------------------------------------------------------------
