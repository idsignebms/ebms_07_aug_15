ALTER TABLE Tbl_CustomerTypeChangeLogs
ADD PresentApprovalRole INT, NextApprovalRole INT
GO
CREATE TABLE Tbl_Audit_CustomerTypeChangeLogs
(
	 CustomerTypeChangeLogId INT
	,GlobalAccountNumber VARCHAR(50)
	,OldCustomerTypeId INT
	,NewCustomerTypeId INT
	,Remarks VARCHAR(200)
	,ApprovalStatusId INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
	,PresentApprovalRole INT
	,NextApprovalRole INT
)
GO
-----------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 8 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.NameChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,ISNULL(T.OldTitle,'--') AS OldTitle
		 ,ISNULL(T.NewTitle,'--') AS Title
		 ,ISNULL(T.OldFirstName,'--') AS OldFirstName
		 ,ISNULL(T.NewFirstName,'--') AS FirstName
		 ,ISNULL(T.OldMiddleName,'--') AS OldMiddleName
		 ,ISNULL(T.NewMiddleName,'--') AS MiddleName
		 ,ISNULL(T.OldLastName,'--') AS OldLastName
		 ,ISNULL(T.NewLastName,'--') AS LastName
		 ,OldKnownAs
		 ,NewKnownAs AS KnownAs
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerNameChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- Description: Update name change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerNameChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @AccountNo VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@OldTitle VARCHAR(50)  
		,@NewTitle VARCHAR(50) 
		,@OldFirstName VARCHAR(50)  
		,@NewFirstName VARCHAR(50) 
		,@OldMiddleName VARCHAR(50)  
		,@NewMiddleName VARCHAR(50) 
		,@OldLastName VARCHAR(50)  
		,@NewLastName VARCHAR(50) 
		,@OldKnownAs VARCHAR(150)  
		,@NewKnownAs VARCHAR(150)  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@NameChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 

	SELECT  
		 @NameChangeLogId = C.value('(NameChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	SELECT 
		 @OldTitle = OldTitle
		,@OldFirstName = OldFirstName
		,@OldMiddleName = OldMiddleName
		,@OldLastName = OldMiddleName
		,@OldKnownAs = OldKnownAs
		,@NewTitle = NewTitle
		,@NewFirstName = NewFirstName
		,@NewMiddleName = NewMiddleName
		,@NewLastName = NewLastName 
		,@NewKnownAs = NewKnownAs
		,@GlobalAccountNo = GlobalAccountNumber
		,@AccountNo = AccountNo
		,@Remarks = Remarks
	FROM Tbl_CustomerNameChangeLogs
	WHERE NameChangeLogId = @NameChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerNameChangeLogs 
											WHERE NameChangeLogId = @NameChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerNameChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND NameChangeLogId = @NameChangeLogId  

							UPDATE [CUSTOMERS].Tbl_CustomersDetail
							SET       
								 Title = @NewTitle
								,FirstName = @NewFirstName  
								,MiddleName = @NewMiddleName  
								,LastName = @NewLastName  
								,KnownAs = @NewKnownAs   
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							
							INSERT INTO Tbl_Audit_CustomerNameChangeLogs(  
								 NameChangeLogId
								,AccountNo
								,GlobalAccountNumber
								,OldTitle
								,NewTitle
								,OldFirstName
								,NewFirstName
								,OldMiddleName
								,NewMiddleName
								,OldLastName
								,NewLastName
								,OldKnownAs
								,NewKnownAs
								,ApproveStatusId
								,Remarks
								,CreatedDate
								,CreatedBy
								,PresentApprovalRole
								,NextApprovalRole)  
							VALUES(  
								 @NameChangeLogId
								,@AccountNo
								,@GlobalAccountNo  
								,@OldTitle
								,@NewTitle 
								,@OldFirstName
								,@NewFirstName
								,@OldMiddleName  
								,@NewMiddleName
								,@OldLastName
								,@NewLastName
								,@OldKnownAs
								,@NewKnownAs
								,@ApprovalStatusId
								,@Remarks
								,dbo.fn_GetCurrentDateTime()  
								,@ModifiedBy
								,@PresentRoleId
								,@NextRoleId) 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerNameChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo  
							AND NameChangeLogId = @NameChangeLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerNameChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND NameChangeLogId = @NameChangeLogId  

					UPDATE [CUSTOMERS].Tbl_CustomersDetail
					SET       
						 Title = @NewTitle
						,FirstName = @NewFirstName  
						,MiddleName = @NewMiddleName  
						,LastName = @NewLastName  
						,KnownAs = @NewKnownAs   
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime() 
					WHERE GlobalAccountNumber = @GlobalAccountNo 
					
					INSERT INTO Tbl_Audit_CustomerNameChangeLogs(  
						 NameChangeLogId
						,AccountNo
						,GlobalAccountNumber
						,OldTitle
						,NewTitle
						,OldFirstName
						,NewFirstName
						,OldMiddleName
						,NewMiddleName
						,OldLastName
						,NewLastName
						,OldKnownAs
						,NewKnownAs
						,ApproveStatusId
						,Remarks
						,CreatedDate
						,CreatedBy
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES(  
						 @NameChangeLogId
						,@AccountNo
						,@GlobalAccountNo  
						,@OldTitle
						,@NewTitle 
						,@OldFirstName
						,@NewFirstName
						,@OldMiddleName  
						,@NewMiddleName
						,@OldLastName
						,@NewLastName
						,@OldKnownAs
						,@NewKnownAs
						,@ApprovalStatusId
						,@Remarks
						,dbo.fn_GetCurrentDateTime()  
						,@ModifiedBy
						,@PresentRoleId
						,@NextRoleId)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
																WHERE NameChangeLogId = @NameChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
						WHERE NameChangeLogId = @NameChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerNameChangeLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND NameChangeLogId = @NameChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END
-------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek   
-- Create date: 03-Apr-2015      
-- Description: The purpose of this procedure is to Update Customer Type 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerType]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @GlobalAccountNumber VARCHAR(50)   
		,@CustomerTypeId INT 
		,@ModifiedBy VARCHAR(50)  
		,@Reason VARCHAR(MAX)  
		,@ApprovalStatusId INT  
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
    
	SELECT @GlobalAccountNumber=C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')  
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')   
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)  
  
		   
	IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
				WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeCustomerTypeBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
			INSERT INTO Tbl_CustomerTypeChangeLogs( 
				 GlobalAccountNumber
				,OldCustomerTypeId
				,NewCustomerTypeId
				,CreatedBy
				,CreatedDate
				,ApprovalStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole)
			SELECT GlobalAccountNumber
				,CustomerTypeId
				,@CustomerTypeId
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,@ApprovalStatusId	
				,@Reason
				,@PresentRoleId
				,@NextRoleId						   
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails		
			WHERE GlobalAccountNumber = @GlobalAccountNumber		
			
			SET @TypeChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails
					SET       
						 CustomerTypeId = @CustomerTypeId 
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerTypeChangeLogs(  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber  
						,OldCustomerTypeId  
						,NewCustomerTypeId  
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber  
						,OldCustomerTypeId  
						,NewCustomerTypeId  
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_CustomerTypeChangeLogs WHERE CustomerTypeChangeLogId = @TypeChangeRequestId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeCustomerTypeBE')
		END	
END
------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.CustomerTypeChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldCustomerTypeId
		 ,T.NewCustomerTypeId
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.OldCustomerTypeId) AS OldCustomerType
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.NewCustomerTypeId) AS NewCustomerType
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerTypeChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- Description: Update type change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerTypeChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@OldCustomerTypeId INT
		,@NewCustomerTypeId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@TypeChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 

	SELECT  
		 @TypeChangeLogId = C.value('(CustomerTypeChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	SELECT 
		 @OldCustomerTypeId = OldCustomerTypeId
		,@NewCustomerTypeId = NewCustomerTypeId
		,@GlobalAccountNo = GlobalAccountNumber
		,@Remarks = Remarks
	FROM Tbl_CustomerTypeChangeLogs
	WHERE CustomerTypeChangeLogId = @TypeChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerTypeChangeLogs 
											WHERE CustomerTypeChangeLogId = @TypeChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerTypeChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerTypeChangeLogId = @TypeChangeLogId 

							UPDATE [CUSTOMERS].Tbl_CustomerProceduralDetails
							SET       
								 CustomerTypeId = @NewCustomerTypeId
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTypeChangeLogs(  
								 CustomerTypeChangeLogId
								,GlobalAccountNumber
								,OldCustomerTypeId
								,NewCustomerTypeId
								,Remarks
								,ApprovalStatusId
								,CreatedBy
								,CreatedDate
								,PresentApprovalRole
								,NextApprovalRole)  
							VALUES(  
								 @TypeChangeLogId
								,@GlobalAccountNo  
								,@OldCustomerTypeId
								,@NewCustomerTypeId 
								,@Remarks
								,@ApprovalStatusId  
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,@PresentRoleId
								,@NextRoleId) 
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerTypeChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerTypeChangeLogId = @TypeChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerTypeChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND CustomerTypeChangeLogId = @TypeChangeLogId 

					UPDATE [CUSTOMERS].Tbl_CustomerProceduralDetails
					SET       
						 CustomerTypeId = @NewCustomerTypeId
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @GlobalAccountNo 
					
					INSERT INTO Tbl_Audit_CustomerTypeChangeLogs(  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber
						,OldCustomerTypeId
						,NewCustomerTypeId
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES(  
						 @TypeChangeLogId
						,@GlobalAccountNo  
						,@OldCustomerTypeId
						,@NewCustomerTypeId 
						,@Remarks
						,@ApprovalStatusId  
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,@PresentRoleId
						,@NextRoleId)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerTypeChangeLogs 
																WHERE CustomerTypeChangeLogId = @TypeChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerTypeChangeLogs 
						WHERE CustomerTypeChangeLogId = @TypeChangeLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerTypeChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND CustomerTypeChangeLogId = @TypeChangeLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
------------------------------------------------------------------------------------------
