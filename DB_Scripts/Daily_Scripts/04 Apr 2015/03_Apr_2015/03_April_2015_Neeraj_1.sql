GO
/****** Object:  StoredProcedure [dbo].[USP_GetRegions]    Script Date: 04/03/2015 13:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Faiz-ID103>  
-- Create date: <02-Jan-2015>  
-- Description: <Retriving Regions records form Tbl_MRegion>  
-- MODIFIED BY:  <NEERAJ KANOJIYA-ID103>  
-- Create date: <03-APRIL-2015>  
-- Description: <LEFT JOIN ADDED>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetRegions]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @PageNo INT  
    ,@PageSize INT  
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RegionsBE') AS T(C)  
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY R.ActiveStatusId ASC , R.CreatedDate DESC ) AS RowNumber
     ,R.RegionId  
     ,R.StateCode 
     ,S.StateName
     ,RegionName
     ,ActiveStatusId  
   FROM Tbl_MRegion R
   LEFT JOIN Tbl_States S  On R.StateCode=S.StateCode  
   WHERE ActiveStatusId IN(1,2)  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('RegionsBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('RegionsBEInfoByXml')   
END
------------------------------------------------------------------------------------
GO
--=============================================================      
--AUTHOR : NEERAJ KANOJIYA      
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS      
--CREATED ON: 30-SEP-14      
--=============================================================      
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchByNo]      
(      
@XmlDoc XML      
)      
AS      
BEGIN      
 DECLARE @BatchNo INT    
		,@BU_ID VARCHAR(50)  
		,@BatchDate DATE
		,@BatchID INT
       
 SELECT @BatchNo  = C.value('(BatchNo)[1]','INT')     
		,@BatchDate = C.value('(BatchDate)[1]','DATE')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')            
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)      
       
       
 --IF EXISTS(SELECT 0 FROM Tbl_BATCH_ADJUSTMENT WHERE BatchNo=@BatchNo)      
 --BEGIN    
 SELECT @BatchID=( SELECT BatchID FROM Tbl_BATCH_ADJUSTMENT
						WHERE BatchNo= @BatchNo
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						AND BU_ID=@BU_ID )
 if(@BatchID!='')
 BEGIN
  SELECT BA.BatchID,    
    BA.BatchNo,    
    BA.BatchDate,    
    BA.BatchTotal,    
    (SELECT COUNT(0)     
     FROM Tbl_BillAdjustments     
     WHERE BatchNo=(SELECT BatchID FROM Tbl_BATCH_ADJUSTMENT    
         WHERE BatchID=@BatchID)) AS TotalCustomers,    
    RC.ReasonCode,   
    RC.RCID,   
    (BA.BatchTotal-    
    (SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=(SELECT BatchID FROM Tbl_BATCH_ADJUSTMENT    
                          WHERE BatchID=@BatchID))) AS PendingAmount,    
    CONVERT(INT, (BA.BatchTotal-                          
    (SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=(SELECT BatchID FROM Tbl_BATCH_ADJUSTMENT    
                          WHERE BatchID=@BatchID)))) AS PendingUnit,    
    (SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=(SELECT BatchID FROM Tbl_BATCH_ADJUSTMENT    
                          WHERE BatchID=@BatchID)) AS BillAdjustmentType    
   FROM  Tbl_BATCH_ADJUSTMENT AS BA    
   JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID    
   WHERE BatchID=@BatchID
   FOR XML PATH('BillAdjustmentsBe'),TYPE   
 END
 ELSE
 BEGIN
 SELECT 0 AS BatchNo
 FOR XML PATH('BillAdjustmentsBe'),TYPE 
 END         
END   