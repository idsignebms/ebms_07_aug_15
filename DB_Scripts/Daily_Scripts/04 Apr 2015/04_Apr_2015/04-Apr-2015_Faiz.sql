GO
-- =============================================      
-- Author:  V.Bhimaraju      
-- Create date: 28-08-2014      
-- Modified By: T.Karthik    
-- Modified date: 03-11-2014    
-- Description: The purpose of this procedure is to get Books list By Cycle      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
      
 DECLARE @CycleId VARCHAR(MAX)       
   
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)      
   
 IF(@CycleId != '')      
  BEGIN      
   SELECT      
   (      
    --SELECT B.BookNo      
    --  ,(CASE   
    --   WHEN ISNULL(ID,'') = ''   
    --    THEN B.BookCode   
    --   ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
    --  END) AS BookNoWithDetails  
    --  ,COUNT(CPD.BookNo)  
    --FROM Tbl_BookNumbers B , CUSTOMERS.Tbl_CustomerProceduralDetails CPD  
    --WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))    
    --AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    --AND B.ActiveStatusId=1    AND CPD.BookNo=B.BookNo  
    --GROUP BY   
    --B.BookNo,ID,B.BookCode  
    --HAVING COUNT(CPD.BookNo)>0  
      
    --ORDER BY B.BookNo ASC      
    
    
    select  B.BookNo      
      ,(CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN B.BookCode   
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
      END) AS BookNoWithDetails  
      ,COUNT(B.BookNo) over() as BookNo
       from Tbl_BookNumbers   B
    where ActiveStatusId=1 
    and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
 ELSE      
  BEGIN      
   SELECT      
   (      
    SELECT (CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN BookCode   
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')   
      END) AS BookNo        
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1      
    ORDER BY BookNo ASC      
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
       
END  
  
GO
------------------------------------------------------------------------------------