GO
DECLARE @MenuId INT

INSERT INTO Tbl_Menus (Name,[Path],ReferenceMenuId,Page_Order)
VALUES('Change Customer Approvals','../ConsumerManagement/ChangesApproval.aspx',1,(SELECT MAX(Page_Order)+1 FROM Tbl_Menus WHERE ReferenceMenuId = 1)) 

SET @MenuId = SCOPE_IDENTITY()

INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,@MenuId,1,1,'Admin',GETDATE(),1)
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(2,@MenuId,1,1,'Admin',GETDATE(),1)
GO
-------------------------------------------------------------------------------------
GO
ALTER TABLE Tbl_CustomerActiveStatusChangeLogs
ADD PresentApprovalRole INT, NextApprovalRole INT
GO
CREATE TABLE Tbl_Audit_CustomerActiveStatusChangeLogs
(
	 ActiveStatusChangeLogId	INT
	,AccountNo	VARCHAR(50)
	,OldStatus	INT
	,NewStatus	INT
	,ApproveStatusId	INT
	,Remarks	VARCHAR(200)
	,CreatedBy	VARCHAR(50)
	,CreatedDate	DATETIME
	,ModifiedBy	VARCHAR(50)
	,ModifiedDate	DATETIME
	,PresentApprovalRole	INT
	,NextApprovalRole	INT
)
GO
--------------------------------------------------------------------
GO
ALTER VIEW [dbo].[UDV_CustomerMeterInformation]   
AS  
 SELECT  
   CD.GlobalAccountNumber   
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,CD.KnownAs  
  ,CD.ActiveStatusId  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName  
  ,PD.ReadCodeID  
  ,PD.SortOrder  
  ,AD.Highestconsumption  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BN.CycleId  
  ,BN.CycleCode  
  ,BN.CycleName  
  ,BN.SCCode  
  ,BN.ServiceCenterId  
  ,BN.ServiceCenterName  
  ,BN.SUCode  
  ,BN.ServiceUnitName  
  ,BN.SU_ID  
  ,BN.BUCode  
  ,BN.BU_ID  
  ,BN.BusinessUnitName  
  ,MS.[StatusName] AS ActiveStatus  
  ,CD.EmailId  
  ,PD.MeterNumber   
  ,MI.MeterType AS MeterTypeId  
  ,PD.PhaseId  
  ,AD.InitialReading   
 ,AD.PresentReading   
  ,AD.AvgReading  
  ,PD.RouteSequenceNumber AS RouteSequenceNo  
  ,PD.CustomerTypeId  
 ,CD.CreatedDate  
 ,CD.CreatedBy  
 ,CD.ModifiedDate  
 ,CD.ModifedBy   
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo   
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
LEFT JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo  
  
-------------------------------------------------------------------------------------------------
GO

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer ActiveStausLog 
   --ModifiedBy : Padmini
   --Modified Date :05-02-2015
-- Modified By: Karteek
-- Modified Date: 03-04-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerActiveStatus]
(
	@XmlDoc xml    
)
AS
BEGIN
	DECLARE   
		 @GlobalAccountNumber VARCHAR(50)	
		,@ModifiedBy VARCHAR(50)
		,@ActiveStatusId INT
		,@Details VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
        
	SELECT     
		 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
		,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
 
	IF((SELECT COUNT(0)FROM Tbl_CustomerActiveStatusChangeLogs 
			WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@StatusChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					
			INSERT INTO Tbl_CustomerActiveStatusChangeLogs( 
				 AccountNo
				,OldStatus
				,NewStatus
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole)
			SELECT GlobalAccountNumber
				,ActiveStatusId
				,@ActiveStatusId
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,@ApprovalStatusId
				,@Details
				,@PresentRoleId
				,@NextRoleId 
			FROM [CUSTOMERS].[Tbl_CustomerSDetail]			
			WHERE GlobalAccountNumber = @GlobalAccountNumber		

			SET @StatusChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 ActiveStatusId = @ActiveStatusId
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(  
						 ActiveStatusChangeLogId	
						,AccountNo	
						,OldStatus	
						,NewStatus	
						,ApproveStatusId	
						,Remarks	
						,CreatedBy	
						,CreatedDate
						,PresentApprovalRole	
						,NextApprovalRole)  
					SELECT  
						 ActiveStatusChangeLogId	
						,AccountNo	
						,OldStatus	
						,NewStatus	
						,ApproveStatusId	
						,Remarks	
						,CreatedBy	
						,CreatedDate
						,PresentApprovalRole	
						,NextApprovalRole
					FROM Tbl_CustomerActiveStatusChangeLogs WHERE ActiveStatusChangeLogId = @StatusChangeRequestId
					
				END
				
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')

		END	
END
-------------------------------------------------------------------------------------
GO

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Status Requested for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerStatusChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 9 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.ActiveStatusChangeLogId
		 ,T.AccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldStatus AS OldStatusId
		 ,T.NewStatus AS NewStatusId
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.OldStatus) AS OldStatus
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.NewStatus) AS NewStatus
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerActiveStatusChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update custoemr status change Details after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerStatusChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(200)  
		,@ApprovalStatusId INT  
		,@OldStatusId INT
		,@NewStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ActiveStatusChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 

	SELECT  
		 @ActiveStatusChangeLogId = C.value('(ActiveStatusChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	SELECT 
		 @OldStatusId = OldStatus
		,@NewStatusId = NewStatus
		,@GlobalAccountNo = AccountNo
		,@Remarks = Remarks
	FROM Tbl_CustomerActiveStatusChangeLogs
	WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs 
											WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerActiveStatusChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE AccountNo = @GlobalAccountNo 
							AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId

							UPDATE CUSTOMERS.Tbl_CustomersDetail
							SET       
								 ActiveStatusId = @NewStatusId
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @GlobalAccountNo
							
							INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(  
								 ActiveStatusChangeLogId
								,AccountNo
								,OldStatus
								,NewStatus
								,ApproveStatusId
								,Remarks
								,CreatedDate
								,CreatedBy
								,PresentApprovalRole
								,NextApprovalRole)  
							VALUES(  
								 @ActiveStatusChangeLogId
								,@GlobalAccountNo  
								,@OldStatusId
								,@NewStatusId 
								,@ApprovalStatusId
								,@Remarks
								,dbo.fn_GetCurrentDateTime()  
								,@ModifiedBy
								,@PresentRoleId
								,@NextRoleId) 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerActiveStatusChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE AccountNo = @GlobalAccountNo 
							AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerActiveStatusChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE AccountNo = @GlobalAccountNo 
					AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 ActiveStatusId = @NewStatusId
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(  
						 ActiveStatusChangeLogId
						,AccountNo
						,OldStatus
						,NewStatus
						,ApproveStatusId
						,Remarks
						,CreatedDate
						,CreatedBy
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES(  
						 @ActiveStatusChangeLogId
						,@GlobalAccountNo  
						,@OldStatusId
						,@NewStatusId 
						,@ApprovalStatusId
						,@Remarks
						,dbo.fn_GetCurrentDateTime()  
						,@ModifiedBy
						,@PresentRoleId
						,@NextRoleId) 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs 
																WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs 
						WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerActiveStatusChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE AccountNo = @GlobalAccountNo  
			AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END
-------------------------------------------------------------------------------------------------------
