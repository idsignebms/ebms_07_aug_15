ALTER TABLE Tbl_Audit_CustomerMeterInfoChangeLogs
ADD MeterChangedDate DATETIME
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs
ADD InitialBillingkWh INT
GO
ALTER TABLE Tbl_Audit_CustomerMeterInfoChangeLogs
ADD InitialBillingkWh INT
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs
ADD NewMeterInitialReading VARCHAR(20)
GO
ALTER TABLE Tbl_Audit_CustomerMeterInfoChangeLogs
ADD NewMeterInitialReading VARCHAR(20)
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs
ADD NewMeterReadingDate DATETIME
GO
ALTER TABLE Tbl_Audit_CustomerMeterInfoChangeLogs
ADD NewMeterReadingDate DATETIME
GO
-------------------------------------------------------------------------------------------
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS INT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
			AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
			AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
			AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	
	DROP TABLE #ReadCustomersList

END
----------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,TC.ClassName
		,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
		,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
             ,CD.Service_Landmark    
             ,CD.Service_City,'',    
             CD.Service_ZipCode) AS ServiceAddress
		,BU.BusinessUnitName
		,SU.ServiceUnitName
		,SC.ServiceCenterName
		,PD.SortOrder AS CustomerSortOrder
		,C.CycleName
		,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
		,BN.SortOrder AS BookSortOrder
		,PD.MeterNumber AS MeterNo
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
			AND PD.ReadCodeID = 1
	INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo     
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
				AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
				AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
				AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
	LEFT JOIN Tbl_DirectCustomersAvgReadings DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	WHERE DAVG.AvgReadingId IS NULL

END
------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,Cr.Usage  AS Usage
		,CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	--INNER JOIN UDV_PrebillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
			AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
			AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
			AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,CAST(SUM(usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	 HAVING (CAST(SUM(usage) AS DECIMAL(18,2)) > CAST(AverageReading AS DECIMAL(18,2))  + ((30/100)*100))
		 OR	(CAST(SUM(usage) AS DECIMAL(18,2)) < 250  + ((30/100)*100))
		 
	DROP TABLE #ReadCustomersList
	
END
----------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
					 
	SELECT	    
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt CD
	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID AND (ISNULL(CD.IsEmbassyCustomer,0) = 1   OR CD.CustomerStatusID = 2)
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	
	UNION ALL
	
	SELECT 
		 CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 AND BD.IsPartialBill = 1
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID 
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	
END
---------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- PrepaidCustomers
	SET	@BookDisableType = '1'

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
	SELECT   
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress AS ServiceAddress
		,CASE WHEN ISNULL(CustomerStatus.CustomerStatusID,0)=0 THEN 'HOLD / Closed Customers' ELSE 'Pre- Paid Meters' END AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	LEFT JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
	LEFT JOIN Tbl_MeterInformation MI ON CD.MeterNumber = MI.MeterNo and MI.MeterType = 1
	WHERE CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	AND (CustomerStatus.CustomerStatusID IS NUll OR  MI.MeterType IS NULL)
	
	UNION ALL 
	
	SELECT  
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress  AS ServiceAddress
		,'BookDisable No Power' AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM UDV_PrebillingRpt CD
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	INNER JOIN Tbl_BillingDisabledBooks BD ON ISNULL(CD.BookNo,0) = BD.BookNo
			AND BD.DisableTypeId IN(SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,','))
			AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
			AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
			AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	--INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId     

END
--------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,[usage] AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID  -- Read Customer 
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId 
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE CD.ReadCodeID = 2 AND CR.CustomerReadingId IS NULL 
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) 

END
--------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID			
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	LEFT JOIN Tbl_DirectCustomersAvgReadings DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	WHERE DAVG.AvgReadingId IS NULL AND CD.ReadCodeID = 1 AND CD.DeafaultUsage IS NULL
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))

END
----------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerMeterInformation CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerMeterInformation  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   

--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)

	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END

			SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

			--SELECT @PresentRoleId = PresentRoleId 
			--		,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)

			DECLARE @PrvMeterNo VARCHAR(50)
			SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber = @AccountNo)

			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET
				InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)


			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
						,@OldMeterReading
						,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = @NewMeterReading,
				InitialReading = @NewMeterInitialReading
			WHERE GlobalAccountNumber = @AccountNo

			DECLARE @NewMeterUsage VARCHAR(50)
			SET @NewMeterUsage =	(ISNULL(CONVERT(BIGINT,@NewMeterReading),0)) - (ISNULL(CONVERT(BIGINT,@NewMeterInitialReading),0))

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END
-----------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Meter NO Requested for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetMeterNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 7 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.MeterInfoChangeLogId
		 ,T.AccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,OldMeterNo
		 ,NewMeterNo
		 ,OldMeterReading
		 ,NewMeterReading
		 ,CONVERT(VARCHAR(20),MeterChangedDate,106) AS MeterChangedDate
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerMeterInfoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)

	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PreviousReading INT
	
	SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END

			SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

			--SELECT @PresentRoleId = PresentRoleId 
			--		,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)

			DECLARE @PrvMeterNo VARCHAR(50)
			SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
							WHERE GlobalAccountNumber = @AccountNo)

			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,@NewMeterInitialReading
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,@NewMeterReading
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,@NewMeterInitialReading
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET
				InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)


			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
						,@OldMeterReading
						,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = @NewMeterReading,
				InitialReading = @NewMeterInitialReading
			WHERE GlobalAccountNumber = @AccountNo

			DECLARE @NewMeterUsage VARCHAR(50)
			SET @NewMeterUsage =	(ISNULL(CONVERT(BIGINT,@NewMeterReading),0)) - (ISNULL(CONVERT(BIGINT,@NewMeterInitialReading),0))

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END
-------------------------------------------------------------------------------------------------
