
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails_ByFeederCycles]    Script Date: 04/04/2015 15:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author  : Suresh Kumar Dasi  
-- Create date  : 16 MAY 2014  
-- Description  : To Get the Customers Details under the selected FeederId and Cycles  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014  
-- Modified By : NEERAJ KANOJIYA	  
-- Modified Date : 1-MARCH-2014
-- Description  : CHANGE UDF FOR PERFORMACE ISSUE AND EXCLUDING PREPAID AND DEACTIVATED CUSTOMERS. 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails_ByFeederCycles]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE   
          --@FeederId VARCHAR(50)  
    @CycleId VARCHAR(MAX)  
   ,@Year INT  
   ,@Month INT  
     
 SELECT       
  --@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')  
   @CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@Year = C.value('(Year)[1]','INT')  
  ,@Month = C.value('(Month)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
   ---------------------------------------------------------------------------------------------------------------------------
 CREATE TABLE #CustomersList
 (TariffClassID INT,CustomersCount int,NormalUsage Decimal(18,2), EstimatedUsage  Decimal(18,2))  
 ---------------------------------------------------------------------------------------------------------------------------
 INSERT INTO #CustomersList
 SELECT CPD.TariffClassID
		,COUNT(0) as CustomersCount 
		,SUM(isnull(CR.Usage,0)) as UsageRead
		,SUM(CASE WHEN ISNULL(CR.CustomerReadingId,0)=0 THEN ISNULL(CAD.AvgReading,0) else 0 end) NonReadUsage
 FROM	CUSTOMERS.Tbl_CustomerProceduralDetails CPD
 INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo
									and CycleId in (SELECT [com] FROM dbo.fn_Split(@CycleId,','))
									and CPD.ReadCodeID=2 -- Read
INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
Left JOIN	 Tbl_CustomerReadings CR ON CR.GlobalAccountNumber=CPD.GlobalAccountNumber and IsBilled=0
Group By  CPD.TariffClassID
 ---------------------------------------------------------------------------------------------------------------------------
 INSERT INTO #CustomersList
 SELECT CPD.TariffClassID
		,COUNT (0) as CustomersCount
		,SUM(ISNULL(CDAV.AverageReading,0)) as UsageOfDirectCustomers
		,SUM(CASE WHEN CDAV.AvgReadingId  IS  NULL	then isnull(CAD.AvgReading,0) else 0 end   ) as	 DirectCustomersAvg
 FROM	 
 CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
 INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo 
							AND CycleId in (SELECT [com] FROM dbo.fn_Split(@CycleId,','))
							AND CPD.ReadCodeID=1 -- Direct
 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
 Left  join	Tbl_DirectCustomersAvgReadings CDAV ON CDAV.GlobalAccountNumber=CPD.GlobalAccountNumber
 Group By   CPD.TariffClassID
--------------------------------------------------------------------------------------------------------------------------- 
  
 SELECT  
  ( 
	  SELECT ClassID AS TariffId
			,ClassName AS TariffName
			,SUM(CustomersCount) as TotalRecords
			,sum(NormalUsage+EstimatedUsage) as Usage
	 FROM	 #CustomersList  CL
	 Inner join Tbl_MTariffClasses	TC ON CL.TariffClassID=TC.ClassID
	 Group By ClassID ,ClassName
	 ORDER BY ClassID 
  --SELECT   
  -- TariffId  
  -- ,T.ClassName AS TariffName  
  -- ,COUNT(0) AS TotalRecords  
  -- ,[dbo].[fn_GetMonthlyUsage_ByCycles](CycleId,TariffId,@Year,@Month) AS Usage  
  --FROM [UDV_CustDetailsForBillGen] CD  
  ----FROM [UDV_CustomerDescription] CD  
  --JOIN Tbl_MTariffClasses T ON CD.TariffId = T.ClassID  
  --WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))  
  --AND ActiveStatusId IN (1,2)  
  ----AND AccountNo NOT IN(SELECT AccountNo FROM Tbl_CustomerDetails WHERE MeterTypeId = 1)  
  --GROUP By TariffId,T.ClassName,CycleId  
  FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')  
    DROP table	#CustomersList
END  
