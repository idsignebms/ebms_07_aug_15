
GO
/****** Object:  StoredProcedure [dbo].[USP_GetEstimationList_Cycle]    Script Date: 04/04/2015 15:00:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                      
-- Author		: Suresh Kumar Dasi                    
-- Create date  : 25 July 2014                      
-- Description  : THIS PROCEDURE WILL GET ESTIMATION DETAILS OF READING   
-- Modified By: T.Karthik
-- Modified Date: 24-12-2014         
-- Modified Desc: Added cross join        
-- =======================================================================                      
ALTER PROCEDURE [dbo].[USP_GetEstimationList_Cycle]                     
(                      
@XmlDoc xml                      
)                      
AS                      
BEGIN 
 DECLARE @Year INT      
         ,@Month int                
         ,@Cycle Varchar(50)
                
	 SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @Cycle = C.value('(Cycle)[1]','VARCHAR(50)')		
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	         
 --  SELECT       
	--	ClassName 
	--	,ClassID
	--	,@Cycle AS Cycle
	--	,CC.CategoryName
	--	,CC.ClusterCategoryId
 --   into #Temp
 --   FROM Tbl_MTariffClasses T
	--CROSS JOIN MASTERS.Tbl_MClusterCategories CC
	    
	    
	    SELECT       
		ClassName 
		,ClassID
		,@Cycle AS Cycle
		,CC.CategoryName
		,CC.ClusterCategoryId
INTO #ClustersList
FROM Tbl_MTariffClasses T
CROSS JOIN MASTERS.Tbl_MClusterCategories CC
	    
	    
  SELECT       
  (      
  SELECT       
		ClassName 
		,TariffId AS ClassID
		,CycleId AS Cycle
		,CC.CategoryName
		,CC.ClusterCategoryId
		--,EnergytoCalculate AS EnergyToCalculate
		,ISNULL((SELECT EnergytoCalculate FROM Tbl_EstimationSettings 
		 
		WHERE CycleId = @Cycle AND BillingMonth = @Month AND BillingYear = @Year 
		AND ClusterCategoryId=CC.ClusterCategoryId AND TariffId=ES.TariffId),0
		) AS EnergyToCalculate
		,BillingMonth AS [Month]
		,BillingYear AS [Year]
		,dbo.fn_GetDirectCustomersCount(ClassID,@Cycle,CC.ClusterCategoryId) AS EstimatedCustomers  
    FROM Tbl_EstimationSettings ES
	--CROSS JOIN Customers.Tbl_MClusterCategories CC
	LEFT JOIN MASTERS.Tbl_MClusterCategories CC ON CC.ClusterCategoryId =ES.ClusterCategoryId
    LEFT JOIN Tbl_MTariffClasses T ON ES.TariffId = T.ClassID    
    WHERE CycleId = @Cycle AND BillingMonth = @Month AND BillingYear = @Year
    FOR XML PATH('EstimationDetails'),TYPE              
                                       
   )      
   ,
   ( 
   SELECT  ClusteList.ClassName as ClassName
	,ClusteList.ClassID	  as ClassID
	,ClusteList.CategoryName   as CategoryName
	,ClusteList.ClusterCategoryId   as ClusterCategoryId
	,CONVERT(INT,sum (case when isnull(ReadingUsage,0) =0 then 0 else 1 end) )  as TotalReadCustomers
	,CONVERT(INT,SUM(isnull(ReadingUsage,0))) as		TotalMetersUsage
	,CONVERT(INT,sum ( case when isnull(ReadCodeID,0) !=2 then 0 else 1 end))   as EstimatedCustomers
	,CONVERT(INT,SUM(isnull(DirectCustomersUsage,0))) as	 TotalMinimumUsage
	,@Cycle AS  Cycle
	 from #ClustersList As ClusteList
LEFT JOIN	
( 
  SELECT CD.GlobalAccountNumber,CPD.ClusterCategoryId,CPD.TariffClassID,CycleId,ReadCodeID
  ,Sum(ISNULL(CR.Usage,0)) as ReadingUsage,Sum(isnull(DCAVG.AverageReading,0))as DirectCustomersUsage
   from
  	   CUSTOMERS.Tbl_CustomerProceduralDetails	 CPD
  inner Join	CUSTOMERS.Tbl_CustomersDetail CD
  ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
  and CD.ActiveStatusId=1
  inner join Tbl_BookNumbers BN On BN.BookNo=CPD.BookNo
  Left Join Tbl_CustomerReadings CR On CR.GlobalAccountNumber=CPD.GlobalAccountNumber
  and Cr.IsBilled=0
  Left Join Tbl_DirectCustomersAvgReadings DCAVG
  on DCAVG.GlobalAccountNumber=CPD.GlobalAccountNumber
  Group By	 CD.GlobalAccountNumber,CPD.ClusterCategoryId,CPD.TariffClassID,CycleId,ReadCodeID
  ) AS CD 
  ON  ClusteList.ClassID = CD.TariffClassID and	  CD.CycleId=ClusteList.Cycle
  and ClusteList.ClusterCategoryId=Cd.ClusterCategoryId
   Group by	 
     ClusteList.ClassName 
	,ClusteList.ClassID
	,ClusteList.CategoryName
	,ClusteList.ClusterCategoryId
	--,ReadCodeID
--   select 
--	 CS.ClassName 
--	,CS.ClassID
--	,@Cycle AS Cycle
--	,CS.CategoryName
--	,CS.ClusterCategoryId
--	,(ABS(sum( case when isnull(CD.ReadCodeID,0)   =2 then 0 else 1 END)   -	   sum(case when (CR.GlobalAccountNumber) IS NULL then 0 else 1 END) ) ) EstimatedCustomers
--	,sum(case when (CR.GlobalAccountNumber) IS NULL then 0 else 1 END) as TotalReadCustomers
--	,CAST(Sum( ISNULL(CR.Usage,0)) AS INT) As TotalMetersUsage
--	,ABS(sum( case when isnull(CD.ReadCodeID,0)   =2 then 0 else 1 END)   -	   sum(case when (CR.GlobalAccountNumber) IS NULL then 0 else 1 END) )  TotalMinimumUsage
	
--from
--	#Temp  CS
--	LEft JOIN UDV_CustomerDescription  CD
--    ON CS.ClusterCategoryId=CD.ClusterCategoryId and CD.TariffId=CS.ClassID and CD.CycleId=CS.Cycle
--     and CD.ActiveStatusId = 1  
--     Left join Tbl_CustomerReadings CR On CD.GlobalAccountNumber   =CR.GlobalAccountNumber and IsBilled=0
--    Group by	--CS.ClusterCategoryId
--     CS.ClassName 
--	,CS.ClassID
--	,CS.CategoryName
--	,CS.ClusterCategoryId
    FOR XML PATH('EstimatedUsageDetails'),TYPE     
   )
  FOR XML PATH(''),ROOT('EstimationInfoByXml')  
   --drop table #ClustersList        
END 