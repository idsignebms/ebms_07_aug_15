GO
-- =============================================  
-- Author  : NEERAJ KANOJIYA  
-- Create date : 1-05-2014  
-- Description : To check Batch Number Exsitence in main and log table  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_IsBatchNoExists]  
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
   
 DECLARE @BatchNo int     
 ,@BatchDate DATETIME
 ,@BU_ID VARCHAR(50)
     ,@IsExists BIT = 0  
       
   
 SELECT @BatchNo = C.value('(BatchNo)[1]','VARCHAR(50)')   
        ,@BatchDate = C.value('(BatchDate)[1]','DATETIME')   
        ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')              
      
 FROM @XmlDoc.nodes('BillingBE') AS T(C)  
   
 --IF EXISTS(SELECT 0 FROM Tbl_BatchDetails WHERE (BatchNo=@BatchNo))    
 --IF EXISTS(SELECT BatchNo FROM (  
 --   SELECT BatchNo FROM Tbl_BatchDetails union   
 --   SELECT BatchNo FROM Tbl_BatchDetailsApproval) A  
 --   WHERE A.BatchNo=@BatchNo  
 --    )  
 IF(EXISTS(SELECT BatchNo   ---Faiz-ID103
    FROM Tbl_BatchDetails   
    WHERE BatchNo=@BatchNo   
      AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120)   
      AND BU_ID=@BU_ID)) 
  BEGIN  
   SET @IsExists = 1  
  END  
   
 SELECT @IsExists AS IsExists FOR XML PATH('BillingBE')  
   
END  
  GO
--------------------------------------------------------------------------------------------------  