
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@PreviousReading INT
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = OldMeterReading 
		,@NewMeterReading = NewMeterReading
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = NewMeterInitialReading
		,@NewMeterReadingDate = NewMeterReadingDate
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	SELECT TOP 1 
		 @PreviousReading = ISNULL(PresentReading,0)
		,@OldMeterReadingDate = ReadDate
	FROM Tbl_CustomerReadings 
	WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage VARCHAR(50)
			SET @NewMeterUsage = (ISNULL(CONVERT(BIGINT,@NewMeterReading),0)) - (ISNULL(CONVERT(BIGINT,@NewMeterInitialReading),0))

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET
								InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage > 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,@PreviousReading
										,@OldMeterReading
										,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
										,@Usage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,@NewMeterInitialReading
										,@NewMeterReading
										,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET
						InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage > 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,@PreviousReading
								,@OldMeterReading
								,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
								,@Usage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,@NewMeterInitialReading
								,@NewMeterReading
								,(SELECT dbo.fn_GetAverageReading(@AccountNo,CONVERT(NUMERIC,@Usage)))
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END
-----------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 10-10-2014
-- Description  : This method is used to insert payment details by accountNo.
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertPayments_ByAccountNo]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @AccountNo VARCHAR(50)
			,@CreatedBy VARCHAR(50)
			,@PaidAmount DECIMAL(20,4)
			,@PaymentRecievedDate VARCHAR(20)
			,@CustomerID VARCHAR(50)
			,@EffectedRows INT
			,@CurrentOutStanding DECIMAL(18,2)
			
	SELECT   @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
			,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
			,@PaymentRecievedDate = C.value('(PaymentReceivedDate)[1]','VARCHAR(20)')
			,@PaidAmount = C.value('(PaidAmount)[1]','DECIMAL(20,4)')
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)	
	
	--SELECT @AccountNo = AccountNo FROM Tbl_CustomerDetails WHERE ( AccountNo=@AccountNo OR OldAccountNo = @AccountNo)
	--SELECT @CustomerID = CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo

	SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE ( GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo)
	
	INSERT INTO Tbl_CustomerPayments(
					 --CustomerID
					 AccountNo
					,CreatedBy
					,CreatedDate
					,RecievedDate
					,PaidAmount
					,ActivestatusId
					,PaymentType
					)
			VALUES(
					--@CustomerID
					 @AccountNo
					,@CreatedBy
					,dbo.fn_GetCurrentDateTime()
					,@PaymentRecievedDate
					,@PaidAmount
					,1
					,3 -- Customer Payments
				)
		
		SELECT @EffectedRows = @@ROWCOUNT	
		
		--SET @CurrentOutStanding=ISNULL((SELECT OutStandingAmount FROM Tbl_CustomerDetails WHERE AccountNo=@AccountNo),0)
		
		--UPDATE Tbl_CustomerDetails	
		--	SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @PaidAmount
		--		,ModifiedBy = @CreatedBy
		--		,ModifiedDate = dbo.fn_GetCurrentDateTime()
		--WHERE AccountNo=@AccountNo		
		
		UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]				
		SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @PaidAmount
			,ModifedBy = @CreatedBy
			,ModifiedDate = dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@AccountNo
		
		--==This is For any payment is done update Paid Amount of latest bill in bills table
		IF ((SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo)>0)
		BEGIN
			DECLARE  @BillsPaidAmount DECIMAL(18,2)
					,@CustomerBillId INT
			SELECT TOP(1) @BillsPaidAmount=ISNULL(PaidAmount,0),@CustomerBillId=CustomerBillId
			FROM Tbl_CustomerBills
			WHERE AccountNo=@AccountNo
			ORDER BY CustomerBillId DESC
			
			UPDATE Tbl_CustomerBills SET PaidAmount=@BillsPaidAmount+@PaidAmount
			WHERE AccountNo=@AccountNo
			AND CustomerBillId=@CustomerBillId
		END

		SELECT 
			(CASE WHEN @EffectedRows > 0 THEN 1 ELSE 0 END) AS EffectedRows
		FOR XML PATH ('PaymentsBe')
		
	
END
---------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')				
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TrxFrom = '')  
		BEGIN  
			SELECT @TrxFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50))   
					FROM Tbl_MPaymentType 
					WHERE ActiveStatusID = 1  
					FOR XML PATH(''), TYPE)  
					.value('.','NVARCHAR(MAX)'),1,1,'')  
		END  
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, PT.PaymentType AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS PaymentType FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.PaymentType = CR.PaymentType   
	INNER JOIN Tbl_MPaymentType PT ON PT.PaymentTypeID = CR.PaymentType 
	  
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the Batch details
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetBatchDetails_ByBatchNo]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @BatchNo INT
		
	SELECT
		 @BatchNo=C.value('(BatchNo)[1]','INT')		
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
		
	SELECT 
		 BD.BatchNo
		,ISNULL(CONVERT(VARCHAR(20),BatchDate,106),'--') AS PaidDate
		,CONVERT(VARCHAR(25), CAST(BatchTotal AS MONEY), 1) AS Amount
		,COUNT(0) AS TotalRecords
		,CONVERT(VARCHAR(25), CAST(SUM(CP.PaidAmount) AS MONEY), 1) AS PaidAmount
	FROM Tbl_BatchDetails BD
	LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchNo AND CP.PaymentType = 1
	WHERE BD.BatchNo = @BatchNo
	GROUP BY BD.CreatedDate,BD.BatchNo,BD.BatchDate,BD.BatchTotal
	FOR XML PATH('RptPaymentsEditListBE')
	
END
------------------------------------------------------------------------------------------------------

GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the List of customers for Payment report By Batch No
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPaymentEditListReport_ByBatchNo]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @BatchNo INT
			,@PageSize INT  
			,@PageNo INT  
		
	SELECT
		 @BatchNo=C.value('(BatchNo)[1]','INT')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 		
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CR.BatchNo = @BatchNo
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	  
	SELECT RowNumber
		,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
		,UserId
		,Name AS UserName
		,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
		,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
		,TotalRecords 
		,GlobalAccountNumber
		,OldAccountNo
		,CycleName AS BookGroup
		,CustomerName
		,ServiceAdress
		,BusinessUnitName
	FROM #CustomerReadingsList
	WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the List of Batches for Payment report
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetBatchListEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			--,@PageSize INT  
			--,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')				
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	SELECT
	(
		SELECT 
			 ISNULL(CONVERT(VARCHAR(20),BD.CreatedDate,106),'--') AS TransactionDate
			,BD.BatchNo 
			,ISNULL(CONVERT(VARCHAR(20),BD.BatchDate,106),'--') AS PaidDate
			,CONVERT(VARCHAR(25), CAST(BD.BatchTotal AS MONEY), 1) AS Amount
			,U.Name AS UserName
			,CONVERT(VARCHAR(25), CAST(SUM(CP.PaidAmount) AS MONEY), 1) AS PaidAmount
			,COUNT(0) AS TotalRecords -- Total Customers
		FROM Tbl_BatchDetails BD
		INNER JOIN Tbl_UserDetails U ON U.UserId = BD.CreatedBy 			 
				AND BD.CreatedBy IN(SELECT [com] AS UserId FROM dbo.fn_Split(@Users,','))
				AND CONVERT (DATE,BD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
				AND BD.BU_ID = @BU_ID 
		LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchNo AND CP.PaymentType = 1
		GROUP BY BD.CreatedDate,BD.BatchNo,BD.BatchDate,BD.BatchTotal
				,U.Name
		HAVING SUM(CP.PaidAmount) >= BD.BatchTotal
		
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
END
------------------------------------------------------------------------------------------------

