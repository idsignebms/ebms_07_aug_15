
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForAssignMeter]    Script Date: 03/25/2015 15:53:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 25-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- =============================================                    
 CREATE PROCEDURE [dbo].[USP_GetCustDetailsForBillGen]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID  
				  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                   
				  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo       
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber    
				  ,A.OldAccountNo                 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful  
				  ,A.ServiceUnitName 
				  ,A.ServiceCenterName
				  ,A.CycleName
				  ,A.BookNo               
		  FROM [UDV_CustomerDescription] AS A    
		  LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END       
 --------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 04/05/2015 15:52:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<NEERAJ KANOJIYA>
-- Create date: <26-MAR-2015>
-- Description:	<This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>
-- =============================================
--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]
( @XmlDoc Xml)
AS
BEGIN
	 
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@ServiceUnitId	VARCHAR(50)
			,@PoleId Varchar(50)
			,@ServiceCenterId VARCHAR(50)
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	SELECT	@GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@Month = C.value('(BillMonth)[1]','VARCHAR(50)') 
			,@Year = C.value('(BillYear)[1]','VARCHAR(50)') 
			FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
			 
	IF(@GlobalAccountNumber !='')
	BEGIN
			SELECT 
			@ActiveStatusId = ActiveStatusId,
			@OutStandingAmount = ISNULL(OutStandingAmount,0)
			,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
			@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh
			,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
			,@OpeningBalance=isnull(OpeningBalance,0)
			,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@BusinessUnitName =BusinessUnitName
			,@TariffName =ClassName
			,@Service_HouseNo =Service_HouseNo
			,@Service_Street =Service_StreetName
			,@Service_City =Service_City
			,@ServiceZipCode  =Service_ZipCode
			,@Postal_HouseNo =Postal_HouseNo
			,@Postal_Street =Postal_StreetName
			,@Postal_City  =Postal_City
			,@Postal_ZipCode  =Postal_ZipCode
			,@Postal_LandMark =Postal_LandMark
			,@Service_LandMark =Service_LandMark
			,@OldAccountNumber=OldAccountNo
			,@BU_ID=BU_ID
			,@ServiceUnitId=ServiceCenterId
			,@PoleId=PoleID
			,@TariffId=ClassID
			,@ServiceCenterId=ServiceCenterId
			 
			 
			FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber 
			 
			 
			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
			BEGIN
				
				SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
					,@RegenCustomerBillId=CustomerBillId	
				FROM Tbl_CustomerBills(NOLOCK)  
				WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
				DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
				
				DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
				SELECT @PaidAmount=SUM(PaidAmount)
				FROM Tbl_CustomerBillPayments(NOLOCK)  
				WHERE BillNo=@RegenCustomerBillId

				IF @PaidAmount IS NOT NULL
				BEGIN
					SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
					UPDATE Tbl_CustomerBillPayments
					SET BillNo=NULL
					WHERE BillNo=@RegenCustomerBillId
				END
				
				----------------------Update Readings as is billed =0 ----------------------------
				UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				
				
				-- Insert Paid Meter Payments
				DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				 
				update CUSTOMERS.Tbl_CustomerActiveDetails
				SET OutStandingAmount=@OutStandingAmount
				where GlobalAccountNumber=@GlobalAccountNumber
				----------------------------------------------------------------------------------
	 
				--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
				DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
				-------------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------------
				--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
				--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
				--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
				
				---------------------------------------------------------------------------------------
				--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
				---------------------------------------------------------------------------------------
			END

			IF @ReadCodeId=2 -- 
			BEGIN
	 			SELECT  @PreviousReading=PreviousReading,
						@CurrentReading = PresentReading,@Usage=Usage,
						@LastBillGenerated=LastBillGenerated,
						@PreviousBalance =Previousbalance,
						@BalanceUnits=BalanceUsage,
						@IsFromReading=IsFromReading,
						@PrevCustomerBillId=CustomerBillId,
						@PreviousBalance=PreviousBalance,
						@Multiplier=Multiplier,
						@ReadDate=ReadDate
				FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
				
				
				
				IF @IsFromReading=1
				BEGIN
						SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
						IF @Usage<@BalanceUnits
						BEGIN
							SET @ActualUsage=@Usage
							SET @Usage=0
							SET @RemaningBalanceUsage=@BalanceUnits-@Usage
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
						END
						ELSE
						BEGIN
							SET @ActualUsage=@Usage
							SET @Usage=@Usage-@BalanceUnits
							SET @RemaningBalanceUsage=0
						END
				END
				ELSE
				BEGIN
						SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
						SET @ActualUsage=@Usage
						SET @RemaningBalanceUsage=@Usage+@BalanceUnits
				END
			END
			ELSE --@CustomerTypeID=1 -- Direct customer
			BEGIN
				set @IsEstimatedRead =1
				 
				-- Get balance usage of the customer
				SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
							  @PreviousBalance=PreviousBalance
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber       
				ORDER BY CustomerBillId DESC 
				
				IF @PreviousBalance IS NULL
				BEGIN
					SET @PreviousBalance=@OpeningBalance
				END

			
		 
				      
				SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
				SET @ReadType=1 -- Direct
				SET @ActualUsage=@Usage
			END
			
					
 			
			
			
			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0
			 
			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
			DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
			
			-- =============================================
			-- If customer is disable 
			SELECT @BookDisableType=DisableTypeId FROM Tbl_BillingDisabledBooks WHERE BookNo=@BookNo AND YearId=@Year AND MonthId=@Month AND IsActive=1 AND ApproveStatusId=2
			 
			
			IF @BookDisableType IS NULL
				SET @BookDisableType=-1
			IF @BookDisableType<>-1 
			BEGIN 
				SET @FixedCharges=0
			END
			ELSE
			BEGIN
				INSERT INTO @tblFixedCharges(ClassID ,Amount)
				SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
				SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges					
			END 
			
			
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
			BEGIN
				
				IF @GetPaidMeterBalance<@FixedCharges
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=0
					SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
					SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
				END
				ELSE
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
					SET @FixedCharges=0
				END
			END
			------------------------
			-- Caluclate tax here
			
			SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			
			 
			
			SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0
			
			set @NetArrears=@OutStandingAmount-@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			
			
			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber
				Group BY CustomerBillId       
				ORDER BY CustomerBillId DESC 
			 
			if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage
			
			if @RemainingBalanceUnits IS NULL
			 set @RemainingBalanceUnits=0
			
			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
			
			 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			-----------------------------------------------------------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
			)        
			 Values( @GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber) 
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber) 
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))        
				,@BU_ID
				,@ServiceUnitId
				,@ServiceCenterId
				,@PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber
				 ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,@TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,@AverageUsageForNewBill
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
			       
		)
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL	
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)
	END
END