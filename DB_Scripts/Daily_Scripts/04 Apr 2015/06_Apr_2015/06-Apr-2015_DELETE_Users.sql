-----------------------------------------------------------------------------
GO
DELETE FROM Tbl_UserBusinessUnits 
	WHERE UserId IN 
		(SELECT UserId FROM Tbl_UserDetails 
		WHERE UserId NOT IN ('Admin','SuperAdmin'))
GO
DELETE FROM Tbl_UserDetails WHERE UserId NOT IN ('Admin','SuperAdmin')	
GO
UPDATE Tbl_MRoles SET IsActive=0 WHERE RoleId IN ( 4,6,7,8,9,10,11)
GO
-----------------------------------------------------------------------------