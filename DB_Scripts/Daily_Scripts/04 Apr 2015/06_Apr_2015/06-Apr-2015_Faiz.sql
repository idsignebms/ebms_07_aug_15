GO
-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 24-Sep-2014  
-- Modified By: T.Karthik  
-- Modified Date : 17-10-2014  
-- Description: Purpose is To get List Of Billing Disabled BookNo  
-- =============================================  
ALTER PROCEDURE USP_GetBillingDisabledBookNo
(  
 @XmlDoc Xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageNo INT  
   ,@PageSize INT  
   ,@YearId INT  
   ,@MonthId INT  
      
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
    ,@PageSize = C.value('(PageSize)[1]','INT')  
    ,@YearId=C.value('(YearId)[1]','INT')  
    ,@MonthId=C.value('(MonthId)[1]','INT')  
       
  FROM @XmlDoc.nodes('BillingDisabledBooksBe') AS T(C)   
    
  CREATE TABLE #BillingDisabledBookNos(Sno INT PRIMARY KEY IDENTITY(1,1),DisabledBookId INT)  
  
  INSERT INTO #BillingDisabledBookNos(DisabledBookId)  
   SELECT  
    DisabledBookId  
   FROM Tbl_BillingDisabledBooks  
   WHERE IsActive=1  
   AND (YearId=@YearId OR @YearId='')  
   AND (MonthId=@MonthId OR @MonthId='')  
   ORDER BY CreatedDate DESC  
     
  DECLARE @Count INT=0  
  SET @Count=(SELECT COUNT(0) FROM #BillingDisabledBookNos)  
    
  SELECT  
    Sno AS RowNumber  
    ,BDB.BookNo AS BookNo  -- Faiz_ID103
    ,(Select ID From Tbl_BookNumbers where BookNo = BDB.BookNo) AS ID
    ,BDB.DisabledBookId  
    ,ISNULL(Remarks,'--') AS Details  
    ,MonthId  
    ,YearId  
    ,((select dbo.fn_GetMonthName([MonthId]))+'-'+CONVERT(varchar(10), [YearId])) AS MonthYear  
    ,@Count AS TotalRecords  
  FROM Tbl_BillingDisabledBooks AS BDB  
  JOIN #BillingDisabledBookNos LBDB ON BDB.DisabledBookId=LBDB.DisabledBookId  
  Left Join Tbl_BookNumbers BN on BDB.BookNo=BN.BookNo -- Faiz_ID103
  AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
     
 END  
 GO
 ----------------------------------------------------------------------------------------------------
 
 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/06/2015 17:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author		:  NEERAJ KANOJIYA
-- Create date	:  27-MARCH-2015
-- Description	:  GET CUSTOMERS FULL DETAILS
-- Modified By  :  Faiz-ID103
-- Modified Date:  06-Apr-2015
-- Description	:  Added Present Reading Field.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  

    DECLARE  @GlobalAccountNumber VARCHAR(50)  
    SELECT            
	   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)     
		SELECT top 1
		
		CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord
		,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord
		,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord
		,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord
		,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs
		,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord
		,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord
		,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord
		,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord
		,CD.DocumentNo	
		,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate	
		,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate	
		,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate	
		,CD.OldAccountNo	
		,CT.CustomerType
		,BookCode
		,CD.PoleID	
		,CD.MeterNumber	
		,TC.ClassName as Tariff	
		,CPD.IsEmbassyCustomer	
		,CPD.EmbassyCode	
		,CD.PhaseId	
		,RC.ReadCode as ReadType
		,CD.IsVIPCustomer
		,CD.IsBEDCEmployee
		,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService	
		,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService	
		,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService
		,PAD.AreaCode AS AreaService 
		,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode			
		,PAD.IsCommunication AS IsCommunicationService			
		,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal	
		,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal	
		,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL	
		,PAD1.AreaCode AS  AreaPostal		
		,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode
		,PAD.IsCommunication AS IsCommunicationPostal				
		,CD.ServiceAddressID		
		,CAD.IsCAPMI
		,InitialBillingKWh	
		,CAD.InitialReading	
		,CAD.PresentReading	--Faiz-ID103
		,CD.AvgReading as AverageReading	
		,CD.Highestconsumption	
		,CD.OutStandingAmount	
		,OpeningBalance
		,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1
		,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2
		,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType
		,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName
		,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName
		,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase
		,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName
		,CTD.TenentId
		,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent
		,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent
		,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent
		,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent
		,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent
		,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent
		,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent
		,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName
		,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy
		,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber
		,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount
		,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy
		,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy  
		,CD.IsSameAsService
		from UDV_CustomerDescription CD
		left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId
		left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId
		left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0
		left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber 
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber
		LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId
		LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId
		LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId
		LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo
		LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId
		LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId
		Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber
		FOR XML PATH('CustomerRegistrationBE'),TYPE
END
GO
------------------------------------------------------------------------------------------------------------

GO
/****** Object:  StoredProcedure [CUSTOMERS].[USP_CustomerRegistration]    Script Date: 04/06/2015 16:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	CUSTOMER REGISTRATION
-- Modified By: Faiz-ID103
-- Modified Date: 06-Mar-2015
-- Description: Added Initial Reading in Step 10 Custmer Active Details
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_CustomerRegistration]
(   
 @XmlDoc xml  
)  
AS  
BEGIN  
DECLARE
				@GlobalAccountNumber  Varchar(10)
				,@AccountNo  Varchar(50)
				,@TitleTanent varchar(10)
				,@FirstNameTanent	varchar(100)
				,@MiddleNameTanent	varchar(100)
				,@LastNameTanent	varchar(100)
				,@PhoneNumberTanent	varchar(20)
				,@AlternatePhoneNumberTanent 	varchar(20)
				,@EmailIdTanent	varchar(MAX)
				,@TitleLandlord	varchar(10)
				,@FirstNameLandlord	varchar(50)
				,@MiddleNameLandlord	varchar(50)
				,@LastNameLandlord	varchar(50)
				,@KnownAs	varchar(150)
				,@HomeContactNumberLandlord	varchar(20)
				,@BusinessPhoneNumberLandlord 	varchar(20)
				,@OtherPhoneNumberLandlord	varchar(20)
				,@EmailIdLandlord	varchar(MAX)
				,@IsSameAsService	BIT
				,@IsSameAsTenent	BIT
				,@DocumentNo	varchar(50)
				,@ApplicationDate	Datetime
				,@ConnectionDate	Datetime
				,@SetupDate	Datetime
				,@IsBEDCEmployee	BIT
				,@IsVIPCustomer	BIT
				,@OldAccountNo	Varchar(20)
				,@CreatedBy	varchar(50)
				,@CustomerTypeId	INT
				,@BookNo	varchar(30)
				,@PoleID	INT
				,@MeterNumber	Varchar(50)
				,@TariffClassID	INT
				,@IsEmbassyCustomer	BIT
				,@EmbassyCode	Varchar(50)
				,@PhaseId	INT
				,@ReadCodeID	INT
				,@IdentityTypeId	INT
				,@IdentityNumber	VARCHAR(100)
				,@UploadDocumentID	varchar(MAX)
				,@CertifiedBy	varchar(50)
				,@Seal1	varchar(50)
				,@Seal2	varchar(50)
				,@ApplicationProcessedBy	varchar(50)
				,@EmployeeName	varchar(100)
				,@InstalledBy 	INT
				,@AgencyId	INT
				,@IsCAPMI	BIT
				,@MeterAmount	Decimal(18,2)
				,@InitialBillingKWh	BigInt
				,@InitialReading	BigInt
				,@PresentReading	BigInt
				,@StatusText NVARCHAR(max)
				,@CreatedDate DATETIME
				,@PostalAddressID INT
				,@Bedcinfoid INT
				,@ContactName varchar(100)
				,@HouseNoPostal	varchar(50)
				,@StreetPostal Varchar(50)
				,@CityPostaL	varchar(50)
				,@AreaPostal	INT
				,@HouseNoService	varchar(50)
				,@StreetService	varchar(50)
				,@CityService	varchar(50)
				,@AreaService	INT
				,@TenentId INT
				,@ServiceAddressID INT
				,@IdentityTypeIdList	varchar(MAX)
				,@IdentityNumberList	varchar(MAX)
				,@DocumentName 	varchar(MAX)
				,@Path 	varchar(MAX)
				,@IsValid bit=1
				,@EmployeeCode INT
				,@PZipCode VARCHAR(50)
				,@SZipCode VARCHAR(50)
				,@AccountTypeId INT
				,@ClusterCategoryId INT
				,@RouteSequenceNumber INT
				,@Length INT
				,@UDFTypeIdList	varchar(MAX)
				,@UDFValueList	varchar(MAX)
				,@IsSuccessful BIT=1
				,@MGActTypeID INT
SELECT 
				 @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')
				,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')
				,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')
				,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')
				,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')
				,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')
				,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')
				,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')
				,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')
				,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')
				,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')
				,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')
				,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')
				,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')
				,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')
				,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')
				,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')
				,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')
				,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')
				,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')
				,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')
				,@SetupDate=C.value('(SetupDate)[1]','Datetime')
				,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')
				,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')
				,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')
				,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')
				,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
				,@BookNo=C.value('(BookNo)[1]','varchar(30)')
				,@PoleID=C.value('(PoleID)[1]','INT')
				,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')
				,@TariffClassID=C.value('(TariffClassID)[1]','INT')
				,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')
				,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')
				,@PhaseId=C.value('(PhaseId)[1]','INT')
				,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')
				,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')
				,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')
				,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')
				,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')
				,@Seal1=C.value('(Seal1)[1]','varchar(50)')
				,@Seal2=C.value('(Seal2)[1]','varchar(50)')
				,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')
				,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')
				,@InstalledBy =C.value('(InstalledBy )[1]','INT')
				,@AgencyId=C.value('(AgencyId)[1]','INT')
				,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')
				,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')
				,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')
				,@InitialReading=C.value('(InitialReading)[1]','BigInt')
				,@PresentReading=C.value('(PresentReading)[1]','BigInt')
				,@HouseNoPostal	=C.value('(	HouseNoPostal	)[1]','	varchar(50)	')
				,@StreetPostal=C.value('(StreetPostal)[1]','varchar(50)')
				,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')
				,@AreaPostal=C.value('(AreaPostal)[1]','INT')
				,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')
				,@StreetService=C.value('(StreetService)[1]','varchar(50)')
				,@CityService=C.value('(CityService)[1]','varchar(50)')
				,@AreaService=C.value('(AreaService)[1]','INT')
				,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')
				,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')
				,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')
				,@Path=C.value('(Path)[1]','varchar(MAX)')
				,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')
				,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')
				,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')
				,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')
				,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')
				,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')
				,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')
				,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')
				,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')
				
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  


--||***************************TEMP RESOURCE REMOVAL****************************||--
BEGIN TRY
	DROP TABLE #DocumentList
	DROP TABLE #UDFList
	DROP TABLE #IdentityList
END TRY
BEGIN CATCH

END CATCH
--||****************************************************************************||--
--===========================REGISTRATION STEPS===========================--

--STEP 1	:	GENERATE GLOBAL ACCOUNT NO

--STEP 2	:	GENERATE ACCOUNT NO.

--STEP 3	:	ADD Customer Postal AddressDetails

--STEP 4	:	ADD CustomerTenentDetails.

--STEP 5	:	ADD Customer Details

--STEP 6	:	ADD Customer Procedural Details

--STEP 7	:	ADD Customer Identity Details

--STEP 8	:	ADD Application Process Details

--STEP 9	:	ADD Application Process Person Details

--STEP 10	:	ADD Tbl_CustomerActiveDetails

--STEP 11	:	ADD Tbl_CustomerDocuments

--STEP 12	:	ADD Tbl_USerDefinedValueDetails

--STEP 13	:	ADD Tbl_GovtCustomers

--STEP 14	:	ADD Tbl_Paidmeterdetails

--===========================******************===========================--


--===========================MEMBERS STARTS===========================--+
SET @StatusText='SUCCESS!'

--GENERATE GLOBAL ACCOUNT NUMBER
--||***************************STEP 1****************************||--
BEGIN TRY
    SET @GlobalAccountNumber=CUSTOMERS.fn_GlobalAccountNumberGenerate();
END TRY
BEGIN CATCH
	SET @StatusText='Cannot generate global account no.';
	SET @IsValid=0;
END CATCH

--||***************************STEP 2****************************||--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		DECLARE @BU_ID VARCHAR(50)
				,@SU_ID VARCHAR(50)
				,@ServiceCenterId VARCHAR(50)
		SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId 
		FROM Tbl_BookNumbers BN 
		JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId
		JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
		JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
		JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
		 WHERE BookNo=@BookNo
		 
		SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate 
		--SET @AccountNo='00000001'
	END TRY
	BEGIN CATCH
	SET @IsValid=0;
		SET @StatusText='Cannot generate account no.';
	END CATCH
END
--GET DATE FROM UDF
SET @CreatedDate=dbo.fn_GetCurrentDateTime();

--==========================Preparation of Identity details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
	CREATE TABLE #IdentityList(
								value1 varchar(MAX)
								,value2 varchar(MAX)
								,GlobalAccountNumber VARCHAR(10)
								,CreatedDate DATETIME
								,CreatedBy VARCHAR(50)
								)
	INSERT INTO #IdentityList(
								value1
								,value2
								,GlobalAccountNumber
								,CreatedDate
								,CreatedBy
							)
	select	Value1
			,Value2
			,@GlobalAccountNumber AS GlobalAccountNumber
			,@CreatedDate  AS CreatedDate
			,@CreatedBy AS CreatedBy
	from  [dbo].[fn_splitTwo](@IdentityTypeIdList,@IdentityNumberList,'|')
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare Identity list.';
	END CATCH
END
--select * from  [dbo].[fn_splitTwo]('2|1|3|','456456|24245|645646|','|')
--==========================Preparation of Document details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@DocumentName)+ LEN(@Path)		
		CREATE TABLE #DocumentList(
									value1 varchar(MAX)
									,value2 varchar(MAX)
									,GlobalAccountNumber VARCHAR(10)
									,ActiveStatusId INT
									,CreatedDate DATETIME
									,CreatedBy VARCHAR(50)
							)
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #DocumentList(
										GlobalAccountNumber
										,value1
										,value2
										,ActiveStatusId
										,CreatedBy
										,CreatedDate
									)
			select	@GlobalAccountNumber AS GlobalAccountNumber
					,Value1
					,Value2
					,1
					,@CreatedBy AS CreatedBy
					,@CreatedDate  AS CreatedDate
			from  [dbo].[fn_splitTwo](@DocumentName,@Path,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare document list.';
	END CATCH
END
--==========================Preparation of User Define Controls details table============================--
	
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@UDFTypeIdList)+ LEN(@UDFValueList)	
		CREATE TABLE #UDFList(
									UDCId	INT
									,Value	VARCHAR(150)
									,GlobalAccountNumber VARCHAR(10)
							)	
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #UDFList(										
									UDCId
									,Value
									,GlobalAccountNumber
									)
			select	
									Value1
									,Value2
									,@GlobalAccountNumber AS GlobalAccountNumber
			from  [dbo].[fn_splitTwo](@UDFTypeIdList,@UDFValueList,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare User Define Control list.';
	END CATCH
END
--===========================MEMBERS ENDS===========================--

--===========================REGISTRATION STARTS===========================--
DECLARE @intErrorCode INT
IF(@IsValid=1)
BEGIN
BEGIN TRY
	BEGIN TRAN
		IF(@GlobalAccountNumber IS NOT NULL)
			BEGIN
				IF(@AccountNo IS NOT NULL)
					BEGIN
		--||***************************STEP 3****************************||--
				SET @StatusText='Postal Address';
				INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoPostal
														,@StreetPostal
														,@CityPostaL
														,@AreaPostal
														,@PZipCode
														,0
														,@CreatedDate
														,@CreatedBy
														)
				SET @PostalAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				
				IF(@IsSameAsService=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoService
														,@StreetService
														,@CityService
														,@AreaService
														,@SZipCode
														,1
														,@CreatedDate
														,@CreatedBy
														)
				SET @ServiceAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				END
				
				--||***************************STEP 4****************************||--
				SET @StatusText='Tenent Address';
				IF(@IsSameAsTenent=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerTenentDetails(
														GlobalAccountNumber
														,Title
														,FirstName
														,MiddleName
														,LastName
														,PhoneNumber
														,AlternatePhoneNumber
														,EmailID
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@TitleTanent
														,@FirstNameTanent
														,@MiddleNameTanent
														,@LastNameTanent
														,@PhoneNumberTanent
														,@AlternatePhoneNumberTanent
														,@EmailIdTanent
														,@CreatedDate
														,@CreatedBy

														)
					SET @TenentId=SCOPE_IDENTITY();--(SELECT TOP 1 TenentId FROM CUSTOMERS.Tbl_CustomerTenentDetails)
				END
				--||***************************STEP 5****************************||--
				SET @StatusText='Customer Details';
					INSERT INTO CUSTOMERS.Tbl_CustomersDetail (
														GlobalAccountNumber
														,AccountNo
														,Title
														,FirstName
														,MiddleName
														,LastName
														,KnownAs
														,EmailId
														,HomeContactNumber
														,BusinessContactNumber
														,OtherContactNumber
														,IsSameAsService
														,ServiceAddressID
														,PostalAddressID
														,IsSameAsTenent
														,DocumentNo
														,ApplicationDate
														,ConnectionDate
														,SetupDate
														,ActiveStatusId
														,IsBEDCEmployee
														,EmployeeCode
														,IsVIPCustomer
														,TenentId
														,OldAccountNo
														,CreatedDate
														,CreatedBy
														)
												values
														(
														@GlobalAccountNumber
														,@AccountNo
														,@TitleLandlord
														,@FirstNameLandlord
														,@MiddleNameLandlord
														,@LastNameLandlord
														,@KnownAs
														,@EmailIdLandlord
														,@HomeContactNumberLandlord
														,@BusinessPhoneNumberLandlord
														,@OtherPhoneNumberLandlord
														,@IsSameAsService 
														,@ServiceAddressID
														,@PostalAddressID
														,@IsSameAsTenent
														,@DocumentNo
														,@ApplicationDate
														,@ConnectionDate
														,@SetupDate
														,1
														,@IsBEDCEmployee
														,@EmployeeCode
														,@IsVIPCustomer
														,@TenentId
														,@OldAccountNo
														,@CreatedDate
														,@CreatedBy
												)
					--||***************************STEP 6****************************||--
					SET @StatusText='Customer Procedural Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerProceduralDetails(
														 GlobalAccountNumber
														,CustomerTypeId
														,AccountTypeId
														,PoleID
														,MeterNumber
														,TariffClassID
														,IsEmbassyCustomer
														,EmbassyCode
														,PhaseId
														,ReadCodeID
														,BookNo
														,ClusterCategoryId
														,RouteSequenceNumber
														,CreatedDate
														,CreatedBy
														,ActiveStatusID
														)
												VALUES	(
														@GlobalAccountNumber
														,@CustomerTypeId
														,@AccountTypeId
														,@PoleID
														,@MeterNumber
														,@TariffClassID
														,@IsEmbassyCustomer
														,@EmbassyCode
														,@PhaseId
														,@ReadCodeID
														,@BookNo
														,@ClusterCategoryId
														,@RouteSequenceNumber
														,@CreatedDate
														,@CreatedBy
														,1
														)
					--||***************************STEP 7****************************||--
					SET @StatusText='Customer identity Details';
					IF EXISTS(SELECT 0 FROM #IdentityList)
					BEGIN
						INSERT INTO CUSTOMERS.Tbl_CustomerIdentityDetails(
															IdentityTypeId
															,IdentityNumber
															,GlobalAccountNumber
															,CreatedDate
															,CreatedBy
															)
						SELECT								*
						FROM #IdentityList
					END
					--||***************************STEP 8****************************||--
					SET @StatusText='Application Process Details';
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessDetails(
														 GlobalAccountNumber
														,CertifiedBy
														,Seal1
														,Seal2
														,ApplicationProcessedBy
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@CertifiedBy
														,@Seal1
														,@Seal2
														,@ApplicationProcessedBy
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 9****************************||--
					SET @StatusText='Application Process Person Details';
					SET @Bedcinfoid=SCOPE_IDENTITY();--(SELECT TOP 1 Bedcinfoid FROM CUSTOMERS.Tbl_ApplicationProcessDetails ORDER BY Bedcinfoid DESC)
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessPersonDetails(
														GlobalAccountNumber
														,Bedcinfoid
														--,EmployeeName
														,InstalledBy
														,AgencyId
														,ContactName
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@Bedcinfoid
														--,@EmployeeName
														,@InstalledBy
														,@AgencyId
														,@ContactName
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 10****************************||--
					SET @StatusText='Customer Active Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerActiveDetails(
														GlobalAccountNumber
														,IsCAPMI
														,MeterAmount
														,InitialBillingKWh
														,PresentReading
														,InitialReading--Faiz-ID103
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@IsCAPMI
														,@MeterAmount
														,@InitialBillingKWh
														,@PresentReading
														,@InitialReading--Faiz-ID103
														,@CreatedDate
														,@CreatedBy
														)
					--||***************************STEP 11****************************||--
					SET @StatusText='Document Details';
					if Exists(SELECT 0 FROM #DocumentList)
					BEGIN
						INSERT INTO Tbl_CustomerDocuments	
															(
															GlobalAccountNo
															,DocumentName
															,[Path]
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
															)
										SELECT				
															GlobalAccountNumber
															,value1
															,value2
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
										FROM  #DocumentList

				 						DROP TABLE #DocumentList
					 END
					--||***************************STEP 12****************************||--
					SET @StatusText='User Define List';
					if Exists(SELECT 0 FROM #UDFList)
					BEGIN
						INSERT INTO Tbl_USerDefinedValueDetails
															(
															AccountNo
															,UDCId
															,Value 
															)
										SELECT				
															GlobalAccountNumber
															,UDCId
															,Value
										FROM #UDFList

					 					
					 END
					 --||***************************STEP 13****************************||--
					SET @StatusText='Tbl_GovtCustomers';
					IF(@MGActTypeID > 0)
					BEGIN
						INSERT INTO Tbl_GovtCustomers
														(
														GlobalAccountNumber
														,MGActTypeID
														)
									values				(
														@GlobalAccountNumber
														,@MGActTypeID			
														)
					END
					 --||***************************STEP 14****************************||-- (Added By - Padmini(25th Feb 2015))
					SET @StatusText='Tbl_Paidmeterdetails';
					IF(@IsCAPMI=1)
					BEGIN
						INSERT INTO Tbl_PaidMeterDetails
														(
														AccountNo
														,MeterNo
														,MeterTypeId
														,MeterCost
														,ActiveStatusId
														,MeterAssignedDate
														,CreatedDate
														,CreatedBy
														)
									values
														(
														@GlobalAccountNumber
														,@MeterNumber
														,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)
														,@MeterAmount
														,1
														,@ConnectionDate
														,@CreatedDate
														,@CreatedBy
														)					
					END
				SET @StatusText	=''
			END
				ELSE
					BEGIN
						SET @GlobalAccountNumber = ''
						SET @IsSuccessful =0
						SET @StatusText='Problem in account number genrate.';
					END
			END
		ELSE
			BEGIN 
				SET @GlobalAccountNumber = ''
				SET @IsSuccessful =0
				SET @StatusText='Problem in global account number genrate.';
			END
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @GlobalAccountNumber = ''
	SET @IsSuccessful =0
END CATCH
END

SELECT 
	@IsSuccessful AS IsSuccessful
	,@StatusText AS StatusText 
	,@GlobalAccountNumber AS GolbalAccountNumber			
	FOR XML PATH('CustomerRegistrationBE'),TYPE
--===========================REGISTRATION ENDS===========================--
END
GO
--------------------------------------------------------------------------------------------------------


GO
/****** Object:  Trigger [dbo].[TR_InsertAuditServiceCenter]    Script Date: 04/06/2015 18:58:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceCenter into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
ALTER TRIGGER [dbo].[TR_InsertAuditServiceCenter]   ON  [dbo].[Tbl_ServiceCenter]
   INSTEAD OF UPDATE
AS 
BEGIN

DECLARE  
     @ServiceCenterId   varchar(20)   ,
	 @ServiceCenterName   varchar(300)  ,
	 @Notes   varchar(max)  ,
	 @SU_ID   varchar(20)  ,
	 @SCCode   varchar(10)  ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ActiveStatusId   int   ,
	 @ModifiedBy   varchar(50) ;
	 
	 SELECT
	  @ServiceCenterId =I.ServiceCenterId
	  ,@ServiceCenterName=I.ServiceCenterName
	  ,@Notes=I.Notes
	  ,@SU_ID=I.SU_ID
	  ,@ActiveStatusId=I.ActiveStatusId
	  ,@ModifiedBy=I.ModifiedBy
	  ,@SCCode=I.SCCode
	  ,@Address1=I.Address1
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 FROM inserted I;
	 
	 INSERT INTO Tbl_Audit__ServiceCenter
	 (
		ServiceCenterId
		,ServiceCenterName
		,Notes
		,SU_ID
		,ActiveStatusId
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate
		,SCCode
		,Address1
		,Address2
		,City
		,ZIP
	 )
	 SELECT [ServiceCenterId]
      ,[ServiceCenterName]
      ,[Notes]
      ,[SU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SCCode]
      ,Address1
      ,Address2
      ,City
      ,ZIP
       FROM Tbl_ServiceCenter WHERE ServiceCenterId=@ServiceCenterId;
	 
	 UPDATE Tbl_ServiceCenter SET
	 [ServiceCenterName]=@ServiceCenterName
      ,[Notes]=@Notes
      ,[SU_ID]=@SU_ID
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifiedBy]=@ModifiedBy
      ,[SCCode]=@SCCode
      ,[Address1]=@Address1
      ,Address2=@Address2
      ,City=@City
      ,ZIP=@ZIP
      ,[ModifiedDate]=dbo.fn_GetCurrentDateTime() 
     WHERE ServiceCenterId=@ServiceCenterId  
      
END
GO
---------------------------------------------------------------------------------------------
GO
-- =============================================    
-- Author:  Bhimaraju Vanka    
-- Create date: 27-12-2014    
-- Description: Purpose is To get List Of Marketers for Bind Grid    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetMarketers]    
(    
 @xmlDoc Xml=null    
)    
AS    
BEGIN    
     
 DECLARE @BU_ID VARCHAR(50)    
 SELECT @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('MarketersBe') as T(C)    
 SELECT    
 (    
  SELECT   MarketerId    
    ,dbo.fn_GetMarketersFullName(MarketerId) AS Marketer    
  FROM Tbl_Marketers    
  --WHERE ActiveStatusId IN(1,2)  
  WHERE ActiveStatusId =1   --Faiz-ID103
  AND BU_ID=@BU_ID OR @BU_ID=''   
  FOR XML PATH('MarketersBe'),TYPE    
 )    
 FOR XML PATH(''),ROOT('MarketersBeInfoByXml')    
END  
GO
------------------------------------------------------------------------------------------
GO
-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jan-2015>    
-- Description: <Retriving Regions records form Tbl_MRegion>    
-- MODIFIED BY:  <NEERAJ KANOJIYA-ID103>    
-- Create date: <03-APRIL-2015>    
-- Description: <LEFT JOIN ADDED>    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetRegions]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
  DECLARE  @PageNo INT    
    ,@PageSize INT    
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
  FROM @XmlDoc.nodes('RegionsBE') AS T(C)    
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY R.ActiveStatusId ASC , R.CreatedDate DESC ) AS RowNumber  
     ,R.RegionId    
     ,R.StateCode   
     ,S.StateName  
     ,RegionName  
     ,ActiveStatusId    
   FROM Tbl_MRegion R  
   LEFT JOIN Tbl_States S  On R.StateCode=S.StateCode 
   WHERE ActiveStatusId IN(1,2) and S.IsActive=1 
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('RegionsBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('RegionsBEInfoByXml')     
END
GO
------------------------------------------------------------------------------
GO
-- =============================================      
-- Author:  <Bhimaraju Vanka>      
-- Create date: <20-FEB-2014>      
-- Description: <Purpose is To get List Of BusinessUnits>    
  
-- Updated By  : Faiz - ID103  
-- Updated Date: 08-01-20185    
-- Description: Added feilds for getting address,city, zip and Region  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBusinessUnitsList]      
(      
 @XmlDoc Xml      
)      
AS      
BEGIN      
      
 DECLARE  @PageNo INT      
   ,@PageSize INT        
          
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')      
    ,@PageSize = C.value('(PageSize)[1]','INT')       
           
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
        
  ;WITH PagedResults AS      
  (      
  SELECT      
      ROW_NUMBER() OVER(ORDER BY BU.ActiveStatusId ASC , BU.CREATEDDATE DESC ) AS RowNumber      
     ,BU.BU_ID      
     ,BU.BusinessUnitName      
     ,BU.Notes      
     ,BU.CreatedDate    
     ,S.StateCode      
     ,BU.RegionId  
     ,BU.ActiveStatusId    
     ,BUCode    
     ,BU.Address1  
     ,BU.Address2  
     ,BU.City  
     ,BU.ZIP  
   FROM Tbl_BussinessUnits BU      
   JOIN Tbl_States S ON S.StateCode=BU.StateCode 
   Join Tbl_MRegion R on R.RegionId=BU.RegionId and R.ActiveStatusId=1 --FaizID103
   WHERE S.IsActive=1    
  )      
      
    
        
   SELECT       
    (      
   SELECT  BU_ID      
     ,BusinessUnitName      
     ,CreatedDate      
     ,(CASE ISNULL(Notes,'') WHEN ''      
        THEN '---'            
        ELSE Notes      
        END) as Notes      
     ,(Select COUNT(0) from PagedResults) as TotalRecords      
     ,ActiveStatusId     
     ,StateCode      
     ,RegionId  
     ,(SELECT RegionName FROM Tbl_MRegion WHERE RegionId=p.RegionId) as RegionName  
     ,(SELECT StateName FROM Tbl_States WHERE StateCode=p.StateCode) AS StateName    
    ,ISNULL(BUCode,'--') AS BUCode    
     ,RowNumber     
     ,Address1  
     ,Address2  
     ,City  
     ,ZIP   
    FROM PagedResults p      
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
    FOR XML PATH('MastersBE'),TYPE      
  )      
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')       
 END     
 GO
 ------------------------------------------------------------------------------------------------  
 GO
-- =============================================    
-- Author:  <Bhimaraju Vanka>    
-- Create date: <13-FEB-2014>    
-- Description: <Delete State Row By StateCode in Tbl_States>   
-- Modified By : Bhargav G  
-- Modified Date : 17th Feb, 2015  
-- Modified Description : Restricting De activation if any customer is associated to that particular state   
-- =============================================    
ALTER PROCEDURE [dbo].[USP_DeleteState]     
(    
 @XmlDoc XML    
)    
AS    
BEGIN    
 DECLARE @StateCode VARCHAR(20)    
  ,@IsActive BIT    
  ,@ModifiedBy VARCHAR(50)    
     
 SELECT @StateCode = C.value('(StateCode)[1]','VARCHAR(20)')    
    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')    
    ,@IsActive = C.value('(IsActive)[1]','BIT')    
 FROM @XmlDoc.nodes('MastersBE') AS T(C)    
     
    IF NOT EXISTS(SELECT 0 FROM Tbl_States S  
     JOIN Tbl_BussinessUnits BU ON BU.StateCode = S.StateCode  
     JOIN Tbl_ServiceUnits SU ON SU.BU_ID = BU.BU_ID  
     JOIN Tbl_ServiceCenter SC ON SC.SU_ID = SU.SU_ID  
     JOIN Tbl_Cycles CY ON CY.ServiceCenterId = SC.ServiceCenterId  
     JOIN Tbl_BookNumbers BN ON BN.CycleId = CY.CycleId  
     JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.BookNo = BN.BookNo  
    WHERE S.StateCode = @StateCode)  
  BEGIN    
   UPDATE Tbl_States     
   SET IsActive = @IsActive,    
   ModifiedBy = @ModifiedBy,    
   ModifiedDate = DATEADD(SECOND,19800,GETUTCDATE())    
   WHERE StateCode = @StateCode  
     
   SELECT 1 AS IsSuccess   
   FOR XML PATH('MastersBE')    
  END  
 ELSE  
  BEGIN  
   SELECT   
    COUNT(0) AS [Count]  
    ,0 AS IsSuccess  
   FROM Tbl_States S  
   JOIN Tbl_BussinessUnits BU ON BU.StateCode = S.StateCode  
   JOIN Tbl_ServiceUnits SU ON SU.BU_ID = BU.BU_ID  
   JOIN Tbl_ServiceCenter SC ON SC.SU_ID = SU.SU_ID  
   JOIN Tbl_Cycles CY ON CY.ServiceCenterId = SC.ServiceCenterId  
   JOIN Tbl_BookNumbers BN ON BN.CycleId = CY.CycleId  
   JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.BookNo = BN.BookNo  
   AND S.StateCode = @StateCode  
   FOR XML PATH('MastersBE')  
  END      
END  
GO
--------------------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 03-MAR-2014  
-- Description: To Get Roles Details  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetRoles]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
 DECLARE @RoleId INT  
 SELECT @RoleId=C.value('(RoleId)[1]','INT')  
 FROM @XmlDoc.nodes('UserManagementBE') AS T(C)  
  
 SELECT (  
  SELECT * FROM Tbl_MRoles   
  WHERE (RoleId=@RoleId OR @RoleId='')  
  AND RoleId !=12 AND IsActive=1 -- Faiz-ID103 
  FOR XML PATH('UserManagementBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
END  
GO
-------------------------------------------------------------