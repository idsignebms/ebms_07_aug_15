
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	SATYA
-- Create date:  07-04-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
CREATE FUNCTION  [dbo].[fn_GetAverageReading_Save]
(
 @GlobalAccountNumber VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
		 
	 
		Declare @CustomerAverage decimal(18,2)
		Declare	 @TotalReacords int
		 

		select Top 1 @CustomerAverage=AverageReading from   Tbl_CustomerReadings where GlobalAccountNumber=@GlobalAccountNumber
		order by CustomerReadingId desc 
		 

		IF	@CustomerAverage IS NULL
		SET @CustomerAverage =@usage
		ELSE

		SET	 @CustomerAverage = (@CustomerAverage+@usage)/2
		
		
	RETURN @CustomerAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_Update]    Script Date: 04/08/2015 01:01:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	SATYA
-- Create date:  07-04-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
CREATE FUNCTION  [dbo].[fn_GetAverageReading_Update]
(
 @GlobalAccountNumber VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
		Declare @CustomerAverage decimal(18,2)
		Declare	 @TotalReacords int

		;WITH OrderedCinemas AS
		(
		   SELECT 
			   AverageReading ,
			   ROW_NUMBER() OVER(ORDER BY CustomerReadingId DESC) AS 'RowNum'
		   FROM Tbl_CustomerReadings
		)
		SELECT 
			 @CustomerAverage=AverageReading
		     
		     
		FROM OrderedCinemas
		WHERE RowNum = 2

		IF	@CustomerAverage IS NULL
		SET @CustomerAverage =@usage
		ELSE

		SET	 @CustomerAverage = (@CustomerAverage+@usage)/2
		
		
	RETURN @CustomerAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 04/08/2015 01:01:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
 
 
Declare @NewAverage decimal(18,2 )

SELECT	Top 1    @NewAverage=AverageReading  
       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
	 order by CustomerReadingId DESC

 ;WITH OrderedCinemas AS
(
   SELECT	Top 2
         AverageReading ,
       ROW_NUMBER() OVER(ORDER BY CustomerReadingId DESC) AS 'RowNum'
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
)
SELECT 
     @NewAverage=AverageReading
FROM OrderedCinemas
WHERE RowNum = 2

IF	@NewAverage IS NULL
SET @NewAverage =@usage
ELSE

SET		 @NewAverage = (@NewAverage+@usage)/2
			
	RETURN @NewAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAverageReading_Updatting_New]    Script Date: 04/08/2015 01:01:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author   : Suresh Kumar Dasi  
-- Create date: 06-05-2014  
-- Description: To Customer Average Reading Calculations Based on Reading Date  
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0005',25)  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerAverageReading_Updatting_New]  
(  
 @GlobalAccountNumber VARCHAR(50)  
 ,@Usage NUMERIC  
 
)  
RETURNS VARCHAR(50)  
AS  
BEGIN  
  
  Declare @PreviousAverageUsage decimal(18,2)
  Declare @LatestAverage decimal(18,2) 
 

;WITH OrderedCinemas AS
(
   SELECT	TOP 2
       AverageReading, Usage, 
       ROW_NUMBER() OVER(ORDER BY CustomerReadingId DESC) AS 'RowNum'
   FROM Tbl_CustomerReadings
   where GlobalAccountNumber='0000057200'
)
SELECT 
   @PreviousAverageUsage=AverageReading 
FROM OrderedCinemas
WHERE RowNum = 2


if @PreviousAverageUsage IS NULL

BEGIN
  SET @LatestAverage=@Usage
   
END

ELSE
BEGIN
SET @LatestAverage=	 (@PreviousAverageUsage+100)/2

END

 

     
 RETURN @LatestAverage  
  
END

GO


