
GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill]    Script Date: 04/08/2015 01:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- =============================================
CREATE PROCEDURE [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 
 
------    No Bill 
-- No Power		  Book Disable No Powser
-- Hold Customer , Close Customer ,Active
--UDV_IsCustomerExists
-- No Power Type is "1"


---------Adjustment -------------------	----------------------------------

-- Except Close Customer 
select * from Tbl_MCustomerStatus



 
	 
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	
	--Modified By Satya Sir 
		
	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	  	
	Select DisableTypeId from Tbl_BillingDisabledBooks    where BookNo =@BookNo  

	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
					 
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 or @CustomerActiveStatusID=3 
							BEGIN
							SELECT 1 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess and Active  Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
						
					 
				
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_Billing]    Script Date: 04/08/2015 01:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- =============================================
CREATE PROCEDURE [dbo].[USP_IsCustomerExistsInBU_Billing] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 


	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	Declare @NoPower int
	
 

	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo

	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	 
	  
	Select @NoPower=case when DisableTypeId=2 then 1 else 0 end from 
	Tbl_BillingDisabledBooks    where BookNo =@BookNo	 and IsActive=1   
	PRINT   @CustomerActiveStatusID
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
		SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
						IF	 @CustomerActiveStatusID  = 1   
							BEGIN
								IF  isnull(@NoPower,0)!=1 
									SELECT 1 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								ELSE
									 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess     Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetConfigurationSettingsMenu]    Script Date: 04/08/2015 01:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author: Karteek
-- Create date: 7 Apr 2015 
-- To get the configuration settings menu for left side
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_GetConfigurationSettingsMenu]
(    
	@XmlDoc XML    
) 
AS
BEGIN     
	SET NOCOUNT ON; 
	   
	DECLARE @Role_Id INT 
		,@PageName VARCHAR(50)
		,@MenuId INT
		
	SELECT @Role_Id = C.value('(Role_Id)[1]','INT')   
		,@PageName = C.value('(PageName)[1]','varchar(50)')
	FROM @XmlDoc.nodes('SideMenusBE') AS T(C)  
	
	SELECT @MenuId = MenuId FROM Tbl_Menus WHERE Name = @PageName AND IsActive = 1
	
	SELECT    
	( 
		SELECT 
			 M.MenuId AS Main_Menu_Text
			,M.Name AS SubMenu_Text
			,M.[Path] AS Path
		FROM Tbl_Menus M
		INNER JOIN Tbl_NewPagePermissions N ON N.MenuId = M.MenuId 
			AND M.IsActive = 1 AND N.IsActive = 1
			AND N.Role_Id = @Role_Id
			AND M.ReferenceMenuId = @MenuId
		FOR XML PATH('SideMenu'),TYPE    
	)    
	FOR XML PATH(''),ROOT('SideMenuBeInfoByXml')   
	
END   
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/08/2015 01:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		@ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@Count=C.value('(TotalReadings)[1]','INT')
		,@Avg=C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage=CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')

FROM @XmlDoc.nodes('BillingBE') AS T(C)


Declare @ReadId int

select @ReadId=CustomerReadingId from Tbl_CustomerReadings
where GlobalAccountNumber=@AccNum

 
	
UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
   ,Usage = @Usage
   ,ModifiedBy = @Modified
   ,IsTamper=@IsTamper
,MeterReadingFrom=@MeterReadingFrom
,ModifiedDate = GETDATE()
,AverageReading = (SELECT dbo.fn_GetAverageReading_Update
(GlobalAccountNumber,@Usage))
FROM Tbl_CustomerReadings CR	
 WHERE  CustomerReadingId=@ReadId 


SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillPreviousReading_New]    Script Date: 04/08/2015 01:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBillPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		@ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@Count=C.value('(TotalReadings)[1]','INT')
		,@Avg=C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage=CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')

FROM @XmlDoc.nodes('BillingBE') AS T(C)


Declare @ReadId int

select @ReadId=CustomerReadingId from Tbl_CustomerReadings
where GlobalAccountNumber=@AccNum

SET @CustUn=(SELECT TOP 1 GlobalAccountNumber FROM
 CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber=@AccNum)
	
UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
   ,Usage = @Usage
   ,ModifiedBy = @Modified
   ,IsTamper=@IsTamper
,MeterReadingFrom=@MeterReadingFrom
,ModifiedDate = GETDATE()
,AverageReading = (SELECT dbo.fn_GetCustomerAverageReading(GlobalAccountNumber,CONVERT(NUMERIC,@Current),@ReadDate))

FROM Tbl_CustomerReadings CR	
 WHERE  CustomerReadingId=@ReadId 


SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationDetailsByCycle_Rajaiah]    Script Date: 04/08/2015 01:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================                      
-- Author  : NEERAJ KANOJIYA                    
-- Create date  : 25 July 2014                      
-- Description  : THIS PROCEDURE WILL GET ESTIMATION DETAILS OF READING  
-- Modified By : Padmini
-- Modified Date : 26-12-2014                 
-- =======================================================================                      
ALTER PROCEDURE [dbo].[USP_GetEstimationDetailsByCycle_Rajaiah]                      
(                      
	@XmlDoc xml                      
)                      
AS                      
BEGIN 
		 DECLARE @Year INT      
				 ,@Month int                
				 ,@Cycle Varchar(50)
				 ,@PageNo INT
				 ,@PageSize INT
		               
		  SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @Cycle = C.value('(Cycle)[1]','VARCHAR(50)'),
		   @PageNo = C.value('(PageNo)[1]','INT'),
		   @PageSize = C.value('(PageSize)[1]','INT')
		   FROM @XmlDoc.nodes('EstimationBE') as T(C)         
		  
  --SELECT   @Year = 2015,   @Month =2,   @Cycle = 'BEDC_C_0013',   @PageNo = 1,   @PageSize = 100000
  
  		   ;wITH Result as      
		   (      
				   SELECT   --ROW_NUMBER() OVER(ORDER BY ClassID ASC ) AS RowNumber    
				  -- ,
				   ClassID--Tariff ID      
				   ,TS.ClassName--Tariff Name      
				   , COUNT(CustomerProcedureID)  AS EstimatedCustomers
				   --,(SELECT COUNT(0) FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CDD JOIN Tbl_BookNumbers BCC ON CDD.BookNo = BCC.BookNo       
					  --WHERE TariffClassID  = ClassID AND BCC.CycleId = @Cycle) AS EstimatedCustomers      
				   ,dbo.fn_GetMonthlyUsage_ByTariff(@Cycle,ClassID,@Year,@Month) AS AvgReadingPerCust      
				   ,(select SUM(EnergytoCalculate) from Tbl_EstimationSettings ES WHERE CycleId=@Cycle AND BillingYear=@Year AND BillingMonth=@Month AND ES.TariffId= TS.ClassID AND ES.ActiveStatusId=1) AS AvgUsage      
				   --,dbo.fn_GetMonthlyUsage_ByTariff(@Cycle,ClassID,@Year,@Month)+(select SUM(EnergytoCaluclate) from Tbl_EstimationSettings WHERE CycleId=@Cycle AND BiingYear=@Year AND BillingMonth=@Month AND TariffId=ClassID) AS Total      
				  FROM Tbl_MTariffClasses TS   
				  INNER JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD ON TS.ClassID=CD.TariffClassID  
				  INNER JOIN Tbl_BookNumbers BN ON CD.BookNo=BN.BookNo 
				  WHERE TS.RefClassID IS NOT NULL AND  BN.CycleId=@Cycle
				  AND TS.ClassID = CD.TariffClassID 
				  GROUP BY ClassID ,TS.ClassName
				   
		   )      
		         
		        SELECT TS.ClassID      
								   ,TS.ClassName      
								   ,ISNULL(B.EstimatedCustomers,0) AS  EstimatedCustomers
								   ,ISNULL(B.AvgReadingPerCust,0) AS AvgReadingPerCust
								   ,ISNULL(B.AvgUsage,0) AS AvgUsage
								   ,ISNULL(B.Total,0) AS Total
								   ,ROW_NUMBER() OVER(ORDER BY TS.ClassID ASC ) AS RowNumber         
								  INTO #tblResults --,B.TotalRecords
							FROM Tbl_MTariffClasses TS   LEFT JOIN 
							( 
								  SELECT  ClassID      
								   ,ClassName      
								   ,EstimatedCustomers      
								   ,AvgReadingPerCust      
								   ,AvgUsage      
								   ,(ISNULL(AvgUsage,0) + ISNULL(AvgReadingPerCust,0)) AS Total
								 --  ,RowNumber
								   --,(Select COUNT(0) from Result) as TotalRecords
								  FROM Result      
								  
						   )B ON B.ClassID=TS.ClassID 
		  SELECT      
		  (               
					SELECT ClassID      
								,ClassName      
								,EstimatedCustomers
								,AvgReadingPerCust
								,AvgUsage
								,Total
								,RowNumber         
								,(Select COUNT(0) from #tblResults) as TotalRecords
					FROM #tblResults
				    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize                      
					 FOR XML PATH('EstimationDetails'),TYPE                                        
			)      
			FOR XML PATH(''),ROOT('EstimationInfoByXml')          
			DROP TABLE #tblResults
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForBillGen]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 25-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- Create date  : 8-APRIL-2015                    
 -- Description  : REMOVE ACTIVE STATUS ID FILTER
 
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForBillGen]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID  
				  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                   
				  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo       
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber    
				  ,A.OldAccountNo                 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful  
				  ,A.ServiceUnitName 
				  ,A.ServiceCenterName
				  ,A.CycleName
				  ,A.BookNo               
		  FROM [UDV_CustomerDescription] AS A    
		  LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				--AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END       
 --------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_Billing]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_Billing] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 


	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	Declare @NoPower int
	
 

	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo

	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	 
	  
	Select @NoPower=case when DisableTypeId=2 then 1 else 0 end from 
	Tbl_BillingDisabledBooks    where BookNo =@BookNo	 and IsActive=1   
	PRINT   @CustomerActiveStatusID
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
		SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
						IF	 @CustomerActiveStatusID  = 1   
							BEGIN
								IF  isnull(@NoPower,0)!=1 
									SELECT 1 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								ELSE
									 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess     Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Satya.K>
-- Create date: <10-Mar-2015>
-- Description:	<The purpose of this procedure is to generate the bills>
-- =============================================

--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		     
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
		 
		    
		   
		 	 
			SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
			,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
			@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
			,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
			,@OpeningBalance=isnull(OpeningBalance,0)
			,@CustomerFullName=CustomerFullName
			,@BusinessUnitName =BusinessUnitName
			,@TariffName =TariffName
			--,@ReadDate =
			--,@Multiplier  
			,@Service_HouseNo =ServiceHouseNo
			,@Service_Street =ServiceStreet
			,@Service_City =ServiceCity
			,@ServiceZipCode  =ServiceZipCode
			,@Postal_HouseNo =ServiceHouseNo
			,@Postal_Street =Postal_StreetName
			,@Postal_City  =Postal_City
			,@Postal_ZipCode  =Postal_ZipCode
			,@Postal_LandMark =Postal_LandMark
			,@Service_LandMark =Service_LandMark
			,@OldAccountNumber=OldAccountNo
			FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
			
			 
			IF	 isnull(@ActiveStatusId,0) != 1    -- In Active Customer
				BREAK;
			 select * from Tbl_MActiveStatusDetails
	 
			 
			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
			BEGIN
				
				SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
					,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
				FROM Tbl_CustomerBills(NOLOCK)  
				WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
				DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
				
				DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
				SELECT @PaidAmount=SUM(PaidAmount)
				FROM Tbl_CustomerBillPayments(NOLOCK)  
				WHERE BillNo=@CusotmerPreviousBillNo
				
				
				 
				
			
				IF @PaidAmount IS NOT NULL
				BEGIN
									
					SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
					UPDATE Tbl_CustomerBillPayments
					SET BillNo=NULL
					WHERE BillNo=@RegenCustomerBillId
				END
				
				----------------------Update Readings as is billed =0 ----------------------------
				UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				
				
				-- Insert Paid Meter Payments
				DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				 
				update CUSTOMERS.Tbl_CustomerActiveDetails
				SET OutStandingAmount=@OutStandingAmount
				where GlobalAccountNumber=@GlobalAccountNumber
				----------------------------------------------------------------------------------
				
				
	 
				--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
				DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
				-------------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------------
				--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
				--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
				--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
				
				---------------------------------------------------------------------------------------
				--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
				---------------------------------------------------------------------------------------
			END
			
			
				IF @ReadCodeId=2 -- 
				BEGIN
					 
					 
					SELECT  @PreviousReading=PreviousReading,
							@CurrentReading = PresentReading,@Usage=Usage,
							@LastBillGenerated=LastBillGenerated,
							@PreviousBalance =Previousbalance,
							@BalanceUnits=BalanceUsage,
							@IsFromReading=IsFromReading,
							@PrevCustomerBillId=CustomerBillId,
							@PreviousBalance=PreviousBalance,
							@Multiplier=Multiplier,
							@ReadDate=ReadDate
					FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
					
					
					
					
					 
					IF @IsFromReading=1
					BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							IF @Usage<@BalanceUnits
							BEGIN
									 
								SET @ActualUsage=@Usage
								SET @Usage=0
								SET @RemaningBalanceUsage=@BalanceUnits-@Usage
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @ActualUsage=@Usage
								SET @Usage=@Usage-@BalanceUnits
								SET @RemaningBalanceUsage=0
							END
					END
					ELSE
					BEGIN
							SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
					END
				END
				ELSE --@CustomerTypeID=1 -- Direct customer
				BEGIN
					set @IsEstimatedRead =1
					 
					 
					-- Get balance usage of the customer
					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
								  @PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 
					
					IF @PreviousBalance IS NULL
					BEGIN
						SET @PreviousBalance=@OpeningBalance
					END

				
			 
					      
					SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
					SET @ReadType=1 -- Direct
					SET @ActualUsage=@Usage
				END
			IF  @ActiveStatusId =2 -- In Active Customers
			BEGIN	 
					 
				 
				SET	 @Usage=0
			END
			
					
 			
			
			
			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0
			 
			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
			DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
			
			-- =============================================
			-- If customer is disable
			 
				--SELECT @BookDisableType=DisableTypeId 
				--FROM Tbl_BillingDisabledBooks 
				--WHERE BookNo=@BookNo AND YearId=@Year 
				--		AND MonthId=@Month AND 
				--		IsActive=1 AND ApproveStatusId=2
				 
				
				--IF @BookDisableType IS NULL
				--	SET @BookDisableType=-1
				--IF @BookDisableType<>-1 
				--BEGIN 
				--	SET @FixedCharges=0
				--END
				--ELSE
				--BEGIN
				--	INSERT INTO @tblFixedCharges(ClassID ,Amount)
				--	SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
				--	SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges					
				--END 
			----------------------------------------Disable Customer Verification---------------------------------------------------------------
			 
			select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
					from Tbl_BillingDisabledBooks	  
					where BookNo=@BookNo and YearId=@Year and MonthId=@Month
				
			IF @IspartialClose IS NULL 
				BEGIN 
					SET @IspartialClose=-1
				END
			IF	@BookDisableType IS NULL
				BEGIN
					SET @BookDisableType= -1
				END
			IF  @IspartialClose =   1
				BEGIN
					
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					IF @BookDisableType = 2  -- Temparary Close
						BEGIN
								
							SET	 @FixedCharges=0
						END
					ELSE
						BEGIN
						
						
							INSERT INTO @tblFixedCharges(ClassID ,Amount)
							SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
							SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
						END
				END
						  
			
			------------------------------------------------------------------------------------------------------------------------------------
			
			
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
			BEGIN
				
				IF @GetPaidMeterBalance<@FixedCharges
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=0
					SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
					SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
				END
				ELSE
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
					SET @FixedCharges=0
				END
			END
			------------------------
			-- Caluclate tax here
			
			SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			
			 
			IF	 @PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
							WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
							WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				
				END
			
			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0
			
			set @NetArrears=@OutStandingAmount-@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			
			
			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber
				Group BY CustomerBillId       
				ORDER BY CustomerBillId DESC 
			 
			if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage
			
			if @RemainingBalanceUnits IS NULL
			 set @RemainingBalanceUnits=0
			 
			 -------------------------------------------------------------------------------------------------------
			SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

			FROM Tbl_LEnergyClassCharges 
			WHERE ClassID =@TariffId
			AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	
				FOR XML PATH(''), TYPE)
			 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
			 
			
				
				
				
			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
			
			 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			-----------------------------------------------------------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
				,TariffRates
			)        
			SELECT GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
				,BU_ID
				,SU_ID
				,ServiceCenterId
				,PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,ISNULL(@AverageUsageForNewBill,0)
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
				,@TariffCharges
			FROM @CustomerMaster C        
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL
			SET @IspartialClose=NULL
			SET @BookDisableType =NULL
			SET @TariffCharges=NULL
			SET @CusotmerPreviousBillNo=NULL
			
			-----------------------------------------------------------------------------------------
			
			--While Loop continue checking
			IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
				BREAK        
			ELSE        
			BEGIN     
			   --BREAK
			   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
			   IF(@GlobalAccountNumber IS NULL) break;        
				Continue        
			END
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		--SELECT * From dbo.fn_GetCustomerBillsForPrint(@CycleId,@Month,@Year)  
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 
 
------    No Bill 
-- No Power		  Book Disable No Powser
-- Hold Customer , Close Customer ,Active
--UDV_IsCustomerExists
-- No Power Type is "1"


---------Adjustment -------------------	----------------------------------

-- Except Close Customer 
select * from Tbl_MCustomerStatus



 
	 
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	
	--Modified By Satya Sir 
		
	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	  	
	Select DisableTypeId from Tbl_BillingDisabledBooks    where BookNo =@BookNo  

	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
					 
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 or @CustomerActiveStatusID=3 
							BEGIN
							SELECT 1 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess and Active  Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
						
					 
				
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillingDisabledBookNo]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 24-Sep-2014  
-- Modified By: T.Karthik  
-- Modified Date : 17-10-2014  
-- Description: Purpose is To get List Of Billing Disabled BookNo  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillingDisabledBookNo]
(  
 @XmlDoc Xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageNo INT  
   ,@PageSize INT  
   ,@YearId INT  
   ,@MonthId INT  
   ,@BU_ID VARCHAR(50)
      
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
    ,@PageSize = C.value('(PageSize)[1]','INT')  
    ,@YearId=C.value('(YearId)[1]','INT')  
    ,@MonthId=C.value('(MonthId)[1]','INT')  
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')         
  FROM @XmlDoc.nodes('BillingDisabledBooksBe') AS T(C)   
    
  CREATE TABLE #BillingDisabledBookNos(Sno INT PRIMARY KEY IDENTITY(1,1),DisabledBookId INT)  
  
  INSERT INTO #BillingDisabledBookNos(DisabledBookId)  
   SELECT  
    DisabledBookId  
   FROM Tbl_BillingDisabledBooks  
   WHERE IsActive = 1  
   AND (YearId=@YearId OR @YearId='')  
   AND (MonthId=@MonthId OR @MonthId='')  
   ORDER BY CreatedDate DESC  
     
  DECLARE @Count INT=0  
  SET @Count=(SELECT COUNT(0) FROM #BillingDisabledBookNos)  
    
  SELECT  
    Sno AS RowNumber  
    ,BDB.BookNo AS BookNo  -- Faiz_ID103
    ,BN.ID AS ID
    ,BDB.DisabledBookId  
    ,ISNULL(Remarks,'--') AS Details  
    ,MonthId  
    ,YearId  
    ,((select dbo.fn_GetMonthName([MonthId]))+'-'+CONVERT(varchar(10), [YearId])) AS MonthYear  
    ,@Count AS TotalRecords
    ,(CASE IsPartialBill WHEN 1 THEN 'Partial Book' ELSE DisableType END) AS DisableType
  FROM Tbl_BillingDisabledBooks AS BDB  
  JOIN #BillingDisabledBookNos LBDB ON BDB.DisabledBookId=LBDB.DisabledBookId  
  INNER JOIN UDV_BookNumberDetails BN on BDB.BookNo=BN.BookNo 
	AND BN.BU_ID = @BU_ID
  INNER JOIN TBl_MDisableType D ON D.DisableTypeId = BDB.DisableTypeId
  --INNER JOIN Tbl_Cycles C ON C.CycleId = BN.CycleId
  --INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = C.ServiceCenterId
  --INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID 
  AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
     
 END  
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDisabledBookNoStatus]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  V.Bhimaraju  
-- Create date: 25-09-2014  
-- Description: The purpose of this procedure update the status of the Disabled BookNo
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateDisabledBookNoStatus]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @DisabledBookId INT,@IsPartialBill INT,@ModifiedBy VARCHAR(50)
	SELECT
		@DisabledBookId=C.value('(DisabledBookId)[1]','INT')
		,@IsPartialBill=C.value('(IsPartialBill)[1]','BIT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingDisabledBooksBe') as T(C)
	
	UPDATE Tbl_BillingDisabledBooks 
	SET IsActive=(CASE @IsPartialBill WHEN 1 THEN 1 ELSE 0 END),
		ModifiedBy=@ModifiedBy,ToDate=dbo.fn_GetCurrentDateTime(),IsPartialBill=@IsPartialBill,
		ModifiedDate=DATEADD(SECOND,19800,GETUTCDATE())	 WHERE DisabledBookId=@DisabledBookId
	SELECT 1 AS IsSuccess FOR XML PATH('BillingDisabledBooksBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerUDFValues]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author		:  NEERAJ KANOJIYA
-- Create date	:  28-MARCH-2015
-- Description	:  GET CUSTOMERS info from user defin controls
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerUDFValues]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  

    DECLARE  @GlobalAccountNumber VARCHAR(50)  
    SELECT            
	   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)  
		
		SELECT 
		(	
			
			SELECT SDCD.FieldName AS UDFTypeIdList
					,CASE WHEN (SELECT VText FROM MASTERS.Tbl_MControlRefMaster AS CRM WHERE CONVERT(VARCHAR(50),CRM.MRefId)=SDV.Value)
							IS NULL  THEN SDV.Value ELSE (SELECT VText FROM MASTERS.Tbl_MControlRefMaster AS CRM WHERE CONVERT(VARCHAR(50),CRM.MRefId)=SDV.Value) END AS UDFValueList
			FROM Tbl_USerDefinedValueDetails AS SDV
			JOIN MASTERS.[Tbl_USerDefinedControlDetails] AS SDCD ON SDV.UDCId=SDCD.UDCId
			WHERE SDV.AccountNo=@GlobalAccountNumber
			FOR XML PATH('CustomerRegistrationList_1BE'),TYPE
		
		
		
		
			--SELECT 
			--		UDVD.Value AS UDFValueList
			--		,FieldName AS UDFTypeIdList
			--FROM Tbl_USerDefinedValueDetails AS UDVD
			--JOIN [MASTERS].[Tbl_USerDefinedControlDetails] AS UDCD ON UDVD.UDCId=UDCD.UDCId
			--WHERE UDVD.AccountNo=@GlobalAccountNumber AND UDCD.IsActive=1
			--FOR XML PATH('CustomerRegistrationList_1BE'),TYPE
		)
		FOR XML PATH(''),ROOT('CustomerRegistrationInfoByXml')
END	
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBusinessUnits]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Bhimaraju Vanka>      
-- Create date: <20-FEB-2014>      
-- Description: <Insert Business units From AddCountries to Tbl_BussinessUnits>     
-- Modified By: Bhimaraju .V      
-- Modified date: 11-JULY-2014      
-- Description: Get BU_ID With Success Message  added BUCode Field    
-- Modified By: NEERAJ KANOJIYA     
-- Modified date: 22-DEC-2014      
-- Description: CALL AUTO GEN FUNC FOR BU CODE AS AN INPUT    
-- =============================================      
ALTER PROCEDURE [dbo].[USP_InsertBusinessUnits]      
 (      
 @XmlDoc xml      
 )      
AS      
BEGIN      
 DECLARE  @BusinessUnitName VARCHAR(300)      
   ,@Notes VARCHAR(MAX)      
   ,@CreatedBy VARCHAR(50)      
   ,@DistrictCode VARCHAR(20)      
   ,@UniqueId VARCHAR(20)    
   ,@StateCode VARCHAR(20)    
   ,@BUCode VARCHAR(10)    
   ,@Address1 VARCHAR(250)    
   ,@Address2 VARCHAR(250)    
   ,@City VARCHAR(50)    
   ,@ZIP VARCHAR(10)  
   ,@RegionId INT        
   
 SELECT   @BusinessUnitName=C.value('(BusinessUnitName)[1]','VARCHAR(300)')      
     ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')      
     ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')      
     ,@DistrictCode=C.value('(DistrictCode)[1]','VARCHAR(20)')      
     ,@StateCode=C.value('(StateCode)[1]','VARCHAR(20)')      
     ,@Address1=C.value('(Address1)[1]','VARCHAR(250)')         
     ,@Address2=C.value('(Address2)[1]','VARCHAR(250)')         
     ,@City=C.value('(City)[1]','VARCHAR(50)')         
     ,@ZIP=C.value('(ZIP)[1]','VARCHAR(10)')    
     ,@RegionId=C.value('(RegionId)[1]','INT')      
   --,@BUCode=C.value('(BUCode)[1]','VARCHAR(10)')    
   FROM @XmlDoc.nodes('MastersBE') AS T(C)     
     
 SET @BUCode=MASTERS.fn_BusinessUnitCodeGenerate(); --Temp comntd  
 SET @UniqueId=(SELECT dbo.fn_GenerateUniqueId(1));    
  
IF(@BUCode IS NOT NULL)        
BEGIN
 IF NOT EXISTs(SELECT 0 FROM Tbl_BussinessUnits WHERE BusinessUnitName=@BusinessUnitName )      
 BEGIN      
  IF(@BUCode IS NOT NULL)  
  BEGIN  
   IF NOT EXISTs(SELECT 0 FROM Tbl_BussinessUnits WHERE BUCode=@BUCode)    
   BEGIN     
     INSERT INTO Tbl_BussinessUnits (      
        BU_ID      
       ,BusinessUnitName      
       ,CreatedBy      
       ,CreatedDate      
       ,Notes      
       --,DistrictCode     
       ,StateCode    
       ,BUCode     
       ,Address1     
       ,Address2     
       ,City     
       ,ZIP  
	   ,RegionId 
       )    
  
     VALUES(      
        @UniqueId      
       ,@BusinessUnitName      
       ,@CreatedBy      
       ,DATEADD(SECOND,19800,GETUTCDATE())      
       ,CASE @Notes WHEN '' THEN NULL ELSE @Notes END      
       --,@DistrictCode     
       , @StateCode    
       --,@BUCode    
       ,@BUCode    
       ,@Address1             
       ,@Address2             
       ,@City             
       ,@ZIP 
	   ,@RegionId  
       )  
  
        SELECT 1 AS IsSuccess,@UniqueId AS BU_ID,@BUCode AS BUCode       
        FOR XML PATH ('MastersBE'),TYPE    
   END    
  ELSE    
   BEGIN    
    SELECT 1 AS IsBUCodeExists,@BUCode AS BUCode        
    FOR XML PATH ('MastersBE'),TYPE    
   END    
  END    
 END      
 ELSE      
   BEGIN      
     SELECT 0 As IsSuccess,@BUCode AS BUCode       
   FOR XML PATH('MastersBE'),TYPE      
   END    
END
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertServiceUnits]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Bhimaraju.V>    
-- Create date: <21-Feb-2014>    
-- Description: <For Insert ServiceUnits Data Into Tbl_ServiceUnits>    
-- ModifiedBy : T.Karthik    
-- Modified date: 29-11-2014    
-- Description: <For Get SU_ID With Success msg    
-- ModifiedBy : NEERAJ KANOJIYA    
-- Modified date: 22-DEC-2014    
-- Description: GET AUTO GEN Service Unit Code For Input.    
-- =============================================  
  
ALTER PROCEDURE [dbo].[USP_InsertServiceUnits]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @ServiceUnitName VARCHAR(300)    
   ,@Notes VARCHAR(MAX)    
   ,@BU_ID VARCHAR(20)    
   ,@CreatedBy VARCHAR(50)    
   ,@SUCode VARCHAR(10)    
   ,@UniqueId VARCHAR(20)    
    ,@Address1 VARCHAR(250)    
   ,@Address2 VARCHAR(250)    
   ,@City VARCHAR(50)    
   ,@ZIP VARCHAR(10)       
  
 SELECT   @ServiceUnitName=C.value('(ServiceUnitName)[1]','VARCHAR(300)')    
   ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')    
   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')    
   --,@SUCode=C.value('(SUCode)[1]','VARCHAR(10)')    
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')    
   ,@Address1=C.value('(Address1)[1]','VARCHAR(250)')         
     ,@Address2=C.value('(Address2)[1]','VARCHAR(250)')         
     ,@City=C.value('(City)[1]','VARCHAR(50)')         
     ,@ZIP=C.value('(ZIP)[1]','VARCHAR(10)')      
   FROM @XmlDoc.nodes('MastersBE') AS T(C)     
   
 SET @SUCode=dbo.fn_ServiceUnitCodeGenerate(@BU_ID);    
 SET @UniqueId=(SELECT dbo.fn_GenerateUniqueId(2));  
   IF(@SUCode IS NOT NULL)  
  BEGIN
  IF NOT EXISTs(SELECT 0 FROM Tbl_ServiceUnits WHERE ServiceUnitName=@ServiceUnitName AND BU_ID=@BU_ID)    
  BEGIN    
    IF NOT EXISTS(SELECT 0 FROM Tbl_ServiceUnits WHERE SUCode=@SUCode AND BU_ID=@BU_ID)    
     IF(@SUCode IS NOT NULL)  
      BEGIN  
       BEGIN    
      INSERT INTO Tbl_ServiceUnits (    
               SU_ID    
              ,ServiceUnitName    
              ,CreatedBy    
              ,CreatedDate    
              ,Notes    
              ,BU_ID    
              ,SUCode    
              ,Address1     
              ,Address2     
              ,City     
              ,ZIP   
              )  
  
            VALUES(    
               @UniqueId    
              ,@ServiceUnitName    
              ,@CreatedBy    
              ,DATEADD(SECOND,19800,GETUTCDATE())    
              ,CASE @Notes WHEN '' THEN NULL ELSE @Notes END    
              ,@BU_ID    
              ,@SUCode    
              ,@Address1             
              ,@Address2             
              ,@City             
              ,@ZIP     
              )  
   
       SELECT 1 AS IsSuccess,@UniqueId AS SU_ID,@SUCode AS SUCode  
       FOR XML PATH ('MastersBE'),TYPE    
    END  
      END  
    ELSE    
    BEGIN    
     SELECT 1 As IsSUCodeExists,@SUCode AS SUCode   
     FOR XML PATH('MastersBE'),TYPE    
    END    
  END  
  ELSE    
  BEGIN    
   SELECT 0 As IsSuccess,@SUCode AS SUCode   
    FOR XML PATH('MastersBE'),TYPE    
  END    
  END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@PreviousReading INT
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = OldMeterReading 
		,@NewMeterReading = NewMeterReading
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = NewMeterInitialReading
		,@NewMeterReadingDate = NewMeterReadingDate
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage > 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,@PreviousReading
										,@OldMeterReading
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,@NewMeterInitialReading
										,@NewMeterReading
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage > 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,@PreviousReading
								,@OldMeterReading
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,@NewMeterInitialReading
								,@NewMeterReading
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)

	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)

	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END

			SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

			--SELECT @PresentRoleId = PresentRoleId 
			--		,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)

			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterReading) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterInitialReading) END
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterReading) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterInitialReading) END
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,@PreviousReading
						,@OldMeterReading
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterReading) END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterInitialReading) END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author		:  NEERAJ KANOJIYA
-- Create date	:  27-MARCH-2015
-- Description	:  GET CUSTOMERS FULL DETAILS
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  

    DECLARE  @GlobalAccountNumber VARCHAR(50)  
    SELECT            
	   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)     
		SELECT top 1
		
		CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord
		,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord
		,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord
		,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord
		,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs
		,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord
		,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord
		,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord
		,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord
		,CD.DocumentNo	
		,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate	
		,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate	
		,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate	
		,CD.OldAccountNo	
		,CT.CustomerType
		,BookCode
		,CD.PoleID	
		,CD.MeterNumber	
		,TC.ClassName as Tariff	
		,CPD.IsEmbassyCustomer	
		,CPD.EmbassyCode	
		,CD.PhaseId	
		,RC.ReadCode as ReadType
		,CD.IsVIPCustomer
		,CD.IsBEDCEmployee
		,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService	
		,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService	
		,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService
		,PAD.AreaCode AS AreaService 
		,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode			
		,PAD.IsCommunication AS IsCommunicationService			
		,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal	
		,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal	
		,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL	
		,PAD1.AreaCode AS  AreaPostal		
		,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode
		,PAD1.IsCommunication AS IsCommunicationPostal				
		,PAD.AddressID AS ServiceAddressID		
		,CAD.IsCAPMI
		,InitialBillingKWh	
		,CAD.InitialReading	
		,CAD.PresentReading	--Faiz-ID103
		,CD.AvgReading as AverageReading	
		,CD.Highestconsumption	
		,CD.OutStandingAmount	
		,OpeningBalance
		,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1
		,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2
		,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType
		,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName
		,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName
		,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase
		,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName
		,CTD.TenentId
		,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent
		,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent
		,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent
		,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent
		,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent
		,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent
		,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent
		,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName
		,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy
		,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName
		,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber
		,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount
		,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy
		,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy  
		,CD.GlobalAccountNumber
		,CD.IsSameAsService
		from UDV_CustomerDescription CD
		left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId
		left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId
		left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1
		left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0
		left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber 
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber
		LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId
		LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId
		LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId
		LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo
		LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId
		LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId
		LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId
		LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId
		Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber
		FOR XML PATH('CustomerRegistrationBE'),TYPE
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- Modified By -- Bhimaraju Vanka
-- Modified Date -- 27-03-2015
-- Reason : Added/Inserting one more field MeterReading From
-- Modified By -- Jeevan Amunuri
-- Modified Date -- 03-04-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading](@XmlDoc XML)
AS
BEGIN
DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@IsTamper BIT
		,@Multiple numeric(20,4)
		,@MeterReadingFrom INT
SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@ReadBy=C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous=C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage= CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')
		
FROM @XmlDoc.nodes('BillingBE') AS T(C)
     
	SET @Multiple= (SELECT MI.MeterMultiplier FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	JOIN Tbl_MeterInformation MI ON MI.MeterNo=CPD.MeterNumber
	WHERE CPD.GlobalAccountNumber=@AccNum)
	
	DECLARE @MeterNumber VARCHAR(50)
	
	SET @MeterNumber=(SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber=@AccNum)
	
	IF(@IsTamper=0)
      BEGIN
		SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
      END
       
  INSERT INTO Tbl_CustomerReadings (
									 --[CustomerUniqueNo]
									  GlobalAccountNumber
									  ,[ReadDate]
									  ,[ReadBy]
									  ,[PreviousReading]
									  ,[PresentReading]
									  ,[AverageReading]
									  ,[Usage]
									  ,[TotalReadingEnergies]
									  ,[TotalReadings]
									  ,[Multiplier]
									  ,[ReadType]
									  ,[CreatedBy]
									  ,[CreatedDate]
									  ,[IsTamper]
									  ,MeterNumber
									  ,[MeterReadingFrom])
  
									VALUES(
									--@CustUn
									@AccNum
										   ,@ReadDate
										   ,@ReadBy
										   ,CONVERT(VARCHAR(50),@Previous)
										   ,CONVERT(VARCHAR(50),@Current)
										   --,(
										   -- (SELECT case when SUM( PresentReading) is NULL THEN 0 ELSE SUM( PresentReading) END FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Current)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1)
										   ,(SELECT dbo.fn_GetAverageReading_Save(@AccNum,@Usage))
										 --,CONVERT(VARCHAR(50),((SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+@Usage)/((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustUn)+1))
										   ,@Usage
										   ,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+@Usage
										   ,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccNum)+1)
										   ,@Multiple
										   ,2
										   ,@CreateBy
										   ,GETDATE()
										   ,@IsTamper
										   ,@MeterNumber
										   ,@MeterReadingFrom)
										  
UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET InitialReading=@Previous, PresentReading=@Current 
,AvgReading=CONVERT(NUMERIC,(SELECT dbo.fn_GetAverageReading_Save(@AccNum,@Usage))) WHERE GlobalAccountNumber= @AccNum

										   
SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
END


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertServiceCenter]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Bhimaraju.V>    
-- Create date: <24-Feb-2014>    
-- ModifiedBy : T.Karthik    
-- Modified date: 29-11-2014    
-- Description: <For Insert ServiceCenter Data Into Tbl_ServiceCenter>    
-- ModifiedBy : NEERAJ KANOJIYA    
-- Modified date: 22-DEC-2014    
-- Description: GET AUTO GEN Service Center Code For Input.    
-- =============================================  
  
ALTER PROCEDURE [dbo].[USP_InsertServiceCenter]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @ServiceCenterName VARCHAR(300)    
   ,@Notes VARCHAR(MAX)    
   ,@SU_ID VARCHAR(20)    
   ,@CreatedBy VARCHAR(50)  
   ,@UniqueId VARCHAR(20)   
   ,@SCCode VARCHAR(10)    
   ,@Address1 VARCHAR(100)    
   ,@Address2 VARCHAR(100)    
   ,@City VARCHAR(50)    
   ,@ZIP VARCHAR(10)      
 SELECT   @ServiceCenterName=C.value('(ServiceCenterName)[1]','VARCHAR(300)')    
   ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')    
   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')    
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')    
   ,@Address1=C.value('(Address1)[1]','VARCHAR(100)')         
     ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')         
     ,@City=C.value('(City)[1]','VARCHAR(50)')         
     ,@ZIP=C.value('(ZIP)[1]','VARCHAR(10)')        
   --,@SCCode=C.value('(SCCode)[1]','VARCHAR(10)')  
  
  
   FROM @XmlDoc.nodes('MastersBE') AS T(C)     
 SET @SCCode=dbo.fn_ServiceCenterCodeGenerate(@SU_ID)--Temp    
 SET @UniqueId=(SELECT dbo.fn_GenerateUniqueId(3));  
   IF(@SCCode IS NOT NULL)
  BEGIN
 IF NOT EXISTs(SELECT 0 FROM Tbl_ServiceCenter WHERE ServiceCenterName=@ServiceCenterName AND SU_ID=@SU_ID)    
 BEGIN    
   IF NOT EXISTs(SELECT 0 FROM Tbl_ServiceCenter WHERE SCCode=@SCCode AND SU_ID=@SU_ID)    
   BEGIN    
   IF(@SCCode IS NOT NULL)  
    BEGIN  
     INSERT INTO Tbl_ServiceCenter (    
             ServiceCenterId    
            ,ServiceCenterName    
            ,CreatedBy    
            ,CreatedDate    
            ,Notes    
            ,SU_ID    
            ,SCCode    
            ,Address1     
            ,Address2     
            ,City     
            ,ZIP   
            )  
  
          VALUES(    
             @UniqueId    
            ,@ServiceCenterName    
            ,@CreatedBy    
            ,DATEADD(SECOND,19800,GETUTCDATE())    
            ,CASE @Notes WHEN '' THEN NULL ELSE @Notes END    
            ,@SU_ID    
            ,@SCCode    
            ,@Address1             
            ,@Address2             
            ,@City             
            ,@ZIP   
            )  
    END               
     SELECT 1 AS IsSuccess,@UniqueId AS ServiceCenterId,@SCCode AS SCCode   
     FOR XML PATH ('MastersBE'),TYPE    
    END    
   ELSE     
   BEGIN    
     SELECT 1 AS IsSCCodeExists,@SCCode AS SCCode    
     FOR XML PATH ('MastersBE'),TYPE    
   END    
  END    
  ELSE    
  BEGIN  
    SELECT 0 AS IsSuccess,@SCCode AS SCCode     
    FOR XML PATH('MastersBE'),TYPE    
  END     
  END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		@ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@Count=C.value('(TotalReadings)[1]','INT')
		,@Avg=C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage=CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')

FROM @XmlDoc.nodes('BillingBE') AS T(C)


Declare @ReadId int

select @ReadId=CustomerReadingId from Tbl_CustomerReadings
where GlobalAccountNumber=@AccNum

 
	
UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
   ,Usage = @Usage
   ,ModifiedBy = @Modified
   ,IsTamper=@IsTamper
,MeterReadingFrom=@MeterReadingFrom
,ModifiedDate = GETDATE()
,AverageReading = (SELECT dbo.fn_GetAverageReading_Update
(GlobalAccountNumber,@Usage))
FROM Tbl_CustomerReadings CR	
 WHERE  CustomerReadingId=@ReadId 


SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END


GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetUserDefinedControls]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 10-JAN-2014      
-- Description: GET UDF LIST      
-- =============================================      
ALTER PROCEDURE [MASTERS].[USP_GetUserDefinedControls]      
AS        
BEGIN        
 SELECT      
 (      
  SELECT       
    UDCD.UDCId      
    ,UDCD.FieldName      
    ,UDCD.ControlTypeId      
    ,UDCD.IsMandatory      
    ,UDCD.IsActive      
    ,CRM.ControlTypeName      
  FROM [MASTERS].[Tbl_USerDefinedControlDetails] AS UDCD       
  LEFT JOIN [MASTERS].[Tbl_ControlRefMaster] AS CRM ON UDCD.UDCId= CRM.ControlTypeId WHERE UDCD.IsActive=1      
  FOR XML PATH('UserDefinedControlsListBE_1'),TYPE      
 ),      
 (      
  SELECT       
    MRefId      
    ,VText       
    ,UDCId      
  FROM MASTERS.[Tbl_MControlRefMaster]        
  WHERE IsActive=1      
  FOR XML PATH('UserDefinedControlsListBE_2'),TYPE      
 )      
 FOR XML PATH(''),ROOT('UserDefinedControlsInfoByXml')      
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetPaymets_New]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetPaymets_New]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
		
	IF (@FromDate ='' OR @FromDate IS NULL)    
		BEGIN    
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
		END 
	IF (@Todate ='' OR @Todate IS NULL)    
		BEGIN    
			SET @Todate = dbo.fn_GetCurrentDateTime()    
		END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,CD.GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,100),'--') AS RecievedDate
			,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD 	
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN Tbl_CustomerPayments CP ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
				--AND (DATEDIFF(DAY,@FromDate,CP.RecievedDate) >= 0 OR @FromDate = '')
				--AND (DATEDIFF(DAY,@ToDate,CP.RecievedDate) <= 0 OR @ToDate = '')
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		INNER JOIN Tbl_MBillingTypes BT ON CP.ReceivedDevice = BT.BillingTypeId
	)
	
	--SELECT 
	--	(
			SELECT	 
				 RowNumber
				,GlobalAccountNumber AS AccountNo
				,Name
				,ServiceAddress
				,BusinessUnitName
				,ServiceCenterName
				,ServiceUnitName
				,CycleName
				,BookNumber
				,CustomerSortOrder
				,BookSortOrder
				,ReceivedDevice
				,PaidAmount
				,RecievedDate
				,TotalDueAmount
				,(SELECT SUM(TotalDueAmount) FROM PagedResults ) AS TotalDue
				,(Select COUNT(0) from PagedResults) as TotalRecords
				,(SELECT SUM(PaidAmount) FROM PagedResults) AS TotalReceivedAmount
			FROM PagedResults p
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	--		FOR XML PATH('Reports'),TYPE
	--	)
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')
	
END
--------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [MASTERS].[USP_Re-AssignMeter]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 5-MARCH-2015  
-- Description: The purpose of this procedure is to assign meter to direct customer.  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_Re-AssignMeter]  
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
  @MeterNumber VARCHAR(50)    
  ,@ReadCodeID VARCHAR(50)   
  ,@GlobalAccountNumber VARCHAR(50)   
  ,@IsSuccessful BIT =1  
  ,@StatusText VARCHAR(200)  
     
BEGIN  
BEGIN TRY  
 BEGIN TRAN  
     
 SET @StatusText='Deserialization'  
  SELECT    
   @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
   ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')    
   ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')  
    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
 IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
	 BEGIN 
		 SET @StatusText='Update Statement'  
		   
		 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
		 SET MeterNumber=@MeterNumber  
		  ,ReadCodeID=@ReadCodeID  
		 WHERE GlobalAccountNumber = @GlobalAccountNumber  
		   	   
		 SET @StatusText='Success'
	 END
 ELSE 
	BEGIN
		SET @StatusText='Meter is in DeActivate state.'
		SET @IsSuccessful =0  
	END 
 COMMIT TRAN   
END TRY      
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @IsSuccessful =0  
END CATCH  
END  
 SELECT   
   @IsSuccessful AS IsSuccessful  
   ,@StatusText AS StatusText  
 FOR XML PATH('CustomerRegistrationBE'),TYPE  
  
END    
--------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_CustomerRegistration]    Script Date: 04/08/2015 01:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	CUSTOMER REGISTRATION
-- Modified By: Faiz-ID103
-- Modified Date: 06-Mar-2015
-- Description: Added Initial Reading in Step 10 Custmer Active Details    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters   
-- Modified BY: NEERAJ KANOJIYA-ID77
-- Modified Date: 07-Apr-2015
-- Description: ADDED COMMUNICATION TYPE INPUTS
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_CustomerRegistration]
(   
 @XmlDoc xml  
)  
AS  
BEGIN  
DECLARE
				@GlobalAccountNumber  Varchar(10)
				,@AccountNo  Varchar(50)
				,@TitleTanent varchar(10)
				,@FirstNameTanent	varchar(100)
				,@MiddleNameTanent	varchar(100)
				,@LastNameTanent	varchar(100)
				,@PhoneNumberTanent	varchar(20)
				,@AlternatePhoneNumberTanent 	varchar(20)
				,@EmailIdTanent	varchar(MAX)
				,@TitleLandlord	varchar(10)
				,@FirstNameLandlord	varchar(50)
				,@MiddleNameLandlord	varchar(50)
				,@LastNameLandlord	varchar(50)
				,@KnownAs	varchar(150)
				,@HomeContactNumberLandlord	varchar(20)
				,@BusinessPhoneNumberLandlord 	varchar(20)
				,@OtherPhoneNumberLandlord	varchar(20)
				,@EmailIdLandlord	varchar(MAX)
				,@IsSameAsService	BIT
				,@IsSameAsTenent	BIT
				,@DocumentNo	varchar(50)
				,@ApplicationDate	Datetime
				,@ConnectionDate	Datetime
				,@SetupDate	Datetime
				,@IsBEDCEmployee	BIT
				,@IsVIPCustomer	BIT
				,@OldAccountNo	Varchar(20)
				,@CreatedBy	varchar(50)
				,@CustomerTypeId	INT
				,@BookNo	varchar(30)
				,@PoleID	INT
				,@MeterNumber	Varchar(50)
				,@TariffClassID	INT
				,@IsEmbassyCustomer	BIT
				,@EmbassyCode	Varchar(50)
				,@PhaseId	INT
				,@ReadCodeID	INT
				,@IdentityTypeId	INT
				,@IdentityNumber	VARCHAR(100)
				,@UploadDocumentID	varchar(MAX)
				,@CertifiedBy	varchar(50)
				,@Seal1	varchar(50)
				,@Seal2	varchar(50)
				,@ApplicationProcessedBy	varchar(50)
				,@EmployeeName	varchar(100)
				,@InstalledBy 	INT
				,@AgencyId	INT
				,@IsCAPMI	BIT
				,@MeterAmount	Decimal(18,2)
				,@InitialBillingKWh	BigInt
				,@InitialReading	BigInt
				,@PresentReading	BigInt
				,@StatusText NVARCHAR(max)
				,@CreatedDate DATETIME
				,@PostalAddressID INT
				,@Bedcinfoid INT
				,@ContactName varchar(100)
				,@HouseNoPostal	varchar(50)
				,@StreetPostal Varchar(255)
				,@CityPostaL	varchar(50)
				,@AreaPostal	INT
				,@HouseNoService	varchar(50)
				,@StreetService	varchar(255)
				,@CityService	varchar(50)
				,@AreaService	INT
				,@TenentId INT
				,@ServiceAddressID INT
				,@IdentityTypeIdList	varchar(MAX)
				,@IdentityNumberList	varchar(MAX)
				,@DocumentName 	varchar(MAX)
				,@Path 	varchar(MAX)
				,@IsValid bit=1
				,@EmployeeCode INT
				,@PZipCode VARCHAR(50)
				,@SZipCode VARCHAR(50)
				,@AccountTypeId INT
				,@ClusterCategoryId INT
				,@RouteSequenceNumber INT
				,@Length INT
				,@UDFTypeIdList	varchar(MAX)
				,@UDFValueList	varchar(MAX)
				,@IsSuccessful BIT=1
				,@MGActTypeID INT
				,@IsCommunicationPostal BIT
				,@IsCommunicationService BIT
SELECT 
				 @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')
				,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')
				,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')
				,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')
				,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')
				,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')
				,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')
				,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')
				,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')
				,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')
				,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')
				,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')
				,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')
				,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')
				,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')
				,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')
				,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')
				,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')
				,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')
				,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')
				,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')
				,@SetupDate=C.value('(SetupDate)[1]','Datetime')
				,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')
				,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')
				,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')
				,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')
				,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
				,@BookNo=C.value('(BookNo)[1]','varchar(30)')
				,@PoleID=C.value('(PoleID)[1]','INT')
				,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')
				,@TariffClassID=C.value('(TariffClassID)[1]','INT')
				,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')
				,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')
				,@PhaseId=C.value('(PhaseId)[1]','INT')
				,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')
				,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')
				,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')
				,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')
				,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')
				,@Seal1=C.value('(Seal1)[1]','varchar(50)')
				,@Seal2=C.value('(Seal2)[1]','varchar(50)')
				,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')
				,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')
				,@InstalledBy =C.value('(InstalledBy )[1]','INT')
				,@AgencyId=C.value('(AgencyId)[1]','INT')
				,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')
				,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')
				,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')
				,@InitialReading=C.value('(InitialReading)[1]','BigInt')
				,@PresentReading=C.value('(PresentReading)[1]','BigInt')
				,@HouseNoPostal	=C.value('(	HouseNoPostal	)[1]','	varchar(50)	')
				,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')
				,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')
				,@AreaPostal=C.value('(AreaPostal)[1]','INT')
				,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')
				,@StreetService=C.value('(StreetService)[1]','varchar(255)')
				,@CityService=C.value('(CityService)[1]','varchar(50)')
				,@AreaService=C.value('(AreaService)[1]','INT')
				,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')
				,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')
				,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')
				,@Path=C.value('(Path)[1]','varchar(MAX)')
				,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')
				,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')
				,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')
				,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')
				,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')
				,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')
				,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')
				,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')
				,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')
				,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')
				,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')
				
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  


--||***************************TEMP RESOURCE REMOVAL****************************||--
BEGIN TRY
	DROP TABLE #DocumentList
	DROP TABLE #UDFList
	DROP TABLE #IdentityList
END TRY
BEGIN CATCH

END CATCH
--||****************************************************************************||--
--===========================REGISTRATION STEPS===========================--

--STEP 1	:	GENERATE GLOBAL ACCOUNT NO

--STEP 2	:	GENERATE ACCOUNT NO.

--STEP 3	:	ADD Customer Postal AddressDetails

--STEP 4	:	ADD CustomerTenentDetails.

--STEP 5	:	ADD Customer Details

--STEP 6	:	ADD Customer Procedural Details

--STEP 7	:	ADD Customer Identity Details

--STEP 8	:	ADD Application Process Details

--STEP 9	:	ADD Application Process Person Details

--STEP 10	:	ADD Tbl_CustomerActiveDetails

--STEP 11	:	ADD Tbl_CustomerDocuments

--STEP 12	:	ADD Tbl_USerDefinedValueDetails

--STEP 13	:	ADD Tbl_GovtCustomers

--STEP 14	:	ADD Tbl_Paidmeterdetails

--===========================******************===========================--


--===========================MEMBERS STARTS===========================--+
SET @StatusText='SUCCESS!'

--GENERATE GLOBAL ACCOUNT NUMBER
--||***************************STEP 1****************************||--
BEGIN TRY
    SET @GlobalAccountNumber=CUSTOMERS.fn_GlobalAccountNumberGenerate();
END TRY
BEGIN CATCH
	SET @StatusText='Cannot generate global account no.';
	SET @IsValid=0;
END CATCH

--||***************************STEP 2****************************||--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		DECLARE @BU_ID VARCHAR(50)
				,@SU_ID VARCHAR(50)
				,@ServiceCenterId VARCHAR(50)
		SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId 
		FROM Tbl_BookNumbers BN 
		JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId
		JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
		JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
		JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
		 WHERE BookNo=@BookNo
		 
		SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate 
		--SET @AccountNo='00000001'
	END TRY
	BEGIN CATCH
	SET @IsValid=0;
		SET @StatusText='Cannot generate account no.';
	END CATCH
END
--GET DATE FROM UDF
SET @CreatedDate=dbo.fn_GetCurrentDateTime();

--==========================Preparation of Identity details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
	CREATE TABLE #IdentityList(
								value1 varchar(MAX)
								,value2 varchar(MAX)
								,GlobalAccountNumber VARCHAR(10)
								,CreatedDate DATETIME
								,CreatedBy VARCHAR(50)
								)
	INSERT INTO #IdentityList(
								value1
								,value2
								,GlobalAccountNumber
								,CreatedDate
								,CreatedBy
							)
	select	Value1
			,Value2
			,@GlobalAccountNumber AS GlobalAccountNumber
			,@CreatedDate  AS CreatedDate
			,@CreatedBy AS CreatedBy
	from  [dbo].[fn_splitTwo](@IdentityTypeIdList,@IdentityNumberList,'|')
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare Identity list.';
	END CATCH
END
--select * from  [dbo].[fn_splitTwo]('2|1|3|','456456|24245|645646|','|')
--==========================Preparation of Document details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@DocumentName)+ LEN(@Path)		
		CREATE TABLE #DocumentList(
									value1 varchar(MAX)
									,value2 varchar(MAX)
									,GlobalAccountNumber VARCHAR(10)
									,ActiveStatusId INT
									,CreatedDate DATETIME
									,CreatedBy VARCHAR(50)
							)
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #DocumentList(
										GlobalAccountNumber
										,value1
										,value2
										,ActiveStatusId
										,CreatedBy
										,CreatedDate
									)
			select	@GlobalAccountNumber AS GlobalAccountNumber
					,Value1
					,Value2
					,1
					,@CreatedBy AS CreatedBy
					,@CreatedDate  AS CreatedDate
			from  [dbo].[fn_splitTwo](@DocumentName,@Path,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare document list.';
	END CATCH
END
--==========================Preparation of User Define Controls details table============================--
	
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@UDFTypeIdList)+ LEN(@UDFValueList)	
		CREATE TABLE #UDFList(
									UDCId	INT
									,Value	VARCHAR(150)
									,GlobalAccountNumber VARCHAR(10)
							)	
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #UDFList(										
									UDCId
									,Value
									,GlobalAccountNumber
									)
			select	
									Value1
									,Value2
									,@GlobalAccountNumber AS GlobalAccountNumber
			from  [dbo].[fn_splitTwo](@UDFTypeIdList,@UDFValueList,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare User Define Control list.';
	END CATCH
END
--===========================MEMBERS ENDS===========================--

--===========================REGISTRATION STARTS===========================--
DECLARE @intErrorCode INT
IF(@IsValid=1)
BEGIN
BEGIN TRY
	BEGIN TRAN
		IF(@GlobalAccountNumber IS NOT NULL)
			BEGIN
				IF(@AccountNo IS NOT NULL)
					BEGIN
		--||***************************STEP 3****************************||--
				SET @StatusText='Postal Address';
				INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														,IsCommunication
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoPostal
														,@StreetPostal
														,@CityPostaL
														,@AreaPostal
														,@PZipCode
														,0
														,@CreatedDate
														,@CreatedBy
														,@IsCommunicationPostal
														)
				SET @PostalAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				
				IF(@IsSameAsService=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														,IsCommunication
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoService
														,@StreetService
														,@CityService
														,@AreaService
														,@SZipCode
														,1
														,@CreatedDate
														,@CreatedBy
														,@IsCommunicationService
														)
				SET @ServiceAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				END
				
				--||***************************STEP 4****************************||--
				SET @StatusText='Tenent Address';
				IF(@IsSameAsTenent=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerTenentDetails(
														GlobalAccountNumber
														,Title
														,FirstName
														,MiddleName
														,LastName
														,PhoneNumber
														,AlternatePhoneNumber
														,EmailID
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@TitleTanent
														,@FirstNameTanent
														,@MiddleNameTanent
														,@LastNameTanent
														,@PhoneNumberTanent
														,@AlternatePhoneNumberTanent
														,@EmailIdTanent
														,@CreatedDate
														,@CreatedBy

														)
					SET @TenentId=SCOPE_IDENTITY();--(SELECT TOP 1 TenentId FROM CUSTOMERS.Tbl_CustomerTenentDetails)
				END
				--||***************************STEP 5****************************||--
				SET @StatusText='Customer Details';
					INSERT INTO CUSTOMERS.Tbl_CustomersDetail (
														GlobalAccountNumber
														,AccountNo
														,Title
														,FirstName
														,MiddleName
														,LastName
														,KnownAs
														,EmailId
														,HomeContactNumber
														,BusinessContactNumber
														,OtherContactNumber
														,IsSameAsService
														,ServiceAddressID
														,PostalAddressID
														,IsSameAsTenent
														,DocumentNo
														,ApplicationDate
														,ConnectionDate
														,SetupDate
														,ActiveStatusId
														,IsBEDCEmployee
														,EmployeeCode
														,IsVIPCustomer
														,TenentId
														,OldAccountNo
														,CreatedDate
														,CreatedBy
														)
												values
														(
														@GlobalAccountNumber
														,@AccountNo
														,@TitleLandlord
														,@FirstNameLandlord
														,@MiddleNameLandlord
														,@LastNameLandlord
														,@KnownAs
														,@EmailIdLandlord
														,@HomeContactNumberLandlord
														,@BusinessPhoneNumberLandlord
														,@OtherPhoneNumberLandlord
														,@IsSameAsService 
														,@ServiceAddressID
														,@PostalAddressID
														,@IsSameAsTenent
														,@DocumentNo
														,@ApplicationDate
														,@ConnectionDate
														,@SetupDate
														,1
														,@IsBEDCEmployee
														,@EmployeeCode
														,@IsVIPCustomer
														,@TenentId
														,@OldAccountNo
														,@CreatedDate
														,@CreatedBy
												)
					--||***************************STEP 6****************************||--
					SET @StatusText='Customer Procedural Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerProceduralDetails(
														 GlobalAccountNumber
														,CustomerTypeId
														,AccountTypeId
														,PoleID
														,MeterNumber
														,TariffClassID
														,IsEmbassyCustomer
														,EmbassyCode
														,PhaseId
														,ReadCodeID
														,BookNo
														,ClusterCategoryId
														,RouteSequenceNumber
														,CreatedDate
														,CreatedBy
														,ActiveStatusID
														)
												VALUES	(
														@GlobalAccountNumber
														,@CustomerTypeId
														,@AccountTypeId
														,@PoleID
														,@MeterNumber
														,@TariffClassID
														,@IsEmbassyCustomer
														,@EmbassyCode
														,@PhaseId
														,@ReadCodeID
														,@BookNo
														,@ClusterCategoryId
														,@RouteSequenceNumber
														,@CreatedDate
														,@CreatedBy
														,1
														)
					--||***************************STEP 7****************************||--
					SET @StatusText='Customer identity Details';
					IF EXISTS(SELECT 0 FROM #IdentityList)
					BEGIN
						INSERT INTO CUSTOMERS.Tbl_CustomerIdentityDetails(
															IdentityTypeId
															,IdentityNumber
															,GlobalAccountNumber
															,CreatedDate
															,CreatedBy
															)
						SELECT								*
						FROM #IdentityList
					END
					--||***************************STEP 8****************************||--
					SET @StatusText='Application Process Details';
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessDetails(
														 GlobalAccountNumber
														,CertifiedBy
														,Seal1
														,Seal2
														,ApplicationProcessedBy
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@CertifiedBy
														,@Seal1
														,@Seal2
														,@ApplicationProcessedBy
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 9****************************||--
					SET @StatusText='Application Process Person Details';
					SET @Bedcinfoid=SCOPE_IDENTITY();--(SELECT TOP 1 Bedcinfoid FROM CUSTOMERS.Tbl_ApplicationProcessDetails ORDER BY Bedcinfoid DESC)
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessPersonDetails(
														GlobalAccountNumber
														,Bedcinfoid
														--,EmployeeName
														,InstalledBy
														,AgencyId
														,ContactName
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@Bedcinfoid
														--,@EmployeeName
														,@InstalledBy
														,@AgencyId
														,@ContactName
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 10****************************||--
					SET @StatusText='Customer Active Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerActiveDetails(
														GlobalAccountNumber
														,IsCAPMI
														,MeterAmount
														,InitialBillingKWh
														,PresentReading
														,InitialReading--Faiz-ID103
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@IsCAPMI
														,@MeterAmount
														,@InitialBillingKWh
														,@PresentReading
														,@InitialReading--Faiz-ID103
														,@CreatedDate
														,@CreatedBy
														)
					--||***************************STEP 11****************************||--
					SET @StatusText='Document Details';
					if Exists(SELECT 0 FROM #DocumentList)
					BEGIN
						INSERT INTO Tbl_CustomerDocuments	
															(
															GlobalAccountNo
															,DocumentName
															,[Path]
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
															)
										SELECT				
															GlobalAccountNumber
															,value1
															,value2
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
										FROM  #DocumentList

				 						DROP TABLE #DocumentList
					 END
					--||***************************STEP 12****************************||--
					SET @StatusText='User Define List';
					if Exists(SELECT 0 FROM #UDFList)
					BEGIN
						INSERT INTO Tbl_USerDefinedValueDetails
															(
															AccountNo
															,UDCId
															,Value 
															)
										SELECT				
															GlobalAccountNumber
															,UDCId
															,Value
										FROM #UDFList

					 					
					 END
					 --||***************************STEP 13****************************||--
					SET @StatusText='Tbl_GovtCustomers';
					IF(@MGActTypeID > 0)
					BEGIN
						INSERT INTO Tbl_GovtCustomers
														(
														GlobalAccountNumber
														,MGActTypeID
														)
									values				(
														@GlobalAccountNumber
														,@MGActTypeID			
														)
					END
					 --||***************************STEP 14****************************||-- (Added By - Padmini(25th Feb 2015))
					SET @StatusText='Tbl_Paidmeterdetails';
					IF(@IsCAPMI=1)
					BEGIN
						INSERT INTO Tbl_PaidMeterDetails
														(
														AccountNo
														,MeterNo
														,MeterTypeId
														,MeterCost
														,ActiveStatusId
														,MeterAssignedDate
														,CreatedDate
														,CreatedBy
														)
									values
														(
														@GlobalAccountNumber
														,@MeterNumber
														,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)
														,@MeterAmount
														,1
														,@ConnectionDate
														,@CreatedDate
														,@CreatedBy
														)					
					END
				SET @StatusText	=''
			END
				ELSE
					BEGIN
						SET @GlobalAccountNumber = ''
						SET @IsSuccessful =0
						SET @StatusText='Problem in account number genrate.';
					END
			END
		ELSE
			BEGIN 
				SET @GlobalAccountNumber = ''
				SET @IsSuccessful =0
				SET @StatusText='Problem in global account number genrate.';
			END
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @GlobalAccountNumber = ''
	SET @IsSuccessful =0
END CATCH
END

SELECT 
	@IsSuccessful AS IsSuccessful
	,@StatusText AS StatusText 
	,@GlobalAccountNumber AS GolbalAccountNumber			
	FOR XML PATH('CustomerRegistrationBE'),TYPE
--===========================REGISTRATION ENDS===========================--
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertTamperedCustomers]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-02-2014
-- Description:	The purpose of this procedure is to insert Tampered Customers
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertTamperedCustomers]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @GlobalAccountNumber VARCHAR(20)
			,@Reason VARCHAR(MAX)
			,@MeterReaderId INT
			,@ReadingDate VARCHAR(20)
			,@CreatedBy VARCHAR(50)
			,@MeterNo VARCHAR(50)
	
	SELECT
		@GlobalAccountNumber=C.value('(GlobalAccountNumber)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@MeterReaderId=C.value('(MeterReaderId)[1]','INT')
		,@ReadingDate=C.value('(ReadingDate)[1]','VARCHAR(20)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('IsTamperBe') as T(C)
	
	IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_TamperedCustomers 
					WHERE GlobalAccountNumber=@GlobalAccountNumber 
					AND ReadingDate=@ReadingDate)
	BEGIN
		--SELECT 1 AS IsExists,Reason AS Reason FROM CUSTOMERS.Tbl_TamperedCustomers WHERE GlobalAccountNumber=@GlobalAccountNumber 
		--			AND ReadingDate=@ReadingDate
		
		UPDATE CUSTOMERS.Tbl_TamperedCustomers
		SET Reason = @Reason
			,ModifiedBy = @CreatedBy
			,ModifiedDate = dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber = @GlobalAccountNumber 
			AND ReadingDate = @ReadingDate
			
		SELECT 1 AS IsSuccess,@Reason AS Reason
		FOR XML PATH('IsTamperBe')
	END
	ELSE
		BEGIN
		INSERT INTO CUSTOMERS.Tbl_TamperedCustomers(
													 GlobalAccountNumber
													,MeterNo
													,MeterReaderId
													,ReadingDate
													,Reason
													,CreatedBy
													,CreatedDate)
											VALUES(  @GlobalAccountNumber
													,@MeterNo
													,@MeterReaderId
													,@ReadingDate
													,@Reason
													,@CreatedBy
													,dbo.fn_GetCurrentDateTime())
		
						SELECT 1 AS IsSuccess,@Reason AS Reason
						FOR XML PATH('IsTamperBe')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address     
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters   
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT   
   ,@IsSameAsServie BIT  
   ,@GlobalAccountNo   VARCHAR(50)        
     ,@NewPostalLandMark  VARCHAR(200)      
     ,@NewPostalStreet   VARCHAR(255)      
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT  
     ,@BookNo VARCHAR(50)  
       SELECT         
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')      
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')  
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)      
BEGIN TRY  
  BEGIN TRAN   
    --UPDATE POSTAL ADDRESS  
 SET @StatusText='Total address count.'     
 SET @TotalAddress=(SELECT COUNT(0)   
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
        )  
          
 --=====================================================================================          
 --Update book number of customer.  
 --=====================================================================================   
 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
 SET BookNo=@BookNo   
 WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1        
 --=====================================================================================          
 --Customer has only one addres and wants to update the address. //CONDITION 1//  
 --=====================================================================================      
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
 BEGIN  
  SET @StatusText='CONDITION 1'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END      
 --=====================================================================================  
 --Customer has one addres and wants to add new address. //CONDITION 2//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 2'   
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
                HouseNo  
                ,LandMark  
                ,StreetName  
                ,City  
                ,AreaCode  
                ,ZipCode  
                ,ModifedBy  
                ,ModifiedDate  
                ,IsCommunication  
                ,IsServiceAddress  
                ,IsActive  
                ,GlobalAccountNumber  
                )  
               VALUES (@NewServiceHouseNo         
                ,@NewServiceLandMark         
                ,@NewServiceStreet          
                ,@NewServiceCity        
                ,@NewServiceAreaCode        
                ,@NewServiceZipCode          
                ,@ModifiedBy        
                ,dbo.fn_GetCurrentDateTime()    
                ,@IsCommunicationService           
                ,1  
                ,1  
                ,@GlobalAccountNo  
                )   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo                 
 END     
   
 --=====================================================================================  
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
 BEGIN  
  SET @StatusText='CONDITION 3'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    IsActive=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END    
 --=====================================================================================  
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 4'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationService  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo     
 END    
 COMMIT TRAN  
END TRY  
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @RowsEffected=0  
END CATCH  
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')           
END
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT   
   ,@IsSameAsServie BIT  
   ,@GlobalAccountNo   VARCHAR(50)        
     ,@NewPostalLandMark  VARCHAR(200)      
     ,@NewPostalStreet   VARCHAR(255)      
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT  
       SELECT         
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')      
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)      
BEGIN TRY  
  BEGIN TRAN   
    --UPDATE POSTAL ADDRESS  
 SET @StatusText='Total address count.'     
 SET @TotalAddress=(SELECT COUNT(0)   
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
        )  
 --=====================================================================================          
 --Customer has only one addres and wants to update the address. //CONDITION 1//  
 --=====================================================================================   
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
 BEGIN  
  SET @StatusText='CONDITION 1'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END      
 --=====================================================================================  
 --Customer has one addres and wants to add new address. //CONDITION 2//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 2'   
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
                HouseNo  
                ,LandMark  
                ,StreetName  
                ,City  
                ,AreaCode  
                ,ZipCode  
                ,ModifedBy  
                ,ModifiedDate  
                ,IsCommunication  
                ,IsServiceAddress  
                ,IsActive  
                ,GlobalAccountNumber  
                )  
               VALUES (@NewServiceHouseNo         
                ,@NewServiceLandMark         
                ,@NewServiceStreet          
                ,@NewServiceCity        
                ,@NewServiceAreaCode        
                ,@NewServiceZipCode          
                ,@ModifiedBy        
                ,dbo.fn_GetCurrentDateTime()    
                ,@IsCommunicationService           
                ,1  
                ,1  
                ,@GlobalAccountNo  
                )   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo                 
 END     
   
 --=====================================================================================  
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
 BEGIN  
  SET @StatusText='CONDITION 3'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    IsActive=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END    
 --=====================================================================================  
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 4'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationService  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo     
 END    
 COMMIT TRAN  
END TRY  
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @RowsEffected=0  
END CATCH  
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')           
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchListEditListReport]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the List of Batches for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchListEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			--,@PageSize INT  
			--,@PageNo INT  
			,@BU_ID VARCHAR(50)
			,@TransactionFrom VARCHAR(MAX) 
		
	SELECT
		 @Users = C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(15)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')		
		,@TransactionFrom = C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	SELECT
	(
		SELECT 
			 ISNULL(CONVERT(VARCHAR(20),BD.CreatedDate,106),'--') AS TransactionDate
			,BD.BatchNo 
			,ISNULL(CONVERT(VARCHAR(20),BD.BatchDate,106),'--') AS PaidDate
			,CONVERT(VARCHAR(25), CAST(BD.BatchTotal AS MONEY), 1) AS Amount
			,U.Name AS UserName
			,CONVERT(VARCHAR(25), CAST(SUM(CP.PaidAmount) AS MONEY), 1) AS PaidAmount
			,COUNT(0) AS TotalRecords -- Total Customers
		FROM Tbl_BatchDetails BD
		INNER JOIN Tbl_UserDetails U ON U.UserId = BD.CreatedBy 			 
				AND BD.CreatedBy IN(SELECT [com] AS UserId FROM dbo.fn_Split(@Users,','))
				AND CONVERT (DATE,BD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
				AND BD.BU_ID = @BU_ID 
		LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchNo AND CP.PaymentType IN (SELECT [com] AS UserId FROM dbo.fn_Split(@TransactionFrom,','))
		GROUP BY BD.CreatedDate,BD.BatchNo,BD.BatchDate,BD.BatchTotal
				,U.Name
		HAVING SUM(CP.PaidAmount) >= BD.BatchTotal
		
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [MASTERS].[USP_IsMeterAvailable_New]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified by	: Satya
-- Modified Date: 6-APRIL-2014  
-- DESC			: MODIFIED CONDITION WITH IS NULL
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_IsMeterAvailable_New]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @MeterNo VARCHAR(100)  
   ,@BUID VARCHAR(50)  
   ,@CustomerTypeId INT  
     
  SELECT     
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')  
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
  
  Declare @MeterAvaiable bit = 1

  IF NOT EXISTS (SELECT	MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2
				  AND M.ActiveStatusId IN(1) 
				  AND (BU_ID=@BUID OR @BUID='')      
				  AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )  
	  BEGIN
		   SET @MeterAvaiable=0	 
	  END	  
  SELECT @MeterAvaiable AS IsSuccess FOR XML PATH('MastersBE'),TYPE    
     
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPagePermisions]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <09-JAN-2015>    
-- Description: <Retriving Master and Child page list for Role Permisions.>    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetPagePermisions]    
AS    
 BEGIN    
 Select (  
 --List for Master Menu Item  
   
   
   SELECT [MenuId]  
      ,[Name]  
      --,[Path]  
      ,[Menu_Order]  
      ,[ReferenceMenuId]  
      --,[Icon1]  
      --,[Icon2]  
      ,[Page_Order]  
      ,[IsActive]  
      --,[ActiveHeader]  
      --,[Menu_Image]  
   FROM Tbl_Menus WHERE Menu_Order IS NOT NULL AND IsActive= 1 ORDER BY Menu_Order  
   FOR XML PATH('AdminListBE'),TYPE  
    ),  
    (  
 --List for Child Menu Item  
    SELECT [MenuId]  
      ,[Name]  
      --,[Path]  
      ,[Menu_Order]  
      ,(CASE [ReferenceMenuId] WHEN 122 THEN 7 ELSE [ReferenceMenuId] END) AS [ReferenceMenuId]
      --,[Icon1]  
      --,[Icon2]  
      ,[Page_Order]  
      ,[IsActive]  
      --,[ActiveHeader]  
      --,[Menu_Image]  
   FROM Tbl_Menus WHERE Menu_Order IS NULL
   AND IsActive= 1 ORDER BY ReferenceMenuId  
   FOR XML PATH('AdminListBE_1'),TYPE  
   )  
   FOR XML PATH(''),ROOT('AdminBEInfoByXml')        
 END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetConfigurationSettingsMenu]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author: Karteek
-- Create date: 7 Apr 2015 
-- To get the configuration settings menu for left side
-- ============================================= 
ALTER PROCEDURE [dbo].[USP_GetConfigurationSettingsMenu]
(    
	@XmlDoc XML    
) 
AS
BEGIN     
	SET NOCOUNT ON; 
	   
	DECLARE @Role_Id INT 
		,@PageName VARCHAR(50)
		,@MenuId INT
		
	SELECT @Role_Id = C.value('(Role_Id)[1]','INT')   
		,@PageName = C.value('(PageName)[1]','varchar(50)')
	FROM @XmlDoc.nodes('SideMenusBE') AS T(C)  
	
	SELECT @MenuId = MenuId FROM Tbl_Menus WHERE Name = @PageName AND IsActive = 1
	
	SELECT    
	( 
		SELECT 
			 M.MenuId AS Main_Menu_Text
			,M.Name AS SubMenu_Text
			,M.[Path] AS Path
		FROM Tbl_Menus M
		INNER JOIN Tbl_NewPagePermissions N ON N.MenuId = M.MenuId 
			AND M.IsActive = 1 AND N.IsActive = 1
			AND N.Role_Id = @Role_Id
			AND M.ReferenceMenuId = @MenuId
		FOR XML PATH('SideMenu'),TYPE    
	)    
	FOR XML PATH(''),ROOT('SideMenuBeInfoByXml')   
	
END   
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	
		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)


		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')


		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and  
		(CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
		@IsExists = (Case when	 CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) 
		,@LatestDate=ReadDate  
		,@AverageReading=AverageReading 

		,@TotalReadings=TotalReadings  
		,@PreviousReading = PreviousReading
		,@PresentReading=PresentReading 
		,@IsBilled=IsBilled  
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC


		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  
		Print @IsActiveMonth


		SELECT TOP(1) @Month = BillMonth ,
		@Year = BillYear ,@LastBillReadType= (Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  

		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		--SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(4),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,@IsExists as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 ,@PreviousReading	as PreviousReading
				 ,@PresentReading   as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,@IsBilled as	 IsBilled
				 ,@IsLatestBill as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,@InititalReading as	 InititalReading
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN  
		--DECLARE @CustomerUnique VARCHAR(50);  
		DECLARE @meter VARCHAR(15);  
		DECLARE @previous VARCHAR(50);  
		DECLARE @present VARCHAR(50);  
		DECLARE @usage NUMERIC(20,4);  
		DECLARE @AvgPre VARCHAR(50)  
		DECLARE @Count INT;  
		DECLARE @IsBilled INT=0  
		DECLARE @IsTamper INT=0  
		,@EstimatedBillDate VARCHAR(50) = ''
		,@CustomerReadingId INT
		,@BillNo VARCHAR(50) 
		
		SELECT TOP (1) @EstimatedBillDate = CONVERT(VARCHAR(50),BillGeneratedDate,106)  
		FROM Tbl_CustomerBills bills
		LEFT JOIN UDV_CustomerMeterInformation CD ON bills.AccountNo = CD.GlobalAccountNumber
		WHERE CONVERT(DATE,BillGeneratedDate) > CONVERT(DATE,@ReadDate) 
		AND (bills.AccountNo = @AccNum OR CD.OldAccountNo = @OldAccountNo OR CD.MeterNumber = @MeterNo) 
		AND bills.ReadCodeId = 3
		ORDER BY CustomerBillId DESC
		  
		SELECT  
			--@CustomerUnique = CD.CustomerUniqueNo  
			 @meter = MeterNumber 
			,@AccNum = CD.GlobalAccountNumber
		FROM UDV_CustomerMeterInformation CD 
		WHERE (CD.GlobalAccountNumber = @AccNum OR CD.OldAccountNo = @AccNum OR CD.MeterNumber = @AccNum)
		--SET @previous=(SELECT TOP 1 PresentReading FROM Tbl_CustomerReadings WHERE CustomerUniqueNo=@CustomerUnique AND  CONVERT(VARCHAR(10),ReadDate , 121)<CONVERT(VARCHAR(10), @ReadDate, 121) ORDER BY CustomerReadingId DESC);  
		
		
		
		SELECT TOP (1) 
			@previous = PresentReading
			,@AvgPre = AverageReading
			,@Count = TotalReadings
			,@CustomerReadingId = CustomerReadingId
			,@IsTamper=ISNULL(IsTamper,0)
		FROM Tbl_CustomerReadings  
		WHERE CONVERT(DATE,ReadDate ) < CONVERT(DATE, @ReadDate)
		AND GlobalAccountNumber = @AccNum AND MeterNumber=@MeterNo
		ORDER BY ReadDate DESC,CustomerReadingId DESC;  
		
		SELECT 
			@BillNo = BillNo 
		FROM Tbl_CustomerReadings CR WHERE CustomerReadingId = @CustomerReadingId
		SELECT @previous = dbo.fn_GetCurrentReading_ByAdjustments(@BillNo,@previous)
							
		--SELECT  @IsBilled=IsBilled 
		--FROM Tbl_CustomerReadingsLog WHERE AccountNumber=@AccNum 
		--AND CONVERT(DATE,LatestReadDate)=CONVERT(DATE,@ReadDate);  

		SELECT 
			TOP 1 @present = PresentReading,@usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		FROM Tbl_CustomerReadings 
		WHERE GlobalAccountNumber=@AccNum  AND MeterNumber=@MeterNo
		AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		ORDER BY CustomerReadingId DESC; 
		
		--SELECT 
		--	TOP 1 @usage = Usage,@IsBilled = IsBilled ,@IsTamper=ISNULL(IsTamper,0) 
		--FROM Tbl_CustomerReadings 
		--WHERE GlobalAccountNumber=@AccNum 
		--AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)
		--ORDER BY CustomerReadingId DESC;		

		SELECT (  
				SELECT 
				  --@CustomerUnique as CustomerUniqueNo   
				   C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo  AS AccountNo
				  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
				  ,ISNULL(@usage,0) as Usage  
				  ,case when T.AvgMaxLimit is null then 0 else T.AvgMaxLimit end AS RouteNum  
				  ,@EstimatedBillDate AS EstimatedBillDate
				  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber)) AS IsActiveMonth				  
				  ,ISNULL((SELECT TOP(1) ISNULL(IsTamper,0) FROM Tbl_CustomerReadings 
					WHERE GlobalAccountNumber COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber
					AND CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC),0) AS IsTamper
				  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
				  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT= @AccNum  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
				  --,(SELECT TOP 1 CR.IsBilled FROM Tbl_CustomerReadings CR
						--WHERE CR.CustomerUniqueNo = @CustomerUnique AND CONVERT(DATE,CR.ReadDate)= CONVERT(DATE, @ReadDate) ORDER BY CR.CustomerReadingId DESC) AS IsBilled 
				  ,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT)  
						 THEN CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
						 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT ORDER BY ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate     
				  ,(CASE WHEN @AvgPre IS NULL THEN   
					 (CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
					 WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT=C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND   Cr.MeterNumber=@MeterNo AND
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
					  IS NULL  
					  THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))   
					  ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT AND  Cr.MeterNumber=@MeterNo AND
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate desc)  
					  END)  
					 ELSE @AvgPre
					 END) AS AverageReading  
				  ,CASE WHEN @Count IS NULL THEN 0 ELSE @Count END AS TotalReadings  
				  ,C.MeterNumber AS MeterNo  
				  --,CASE WHEN @previous IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE @previous END AS PreviousReading
				  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PreviousReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.PresentReading,0)) ELSE @present END AS PresentReading
				  ,CASE WHEN @present IS NULL THEN CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE '1' END AS PrvExist  
				  ,@IsBilled AS IsBilled
				  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
				  ,dbo.fn_LastBillGeneratedReadType(@AccNum) AS LastBillReadType
				  FROM UDV_CustomerMeterInformation C   
				  LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
				  WHERE GlobalAccountNumber = @AccNum
				  AND (C.BU_ID=@BUID OR @BUID='')  AND C.MeterTypeId=2
				  FOR XML PATH('BillingBE'),TYPE
		  )
	   FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	end  
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  	
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
				 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
			  -- IS NULL  
			  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
			  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
			  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
			  --AS PreviousReading  
			 --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
			 
			 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
			 --WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
				-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
			  --,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  
			  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
		  --)  
		--SELECT(  
		--	SELECT CustomerUniqueNo
		--		,RowNumber
		--		,AccNum
		--		,OldAccountNo
		--		,EstimatedBillDate
		--		,IsActiveMonth
		--		,IsExists
		--		,LatestDate
		--		,Name
		--		,MeterNo
		--		,MeterDials  
		--		,Decimals  
		--		--,RouteNum  
		--		,AverageReading  
		--		,TotalReadings  
		--		,PresentReading    
		--		,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
		--		,PrvExist  
		--		,Usage  
		--		,IsBilled 
		--		,IsLatestBill
		--		,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords       
		--   FROM PagedResults  
		--   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
		--FOR XML PATH('BillingBE'),TYPE
		--)
	 --FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetRoles]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 03-MAR-2014  
-- Description: To Get Roles Details  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetRoles]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
 DECLARE @RoleId INT  
 SELECT @RoleId=C.value('(RoleId)[1]','INT')  
 FROM @XmlDoc.nodes('UserManagementBE') AS T(C)  
  
 SELECT (  
  SELECT * FROM Tbl_MRoles   
  WHERE (RoleId=@RoleId OR @RoleId='')  
  AND RoleId !=12 AND IsActive=1 -- Faiz-ID103 
  FOR XML PATH('UserManagementBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteState]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Bhimaraju Vanka>    
-- Create date: <13-FEB-2014>    
-- Description: <Delete State Row By StateCode in Tbl_States>   
-- Modified By : Bhargav G  
-- Modified Date : 17th Feb, 2015  
-- Modified Description : Restricting De activation if any customer is associated to that particular state   
-- =============================================    
ALTER PROCEDURE [dbo].[USP_DeleteState]     
(    
 @XmlDoc XML    
)    
AS    
BEGIN    
 DECLARE @StateCode VARCHAR(20)    
  ,@IsActive BIT    
  ,@ModifiedBy VARCHAR(50)    
     
 SELECT @StateCode = C.value('(StateCode)[1]','VARCHAR(20)')    
    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')    
    ,@IsActive = C.value('(IsActive)[1]','BIT')    
 FROM @XmlDoc.nodes('MastersBE') AS T(C)    
     
    IF NOT EXISTS(SELECT 0 FROM Tbl_States S  
     JOIN Tbl_BussinessUnits BU ON BU.StateCode = S.StateCode  
     JOIN Tbl_ServiceUnits SU ON SU.BU_ID = BU.BU_ID  
     JOIN Tbl_ServiceCenter SC ON SC.SU_ID = SU.SU_ID  
     JOIN Tbl_Cycles CY ON CY.ServiceCenterId = SC.ServiceCenterId  
     JOIN Tbl_BookNumbers BN ON BN.CycleId = CY.CycleId  
     JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.BookNo = BN.BookNo  
    WHERE S.StateCode = @StateCode)  
  BEGIN    
   UPDATE Tbl_States     
   SET IsActive = @IsActive,    
   ModifiedBy = @ModifiedBy,    
   ModifiedDate = DATEADD(SECOND,19800,GETUTCDATE())    
   WHERE StateCode = @StateCode  
     
   SELECT 1 AS IsSuccess   
   FOR XML PATH('MastersBE')    
  END  
 ELSE  
  BEGIN  
   SELECT   
    COUNT(0) AS [Count]  
    ,0 AS IsSuccess  
   FROM Tbl_States S  
   JOIN Tbl_BussinessUnits BU ON BU.StateCode = S.StateCode  
   JOIN Tbl_ServiceUnits SU ON SU.BU_ID = BU.BU_ID  
   JOIN Tbl_ServiceCenter SC ON SC.SU_ID = SU.SU_ID  
   JOIN Tbl_Cycles CY ON CY.ServiceCenterId = SC.ServiceCenterId  
   JOIN Tbl_BookNumbers BN ON BN.CycleId = CY.CycleId  
   JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.BookNo = BN.BookNo  
   AND S.StateCode = @StateCode  
   FOR XML PATH('MastersBE')  
  END      
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillPreviousReading_New]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBillPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		@ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

SELECT @ReadDate=C.value('(ReadDate)[1]','datetime')
		,@Count=C.value('(TotalReadings)[1]','INT')
		,@Avg=C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current=C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage=CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum=C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper=C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom=C.value('(MeterReadingFrom)[1]','INT')

FROM @XmlDoc.nodes('BillingBE') AS T(C)


Declare @ReadId int

select @ReadId=CustomerReadingId from Tbl_CustomerReadings
where GlobalAccountNumber=@AccNum

SET @CustUn=(SELECT TOP 1 GlobalAccountNumber FROM
 CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber=@AccNum)
	
UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
   ,Usage = @Usage
   ,ModifiedBy = @Modified
   ,IsTamper=@IsTamper
,MeterReadingFrom=@MeterReadingFrom
,ModifiedDate = GETDATE()
,AverageReading = (SELECT dbo.fn_GetCustomerAverageReading(GlobalAccountNumber,CONVERT(NUMERIC,@Current),@ReadDate))

FROM Tbl_CustomerReadings CR	
 WHERE  CustomerReadingId=@ReadId 


SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBusinessUnitsList]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Bhimaraju Vanka>      
-- Create date: <20-FEB-2014>      
-- Description: <Purpose is To get List Of BusinessUnits>    
  
-- Updated By  : Faiz - ID103  
-- Updated Date: 08-01-20185    
-- Description: Added feilds for getting address,city, zip and Region  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBusinessUnitsList]      
(      
 @XmlDoc Xml      
)      
AS      
BEGIN      
      
 DECLARE  @PageNo INT      
   ,@PageSize INT        
          
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')      
    ,@PageSize = C.value('(PageSize)[1]','INT')       
           
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
        
  ;WITH PagedResults AS      
  (      
  SELECT      
      ROW_NUMBER() OVER(ORDER BY BU.ActiveStatusId ASC , BU.CREATEDDATE DESC ) AS RowNumber      
     ,BU.BU_ID      
     ,BU.BusinessUnitName      
     ,BU.Notes      
     ,BU.CreatedDate    
     ,S.StateCode      
     ,BU.RegionId  
     ,BU.ActiveStatusId    
     ,BUCode    
     ,BU.Address1  
     ,BU.Address2  
     ,BU.City  
     ,BU.ZIP  
   FROM Tbl_BussinessUnits BU      
   JOIN Tbl_States S ON S.StateCode=BU.StateCode 
   Join Tbl_MRegion R on R.RegionId=BU.RegionId and R.ActiveStatusId=1 --FaizID103
   WHERE S.IsActive=1    
  )      
      
    
        
   SELECT       
    (      
   SELECT  BU_ID      
     ,BusinessUnitName      
     ,CreatedDate      
     ,(CASE ISNULL(Notes,'') WHEN ''      
        THEN '---'            
        ELSE Notes      
        END) as Notes      
     ,(Select COUNT(0) from PagedResults) as TotalRecords      
     ,ActiveStatusId     
     ,StateCode      
     ,RegionId  
     ,(SELECT RegionName FROM Tbl_MRegion WHERE RegionId=p.RegionId) as RegionName  
     ,(SELECT StateName FROM Tbl_States WHERE StateCode=p.StateCode) AS StateName    
    ,ISNULL(BUCode,'--') AS BUCode    
     ,RowNumber     
     ,Address1  
     ,Address2  
     ,City  
     ,ZIP   
    FROM PagedResults p      
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
    FOR XML PATH('MastersBE'),TYPE      
  )      
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')       
 END     
 ------------------------------------------------------------------------------------------------  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationDetailsByCycle_Rajaiah]    Script Date: 04/08/2015 01:05:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================                      
-- Author  : NEERAJ KANOJIYA                    
-- Create date  : 25 July 2014                      
-- Description  : THIS PROCEDURE WILL GET ESTIMATION DETAILS OF READING  
-- Modified By : Padmini
-- Modified Date : 26-12-2014                 
-- =======================================================================                      
ALTER PROCEDURE [dbo].[USP_GetEstimationDetailsByCycle_Rajaiah]                      
(                      
	@XmlDoc xml                      
)                      
AS                      
BEGIN 
		 DECLARE @Year INT      
				 ,@Month int                
				 ,@Cycle Varchar(50)
				 ,@PageNo INT
				 ,@PageSize INT
		               
		  SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @Cycle = C.value('(Cycle)[1]','VARCHAR(50)'),
		   @PageNo = C.value('(PageNo)[1]','INT'),
		   @PageSize = C.value('(PageSize)[1]','INT')
		   FROM @XmlDoc.nodes('EstimationBE') as T(C)         
		  
  --SELECT   @Year = 2015,   @Month =2,   @Cycle = 'BEDC_C_0013',   @PageNo = 1,   @PageSize = 100000
  
  		   ;wITH Result as      
		   (      
				   SELECT   --ROW_NUMBER() OVER(ORDER BY ClassID ASC ) AS RowNumber    
				  -- ,
				   ClassID--Tariff ID      
				   ,TS.ClassName--Tariff Name      
				   , COUNT(CustomerProcedureID)  AS EstimatedCustomers
				   --,(SELECT COUNT(0) FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CDD JOIN Tbl_BookNumbers BCC ON CDD.BookNo = BCC.BookNo       
					  --WHERE TariffClassID  = ClassID AND BCC.CycleId = @Cycle) AS EstimatedCustomers      
				   ,dbo.fn_GetMonthlyUsage_ByTariff(@Cycle,ClassID,@Year,@Month) AS AvgReadingPerCust      
				   ,(select SUM(EnergytoCalculate) from Tbl_EstimationSettings ES WHERE CycleId=@Cycle AND BillingYear=@Year AND BillingMonth=@Month AND ES.TariffId= TS.ClassID AND ES.ActiveStatusId=1) AS AvgUsage      
				   --,dbo.fn_GetMonthlyUsage_ByTariff(@Cycle,ClassID,@Year,@Month)+(select SUM(EnergytoCaluclate) from Tbl_EstimationSettings WHERE CycleId=@Cycle AND BiingYear=@Year AND BillingMonth=@Month AND TariffId=ClassID) AS Total      
				  FROM Tbl_MTariffClasses TS   
				  INNER JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD ON TS.ClassID=CD.TariffClassID  
				  INNER JOIN Tbl_BookNumbers BN ON CD.BookNo=BN.BookNo 
				  WHERE TS.RefClassID IS NOT NULL AND  BN.CycleId=@Cycle
				  AND TS.ClassID = CD.TariffClassID 
				  GROUP BY ClassID ,TS.ClassName
				   
		   )      
		         
		        SELECT TS.ClassID      
								   ,TS.ClassName      
								   ,ISNULL(B.EstimatedCustomers,0) AS  EstimatedCustomers
								   ,ISNULL(B.AvgReadingPerCust,0) AS AvgReadingPerCust
								   ,ISNULL(B.AvgUsage,0) AS AvgUsage
								   ,ISNULL(B.Total,0) AS Total
								   ,ROW_NUMBER() OVER(ORDER BY TS.ClassID ASC ) AS RowNumber         
								  INTO #tblResults --,B.TotalRecords
							FROM Tbl_MTariffClasses TS   LEFT JOIN 
							( 
								  SELECT  ClassID      
								   ,ClassName      
								   ,EstimatedCustomers      
								   ,AvgReadingPerCust      
								   ,AvgUsage      
								   ,(ISNULL(AvgUsage,0) + ISNULL(AvgReadingPerCust,0)) AS Total
								 --  ,RowNumber
								   --,(Select COUNT(0) from Result) as TotalRecords
								  FROM Result      
								  
						   )B ON B.ClassID=TS.ClassID 
		  SELECT      
		  (               
					SELECT ClassID      
								,ClassName      
								,EstimatedCustomers
								,AvgReadingPerCust
								,AvgUsage
								,Total
								,RowNumber         
								,(Select COUNT(0) from #tblResults) as TotalRecords
					FROM #tblResults
				    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize                      
					 FOR XML PATH('EstimationDetails'),TYPE                                        
			)      
			FOR XML PATH(''),ROOT('EstimationInfoByXml')          
			DROP TABLE #tblResults
END

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/08/2015 01:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya.K>
-- Create date: <10-Mar-2015>
-- Description:	<The purpose of this procedure is to generate the bills>
-- =============================================

--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		     
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
		 
		    
		   
		 	 
			SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
			,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
			@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
			,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
			,@OpeningBalance=isnull(OpeningBalance,0)
			,@CustomerFullName=CustomerFullName
			,@BusinessUnitName =BusinessUnitName
			,@TariffName =TariffName
			--,@ReadDate =
			--,@Multiplier  
			,@Service_HouseNo =ServiceHouseNo
			,@Service_Street =ServiceStreet
			,@Service_City =ServiceCity
			,@ServiceZipCode  =ServiceZipCode
			,@Postal_HouseNo =ServiceHouseNo
			,@Postal_Street =Postal_StreetName
			,@Postal_City  =Postal_City
			,@Postal_ZipCode  =Postal_ZipCode
			,@Postal_LandMark =Postal_LandMark
			,@Service_LandMark =Service_LandMark
			,@OldAccountNumber=OldAccountNo
			FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
			
			 
			
			
	 
			 
			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
			BEGIN
				
				SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
					,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
				FROM Tbl_CustomerBills(NOLOCK)  
				WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
				DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
				
				DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
				SELECT @PaidAmount=SUM(PaidAmount)
				FROM Tbl_CustomerBillPayments(NOLOCK)  
				WHERE BillNo=@CusotmerPreviousBillNo
				
				
				 
				
			
				IF @PaidAmount IS NOT NULL
				BEGIN
									
					SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
					UPDATE Tbl_CustomerBillPayments
					SET BillNo=NULL
					WHERE BillNo=@RegenCustomerBillId
				END
				
				----------------------Update Readings as is billed =0 ----------------------------
				UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				
				
				-- Insert Paid Meter Payments
				DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
				 
				update CUSTOMERS.Tbl_CustomerActiveDetails
				SET OutStandingAmount=@OutStandingAmount
				where GlobalAccountNumber=@GlobalAccountNumber
				----------------------------------------------------------------------------------
				
				
	 
				--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
				DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
				-------------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------------
				--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
				--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
				--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
				
				---------------------------------------------------------------------------------------
				--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
				---------------------------------------------------------------------------------------
			END
			
			   IF	 isnull(@ActiveStatusId,0) != 1    -- In Active Customer
					BREAK;
			 
			 
						select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
						from Tbl_BillingDisabledBooks	  
						where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
				
			  IF 	isnull(@BookDisableType,0)   = 1
		 			BREAK;
		 		
		 		
				IF @ReadCodeId=2 -- 
				BEGIN
					 
					 
					SELECT  @PreviousReading=PreviousReading,
							@CurrentReading = PresentReading,@Usage=Usage,
							@LastBillGenerated=LastBillGenerated,
							@PreviousBalance =Previousbalance,
							@BalanceUnits=BalanceUsage,
							@IsFromReading=IsFromReading,
							@PrevCustomerBillId=CustomerBillId,
							@PreviousBalance=PreviousBalance,
							@Multiplier=Multiplier,
							@ReadDate=ReadDate
					FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
					
					
					
					
					 
					IF @IsFromReading=1
					BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							IF @Usage<@BalanceUnits
							BEGIN
									 
								SET @ActualUsage=@Usage
								SET @Usage=0
								SET @RemaningBalanceUsage=@BalanceUnits-@Usage
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @ActualUsage=@Usage
								SET @Usage=@Usage-@BalanceUnits
								SET @RemaningBalanceUsage=0
							END
					END
					ELSE
					BEGIN
							SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
					END
				END
				ELSE --@CustomerTypeID=1 -- Direct customer
				BEGIN
					set @IsEstimatedRead =1
					 
					 
					-- Get balance usage of the customer
					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
								  @PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 
					
					IF @PreviousBalance IS NULL
					BEGIN
						SET @PreviousBalance=@OpeningBalance
					END

				
			 
					      
					SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
					SET @ReadType=1 -- Direct
					SET @ActualUsage=@Usage
				END
			IF  @ActiveStatusId =2 -- In Active Customers
			BEGIN	 
					 
				 
				SET	 @Usage=0
			END
			
					
 			
			
			
			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0
			 
			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
			DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
			
			------------------------------------------------------------------------------------ 

			IF  isnull(@IspartialClose,0) =   1
				BEGIN
					
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					
				
							INSERT INTO @tblFixedCharges(ClassID ,Amount)
							SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
							SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
						 
				END
						  
			
					IF @BookDisableType = 2  -- Temparary Close
						BEGIN
								
							SET	 @EnergyCharges=0
						END
			------------------------------------------------------------------------------------------------------------------------------------
			
			
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
			BEGIN
				
				IF @GetPaidMeterBalance<@FixedCharges
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=0
					SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
					SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
				END
				ELSE
				BEGIN
					SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
					SET @FixedCharges=0
				END
			END
			------------------------
			-- Caluclate tax here
			
			SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			
			 
			IF	 @PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
							WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
							WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				
				END
			
			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0
			
			set @NetArrears=@OutStandingAmount-@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
			
			
			
			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
				FROM Tbl_CustomerBills (NOLOCK)      
				WHERE AccountNo = @GlobalAccountNumber
				Group BY CustomerBillId       
				ORDER BY CustomerBillId DESC 
			 
			if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage
			
			if @RemainingBalanceUnits IS NULL
			 set @RemainingBalanceUnits=0
			 
			 -------------------------------------------------------------------------------------------------------
			SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

			FROM Tbl_LEnergyClassCharges 
			WHERE ClassID =@TariffId
			AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	
				FOR XML PATH(''), TYPE)
			 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
			 
			
				
				
				
			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
			
			 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			-----------------------------------------------------------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
				,TariffRates
			)        
			SELECT GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
				,BU_ID
				,SU_ID
				,ServiceCenterId
				,PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,ISNULL(@AverageUsageForNewBill,0)
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
				,@TariffCharges
			FROM @CustomerMaster C        
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL
			SET @IspartialClose=NULL
			SET @BookDisableType =NULL
			SET @TariffCharges=NULL
			SET @CusotmerPreviousBillNo=NULL
			
			-----------------------------------------------------------------------------------------
			
			--While Loop continue checking
			IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
				BREAK        
			ELSE        
			BEGIN     
			   --BREAK
			   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
			   IF(@GlobalAccountNumber IS NULL) break;        
				Continue        
			END
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		--SELECT * From dbo.fn_GetCustomerBillsForPrint(@CycleId,@Month,@Year)  
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustInfoForAdjustmentNoBill]    Script Date: 04/08/2015 01:49:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 8-APRIL-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 CREATE PROCEDURE [dbo].[USP_GetCustInfoForAdjustmentNoBill]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)             
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name                   
      ,A.GlobalAccountNumber AS AccountNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                      
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill]    Script Date: 04/08/2015 01:49:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 
 
------    No Bill 
-- No Power		  Book Disable No Powser
-- Hold Customer , Close Customer ,Active
--UDV_IsCustomerExists
-- No Power Type is "1"


---------Adjustment -------------------	----------------------------------

-- Except Close Customer 
--select * from Tbl_MCustomerStatus



 
	 
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	
	--Modified By Satya Sir 
		
	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	  	
	--Select DisableTypeId from Tbl_BillingDisabledBooks    where BookNo =@BookNo  

	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
					 
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 or @CustomerActiveStatusID=3 
							BEGIN
							SELECT 1 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess and Active  Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
						
					 
				
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END

GO


