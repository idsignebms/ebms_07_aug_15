
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[UDV_IsCustomerExists]
AS
	SELECT CD.GlobalAccountNumber,
		CD.OldAccountNo,
		PD.MeterNumber
		,Cd.ActiveStatusId
		,SU.BU_ID
		,PD.ReadCodeID
		,PD.BookNo
	FROM CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
         CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
         INNER JOIN Tbl_BookNumbers BN ON	PD.BookNo=BN.BookNo
         INNER JOIN Tbl_Cycles CY ON CY.CycleId=BN.CycleId
         INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId
         INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID


GO


