GO
ALTER TABLE Tbl_BillDetails ADD Notes VARCHAR(MAX)
Go
ALTER TABLE Tbl_BillDetails ADD BillFilePath VARCHAR(MAX)
GO
Update TBL_FunctionalAccessPermission
set [Function]='Change Customer Book No' where [Function]='Chnage Customer BookNo'
GO
---------------------------------------------------------------------------------------
GO
UPDATE Tbl_Menus SET IsActive=0 WHERE Name = 'Check Meter Customers List' AND ReferenceMenuId = 8
GO
--------------------------------------------------------------------------------------------------

GO
/****** Object:  Trigger [dbo].[TR_InsertAuditServiceUnits]    Script Date: 04/08/2015 16:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceUnits into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
ALTER TRIGGER [dbo].[TR_InsertAuditServiceUnits]  ON  [dbo].[Tbl_ServiceUnits]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE  
	 @SU_ID   varchar (20)   ,
	 @ServiceUnitName   varchar (300)  ,
	 @Notes   varchar (max)  ,
	 @BU_ID   varchar (20)  ,
	 @SUCode   varchar (10)  ,
	 @ActiveStatusId   int   ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ModifiedBy   varchar (50) ;
	 
	 SELECT 
	 @SU_ID =I.SU_ID
	 ,@ServiceUnitName=I.ServiceUnitName
	 ,@Notes=I.Notes
	 ,@BU_ID=I.BU_ID
	 ,@SUCode=I.SUCode
	 ,@Address1=I.Address1
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 ,@ActiveStatusId=I.ActiveStatusId
	 ,@ModifiedBy=I.ModifiedBy
	 FROM inserted I; 
	 
	 INSERT INTO Tbl_Audit_ServiceUnits
	 (
		SU_ID
		,ServiceUnitName
		,Notes
		,BU_ID
		,ActiveStatusId
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate
		,SUCode
		,Address1
		,Address2
		,City
		,ZIP

	 )
	 SELECT [SU_ID]
      ,[ServiceUnitName]
      ,[Notes]
      ,[BU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SUCode] 
      ,Address1
      ,Address2
      ,City
      ,ZIP
      FROM Tbl_ServiceUnits WHERE SU_ID=@SU_ID;
	 
	 UPDATE Tbl_ServiceUnits SET
							   [ServiceUnitName]=@ServiceUnitName
							  ,[Notes]=@Notes
							  ,[BU_ID]=@BU_ID
							  ,[SUCode]=@SUCode
							  ,[ActiveStatusId]=@ActiveStatusId
							  ,[Address1]=@Address1
							  ,Address2=@Address2
						       ,City=@City
							   ,ZIP=@ZIP
							  ,[ModifiedBy]=@ModifiedBy
							  ,[ModifiedDate]=GETDATE() 
	 WHERE SU_ID=@SU_ID;

END
