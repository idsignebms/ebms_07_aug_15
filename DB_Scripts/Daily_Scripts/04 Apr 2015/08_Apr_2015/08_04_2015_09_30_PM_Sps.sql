
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateFiilePathCustomerDetails]    Script Date: 04/08/2015 21:33:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 08-APRIL-2015                    
 -- Description  : THIS PROCEDURE IS TO UPDATE FILE PATH BY CUSTOMER BILL ID
 -- =============================================================================                    
 CREATE PROCEDURE [dbo].[USP_UpdateFiilePathCustomerDetails]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
   BEGIN                    
  DECLARE @CustomerBillId VARCHAR(50) 
			,@BillFilePath VARCHAR(MAX)                 
  SELECT         
	@CustomerBillId = C.value('(CustomerBillId)[1]','VARCHAR(50)')                                                                
	,@BillFilePath = C.value('(FilePath)[1]','VARCHAR(MAX)')                                                                
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)  
	
	UPDATE Tbl_BillDetails SET BillFilePath=@BillFilePath 
	WHERE CustomerBillID=@CustomerBillId
	SELECT @@ROWCOUNT AS RowsEffected FOR XML PATH('BillGenerationBe'),TYPE
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
		 
			BEGIN TRY		    
					   
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						 
						
						
				 
						 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							
							 
							
						
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  -- IF	 isnull(@ActiveStatusId,0) != 1    -- In Active Customer
								--BREAK;
						 
						 
								--	select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
								--	from Tbl_BillingDisabledBooks	  
								--	where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
							
						  --IF 	isnull(@BookDisableType,0)   = 1
		 					--	BREAK;
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE --@CustomerTypeID=1 -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
						IF  @ActiveStatusId =2 -- In Active Customers
						BEGIN	 
								 
							 
							SET	 @Usage=0
						END
						
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
								
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							END
									  
						
								IF @BookDisableType = 2  -- Temparary Close
									BEGIN
											
										SET	 @EnergyCharges=0
									END
						------------------------------------------------------------------------------------------------------------------------------------
						
						
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						
						 
						IF	 @PrevCustomerBillId IS NULL 
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						
							
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						   --BREAK
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						   IF(@GlobalAccountNumber IS NULL) break;        
							Continue        
						END
			END TRY
			BEGIN CATCH
					 PRINT 'Exception'
					 PRINT @GlobalAccountNumber
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCycles]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Modified date: 27-10-2014    
-- Description: The purpose of this procedure is to get list of Cycles    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCycles]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT     
   ,@BUID VARCHAR(50)     
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY C.ActiveStatusId ASC , C.CreatedDate DESC ) AS RowNumber    
    ,C.CycleId    
    ,C.CycleName    
    --,CycleNo    
    ,ISNULL(C.DetailsOfCycle,'---') AS Details    
    ,C.ActiveStatusId    
    ,C.CreatedBy  
    --,C.BU_ID    
    --,C.SU_ID    
    ,B.BU_ID    
    ,S.SU_ID  
    ,C.ServiceCenterId    
    ,ISNULL(C.CycleCode,'--') AS CycleCode  
    ,B.BusinessUnitName  
    ,ST.StateName  
    ,S.ServiceUnitName  
    ,SC.ServiceCenterName  
   FROM Tbl_Cycles AS C  
    LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
 LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID  
 LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  
 LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode  
 AND c.ActiveStatusId in (1,2)
 ----START conditions to chek parents are active---
 --AND SC.ActiveStatusId = 1
 --AND S.ActiveStatusId = 1
 --AND B.ActiveStatusId = 1
 --AND ST.IsActive = 1
 ----END conditions to chek parents are active---
   WHERE (B.BU_ID=@BUID OR @BUID='')    
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteRegion]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------
  
-- =============================================      
-- Author:  <Faiz - ID103>      
-- Create date: <02-Feb-2015>      
-- Description: <Delete Region Row By RegionId in Tbl_MRegions>  
-- Modified By : Bhargav Gajawada
-- Modified Date : 17th Feb, 2015
-- Modified Description : Restricted user to deactivate 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_DeleteRegion]   
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
	DECLARE @RegionId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  

	SELECT @RegionId = C.value('(RegionId)[1]','VARCHAR(50)')  
			,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('RegionsBE') AS T(C)  

	IF NOT EXISTS(SELECT 0 FROM Tbl_States S
					JOIN Tbl_MRegion R ON R.StateCode = S.StateCode
					JOIN Tbl_BussinessUnits BU ON BU.StateCode = S.StateCode
					JOIN Tbl_ServiceUnits SU ON SU.BU_ID = BU.BU_ID
					JOIN Tbl_ServiceCenter SC ON SC.SU_ID = SU.SU_ID
					JOIN Tbl_Cycles CY ON CY.ServiceCenterId = SC.ServiceCenterId
					JOIN Tbl_BookNumbers BN ON BN.CycleId = CY.CycleId
					JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.BookNo = BN.BookNo
					AND R.RegionId = @RegionId)
		BEGIN
			UPDATE Tbl_MRegion   
				SET ActiveStatusId = @ActiveStatusId,  
					ModifiedBy = @ModifiedBy,  
					ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE RegionId = @RegionId  

			SELECT @@ROWCOUNT As RowsEffected      
			FOR XML PATH('RegionsBE'),TYPE     
		END
	ELSE
		BEGIN
			SELECT 
				COUNT(0) AS [Count]
				,0 AS RowsEffected
			FROM Tbl_States S
			JOIN Tbl_MRegion R ON R.StateCode = S.StateCode
			JOIN Tbl_BussinessUnits BU ON BU.StateCode = S.StateCode
			JOIN Tbl_ServiceUnits SU ON SU.BU_ID = BU.BU_ID
			JOIN Tbl_ServiceCenter SC ON SC.SU_ID = SU.SU_ID
			JOIN Tbl_Cycles CY ON CY.ServiceCenterId = SC.ServiceCenterId
			JOIN Tbl_BookNumbers BN ON BN.CycleId = CY.CycleId
			JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.BookNo = BN.BookNo
			AND R.RegionId = @RegionId
			FOR XML PATH('RegionsBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSCWitlBillingStaus]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author:  NEERAJ    
-- Create date: 10-FEB-2015           
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC with billing status.    
-- Modified date: 16-FEB-2015           
-- Description: Added new field total customers  
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSCWitlBillingStaus]        
(        
@XmlDoc xml        
)        
AS        
BEGIN        
        
 DECLARE @BU_ID VARCHAR(20)    
   ,@SU_ID VARCHAR(MAX)    
   ,@ServiceCenterId VARCHAR(MAX)     
   ,@BillYear VARCHAR(MAX)    
   ,@BillMonth VARCHAR(MAX)       
          
 SELECT        
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')        
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')        
   ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')        
   ,@BillYear=C.value('(BillYear)[1]','VARCHAR(20)')          
   ,@BillMonth=C.value('(BillMonth)[1]','VARCHAR(20)')          
     
     
 FROM @XmlDoc.nodes('MastersBE') as T(C)        
         
 SELECT        
 (        
    SELECT        
   CycleId        
   ,(CycleName + ' ('+CycleCode+') - ' + SC.ServiceCenterName ) as CycleName
   ,(select COUNT(0)     
     from Tbl_CustomerBills as cb     
     where  cb.BillMonth=@BillMonth     
       and cb.BillYear=@BillYear     
       AND cb.BU_ID=BU.BU_ID    
       AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
       AND CB.CycleId=CY.CycleId) AS BillGenCount    
   ,(select COUNT(0)     
     from Tbl_CustomerBillPayments as bp JOIN Tbl_CustomerBills AS CB ON bp.BillNo=cb.BillNo    
     where  cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS PaymentCount       
  ,(select COUNT(0)     
     from Tbl_CustomerBills AS CB     
     JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId    
     where cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS BillAdjustmentCount      
   ,(SELECT COUNT(0)    
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=cy.CycleId) AS TotalCustomers  
  FROM Tbl_Cycles cy    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
  WHERE CY.ActiveStatusId=1        
  AND (BU.BU_ID=@BU_ID OR @BU_ID='')        
  AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')        
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')        
  ORDER BY CycleName ASC       
  FOR XML PATH('MastersBE'),TYPE        
 )        
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
END 


GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBooks_ByCycles]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author  : Suresh Kumar Dasi  
-- Create date  : 17 OCT 2014  
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles  
-- Modified By : Padmini  
-- Modified Date : 25-12-2014  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDisableBooks_ByCycles]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @CycleId VARCHAR(MAX)  
   ,@BillMonth INT  
   ,@BillYear INT  
     
 SELECT       
  @CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@BillMonth = C.value('(BillingMonth)[1]','INT')  
  ,@BillYear = C.value('(BillingYear)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
 SELECT  
  (  
  SELECT   
    BN.BookNo+'('+ BookCode+')' AS BookNo -- Modified By Padmini
   ,COUNT(0) AS TotalRecords  
  FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD  
  JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID  
  JOIN Tbl_BookNumbers BN ON CD.BookNo=BN.BookNo
  --WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))  
  WHERE CycleId IN (SELECT [com] FROM dbo.fn_Split(@CycleId,',')) -- Modified By Padmini
  AND CD.BookNo IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive = 1 AND MonthId = @BillMonth AND YearId = @BillYear)  
  AND CD.ActiveStatusId = 1  
  GROUP BY BN.BookNo,BookCode  
  FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')      
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCycleDetails]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 26-02-2014  
-- Modified Date : 01-03-2014  
-- Description: The purpose of this procedure is to Update Cycle Details  
-- Modified By:Bhimaraju.V  
-- Modified Date : 04-03-2014  
-- Description: Adding Some Fields in Procedure  
-- ModifiedBy : T.Karthik  
-- Modified date: 29-11-2014  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateCycleDetails]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @CycleId VARCHAR(20),@CycleName VARCHAR(300),@Details VARCHAR(MAX),  
  @ModifiedBy VARCHAR(50)  
  --,@BU_ID VARCHAR(20)  
  --,@SU_ID VARCHAR(20)  
  ,@ServiceCenterId VARCHAR(20)
  ,@ServiceCenterIdNew VARCHAR(20),  
  @ContactName VARCHAR(200),@ContactNo VARCHAR(20),@CycleNo INT,@CycleCode VARCHAR(10)  
    
 SELECT  
  @CycleId=C.value('(CycleId)[1]','VARCHAR(20)')  
  ,@CycleName=C.value('(CycleName)[1]','VARCHAR(300)')  
  ,@CycleCode=C.value('(CycleCode)[1]','VARCHAR(10)')  
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  --,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')  
  --,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')  
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')  
  ,@ContactName=C.value('(ContactName)[1]','VARCHAR(200)')  
  ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 IF NOT EXISTS(SELECT 0 FROM Tbl_Cycles WHERE CycleName=@CycleName AND ServiceCenterId=@ServiceCenterId AND CycleId!=@CycleId)  
 BEGIN  
  --IF NOT EXISTS (SELECT 0 FROM Tbl_Cycles WHERE CycleCode=@CycleCode AND ServiceCenterId=@ServiceCenterId AND CycleId!=@CycleId)  
  -- BEGIN  
  set @ServiceCenterIdNew = (SELECT top 1 ServiceCenterId from Tbl_Cycles where CycleId=@CycleId)
  IF (@ServiceCenterId = @ServiceCenterIdNew)
Begin
	--SET @CycleCode = (Select SCCode from Tbl_ServiceCenter where ServiceCenterId = @ServiceCenterId) 
	SET @CycleCode = (Select CycleCode from Tbl_Cycles where CycleId = @CycleId) --Faiz-ID103
end
else
Begin
	SET @CycleCode = MASTERS.fn_CycleCodeGenerate(@ServiceCenterId) 
end
  
  
    UPDATE Tbl_Cycles SET CycleName=@CycleName  
          ,DetailsOfCycle=CASE @Details WHEN '' THEN NULL ELSE @Details END  
          ,ModifiedBy=@ModifiedBy  
          ,ModifiedDate=DATEADD(SECOND,19800,GETUTCDATE())  
          --,BU_ID=@BU_ID  
          --,SU_ID=@SU_ID  
          ,ServiceCenterId=@ServiceCenterId  
          ,ContactName=@ContactName  
          ,ContactNo=@ContactNo  
          ,CycleCode=@CycleCode  
    WHERE CycleId=@CycleId  
       
    SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')  
   --END   
  --ELSE  
  --BEGIN  
  -- SELECT 1 AS IsCycleCodeExists FOR XML PATH('MastersBE')  
  --END   
 END   
  ELSE  
  BEGIN  
  SELECT 1 AS IsCycleExists FOR XML PATH('MastersBE')  
  END  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateServiceCenter]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Bhimaraju Vanka>    
-- Create date: <24-FEB-2014>    
-- ModifiedBy : T.Karthik    
-- Modified date: 29-11-2014    
-- Description: <Update Service Centers From AddServiceCenters to Tbl_ServiceCenter>    
  
--Modified By:  Faiz - ID103  
--Modified Date: 08-Jan-2015  
--Description: updated for address city and zip  
-- =============================================      
  
ALTER PROCEDURE [dbo].[USP_UpdateServiceCenter]    
 (    
 @XmlDoc XML=null    
)    
AS    
BEGIN     
 SET NOCOUNT ON;    
 DECLARE    
      
      @ServiceCenterId VARCHAR(20)     
     ,@ServiceCenterName VARCHAR(300)    
     ,@SU_ID VARCHAR(20)     
     ,@SU_IdNew VARCHAR(20)
     ,@Address1 varchar(100)    
     ,@Address2 varchar(100)    
     ,@City varchar(100)    
     ,@ZIP varchar(100)  
  ,@Notes varchar(MAX)    
  ,@SCCode varchar(10)    
  ,@ModifiedBy VARCHAR(50)    
        
 SELECT     
    @ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')    
   ,@ServiceCenterName=C.value('(ServiceCenterName)[1]','VARCHAR(300)')    
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')    
   ,@SCCode=C.value('(SCCode)[1]','VARCHAR(10)')      
 ,@Address1=C.value('(Address1)[1]','VARCHAR(100)')   
 ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')  
 ,@City =C.value('(City)[1]','VARCHAR(100)')  
 ,@ZIP =C.value('(ZIP)[1]','VARCHAR(100)')      
   ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')    
      ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)           
     
 IF NOT EXISTS(SELECT 0 FROM Tbl_ServiceCenter WHERE ServiceCenterName=@ServiceCenterName AND SU_ID=@SU_ID AND ServiceCenterId!=@ServiceCenterId)    
 BEGIN    
 --IF NOT EXISTS(SELECT 0 FROM Tbl_ServiceCenter WHERE SCCode=@SCCode AND SU_ID=@SU_ID AND ServiceCenterId!=@ServiceCenterId)    
 --BEGIN    
 
 set @SU_IdNew = (SELECT top 1 SU_ID from Tbl_ServiceCenter where ServiceCenterId=@ServiceCenterId)

IF (@SU_Id = @SU_IdNew)
Begin
	--SET @SCCode = (Select SUCode from Tbl_ServiceUnits where SU_ID = @SU_Id) 
	SET @SCCode = (Select SCCode from Tbl_ServiceCenter where ServiceCenterId = @ServiceCenterId) 
end
else
Begin
	SET @SCCode = dbo.fn_ServiceCenterCodeGenerate(@SU_ID) 
end
 
 
   UPDATE Tbl_ServiceCenter SET     ServiceCenterId = @ServiceCenterId    
           ,ServiceCenterName=@ServiceCenterName     
         ,Address1=@Address1  
   ,Address2=@Address2  
   ,City =@City  
   ,ZIP =@ZIP  
           ,ModifiedBy = @ModifiedBy    
           ,SU_ID=@SU_ID    
           ,ModifiedDate= DATEADD(SECOND,19800,GETUTCDATE())    
           ,Notes=(CASE @Notes WHEN '' THEN NULL ELSE @Notes END)    
           ,SCCode=@SCCode    
         WHERE ServiceCenterId = @ServiceCenterId    
    SELECT 1 As IsSuccess    
    FOR XML PATH('MastersBE'),TYPE    
 END    
 --ELSE    
 -- BEGIN    
 --   SELECT 1 As IsSCCodeExists    
 --   FOR XML PATH('MastersBE'),TYPE    
 -- END    
 --END    
     
 ELSE    
  BEGIN    
    SELECT 0 As IsSuccess    
    FOR XML PATH('MastersBE'),TYPE    
  END     
      
END    
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_IsPaymentsBacthesClose_Month]    Script Date: 04/08/2015 20:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 18 OCT 2014
-- Description  : To Check the Payments related old batches are close not to on Month
-- Modified By: T.karthik
-- Modified date: 30-12-2014
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsPaymentsBacthesClose_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@BU_ID VARCHAR(50)
	DECLARE @TempTable TABLE(BatchNo INT,BatchTotal DECIMAL(18,2),UsedBatchTotal DECIMAL(18,2))
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	

	INSERT INTO @TempTable(BatchNo,BatchTotal,UsedBatchTotal)			
	SELECT
		 BatchNo
		,BatchTotal
		,(SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments CP WITH (NOLOCK) WHERE BatchNo IS NOT NULL AND CP.BatchNo = BD.BatchNo)
	FROM Tbl_BatchDetails BD WITH (NOLOCK)
	WHERE BU_ID = @BU_ID
	
	IF EXISTS(SELECT 0 FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0))
		BEGIN
			SELECT 
				1 AS IsExists
				,SUBSTRING(
					(SELECT ','+CONVERT(VARCHAR(MAX),BatchNo) FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0) FOR XML PATH('')
						),2,10000000000) AS OpenBatches
			FOR XML PATH('BillGenerationBe'),TYPE			
		END
	ELSE
		BEGIN
			SELECT 0 AS IsExists
			FOR XML PATH('BillGenerationBe'),TYPE
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsAdjustmentBacthesClose_Month]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 18 OCT 2014
-- Description  : To Check the Adjustment related old batches are close not to on Month
-- Modified By: T.karthik
-- Modified date: 30-12-2014
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsAdjustmentBacthesClose_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@BU_ID VARCHAR(50)
	DECLARE @TempTable TABLE(BatchID INT,BatchNo INT,BatchTotal DECIMAL(18,2),UsedBatchTotal DECIMAL(18,2))
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	

	INSERT INTO @TempTable(BatchID,BatchNo,BatchTotal,UsedBatchTotal)			
	SELECT
		 BatchID
		 ,BatchNo
		,BatchTotal
		,(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments CP  WITH (NOLOCK) WHERE BatchNo IS NOT NULL AND CP.BatchNo = BD.BatchID)
	FROM Tbl_BATCH_ADJUSTMENT BD WITH (NOLOCK)
	WHERE BU_ID = @BU_ID

	IF EXISTS(SELECT 0 FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0))
		BEGIN
			SELECT 1 AS IsExists,SUBSTRING(
					(SELECT ','+CONVERT(VARCHAR(MAX),BatchNo) FROM @TempTable WHERE BatchTotal > ISNULL(UsedBatchTotal,0) FOR XML PATH('')
						),2,10000000000) AS OpenBatches
			FOR XML PATH('BillGenerationBe'),TYPE			
		END
	ELSE
		BEGIN
			SELECT 0 AS IsExists
			FOR XML PATH('BillGenerationBe'),TYPE
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetUserServiceUnits_BuIds]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 04-06-2014  
-- Description: The purpose of this procedure is to get Service Units based in BU_Ids  
-- Modified BY : Karteek
-- Modified Date : 08 Apr 2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetUserServiceUnits_BuIds]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @BU_IDs VARCHAR(MAX),@UserId VARCHAR(50)  
   
 SELECT  
  @BU_IDs = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)  
 
 
	IF(@UserId='')  
		BEGIN  
			SELECT  
			(  
				SELECT  
					SU_ID  
					,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
				FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
				AND ActiveStatusId=1  
				ORDER BY ServiceUnitName ASC  
				FOR XML PATH('Consumer'),TYPE  
			)  
			FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
		END  
	ELSE  
		BEGIN  
			
			IF NOT EXISTS(SELECT 0 FROM Tbl_UserServiceUnits WHERE UserId = @UserId AND ActiveStatusId = 1)
				BEGIN
					SELECT  
					(  
						SELECT  
							SU_ID  
							,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
						FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
						AND ActiveStatusId = 1  
						ORDER BY ServiceUnitName ASC  
						FOR XML PATH('Consumer'),TYPE  
					)  
					FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
				END
			ELSE
				BEGIN
					SELECT  
					(  
						SELECT  
							SU_ID  
							,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
						FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
						AND SU_ID IN(SELECT SU_ID FROM Tbl_UserServiceUnits WHERE UserId = @UserId AND ActiveStatusId = 1)  
						AND ActiveStatusId = 1  
						ORDER BY ServiceUnitName ASC  
						FOR XML PATH('Consumer'),TYPE  
					)  
					FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')
				END
		END
   
 --IF(@UserId='')  
 --BEGIN  
 -- SELECT  
 -- (  
 --  SELECT  
 --   SU_ID  
 --   ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --  FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
 --   AND ActiveStatusId=1  
 --  ORDER BY ServiceUnitName ASC  
 -- FOR XML PATH('Consumer'),TYPE  
 -- )  
 -- FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 --END  
 --ELSE  
 --BEGIN  
 -- DECLARE @RoleId INT  
 -- SELECT @RoleId=RoleId FROM Tbl_UserDetails WHERE UserId=@UserId  
 -- IF(@RoleId=1 OR @RoleId=2 OR @RoleId=3 OR @RoleId=5 OR @RoleId=12)   
 -- BEGIN  
 --  SELECT  
 --  (  
 --   SELECT  
 --    SU_ID  
 --    ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --    FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
 --    AND ActiveStatusId=1  
 --   ORDER BY ServiceUnitName ASC  
 --   FOR XML PATH('Consumer'),TYPE  
 --  )  
 --  FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 -- END  
 -- ELSE IF(@RoleId=6) -- SU Manager  
 -- BEGIN  
 --  SELECT  
 --  (  
 --   SELECT  
 --    SU_ID  
 --    ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --    FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
 --    AND SU_ID IN(SELECT SU_ID FROM Tbl_UserServiceUnits WHERE UserId=@UserId AND ActiveStatusId=1)  
 --    AND ActiveStatusId=1  
 --   ORDER BY ServiceUnitName ASC  
 --   FOR XML PATH('Consumer'),TYPE  
 --  )  
 --  FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 -- END  
 -- ELSE IF(@RoleId=4) -- Cashier  
 -- BEGIN  
 --  SELECT  
 --  (  
 --   SELECT  
 --    SU_ID  
 --    ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --    FROM Tbl_ServiceUnits WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_IDs,','))  
 --    AND SU_ID IN(SELECT SU_ID FROM Tbl_UserServiceCenters WHERE UserId=@UserId AND ActiveStatusId=1 GROUP BY SU_ID)  
 --    AND ActiveStatusId=1  
 --   ORDER BY ServiceUnitName ASC  
 --   FOR XML PATH('Consumer'),TYPE  
 --  )  
 --  FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 -- END  
 --END  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetServiceUnits]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 18-02-2014  
-- Description: The purpose of this procedure is to get Service Units  
-- ModifiedBy : V.Bhimaraju  
-- Description: To Get With Out Any Parameter and name With BU_ID  
-- Modified By : T.Karthik  
-- Modified Date : 14-03-2014  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetServiceUnits]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @BU_ID VARCHAR(20),@UserId VARCHAR(50)  
 SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')  
  ,@UserId=C.value('(UserId)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)  
   
	IF(@UserId='')  
		BEGIN  
			SELECT  
			(  
				SELECT  
					SU_ID  
					,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
				FROM Tbl_ServiceUnits WHERE (BU_ID = @BU_ID OR @BU_ID = '')  
				AND ActiveStatusId = 1  
				ORDER BY ServiceUnitName ASC  
				FOR XML PATH('Consumer'),TYPE  
			)  
			FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
		END  
 ELSE  
 BEGIN  
 
	  IF NOT EXISTS(SELECT 0 FROM Tbl_UserServiceUnits WHERE UserId = @UserId AND ActiveStatusId = 1)
		BEGIN
			   SELECT  
			   (  
					SELECT  
						 SU_ID  
						 ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
					 FROM Tbl_ServiceUnits WHERE (BU_ID = @BU_ID OR @BU_ID = '')  
					 AND ActiveStatusId=1  
					ORDER BY ServiceUnitName ASC  
					FOR XML PATH('Consumer'),TYPE  
			   )  
			   FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')
		END
	  ELSE 
		BEGIN
			   SELECT  
			   (  
					SELECT  
						 SU_ID  
						 ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
					 FROM Tbl_ServiceUnits WHERE (BU_ID = @BU_ID OR @BU_ID = '')  
					 AND SU_ID IN(SELECT SU_ID FROM Tbl_UserServiceUnits WHERE UserId = @UserId AND ActiveStatusId=1)  
					 AND ActiveStatusId = 1  
					 ORDER BY ServiceUnitName ASC  
					 FOR XML PATH('Consumer'),TYPE  
			   )  
			   FOR XML PATH(''),ROOT('ConsumerBeInfoByXml') 
	    
	  END
	
 -- DECLARE @RoleId INT  
 -- SELECT @RoleId=RoleId FROM Tbl_UserDetails WHERE UserId=@UserId  
  
 -- IF(@RoleId=1 OR @RoleId=2 OR @RoleId=3 OR @RoleId=5 OR @RoleId=12)   
 -- BEGIN  
 --  SELECT  
 --  (  
 --   SELECT  
 --    SU_ID  
 --    ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --    FROM Tbl_ServiceUnits WHERE (BU_ID=@BU_ID OR @BU_ID='')  
 --    AND ActiveStatusId=1  
 --   ORDER BY ServiceUnitName ASC  
 --   FOR XML PATH('Consumer'),TYPE  
 --  )  
 --  FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 -- END  
 -- ELSE IF(@RoleId=6) -- SU Manager  
 -- BEGIN  
 --  SELECT  
 --  (  
 --   SELECT  
 --    SU_ID  
 --    ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --    FROM Tbl_ServiceUnits WHERE (BU_ID=@BU_ID OR @BU_ID='')  
 --    AND SU_ID IN(SELECT SU_ID FROM Tbl_UserServiceUnits WHERE UserId=@UserId AND ActiveStatusId=1)  
 --    AND ActiveStatusId=1  
 --   ORDER BY ServiceUnitName ASC  
 --   FOR XML PATH('Consumer'),TYPE  
 --  )  
 --  FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 -- END  
 -- ELSE IF(@RoleId=4) -- Cashier  
 -- BEGIN  
 --  SELECT  
 --  (  
 --   SELECT  
 --    SU_ID  
 --    ,(ServiceUnitName +' ('+ISNULL(SUCode,'')+')') AS ServiceUnitName  
 --    FROM Tbl_ServiceUnits WHERE (BU_ID=@BU_ID OR @BU_ID='')  
 --    AND SU_ID IN(SELECT SU_ID FROM Tbl_UserServiceCenters WHERE UserId=@UserId AND ActiveStatusId=1 GROUP BY SU_ID)  
 --    AND ActiveStatusId=1  
 --   ORDER BY ServiceUnitName ASC  
 --   FOR XML PATH('Consumer'),TYPE  
 --  )  
 --  FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
 -- END  
 --END  
END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBusinessUnits]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 18-02-2014  
-- Modified date: 13-03-2014  
-- Modified By: V.Bhimaraju  
-- Modified date: 23-04-2014  
-- Description: The purpose of this procedure is to get Business Units  
-- Modified By : Bhargav G
-- Modified Date : 20th Feb, 2015
-- Modified Description : Appended BUCode beside BUName
-- Modified BY : Karteek
-- Modified Date : 08 Apr 2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBusinessUnits]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @StateCode VARCHAR(20)
			,@UserId VARCHAR(50)  
	
	SELECT @StateCode=C.value('(StateCode)[1]','VARCHAR(20)')  
			,@UserId=C.value('(UserId)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('ConsumerBe') as T(C)  

	IF(@UserId='')  
		BEGIN  
			SELECT  
			(  
				SELECT BU_ID  
					,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
				FROM Tbl_BussinessUnits 
					WHERE (StateCode=@StateCode OR @StateCode='')  
					AND ActiveStatusId=1  
				ORDER BY BusinessUnitName ASC  
				FOR XML PATH('Consumer'),TYPE  
			)  
			FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
		END  
	ELSE  
		BEGIN  
		
			IF EXISTS(SELECT 0 FROM Tbl_UserBusinessUnits WHERE UserId = @UserId AND ActiveStatusId = 1)
				BEGIN
					SELECT  
					(  
						SELECT BU_ID  
							,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
						FROM Tbl_BussinessUnits AS BU 
						WHERE (StateCode=@StateCode OR @StateCode='')  
						AND ActiveStatusId=1 
						AND BU.BU_ID IN(SELECT BU_ID FROM Tbl_UserBusinessUnits 
											WHERE UserId=@UserId 
											AND ActiveStatusId=1)  
						ORDER BY BusinessUnitName ASC
						FOR XML PATH('Consumer'),TYPE  
					)  
					FOR XML PATH(''),ROOT('ConsumerBeInfoByXml') 
				END
			ELSE
				BEGIN
					SELECT  
					(  
						SELECT BU_ID  
							,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
						FROM Tbl_BussinessUnits 
						WHERE (StateCode=@StateCode OR @StateCode='')  
						AND ActiveStatusId=1  
						ORDER BY BusinessUnitName ASC
						FOR XML PATH('Consumer'),TYPE  
					)  
					FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
				END
			
			--DECLARE @RoleId INT  
			
			--SELECT @RoleId=RoleId FROM Tbl_UserDetails 
			--	WHERE UserId=@UserId  

			--IF(@RoleId=1 OR @RoleId=12)  
			--	BEGIN  
			--		SELECT  
			--		(  
			--			SELECT BU_ID  
			--				,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
			--			FROM Tbl_BussinessUnits 
			--			WHERE (StateCode=@StateCode OR @StateCode='')  
			--			AND ActiveStatusId=1  
			--			ORDER BY BusinessUnitName ASC  
			--			FOR XML PATH('Consumer'),TYPE  
			--		)  
			--		FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
			--	END  
			--ELSE IF(@RoleId=2 OR @RoleId=3 OR @RoleId=5)  
			--	BEGIN  
			--		SELECT  
			--		(  
			--			SELECT BU_ID  
			--				,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
			--			FROM Tbl_BussinessUnits AS BU 
			--			WHERE (StateCode=@StateCode OR @StateCode='')  
			--			AND ActiveStatusId=1 
			--			AND BU.BU_ID IN(SELECT BU_ID FROM Tbl_UserBusinessUnits 
			--								WHERE UserId=@UserId 
			--								AND ActiveStatusId=1)  
			--			ORDER BY BusinessUnitName ASC  
			--			FOR XML PATH('Consumer'),TYPE  
			--		)  
			--		FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
			--	END  
			--ELSE IF(@RoleId=4)  
			--	BEGIN  
			--		SELECT  
			--		(  
			--			SELECT BU_ID  
			--				,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
			--			FROM Tbl_BussinessUnits AS BU 
			--			WHERE (StateCode=@StateCode OR @StateCode='')  
			--			AND ActiveStatusId=1 
			--			AND BU.BU_ID IN(SELECT BU_ID FROM Tbl_UserServiceCenters 
			--								WHERE UserId=@UserId 
			--								AND ActiveStatusId=1 
			--								GROUP BY BU_ID)  
			--			ORDER BY BusinessUnitName ASC  
			--			FOR XML PATH('Consumer'),TYPE  
			--		)  
			--		FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
			--	END  
			--ELSE IF(@RoleId=6)  
			--	BEGIN  
			--		SELECT  
			--		(  
			--			SELECT BU_ID  
			--				,BusinessUnitName + ' (' + BUCode + ')' AS BusinessUnitName
			--			FROM Tbl_BussinessUnits AS BU 
			--			WHERE (StateCode=@StateCode OR @StateCode='')  
			--			AND ActiveStatusId=1 
			--			AND BU.BU_ID IN(SELECT BU_ID FROM Tbl_UserServiceUnits 
			--								WHERE UserId=@UserId 
			--								AND ActiveStatusId=1 
			--								GROUP BY BU_ID)  
			--			ORDER BY BusinessUnitName ASC  
			--			FOR XML PATH('Consumer'),TYPE  
			--		)  
			--		FOR XML PATH(''),ROOT('ConsumerBeInfoByXml')  
			--	END  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml    
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,BookCode  
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL   
  ,PAD1.AreaCode AS  AreaPostal    
  ,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_DeletePaymentEntryList]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  Neeraj Kanojiya        
-- Create date: 11-04-2014        
-- Modified date : 16-04-2014        
-- Description: The purpose of this procedure is to delete PaymentEntry list        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_DeletePaymentEntryList]         
(        
@XmlDoc xml        
)        
AS        
BEGIN        
 DECLARE @CustomerPaymentID INT        
 ,@CustomerBPID INT     
   ,@Flag INT       
         
 SELECT        
  @CustomerPaymentID=C.value('(CustomerPaymentID)[1]','INT')        
  ,@CustomerBPID=C.value('(CustomerBPID)[1]','INT')   
  ,@Flag=C.value('(Flag)[1]','INT')             
 FROM @XmlDoc.nodes('MastersBE') AS T(C)
 
  DECLARE @AccountNo VARCHAR(50), @PaiddAmount DECIMAL(18,2)
 
 select @AccountNo = AccountNo, @PaiddAmount = ISNULL(PaidAmount,0) from Tbl_CustomerPayments where   CustomerPaymentId=@CustomerPaymentID
          
 IF(@Flag=1)  --Deletion in temp table    
 BEGIN    
     DELETE Tbl_CustomerBillPaymentsApproval WHERE CustomerPaymentId=@CustomerPaymentID        
   DELETE Tbl_CustomerPaymentsApproval WHERE CustomerPaymentID=@CustomerPaymentID      
    End  
 ELSE IF(@Flag=2) --Deletion in base table    
    BEGIN  
    
    UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
    SET OutStandingAmount = OutStandingAmount + @PaiddAmount
    WHERE GlobalAccountNumber = @AccountNo
    
    UPDATE Tbl_CustomerBills SET PaymentStatusID=2 --Opening bill which are closed after payment.
			WHERE BillNo IN(SELECT BillNo 
							FROM Tbl_CustomerBillPayments 
							WHERE CustomerPaymentId=@CustomerPaymentID)
							
    DELETE Tbl_CustomerBillPayments WHERE CustomerPaymentId=@CustomerPaymentID 
           
    DELETE Tbl_CustomerPayments WHERE CustomerPaymentID=@CustomerPaymentID       
    
    End  
 SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')        
         
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustInfoForAdjustmentNoBill]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 8-APRIL-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 ALTER PROCEDURE [dbo].[USP_GetCustInfoForAdjustmentNoBill]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)             
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name                   
      ,A.GlobalAccountNumber AS AccountNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                      
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill]    Script Date: 04/08/2015 20:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_AdjustmentNoBill] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 
 
------    No Bill 
-- No Power		  Book Disable No Powser
-- Hold Customer , Close Customer ,Active
--UDV_IsCustomerExists
-- No Power Type is "1"


---------Adjustment -------------------	----------------------------------

-- Except Close Customer 
--select * from Tbl_MCustomerStatus



 
	 
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	
	--Modified By Satya Sir 
		
	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	  	
	--Select DisableTypeId from Tbl_BillingDisabledBooks    where BookNo =@BookNo  

	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
					 
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 or @CustomerActiveStatusID=3 
							BEGIN
							SELECT 1 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess and Active  Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
						
					 
				
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateFiilePathCustomerDetails]    Script Date: 04/08/2015 20:37:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 08-APRIL-2015                    
 -- Description  : THIS PROCEDURE IS TO UPDATE FILE PATH BY CUSTOMER BILL ID
 -- =============================================================================                    
 ALTER PROCEDURE [dbo].[USP_UpdateFiilePathCustomerDetails]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @CustomerBillId VARCHAR(50) 
			,@BillFilePath VARCHAR(MAX)                 
  SELECT         
	@CustomerBillId = C.value('(CustomerBillId)[1]','VARCHAR(50)')                                                                
	,@BillFilePath = C.value('(FilePath)[1]','VARCHAR(MAX)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	UPDATE Tbl_BillDetails SET BillFilePath=@BillFilePath
	SELECT @@ROWCOUNT AS RowsEffected FOR XML PATH(''),TYPE
END




GO
/****** Object:  StoredProcedure [dbo].[USP_GetCycles]    Script Date: 04/08/2015 21:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Modified date: 27-10-2014    
-- Description: The purpose of this procedure is to get list of Cycles    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCycles]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT     
   ,@BUID VARCHAR(50)     
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY C.ActiveStatusId ASC , C.CreatedDate DESC ) AS RowNumber    
    ,C.CycleId    
    ,C.CycleName    
    --,CycleNo    
    ,ISNULL(C.DetailsOfCycle,'---') AS Details    
    ,C.ActiveStatusId    
    ,C.CreatedBy  
    --,C.BU_ID    
    --,C.SU_ID    
    ,B.BU_ID    
    ,S.SU_ID  
    ,C.ServiceCenterId    
    ,ISNULL(C.CycleCode,'--') AS CycleCode  
    ,B.BusinessUnitName  
    ,ST.StateName  
    ,S.ServiceUnitName  
    ,SC.ServiceCenterName  
   FROM Tbl_Cycles AS C  
    LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1
 LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID  AND S.ActiveStatusId = 1
 LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  AND B.ActiveStatusId = 1
 LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode  AND ST.IsActive = 1
 AND C.ActiveStatusId in (1,2)
 
   WHERE (B.BU_ID=@BUID OR @BUID='')    
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END  



GO
/****** Object:  StoredProcedure [dbo].[USP_GetBookNumbers]    Script Date: 04/08/2015 21:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Description: The purpose of this procedure is to get list of Book Numbers    
-- Modified By: V.BhimaRaju    
--Modified Date: 27-02-2014    
--Modified Date: 28-03-2014    
-- Modified By: T.Karthik  
-- Modified Date: 27-10-2014  
-- Description: To Wrong in Table and For --- in Details    
-- Modified By: Padmini  
-- Modified Date:17-Feb-2015  
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBookNumbers]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT      
   ,@BUID VARCHAR(50)  
      
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY BN.ActiveStatusId ASC , BN.CreatedDate DESC ) AS RowNumber    
    ,BN.ID AS BookNo  
    ,(SELECT dbo.fn_GetCycleIdByBook(BN.BookNo)) As CycleId  
    ,(SELECT CycleName From Tbl_Cycles WHERE CycleId=(SELECT dbo.fn_GetCycleIdByBook(BookNo))) CycleName  
    ,BN.ActiveStatusId    
    ,BN.CreatedBy    
    ,BN.NoOfAccounts    
    --,Details    
    ,ISNULL(BN.Details,'---')AS Details    
    ,BU.BU_ID    
    ,SU.SU_ID    
    ,SC.ServiceCenterId    
    ,ISNULL(BN.BookCode,'---')AS BookCode    
    ,BU.BusinessUnitName AS BusinessUnitName    
    ,SU.ServiceUnitName  AS ServiceUnitName    
    ,SC.ServiceCenterName AS ServiceCenterName  
 ,MarketerId  
 ,ISNULL(dbo.fn_GetMarketersFullName(MarketerId),'--') AS Marketer  
 ,ST.StateName     
 FROM Tbl_BookNumbers AS BN  
 JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  AND C.ActiveStatusId = 1
    JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1
    JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  AND SU.ActiveStatusId = 1
    JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  AND BU.ActiveStatusId = 1
 LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode  AND ST.IsActive = 1
   WHERE (BU.BU_ID=@BUID OR @BUID='')  
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END    
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateFiilePathCustomerDetails]    Script Date: 04/08/2015 21:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 08-APRIL-2015                    
 -- Description  : THIS PROCEDURE IS TO UPDATE FILE PATH BY CUSTOMER BILL ID
 -- =============================================================================                    
 ALTER PROCEDURE [dbo].[USP_UpdateFiilePathCustomerDetails]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
   BEGIN                    
  DECLARE @CustomerBillId VARCHAR(50) 
			,@BillFilePath VARCHAR(MAX)                 
  SELECT         
	@CustomerBillId = C.value('(CustomerBillId)[1]','VARCHAR(50)')                                                                
	,@BillFilePath = C.value('(FilePath)[1]','VARCHAR(MAX)')                                                                
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)  
	
	UPDATE Tbl_BillDetails SET BillFilePath=@BillFilePath 
	WHERE CustomerBillID=@CustomerBillId
	SELECT @@ROWCOUNT AS RowsEffected FOR XML PATH('BillGenerationBe'),TYPE
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 04/08/2015 21:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<NEERAJ KANOJIYA>
-- Create date: <26-MAR-2015>
-- Description:	<This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>
-- =============================================
--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]
( @XmlDoc Xml)
AS
BEGIN
	 
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@ServiceUnitId	VARCHAR(50)
			,@PoleId Varchar(50)
			,@ServiceCenterId VARCHAR(50)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	SELECT	@GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@Month = C.value('(BillMonth)[1]','VARCHAR(50)') 
			,@Year = C.value('(BillYear)[1]','VARCHAR(50)') 
			FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
			 
	IF(@GlobalAccountNumber !='')
	BEGIN
			
			 ----------------------------------------COPY START --------------------------------------------------------
			    
						SELECT 
						@ActiveStatusId = ActiveStatusId,
						@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =ClassName
						,@Service_HouseNo =Service_HouseNo
						,@Service_Street =Service_StreetName
						,@Service_City =Service_City
						,@ServiceZipCode  =Service_ZipCode
						,@Postal_HouseNo =Postal_HouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@BU_ID=BU_ID
						,@ServiceUnitId=ServiceCenterId
						,@PoleId=PoleID
						,@TariffId=ClassID
						,@ServiceCenterId=ServiceCenterId
						FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber 
						 
						 
						 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							
							 
							
						
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						   IF isnull(@ActiveStatusId,0) != 1    -- In Active Customer
								RETURN
									select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
									from Tbl_BillingDisabledBooks	  
									where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
						  IF 	isnull(@BookDisableType,0)   = 1
		 						RETURN
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE --@CustomerTypeID=1 -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
						IF  @ActiveStatusId =2 -- In Active Customers
						BEGIN	 
								 
							 
							SET	 @Usage=0
						END
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
								
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							END
								IF @BookDisableType = 2  -- Temparary Close
									BEGIN
											
										SET	 @EnergyCharges=0
									END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						IF	 @PrevCustomerBillId IS NULL 
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
					
					
			 
			----------------------------------------------------------COPY END-------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
			)        
			 Values( @GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber) 
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber) 
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))        
				,@BU_ID
				,@ServiceUnitId
				,@ServiceCenterId
				,@PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber
				 ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,@TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,@AverageUsageForNewBill
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
			       
		)
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL	
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)
	END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNumbers]    Script Date: 04/08/2015 21:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Description: The purpose of this procedure is to get list of Book Numbers    
-- Modified By: V.BhimaRaju    
--Modified Date: 27-02-2014    
--Modified Date: 28-03-2014    
-- Modified By: T.Karthik  
-- Modified Date: 27-10-2014  
-- Description: To Wrong in Table and For --- in Details    
-- Modified By: Padmini  
-- Modified Date:17-Feb-2015  
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBookNumbers]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT      
   ,@BUID VARCHAR(50)  
      
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY BN.ActiveStatusId ASC , BN.CreatedDate DESC ) AS RowNumber    
    ,BN.ID AS BookNo  
    ,(SELECT dbo.fn_GetCycleIdByBook(BN.BookNo)) As CycleId  
    ,(SELECT CycleName From Tbl_Cycles WHERE CycleId=(SELECT dbo.fn_GetCycleIdByBook(BookNo))) CycleName  
    ,BN.ActiveStatusId    
    ,BN.CreatedBy    
    ,BN.NoOfAccounts    
    --,Details    
    ,ISNULL(BN.Details,'---')AS Details    
    ,BU.BU_ID    
    ,SU.SU_ID    
    ,SC.ServiceCenterId    
    ,ISNULL(BN.BookCode,'---')AS BookCode    
    ,BU.BusinessUnitName AS BusinessUnitName    
    ,SU.ServiceUnitName  AS ServiceUnitName    
    ,SC.ServiceCenterName AS ServiceCenterName  
 ,MarketerId  
 ,ISNULL(dbo.fn_GetMarketersFullName(MarketerId),'--') AS Marketer  
 ,ST.StateName     
 FROM Tbl_BookNumbers AS BN  
 JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  AND C.ActiveStatusId = 1
    JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1
    JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  AND SU.ActiveStatusId = 1
    JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  AND BU.ActiveStatusId = 1
 LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode  AND ST.IsActive = 1
   WHERE (BU.BU_ID=@BUID OR @BUID='')  
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END    
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCycles]    Script Date: 04/08/2015 21:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Modified date: 27-10-2014    
-- Description: The purpose of this procedure is to get list of Cycles    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCycles]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT     
   ,@BUID VARCHAR(50)     
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY C.ActiveStatusId ASC , C.CreatedDate DESC ) AS RowNumber    
    ,C.CycleId    
    ,C.CycleName    
    --,CycleNo    
    ,ISNULL(C.DetailsOfCycle,'---') AS Details    
    ,C.ActiveStatusId    
    ,C.CreatedBy  
    --,C.BU_ID    
    --,C.SU_ID    
    ,B.BU_ID    
    ,S.SU_ID  
    ,C.ServiceCenterId    
    ,ISNULL(C.CycleCode,'--') AS CycleCode  
    ,B.BusinessUnitName  
    ,ST.StateName  
    ,S.ServiceUnitName  
    ,SC.ServiceCenterName  
   FROM Tbl_Cycles AS C  
    LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1
 LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID  AND S.ActiveStatusId = 1
 LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  AND B.ActiveStatusId = 1
 LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode  AND ST.IsActive = 1
 AND C.ActiveStatusId in (1,2)
 
   WHERE (B.BU_ID=@BUID OR @BUID='')    
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/08/2015 21:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
		 
			BEGIN TRY		    
					   
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						 
						
						
				 
						 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							
							 
							
						
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						   IF	 isnull(@ActiveStatusId,0) != 1    -- In Active Customer
								BREAK;
						 
						 
									select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
									from Tbl_BillingDisabledBooks	  
									where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
							
						  IF 	isnull(@BookDisableType,0)   = 1
		 						BREAK;
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE --@CustomerTypeID=1 -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
						IF  @ActiveStatusId =2 -- In Active Customers
						BEGIN	 
								 
							 
							SET	 @Usage=0
						END
						
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
								
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							END
									  
						
								IF @BookDisableType = 2  -- Temparary Close
									BEGIN
											
										SET	 @EnergyCharges=0
									END
						------------------------------------------------------------------------------------------------------------------------------------
						
						
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						
						 
						IF	 @PrevCustomerBillId IS NULL 
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						
							
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						   --BREAK
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						   IF(@GlobalAccountNumber IS NULL) break;        
							Continue        
						END
			END TRY
			BEGIN CATCH
					 PRINT 'Exception'
					 PRINT @GlobalAccountNumber
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO


