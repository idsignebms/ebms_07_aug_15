
GO

/****** Object:  StoredProcedure [dbo].[UPS_GetBillTextFilesForCustomers]    Script Date: 04/09/2015 20:41:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <19-March-2014>        
-- Description: <GET EBILL DAILY DETAILS>        
-- =============================================        
CREATE PROCEDURE  [dbo].[UPS_GetBillTextFilesForCustomers]       
(      
@XmlDoc XML     
)       
AS        
BEGIN       
    
Declare @Month INT,      
  @Year INT    
      
SELECT      
   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
       
    
 SELECT CY.ServiceCenterId ,    
   BS.BillingYear,    
   BillingMonth,    
   SCDetails.BusinessUnitName,    
   SCDetails.ServiceCenterName,      
   SCDetails.ServiceUnitName,    
   '~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,     
   --MAX(BS.BillingFile) As FilePath,      
   dbo.fn_GetMonthName(BillingMonth) as [MonthName]      
 FROM Tbl_BillingQueueSchedule BS        
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId        
 and BillGenarationStatusId=1    
 AND BS.BillingMonth=2--@Month     
 AND BS.BillingYear=2015--@Year      
 INNER JOIN      
 [UDV_ServiceCenterDetails] SCDetails      
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId      
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth       
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,      
 SCDetails.ServiceUnitName        
    
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForContactInfoByAccno]    Script Date: 04/09/2015 20:41:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author: Karteek
-- Create date: 09-04-2015 
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For Contact info 
-- =============================================    
CREATE PROCEDURE [dbo].[USP_GetCustDetForContactInfoByAccno]    
(    
	@XmlDoc xml    
)    
AS    
BEGIN
    
	DECLARE @AccountNo VARCHAR(50)

	SELECT  @AccountNo = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')		
	FROM @XmlDoc.nodes('ChangeContactBE') as T(C)    

	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
				 CD.GlobalAccountNumber AS GlobalAccountNumber
				,CD.AccountNo As AccountNo
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
						,CD.Service_Landmark    
						,CD.Service_City,'',    
						CD.Service_ZipCode) AS ServiceAddress
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				,ISNULL(CD.EmailId,'--') AS OldEmailId
				,ISNULL(CD.HomeContactNo,'--') AS OldHomeContactNumber
				,ISNULL(CD.BusinessContactNo,'--') AS OldBusinessContactNumber
				,ISNULL(CD.OtherContactNo,'--') AS OldOtherContactNumber
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
			FROM UDV_CustomerDescription CD
			WHERE (CD.GlobalAccountNumber = @AccountNo  OR OldAccountNo = @AccountNo)
			AND ActiveStatusId IN (1,2)  
			FOR XML PATH('ChangeContactBE')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeContactBE')  
		END
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerContactInfo]    Script Date: 04/09/2015 20:41:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================   
-- Author: Karteek
-- Create date: 09-04-2015  
-- Description: The purpose this procedure is to inserting changes of customer contact details
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ChangeCustomerContactInfo]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @GlobalAccountNumber VARCHAR(50)  
		,@NewEmailId VARCHAR(100)  
		,@NewHomeContactNumber VARCHAR(20)  
		,@NewBusinessContactNumber VARCHAR(20)  
		,@NewOtherContactNumber VARCHAR(20)  
		,@ModifiedBy VARCHAR(50)  
		,@Details VARCHAR(MAX)  
		,@ApprovalStatusId INT 
		,@IsFinalApproval BIT 
		,@FunctionId INT
    
	SELECT @GlobalAccountNumber = C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@NewEmailId = C.value('(NewEmailId)[1]','VARCHAR(100)')  
		,@NewHomeContactNumber = C.value('(NewHomeContactNumber)[1]','VARCHAR(20)')  
		,@NewBusinessContactNumber = C.value('(NewBusinessContactNumber)[1]','VARCHAR(20)')  
		,@NewOtherContactNumber = C.value('(NewOtherContactNumber)[1]','VARCHAR(50)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(20)')  
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
	FROM @XmlDoc.nodes('ChangeContactBE') as T(C)  
   
	IF((SELECT COUNT(0) FROM Tbl_CustomerContactInfoChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApprovalStatusId IN (1,4)) = 0)  
		BEGIN  		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ContactChangeRequestId INT
			
			--SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			--SET @CurrentLevel = 0 -- For Stating Level			
			
			--IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
			--	BEGIN
			--		SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
			--	END
					
			--SELECT @PresentRoleId = PresentRoleId 
			--	,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
		
			INSERT INTO Tbl_CustomerContactInfoChangeLogs(  
				 GlobalAccountNumber 
				,OldEmailId  
				,NewEmailId  
				,OldHomeContactNumber  
				,NewHomeContactNumber  
				,OldBusinessContactNumber  
				,NewBusinessContactNumber  
				,OldOtherContactNumber  
				,NewOtherContactNumber 
				,CreatedBy  
				,CreatedDate  
				,ApprovalStatusId  
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole) 
			SELECT @GlobalAccountNumber  
				,CASE EmailId WHEN '' THEN NULL ELSE EmailId END    
				,CASE @NewEmailId WHEN '' THEN NULL ELSE @NewEmailId END   
				,CASE HomeContactNumber WHEN '' THEN NULL ELSE HomeContactNumber END   
				,CASE @NewHomeContactNumber WHEN '' THEN NULL ELSE @NewHomeContactNumber END  
				,CASE BusinessContactNumber WHEN '' THEN NULL ELSE BusinessContactNumber END  
				,CASE @NewBusinessContactNumber WHEN '' THEN NULL ELSE @NewBusinessContactNumber END   
				,CASE OtherContactNumber WHEN '' THEN NULL ELSE OtherContactNumber END     
				,CASE @NewOtherContactNumber WHEN '' THEN NULL ELSE @NewOtherContactNumber END  
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()  
				,@ApprovalStatusId  
				,@Details  
				,@PresentRoleId
				,@NextRoleId
			FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			SET @ContactChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 EmailId = (CASE @NewEmailId WHEN '' THEN NULL ELSE @NewEmailId END) 
						,HomeContactNumber = (CASE @NewHomeContactNumber WHEN '' THEN NULL ELSE @NewHomeContactNumber END)  
						,BusinessContactNumber = (CASE @NewBusinessContactNumber WHEN '' THEN NULL ELSE @NewBusinessContactNumber END)  
						,OtherContactNumber = (CASE @NewOtherContactNumber WHEN '' THEN NULL ELSE @NewOtherContactNumber END)  
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerContactInfoChangeLogs(  
						 CustomerContactInfoChangeLogId
						,GlobalAccountNumber  
						,OldEmailId  
						,NewEmailId  
						,OldHomeContactNumber  
						,NewHomeContactNumber  
						,OldBusinessContactNumber  
						,NewBusinessContactNumber  
						,OldOtherContactNumber  
						,NewOtherContactNumber 
						,CreatedBy  
						,CreatedDate  
						,ApprovalStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 CustomerContactInfoChangeLogId
						,GlobalAccountNumber  
						,OldEmailId  
						,NewEmailId  
						,OldHomeContactNumber  
						,NewHomeContactNumber  
						,OldBusinessContactNumber  
						,NewBusinessContactNumber  
						,OldOtherContactNumber  
						,NewOtherContactNumber 
						,CreatedBy  
						,CreatedDate  
						,ApprovalStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_CustomerContactInfoChangeLogs WHERE CustomerContactInfoChangeLogId = @ContactChangeRequestId
					
				END
  
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeContactBE')  
		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeContactBE')  
		END  
    
END  
  
---------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType_Rajaiah]    Script Date: 04/09/2015 20:41:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
-- EXEC USP_RptGetBillingStatusForCustomerType_Rajaiah
CREATE PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType_Rajaiah] 
	-- Add the parameters for the stored procedure here
(      
@XmlDoc Xml=null      
)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	--SET @Month=2
	--SET @Year=2015
	--SET @BU_ID='BEDC_BU_0001'
	
	SELECT   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
  FROM @XmlDoc.nodes('NewReportsBe') AS T(C)  
  
    -- Insert statements for procedure here
    SELECT	CT.CustomerType,
			CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			ISNULL(CBills.TotalBillAmountWithTax,0) AS AmountBilled,
			--WAT,
			--PIM,
			CD.ActiveStatus AS CustomerStatus,
			RC.ReadCode,
			ISNULL(CBills.PaidAmount,0) AS TotalCollection,
			ISNULL(CBills.AdjustmentAmmount,0) AS TotalAdjustment,
			ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance
			INTO #tmpCustomerServices
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
			CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
			AND CD.GlobalAccountNumber=CBills.AccountNo
	INNER JOIN Tbl_MCustomerTypes CT(NOLOCK) ON CT.CustomerTypeId=CD.CustomerTypeId
	INNER JOIN Tbl_MReadCodes RC(NOLOCK) ON RC.ReadCodeId=CBills.ReadCodeId 

	--SELECT * FROM #tmpCustomerServices

	SELECT --ROW_NUMBER() OVER(ORDER BY CustomerType,ServiceCenter ASC) AS ID,
		CustomerType,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(AmountBilled) As AmountBilled,
		SUM(ISNULL(Minimum,0) ) AS [MIN FC],
		SUM(ISNULL([Read],0)) AS [Read],
		SUM(ISNULL(Estimate,0) ) AS EST,
		SUM(ISNULL(Direct,0)) AS Direct,
		SUM(ISNULL(NoOfBilled,0)) AS NoOfBilled,  
		SUM(ISNULL(Response,0)) AS Response,
		SUM(TotalCollection) AS TotalCollection,
		SUM(TotalAdjustment) AS TotalAdjustment,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) AS ClossingBalance
		INTO #tmpCustomerServicesInfo
	FROM
	(
		SELECT	CustomerType,
			COUNT(CustomerStatus) AS TotalCustomerTariff,		
			CustomerStatus,
			SUM(EnergyBilled) AS EnergyBilled, 
			SUM(RevenueBilled) AS RevenueBilled,
			SUM(AmountBilled) AS AmountBilled,
			COUNT(ReadCode) AS ReadCodeType,
			ReadCode,
			COUNT(AmountBilled) AS NoOfBilled,  
			COUNT(TotalCollection) AS Response,
			SUM(TotalCollection) AS TotalCollection,
			SUM(TotalAdjustment) AS TotalAdjustment,
			SUM(OpeningBalance) AS OpeningBalance,
			SUM(ClossingBalance) AS ClossingBalance
		FROM #tmpCustomerServices
		GROUP BY CustomerStatus,CustomerType,ReadCode
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	PIVOT (SUM(ReadCodeType) FOR ReadCode IN(Direct,[Read],Estimate,Minimum)) AS  ListOfReadCodeType
	GROUP BY CustomerType
	ORDER BY CustomerType

	
	
	--==========================================================================================================
	DECLARE @tblCustomerServices AS TABLE
	(
		ID INT IDENTITY(1,1),
		CustomerType VARCHAR(100),
		Active INT,
		InActive INT,
		--Hold INT,
		TotalPopulation INT,
		EnergyBilled DECIMAL(18,2),
		RevenueBilled DECIMAL(18,2),
		AmountBilled DECIMAL(18,2),
		[MIN FC] INT,
		[Read] INT,
		EST INT, 
		Direct INT,
		NoOfBilled INT,
		Response INT,
		TotalCollection DECIMAL(18,2),
		TotalAdjustment DECIMAL(18,2),
		OpeningBalance DECIMAL(18,2),
		ClossingBalance DECIMAL(18,2)
	)
	INSERT INTO @tblCustomerServices(CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance)
	SELECT CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance
	FROM #tmpCustomerServicesInfo
	
	INSERT INTO @tblCustomerServices(CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance)
	SELECT 'SUB Total' AS CustomerType,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(TotalPopulation) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(AmountBilled) As AmountBilled,
		SUM(ISNULL([MIN FC],0) ) AS [MIN FC],
		SUM(ISNULL([Read],0)) AS [Read],
		SUM(ISNULL(EST,0) ) AS EST,
		SUM(ISNULL(Direct,0)) AS Direct,
		SUM(ISNULL(NoOfBilled,0)) AS NoOfBilled,  
		SUM(ISNULL(Response,0)) AS Response,
		SUM(TotalCollection) AS TotalCollection,
		SUM(TotalAdjustment) AS TotalAdjustment,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) AS ClossingBalance		
	FROM #tmpCustomerServicesInfo

	SELECT CustomerType,Active,	TotalPopulation,
		'' AS EnergyDelivered,EnergyBilled ,RevenueBilled,AmountBilled,
		'' AS WAT, '' AS PIM,[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance
	FROM @tblCustomerServices
	
	DROP TABLE #tmpCustomerServices
	DROP TABLE #tmpCustomerServicesInfo

	
END

GO



GO

/****** Object:  StoredProcedure [dbo].[UPS_GetBillTextFilesForCustomers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <19-March-2014>        
-- Description: <GET EBILL DAILY DETAILS>        
-- =============================================        
ALTER PROCEDURE  [dbo].[UPS_GetBillTextFilesForCustomers]       
(      
@XmlDoc XML     
)       
AS        
BEGIN       
    
Declare @Month INT,      
  @Year INT    
      
SELECT      
   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
       
    
 SELECT CY.ServiceCenterId ,    
   BS.BillingYear,    
   BillingMonth,    
   SCDetails.BusinessUnitName,    
   SCDetails.ServiceCenterName,      
   SCDetails.ServiceUnitName,    
   '~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,     
   --MAX(BS.BillingFile) As FilePath,      
   dbo.fn_GetMonthName(BillingMonth) as [MonthName]      
 FROM Tbl_BillingQueueSchedule BS        
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId        
 and BillGenarationStatusId=1    
 AND BS.BillingMonth=2--@Month     
 AND BS.BillingYear=2015--@Year      
 INNER JOIN      
 [UDV_ServiceCenterDetails] SCDetails      
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId      
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth       
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,      
 SCDetails.ServiceUnitName        
    
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCycles]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  T.Karthik      
-- Create date: 26-02-2014      
-- Modified date: 27-10-2014      
-- Description: The purpose of this procedure is to get list of Cycles      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetCycles]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @PageNo INT      
   ,@PageSize INT       
   ,@BUID VARCHAR(50)       
          
  SELECT         
   @PageNo = C.value('(PageNo)[1]','INT')      
   ,@PageSize = C.value('(PageSize)[1]','INT')       
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')      
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  ;WITH PagedResults AS      
  (      
   SELECT      
     ROW_NUMBER() OVER(ORDER BY C.ActiveStatusId ASC , C.CreatedDate DESC ) AS RowNumber      
    ,C.CycleId      
    ,C.CycleName      
    --,CycleNo      
    ,ISNULL(C.DetailsOfCycle,'---') AS Details      
    ,C.ActiveStatusId      
    ,C.CreatedBy    
    --,C.BU_ID      
    --,C.SU_ID      
    ,B.BU_ID      
    ,S.SU_ID    
    ,C.ServiceCenterId      
    ,ISNULL(C.CycleCode,'--') AS CycleCode    
    ,B.BusinessUnitName    
    ,ST.StateName    
    ,S.ServiceUnitName    
    ,SC.ServiceCenterName    
   FROM Tbl_Cycles AS C    
 JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1  
 JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID  AND S.ActiveStatusId = 1  
 JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  AND B.ActiveStatusId = 1  
 JOIN Tbl_States ST ON ST.StateCode=B.StateCode  AND ST.IsActive = 1  
 AND C.ActiveStatusId in (1,2)  
   
   WHERE (B.BU_ID=@BUID OR @BUID='')      
  )      
        
  SELECT       
    (      
   SELECT *      
     ,(Select COUNT(0) from PagedResults) as TotalRecords           
   FROM PagedResults      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
   FOR XML PATH('MastersBE'),TYPE      
  )      
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')       
END    
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNumbers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  T.Karthik      
-- Create date: 26-02-2014      
-- Description: The purpose of this procedure is to get list of Book Numbers      
-- Modified By: V.BhimaRaju      
--Modified Date: 27-02-2014      
--Modified Date: 28-03-2014      
-- Modified By: T.Karthik    
-- Modified Date: 27-10-2014    
-- Description: To Wrong in Table and For --- in Details      
-- Modified By: Padmini    
-- Modified Date:17-Feb-2015    
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order    
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBookNumbers]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @PageNo INT      
   ,@PageSize INT        
   ,@BUID VARCHAR(50)    
        
  SELECT         
   @PageNo = C.value('(PageNo)[1]','INT')      
   ,@PageSize = C.value('(PageSize)[1]','INT')       
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  ;WITH PagedResults AS      
  (      
   SELECT      
     ROW_NUMBER() OVER(ORDER BY BN.ActiveStatusId ASC , BN.CreatedDate DESC ) AS RowNumber      
    ,BN.ID AS BookNo    
    ,(SELECT dbo.fn_GetCycleIdByBook(BN.BookNo)) As CycleId    
    ,(SELECT CycleName From Tbl_Cycles WHERE CycleId=(SELECT dbo.fn_GetCycleIdByBook(BookNo))) CycleName    
    ,BN.ActiveStatusId      
    ,BN.CreatedBy      
    ,BN.NoOfAccounts      
    --,Details      
    ,ISNULL(BN.Details,'---')AS Details      
    ,BU.BU_ID      
    ,SU.SU_ID      
    ,SC.ServiceCenterId      
    ,ISNULL(BN.BookCode,'---')AS BookCode      
    ,BU.BusinessUnitName AS BusinessUnitName      
    ,SU.ServiceUnitName  AS ServiceUnitName      
    ,SC.ServiceCenterName AS ServiceCenterName    
 ,MarketerId    
 ,ISNULL(dbo.fn_GetMarketersFullName(MarketerId),'--') AS Marketer    
 ,ST.StateName       
 ,BN.BookNo AS BNO
 FROM Tbl_BookNumbers AS BN    
 JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  AND C.ActiveStatusId = 1  
    JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1  
    JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  AND SU.ActiveStatusId = 1  
    JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  AND BU.ActiveStatusId = 1  
 JOIN Tbl_States ST ON ST.StateCode=BU.StateCode  AND ST.IsActive = 1  
   WHERE (BU.BU_ID=@BUID OR @BUID='')    
  )      
        
  SELECT       
    (      
   SELECT *      
     ,(Select COUNT(0) from PagedResults) as TotalRecords           
   FROM PagedResults      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('MastersBE'),TYPE      
  )      
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')       
END      

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerContactInfo]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================   
-- Author: Karteek
-- Create date: 09-04-2015  
-- Description: The purpose this procedure is to inserting changes of customer contact details
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerContactInfo]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @GlobalAccountNumber VARCHAR(50)  
		,@NewEmailId VARCHAR(100)  
		,@NewHomeContactNumber VARCHAR(20)  
		,@NewBusinessContactNumber VARCHAR(20)  
		,@NewOtherContactNumber VARCHAR(20)  
		,@ModifiedBy VARCHAR(50)  
		,@Details VARCHAR(MAX)  
		,@ApprovalStatusId INT 
		,@IsFinalApproval BIT 
		,@FunctionId INT
    
	SELECT @GlobalAccountNumber = C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@NewEmailId = C.value('(NewEmailId)[1]','VARCHAR(100)')  
		,@NewHomeContactNumber = C.value('(NewHomeContactNumber)[1]','VARCHAR(20)')  
		,@NewBusinessContactNumber = C.value('(NewBusinessContactNumber)[1]','VARCHAR(20)')  
		,@NewOtherContactNumber = C.value('(NewOtherContactNumber)[1]','VARCHAR(50)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(20)')  
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
	FROM @XmlDoc.nodes('ChangeContactBE') as T(C)  
   
	IF((SELECT COUNT(0) FROM Tbl_CustomerContactInfoChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApprovalStatusId IN (1,4)) = 0)  
		BEGIN  		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ContactChangeRequestId INT
			
			--SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			--SET @CurrentLevel = 0 -- For Stating Level			
			
			--IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
			--	BEGIN
			--		SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
			--	END
					
			--SELECT @PresentRoleId = PresentRoleId 
			--	,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
		
			INSERT INTO Tbl_CustomerContactInfoChangeLogs(  
				 GlobalAccountNumber 
				,OldEmailId  
				,NewEmailId  
				,OldHomeContactNumber  
				,NewHomeContactNumber  
				,OldBusinessContactNumber  
				,NewBusinessContactNumber  
				,OldOtherContactNumber  
				,NewOtherContactNumber 
				,CreatedBy  
				,CreatedDate  
				,ApprovalStatusId  
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole) 
			SELECT @GlobalAccountNumber  
				,CASE EmailId WHEN '' THEN NULL ELSE EmailId END    
				,CASE @NewEmailId WHEN '' THEN NULL ELSE @NewEmailId END   
				,CASE HomeContactNumber WHEN '' THEN NULL ELSE HomeContactNumber END   
				,CASE @NewHomeContactNumber WHEN '' THEN NULL ELSE @NewHomeContactNumber END  
				,CASE BusinessContactNumber WHEN '' THEN NULL ELSE BusinessContactNumber END  
				,CASE @NewBusinessContactNumber WHEN '' THEN NULL ELSE @NewBusinessContactNumber END   
				,CASE OtherContactNumber WHEN '' THEN NULL ELSE OtherContactNumber END     
				,CASE @NewOtherContactNumber WHEN '' THEN NULL ELSE @NewOtherContactNumber END  
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()  
				,@ApprovalStatusId  
				,@Details  
				,@PresentRoleId
				,@NextRoleId
			FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			SET @ContactChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 EmailId = (CASE @NewEmailId WHEN '' THEN NULL ELSE @NewEmailId END) 
						,HomeContactNumber = (CASE @NewHomeContactNumber WHEN '' THEN NULL ELSE @NewHomeContactNumber END)  
						,BusinessContactNumber = (CASE @NewBusinessContactNumber WHEN '' THEN NULL ELSE @NewBusinessContactNumber END)  
						,OtherContactNumber = (CASE @NewOtherContactNumber WHEN '' THEN NULL ELSE @NewOtherContactNumber END)  
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerContactInfoChangeLogs(  
						 CustomerContactInfoChangeLogId
						,GlobalAccountNumber  
						,OldEmailId  
						,NewEmailId  
						,OldHomeContactNumber  
						,NewHomeContactNumber  
						,OldBusinessContactNumber  
						,NewBusinessContactNumber  
						,OldOtherContactNumber  
						,NewOtherContactNumber 
						,CreatedBy  
						,CreatedDate  
						,ApprovalStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 CustomerContactInfoChangeLogId
						,GlobalAccountNumber  
						,OldEmailId  
						,NewEmailId  
						,OldHomeContactNumber  
						,NewHomeContactNumber  
						,OldBusinessContactNumber  
						,NewBusinessContactNumber  
						,OldOtherContactNumber  
						,NewOtherContactNumber 
						,CreatedBy  
						,CreatedDate  
						,ApprovalStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_CustomerContactInfoChangeLogs WHERE CustomerContactInfoChangeLogId = @ContactChangeRequestId
					
				END
  
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeContactBE')  
		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeContactBE')  
		END  
    
END  
  
---------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	-- Xml data reading
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
		 
			BEGIN TRY		    
					   
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						 
						
						
				 
						 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						PRINT @GlobalAccountNumber
							 
							 
						   IF	 isnull(@ActiveStatusId,0) != 1 
						   BEGIN
						   PRINT '----------'
						   PRINT 'In Active'
						   PRINT @GlobalAccountNumber
						   PRINT @ActiveStatusId
						   PRINT '----------'
						   BREAK;
						   END   -- In Active Customer
						   
							
						 
						 
							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
					
						  IF 	isnull(@BookDisableType,0)   = 1
		 						BREAK;
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE --@CustomerTypeID=1 -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
					
						 
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
							 
							
									  
						
								
						------------------------------------------------------------------------------------------------------------------------------------
						
						
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						
						 
						  
						 
						IF	 @PrevCustomerBillId IS NULL 
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						   --if  @GlobalAccountNumber='0000057195' BREAK
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						   IF(@GlobalAccountNumber IS NULL) break;        
							Continue        
						END
			END TRY
			BEGIN CATCH
					 
					 
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBookNumberDetails]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 26-02-2014  
-- Description: The purpose of this procedure is to update Book Number details  
-- ModifiedBy: V.BhimaRaju  
--Modified Date:27-02-2014  
--Modified Date:29-03-2014  
-- ModifiedBy : T.Karthik  
-- Modified date: 01-12-2014  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateBookNumberDetails]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @ID VARCHAR(20)
 ,@ModifiedBy VARCHAR(50)
 ,@NoOfAccounts INT
 ,@CycleId VARCHAR(20)
 --,@ServiceCenterIdNew VARCHAR(20)
 ,@BookNo Varchar(20)
 --@BU_ID VARCHAR(20)  
 --,@SU_ID VARCHAR(20)  
 ,@ServiceCenterId VARCHAR(20)  
 ,@Details VARCHAR(MAX)  
 ,@CurrentDate DATETIME ,@BookCode VARCHAR(10),@MarketerId INT  
 ,@OldServiceCenterId varchar(20)
 SELECT @CurrentDate  = dbo.fn_GetCurrentDateTime()  
   
 SELECT  
  @ID=C.value('(BookNo)[1]','VARCHAR(20)')  
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@NoOfAccounts=C.value('(NoOfAccounts)[1]','INT')  
  ,@MarketerId=C.value('(MarketerId)[1]','INT')  
  --,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')  
  --,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')  
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')  
  ,@CycleId=C.value('(CycleId)[1]','VARCHAR(20)')  
  --,@BookCode=C.value('(BookCode)[1]','VARCHAR(10)')  
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@OldServiceCenterId=C.value('(OldSeviceCenterID)[1]','VARCHAR(20)')  
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 --IF EXISTS(SELECT 0 FROM Tbl_BookNumbers WHERE BookCode=@BookCode AND ServiceCenterId=@ServiceCenterId AND BookNo != @BookNo)  
 --BEGIN  
 -- SELECT 1 AS IsBookCodeExists FOR XML PATH('MastersBE')  
 --END  
 --ELSE  
 --BEGIN  
 --===================
 declare @OldCycleId VARCHAR(50)
 
 select @OldCycleId = CycleId from Tbl_Cycles 
	WHERE ServiceCenterId = @OldServiceCenterId
	and CycleId in(select CycleId from Tbl_BookNumbers where ID = @ID)
 
--set @ServiceCenterIdNew = (select top 1 ServiceCenterId from Tbl_Cycles where CycleId = @CycleId)
-- set @ServiceCenterId = (select top 1 ServiceCenterId from Tbl_Cycles 
--							where CycleId = (SELECT top 1 CycleId from Tbl_BookNumbers 
--												where ID=@ID)
--							)
--===================
  IF (@ServiceCenterId = @OldServiceCenterId)
Begin
	SET @BookCode = (Select BookCode from Tbl_BookNumbers where ID = @ID and CycleId = @OldCycleId) 
end
else
Begin
	SET @BookCode = MASTERS.fn_BookCodeGenerate(@ServiceCenterId) 
end
  
 
 
   UPDATE Tbl_BookNumbers 
   SET ModifiedBy=@ModifiedBy
   ,ModifiedDate=@CurrentDate
   ,NoOfAccounts=CASE @NoOfAccounts WHEN 0 THEN NULL ELSE @NoOfAccounts END  
    --,BU_ID=@BU_ID  
    --,SU_ID=@SU_ID  
    --,ServiceCenterId=@ServiceCenterId  
    ,Details=CASE @Details WHEN '' THEN NULL ELSE @Details END  
    ,CycleId=@CycleId  
    ,BookCode=@BookCode  
    ,MarketerId=@MarketerId  
   WHERE ID=@ID and CycleId = @OldCycleId  
   --AND ServiceCenterId=@ServiceCenterId   
     
   --INSERT INTO Tbl_Audit_BookNumbers(BookNo,ID,CycleId,ModifiedBy,ModifiedDate  
   --           ,BU_ID,SU_ID,ServiceCenterId,NoOfAccounts,Details,BookCode  
   --           ,StatusId,ActiveStatusId)  
   --       VALUES(@BookNo,NULL,@CycleId,@ModifiedBy,@CurrentDate,@BU_ID,@SU_ID,@ServiceCenterId,  
   --       (CASE @NoOfAccounts WHEN 0 THEN NULL ELSE @NoOfAccounts END),(CASE @Details WHEN '' THEN NULL ELSE @Details END),  
   --       @BookCode,'U',1) --For Audit Table Updation  
     
  
   SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')  
 --END  
END


--select * from Tbl_BookNumbers
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForContactInfoByAccno]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author: Karteek
-- Create date: 09-04-2015 
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For Contact info 
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForContactInfoByAccno]    
(    
	@XmlDoc xml    
)    
AS    
BEGIN
    
	DECLARE @AccountNo VARCHAR(50)

	SELECT  @AccountNo = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')		
	FROM @XmlDoc.nodes('ChangeContactBE') as T(C)    

	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
				 CD.GlobalAccountNumber AS GlobalAccountNumber
				,CD.AccountNo As AccountNo
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
						,CD.Service_Landmark    
						,CD.Service_City,'',    
						CD.Service_ZipCode) AS ServiceAddress
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				,ISNULL(CD.EmailId,'--') AS OldEmailId
				,ISNULL(CD.HomeContactNo,'--') AS OldHomeContactNumber
				,ISNULL(CD.BusinessContactNo,'--') AS OldBusinessContactNumber
				,ISNULL(CD.OtherContactNo,'--') AS OldOtherContactNumber
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
			FROM UDV_CustomerDescription CD
			WHERE (CD.GlobalAccountNumber = @AccountNo  OR OldAccountNo = @AccountNo)
			AND ActiveStatusId IN (1,2)  
			FOR XML PATH('ChangeContactBE')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeContactBE')  
		END
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType_Rajaiah]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
-- EXEC USP_RptGetBillingStatusForCustomerType_Rajaiah
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType_Rajaiah] 
	-- Add the parameters for the stored procedure here
(      
@XmlDoc Xml=null      
)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	--SET @Month=2
	--SET @Year=2015
	--SET @BU_ID='BEDC_BU_0001'
	
	SELECT   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
  FROM @XmlDoc.nodes('NewReportsBe') AS T(C)  
  
    -- Insert statements for procedure here
    SELECT	CT.CustomerType,
			CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			ISNULL(CBills.TotalBillAmountWithTax,0) AS AmountBilled,
			--WAT,
			--PIM,
			CD.ActiveStatus AS CustomerStatus,
			RC.ReadCode,
			ISNULL(CBills.PaidAmount,0) AS TotalCollection,
			ISNULL(CBills.AdjustmentAmmount,0) AS TotalAdjustment,
			ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance
			INTO #tmpCustomerServices
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerDescription CD(NOLOCK) ON 
			CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
			AND CD.GlobalAccountNumber=CBills.AccountNo
	INNER JOIN Tbl_MCustomerTypes CT(NOLOCK) ON CT.CustomerTypeId=CD.CustomerTypeId
	INNER JOIN Tbl_MReadCodes RC(NOLOCK) ON RC.ReadCodeId=CBills.ReadCodeId 

	--SELECT * FROM #tmpCustomerServices

	SELECT --ROW_NUMBER() OVER(ORDER BY CustomerType,ServiceCenter ASC) AS ID,
		CustomerType,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(AmountBilled) As AmountBilled,
		SUM(ISNULL(Minimum,0) ) AS [MIN FC],
		SUM(ISNULL([Read],0)) AS [Read],
		SUM(ISNULL(Estimate,0) ) AS EST,
		SUM(ISNULL(Direct,0)) AS Direct,
		SUM(ISNULL(NoOfBilled,0)) AS NoOfBilled,  
		SUM(ISNULL(Response,0)) AS Response,
		SUM(TotalCollection) AS TotalCollection,
		SUM(TotalAdjustment) AS TotalAdjustment,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) AS ClossingBalance
		INTO #tmpCustomerServicesInfo
	FROM
	(
		SELECT	CustomerType,
			COUNT(CustomerStatus) AS TotalCustomerTariff,		
			CustomerStatus,
			SUM(EnergyBilled) AS EnergyBilled, 
			SUM(RevenueBilled) AS RevenueBilled,
			SUM(AmountBilled) AS AmountBilled,
			COUNT(ReadCode) AS ReadCodeType,
			ReadCode,
			COUNT(AmountBilled) AS NoOfBilled,  
			COUNT(TotalCollection) AS Response,
			SUM(TotalCollection) AS TotalCollection,
			SUM(TotalAdjustment) AS TotalAdjustment,
			SUM(OpeningBalance) AS OpeningBalance,
			SUM(ClossingBalance) AS ClossingBalance
		FROM #tmpCustomerServices
		GROUP BY CustomerStatus,CustomerType,ReadCode
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	PIVOT (SUM(ReadCodeType) FOR ReadCode IN(Direct,[Read],Estimate,Minimum)) AS  ListOfReadCodeType
	GROUP BY CustomerType
	ORDER BY CustomerType

	
	
	--==========================================================================================================
	DECLARE @tblCustomerServices AS TABLE
	(
		ID INT IDENTITY(1,1),
		CustomerType VARCHAR(100),
		Active INT,
		InActive INT,
		--Hold INT,
		TotalPopulation INT,
		EnergyBilled DECIMAL(18,2),
		RevenueBilled DECIMAL(18,2),
		AmountBilled DECIMAL(18,2),
		[MIN FC] INT,
		[Read] INT,
		EST INT, 
		Direct INT,
		NoOfBilled INT,
		Response INT,
		TotalCollection DECIMAL(18,2),
		TotalAdjustment DECIMAL(18,2),
		OpeningBalance DECIMAL(18,2),
		ClossingBalance DECIMAL(18,2)
	)
	INSERT INTO @tblCustomerServices(CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance)
	SELECT CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance
	FROM #tmpCustomerServicesInfo
	
	INSERT INTO @tblCustomerServices(CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance)
	SELECT 'SUB Total' AS CustomerType,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(TotalPopulation) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(AmountBilled) As AmountBilled,
		SUM(ISNULL([MIN FC],0) ) AS [MIN FC],
		SUM(ISNULL([Read],0)) AS [Read],
		SUM(ISNULL(EST,0) ) AS EST,
		SUM(ISNULL(Direct,0)) AS Direct,
		SUM(ISNULL(NoOfBilled,0)) AS NoOfBilled,  
		SUM(ISNULL(Response,0)) AS Response,
		SUM(TotalCollection) AS TotalCollection,
		SUM(TotalAdjustment) AS TotalAdjustment,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) AS ClossingBalance		
	FROM #tmpCustomerServicesInfo

	SELECT CustomerType,Active,	TotalPopulation,
		'' AS EnergyDelivered,EnergyBilled ,RevenueBilled,AmountBilled,
		'' AS WAT, '' AS PIM,[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance
	FROM @tblCustomerServices
	
	DROP TABLE #tmpCustomerServices
	DROP TABLE #tmpCustomerServicesInfo

	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookNo + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
	FROM
	UDV_PrebillingRpt CD
	LEFT JOIN Tbl_DirectCustomersAvgReadings DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	WHERE isnull(DAVG.AvgReadingId,0)=0 
	and  CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) 
	and  CD.ReadCodeID = 1 
	AND CD.ActiveStatusId=1  and CD.BoolDisableTypeId= 1 -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetPartialBillCustomers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
					 
	SELECT	    
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt CD
	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	
	UNION ALL
	
	SELECT 
		 CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 AND BD.IsPartialBill = 1
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	
END
---------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,Cr.Usage  AS Usage
		,CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
			AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
			AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
			AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,CAST(SUM(usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	 HAVING 
	 
	 
	 CAST(SUM(usage) AS DECIMAL(18,2)) 
	 
	 between   
	 AverageReading- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	  AverageReading +((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 
	 
		 
	DROP TABLE #ReadCustomersList
	
END
----------------------------------------------------------------------------------------------------
 


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD
	WHERE  isnull(CD.DeafaultUsage,0)=0  
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))

END
----------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNonReadCustomers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''
				,@ActiveStatusIds varchar(max)='2,3,4'

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,[usage] AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE CD.ReadCodeID = 2 AND CR.CustomerReadingId IS NULL 
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) 
		AND ActiveStatusId=1  and BoolDisableTypeId= 1 -- Disable books and	   Active Customers

END
--------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetReadCustomers]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

Modified Date : 09-04-2015

Description : 
In Active,Closed,Hold,Tempary CLose  Customers - No Need to Come
 Hold -- 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
			,@ActiveStatusIds varchar(max)='2,3,4'
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS INT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	AND ActiveStatusId=1  and BoolDisableTypeId= 1 -- Disable books and	   Active Customers
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy


	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	
	DROP TABLE #ReadCustomersList

END
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateFiilePathCustomerDetails]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 08-APRIL-2015                    
 -- Description  : THIS PROCEDURE IS TO UPDATE FILE PATH BY CUSTOMER BILL ID
 -- =============================================================================                    
 ALTER PROCEDURE [dbo].[USP_UpdateFiilePathCustomerDetails]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
   BEGIN                    
  DECLARE @CustomerBillId VARCHAR(50) 
			,@BillFilePath VARCHAR(MAX)                 
  SELECT         
	@CustomerBillId = C.value('(CustomerBillId)[1]','VARCHAR(50)')                                                                
	,@BillFilePath = C.value('(FilePath)[1]','VARCHAR(MAX)')                                                                
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)  
	
	UPDATE Tbl_BillDetails SET BillFilePath=@BillFilePath 
	WHERE CustomerBillID=@CustomerBillId
	SELECT @@ROWCOUNT AS RowsEffected FOR XML PATH('BillGenerationBe'),TYPE
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 04/09/2015 20:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<NEERAJ KANOJIYA>
-- Create date: <26-MAR-2015>
-- Description:	<This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>
-- =============================================
--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]
( @XmlDoc Xml)
AS
BEGIN
	 
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@ServiceUnitId	VARCHAR(50)
			,@PoleId Varchar(50)
			,@ServiceCenterId VARCHAR(50)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	SELECT	@GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@Month = C.value('(BillMonth)[1]','VARCHAR(50)') 
			,@Year = C.value('(BillYear)[1]','VARCHAR(50)') 
			FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
			 
	IF(@GlobalAccountNumber !='')
	BEGIN
			
			 ----------------------------------------COPY START --------------------------------------------------------
			    
						SELECT 
						@ActiveStatusId = ActiveStatusId,
						@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =ClassName
						,@Service_HouseNo =Service_HouseNo
						,@Service_Street =Service_StreetName
						,@Service_City =Service_City
						,@ServiceZipCode  =Service_ZipCode
						,@Postal_HouseNo =Postal_HouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@BU_ID=BU_ID
						,@ServiceUnitId=ServiceCenterId
						,@PoleId=PoleID
						,@TariffId=ClassID
						,@ServiceCenterId=ServiceCenterId
						FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber 
						 
						 
						 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							
							 
							
						
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						   IF isnull(@ActiveStatusId,0) != 1    -- In Active Customer
								RETURN
									select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
									from Tbl_BillingDisabledBooks	  
									where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
						  IF 	isnull(@BookDisableType,0)   = 1
		 						RETURN
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE --@CustomerTypeID=1 -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
						IF  @ActiveStatusId =2 -- In Active Customers
						BEGIN	 
								 
							 
							SET	 @Usage=0
						END
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
								
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							END
								IF @BookDisableType = 2  -- Temparary Close
									BEGIN
											
										SET	 @EnergyCharges=0
									END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						IF	 @PrevCustomerBillId IS NULL 
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0    
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
					
					
			 
			----------------------------------------------------------COPY END-------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
			)        
			 Values( @GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber) 
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber) 
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))        
				,@BU_ID
				,@ServiceUnitId
				,@ServiceCenterId
				,@PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber
				 ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,@TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,@AverageUsageForNewBill
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
			       
		)
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL	
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)
	END
END
GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/09/2015 21:42:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   
						   IF	 isnull(@ActiveStatusId,0) != 1 
						   BEGIN
						   GOTO Loop_End;
						   END   -- In Active Customer
						   
						   IF isnull(@CustomerTypeID,0) = 3	 
						   BEGIN
						   GOTO Loop_End;; --3		PREPAID  CUSTOMER TYPE
						   END
						 
						 
							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
					
						  IF 	isnull(@BookDisableType,0)   = 1
		 						GOTO Loop_End;
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE -- -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
					
						 
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
							 
							
									  
						
								
						------------------------------------------------------------------------------------------------------------------------------------
						
						
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH
					 
					 
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------
