GO
CREATE TABLE Tbl_CustomerContactInfoChangeLogs
(
	 CustomerContactInfoChangeLogId INT PRIMARY KEY IDENTITY(1,1)
	,GlobalAccountNumber VARCHAR(50)
	,OldEmailId VARCHAR(100)
	,NewEmailId VARCHAR(100)
	,OldHomeContactNumber VARCHAR(20) 
	,NewHomeContactNumber VARCHAR(20) 
	,OldBusinessContactNumber VARCHAR(20) 
	,NewBusinessContactNumber VARCHAR(20) 
	,OldOtherContactNumber VARCHAR(20) 
	,NewOtherContactNumber VARCHAR(20)
	,ApprovalStatusId INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
	,PresentApprovalRole INT
	,NextApprovalRole INT
	,Remarks VARCHAR(500)
)
GO
CREATE TABLE Tbl_Audit_CustomerContactInfoChangeLogs
(
	 CustomerContactInfoChangeLogId INT 
	,GlobalAccountNumber VARCHAR(50)
	,OldEmailId VARCHAR(100)
	,NewEmailId VARCHAR(100)
	,OldHomeContactNumber VARCHAR(20) 
	,NewHomeContactNumber VARCHAR(20) 
	,OldBusinessContactNumber VARCHAR(20) 
	,NewBusinessContactNumber VARCHAR(20) 
	,OldOtherContactNumber VARCHAR(20) 
	,NewOtherContactNumber VARCHAR(20)
	,ApprovalStatusId INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
	,PresentApprovalRole INT
	,NextApprovalRole INT
	,Remarks VARCHAR(500)
)
GO
------------------------------------------------------------------------------
GO
-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <19-MAR-2014>  
-- Description: <Insert Previous data of ServiceCenter into Audit table Before Update>  
-- Modified By: Padmini  
--Modified Date: 19th Feb 2015   
-- =============================================  
ALTER TRIGGER [dbo].[TR_InsertAuditServiceCenter]   ON  [dbo].[Tbl_ServiceCenter]  
   INSTEAD OF UPDATE  
AS   
BEGIN  
  
DECLARE    
     @ServiceCenterId   varchar(20)   ,  
  @ServiceCenterName   varchar(300)  ,  
  @Notes   varchar(max)  ,  
  @SU_ID   varchar(20)  ,  
  @SCCode   varchar(10)  ,  
  @Address1 varchar(100) ,  
  @Address2 varchar(100),  
  @City varchar(100),  
  @ZIP varchar(100),  
  @ActiveStatusId   int   ,  
  @ModifiedBy   varchar(50) ;  
    
  SELECT  
   @ServiceCenterId =I.ServiceCenterId  
   ,@ServiceCenterName=I.ServiceCenterName  
   ,@Notes=I.Notes  
   ,@SU_ID=I.SU_ID  
   ,@ActiveStatusId=I.ActiveStatusId  
   ,@ModifiedBy=I.ModifiedBy  
   ,@SCCode=I.SCCode  
   ,@Address1=I.Address1  
   ,@Address2=I.Address2  
   ,@City=I.City  
   ,@ZIP=I.ZIP  
  FROM inserted I;  
    
  INSERT INTO Tbl_Audit__ServiceCenter  
  (  
  ServiceCenterId  
  ,ServiceCenterName  
  ,Notes  
  ,SU_ID  
  ,ActiveStatusId  
  ,CreatedBy  
  ,CreatedDate  
  ,ModifiedBy  
  ,ModifiedDate  
  ,SCCode  
  ,Address1  
  ,Address2  
  ,City  
  ,ZIP  
  )  
  SELECT [ServiceCenterId]  
      ,[ServiceCenterName]  
      ,[Notes]  
      ,[SU_ID]  
      ,[ActiveStatusId]  
      ,[CreatedBy]  
      ,[CreatedDate]  
      ,[ModifiedBy]  
      ,[ModifiedDate]  
      ,[SCCode]  
      ,Address1  
      ,Address2  
      ,City  
      ,ZIP  
       FROM Tbl_ServiceCenter WHERE ServiceCenterId=@ServiceCenterId;  
    
  UPDATE Tbl_ServiceCenter SET  
  [ServiceCenterName]=@ServiceCenterName  
      ,[Notes]=@Notes  
      ,[SU_ID]=@SU_ID  
      ,[ActiveStatusId]=@ActiveStatusId  
      ,[ModifiedBy]=@ModifiedBy  
      ,[SCCode]=@SCCode  
      ,[Address1]=@Address1  
      ,Address2=@Address2  
      ,City=@City  
      ,ZIP=@ZIP  
      ,[ModifiedDate]=dbo.fn_GetCurrentDateTime()   
     WHERE ServiceCenterId=@ServiceCenterId    
        
END  