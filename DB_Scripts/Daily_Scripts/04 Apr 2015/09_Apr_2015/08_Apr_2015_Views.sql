
GO

/****** Object:  View [dbo].[UDV_PrebillingRpt]    Script Date: 04/09/2015 20:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
  
ALTER VIEW [dbo].[UDV_PrebillingRpt]    
  
AS   
 SELECT CD.GlobalAccountNumber  
 ,CD.AccountNo  
 ,CD.OldAccountNo  
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode) AS [ServiceAddress]   
   ,PD.SortOrder  
   ,BN.SortOrder AS BookSortOrder  
   ,CD.ConnectionDate  
   ,PD.PoleID    
   ,PD.TariffClassID AS TariffId    
   ,TC.ClassName    
   ,PD.ReadCodeID    
   ,BN.BookNo    
   ,BN.BookCode  
   ,BN.ID AS BookId    
   ,BU.BU_ID    
   ,BU.BusinessUnitName    
   ,BU.BUCode    
   ,SU.SU_ID    
   ,SU.ServiceUnitName    
   ,SU.SUCode    
   ,SC.ServiceCenterId    
   ,SC.ServiceCenterName    
   ,SC.SCCode    
   ,C.CycleId    
   ,C.CycleName    
   ,C.CycleCode  
   ,Pd.MeterNumber
   ,PD.ActiveStatusId   
   ,CAD.InitialBillingKWh as DeafaultUsage  
   ,CustomerStatus.StatusId as CustomerStatusID  
   ,PD.IsEmbassyCustomer
   ,Isnull(BDB.DisableTypeId,0) as BoolDisableTypeId
   ,Isnull(BDB.IsPartialBill,0) as IsPartialBook
      
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER Join CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID  
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID  
  INNER JOIn Tbl_MCustomerStatus AS CustomerStatus ON   CD.ActiveStatusId = CustomerStatus.StatusId
  LEFT JOIN Tbl_BillingDisabledBooks BDB ON BDB.BookNo=Pd.BookNo  and IsActive=1 
----------------------------------------------------------------------------------------------------  
  


GO


