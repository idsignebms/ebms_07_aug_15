
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetBillWiseBillAmountByAccountNo]    Script Date: 04/10/2015 22:00:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 10 Apr 2015
-- Description:	To get the bill wise paid due amount by Account number
-- =============================================
CREATE FUNCTION [dbo].[fn_GetBillWiseBillAmountByAccountNo]
(	
	 @AccountNo VARCHAR(50)
	,@PaidAmount DECIMAL(18,2)
)
RETURNS @PaidBills TABLE(SNo INT IDENTITY(1,1), CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
AS
BEGIN  

	DECLARE @CustomerPendingBills TABLE(SNo INT IDENTITY(1,1), CustomerBillId INT, BillNo VARCHAR(50), BillAmount DECIMAL(18,2))
	
	INSERT INTO @CustomerPendingBills(CustomerBillId, BillNo, BillAmount)
	SELECT CustomerBillId
		,BillNo
		,(ISNULL(TotalBillAmountWithTax,0) - ISNULL(PaidAmount,0)) AS BillAmount
	FROM Tbl_CustomerBills
	WHERE AccountNo = @AccountNo
	AND PaymentStatusID = 2
	ORDER BY BillNo ASC
	
	DECLARE @RemainingPaidAmount DECIMAL(18,2)
		,@PresentBillNo VARCHAR(50), @BillAmount DECIMAL(18,2)
		,@CustomerBillId INT
		,@SNo INT, @TotalCount INT
		
	SET @RemainingPaidAmount = @PaidAmount
	SET @SNo = 1
	SELECT @TotalCount = COUNT(0) FROM @CustomerPendingBills 
	
	WHILE(@SNo <= @TotalCount)
	BEGIN		
		SELECT @PresentBillNo = BillNo
			,@BillAmount = BillAmount
			,@CustomerBillId = CustomerBillId
			 FROM @CustomerPendingBills WHERE SNo = @SNo
		
		IF(@RemainingPaidAmount >= @BillAmount)
			BEGIN				
				INSERT INTO @PaidBills(CustomerBillId, BillNo, PaidAmount, BillStatus)
				VALUES(@CustomerBillId,@PresentBillNo,@BillAmount, 1)
				
				SET @RemainingPaidAmount = @RemainingPaidAmount - @BillAmount
			END
		ELSE 
			BEGIN				
				INSERT INTO @PaidBills(CustomerBillId, BillNo, PaidAmount, BillStatus)
				VALUES(@CustomerBillId,@PresentBillNo,@RemainingPaidAmount, 2)	
				
				SET @RemainingPaidAmount = 0
			END

		SET @SNo = @SNo + 1
			
		IF(@RemainingPaidAmount = 0)
			BREAK;
	END
	
	IF(@RemainingPaidAmount > 0)
		BEGIN
			INSERT INTO @PaidBills(PaidAmount, BillStatus)
			VALUES(@RemainingPaidAmount, 1)	
		END

RETURN 

END
GO


