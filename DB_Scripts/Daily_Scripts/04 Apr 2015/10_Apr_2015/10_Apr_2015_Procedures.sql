
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments]    Script Date: 04/10/2015 22:06:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 10-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
CREATE PROCEDURE [dbo].[USP_InsertBulkCustomerPayments]          
(          
	 @XmlDoc XML           
	,@MultiXmlDoc XML          
)          
AS          
BEGIN    

DECLARE @CreatedBy VARCHAR(50)    
	,@DocumentPath VARCHAR(MAX)          
	,@DocumentName VARCHAR(MAX)         
	,@BatchNo VARCHAR(50) 
	,@StatusText VARCHAR(200) = ''
	,@IsSuccess BIT = 0
	
	SELECT            
		 @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')          
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')          
		,@DocumentName = C.value('(DocumentName)[1]','VARCHAR(MAX)')        
		,@BatchNo = C.value('(BatchNo)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)    	

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), AccountNo VARCHAR(50), ReceiptNo VARCHAR(20),           
							PaymentMode VARCHAR(20), PaymentModeId INT, 
							AmountPaid DECIMAL(18,2), RecievedDate DATETIME)
	
	INSERT INTO @TempCustomer(AccountNo, ReceiptNo, PaymentMode, PaymentModeId, AmountPaid, RecievedDate)           
	SELECT           
		 T.c.value('(AccountNo)[1]','VARCHAR(50)')           
		,T.c.value('(ReceiptNO)[1]','VARCHAR(20)')           
		,T.c.value('(PaymentMode)[1]','VARCHAR(20)')          
		,T.c.value('(PaymentModeId)[1]','VARCHAR(20)')          
		,T.c.value('(PaidAmount)[1]','DECIMAL(18,2)')          
		,T.c.value('(PaymentDate)[1]','DATETIME')          
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Table1') AS T(c)    
							
	DECLARE @SNo INT, @TotalCount INT
			,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
			,@CustomerPaymentId INT
			
	DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
	SET @SNo = 1
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer    
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SELECT @PresentAccountNo = AccountNo, @PaidAmount = AmountPaid FROM @TempCustomer WHERE SNo = @SNo
					
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 AccountNo
						,ReceiptNo
						,PaymentModeId
						,@DocumentPath
						,@DocumentName
						,RecievedDate
						,2 -- For Bulk Upload
						,AmountPaid
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempCustomer WHERE AccountNo = @PresentAccountNo 
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @StatusText = 'Customer Pending Bills'
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
					
					SET @StatusText = 'Customer Bills Paymnets'
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					SET @StatusText = 'Customer Bills'
					UPDATE CB
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @StatusText = 'Success'
					SET @IsSuccess = 1
					SET @SNo = @SNo + 1
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess =0
			END CATCH		
		END
		
	SELECT   
		 @IsSuccess As IsSuccess
		,@StatusText AS StatusText
	FOR XML PATH('PaymentsBe'),TYPE
		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose12]    Script Date: 04/10/2015 22:06:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose12]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   
						   IF	 isnull(@ActiveStatusId,0) != 1 
						   BEGIN
						   GOTO Loop_End;
						   END   -- In Active Customer
						   
						   IF isnull(@CustomerTypeID,0) = 3	 
						   BEGIN
						   GOTO Loop_End;; --3		PREPAID  CUSTOMER TYPE
						   END
						 
						 
							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
					
						  IF 	isnull(@BookDisableType,0)   = 1
		 						GOTO Loop_End;
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE -- -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
					
						 
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
							 
							
									  
						
								
						------------------------------------------------------------------------------------------------------------------------------------
						
						
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH
					 
					 
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillPayments]    Script Date: 04/10/2015 22:06:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 10 Apr 2015
-- Description:	To insert the customer bill payments
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertCustomerBillPayments]
(
	@XmlDoc XML
)
AS
BEGIN
	
	DECLARE @AccountNo VARCHAR(50)  
		,@ReceiptNo VARCHAR(20)  
		,@PaymentMode INT  
		,@DocumentPath VARCHAR(MAX)  
		,@DocumentName VARCHAR(MAX) 
		,@Cashier VARCHAR(50)  
		,@CashOffice INT  
		,@CreatedBy VARCHAR(50)  
		,@BatchNo INT  
		,@PaidAmount DECIMAL(20,4)  
		,@PaymentRecievedDate VARCHAR(20)  
		,@CustomerPaymentId INT  
		,@EffectedRows INT  
		,@PaymentType INT
		,@IsCustomerExists BIT = 0
		,@IsSuccess BIT = 0
		,@StatusText VARCHAR(200) = ''
		
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@ReceiptNo = C.value('(ReceiptNo)[1]','VARCHAR(20)')  
		,@PaymentMode = C.value('(PaymentModeId)[1]','INT')  
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
		,@DocumentName = C.value('(Document)[1]','VARCHAR(MAX)') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@BatchNo = C.value('(BatchNo)[1]','INT')  
		,@PaidAmount = C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
		,@PaymentRecievedDate = C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
		,@PaymentType = C.value('(PaymentType)[1]','INT')  
	FROM @XmlDoc.nodes('MastersBE') AS T(C) 
	
	IF(@AccountNo IS NOT NULL)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @AccountNo
						,@ReceiptNo
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PaymentRecievedDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
					
					SET @StatusText = 'Customer Pending Bills'
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@AccountNo,@PaidAmount) 
					
					SET @StatusText = 'Customer Bills Paymnets'
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					SET @StatusText = 'Customer Bills'
					UPDATE CB
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @IsCustomerExists = 1
					SET @IsSuccess = 1
					SET @StatusText = 'Success'
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsCustomerExists = 0
				SET @IsSuccess =0
			END CATCH			
		END
	
	SELECT  
		 @IsCustomerExists AS IsCustomerExists  
		,@IsSuccess AS IsSuccess  
		,@StatusText AS StatusText
	FOR XML PATH ('MastersBE') 
	
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 
ALTER PROCEDURE  [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   
						   IF	 isnull(@ActiveStatusId,0) != 1	  or	 isnull(@ActiveStatusId,0) !=2    or isnull(@CustomerTypeID,0) = 3	 
						   BEGIN
						   GOTO Loop_End;
						   END   -- In Active Customer

							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1


							IF isnull(@BookDisableType,0)   = 1
		 						GOTO Loop_End;

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE -- -- Direct customer
							BEGIN

								set @IsEstimatedRead =1
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 or  isnull(@ActiveStatusId,0) =2
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH
					 
					 
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================          
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <25-MAR-2014>          
-- Description: Customer Payment from file upload   
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <24-MAR-2015>          
-- Description: Romove join to check customer bill in process
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]          
 (  
@XmlDoc XML  
,@MultiXmlDoc XML  
 )          
AS          
BEGIN          
          
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)     
        ,ReceivedDate DATETIME,PaymentModeId VARCHAR(50))    
 DECLARE @Bu_Id VARCHAR(50)  
   
 SELECT   
 @Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)      
         
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)      
 SELECT             
  c.value('(SNO)[1]','INT')                         
 ,c.value('(AccountNo)[1]','VARCHAR(50)')  
 ,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                         
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                         
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)    
    ,c.value('(PaymentMode)[1]','VARCHAR(50)')    
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)        
  SELECT    
  (    
   SELECT     
   ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
   ,CD.GlobalAccountNumber  AS AccountNo  
  , CD.OldAccountNo AS OldAccountNo  
     ,TempDetails.AmountPaid AS PaidAmount    
     ,TempDetails.ReceiptNO    
     ,TempDetails.ReceivedDate   
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
  ,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid, TempDetails.ReceiptNO) AS IsDuplicate  
  ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists    
  ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId)   
    ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentMode=PM.PaymentMode) END) AS PaymentMode    
  ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId    
  --,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers     
  --  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess    
 -- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills     
   FROM @TempCustomer AS TempDetails     
    INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo  
    LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentMode    
 FOR XML PATH('PaymentsList'),TYPE      
 )    
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')        
END   
------------------------------------------------------------------------------------------------------------------
--SELECT * FROM Tbl_MPaymentMode
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillingDisabledBookNos]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Bhimaraju.V    
-- Create date: 25-09-2014    
-- Description: The purpose of this procedure is to insert Details of Disabled BookNos for Billing     
-- Modified By: Faiz-ID103
-- Modified Date: 10-Apr-2015
-- Description: Added the disable date
-- =============================================    
ALTER PROCEDURE [dbo].[USP_InsertBillingDisabledBookNos]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @BookNo VARCHAR(20)  
  ,@Details VARCHAR(MAX)  
  ,@YearId INT  
  ,@MonthId INT    
  ,@CreatedBy VARCHAR(50)  
  ,@ApproveStatusId INT  
  ,@DisableTypeId INT    
  ,@DisableDate VARCHAR(10)  
      
 SELECT    
   @BookNo=C.value('(BookNo)[1]','VARCHAR(20)')    
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
  ,@YearId=C.value('(YearId)[1]','INT')    
  ,@MonthId=C.value('(MonthId)[1]','INT')    
  ,@ApproveStatusId=C.value('(ApproveStatusId)[1]','INT')    
  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')    
  ,@DisableTypeId=C.value('(DisableTypeId)[1]','INT')    
  ,@DisableDate=C.value('(DisableDate)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('BillingDisabledBooksBe') as T(C)    
     
IF EXISTS(SELECT 0 FROM Tbl_BillingDisabledBooks WHERE YearId=@YearId AND MonthId=@MonthId AND BookNo=@BookNo AND IsActive=1)    
 BEGIN    
  SELECT 1 AS BookNoExistsForThisMonth FOR XML PATH('BillingDisabledBooksBe')    
 END    
ELSE    
 BEGIN    
  INSERT INTO Tbl_BillingDisabledBooks(    BookNo    
            ,YearId    
            ,MonthId    
            ,Remarks    
            ,ApproveStatusId    
            ,CreatedBy    
            ,CreatedDate    
            ,DisableTypeId    
            ,FromDate    
            ,DisableDate  
           )    
            
          VALUES(    
             @BookNo    
            ,@YearId    
            ,@MonthId    
            ,CASE @Details WHEN '' THEN NULL ELSE @Details END    
            ,@ApproveStatusId    
            ,@CreatedBy    
            ,dbo.fn_GetCurrentDateTime()    
            ,@DisableTypeId    
            ,dbo.fn_GetCurrentDateTime()  
            ,@DisableDate  
            )    
       
  SELECT 1 AS IsSuccess FOR XML PATH('BillingDisabledBooksBe')    
 END    
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillingDisabledBookNo]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Bhimaraju Vanka    
-- Create date: 24-Sep-2014    
-- Modified By: T.Karthik    
-- Modified Date : 17-10-2014    
-- Description: Purpose is To get List Of Billing Disabled BookNo   
-- Modified By: Faiz-ID103
-- Modified Date: 10-Apr-2015
-- Description: Disable field added  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBillingDisabledBookNo]  
(    
 @XmlDoc Xml    
)    
AS    
BEGIN    
    
 DECLARE  @PageNo INT    
   ,@PageSize INT    
   ,@YearId INT    
   ,@MonthId INT    
   ,@BU_ID VARCHAR(50)  
        
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')    
    ,@PageSize = C.value('(PageSize)[1]','INT')    
    ,@YearId=C.value('(YearId)[1]','INT')    
    ,@MonthId=C.value('(MonthId)[1]','INT')    
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')           
  FROM @XmlDoc.nodes('BillingDisabledBooksBe') AS T(C)     
      
  CREATE TABLE #BillingDisabledBookNos(Sno INT PRIMARY KEY IDENTITY(1,1),DisabledBookId INT)    
    
  INSERT INTO #BillingDisabledBookNos(DisabledBookId)    
   SELECT    
    DisabledBookId    
   FROM Tbl_BillingDisabledBooks    
   WHERE IsActive = 1    
   AND (YearId=@YearId OR @YearId='')    
   AND (MonthId=@MonthId OR @MonthId='')    
   ORDER BY CreatedDate DESC    
       
  DECLARE @Count INT=0    
  SET @Count=(SELECT COUNT(0) FROM #BillingDisabledBookNos)    
      
  SELECT    
    Sno AS RowNumber    
    ,BDB.BookNo AS BookNo  -- Faiz_ID103  
    ,BN.ID AS ID  
    ,BDB.DisabledBookId    
    ,ISNULL(Remarks,'--') AS Details    
    ,MonthId    
    ,YearId    
    ,((select dbo.fn_GetMonthName([MonthId]))+'-'+CONVERT(varchar(10), [YearId])) AS MonthYear    
    ,@Count AS TotalRecords  
    ,(CASE IsPartialBill WHEN 1 THEN 'Partial Book' ELSE DisableType END) AS DisableType  
    ,ISNULL((CONVERT(VARCHAR(20),DisableDate,106)),'--') AS DisableDate
  FROM Tbl_BillingDisabledBooks AS BDB    
  JOIN #BillingDisabledBookNos LBDB ON BDB.DisabledBookId=LBDB.DisabledBookId    
  INNER JOIN UDV_BookNumberDetails BN on BDB.BookNo=BN.BookNo   
 AND BN.BU_ID = @BU_ID  
  INNER JOIN TBl_MDisableType D ON D.DisableTypeId = BDB.DisableTypeId  
  --INNER JOIN Tbl_Cycles C ON C.CycleId = BN.CycleId  
  --INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = C.ServiceCenterId  
  --INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID   
  AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
       
 END    
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 10-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBulkCustomerPayments]          
(          
	 @XmlDoc XML           
	,@MultiXmlDoc XML          
)          
AS          
BEGIN    

DECLARE @CreatedBy VARCHAR(50)    
	,@DocumentPath VARCHAR(MAX)          
	,@DocumentName VARCHAR(MAX)         
	,@BatchNo VARCHAR(50) 
	,@StatusText VARCHAR(200) = ''
	,@IsSuccess BIT = 0
	
	SELECT            
		 @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')          
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')          
		,@DocumentName = C.value('(DocumentName)[1]','VARCHAR(MAX)')        
		,@BatchNo = C.value('(BatchNo)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)    	

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), AccountNo VARCHAR(50), ReceiptNo VARCHAR(20),           
							PaymentMode VARCHAR(20), PaymentModeId INT, 
							AmountPaid DECIMAL(18,2), RecievedDate DATETIME)
	
	INSERT INTO @TempCustomer(AccountNo, ReceiptNo, PaymentMode, PaymentModeId, AmountPaid, RecievedDate)           
	SELECT           
		 T.c.value('(AccountNo)[1]','VARCHAR(50)')           
		,T.c.value('(ReceiptNO)[1]','VARCHAR(20)')           
		,T.c.value('(PaymentMode)[1]','VARCHAR(20)')          
		,T.c.value('(PaymentModeId)[1]','VARCHAR(20)')          
		,T.c.value('(PaidAmount)[1]','DECIMAL(18,2)')          
		,T.c.value('(PaymentDate)[1]','DATETIME')          
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Table1') AS T(c)    
							
	DECLARE @SNo INT, @TotalCount INT
			,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
			,@CustomerPaymentId INT
			
	DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
	SET @SNo = 1
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer    
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SELECT @PresentAccountNo = AccountNo, @PaidAmount = AmountPaid FROM @TempCustomer WHERE SNo = @SNo
					
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 AccountNo
						,ReceiptNo
						,PaymentModeId
						,@DocumentPath
						,@DocumentName
						,RecievedDate
						,2 -- For Bulk Upload
						,AmountPaid
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempCustomer WHERE AccountNo = @PresentAccountNo 
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @StatusText = 'Customer Pending Bills'
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
					
					SET @StatusText = 'Customer Bills Paymnets'
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					SET @StatusText = 'Customer Bills'
					UPDATE CB
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @StatusText = 'Success'
					SET @IsSuccess = 1
					SET @SNo = @SNo + 1
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess =0
			END CATCH		
		END
		
	SELECT   
		 @IsSuccess As IsSuccess
		,@StatusText AS StatusText
	FOR XML PATH('PaymentsBe'),TYPE
		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose12]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose12]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   
						   IF	 isnull(@ActiveStatusId,0) != 1 
						   BEGIN
						   GOTO Loop_End;
						   END   -- In Active Customer
						   
						   IF isnull(@CustomerTypeID,0) = 3	 
						   BEGIN
						   GOTO Loop_End;; --3		PREPAID  CUSTOMER TYPE
						   END
						 
						 
							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
					
						  IF 	isnull(@BookDisableType,0)   = 1
		 						GOTO Loop_End;
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE -- -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
					
						 
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
							 
							
									  
						
								
						------------------------------------------------------------------------------------------------------------------------------------
						
						
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH
					 
					 
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillPayments]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 10 Apr 2015
-- Description:	To insert the customer bill payments
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillPayments]
(
	@XmlDoc XML
)
AS
BEGIN
	
	DECLARE @AccountNo VARCHAR(50)  
		,@ReceiptNo VARCHAR(20)  
		,@PaymentMode INT  
		,@DocumentPath VARCHAR(MAX)  
		,@DocumentName VARCHAR(MAX) 
		,@Cashier VARCHAR(50)  
		,@CashOffice INT  
		,@CreatedBy VARCHAR(50)  
		,@BatchNo INT  
		,@PaidAmount DECIMAL(20,4)  
		,@PaymentRecievedDate VARCHAR(20)  
		,@CustomerPaymentId INT  
		,@EffectedRows INT  
		,@PaymentType INT
		,@IsCustomerExists BIT = 0
		,@IsSuccess BIT = 0
		,@StatusText VARCHAR(200) = ''
		
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@ReceiptNo = C.value('(ReceiptNo)[1]','VARCHAR(20)')  
		,@PaymentMode = C.value('(PaymentModeId)[1]','INT')  
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
		,@DocumentName = C.value('(Document)[1]','VARCHAR(MAX)') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@BatchNo = C.value('(BatchNo)[1]','INT')  
		,@PaidAmount = C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
		,@PaymentRecievedDate = C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
		,@PaymentType = C.value('(PaymentType)[1]','INT')  
	FROM @XmlDoc.nodes('MastersBE') AS T(C) 
	
	IF(@AccountNo IS NOT NULL)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @AccountNo
						,@ReceiptNo
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PaymentRecievedDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
					
					SET @StatusText = 'Customer Pending Bills'
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@AccountNo,@PaidAmount) 
					
					SET @StatusText = 'Customer Bills Paymnets'
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					SET @StatusText = 'Customer Bills'
					UPDATE CB
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @IsCustomerExists = 1
					SET @IsSuccess = 1
					SET @StatusText = 'Success'
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsCustomerExists = 0
				SET @IsSuccess =0
			END CATCH			
		END
	
	SELECT  
		 @IsCustomerExists AS IsCustomerExists  
		,@IsSuccess AS IsSuccess  
		,@StatusText AS StatusText
	FOR XML PATH ('MastersBE') 
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
			,@BU_ID VARCHAR(50)
			,@Active INT=1
 SELECT  
  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
  IF NOT EXISTS(SELECT 0 FROM [UDV_CustomerDescription] 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId IN (1,2)
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
	BEGIN
		SET @Active=0
	END
	
   
 IF(@Active=1)  
	 BEGIN  
		  SELECT  
		   --dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		   dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
		   ,ClassName  
		   --,(SELECT DBO.fn_GetCustomerTotalDueAmount(@AccountNo)) AS DueAmount  --Commented By Raja-ID065
		   ,OutStandingAmount AS DueAmount  --Commented By Raja-ID065
		   --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS DueAmount --Raja-ID065
		   --,(SELECT CONVERT(DECIMAL(20,2),SUM(ISNULL(TotalBillAmountWithArrears,0))) FROM Tbl_CustomerBills WHERE ActiveStatusId=1 AND BillStatusId=2 AND AccountNo=@AccountNo) AS DueAmount  
		   ,1 AS IsCustomerExists  
		   ,GlobalAccountNumber AS AccountNo
		  FROM [UDV_CustomerDescription] 
		  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)		    
		  AND(BU_ID = @BU_ID OR @BU_ID = '')
		  FOR XML PATH('MastersBE')  
	 END  
	 ELSE  
		 BEGIN  
			SELECT 0 AS IsCustomerExists 
			FOR XML PATH('MastersBE')  
		 END  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerStatusChangeApproval]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Modified By: Karteek  
-- Modified Date: 04-04-2015  
-- Description: Update custoemr status change Details after approval    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_CustomerStatusChangeApproval]    
(    
 @XmlDoc XML=null    
)    
AS    
BEGIN     
SET NOCOUNT ON;  
   
 DECLARE    
   @GlobalAccountNo VARCHAR(50)     
  ,@Remarks VARCHAR(200)    
  ,@ApprovalStatusId INT    
  ,@OldStatusId INT  
  ,@NewStatusId INT  
  ,@FunctionId INT  
  ,@ModifiedBy VARCHAR(50)  
  ,@PresentRoleId INT = NULL  
  ,@NextRoleId INT = NULL  
  ,@CurrentLevel INT  
  ,@ActiveStatusChangeLogId INT  
  ,@IsFinalApproval BIT = 0  
  ,@Details VARCHAR(200) 
  ,@ChangeDate VARCHAR(10)  
  
 SELECT    
   @ActiveStatusChangeLogId = C.value('(ActiveStatusChangeLogId)[1]','INT')    
  ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')     
  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
  ,@Details = C.value('(Details)[1]','VARCHAR(200)')   
  --,@ChangeDate = C.value('(ChangeDate)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 SELECT   
   @OldStatusId = OldStatus  
  ,@NewStatusId = NewStatus  
  ,@GlobalAccountNo = AccountNo  
  ,@Remarks = Remarks  
  ,@ChangeDate = ChangeDate
 FROM Tbl_CustomerActiveStatusChangeLogs  
 WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
   
 IF(@ApprovalStatusId = 2)  -- Request Approved  
  BEGIN    
   DECLARE @CurrentRoleId INT  
   SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId  
  
   IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists  
    BEGIN  
     SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND  
         RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs   
           WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId))  
  
     SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval  
     FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)  
  
     IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)  
      BEGIN -- If Approval levels are there and If it is final approval then update tariff  
        
       UPDATE Tbl_CustomerActiveStatusChangeLogs   
       SET   -- Updating Request with Level Roles & Approval status   
         ModifiedBy = @ModifiedBy  
        ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
        ,PresentApprovalRole = @PresentRoleId  
        ,NextApprovalRole = @NextRoleId   
        ,ApproveStatusId = @ApprovalStatusId  
       WHERE AccountNo = @GlobalAccountNo   
       AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
  
       UPDATE CUSTOMERS.Tbl_CustomersDetail  
       SET         
         ActiveStatusId = @NewStatusId  
        ,ModifedBy = @ModifiedBy    
        ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
       WHERE GlobalAccountNumber = @GlobalAccountNo  
         
       INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
         ActiveStatusChangeLogId  
        ,AccountNo  
        ,OldStatus  
        ,NewStatus  
        ,ApproveStatusId  
        ,Remarks  
        ,CreatedDate  
        ,CreatedBy  
        ,PresentApprovalRole  
        ,NextApprovalRole
        ,ChangeDate)    
       VALUES(    
         @ActiveStatusChangeLogId  
        ,@GlobalAccountNo    
        ,@OldStatusId  
        ,@NewStatusId   
        ,@ApprovalStatusId  
        ,@Remarks  
        ,dbo.fn_GetCurrentDateTime()    
        ,@ModifiedBy  
        ,@PresentRoleId  
        ,@NextRoleId
        ,@ChangeDate)   
          
      END  
     ELSE -- Not a final approval  
      BEGIN  
       UPDATE Tbl_CustomerActiveStatusChangeLogs   
       SET   -- Updating Request with Level Roles  
         ModifiedBy = @ModifiedBy  
        ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
        ,PresentApprovalRole = @PresentRoleId  
        ,NextApprovalRole = @NextRoleId   
        ,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)   
       WHERE AccountNo = @GlobalAccountNo   
       AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
         
      END  
    END  
   ELSE   
    BEGIN -- No Approval Levels are there  
     UPDATE Tbl_CustomerActiveStatusChangeLogs   
     SET   -- Updating Request with Level Roles & Approval status   
       ModifiedBy = @ModifiedBy  
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
      ,PresentApprovalRole = @PresentRoleId  
      ,NextApprovalRole = @NextRoleId   
      ,ApproveStatusId = @ApprovalStatusId  
     WHERE AccountNo = @GlobalAccountNo   
     AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
  
     UPDATE CUSTOMERS.Tbl_CustomersDetail  
     SET         
       ActiveStatusId = @NewStatusId  
      ,ModifedBy = @ModifiedBy    
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
     WHERE GlobalAccountNumber = @GlobalAccountNo  
       
     INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
       ActiveStatusChangeLogId  
      ,AccountNo  
      ,OldStatus  
      ,NewStatus  
      ,ApproveStatusId  
      ,Remarks  
      ,CreatedDate  
      ,CreatedBy  
      ,PresentApprovalRole  
      ,NextApprovalRole
      ,ChangeDate)    
     VALUES(    
       @ActiveStatusChangeLogId  
      ,@GlobalAccountNo    
      ,@OldStatusId  
      ,@NewStatusId   
      ,@ApprovalStatusId  
      ,@Remarks  
      ,dbo.fn_GetCurrentDateTime()    
      ,@ModifiedBy  
      ,@PresentRoleId  
      ,@NextRoleId
      ,@ChangeDate)   
       
   END  
  
   SELECT 1 As IsSuccess    
   FOR XML PATH('ChangeBookNoBe'),TYPE  
  END    
 ELSE    
  BEGIN  -- Request Not Approved  
   IF(@ApprovalStatusId = 3) -- Request is Rejected  
    BEGIN  
     IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)  
      BEGIN -- Approval levels are there and status is rejected  
       SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND  
             RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs   
                WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId))  
  
       SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   
       FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)  
      END  
    END  
   ELSE IF(@ApprovalStatusId = 4) -- Request is Hold  
    BEGIN  
     IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)  
      SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs   
      WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
    END  
  
   -- If there are no Approval levels then Present and Next Approval roles will be updated as NULL     
   UPDATE Tbl_CustomerActiveStatusChangeLogs   
   SET     
     ModifiedBy = @ModifiedBy  
    ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
    ,PresentApprovalRole = @PresentRoleId  
    ,NextApprovalRole = @NextRoleId   
    ,ApproveStatusId = @ApprovalStatusId  
    ,Remarks = @Details  
   WHERE AccountNo = @GlobalAccountNo    
   AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
  
   SELECT 1 As IsSuccess    
   FOR XML PATH('ChangeBookNoBe'),TYPE    
  END    
  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerActiveStatus]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 07-10-2014      
-- Description: The purpose of this procedure is to Update Customer ActiveStausLog   
   --ModifiedBy : Padmini  
   --Modified Date :05-02-2015  
-- Modified By: Karteek  
-- Modified Date: 03-04-2015   
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerActiveStatus]  
(  
 @XmlDoc xml      
)  
AS  
BEGIN  
 DECLARE     
   @GlobalAccountNumber VARCHAR(50)   
  ,@ModifiedBy VARCHAR(50)  
  ,@ActiveStatusId INT  
  ,@Details VARCHAR(MAX)  
  ,@ApprovalStatusId INT  
  ,@IsFinalApproval BIT = 0  
  ,@FunctionId INT  
  ,@ChangeDate VARCHAR(10)
          
 SELECT       
   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
  ,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')  
  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')    
  ,@FunctionId = C.value('(FunctionId)[1]','INT')    
  ,@ChangeDate = C.value('(ChangeDate)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 IF((SELECT COUNT(0)FROM Tbl_CustomerActiveStatusChangeLogs   
   WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN(1,4)) > 0)  
  BEGIN  
   SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
  END     
 ELSE  
  BEGIN  
   DECLARE @RoleId INT, @CurrentLevel INT  
     ,@PresentRoleId INT, @NextRoleId INT  
     ,@StatusChangeRequestId INT  
     
   SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId  
   SET @CurrentLevel = 0 -- For Stating Level     
     
   IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
    BEGIN  
     SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId  
    END  
       
   SELECT @PresentRoleId = PresentRoleId   
    ,@NextRoleId = NextRoleId   
   FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)  
       
   INSERT INTO Tbl_CustomerActiveStatusChangeLogs(   
     AccountNo  
    ,OldStatus  
    ,NewStatus  
    ,CreatedBy  
    ,CreatedDate  
    ,ChangeDate
    ,ApproveStatusId  
    ,Remarks  
    ,PresentApprovalRole  
    ,NextApprovalRole)  
   SELECT GlobalAccountNumber  
    ,ActiveStatusId  
    ,@ActiveStatusId  
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
    ,@ChangeDate 
    ,@ApprovalStatusId  
    ,@Details  
    ,@PresentRoleId  
    ,@NextRoleId   
   FROM [CUSTOMERS].[Tbl_CustomerSDetail]     
   WHERE GlobalAccountNumber = @GlobalAccountNumber    
  
   SET @StatusChangeRequestId = SCOPE_IDENTITY()  
     
   IF(@IsFinalApproval = 1)  
    BEGIN  
       
     UPDATE CUSTOMERS.Tbl_CustomersDetail  
     SET         
       ActiveStatusId = @ActiveStatusId  
      ,ModifedBy = @ModifiedBy    
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
     WHERE GlobalAccountNumber = @GlobalAccountNumber   
       
     INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole)    
     SELECT    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,@ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole  
     FROM Tbl_CustomerActiveStatusChangeLogs WHERE ActiveStatusChangeLogId = @StatusChangeRequestId  
       
    END  
      
   SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
  
  END   
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillAdjustmentDetails]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  : NEERAJ KANOJIYA                
 -- Create date  : 7 OCT 2014                  
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT #        
 -- Modified BY : Suresh Kumar Dasi 
 -- Modified By : Padmini
 -- Modified Date : 25-12-2014    
 -- =============================================                  
 ALTER PROCEDURE [dbo].[USP_GetBillAdjustmentDetails]                  
 (                  
 @XmlDoc xml                  
 )                  
 AS                  
 BEGIN                  
  DECLARE	@SearchValue VARCHAR(50)
			,@AccountNo VARCHAR(20)                 
			,@BillNo VARCHAR(20)            
		--	,@OldAccountNo VARCHAR(20)            
		--	,@MeterNo VARCHAR(20)            
		--	,@CustomerBillId INT      
  SELECT       
	@SearchValue = C.value('(SearchValue)[1]','VARCHAR(50)')                                     
   --@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)'),          
   --@BillNo = C.value('(BillNo)[1]','VARCHAR(50)'),                          
   --@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(20)'),                          
   --@MeterNo = C.value('(MeterNo)[1]','VARCHAR(20)')                          
  FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
   
    
  --IF(@AccountNo = '' OR @AccountNo IS NULL)              
  -- BEGIN          
  -- SELECT TOP 1 @AccountNo = AccountNo FROM Tbl_CustomerBills WHERE BillNo=@BillNo OR MeterNo=@MeterNo    
  -- IF(@AccountNo = '' OR @AccountNo IS NULL)         
  --  SELECT @AccountNo=AccountNo FROM Tbl_CustomerDetails WHERE OldAccountNo=@OldAccountNo  
  -- END   
  --IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo=@SearchValue) 
	 -- BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.AccountNo)
		--				FROM Tbl_CustomerDetails AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.AccountNo=CB.AccountNo
		--				WHERE CD.OldAccountNo=@SearchValue OR CD.MeterNo=@SearchValue OR CB.BillNo=@SearchValue)					
	 -- END
    IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@SearchValue )
	  BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.GlobalAccountNumber)
		--				FROM [CUSTOMERS].[Tbl_CustomerSDetail] AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
		--				JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber 			
		--				WHERE CD.OldAccountNo=@SearchValue OR CPD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue)					
		
		SET @AccountNo= (SELECT GlobalAccountNumber 
							FROM UDV_CustomerDescription CD
							LEFT JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
							WHERE (CD.OldAccountNo=@SearchValue OR CD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue))
		
	  END
  ELSE
	SET @AccountNo=@SearchValue
            
  SELECT                
  (                
  --Get customers details by account #                
  SELECT                
   A.GlobalAccountNumber AS CustomerID               
  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name             
  ,A.GlobalAccountNumber AS AccountNo              
  ,A.DocumentNo       
  ,A.MeterNumber AS MeterNo  
  ,A.OldAccountNo               
  ,A.RouteSequenceNo AS RouteSequenceNumber                 
  ,B.BusinessUnitName                  
  ,D.ServiceUnitName                  
  ,C.ServiceCenterName                  
  ,ReadCodeId                
  ,A.TariffId AS ClassID                
  ,E.ClassName               
  ,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings                  
  ,A.OutStandingAmount AS TotalDueAmount --Commented by Raja-ID065    
  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount --Commented by Raja-ID065    
  --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS TotalDueAmount --Raja-ID065    
  ,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate                  
  ,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount              
  ,dbo.fn_GetCustomerServiceAddress(GlobalAccountNumber) AS ServiceAddress              
  ,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate       
  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials                   
    FROM [UDV_CustomerDescription] AS A                 
    JOIN Tbl_BussinessUnits  AS B ON A.BU_ID = B.BU_ID                
    JOIN Tbl_ServiceCenter   AS C ON A.ServiceCenterId=C.ServiceCenterId                
    JOIN Tbl_ServiceUnits    AS D ON A.SU_ID=D.SU_ID                
    JOIN Tbl_MTariffClasses AS E ON A.TariffId = E.ClassID                   
    WHERE GlobalAccountNumber = @AccountNo                     
    FOR XML PATH('Customerdetails'),TYPE                  
 )              
 ,                
 (                
  --Get customers bill details by account #                
  SELECT TOP(3)                
   A.CustomerBillId AS CustomerBillID,                
  (SELECT BillAdjustmentId FROM Tbl_BillAdjustments AS B WHERE B.CustomerBillId = A.CustomerBillID) AS BAID,              
   A.CustomerId,                
   CONVERT(DECIMAL(18,2),A.Vat) AS Vat,              
   A.BillNo,              
   A.AccountNo,                        
   A.BillNo,                 
   ISNULL(A.PreviousReading,'0') AS PreviousReading,                  
   ISNULL(A.PresentReading,'0') AS PresentReading,                  
   A.Usage AS Consumption,                
   A.BillYear,                
   A.BillMonth,       A.ReadCodeId  
   ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(A.BillYear,A.BillMonth, A.AccountNo)) AS IsActiveMonth, --Raja-ID065 For getting month locked or not    
   (SELECT dbo.fn_IsCustomerBill_Latest(A.CustomerBillId,A.AccountNo)) AS IsSuccess, --For Checking Is this Bill Latest or NOT ?  
   A.NetEnergyCharges AS NetEnergyCharges,        
   --(SELECT dbo.fn_IsCustomerBilledMonthLocked(CONVERT(int, A.BillYear),CONVERT(int, A.BillMonth), A.AccountNo)) AS IsActiveMonth ,      
   --dbo.fn_GetBillPayments(A.CustomerBillId) AS TotaPayments,          
   dbo.fn_GetTotalPaidAmountOfBill(A.BillNo) AS TotaPayments,          
   --dbo.fn_GetDueBill(A.BillNo) AS DueBill,  --Commented Bu Raja-ID065    
   --CONVERT(DECIMAL(20,2),A.TotalBillAmountWithTax - dbo.fn_GetBillPayments(A.CustomerBillId)) AS DueBill, --Commented By Raja-ID065    
   (SELECT dbo.fn_IsBillAdjusted(A.CustomerBillId))AS IsAdjested,    
   (SELECT ApprovalStatusId FROM Tbl_BillAdjustments WHERE CustomerBillId = A.CustomerBillId) AS ApprovalStatusId,              
   CONVERT(DECIMAL(20,2),TotalBillAmount) AS TotalBillAmount,    
   (CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal, -- Raja-ID065 For GrandTotal    
   ((CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount))-ISNULL(dbo.fn_GetTotalPaidAmountOfBill(A.BillNo),0)) AS DueBill,-- Raja-ID065 For Due Bill    
   --(SELECT ReadCodeId FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS ReadCode,   
   --(SELECT OldAccountNo FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS OldAccountNo,
   (SELECT ReadCodeId FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber =@AccountNo) AS ReadCode,   
   (SELECT OldAccountNo FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber =@AccountNo) AS OldAccountNo,                               
   (SELECT top(1) CustomerReadingId FROM Tbl_CustomerReadings              
    WHERE GlobalAccountNumber=@AccountNo AND               
   ISBILLED=1               
    ORDER BY CustomerReadingId DESC) AS CustomerReadingId              
 --     A.CustomerReadingId                
   FROM Tbl_CustomerBills  AS A                       
  WHERE AccountNo = @AccountNo   
  AND BillNo = (CASE WHEN ISNULL(@BillNo,'') = '' THEN BillNo ELSE @BillNo END)   
  AND PaymentStatusID = 2                
  ORDER BY CustomerBillID DESC                      
  FOR XML PATH('CustomerBillDetails'),TYPE                  
 )                
 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                  
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 04/10/2015 22:07:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<NEERAJ KANOJIYA>
-- Create date: <26-MAR-2015>
-- Description:	<This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>
-- =============================================
--Exec USP_BillGenaraton
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]
( @XmlDoc Xml)
AS
BEGIN
	 
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@ServiceUnitId	VARCHAR(50)
			,@PoleId Varchar(50)
			,@ServiceCenterId VARCHAR(50)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        
	
	SELECT	@GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@Month = C.value('(BillMonth)[1]','VARCHAR(50)') 
			,@Year = C.value('(BillYear)[1]','VARCHAR(50)') 
			FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
			 
	IF(@GlobalAccountNumber !='')
	BEGIN
			
			
			    
						SELECT 
						@ActiveStatusId = ActiveStatusId,
						@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =ClassName
						,@Service_HouseNo =Service_HouseNo
						,@Service_Street =Service_StreetName
						,@Service_City =Service_City
						,@ServiceZipCode  =Service_ZipCode
						,@Postal_HouseNo =Postal_HouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@BU_ID=BU_ID
						,@ServiceUnitId=ServiceCenterId
						,@PoleId=PoleID
						,@TariffId=ClassID
						,@ServiceCenterId=ServiceCenterId
						FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber 
						 
						 ----------------------------------------COPY START -------------------------------------------------------- 
						 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   
						   IF	 isnull(@ActiveStatusId,0) != 1 
						   BEGIN
						   Return;
						   END   -- In Active Customer
						   
						   IF isnull(@CustomerTypeID,0) = 3	 
						   BEGIN
						     return;  --3		PREPAID  CUSTOMER TYPE
						   END
						 
						 
							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1
					
						  IF 	isnull(@BookDisableType,0)   = 1
		 						Return
					 		
					 		
							IF @ReadCodeId=2 -- 
							BEGIN
								 
								 
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
								
								
								
								
								 
								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
												 
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE -- -- Direct customer
							BEGIN
								set @IsEstimatedRead =1
								 
								 
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END
					
						 
								
			 			
						
						
						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						
						------------------------------------------------------------------------------------ 

						IF  isnull(@IspartialClose,0) =   1
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
						
			 
			----------------------------------------------------------COPY END-------------------------------------------------------------------
			--- Need to verify all fields before insert
			INSERT INTO Tbl_CustomerBills
			(
				[AccountNo]   --@GlobalAccountNumber     
				,[TotalBillAmount] --@TotalBillAmountWithTax      
				,[ServiceAddress] --@EnergyCharges 
				,[MeterNo]   -- @MeterNumber    
				,[Dials]     --   
				,[NetArrears] --   @NetArrears    
				,[NetEnergyCharges] --  @EnergyCharges     
				,[NetFixedCharges]   --@FixedCharges     
				,[VAT]  --     @TaxValue 
				,[VATPercentage]  --  @TaxPercentage    
				,[Messages]  --      
				,[BU_ID]  --      
				,[SU_ID]  --    
				,[ServiceCenterId]  
				,[PoleId] --       
				,[BillGeneratedBy] --       
				,[BillGeneratedDate]        
				,PaymentLastDate        --
				,[TariffId]  -- @TariffId     
				,[BillYear]    --@Year    
				,[BillMonth]   --@Month     
				,[CycleId]   -- @CycleId
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
				,[ActiveStatusId]--        
				,[CreatedDate]--GETDATE()        
				,[CreatedBy]        
				,[ModifedBy]        
				,[ModifiedDate]        
				,[BillNo]  --      
				,PaymentStatusID        
				,[PreviousReading]  --@PreviousReading      
				,[PresentReading]   --  @CurrentReading   
				,[Usage]     --@Usage   
				,[AverageReading] -- @AverageUsageForNewBill      
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
				,[EstimatedUsage] --@Usage       
				,[ReadCodeId]  --  @ReadType    
				,[ReadType]  --  @ReadType
				,AdjustmentAmmount -- @AdjustmentAmount  
				,BalanceUsage    --@RemaningBalanceUsage
				,BillingTypeId -- 
				,ActualUsage
				,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
				,PreviousBalance--@PreviousBalance
			)        
			 Values( @GlobalAccountNumber
				,@TotalBillAmountWithTax   
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber) 
				,@MeterNumber    
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber) 
				,@NetArrears       
				,@EnergyCharges 
				,@FixedCharges
				,@TaxValue 
				,@TaxPercentage        
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))        
				,@BU_ID
				,@ServiceUnitId
				,@ServiceCenterId
				,@PoleId        
				,@BillGeneratedBY        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber
				 ORDER BY RecievedDate DESC) --@LastDateOfBill        
				,@TariffId
				,@Year
				,@Month
				,@CycleId
				,@TotalBillAmountWithArrears 
				,1 --ActiveStatusId        
				,(SELECT dbo.fn_GetCurrentDateTime())        
				,@BillGeneratedBY        
				,NULL --ModifedBy
				,NULL --ModifedDate
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo      
				,2 -- PaymentStatusID   
				,@PreviousReading        
				,@CurrentReading        
				,@Usage        
				,@AverageUsageForNewBill
				,@TotalBillAmountWithTax             
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
				,@ReadType
				,@ReadType       
				,@AdjustmentAmount    
				,@RemaningBalanceUsage   
				,2 -- BillingTypeId 
				,@ActualUsage
				,@PrevBillTotalPaidAmount
				,@PreviousBalance
			       
		)
			set @CusotmerNewBillID = SCOPE_IDENTITY() 
			----------------------- Update Customer Outstanding ------------------------------

			-- Insert Paid Meter Payments
			INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
			SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
			 
			update CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount=@TotalBillAmountWithArrears
			where GlobalAccountNumber=@GlobalAccountNumber
			----------------------------------------------------------------------------------
			----------------------Update Readings as is billed =1 ----------------------------
			
			update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
 
			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
			
			insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
			select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
			
			delete from @tblFixedCharges
			 
			------------------------------------------------------------------------------------
			
			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
			
			---------------------------------------------------------------------------------------
			Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
			
			--------------Save Bill Deails of customer name,BU-----------------------------------------------------
			INSERT INTO Tbl_BillDetails
						(CustomerBillID,
						 BusinessUnitName,
						 CustomerFullName,
						 Multiplier,
						 Postal_City,
						 Postal_HouseNo,
						 Postal_Street,
						 Postal_ZipCode,
						 ReadDate,
						 ServiceZipCode,
						 Service_City,
						 Service_HouseNo,
						 Service_Street,
						 TariffName,
						 Postal_LandMark,
						 Service_Landmark,
						 OldAccountNumber)
					VALUES
						(@CusotmerNewBillID,
						@BusinessUnitName,
						@CustomerFullName,
						@Multiplier,
						@Postal_City,
						@Postal_HouseNo,
						@Postal_Street,
						@Postal_ZipCode,
						@ReadDate,
						@ServiceZipCode,
						@Service_City,
						@Service_HouseNo,
						@Service_Street,
						@TariffName,
						@Postal_LandMark,
						@Service_LandMark,
						@OldAccountNumber)
			
			---------------------------------------------------Set Variables to NULL-
			
			SET @TotalAmount = NULL
			SET @EnergyCharges = NULL
			SET @FixedCharges = NULL
			SET @TaxValue = NULL
			 
			SET @PreviousReading  = NULL      
			SET @CurrentReading   = NULL     
			SET @Usage   = NULL
			SET @ReadType =NULL
			SET @BillType   = NULL     
			SET @AdjustmentAmount    = NULL
			SET @RemainingBalanceUnits   = NULL 
			SET @TotalBillAmountWithArrears=NULL
			SET @BookNo=NULL
			SET @AverageUsageForNewBill=NULL	
			SET @IsHaveLatest=0
			SET @ActualUsage=NULL
			SET @RegenCustomerBillId =NULL
			SET @PaidAmount  =NULL
			SET @PrevBillTotalPaidAmount =NULL
			SET @PreviousBalance =NULL
			SET @OpeningBalance =NULL
			SET @CustomerFullName  =NULL
			SET @BusinessUnitName  =NULL
			SET @TariffName  =NULL
			SET @ReadDate  =NULL
			SET @Multiplier  =NULL
			SET @Service_HouseNo  =NULL
			SET @Service_Street  =NULL
			SET @Service_City  =NULL
			SET @ServiceZipCode =NULL
			SET @Postal_HouseNo  =NULL
			SET @Postal_Street =NULL
			SET @Postal_City  =NULL
			SET @Postal_ZipCode  =NULL
			SET @Postal_LandMark =NULL
			SET	@Service_LandMark =NULL
			SET @OldAccountNumber=NULL	
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)
	END
END

GO


