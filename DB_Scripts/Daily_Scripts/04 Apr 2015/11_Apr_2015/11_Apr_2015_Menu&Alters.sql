-- Delete Meter Reading From 
GO
DELETE FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFromId IN(3,5)
GO
-- Isert New Menu
GO
INSERT INTO Tbl_Menus (MenuId,Name,[Path],ReferenceMenuId,Page_Order,IsActive)
VALUES(176,'Change Customer Contact Info','../ConsumerManagement/ChangeCustomerContactInfo.aspx',1,(SELECT MAX(Page_Order)+1 FROM Tbl_Menus WHERE ReferenceMenuId = 1),1) 

INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,176,1,1,'Admin',GETDATE(),1)
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,176,1,1,'Admin',GETDATE(),1)
GO


