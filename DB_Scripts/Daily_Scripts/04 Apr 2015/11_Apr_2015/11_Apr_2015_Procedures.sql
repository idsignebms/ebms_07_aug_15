
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 04/11/2015 21:19:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
CREATE PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
    SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo
	ORDER BY CustomerPaymentID DESC
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		,ReadType
		,Usage AS Consumption
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 
		,CD.GlobalAccountNumber AS AccountNo
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
											,CD.Service_Landmark    
											,CD.Service_City,''    
											,CD.Service_ZipCode) AS ServiceAddress
		,CAD.OutStandingAmount AS TotalDueAmount
		,Dials AS MeterDials
		,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal  
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,COUNT(0) OVER() AS TotalRecords
	FROM Tbl_CustomerBills CB
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber
			AND CD.GlobalAccountNumber = @AccountNo	AND ISNULL(CB.PaymentStatusID,2) = 2
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo 
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- Modified By -- Bhimaraju Vanka
-- Modified Date -- 27-03-2015
-- Reason : Added/Inserting one more field MeterReading From
-- Modified By -- Jeevan Amunuri
-- Modified Date -- 03-04-2015
-- Modified By -- Karteek
-- Modified Date -- 06-04-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999999999999'
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

	IF(@IsTamper=0)
		BEGIN
			SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
		END
		
	IF(@IsRollover=1)
		BEGIN
			SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
			Declare @FirstReadingUsage bigint = convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
			Declare @SecountReadingUsage Bigint =case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
			SET @Usage=@FirstReadingUsage
			IF @SecountReadingUsage != 0
				BEGIN
				   	INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom])
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,CONVERT(VARCHAR(50),@Previous)
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom)
				END
 
		END
		
	SELECT @AverageReading = dbo.fn_GetAverageReading(@AccNum,@Usage)

	INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom])
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,CONVERT(VARCHAR(50),@Current)
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom)

	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillingDisabledBookNo]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Bhimaraju Vanka    
-- Create date: 24-Sep-2014    
-- Modified By: T.Karthik    
-- Modified Date : 17-10-2014    
-- Description: Purpose is To get List Of Billing Disabled BookNo   
-- Modified By: Faiz-ID103
-- Modified Date: 10-Apr-2015
-- Description: Disable field added  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBillingDisabledBookNo]  
(    
 @XmlDoc Xml    
)    
AS    
BEGIN    
    
 DECLARE  @PageNo INT    
   ,@PageSize INT    
   ,@YearId INT    
   ,@MonthId INT    
   ,@BU_ID VARCHAR(50)  
        
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')    
    ,@PageSize = C.value('(PageSize)[1]','INT')    
    ,@YearId=C.value('(YearId)[1]','INT')    
    ,@MonthId=C.value('(MonthId)[1]','INT')    
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')           
  FROM @XmlDoc.nodes('BillingDisabledBooksBe') AS T(C)     
      
  CREATE TABLE #BillingDisabledBookNos(Sno INT PRIMARY KEY IDENTITY(1,1),DisabledBookId INT)    
    
  INSERT INTO #BillingDisabledBookNos(DisabledBookId)    
   SELECT    
    DisabledBookId    
   FROM Tbl_BillingDisabledBooks    
   WHERE IsActive = 1    
   AND (YearId=@YearId OR @YearId=0)    
   AND (MonthId=@MonthId OR @MonthId=0)    
   ORDER BY CreatedDate DESC    
       
  DECLARE @Count INT=0    
  SET @Count=(SELECT COUNT(0) FROM #BillingDisabledBookNos)    
      
  SELECT    
    Sno AS RowNumber    
    ,BDB.BookNo AS BookNo  -- Faiz_ID103  
    ,BN.ID AS ID  
    ,BDB.DisabledBookId    
    ,ISNULL(Remarks,'--') AS Details    
    ,MonthId    
    ,YearId    
    ,((select dbo.fn_GetMonthName([MonthId]))+'-'+CONVERT(varchar(10), [YearId])) AS MonthYear    
    ,@Count AS TotalRecords  
    ,(CASE IsPartialBill WHEN 1 THEN 'Partial Book' ELSE DisableType END) AS DisableType  
    ,ISNULL((CONVERT(VARCHAR(20),DisableDate,106)),'--') AS DisableDate
  FROM Tbl_BillingDisabledBooks AS BDB    
  JOIN #BillingDisabledBookNos LBDB ON BDB.DisabledBookId=LBDB.DisabledBookId    
  INNER JOIN UDV_BookNumberDetails BN on BDB.BookNo=BN.BookNo   
 AND BN.BU_ID = @BU_ID  
  INNER JOIN TBl_MDisableType D ON D.DisableTypeId = BDB.DisableTypeId  
  --INNER JOIN Tbl_Cycles C ON C.CycleId = BN.CycleId  
  --INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = C.ServiceCenterId  
  --INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID   
  AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
       
 END    
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml    
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,BookCode  
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL   
  ,PAD1.AreaCode AS  AreaPostal    
  ,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	
		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)


		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')


		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and  
		(CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
		@IsExists = (Case when	 CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) 
		,@LatestDate=ReadDate  
		,@AverageReading=AverageReading 
		,@TotalReadings=TotalReadings  
		,@PreviousReading = PreviousReading
		,@PresentReading=PresentReading 
		,@IsBilled=IsBilled
		,@ReadingsMeterNumber=MeterNumber 
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC


		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  


		SELECT TOP(1) @Month = BillMonth ,
		@Year = BillYear ,@LastBillReadType= 
		(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,@IsExists as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,@IsBilled as	 IsBilled
				 ,@IsLatestBill as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,@InititalReading as	 InititalReading
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
    SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo
	ORDER BY CustomerPaymentID DESC
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		,ReadType
		,Usage AS Consumption
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 
		,CD.GlobalAccountNumber AS AccountNo
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
											,CD.Service_Landmark    
											,CD.Service_City,''    
											,CD.Service_ZipCode) AS ServiceAddress
		,CAD.OutStandingAmount AS TotalDueAmount
		,Dials AS MeterDials
		,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal  
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,COUNT(0) OVER() AS TotalRecords
	FROM Tbl_CustomerBills CB
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber
			AND CD.GlobalAccountNumber = @AccountNo	AND ISNULL(CB.PaymentStatusID,2) = 2
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo 
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  Bhimaraju V 
-- Create date: 12-03-2014 5 
-- Description: Get Details For PaymentEntry-AccountWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)
	
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('PaymentsBe') as T(C)	

	SELECT CD.GlobalAccountNumber
		,CD.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,MeterNumber
		,ClassName
		,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
             ,CD.Service_Landmark    
             ,CD.Service_City,''    
             ,CD.Service_ZipCode) AS ServiceAddress 
		,OutStandingAmount
		,ISNULL(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber),'--') AS LastPaidDate
		,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS LastPaidAmount
		,ISNULL(dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber),'--') AS LastBillGeneratedDate		
		,dbo.fn_GetCustomerLastBillAmountWithoutArrears(CD.GlobalAccountNumber) AS LastBillAmount
		,1 AS IsExists
	FROM UDV_CustomerDescription CD
	WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
	FOR XML PATH('PaymentsBe')
	 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
	SET @Active = [dbo].[fn_IsAccountNoExists_BU_Id](@AccountNo,@BU_ID)

	SELECT  
		 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
		,ClassName  
		,OutStandingAmount AS DueAmount  --Commented By Raja-ID065
		,(SELECT [dbo].[fn_IsAccountNoExists_BU_Id](@AccountNo,@BU_ID)) AS IsCustomerExists  
		,GlobalAccountNumber AS AccountNo
	FROM [UDV_CustomerDescription] 
	WHERE (GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo)		    
	AND(BU_ID = @BU_ID OR @BU_ID = '')
	FOR XML PATH('MastersBE')  
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentEntryListByBatchNo]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  Neeraj Kanojiya        
-- Create date: 11-04-2014        
-- Modified date : 16-04-2014        
-- Description: The purpose of this procedure is to Get PaymentEntry list by Batch no        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetPaymentEntryListByBatchNo]         
(        
@XmlDoc xml        
)        
AS        
BEGIN        
	DECLARE @BatchNo INT        
		,@Flag INT    
		    
	SELECT        
		@BatchNo=C.value('(BatchNo)[1]','INT'),        
		@Flag=C.value('(Flag)[1]','INT')        
	FROM @XmlDoc.nodes('MastersBE') AS T(C)  
	       
	IF(@Flag=1)        
		BEGIN        
			SELECT        
			(        
				SELECT         
					 CP.AccountNo,CP.CustomerPaymentID,cbp.BillNo
					,Convert(decimal(18,2), cbp.PaidAmount) AS BillTotal   
					,CP.PaidAmount,cbp.CustomerBillPaymentId as CustomerBPID        
				FROM Tbl_CustomerPaymentsApproval AS CP 
				LEFT JOIN Tbl_CustomerBillPaymentsApproval cbp ON CP.CustomerPaymentID = cbp.CustomerPaymentId       
				WHERE CP.BatchNo = @BatchNo AND CP.ActivestatusId = 1         
				ORDER BY CP.CreatedDate DESC        
				FOR XML PATH('MastersBE'),TYPE        
			)        
			FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
		END        
	IF(@Flag=2)        
		BEGIN        
			SELECT        
			(        
				SELECT         
					 CP.AccountNo,CP.CustomerPaymentID,cbp.BillNo
					,Convert(decimal(18,2), cbp.PaidAmount) AS BillTotal                  
					,CP.PaidAmount,cbp.CustomerBillPaymentId as CustomerBPID        
				FROM Tbl_CustomerPayments AS CP 
				LEFT JOIN Tbl_CustomerBillPayments cbp on CP.CustomerPaymentID = cbp.CustomerPaymentId        
				WHERE CP.BatchNo = @BatchNo AND CP.ActivestatusId = 1         
				ORDER BY CP.CreatedDate DESC        
				FOR XML PATH('MastersBE'),TYPE        
			)        
			FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
		END         
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPendingBills]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  T.Karthik      
-- Create date: 12-04-2014      
-- Modified date : 15-04-2014      
-- Description: The purpose of this procedure is to get Customer Pending bills      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetCustomerPendingBills]      
(      
	@XmlDoc xml      
)      
AS      
BEGIN      
	DECLARE @AccountNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		
	SELECT      
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')      
	FROM @XmlDoc.nodes('MastersBE') AS T(C)
 
	--IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	--	BEGIN
	--		SET @AccountNo = (SELECT GlobalAccountNumber FROM UDV_CustomerDescription 
	--						WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo))
	--	END 
       
	SELECT      
	(      
		SELECT      
			 BillNo      
			,CONVERT(VARCHAR(30),BillGeneratedDate,103) AS BillDate  
			,CONVERT(DECIMAL(20,2),TotalBillAmountWithTax) AS BillTotal 
			,((CONVERT(DECIMAL(18,2),Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount)) - ISNULL(dbo.fn_GetTotalPaidAmountOfBill(BillNo),0)) AS DueAmount-- Neeraj-ID077 For Due Bill    
		FROM Tbl_CustomerBills AS CB
		WHERE AccountNo = @AccountNo AND PaymentStatusID = 2 AND ActiveStatusId = 1      
		ORDER BY BillYear ASC,BillMonth ASC      
		FOR XML PATH('MastersBE'),TYPE      
	)      
	FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
END     
----------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBatchTotal]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 16-04-2014    
-- Description: The purpose of this procedure is to Update Batch TOTAL    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_UpdateBatchTotal]     
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BatchNo INT    
		,@ModifiedBy VARCHAR(50)    
		,@BatchTotal NUMERIC(20,4)
		,@Flag INT
		,@IsSuccess BIT = 0    
	     
	SELECT    
		 @BatchNo=C.value('(BatchNo)[1]','INT')    
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
		,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')    
		,@Flag=C.value('(Flag)[1]','INT')    
	FROM @XmlDoc.nodes('BillingBE') as T(C)  
	  
	 IF(@Flag = 1)  --Updating in temp table
		BEGIN
			UPDATE Tbl_BatchDetailsApproval SET BatchTotal = BatchTotal + @BatchTotal WHERE BatchNo = @BatchNo    
			SET @IsSuccess = 1    
		END
	 ELSE IF(@Flag = 2) --Updating in base table
		BEGIN
			UPDATE Tbl_BatchDetails SET BatchTotal = BatchTotal + @BatchTotal WHERE BatchNo = @BatchNo    
			SET @IsSuccess = 1
		END
		SELECT @IsSuccess As IsSuccess
		FOR XML PATH('BillingBE'),TYPE	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_DeletePaymentEntryList]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author:  Neeraj Kanojiya        
-- Create date: 11-04-2014        
-- Modified date : 16-04-2014        
-- Description: The purpose of this procedure is to delete PaymentEntry list        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_DeletePaymentEntryList]         
(        
@XmlDoc xml        
)        
AS        
BEGIN        
	DECLARE @CustomerPaymentID INT        
		,@CustomerBPID INT     
		,@Flag INT
		,@IsSuccess BIT = 0       
         
	SELECT        
		 @CustomerPaymentID=C.value('(CustomerPaymentID)[1]','INT')        
		,@CustomerBPID=C.value('(CustomerBPID)[1]','INT')   
		,@Flag=C.value('(Flag)[1]','INT')             
	FROM @XmlDoc.nodes('MastersBE') AS T(C)
 
	DECLARE @AccountNo VARCHAR(50), @PaiddAmount DECIMAL(18,2)
 
	SELECT @AccountNo = AccountNo, @PaiddAmount = ISNULL(PaidAmount,0) FROM Tbl_CustomerPayments WHERE CustomerPaymentId = @CustomerPaymentID
          
	IF(@Flag=1)  --Deletion in temp table    
		BEGIN    
			DELETE Tbl_CustomerBillPaymentsApproval WHERE CustomerPaymentId = @CustomerPaymentID        
			DELETE Tbl_CustomerPaymentsApproval WHERE CustomerPaymentID = @CustomerPaymentID 
			
			SET @IsSuccess = 1     
		End  
	ELSE IF(@Flag=2) --Deletion in base table    
		BEGIN  
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET OutStandingAmount = ISNULL(OutStandingAmount,0) + ISNULL(@PaiddAmount,0)
			WHERE GlobalAccountNumber = @AccountNo

			UPDATE Tbl_CustomerBills SET PaymentStatusID=2 --Opening bill which are closed after payment.
			WHERE BillNo IN(SELECT BillNo FROM Tbl_CustomerBillPayments 
							WHERE CustomerPaymentId = @CustomerPaymentID)

			DELETE Tbl_CustomerBillPayments WHERE CustomerPaymentId = @CustomerPaymentID 
			DELETE Tbl_CustomerPayments WHERE CustomerPaymentID = @CustomerPaymentID
			
			SET @IsSuccess = 1         
		End  
	SELECT @IsSuccess AS IsSuccess FOR XML PATH('MastersBE')
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillAdjustmentDetails]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  : NEERAJ KANOJIYA                
 -- Create date  : 7 OCT 2014                  
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT #        
 -- Modified BY : Suresh Kumar Dasi 
 -- Modified By : Padmini
 -- Modified Date : 25-12-2014    
 -- =============================================                  
 ALTER PROCEDURE [dbo].[USP_GetBillAdjustmentDetails]                  
 (                  
 @XmlDoc xml                  
 )                  
 AS                  
 BEGIN                  
  DECLARE	@SearchValue VARCHAR(50)
			,@AccountNo VARCHAR(20)                 
			,@BillNo VARCHAR(20)            
		--	,@OldAccountNo VARCHAR(20)            
		--	,@MeterNo VARCHAR(20)            
		--	,@CustomerBillId INT      
  SELECT       
	@SearchValue = C.value('(SearchValue)[1]','VARCHAR(50)')                                     
   --@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)'),          
   --@BillNo = C.value('(BillNo)[1]','VARCHAR(50)'),                          
   --@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(20)'),                          
   --@MeterNo = C.value('(MeterNo)[1]','VARCHAR(20)')                          
  FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
   
    
  --IF(@AccountNo = '' OR @AccountNo IS NULL)              
  -- BEGIN          
  -- SELECT TOP 1 @AccountNo = AccountNo FROM Tbl_CustomerBills WHERE BillNo=@BillNo OR MeterNo=@MeterNo    
  -- IF(@AccountNo = '' OR @AccountNo IS NULL)         
  --  SELECT @AccountNo=AccountNo FROM Tbl_CustomerDetails WHERE OldAccountNo=@OldAccountNo  
  -- END   
  --IF NOT EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo=@SearchValue) 
	 -- BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.AccountNo)
		--				FROM Tbl_CustomerDetails AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.AccountNo=CB.AccountNo
		--				WHERE CD.OldAccountNo=@SearchValue OR CD.MeterNo=@SearchValue OR CB.BillNo=@SearchValue)					
	 -- END
    IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@SearchValue )
	  BEGIN
		--SET @AccountNo= (SELECT	TOP 1(CD.GlobalAccountNumber)
		--				FROM [CUSTOMERS].[Tbl_CustomerSDetail] AS CD
		--				JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
		--				JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber 			
		--				WHERE CD.OldAccountNo=@SearchValue OR CPD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue)					
		
		SET @AccountNo= (SELECT GlobalAccountNumber 
							FROM UDV_CustomerDescription CD
							LEFT JOIN Tbl_CustomerBills AS CB ON CD.GlobalAccountNumber=CB.AccountNo
							WHERE (CD.OldAccountNo=@SearchValue OR CD.MeterNumber=@SearchValue OR CB.BillNo=@SearchValue))
		
	  END
  ELSE
	SET @AccountNo=@SearchValue
            
  SELECT                
  (                
  --Get customers details by account #                
  SELECT                
   A.GlobalAccountNumber AS CustomerID               
  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name             
  ,A.GlobalAccountNumber AS AccountNo              
  ,A.DocumentNo       
  ,A.MeterNumber AS MeterNo  
  ,A.OldAccountNo               
  ,A.RouteSequenceNo AS RouteSequenceNumber                 
  ,B.BusinessUnitName                  
  ,D.ServiceUnitName                  
  ,C.ServiceCenterName                  
  ,ReadCodeId                
  ,A.TariffId AS ClassID                
  ,E.ClassName               
  ,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings                  
  ,A.OutStandingAmount AS TotalDueAmount --Commented by Raja-ID065    
  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount --Commented by Raja-ID065    
  --,(SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC) AS TotalDueAmount --Raja-ID065    
  ,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate                  
  ,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount              
  ,dbo.fn_GetCustomerServiceAddress(GlobalAccountNumber) AS ServiceAddress              
  ,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate       
  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials                   
    FROM [UDV_CustomerDescription] AS A                 
    JOIN Tbl_BussinessUnits  AS B ON A.BU_ID = B.BU_ID                
    JOIN Tbl_ServiceCenter   AS C ON A.ServiceCenterId=C.ServiceCenterId                
    JOIN Tbl_ServiceUnits    AS D ON A.SU_ID=D.SU_ID                
    JOIN Tbl_MTariffClasses AS E ON A.TariffId = E.ClassID                   
    WHERE GlobalAccountNumber = @AccountNo                     
    FOR XML PATH('Customerdetails'),TYPE                  
 )              
 ,                
 (                
  --Get customers bill details by account #                
  SELECT TOP(3)                
   A.CustomerBillId AS CustomerBillID,                
  (SELECT BillAdjustmentId FROM Tbl_BillAdjustments AS B WHERE B.CustomerBillId = A.CustomerBillID) AS BAID,              
   A.CustomerId,                
   CONVERT(DECIMAL(18,2),A.Vat) AS Vat,              
   A.BillNo,              
   A.AccountNo,                        
   A.BillNo,                 
   ISNULL(A.PreviousReading,'0') AS PreviousReading,                  
   ISNULL(A.PresentReading,'0') AS PresentReading,                  
   A.Usage AS Consumption,                
   A.BillYear,                
   A.BillMonth,       A.ReadCodeId  
   ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(A.BillYear,A.BillMonth, A.AccountNo)) AS IsActiveMonth, --Raja-ID065 For getting month locked or not    
   (SELECT dbo.fn_IsCustomerBill_Latest(A.CustomerBillId,A.AccountNo)) AS IsSuccess, --For Checking Is this Bill Latest or NOT ?  
   A.NetEnergyCharges AS NetEnergyCharges,        
   --(SELECT dbo.fn_IsCustomerBilledMonthLocked(CONVERT(int, A.BillYear),CONVERT(int, A.BillMonth), A.AccountNo)) AS IsActiveMonth ,      
   --dbo.fn_GetBillPayments(A.CustomerBillId) AS TotaPayments,          
   dbo.fn_GetTotalPaidAmountOfBill(A.BillNo) AS TotaPayments,          
   --dbo.fn_GetDueBill(A.BillNo) AS DueBill,  --Commented Bu Raja-ID065    
   --CONVERT(DECIMAL(20,2),A.TotalBillAmountWithTax - dbo.fn_GetBillPayments(A.CustomerBillId)) AS DueBill, --Commented By Raja-ID065    
   (SELECT dbo.fn_IsBillAdjusted(A.CustomerBillId))AS IsAdjested,    
   (SELECT ApprovalStatusId FROM Tbl_BillAdjustments WHERE CustomerBillId = A.CustomerBillId) AS ApprovalStatusId,              
   CONVERT(DECIMAL(20,2),TotalBillAmount) AS TotalBillAmount,    
   (CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal, -- Raja-ID065 For GrandTotal    
   ((CONVERT(DECIMAL(18,2),A.Vat) + CONVERT(DECIMAL(20,2),TotalBillAmount))-ISNULL(dbo.fn_GetTotalPaidAmountOfBill(A.BillNo),0)) AS DueBill,-- Raja-ID065 For Due Bill    
   --(SELECT ReadCodeId FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS ReadCode,   
   --(SELECT OldAccountNo FROM Tbl_CustomerDetails WHERE AccountNo =@AccountNo) AS OldAccountNo,
   (SELECT ReadCodeId FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber =@AccountNo) AS ReadCode,   
   (SELECT OldAccountNo FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber =@AccountNo) AS OldAccountNo,                               
   (SELECT top(1) CustomerReadingId FROM Tbl_CustomerReadings              
    WHERE GlobalAccountNumber=@AccountNo AND               
   ISBILLED=1               
    ORDER BY CustomerReadingId DESC) AS CustomerReadingId              
 --     A.CustomerReadingId                
   FROM Tbl_CustomerBills  AS A                       
  WHERE AccountNo = @AccountNo   
  AND BillNo = (CASE WHEN ISNULL(@BillNo,'') = '' THEN BillNo ELSE @BillNo END)   
  AND PaymentStatusID = 2                
  ORDER BY CustomerBillID DESC                      
  FOR XML PATH('CustomerBillDetails'),TYPE                  
 )                
 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                  
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillPaymentsApproval]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Suresh Kumar D
-- Create date: 18-07-2014
-- Description:	Insert Bill Payments With Approval
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertBillPaymentsApproval]
	(
	@XmlDoc xml
	)
AS
BEGIN
	   DECLARE  @CustomerID VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@ReceiptNo VARCHAR(20)
				,@PaymentMode INT
				,@DocumentPath VARCHAR(MAX)
				,@DocumentName VARCHAR(MAX)
				,@BillNo VARCHAR(50)
				,@Cashier VARCHAR(50)
				,@CashOffice INT
				,@CreatedBy VARCHAR(50)
				,@BatchNo INT
				,@PaidAmount DECIMAL(20,4)
				,@PaymentRecievedDate VARCHAR(20)
				,@CustomerPaymentId INT
				,@EffectedRows INT
		DECLARE @PaidBillTemp TABLE (CustomerBillId INT,AmountPaid DECIMAL(18,2),BillNo VARCHAR(50),BillStatus INT)		
			
			
		SELECT   @CustomerID=C.value('(CustomerID)[1]','VARCHAR(50)')
				,@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
				,@ReceiptNo=C.value('(ReceiptNo)[1]','VARCHAR(20)')
				,@PaymentMode=C.value('(PaymentModeId)[1]','INT')
				,@DocumentPath=C.value('(DocumentPath)[1]','VARCHAR(MAX)')
				,@DocumentName=C.value('(Document)[1]','VARCHAR(MAX)')
				,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
				,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
				,@BatchNo=C.value('(BatchNo)[1]','INT')
				,@PaidAmount=C.value('(PaidAmount)[1]','DECIMAL(20,4)')
				,@PaymentRecievedDate=C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)	
	
		IF(@AccountNo IS NOT NULL)
			BEGIN			
				DELETE FROM @PaidBillTemp
				INSERT INTO @PaidBillTemp
				SELECT CustomerBillId,PaidAmount,BillNo,BillStatus FROM [dbo].fn_GetBillWiseBillAmountByAccountNo(@AccountNo,@PaidAmount)
				
			
				INSERT INTO Tbl_CustomerPaymentsApproval(
					 AccountNo
					,ReceiptNo
					,PaymentMode
					,DocumentPath
					,DocumentName
					,Cashier
					,CashOffice
					,CreatedBy
					,CreatedDate
					,BatchNo
					,RecievedDate
					,PaidAmount
					,ActivestatusId
					)
				VALUES( 	
					 @AccountNo
					,@ReceiptNo
					,@PaymentMode
					,@DocumentPath
					,@DocumentName
					,@Cashier
					,@CashOffice
					,@CreatedBy
					,dbo.fn_GetCurrentDateTime()
					,@BatchNo
					,@PaymentRecievedDate
					,@PaidAmount
					,1
					)
					
				SELECT @CustomerPaymentId  = SCOPE_IDENTITY()					
				
				INSERT INTO Tbl_CustomerBillPaymentsApproval(CustomerPaymentId,PaidAmount,BillNo,CreatedBy,CreatedDate)				
				SELECT @CustomerPaymentId,AmountPaid,BillNo,@CreatedBy,dbo.fn_GetCurrentDateTime() FROM @PaidBillTemp
							
				UPDATE CB 
					SET PaymentStatusID = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.AmountPaid,0))
				FROM Tbl_CustomerBills CB
				INNER JOIN @PaidBillTemp PB ON CB.BillNo = PB.BillNo
				
				SELECT @EffectedRows = @@ROWCOUNT
				
				UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
				SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
				WHERE GlobalAccountNumber = @AccountNo
				
				SELECT @EffectedRows = @@ROWCOUNT
				
				SELECT 
					 1 AS IsCustomerExists
					,(CASE WHEN @EffectedRows > 0 THEN 1 ELSE 0 END)AS IsSuccess
					,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPaymentsApproval 
						WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount
				FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo
				FOR XML PATH ('MastersBE')	
					
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteBatchWithPayments]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Neeraj Kanojiya    
-- Create date: 07-May-2014    
-- Modified date : Null    
-- Description: The purpose of this procedure is to delete Batche with PaymentEntry list    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_DeleteBatchWithPayments]     
(    
	@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @CustomerPaymentID INT    
		,@CustomerBPID INT    
		,@BatchNO INT    
     
	SELECT    
		 @CustomerPaymentID=C.value('(CustomerPaymentID)[1]','INT')    
		,@CustomerBPID=C.value('(CustomerBPID)[1]','INT')    
		,@BatchNo=C.value('(BatchNo)[1]','int')      
	FROM @XmlDoc.nodes('MastersBE') AS T(C)     
     
	DELETE Tbl_CustomerBillPaymentsApproval WHERE CustomerPaymentId IN (SELECT CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNO)    
	DELETE Tbl_CustomerPaymentsApproval WHERE BatchNo=@BatchNO    
	DELETE Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNO   

	DELETE Tbl_CustomerBillPayments WHERE CustomerPaymentId IN (SELECT CustomerPaymentID FROM Tbl_CustomerPayments WHERE BatchNo=@BatchNO)    
	DELETE Tbl_CustomerPayments WHERE BatchNo=@BatchNO    
	DELETE Tbl_BatchDetails WHERE BatchNo=@BatchNO   

	SELECT @@ROWCOUNT AS IsSuccess FOR XML PATH('MastersBE')    
     
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBatchDetails]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-03-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertBatchDetails](@XmlDoc XML)      
AS            
BEGIN            
	DECLARE  @BatchNo int,            
		 @BatchName varchar(50) ,            
		 @BatchDate datetime ,            
		 @CashOffice INT ,            
		 @CashierID VARCHAR(50) ,            
		 @CreatedBy varchar(50),            
		 @BatchTotal NUMERIC(20,4),            
		 @Description VARCHAR(MAX), 
		 @BU_ID VARCHAR(50),       
		 @Flag INT            
             
	SELECT             
		 @BatchNo=C.value('(BatchNo)[1]','int')            
		,@BatchName=C.value('(BatchName)[1]','varchar(50)')            
		,@BatchDate=C.value('(BatchDate)[1]','DATETIME')            
		,@CashOffice = C.value('(OfficeId)[1]','INT')            
		,@CashierID = C.value('(CashierId)[1]','VARCHAR(50)')             
		,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')            
		,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')            
		,@Description=C.value('(Description)[1]','VARCHAR(MAX)')            
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')            
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)         
 --TEMP INSERTING FOR APPROVAL           
	IF(@Flag=1)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo AND BatchStatus = 1))            
				BEGIN            
					INSERT INTO Tbl_BatchDetailsApproval(            
					BatchNo,            
					BatchName,            
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,            
					[Description]
					)            
					VALUES(            
					@BatchNo,            
					Case  @BatchName when '' then null else @BatchName end,  
					@BatchDate,  
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					Case  @CreatedBy when '' then null else @CreatedBy end,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					CASE @Description WHEN '' THEN NULL ELSE @Description END
					)          
					SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')            
				END            
			ELSE
				BEGIN            
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate,BatchName  
						,BatchNo  
						,BatchTotal  
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0))   
					FROM Tbl_CustomerPaymentsApproval            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetailsApproval   
					WHERE BatchNo=@BatchNo   
					FOR XML PATH('BillingBE') 
				END 
		END      
	IF(@Flag=2)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails 
								WHERE BatchNo=@BatchNo 
								--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
								AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
								AND BU_ID=@BU_ID))            
				BEGIN            
					INSERT INTO Tbl_BatchDetails(            
					BatchNo,             
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,  
					BU_ID           
					)            
					VALUES(            
					@BatchNo,     
					@BatchDate,            
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					@CreatedBy,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					@BU_ID       
					)            
					SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')            
				END            
			ELSE 
				BEGIN           
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate  
						,BatchName  
						,BatchNo  
						,BatchTotal  
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetails   
					WHERE BatchNo=@BatchNo 
					--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
					AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
					AND BU_ID=@BU_ID
					FOR XML PATH('BillingBE') 
				END 
		END      
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchDetailsByBatchNo]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 11-04-2014    
-- Modified Date:14-04-2014    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBatchDetailsByBatchNo]     
(    
	@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BatchNo INT    
		,@Flag INT      
		,@BU_ID VARCHAR(50)       
		,@BatchDate DATE 
	SELECT    
		 @BatchNo=C.value('(BatchNo)[1]','INT')    
		,@Flag=C.value('(Flag)[1]','INT')        
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')        
		,@BatchDate=C.value('(BatchDate)[1]','DATE') 
	FROM @XmlDoc.nodes('BillingBE') as T(C)    
 
	IF(@Flag=1)--GET DETAILS FROM TEMP TABLES    
		BEGIN    
			SELECT    
				 CONVERT(VARCHAR(20),BatchDate,103) AS BatchDate    
				,BatchName    
				,BatchNo    
				,CONVERT(DECIMAL(20,2),BatchTotal) AS BatchTotal    
				,CashierID AS CashierId    
				,CashOffice AS OfficeId    
				,[Description]    
				,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPaymentsApproval 
						WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount    
			FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo    
			FOR XML PATH('BillingBE')    
		END    
	ELSE IF(@Flag=2)--GET DETAILS FROM PERMANENT TABLE    
		BEGIN    
			SELECT     
				 CONVERT(VARCHAR(20),BatchDate,103) AS BatchDate    
				,BatchName    
				,BatchNo    
				,CONVERT(DECIMAL(20,2),BatchTotal) AS BatchTotal    
				,CashierID AS CashierId    
				,CashOffice AS OfficeId    
				,[Description]    
				,(SELECT CONVERT(DECIMAL(20,2),BatchTotal- ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments 
						WHERE BatchNo=@BatchNo AND ActivestatusId=1) AS BatchPendingAmount    
			FROM Tbl_BatchDetails 
			WHERE BatchNo=@BatchNo   
			--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120)
			AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
			AND BU_ID=@BU_ID 
			FOR XML PATH('BillingBE')    
		END    
END    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 06-10-2014   
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015      
-- Description: The purpose of this procedure is to get limited Meter change request logs    
-- =============================================       
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''    
      
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
    
	-- start By Karteek  
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek   
      
	SELECT(    
		 SELECT TOP 10 CMI.AccountNo    
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType    
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType    
			   ,CMI.Remarks    
			   ,CMI.OldDials    
			   ,CMI.NewDials    
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate     
			   ,ApproveSttus.ApprovalStatus    
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name    
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI    
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber    
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)    
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID     
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID  
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId  
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId  
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo  
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId  
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId      
		 -- changed by karteek end     
		 GROUP BY CMI.AccountNo    
			,OldMeterTypeId    
		    ,NewMeterTypeId    
		    ,CMI.Remarks    
			,CMI.OldDials    
		    ,CMI.NewDials    
		    ,CONVERT(VARCHAR(30),CMI.CreatedDate,107)    
		    ,ApproveSttus.ApprovalStatus    
		    ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName    
		FOR XML PATH('AuditTrayList'),TYPE    
	)    
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
	
END   
---------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 BookChange.AccountNo  
			,BookChange.ApproveStatusId  
			,BookChange.OldBookNo  
			,BookChange.NewBookNo  
			,BookChange.Remarks  
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId 
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--GROUP BY BookChange.AccountNo  
		--   ,BookChange.ApproveStatusId  
		--   ,BookChange.OldBookNo  
		--   ,BookChange.NewBookNo  
		--   ,BookChange.Remarks  
		--   ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107)  
		--   ,ApproveSttus.ApprovalStatus  
		--   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsWithLimit]    Script Date: 04/11/2015 21:20:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014   
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
---- Description: The purpose of this procedure is to get limited Tariff change request logs  
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 TCR.AccountNo  
			,TCR.ApprovalStatusId  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
			,TCR.Remarks  
			,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_LCustomerTariffChangeRequest  TCR  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId   
		--GROUP BY TCR.AccountNo  
		--,TCR.ApprovalStatusId  
		--,PreviousTariffId  
		--,ChangeRequestedTariffId  
		--,TCR.Remarks  
		--,CONVERT(VARCHAR(30),TCR.CreatedDate,107)  
		--,ApproveSttus.ApprovalStatus  
		--,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit]    Script Date: 04/11/2015 21:20:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek     
     
	SELECT
	(  
		SELECT  TOP 10 CCL.AccountNo  
			,CCL.ApproveStatusId  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			,CCL.Remarks  
			,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		--GROUP BY CCL.AccountNo  
		--,CCL.ApproveStatusId  
		--,CCL.Remarks  
		--,CONVERT(VARCHAR(30),CCL.CreatedDate,107)   
		--,ApproveSttus.ApprovalStatus  
		--,OldStatus  
		--,NewStatus  
		--,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/11/2015 21:20:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 
ALTER PROCEDURE  [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			 
								
	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET 	@CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
	
	 --select * from #tmpCustomerBillsDetails		  
	-- Looping cycle id and get each cycle info 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId
							
							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo
							
							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END
							
							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							 
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------
							
							
				 
							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   
						   IF	 isnull(@ActiveStatusId,0) != 1	  or	 isnull(@ActiveStatusId,0) !=2    or isnull(@CustomerTypeID,0) = 3	 
						   BEGIN
						   GOTO Loop_End;
						   END   -- In Active Customer

							select @IspartialClose=IsPartialBill,@BookDisableType=DisableTypeId 
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo and YearId=@Year and MonthId=@Month	and IsActive=1


							IF isnull(@BookDisableType,0)   = 1
		 						GOTO Loop_End;

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
								END
							END
							ELSE -- -- Direct customer
							BEGIN

								set @IsEstimatedRead =1
								-- Get balance usage of the customer
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

							
						 
								      
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 or  isnull(@ActiveStatusId,0) =2
							BEGIN
								
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END
							
										INSERT INTO @tblFixedCharges(ClassID ,Amount)
										SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges				
									 
							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
						
						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						
						---------------------------------------------------Set Variables to NULL-
						
						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						
						-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH
					 
					 
					 
			END CATCH
		END
		
		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 04/11/2015 21:20:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================          
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <25-MAR-2014>          
-- Description: Customer Payment from file upload   
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <24-MAR-2015>          
-- Description: Romove join to check customer bill in process
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]          
 (  
@XmlDoc XML  
,@MultiXmlDoc XML  
 )          
AS          
BEGIN          
          
 DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)     
        ,ReceivedDate DATETIME,PaymentModeId VARCHAR(50))    
 DECLARE @Bu_Id VARCHAR(50)  
   
 SELECT   
 @Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('PaymentsBe') as T(C)      
         
 INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)      
 SELECT             
  c.value('(SNO)[1]','INT')                         
 ,c.value('(AccountNo)[1]','VARCHAR(50)')  
 ,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                         
    ,c.value('(ReceiptNO)[1]','VARCHAR(50)')                         
    ,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)    
    ,c.value('(PaymentMode)[1]','VARCHAR(50)')    
 FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)        
  SELECT    
  (    
   SELECT     
   ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
   ,CD.GlobalAccountNumber  AS AccountNo  
  , CD.OldAccountNo AS OldAccountNo  
     ,TempDetails.AmountPaid AS PaidAmount    
     ,TempDetails.ReceiptNO    
     ,TempDetails.ReceivedDate   
     ,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
  ,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid, TempDetails.ReceiptNO) AS IsDuplicate  
  ,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists    
  ,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId)   
    ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentMode=PM.PaymentMode) END) AS PaymentMode    
  ,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId    
  --,(CASE WHEN (SELECT TempDetails.AccountNo FROM Tbl_BillingQueeCustomers     
  --  WHERE BillGenarationStatusId !=4 AND AccountNo=TempDetails.AccountNo) IS NULL THEN 0 ELSE 1 END) AS IsBillInProcess    
 -- ,dbo.fn_GetCustomerBillPendings(TempDetails.AccountNo) AS TotalPendingBills     
   FROM @TempCustomer AS TempDetails     
    INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo  
    LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentMode    
 FOR XML PATH('PaymentsList'),TYPE      
 )    
 FOR XML PATH(''),ROOT('PaymentsInfoByXml')        
END   
------------------------------------------------------------------------------------------------------------------
--SELECT * FROM Tbl_MPaymentMode
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillingDisabledBookNos]    Script Date: 04/11/2015 21:20:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Bhimaraju.V    
-- Create date: 25-09-2014    
-- Description: The purpose of this procedure is to insert Details of Disabled BookNos for Billing     
-- Modified By: Faiz-ID103
-- Modified Date: 10-Apr-2015
-- Description: Added the disable date
-- =============================================    
ALTER PROCEDURE [dbo].[USP_InsertBillingDisabledBookNos]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @BookNo VARCHAR(20)  
  ,@Details VARCHAR(MAX)  
  ,@YearId INT  
  ,@MonthId INT    
  ,@CreatedBy VARCHAR(50)  
  ,@ApproveStatusId INT  
  ,@DisableTypeId INT    
  ,@DisableDate VARCHAR(10)  
      
 SELECT    
   @BookNo=C.value('(BookNo)[1]','VARCHAR(20)')    
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
  ,@YearId=C.value('(YearId)[1]','INT')    
  ,@MonthId=C.value('(MonthId)[1]','INT')    
  ,@ApproveStatusId=C.value('(ApproveStatusId)[1]','INT')    
  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')    
  ,@DisableTypeId=C.value('(DisableTypeId)[1]','INT')    
  ,@DisableDate=C.value('(DisableDate)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('BillingDisabledBooksBe') as T(C)    
     
IF EXISTS(SELECT 0 FROM Tbl_BillingDisabledBooks WHERE YearId=@YearId AND MonthId=@MonthId AND BookNo=@BookNo AND IsActive=1)    
 BEGIN    
  SELECT 1 AS BookNoExistsForThisMonth FOR XML PATH('BillingDisabledBooksBe')    
 END    
ELSE    
 BEGIN    
  INSERT INTO Tbl_BillingDisabledBooks(    BookNo    
            ,YearId    
            ,MonthId    
            ,Remarks    
            ,ApproveStatusId    
            ,CreatedBy    
            ,CreatedDate    
            ,DisableTypeId    
            ,FromDate    
            ,DisableDate  
           )    
            
          VALUES(    
             @BookNo    
            ,@YearId    
            ,@MonthId    
            ,CASE @Details WHEN '' THEN NULL ELSE @Details END    
            ,@ApproveStatusId    
            ,@CreatedBy    
            ,dbo.fn_GetCurrentDateTime()    
            ,@DisableTypeId    
            ,dbo.fn_GetCurrentDateTime()  
            ,@DisableDate  
            )    
       
  SELECT 1 AS IsSuccess FOR XML PATH('BillingDisabledBooksBe')    
 END    
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments]    Script Date: 04/11/2015 21:20:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 10-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBulkCustomerPayments]          
(          
	 @XmlDoc XML           
	,@MultiXmlDoc XML          
)          
AS          
BEGIN    

DECLARE @CreatedBy VARCHAR(50)    
	,@DocumentPath VARCHAR(MAX)          
	,@DocumentName VARCHAR(MAX)         
	,@BatchNo VARCHAR(50) 
	,@StatusText VARCHAR(200) = ''
	,@IsSuccess BIT = 0
	
	SELECT            
		 @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')          
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')          
		,@DocumentName = C.value('(DocumentName)[1]','VARCHAR(MAX)')        
		,@BatchNo = C.value('(BatchNo)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)    	

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), AccountNo VARCHAR(50), ReceiptNo VARCHAR(20),           
							PaymentMode VARCHAR(20), PaymentModeId INT, 
							AmountPaid DECIMAL(18,2), RecievedDate DATETIME)
	
	INSERT INTO @TempCustomer(AccountNo, ReceiptNo, PaymentMode, PaymentModeId, AmountPaid, RecievedDate)           
	SELECT           
		 T.c.value('(AccountNo)[1]','VARCHAR(50)')           
		,T.c.value('(ReceiptNO)[1]','VARCHAR(20)')           
		,T.c.value('(PaymentMode)[1]','VARCHAR(20)')          
		,T.c.value('(PaymentModeId)[1]','VARCHAR(20)')          
		,T.c.value('(PaidAmount)[1]','DECIMAL(18,2)')          
		,T.c.value('(PaymentDate)[1]','DATETIME')          
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Table1') AS T(c)    
							
	DECLARE @SNo INT, @TotalCount INT
			,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
			,@CustomerPaymentId INT
			
	DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
	SET @SNo = 1
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer    
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SELECT @PresentAccountNo = AccountNo, @PaidAmount = AmountPaid FROM @TempCustomer WHERE SNo = @SNo
					
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 AccountNo
						,ReceiptNo
						,PaymentModeId
						,@DocumentPath
						,@DocumentName
						,RecievedDate
						,2 -- For Bulk Upload
						,AmountPaid
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempCustomer WHERE AccountNo = @PresentAccountNo 
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @StatusText = 'Customer Pending Bills'
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
					
					SET @StatusText = 'Customer Bills Paymnets'
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					SET @StatusText = 'Customer Bills'
					UPDATE CB
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @StatusText = 'Success'
					SET @IsSuccess = 1
					SET @SNo = @SNo + 1
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess =0
			END CATCH		
		END
		
	SELECT   
		 @IsSuccess As IsSuccess
		,@StatusText AS StatusText
	FOR XML PATH('PaymentsBe'),TYPE
		
END
GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/11/2015 21:53:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- Modified By -- Bhimaraju Vanka
-- Modified Date -- 27-03-2015
-- Reason : Added/Inserting one more field MeterReading From
-- Modified By -- Jeevan Amunuri
-- Modified Date -- 03-04-2015
-- Modified By -- Karteek
-- Modified Date -- 06-04-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

	IF(@IsTamper=0)
		BEGIN
			SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
		END
		
	IF(@IsRollover=1)
		BEGIN
			SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
			Declare @FirstReadingUsage bigint = convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
			Declare @SecountReadingUsage Bigint =case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
			SET @Usage=@FirstReadingUsage
			SET @SecountReadingUsage =@SecountReadingUsage-@FirstReadingUsage
			IF @SecountReadingUsage != 0
				BEGIN
				   	INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom])
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,CONVERT(VARCHAR(50),@Previous)
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom)
				END
 
		END
		
	SELECT @AverageReading = dbo.fn_GetAverageReading(@AccNum,@Usage)

	INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom])
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,CONVERT(VARCHAR(50),@Current)
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom)

	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END