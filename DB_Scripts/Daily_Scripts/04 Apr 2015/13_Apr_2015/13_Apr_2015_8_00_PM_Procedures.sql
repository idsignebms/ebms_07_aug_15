
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomerLedgers_Rajaiah]    Script Date: 04/13/2015 18:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 25-09-2014  
-- Modified By: T.Karthik
-- Modified date: 28-10-2014
-- Description: The purpose of this procedure is to Get the Customers Ledger Report
-- =============================================  
CREATE PROCEDURE [dbo].[USP_RptGetCustomerLedgers_Rajaiah]
(  
@XmlDoc XML  
)  
AS  
BEGIN  
	
	Declare @AcountNo VARCHAR(50)
		,@OldAcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@ReportMonths INT
		,@CurrentDate DATETIME 
		,@BUID VARCHAR(50)=''

	SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()	 

	SELECT  
		  @AcountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@OldAcountNo = C.value('(OldAccountNo)[1]','VARCHAR(50)')   
		  ,@ReportMonths = C.value('(ReportMonths)[1]','INT') 
		  ,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C)  
	
	SELECT @AcountNo = GlobalAccountNumber FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND (BU_ID=@BUID OR @BUID='') AND ActiveStatusId=1

   IF @AcountNo IS NOT NULL
   BEGIN
			DECLARE @ResultTable TABLE(Id INT IDENTITY(1,1),CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))
			DECLARE @FinalResult TABLE(Id INT,CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))

			DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
		    
			INSERT INTO @ResultedMonths
			SELECT [YEAR],[MONTH] FROM DBO.fn_GetCurrentYearMonths_ByCurrentDate(@CurrentDate)-- ORDER BY Id DESC
									
			INSERT INTO @ResultTable (CreateDate,Particulars,Debit,Credit,Balance,Remarks)
			SELECT OpeningDate,'Opening Balance',TotalPendingAmount,0,0,''
			FROM dbo.fn_GetCustomerYear_OpeningBalance(@AcountNo)	
			

			SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
			---- Collection all the transaction Start
			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
			  BEGIN      
				SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
				
				SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
				SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

				INSERT INTO @ResultTable (CreateDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks)
				SELECT CreatedDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks FROM 
				(SELECT CONVERT(DATE,CreatedDate) AS CreatedDate
						,BillNo AS ReferenceNo
						,'Sale of energy' As Particulars
						,0 AS Debit
						,TotalBillAmountWithTax As Credit
						,0 AS Balance
						,'' As Remarks
					FROM Tbl_CustomerBills 
					WHERE AccountNo = @AcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
					--AND BillMonth = @PresentMonth AND BillYear = @PresentYear
				UNION
					SELECT 
						CONVERT(DATE,CreatedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),BillAdjustmentId) AS ReferenceNo
						,'Adjustment' As Particulars
						,0 AS Debit
						,AmountEffected As Credit,0 AS Balance,Remarks
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @AcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				UNION
					SELECT CONVERT(DATE,RecievedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),CustomerPaymentID) AS ReferenceNo
						,(SELECT BillingType FROM Tbl_MBillingTypes WHERE BillingTypeId = CP.ReceivedDevice)+' Payments' As Particulars
						,PaidAmount AS Debit
						,0 As Credit
						,0 AS Balance
						,Remarks
					FROM Tbl_CustomerPayments CP
					WHERE AccountNo = @AcountNo
					AND CONVERT(DATE,RecievedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				) AS A ORDER By CONVERT(DATE,A.CreatedDate)	
				  
				  IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
							IF(@MonthId IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Collection all the transaction END

			---- Finalise the transaction Records Start

			DECLARE @PresentTrans INT 
			SELECT TOP(1) @PresentTrans = Id FROM @ResultTable ORDER BY Id ASC    

			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultTable WHERE Id >= @PresentTrans ORDER BY Id ASC))    
			  BEGIN 
					
					IF NOT EXISTS(SELECT 0 FROM @FinalResult WHERE Id < @PresentTrans)
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 THEN ((0 - Debit)) 
								   WHEN Credit <> 0 THEN (0 + Credit)
								   ELSE 0 END)AS Balance
							,CreateDate
							,Remarks	   
						FROM @ResultTable WHERE Id = @PresentTrans
					END
				ELSE
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) - Debit))
								 WHEN Credit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) + Credit))
									END)AS Balance
							,CreateDate
							,Remarks		
						FROM @ResultTable WHERE Id = @PresentTrans
					END
					
				  IF(@PresentTrans = (SELECT TOP(1) Id FROM @ResultTable ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentTrans = (SELECT TOP(1) Id FROM @ResultTable WHERE  Id > @PresentTrans ORDER BY Id ASC)    
							IF(@PresentTrans IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Finalise the transaction Records End
		    
		   
			 SELECT   
			 (  
				 SELECT 
					 GlobalAccountNumber
					  --,KnownAs AS Name
					  ,(SELECT dbo.fn_GetCustomerFullName(GlobalAccountNumber)) AS Name
					  ,ISNULL(HomeContactNo,ISNULL(BusinessContactNo,OtherContactNo))AS MobileNo
					  ,OldAccountNo
					  ,CD.ClassName 
					  ,CD.BusinessUnitName
					  ,CD.ServiceUnitName
					  ,CD.ServiceCenterName
					  ,CD.BookNo
					  ,CD.CycleName
					  ,CD.MeterNumber
					  ,dbo.fn_GetCustomerServiceAddress(GlobalAccountNumber) AS ServiceAddress
					  ,1 AS IsSuccess
			   FROM [UDV_CustomerDescription] CD
			   WHERE GlobalAccountNumber = @AcountNo
			  FOR XML PATH('CustomerDetails'),TYPE  
			)
			,(
				SELECT 
					Particulars
					,Debit
					,Credit 
					,Balance
					,-1*Balance As Due		
					,CONVERT(VARCHAR(50),CreateDate,106) AS TransactionDate
					,ReferenceNo
					,Remarks
				FROM @FinalResult 
				--ORDER BY Id DESC
			  FOR XML PATH('CustomerLedger'),TYPE  
			 )  
			 FOR XML PATH(''),ROOT('RptCustomerLedgerInfoByXml')  
	END
    ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND BU_ID!=@BUID AND ActiveStatusId=1)
    BEGIN
		SELECT
		(
			SELECT 1 AS IsSuccess,1 AS IsCustExistsInOtherBU
		FOR XML PATH('CustomerDetails'),TYPE  
		)  
		FOR XML PATH(''),ROOT('RptCustomerLedgerInfoByXml')
    END
    ELSE
    BEGIN
		SELECT
		(
			SELECT 0 AS IsSuccess
		FOR XML PATH('CustomerDetails'),TYPE  
		)  
		FOR XML PATH(''),ROOT('RptCustomerLedgerInfoByXml')
    END
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersNoNameOrAddList]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 16-Sep-2014  
-- Description: Get The Details of The Customers With out Having No Name Or Address  
-- Modified By : Karthik  
-- Modified Date : 27-12-2014  
-- Modified By : Padmini  
-- Modified Date : 01/12/2015  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
  
 DECLARE  @PageSize INT  
   ,@PageNo INT  
   ,@BU_ID VARCHAR(MAX)  
   ,@SU_ID VARCHAR(MAX)  
   ,@SC_ID VARCHAR(MAX)    
   ,@CycleId VARCHAR(MAX)  
   ,@BookNo VARCHAR(MAX)     
     
 SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('ReportsBe') AS T(C)  
    
   
 ;WITH PagedResults AS  
 (  
  SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber  
    --,ISNULL(Name,'--') AS Name  
    ,(SELECT dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)) AS Name  
    ,CD.GlobalAccountNumber AS GlobalAccountNumber  
    ,CD.AccountNo AS AccountNo  
    ,(SELECT dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber)) As ServiceAddress  
    --,(SELECT BusinessUnitName FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS BusinessUnitName  
    ,BusinessUnitName AS BusinessUnitName  
    ,ISNULL(OldAccountNo,'--') AS OldAccountNo  
    --,ServiceHouseNo,ServiceCity,ServiceLandMark,ServiceStreet,PostalHouseNo,PostalCity,PostalLandMark,PostalStreet  
    --FROM Tbl_CustomerDetails CD  
    --WHERE ( Name IS NULL OR Name=''   
    --  OR PostalStreet IS NULL OR PostalStreet = ''   
    --  OR ServiceHouseNo IS NULL OR ServiceHouseNo = ''   
    --  OR ServiceCity IS NULL OR ServiceCity = ''   
    --  OR ServiceLandMark IS NULL OR ServiceLandMark = ''   
    --  OR ServiceStreet IS NULL OR ServiceStreet = ''   
    --  OR PostalHouseNo IS NULL OR PostalHouseNo = ''   
    --  OR PostalCity IS NULL OR PostalCity = ''   
    --  OR PostalLandMark IS NULL OR PostalLandMark = '' )  
    --AND ActiveStatusId=1  
    --AND (BU_ID=@BU_ID OR @BU_ID ='')  
    --AND (SU_ID =@SU_ID OR @SU_ID = '')  
    --AND (ServiceCenterId =@SC_ID OR @SC_ID = '')  
    --AND (BookNo =@BookNo OR @BookNo = '')  
    --AND ((SELECT dbo.fn_GetCycleIdByBook(BookNo))=@CycleId OR @CycleId = '')  
    FROM UDV_CustomerDescription CD  
    JOIN (SELECT * FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE IsServiceAddress=0) PAD ON CD.GlobalAccountNumber=PAD.GlobalAccountNumber  
    JOIN (SELECT * FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE IsServiceAddress=1) SAD ON CD.GlobalAccountNumber=SAD.GlobalAccountNumber  
    WHERE ( FirstName IS NULL OR FirstName=''  
         --OR MiddleName IS NULL OR MiddleName=''  
      OR LastName IS NULL OR LastName=''   
      OR (PAD.StreetName IS NULL OR PAD.StreetName = ''   
      OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
      OR PAD.City IS NULL OR PAD.City = '')   
      --OR PAD.Landmark IS NULL OR PAD.Landmark = '' )  
      OR (SAD.StreetName IS NULL OR SAD.StreetName = ''   
      OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
      OR SAD.City IS NULL OR SAD.City = ''))   
      --OR SAD.Landmark IS NULL OR SAD.Landmark = '' ))  
    AND ActiveStatusId=1  
    AND (BU_ID=@BU_ID OR @BU_ID ='')  
    --AND (SU_ID =@SU_ID OR @SU_ID = '')  
    AND (CD.SU_ID IN(SELECT [com] FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')  
    AND (CD.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split(@SC_ID,',')) OR @SC_ID='')  
    --AND (ServiceCenterId =@SC_ID OR @SC_ID = '')  
    AND (CD.BookNo IN (SELECT [com] FROM dbo.fn_Split(@BookNo,',')) OR @BookNo='')  
    --AND (BookNo =@BookNo OR @BookNo = '')  
    AND (CD.CycleId IN (SELECT [com] FROM dbo.fn_Split(@CycleId,','))OR @CycleId='')  
    --AND ((SELECT dbo.fn_GetCycleIdByBook(BookNo))=@CycleId OR @CycleId = '')  
 )  
   
 --SELECT   
 --   (  
   SELECT  
     *  
    ,(Select COUNT(0) from PagedResults) as TotalRecords  
    FROM PagedResults p  
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
  --  FOR XML PATH('Reports'),TYPE  
  --)  
  --FOR XML PATH(''),ROOT('ReportsBeInfoByXml')  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <26-MAR-2014>
-- Description:	<Get BillReading details from customerreading table>
-- Modified BY : Suresh Kumar Dasi
-- Reason:	<Based on ALter Table>
-- Modified By -- Padmini
-- Modified Date -- 27-12-2014
-- Modified By -- Bhimaraju Vanka
-- Modified Date -- 27-03-2015
-- Reason : Added/Inserting one more field MeterReading From
-- Modified By -- Jeevan Amunuri
-- Modified Date -- 03-04-2015
-- Modified By -- Karteek
-- Modified Date -- 06-04-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	 Declare @FirstReadingUsage bigint =1
	 Declare @SecountReadingUsage Bigint =0
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  
		END
		SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom])
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,CONVERT(VARCHAR(50),@Current)
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				   	INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom])
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,CONVERT(VARCHAR(50),@Previous)
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom)
									
					 
				END
			
 
		END
		
 
	
	
	
	 



	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersNoNameOrAddList_New]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 16-Sep-2014  
-- Description: Get The Details of The Customers With out Having No Name Or Address  
-- Modified By : Karthik  
-- Modified Date : 27-12-2014  
-- Modified By : Padmini  
-- Modified Date : 01/12/2015  
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015     
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList_New]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageSize INT  
   ,@PageNo INT  
   ,@BU_ID VARCHAR(MAX)  
   ,@SU_ID VARCHAR(MAX)  
   ,@SC_ID VARCHAR(MAX)    
   ,@CycleId VARCHAR(MAX)  
   ,@BookNo VARCHAR(MAX)     
     
 SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('ReportsBe') AS T(C)  
   
   
 IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SU_ID = '')  
  BEGIN  
   SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50))   
      FROM Tbl_ServiceUnits   
      WHERE ActiveStatusId = 1  
      AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SC_ID = '')  
  BEGIN  
   SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50))   
      FROM Tbl_ServiceCenter  
      WHERE ActiveStatusId = 1  
      AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@CycleId = '')  
  BEGIN  
   SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50))   
      FROM Tbl_Cycles  
      WHERE ActiveStatusId = 1  
      AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@BookNo = '')  
  BEGIN  
   SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50))   
      FROM Tbl_BookNumbers  
      WHERE ActiveStatusId = 1  
      AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
   
 ;WITH PagedResults AS  
 (  
  SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber  
    ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
    ,CD.GlobalAccountNumber AS GlobalAccountNumber  
    ,CD.AccountNo AS AccountNo  
    ,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode)) As ServiceAddress  
    ,ISNULL(OldAccountNo,'--') AS OldAccountNo  
    ,CD.CycleName  
    ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber  
    ,CD.SortOrder AS CustomerSortOrder  
    ,CD.BookSortOrder  
    ,CD.BusinessUnitName  
    ,CD.ServiceUnitName  
    ,CD.ServiceCenterName  
    FROM UDV_CustomerDescription CD  
    INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
      AND CD.ActiveStatusId=1 AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName='')  
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber   
      AND PAD.IsServiceAddress = 0 AND (PAD.StreetName IS NULL OR PAD.StreetName = ''   
               OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
               OR PAD.City IS NULL OR PAD.City = '')  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber   
      AND SAD.IsServiceAddress = 1 AND (SAD.StreetName IS NULL OR SAD.StreetName = ''   
               OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
               OR SAD.City IS NULL OR SAD.City = '')   
 )  
   
 --SELECT   
 --(  
  SELECT  
    RowNumber  
   ,Name  
   ,GlobalAccountNumber  
   ,AccountNo  
   ,ServiceAddress  
   ,BusinessUnitName  
   ,OldAccountNo  
   ,CycleName  
   ,BookNumber  
   ,CustomerSortOrder  
   ,BookSortOrder  
   ,BusinessUnitName  
   ,ServiceUnitName  
   ,ServiceCenterName  
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
  FROM PagedResults p  
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
 -- FOR XML PATH('Reports'),TYPE  
 --)  
 --FOR XML PATH(''),ROOT('ReportsBeInfoByXml')  
END  
-------------------------------------------------------------------------------------------------  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetConfigurationSettingsMenu]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author: Karteek  
-- Create date: 7 Apr 2015   
-- To get the configuration settings menu for left side  
-- =============================================   
ALTER PROCEDURE [dbo].[USP_GetConfigurationSettingsMenu]  
(      
 @XmlDoc XML      
)   
AS  
BEGIN       
 SET NOCOUNT ON;   
      
 DECLARE @Role_Id INT   
  ,@PageName VARCHAR(50)  
  ,@MenuId INT  
    
 SELECT @Role_Id = C.value('(Role_Id)[1]','INT')     
  ,@PageName = C.value('(PageName)[1]','varchar(50)')  
 FROM @XmlDoc.nodes('SideMenusBE') AS T(C)    
   
 SELECT @MenuId = MenuId FROM Tbl_Menus WHERE Name = @PageName AND IsActive = 1  
   
 SELECT      
 (   
  SELECT   
    M.MenuId AS Main_Menu_Text  
   ,M.Name AS SubMenu_Text  
   ,M.[Path] AS Path  
  FROM Tbl_Menus M  
  INNER JOIN Tbl_NewPagePermissions N ON N.MenuId = M.MenuId   
   AND M.IsActive = 1 AND N.IsActive = 1  
   AND N.Role_Id = @Role_Id  
   AND M.ReferenceMenuId = @MenuId  
   ORDER BY Page_Order   --Faiz-ID103
  FOR XML PATH('SideMenu'),TYPE      
 )      
 FOR XML PATH(''),ROOT('SideMenuBeInfoByXml')     
   
END     
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- ModiifiedBY: <Suresh Kumar Dasi>  
-- Description: to Solve the Date and DataType Convertions related Issues  
-- Modified By: T.Karthik
-- Modified Date: 27-12-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading]  
 (@MultiXmlDoc XML)  
AS  
BEGIN  
  
 DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),       
                             CurrentReading varchar(50),      
                             ReadDate DATETIME)     
                                
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)             
   
 INSERT INTO @TempCustomer(AccountNo ,CurrentReading,ReadDate)       
 SELECT       
	  c.value('(Account_Number)[1]','VARCHAR(50)')       
	  ,(CASE c.value('(Current_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Current_Reading)[1]','VARCHAR(50)') END)      
	  ,CONVERT(DATETIME,c.value('(Read_Date)[1]','varchar(50)'))  
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)   

DELETE FROM @CustomersCount
INSERT INTO @CustomersCount
SELECT AccountNo,COUNT(0) As Total from @TempCustomer
GROUP BY AccountNo

;With FinalResult AS
(
SELECT   
  CD.GlobalAccountNumber as AccNum  
  ,B.Name  
  ,B.MeterNo  
  ,B.MeterDials  
  ,ReadCodeId  
  ,B.Decimals
  ,B.ActiveStatusId
  ,B.BU_ID  
  ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(DATEPART(YY,T1.ReadDate),DATEPART(MM,T1.ReadDate), CD.GlobalAccountNumber)) AS IsActiveBillMonth
  ,(SELECT dbo.fn_IsBillMonthOpened(CONVERT(DATE, T1.ReadDate))) AS IsActiveMonth  
   ,(CASE WHEN (SELECT TOP(1) AverageReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate) < CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       IS NULL  
       THEN CONVERT(VARCHAR(50),ISNULL(AvgReading,0))   
       ELSE (SELECT TOP(1) AverageReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate) ORDER BY CustomerReadingId desc)  
       END) AS AverageReading  
  ,(CASE WHEN (SELECT TOP(1) TotalReadings FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
        CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       IS NULL  
       THEN '0'   
       ELSE (SELECT TOP(1) TotalReadings FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       END) AS TotalReadings  
  ,(CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       IS NULL  
       THEN CONVERT(VARCHAR(50),ISNULL(B.InitialReading,0))   
       ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate )<CONVERT(DATE, T1.ReadDate)  order by CustomerReadingId desc)  
       END)AS PreviousReading  
  ,T1.CurrentReading AS PresentReading  
  ,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
         CONVERT(DATE,ReadDate ) < CONVERT(DATE, T1.ReadDate)  ORDER BY CustomerReadingId DESC) AS BillNo
  ,CONVERT(VARCHAR(10),ISNULL((CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CONVERT(DATE,CR.ReadDate) = CONVERT(DATE,T1.ReadDate))  
      THEN 1 ELSE 0 END),0)) AS PrvExist
  ,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,T1.ReadDate))  
      THEN 1 ELSE 0 END) AS IsExists  
  ,CONVERT(VARCHAR(20),T1.ReadDate,103) AS BatchDate   
  ,CONVERT(VARCHAR(20),T1.ReadDate,106)AS ReadDate 
  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
			WHERE CR.GlobalAccountNumber = CD.GlobalAccountNumber AND   
			CONVERT(DATE,CR.ReadDate )=CONVERT(DATE, T1.ReadDate) ORDER BY CustomerReadingId DESC) AS IsBilled 
  ,(CASE WHEN EXISTS(SELECT AccountNo FROM @CustomersCount WHERE Total > 1 AND AccountNo = CD.GlobalAccountNumber) THEN 1 ELSE 0 END) AS IsDuplicate  
 FROM @TempCustomer T1 
INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON (CD.GlobalAccountNumber=T1.AccountNo OR CD.OldAccountNo=T1.AccountNo) 
	LEFT JOIN( SELECT 
				 C.GlobalAccountNumber AS AccNum  
				 ,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name  
				 ,ReadCodeId  
				 ,C.MeterNumber AS MeterNo  
				 ,M.MeterDials AS MeterDials  
				 ,M.Decimals AS Decimals  
				 ,C.InitialReading
				 ,C.AvgReading
				 ,C.ActiveStatusId
				 ,C.BU_ID
				 ,(CASE WHEN T.AvgMaxLimit IS NULL THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			 FROM UDV_CustomerDescription C   
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
			LEFT JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo   
		   ) B ON CD.GlobalAccountNumber=B.AccNum  
)

	SELECT
		(     
		SELECT 
		      AccNum  
			  ,Name  
			  ,MeterNo  
			  ,MeterDials  
			  ,ReadCodeId  
			  ,Decimals  
			  ,IsActiveBillMonth
			  ,IsActiveMonth  
			  ,AverageReading  
			  ,TotalReadings  
			  ,dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading) AS PreviousReading
			  ,PresentReading  
			  ,((CASE WHEN(PresentReading LIKE '%[^0-9]%') THEN 0 ELSE CONVERT(DECIMAL(18,0),PresentReading) END)- dbo.fn_GetCurrentReading_ByAdjustments(BillNo,PreviousReading)) AS Usage
			  ,PrvExist
			  ,IsExists  
			  ,BatchDate   
			  ,ReadDate  
			  ,IsBilled 
			  ,IsDuplicate
			  ,ActiveStatusId
			  ,BU_ID
		 FROM FinalResult     
		FOR XML PATH('BillingBE'),TYPE  
		)  
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@Count = C.value('(TotalReadings)[1]','INT')
		,@Avg = C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)


	DECLARE @ReadId INT

	SELECT TOP(1) @ReadId = CustomerReadingId FROM Tbl_CustomerReadings
	WHERE GlobalAccountNumber = @AccNum
	ORDER BY CustomerReadingId DESC
	
	UPDATE CR SET PresentReading = CONVERT(VARCHAR(50),@Current)
		,Usage = @Usage
		,ModifiedBy = @Modified
		,IsTamper = @IsTamper
		,MeterReadingFrom = @MeterReadingFrom
		,ModifiedDate = GETDATE()
		,AverageReading = (SELECT dbo.fn_GetAverageReading_Update(GlobalAccountNumber,@Usage))
	FROM Tbl_CustomerReadings CR	
	WHERE  CustomerReadingId = @ReadId 

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')

END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	
		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)


		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')


		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and  
		(CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
		@IsExists = (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) 
		,@LatestDate=ReadDate 
		,@AverageReading=AverageReading 
		,@TotalReadings=TotalReadings  
		,@PreviousReading = PreviousReading
		,@PresentReading=PresentReading 
		,@IsBilled=IsBilled
		,@ReadingsMeterNumber=MeterNumber 
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC


		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  


		SELECT TOP(1) @Month = BillMonth,
		@Year = BillYear ,@LastBillReadType= 
		(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,@IsExists as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,@IsBilled as	 IsBilled
				 ,@IsLatestBill as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,@InititalReading as	 InititalReading
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBatchDetails]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-03-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertBatchDetails](@XmlDoc XML)      
AS            
BEGIN            
	DECLARE  @BatchNo int,            
		 @BatchName varchar(50) ,            
		 @BatchDate datetime ,            
		 @CashOffice INT ,            
		 @CashierID VARCHAR(50) ,            
		 @CreatedBy varchar(50),            
		 @BatchTotal NUMERIC(20,4),            
		 @Description VARCHAR(MAX), 
		 @BU_ID VARCHAR(50),       
		 @Flag INT            
             
	SELECT             
		 @BatchNo=C.value('(BatchNo)[1]','int')            
		,@BatchName=C.value('(BatchName)[1]','varchar(50)')            
		,@BatchDate=C.value('(BatchDate)[1]','DATETIME')            
		,@CashOffice = C.value('(OfficeId)[1]','INT')            
		,@CashierID = C.value('(CashierId)[1]','VARCHAR(50)')             
		,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')            
		,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')            
		,@Description=C.value('(Description)[1]','VARCHAR(MAX)')            
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')            
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)         
 --TEMP INSERTING FOR APPROVAL           
	IF(@Flag=1)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo AND BatchStatus = 1))            
				BEGIN            
					INSERT INTO Tbl_BatchDetailsApproval(            
					BatchNo,            
					BatchName,            
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,            
					[Description]
					)            
					VALUES(            
					@BatchNo,            
					Case  @BatchName when '' then null else @BatchName end,  
					@BatchDate,  
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					Case  @CreatedBy when '' then null else @CreatedBy end,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					CASE @Description WHEN '' THEN NULL ELSE @Description END
					)          
					SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')            
				END            
			ELSE
				BEGIN            
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate,BatchName  
						,BatchNo  
						,BatchTotal  
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0))   
					FROM Tbl_CustomerPaymentsApproval            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetailsApproval   
					WHERE BatchNo=@BatchNo   
					FOR XML PATH('BillingBE') 
				END 
		END      
	IF(@Flag=2)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails 
								WHERE BatchNo=@BatchNo 
								--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
								AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
								AND BU_ID=@BU_ID))            
				BEGIN            
					INSERT INTO Tbl_BatchDetails(            
					BatchNo,             
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,  
					BU_ID,   
					[Description]  
					)            
					VALUES(            
					@BatchNo,     
					@BatchDate,            
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					@CreatedBy,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					@BU_ID,
					CASE @Description WHEN '' THEN NULL ELSE @Description END       
					)            
					SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')            
				END            
			ELSE 
				BEGIN           
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate  
						,BatchName  
						,BatchNo  
						,BatchTotal  
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetails   
					WHERE BatchNo=@BatchNo 
					--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
					AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
					AND BU_ID=@BU_ID
					FOR XML PATH('BillingBE') 
				END 
		END      
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <25-MAR-2014>          
-- Description: Customer Payment from file upload   
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <24-MAR-2015>          
-- Description: Romove join to check customer bill in process
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]          
(  
	 @XmlDoc XML  
	,@MultiXmlDoc XML  
)          
AS          
BEGIN          
          
	DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)     
		,ReceivedDate DATETIME,PaymentModeId VARCHAR(50))   
         
	DECLARE @Bu_Id VARCHAR(50)  
   
	SELECT   
		@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('PaymentsBe') as T(C)      
         
	INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)      
	SELECT             
		c.value('(SNO)[1]','INT')                         
		,c.value('(AccountNo)[1]','VARCHAR(50)')  
		,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                         
		,c.value('(ReceiptNO)[1]','VARCHAR(50)')                         
		,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)    
		,c.value('(PaymentMode)[1]','VARCHAR(50)')    
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)    
	    
	SELECT    
	(    
		SELECT     
			 ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
			,CD.GlobalAccountNumber  AS AccountNo  
			, CD.OldAccountNo AS OldAccountNo  
			,TempDetails.AmountPaid AS PaidAmount    
			,TempDetails.ReceiptNO    
			,TempDetails.ReceivedDate   
			,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
			,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid, TempDetails.ReceiptNO) AS IsDuplicate  
			,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists    
			,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId)   
			ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentMode=PM.PaymentMode) END) AS PaymentMode    
			,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId 
		FROM @TempCustomer AS TempDetails     
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo  
		LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentMode    
		FOR XML PATH('PaymentsList'),TYPE      
	)    
	FOR XML PATH(''),ROOT('PaymentsInfoByXml')  
	      
END   
------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_IsBatchNoExists]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author  : NEERAJ KANOJIYA  
-- Create date : 1-05-2014  
-- Description : To check Batch Number Exsitence in main and log table  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_IsBatchNoExists]  
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
   
	DECLARE @BatchNo int     
		,@BatchDate DATETIME
		,@BU_ID VARCHAR(50)
		,@IsExists BIT = 0  
          
	SELECT @BatchNo = C.value('(BatchNo)[1]','VARCHAR(50)')   
		,@BatchDate = C.value('(BatchDate)[1]','DATETIME')   
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  
   
	IF(EXISTS(SELECT BatchNo FROM Tbl_BatchDetails   
			WHERE BatchNo = @BatchNo   
			--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120)   
			AND CONVERT(DATE, BatchDate) =  CONVERT(DATE,@BatchDate)  
			AND BU_ID = @BU_ID)) 
		BEGIN  
			SET @IsExists = 1  
		END  
   
	SELECT @IsExists AS IsExists FOR XML PATH('BillingBE')  
   
END  
  
--------------------------------------------------------------------------------------------------  
GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBillingDirectCustomer]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-Mar-2015>
-- Description:	<To get the report of PreBilling Direct Customers>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBillingDirectCustomer]
(
@XmlDoc XML
)
AS
BEGIN    

Declare @ReadCode INT    
			,@BU_ID VARCHAR(20)
			,@SU_ID VARCHAR(20)
			,@ServiceCenterId VARCHAR(20)
			
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(20)')
		,@ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(20)')
FROM	@XmlDoc.nodes('RptPreBillingBe') as T(C)     


   ;WITH Results AS    
   (    
   SELECT     
    CD.GlobalAccountNumber    
    ,MIN(CR.PreviousReading) AS PreviousReading    
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name     
    ,CD.ClassName    
    ,OldAccountNo    
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
   FROM UDV_CustomerDescription CD    
   LEFT JOIN Tbl_CustomerReadings CR ON (CD.GlobalAccountNumber != CR.GlobalAccountNumber AND IsBilled=0)    
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId     
   AND CD.BU_ID = @BU_Id-----  
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
   GROUP BY CD.GlobalAccountNumber    
     ,CD.Title    
     ,CD.FirstName    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.ClassName    
     ,OldAccountNo    
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
   UNION     
   SELECT     
    CD.GlobalAccountNumber    
    ,MIN(CR.PreviousReading) AS PreviousReading    
    ,(SELECT dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name     
    ,CD.ClassName    
    ,OldAccountNo    
    ,dbo.fn_GetCustomerServiceAddress_New(Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode) AS ServiceAddress     
    --,dbo.fn_GetCustomerServiceAddress(CD.GlobalAccountNumber) AS ServiceAddress    
   FROM UDV_CustomerDescription CD    
   LEFT JOIN Tbl_CustomerReadings CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber    
   LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CD.TariffId    
   AND CD.BU_ID = @BU_Id-----  
    AND CR.IsBilled=0    
    LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails Adress On  
    Adress.GlobalAccountNumber=CD.GlobalAccountNumber AND   
    CD.ServiceAddressID=Adress.AddressID and Adress.IsServiceAddress = 0  
   WHERE CustomerTypeId = 1    
   GROUP BY CD.GlobalAccountNumber    
     ,CD.Title    
     ,CD.FirstName    
     ,CD.MiddleName    
     ,CD.LastName    
     ,CD.ClassName    
     ,OldAccountNo    
     ,Adress.HouseNo,Adress.StreetName,Adress.Landmark,Adress.City,Adress.Details,Adress.ZipCode  
     ,Adress.AddressID   
   )    
       
   SELECT    
   (    
   SELECT * FROM Results    
    FOR XML PATH('PreBillingList'),TYPE    
   )       
   FOR XML PATH(''),ROOT('RptPreBillingInfoByXml')    
  END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD
	WHERE  isnull(CD.DeafaultUsage,0)=0  
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))

END
----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetReadCustomers]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

Modified Date : 09-04-2015

Description : 
In Active,Closed,Hold,Tempary CLose  Customers - No Need to Come
 Hold -- 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
			,@ActiveStatusIds varchar(max)='2,3,4'
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS INT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	AND ActiveStatusId=1  and BoolDisableTypeId= 1 -- Disable books and	   Active Customers
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy


	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	
	DROP TABLE #ReadCustomersList

END
----------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetPartialBillCustomers]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
					 
	SELECT	    
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt CD
	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	
	UNION ALL
	
	SELECT 
		 CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 AND BD.IsPartialBill = 1
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	
END
---------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNonReadCustomers]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''
				,@ActiveStatusIds varchar(max)='2,3,4'

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,[usage] AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE CD.ReadCodeID = 2 AND CR.CustomerReadingId IS NULL 
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) 
		AND ActiveStatusId=1  and BoolDisableTypeId= 1 -- Disable books and	   Active Customers

END
--------------------------------------------------------------------------------------------




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNoBillsCustomers]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- PrepaidCustomers
	SET	@BookDisableType = '1'

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
	SELECT   
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress AS ServiceAddress
		,CASE WHEN ISNULL(CustomerStatus.CustomerStatusID,0)=0 THEN 'HOLD / Closed Customers' ELSE 'Pre- Paid Meters' END AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	LEFT JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
	LEFT JOIN Tbl_MeterInformation MI ON CD.MeterNumber = MI.MeterNo and MI.MeterType = 1
	WHERE CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	AND (CustomerStatus.CustomerStatusID IS NUll OR  MI.MeterType IS NULL)
	
	UNION ALL 
	
	SELECT  
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress  AS ServiceAddress
		,'BookDisable No Power' AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM UDV_PrebillingRpt CD
	--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,',')) BU ON BU.BU_ID = CD.BU_ID
	--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,',')) SU ON SU.SU_Id = CD.SU_ID
	--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) SC ON SC.SC_ID = CD.ServiceCenterId
	INNER JOIN Tbl_BillingDisabledBooks BD ON ISNULL(CD.BookNo,0) = BD.BookNo
			AND BD.DisableTypeId IN(SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,','))
			AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
			AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
			AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	--INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId     

END
--------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 04/13/2015 20:03:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,Cr.Usage  AS Usage
		,CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
			AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
			AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
			AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,CAST(SUM(usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CustomerSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
	 HAVING 
	 
	 
	 CAST(SUM(usage) AS DECIMAL(18,2)) 
	 
	 between   
	 AverageReading- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	  AverageReading +((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 
	 
		 
	DROP TABLE #ReadCustomersList
	
END
----------------------------------------------------------------------------------------------------
 



GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 04/13/2015 20:04:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookNo + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
	FROM
	UDV_PrebillingRpt CD
	LEFT JOIN Tbl_DirectCustomersAvgReadings DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	WHERE isnull(DAVG.AvgReadingId,0)=0 
	and  CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) 
	and  CD.ReadCodeID = 1 
	AND CD.ActiveStatusId=1  and CD.BoolDisableTypeId= 1 -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------



GO


