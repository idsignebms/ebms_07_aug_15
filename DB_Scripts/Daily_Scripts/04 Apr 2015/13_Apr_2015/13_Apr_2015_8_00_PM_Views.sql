
GO

/****** Object:  View [dbo].[UDV_ReadCustomerDetailsWithMeterDetails]    Script Date: 04/13/2015 18:20:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




  
  
CREATE VIEW  [dbo].[UDV_ReadCustomerDetailsWithMeterDetails]   
AS
  
 SELECT  
   CD.GlobalAccountNumber   
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,CD.KnownAs  
  ,CD.ActiveStatusId  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName  
  ,PD.ReadCodeID  
  ,PD.SortOrder  
  ,AD.Highestconsumption  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BN.CycleId  
  ,BN.CycleCode  
  ,BN.CycleName  
  ,BN.SCCode  
  ,BN.ServiceCenterId  
  ,BN.ServiceCenterName  
  ,BN.SUCode  
  ,BN.ServiceUnitName  
  ,BN.SU_ID  
  ,BN.BUCode  
  ,BN.BU_ID  
  ,BN.BusinessUnitName  
  ,MS.[StatusName] AS ActiveStatus  
  ,CD.EmailId  
  ,PD.MeterNumber   
  ,MI.MeterType AS MeterTypeId  
  ,PD.PhaseId  
  ,AD.InitialReading   
  ,AD.PresentReading   
  ,AD.AvgReading  
  ,PD.RouteSequenceNumber AS RouteSequenceNo  
  ,PD.CustomerTypeId  
  ,CD.CreatedDate  
  ,CD.CreatedBy  
  ,CD.ModifiedDate  
  ,CD.ModifedBy   
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
and Pd.ReadCodeID=2
ANd PD.ActiveStatusId=1 -- ReadCustomers
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber	
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo   
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
Inner JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo  and MI.MeterDials >0





GO


