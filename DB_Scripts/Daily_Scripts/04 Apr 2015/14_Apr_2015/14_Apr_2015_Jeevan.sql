GO
/****** Object:  StoredProcedure [dbo].[USP_GetBatchAdjustmentStatus]    Script Date: 04/14/2015 11:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-DEC-2014>
-- Description:	<Retriving Batch Adjustment Status (Open/Close)>
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchAdjustmentStatus]
AS
	BEGIN
		SELECT   ROW_NUMBER() OVER(ORDER BY BA.BatchDate) AS RowNumber 
				,BA.BatchID
				,BA.BatchNo
				,ISNULL(CONVERT(VARCHAR(50),BA.BatchDate,106),'--') AS BatchDate
				,BA.BatchTotal
				,ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0) AS PaidAmount
				,BU_ID AS BUID
		FROM	Tbl_BATCH_ADJUSTMENT AS BA
		WHERE	BatchTotal>(ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0))
		ORDER BY BA.BatchDate
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 04/14/2015 12:25:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
    SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo
	ORDER BY CustomerPaymentID DESC
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		,ReadType
		,Usage AS Consumption
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 
		,CD.GlobalAccountNumber AS AccountNo
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
											,CD.Service_Landmark    
											,CD.Service_City,''    
											,CD.Service_ZipCode) AS ServiceAddress
		,CAD.OutStandingAmount AS TotalDueAmount
		,Dials AS MeterDials
		,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal  
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,COUNT(0) OVER() AS TotalRecords
	FROM Tbl_CustomerBills CB
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber
			AND CD.GlobalAccountNumber = @AccountNo	AND ISNULL(CB.PaymentStatusID,2) = 2
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo 
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     