
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_Bookwise]    Script Date: 04/15/2015 14:36:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBillPreviousReading_Bookwise] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
	dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
	,MeterNumber as CustomerExistingMeterNumber
	,MeterDials As CustomerExistingMeterDials
	,Decimals as Decimals
	,InitialReading as	InitialReading
	,AccountNo
	,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeId = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
----------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			CustomerBills.BillGeneratedDate as EstimatedBillDate,
			0 AS IsActiveMonth,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExist,
			ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--') as LatestDate,
			CustomerList.Name,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			CustomerReadings.IsTamper,
			0 as Decimals,
			'' AS BillNo,
			ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading,
			CustomerReadings.TotalReadings,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then NULL else CustomerReadings.PresentReading end) 
				else NULL End as  PresentReading,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end   as PrvExist,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end AS Usage,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading else CustomerReadings.PreviousReading end)  	
				else isnull(CustomerList.InitialReading,0) End as PreviousReading,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then CONVERT(VARCHAR(10),CustomerBills.ReadType) else '--' end)  LastBillReadType
			,CustomerList.BookNo,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	 
			LEFT JOIN	  
			#CustomerLatestBills as CustomerBills 
			ON CustomerBills.GlobalAccountNumber=CustomerList.GlobalAccountNumber
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table  #CustomersListBook   
DROP Table  #CustomerLatestBills
DROP Table  #CusotmersWithMaxBillID
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDirectCustomerAverageUpload]    Script Date: 04/15/2015 14:36:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @GlobalAccountNumber VARCHAR(50)
			,@AverageReading DECIMAL(18,2)
			,@ModifiedBy VARCHAR(50)

	--SET	@AccountNo= '0000057173'
	--SET @BUID='BEDC_BU_0004'   --BUnit_AP
	
	SELECT  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
			,@AverageReading = C.value('(AverageReading)[1]','DECIMAL(18,2)') 
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	IF EXISTS (SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
	BEGIN
		--Print 'Avg reading for customer exiting- update the avg reading'
		Update Tbl_DirectCustomersAvgReadings
		SET AverageReading=@AverageReading
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@GlobalAccountNumber
		
		
		SELECT 1 AS IsSuccess
		FOR XML PATH('DirectCustomerAverageUploadBE')
	END
	ELSE
	BEGIN
		--Print 'Avg reading for customer Not exiting- Insert the avg reading'
		Insert Into Tbl_DirectCustomersAvgReadings(
					GlobalAccountNumber
					,AverageReading
					,ActiveStatusId
					,CreatedBy
					,CreatedDate
					)
		Values		(@GlobalAccountNumber
					,@AverageReading
					,1
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					)
		
		SELECT 1 AS IsSuccess
		FOR XML PATH('DirectCustomerAverageUploadBE')
	END
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDirectCustomerAverageUpload]    Script Date: 04/15/2015 14:36:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)=''
			,@BUID VARCHAR(50)=''
			,@ReadCodeID INT
			,@GlobalAccountNumber VARCHAR(50)

	--SET	@AccountNo= '0000057173'
	--SET @BUID='BEDC_BU_0004'   --BUnit_AP
	
		SELECT  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
				,@BUID = C.value('(BUID)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	
	
	--Modified By Satya Sir 
		
	Select @CustomerBusinessUnitID=BU_ID
			,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
			,@ReadCodeID=ReadCodeID
			,@GlobalAccountNumber=GlobalAccountNumber
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	PRINT	 @ReadCodeID
	PRINT	 @GlobalAccountNumber
	
	IF @CustomerBusinessUnitID IS NULL-- Is Customer Exists 
		BEGIN
			--Print 'Customer Not Exist'
			SELECT 1 AS IsAccountNoNotExists
			FOR XML PATH('DirectCustomerAverageUploadBE')
		END
	ELSE -- If Custoemr exists
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID ='' -- Is custoemr belongs to same BU
				BEGIN
					IF @ReadCodeID=1 -- Check Is Customer Active/InActive
					BEGIN
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 
							BEGIN
								 --print 'Sucess and Active/InActive Customer' 
								 
								 SELECT 1 AS IsSuccess 
										,@GlobalAccountNumber AS GlobalAccountNumber
										,CD.AccountNo As AccountNo  
										,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name  
										--,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress  
										,(dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)) AS ServiceAddress       
										--,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo  
										,(CASE WHEN (ISNULL(CD.OldAccountNo,'')='') then '--' else  CD.OldAccountNo end) AS OldAccountNo
										,CD.ClassName as Tariff
								FROM UDV_CustomerDescription CD  
								WHERE (CD.GlobalAccountNumber = @GlobalAccountNumber)  
								AND ActiveStatusId IN (1,2)    
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
						ELSE --IF customer Hold/Closed
							BEGIN
								--print 'Failure In Active Status Hold/Closed Customer'
								SELECT 1 AS IsHoldOrClosed
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
					END
					ELSE -- If Read Customer
					BEGIN
						--Print 'Read Customer'
						SELECT 1 AS IsReadCustomer
						FOR XML PATH('DirectCustomerAverageUploadBE')
					END		
				END
				ELSE-- IF Customer belongs to other BU
				BEGIN
					--Print 'Not Belogns to the Same BU' 
					SELECT 1 AS IsCustExistsInOtherBU
					FOR XML PATH('DirectCustomerAverageUploadBE')
				END
		END
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertDirectCustomerBillReading]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V  
-- Create date: 26-Feb-2015  
-- Description: AverageReading details are insertion if exists updation for those customers  
-- Modified By: Faiz-ID103
-- Modified Date: 15-Apr-2015
-- Description: Modified the SP In insert condition as the privious condition was wrong and resulted to duplication of records
-- =============================================  
ALTER PROCEDURE [dbo].[USP_InsertDirectCustomerBillReading]  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN  
  
  DECLARE @TempCustomer TABLE(AccNum VARCHAR(50),       
                              AverageReading VARCHAR(50),     
                             CreatedBy VARCHAR(50))  
  INSERT INTO @TempCustomer(AccNum ,AverageReading,CreatedBy)       
  SELECT       
    c.value('(AccNum)[1]','VARCHAR(50)')       
   ,c.value('(AverageReading)[1]','VARCHAR(50)')       
   ,c.value('(CreatedBy)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('//NewDataSet/Table1') AS T(c)  
    
    
  UPDATE DC   
  SET DC.AverageReading=Tc.AverageReading  
   ,DC.ModifiedBy=TC.CreatedBy  
   ,DC.ModifiedDate=dbo.fn_GetCurrentDateTime()  
  FROM Tbl_DirectCustomersAvgReadings DC  
  INNER JOIN @TempCustomer TC ON TC.AccNum = DC.GlobalAccountNumber  
  WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM @TempCustomer)  
    
    
  INSERT INTO Tbl_DirectCustomersAvgReadings  
     (  
       GlobalAccountNumber  
      ,AverageReading  
      ,CreatedBy  
      ,CreatedDate  
     )  
   SELECT   TC.AccNum  
      ,TC.AverageReading  
      ,TC.CreatedBy  
      ,dbo.fn_GetCurrentDateTime()  
      FROM Tbl_DirectCustomersAvgReadings DC --Faiz-ID103
	  RIGHT JOIN @TempCustomer TC on TC.AccNum=DC.GlobalAccountNumber 
	  WHERE GlobalAccountNumber IS NULL
  ----============== Privious code commented START (Faiz-ID103)================
  -- FROM @TempCustomer AS TC  
  --WHERE TC.AccNum NOT IN (SELECT DISTINCT GlobalAccountNumber   
  --  FROM Tbl_DirectCustomersAvgReadings DC  
  --  INNER JOIN @TempCustomer TC1 ON TC1.AccNum <> DC.GlobalAccountNumber  
  -- )
  ----============== Privious code commented END===============================
 SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')  
  
    
 --DECLARE @MinReading varchar(50)  
 --   ,@CreateBy varchar(50)    
 --   ,@AccNum varchar(50)  
     
 -- SELECT    @MinReading=C.value('(AverageReading)[1]','VARCHAR(50)')  
 --    ,@CreateBy=C.value('(CreatedBy)[1]','varchar(50)')    
 --    ,@AccNum=C.value('(AccNum)[1]','varchar(50)')    
 -- FROM @XmlDoc.nodes('BillingBE') AS T(C)  
    
    
 -- IF EXISTS(SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@AccNum)  
 -- BEGIN  
 --  UPDATE Tbl_DirectCustomersAvgReadings  
 --  SET AverageReading=@MinReading  
 --   ,ModifiedBy=@CreateBy  
 --   ,ModifiedDate=dbo.fn_GetCurrentDateTime()  
 --  WHERE GlobalAccountNumber=@AccNum  
 --  SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')     
 -- END  
 --ELSE  
 -- BEGIN  
 --  INSERT INTO Tbl_DirectCustomersAvgReadings  
 --    (  
 --      GlobalAccountNumber  
 --     ,AverageReading  
 --     ,CreatedBy  
 --     ,CreatedDate  
 --    )  
 --  VALUES  
 --    (  
 --     @AccNum  
 --     ,@MinReading  
 --     ,@CreateBy  
 --     ,dbo.fn_GetCurrentDateTime()  
 --    )  
 --  SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')  
 -- END          
END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_Bookwise]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_Bookwise] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
	dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
	,MeterNumber as CustomerExistingMeterNumber
	,MeterDials As CustomerExistingMeterDials
	,Decimals as Decimals
	,InitialReading as	InitialReading
	,AccountNo
	,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeId = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
----------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			CustomerBills.BillGeneratedDate as EstimatedBillDate,
			0 AS IsActiveMonth,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExist,
			ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--') as LatestDate,
			CustomerList.Name,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			CustomerReadings.IsTamper,
			0 as Decimals,
			'' AS BillNo,
			ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading,
			CustomerReadings.TotalReadings,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then NULL else CustomerReadings.PresentReading end) 
				else NULL End as  PresentReading,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end   as PrvExist,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end AS Usage,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading else CustomerReadings.PreviousReading end)  	
				else isnull(CustomerList.InitialReading,0) End as PreviousReading,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then CONVERT(VARCHAR(10),CustomerBills.ReadType) else '--' end)  LastBillReadType
			,CustomerList.BookNo,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	 
			LEFT JOIN	  
			#CustomerLatestBills as CustomerBills 
			ON CustomerBills.GlobalAccountNumber=CustomerList.GlobalAccountNumber
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table  #CustomersListBook   
DROP Table  #CustomerLatestBills
DROP Table  #CusotmersWithMaxBillID
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT
		,@Rollover BIT
		,@Previous varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@Count = C.value('(TotalReadings)[1]','INT')
		,@Avg = C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@Rollover = C.value('(Rollover)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@Previous = C.value('(PreviousReading)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @ReadId INT
	Declare	 @Dials INT
	Declare	  @Multipler INT
	Declare @IsExistingRollOver bit
	Declare	   @MeterNumber varchar(50),@FirstPrevReading varchar(50),@FirstPresentValue varchar(50) 
	 ,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
	 
		,@SecoundReadingPrevReading varchar(20)
		,@AverageReading VARCHAR(50)
		 
	SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=IsRollOver,@MeterNumber=MeterNumber
	,@Dials	= LEN(PreviousReading) ,@ReadDate=ReadDate 
	FROM Tbl_CustomerReadings
	WHERE GlobalAccountNumber = @AccNum
	ORDER BY CustomerReadingId DESC

		 
		 
	IF @IsExistingRollOver = 1
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
	END
	ELSE
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId    
	END
	 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	Declare @FirstReadingUsage bigint =@Usage
	Declare @SecountReadingUsage Bigint =0
	SET @FirstPrevReading   = @Previous
	SET @FirstPresentValue    =	 @Current
	
	IF @Rollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		 
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@Rollover
		)
	IF(@Rollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage+1
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@Rollover)
									
					 
				END
			
 
		END
	
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum
	

	 

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
	   
END



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
		SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@IsRollover)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@IsRollover)
									
					 
				END
			
 
		END
		
 
	
	
	
	 



	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDirectCustomerAverageUpload]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)=''
			,@BUID VARCHAR(50)=''
			,@ReadCodeID INT
			,@GlobalAccountNumber VARCHAR(50)

	--SET	@AccountNo= '0000057173'
	--SET @BUID='BEDC_BU_0004'   --BUnit_AP
	
		SELECT  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
				,@BUID = C.value('(BUID)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	
	
	--Modified By Satya Sir 
		
	Select @CustomerBusinessUnitID=BU_ID
			,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
			,@ReadCodeID=ReadCodeID
			,@GlobalAccountNumber=GlobalAccountNumber
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	PRINT	 @ReadCodeID
	PRINT	 @GlobalAccountNumber
	
	IF @CustomerBusinessUnitID IS NULL-- Is Customer Exists 
		BEGIN
			--Print 'Customer Not Exist'
			SELECT 1 AS IsAccountNoNotExists
			FOR XML PATH('DirectCustomerAverageUploadBE')
		END
	ELSE -- If Custoemr exists
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID ='' -- Is custoemr belongs to same BU
				BEGIN
					IF @ReadCodeID=1 -- Check Is Customer Active/InActive
					BEGIN
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 
							BEGIN
								 --print 'Sucess and Active/InActive Customer' 
								 
								 SELECT 1 AS IsSuccess 
										,@GlobalAccountNumber AS GlobalAccountNumber
										,CD.AccountNo As AccountNo  
										,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name  
										--,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress  
										,(dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)) AS ServiceAddress       
										--,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo  
										,(CASE WHEN (ISNULL(CD.OldAccountNo,'')='') then '--' else  CD.OldAccountNo end) AS OldAccountNo
										,CD.ClassName as Tariff
								FROM UDV_CustomerDescription CD  
								WHERE (CD.GlobalAccountNumber = @GlobalAccountNumber)  
								AND ActiveStatusId IN (1,2)    
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
						ELSE --IF customer Hold/Closed
							BEGIN
								--print 'Failure In Active Status Hold/Closed Customer'
								SELECT 1 AS IsHoldOrClosed
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
					END
					ELSE -- If Read Customer
					BEGIN
						--Print 'Read Customer'
						SELECT 1 AS IsReadCustomer
						FOR XML PATH('DirectCustomerAverageUploadBE')
					END		
				END
				ELSE-- IF Customer belongs to other BU
				BEGIN
					--Print 'Not Belogns to the Same BU' 
					SELECT 1 AS IsCustExistsInOtherBU
					FOR XML PATH('DirectCustomerAverageUploadBE')
				END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)

		  Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
		 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExist 
		,ReadDate   as  LatestDate 
		,AverageReading 
		,TotalReadings  
		,PreviousReading
		,PresentReading 
		,IsBilled
		,MeterNumber   as  ReadingsMeterNumber
		,IsRollOver
		,GlobalAccountNumber
		,CustomerReadingId
		INTO #CustomerTopTwoReading
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC
		IF EXISTS (select 0 from #CustomerTopTwoReading where IsRollOver=1) 
			BEGIN

				SELECT  
					@usage=Sum(Usage),
					@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
					@IsExists = max(case when IsExist=0 then 0 else 1 end)
					,@LatestDate=max(LatestDate) 
					,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
					,@TotalReadings=max(TotalReadings)  
					,@PreviousReading = max(PreviousReading)
					,@PresentReading=min(PresentReading) 
					,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
					,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
					,@IsRollOver =1 
				FROM #CustomerTopTwoReading  AS TopTwoReadings
				Group By GlobalAccountNumber

			END
		ELSE
			BEGIN
					 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
					@IsExists = IsExist
					,@LatestDate=LatestDate 
					,@AverageReading=AverageReading 
					,@TotalReadings=TotalReadings  
					,@PreviousReading = PreviousReading
					,@PresentReading=PresentReading 
					,@IsBilled=IsBilled
					,@ReadingsMeterNumber=ReadingsMeterNumber 
					,@IsRollOver =0
					from #CustomerTopTwoReading
			END
		
		DROP table		#CustomerTopTwoReading

		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		SELECT TOP(1) @Month = BillMonth,
		@Year = BillYear ,@LastBillReadType= 
		(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,@IsExists as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,(Case when CONVERT(DATE,@ReadDate) >= CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
				 ,(Case when CONVERT(DATE,@ReadDate) >= CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,@InititalReading as	 InititalReading
				 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDirectCustomerAverageUpload]    Script Date: 04/15/2015 14:37:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @GlobalAccountNumber VARCHAR(50)
			,@AverageReading DECIMAL(18,2)
			,@ModifiedBy VARCHAR(50)

	--SET	@AccountNo= '0000057173'
	--SET @BUID='BEDC_BU_0004'   --BUnit_AP
	
	SELECT  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
			,@AverageReading = C.value('(AverageReading)[1]','DECIMAL(18,2)') 
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	IF EXISTS (SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
	BEGIN
		--Print 'Avg reading for customer exiting- update the avg reading'
		Update Tbl_DirectCustomersAvgReadings
		SET AverageReading=@AverageReading
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = dbo.fn_GetCurrentDateTime()
		WHERE GlobalAccountNumber=@GlobalAccountNumber
		
		
		SELECT 1 AS IsSuccess
		FOR XML PATH('DirectCustomerAverageUploadBE')
	END
	ELSE
	BEGIN
		--Print 'Avg reading for customer Not exiting- Insert the avg reading'
		Insert Into Tbl_DirectCustomersAvgReadings(
					GlobalAccountNumber
					,AverageReading
					,ActiveStatusId
					,CreatedBy
					,CreatedDate
					)
		Values		(@GlobalAccountNumber
					,@AverageReading
					,1
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					)
		
		SELECT 1 AS IsSuccess
		FOR XML PATH('DirectCustomerAverageUploadBE')
	END
	
END

GO


