
GO

/****** Object:  View [dbo].[UDV_CustomerMeterInformation]    Script Date: 04/15/2015 14:35:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
  
ALTER VIEW [dbo].[UDV_CustomerMeterInformation]   
AS  
 SELECT  
   CD.GlobalAccountNumber   
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,CD.KnownAs  
  ,CD.ActiveStatusId  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName  
  ,PD.ReadCodeID  
  ,PD.SortOrder  
  ,AD.Highestconsumption  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BN.CycleId  
  ,BN.CycleCode  
  ,BN.CycleName  
  ,BN.SCCode  
  ,BN.ServiceCenterId  
  ,BN.ServiceCenterName  
  ,BN.SUCode  
  ,BN.ServiceUnitName  
  ,BN.SU_ID  
  ,BN.BUCode  
  ,BN.BU_ID  
  ,BN.BusinessUnitName  
  ,MS.[StatusName] AS ActiveStatus  
  ,CD.EmailId  
  ,PD.MeterNumber   
  ,MI.MeterType AS MeterTypeId  
  ,PD.PhaseId  
  ,AD.InitialReading   
 ,AD.PresentReading   
  ,AD.AvgReading  
  ,PD.RouteSequenceNumber AS RouteSequenceNo  
  ,PD.CustomerTypeId  
 ,CD.CreatedDate  
 ,CD.CreatedBy  
 ,CD.ModifiedDate  
 ,CD.ModifedBy 
 ,MI.MeterDials
 ,MI.Decimals  
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo   
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
LEFT JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo  
  
  

GO


