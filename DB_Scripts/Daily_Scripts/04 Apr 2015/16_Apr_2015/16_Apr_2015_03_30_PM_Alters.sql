GO
UPDATE Tbl_Menus SET Path='../Billing/DirectCustomersConsumptionUpload.aspx' 
WHERE Path='../Billing/DirectCustomersReadingsUpload.aspx'
GO
Insert Into Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
values('Direct Customer Average Update','../ConsumerManagement/DirectCustomerAverageUpload.aspx',4,22,1,1,178)
GO
Insert INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
values(1,178,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
Insert INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
values(12,178,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO