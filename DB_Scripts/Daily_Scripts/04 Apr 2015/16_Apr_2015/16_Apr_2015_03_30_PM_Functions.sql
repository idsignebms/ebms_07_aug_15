
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_Update]    Script Date: 04/16/2015 15:28:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_Update]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
 
 
Declare @NewAverage decimal(18,2 ),@TotalRecords INT
    SELECT	Top 1
         @NewAverage=AverageReading  
       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
 
 SELECT	@TotalRecords =COUNT(0)       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   
   IF(@TotalRecords>0) 
		SET @NewAverage = (isnull(@NewAverage,0)+@usage)/2
	ELSE
		SET @NewAverage = @usage
			
	RETURN @NewAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_Save]    Script Date: 04/16/2015 15:28:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_Save]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
 
 
Declare @NewAverage decimal(18,2 ),@TotalRecords INT
    SELECT	Top 1
         @NewAverage=AverageReading  
       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
 
 SELECT	@TotalRecords =COUNT(0)       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   
   IF(@TotalRecords>0) 
		SET @NewAverage = (isnull(@NewAverage,0)+@usage)/2
	ELSE
		SET @NewAverage = @usage
			
	RETURN @NewAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 04/16/2015 15:28:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
 
 
Declare @NewAverage decimal(18,2 ),@TotalRecords INT
    SELECT	Top 1
         @NewAverage=AverageReading  
       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
 
 SELECT	@TotalRecords =COUNT(0)       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   
   IF(@TotalRecords>0) 
		SET @NewAverage = (isnull(@NewAverage,0)+@usage)/2
	ELSE
		SET @NewAverage = @usage
			
	RETURN @NewAverage

END

GO


