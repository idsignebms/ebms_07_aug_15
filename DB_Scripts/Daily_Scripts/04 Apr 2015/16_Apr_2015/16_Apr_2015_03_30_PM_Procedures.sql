
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDirectCustomerAverageUpload]    Script Date: 04/16/2015 15:29:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)=''
			,@BUID VARCHAR(50)=''
			,@ReadCodeID INT
			,@GlobalAccountNumber VARCHAR(50)

	--SET	@AccountNo= '0000057173'
	--SET @BUID='BEDC_BU_0004'   --BUnit_AP
	
		SELECT  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
				,@BUID = C.value('(BUID)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	
		
	Select @CustomerBusinessUnitID=BU_ID
			,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
			,@ReadCodeID=ReadCodeID
			,@GlobalAccountNumber=GlobalAccountNumber
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	
	IF @CustomerBusinessUnitID IS NULL-- Is Customer Exists 
		BEGIN
		
			SELECT 1 AS IsAccountNoNotExists
			FOR XML PATH('DirectCustomerAverageUploadBE')
		END
	ELSE -- If Custoemr exists
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID ='' -- Is custoemr belongs to same BU
				BEGIN
					IF @ReadCodeID=1 -- Check Is Customer Active/InActive
					BEGIN
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 
							BEGIN
								 SELECT 1 AS IsSuccess 
										,@GlobalAccountNumber AS GlobalAccountNumber
										,CD.AccountNo As AccountNo  
										,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name  
										--,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress  
										,(dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)) AS ServiceAddress       
										--,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo  
										,(CASE WHEN (ISNULL(CD.OldAccountNo,'')='') then '--' else  CD.OldAccountNo end) AS OldAccountNo
										,CD.ClassName as Tariff
								FROM UDV_CustomerDescription CD  
								WHERE (CD.GlobalAccountNumber = @GlobalAccountNumber)  
								AND ActiveStatusId IN (1,2)    
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
						ELSE --IF customer Hold/Closed
							BEGIN
								--print 'Failure In Active Status Hold/Closed Customer'
								SELECT 1 AS IsHoldOrClosed
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
					END
					ELSE -- If Read Customer
					BEGIN
						--Print 'Read Customer'
						SELECT 1 AS IsReadCustomer
						FOR XML PATH('DirectCustomerAverageUploadBE')
					END		
				END
				ELSE-- IF Customer belongs to other BU
				BEGIN
					--Print 'Not Belogns to the Same BU' 
					SELECT 1 AS IsCustExistsInOtherBU
					FOR XML PATH('DirectCustomerAverageUploadBE')
				END
		END
END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 04/16/2015 15:29:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 08-JAN-2014    
-- Description: The purpose of this procedure is to get list of MeterInformation    
-- =============================================    
CREATE PROCEDURE [MASTERS].[USP_GetAvailableMeterList]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterNo VARCHAR(100)    
   ,@MeterSerialNo VARCHAR(100)    
   ,@BUID VARCHAR(50)    
   ,@CustomerTypeId INT    
       
  SELECT       
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')    
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')    
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  IF (@CustomerTypeId=3)    
   BEGIN    
    SELECT    
    (    
     SELECT    
       MeterId    
       ,MeterNo    
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType    
       ,MeterSize    
       ,MeterMultiplier    
       ,MeterBrand    
       ,MeterSerialNo    
       ,BU.BusinessUnitName  
       ,MeterDials
      FROM Tbl_MeterInformation AS M    
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID  
      WHERE M.ActiveStatusId IN(1)    
      AND (M.BU_ID=@BUID OR @BUID='')    
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails     
       WHERE MeterNumber !='' )    
       AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)    
      AND M.MeterType=1    
      AND M.MeterNo LIKE '%'+@MeterNo+'%'     
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'    
     FOR XML PATH('MastersBE'),TYPE    
    )    
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')    
   END    
  ELSE    
   BEGIN    
    SELECT    
    (    
     SELECT    
       MeterId    
       ,MeterNo    
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType    
       ,MeterSize    
       ,MeterMultiplier    
       ,MeterBrand    
       ,MeterSerialNo     
       ,MeterDials
       ,BU.BusinessUnitName  
      FROM Tbl_MeterInformation AS M    
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID  
      WHERE M.ActiveStatusId IN(1)    
      AND (M.BU_ID=@BUID OR @BUID='')    
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails     
       WHERE MeterNumber !='' )    
       AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)    
      AND M.MeterType !=1    
      AND M.MeterNo LIKE '%'+@MeterNo+'%'     
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'    
     FOR XML PATH('MastersBE'),TYPE    
    )    
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')    
   END    
       
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/16/2015 15:29:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  NEERAJ KANOJIYA        
-- Create date: 1/APRIL/2015        
-- Description: This Procedure is used to change customer address       
-- Modified BY: Faiz-ID103  
-- Modified Date: 07-Apr-2015  
-- Description: changed the street field value from 200 to 255 characters     
-- =============================================      
CREATE PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]        
(        
 @XmlDoc xml           
)        
AS        
BEGIN      
 DECLARE @TotalAddress INT     
   ,@IsSameAsServie BIT    
   ,@GlobalAccountNo   VARCHAR(50)          
     ,@NewPostalLandMark  VARCHAR(200)        
     ,@NewPostalStreet   VARCHAR(255)        
     ,@NewPostalCity   VARCHAR(200)        
     ,@NewPostalHouseNo   VARCHAR(200)        
     ,@NewPostalZipCode   VARCHAR(50)        
     ,@NewServiceLandMark  VARCHAR(200)        
     ,@NewServiceStreet   VARCHAR(255)        
     ,@NewServiceCity   VARCHAR(200)        
     ,@NewServiceHouseNo  VARCHAR(200)        
     ,@NewServiceZipCode  VARCHAR(50)        
     ,@NewPostalAreaCode  VARCHAR(50)        
     ,@NewServiceAreaCode  VARCHAR(50)        
     ,@NewPostalAddressID  INT        
     ,@NewServiceAddressID  INT       
     ,@ModifiedBy    VARCHAR(50)        
     ,@Details     VARCHAR(MAX)        
     ,@RowsEffected    INT        
     ,@AddressID INT       
     ,@StatusText VARCHAR(50)    
     ,@IsCommunicationPostal BIT    
     ,@IsCommunicationService BIT    
     ,@ApprovalStatusId INT=2      
     ,@PostalAddressID INT    
     ,@ServiceAddressID INT    
     ,@BookNo VARCHAR(50)    
       SELECT           
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')        
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')            
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')            
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')            
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')           
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')          
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')            
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')            
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')            
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')           
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')        
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')        
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')        
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')        
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')        
     ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')        
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')        
     ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')        
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')    
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')    
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
BEGIN TRY    
  BEGIN TRAN     
    --UPDATE POSTAL ADDRESS    
 SET @StatusText='Total address count.'       
 SET @TotalAddress=(SELECT COUNT(0)     
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1    
        )    
            
 --=====================================================================================            
 --Update book number of customer.    
 --=====================================================================================     
Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)
IF(@ExistingBookNo!=@BookNo)
BEGIN
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails     
	SET BookNo=@BookNo,IsBookNoChanged=1     
	WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1
END
 --=====================================================================================            
 --Customer has only one addres and wants to update the address. //CONDITION 1//    
 --=====================================================================================        
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)    
 BEGIN    
  SET @StatusText='CONDITION 1'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END        
 --=====================================================================================    
 --Customer has one addres and wants to add new address. //CONDITION 2//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 2'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(    
                HouseNo    
                ,LandMark    
                ,StreetName    
                ,City    
                ,AreaCode    
                ,ZipCode    
                ,ModifedBy    
                ,ModifiedDate    
                ,IsCommunication    
                ,IsServiceAddress    
                ,IsActive    
                ,GlobalAccountNumber    
                )    
               VALUES (@NewServiceHouseNo           
                ,@NewServiceLandMark           
                ,@NewServiceStreet            
                ,@NewServiceCity          
                ,@NewServiceAreaCode          
             ,@NewServiceZipCode            
                ,@ModifiedBy          
                ,dbo.fn_GetCurrentDateTime()      
                ,@IsCommunicationService             
                ,1    
                ,1    
                ,@GlobalAccountNo    
                )     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo                   
 END       
     
 --=====================================================================================    
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)     
 BEGIN    
  SET @StatusText='CONDITION 3'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    IsActive=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END      
 --=====================================================================================    
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 4'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END          
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END          
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END          
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END          
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END            
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END             
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationService    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo       
 END      
 COMMIT TRAN    
END TRY    
BEGIN CATCH    
 ROLLBACK TRAN    
 SET @RowsEffected=0    
END CATCH    
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')             
END  
GO


