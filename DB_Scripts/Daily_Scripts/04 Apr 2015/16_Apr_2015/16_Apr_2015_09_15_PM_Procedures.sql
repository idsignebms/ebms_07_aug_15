
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersLog]    Script Date: 04/16/2015 21:12:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- =============================================      
CREATE PROCEDURE [dbo].[USP_GetAssignMetersLog]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT AM.GlobalAccountNo AS AccountNo
			   ,AM.MeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,AM.InitialReading
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks  
			   ,AM.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersWithLimit]    Script Date: 04/16/2015 21:12:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- =============================================      
CREATE PROCEDURE [dbo].[USP_GetAssignMetersWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 AM.GlobalAccountNo AS AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,AM.InitialReading
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks  
			   ,AM.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 -- changed by karteek end       
		 GROUP BY AM.GlobalAccountNo
			   ,AM.CreatedBy
			   ,AM.MeterNo
			   ,AM.AssignedMeterDate
			   ,AM.InitialReading
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107)  
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks  
			   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectLogs]    Script Date: 04/16/2015 21:12:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get ReadToDirect request logs  
-- =============================================     
CREATE PROCEDURE [dbo].[USP_GetReadToDirectLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT RD.GlobalAccountNo AS AccountNo
			   ,RD.MeterNo
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 -- changed by karteek end 
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 04/16/2015 21:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- =============================================     
CREATE PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 RD.GlobalAccountNo AS AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 -- changed by karteek end       
		 GROUP BY RD.GlobalAccountNo
			   ,RD.MeterNo
			   ,RD.CreatedBy  
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107)  
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersLog]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersLog]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT AM.GlobalAccountNo AS AccountNo
			   ,AM.MeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,AM.InitialReading
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks  
			   ,AM.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersWithLimit]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 AM.GlobalAccountNo AS AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,AM.InitialReading
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks  
			   ,AM.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 -- changed by karteek end       
		 GROUP BY AM.GlobalAccountNo
			   ,AM.CreatedBy
			   ,AM.MeterNo
			   ,AM.AssignedMeterDate
			   ,AM.InitialReading
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107)  
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks  
			   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]  
(  
@XmlDoc xml  
)
AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT
--		,@IsFinalApproval BIT = 0
--		,@FunctionId INT

--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
--	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
--				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
--		BEGIN
--			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
--		END   
--	ELSE
--		BEGIN
--			DECLARE @RoleId INT, @CurrentLevel INT
--					,@PresentRoleId INT, @NextRoleId INT
--					,@ReadToDirectId INT
			
--			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
--			SET @CurrentLevel = 0 -- For Stating Level			
			
--			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
--				BEGIN
--					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
--				END
					
--			SELECT @PresentRoleId = PresentRoleId 
--				,@NextRoleId = NextRoleId 
--			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
--			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
--											 GlobalAccountNo
--											,MeterNo
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											,PresentApprovalRole
--											,NextApprovalRole
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											,@PresentRoleId
--											,@NextRoleId
--											)
			
--			SET @ReadToDirectId = SCOPE_IDENTITY()
			
--			IF(@IsFinalApproval = 1)
--				BEGIN
					
--					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--						SET ReadCodeID=1, MeterNumber = NULL
--					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
--					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
--						 ReadToDirectId
--						,GlobalAccountNo
--						,MeterNo
--						,Remarks
--						,ApprovalStatusId
--						,CreatedBy
--						,CreatedDate
--						,PresentApprovalRole
--						,NextApprovalRole)  
--					SELECT  
--						 ReadToDirectId
--						,GlobalAccountNo
--						,MeterNo
--						,Remarks  
--						,ApprovalStatusId  
--						,CreatedBy  
--						,CreatedDate  
--						,PresentApprovalRole
--						,NextApprovalRole
--					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
--				END
			
--			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
--		END
--END


BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@StatusText VARCHAR(200)='Initiation'
		,@IsSuccessful BIT=1
		,@ApprovalStatusId INT

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	SET @StatusText='Deserialization'
	
	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	SET @StatusText='Update Statement'
	
	UPDATE TBL_ReadToDirectCustomerActivity 
	SET IsLatest=0
	WHERE GlobalAccountNo = @GlobalAccountNumber
	
	SET @StatusText='Insert Log Statement'
	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
												(
												GlobalAccountNo
												,MeterNo
												,Remarks
												,CreatedBy 
												,CreatedDate
												,ApprovalStatusId
												)
										VALUES
												(
												@GlobalAccountNumber
												,@MeterNumber
												,@Reason
												,@CreatedBy
												,DBO.fn_GetCurrentDateTime()
												,@ApprovalStatusId
												)
	SET @StatusText='Update Statement'
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
		SET ReadCodeID=1, MeterNumber = NULL
	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('MastersBE'),TYPE

END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
--	DECLARE 
--		 @MeterNumber VARCHAR(50)
--		,@GlobalAccountNumber VARCHAR(50)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT
--		,@IsFinalApproval BIT = 0
--		,@FunctionId INT
--	SELECT  
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--		,@AssignedMeterDate=C.value('(AssignedMeterDate)[1]','VARCHAR(20)')
--		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
--		,@FunctionId = C.value('(FunctionId)[1]','INT')
--		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
--	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
--				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
--		BEGIN
--			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
--		END   
--	ELSE
--		BEGIN
--			DECLARE @RoleId INT, @CurrentLevel INT
--					,@PresentRoleId INT, @NextRoleId INT
--					,@AssignedMeterRequestId INT
			
--			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
--			SET @CurrentLevel = 0 -- For Stating Level			
			
--			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
--				BEGIN
--					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
--				END
					
--			SELECT @PresentRoleId = PresentRoleId 
--				,@NextRoleId = NextRoleId 
--			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
--			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											,PresentApprovalRole
--											,NextApprovalRole
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											,@PresentRoleId
--											,@NextRoleId
--											)
			
--			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
--			IF(@IsFinalApproval = 1)
--				BEGIN
					
--					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--							SET MeterNumber=@MeterNumber
--								,ReadCodeID=2
--					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--					SET InitialReading=@InitialReading
--						,PresentReading=@InitialReading
--					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
--					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
--						 AssignedMeterId
--						,GlobalAccountNo
--						,MeterNo
--						,AssignedMeterDate
--						,InitialReading
--						,Remarks
--						,ApprovalStatusId
--						,CreatedBy
--						,CreatedDate
--						,PresentApprovalRole
--						,NextApprovalRole)  
--					SELECT  
--						 AssignedMeterId
--						,GlobalAccountNo
--						,MeterNo
--						,AssignedMeterDate
--						,InitialReading
--						,Remarks  
--						,ApprovalStatusId  
--						,CreatedBy  
--						,CreatedDate  
--						,PresentApprovalRole
--						,NextApprovalRole
--					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
--				END
			
--			SELECT 1 AS IsSuccess FOR XML PATH('CustomerRegistrationBE')
--		END
--END

DECLARE @MeterNumber VARCHAR(50)  
		,@ReadCodeID VARCHAR(50) 
		,@GlobalAccountNumber VARCHAR(50) 
		,@IsSuccessful BIT =1
		,@StatusText VARCHAR(200)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	--SET @StatusText='Deserialization'
	 SELECT  
	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
	  ,@AssignedMeterDate=C.value('(AssignedMeterDate)[1]','VARCHAR(20)')
	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
	SET @StatusText='Insert Log'
	
		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											)
											
		
	SET @StatusText='Update Statement'
	
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
	SET MeterNumber=@MeterNumber
		,ReadCodeID=@ReadCodeID
	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	SET InitialReading=@InitialReading
		,PresentReading=@InitialReading
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('CustomerRegistrationBE'),TYPE

END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectLogs]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get ReadToDirect request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT RD.GlobalAccountNo AS AccountNo
			   ,RD.MeterNo
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 -- changed by karteek end 
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 RD.GlobalAccountNo AS AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 -- changed by karteek end       
		 GROUP BY RD.GlobalAccountNo
			   ,RD.MeterNo
			   ,RD.CreatedBy  
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107)  
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
-- =============================================        
-- Author:  NEERAJ KANOJIYA        
-- Create date: 1/APRIL/2015        
-- Description: This Procedure is used to change customer address       
-- Modified BY: Faiz-ID103  
-- Modified Date: 07-Apr-2015  
-- Description: changed the street field value from 200 to 255 characters      
-- Modified BY: Faiz-ID103  
-- Modified Date: 16-Apr-2015  
-- Description: Add the condition for IsBookNoChanged
-- =============================================      
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]        
(        
 @XmlDoc xml           
)        
AS        
BEGIN      
 DECLARE @TotalAddress INT     
   ,@IsSameAsServie BIT    
   ,@GlobalAccountNo   VARCHAR(50)          
     ,@NewPostalLandMark  VARCHAR(200)        
     ,@NewPostalStreet   VARCHAR(255)        
     ,@NewPostalCity   VARCHAR(200)        
     ,@NewPostalHouseNo   VARCHAR(200)        
     ,@NewPostalZipCode   VARCHAR(50)        
     ,@NewServiceLandMark  VARCHAR(200)        
     ,@NewServiceStreet   VARCHAR(255)        
     ,@NewServiceCity   VARCHAR(200)        
     ,@NewServiceHouseNo  VARCHAR(200)        
     ,@NewServiceZipCode  VARCHAR(50)        
     ,@NewPostalAreaCode  VARCHAR(50)        
     ,@NewServiceAreaCode  VARCHAR(50)        
     ,@NewPostalAddressID  INT        
     ,@NewServiceAddressID  INT       
     ,@ModifiedBy    VARCHAR(50)        
     ,@Details     VARCHAR(MAX)        
     ,@RowsEffected    INT        
     ,@AddressID INT       
     ,@StatusText VARCHAR(50)    
     ,@IsCommunicationPostal BIT    
     ,@IsCommunicationService BIT    
     ,@ApprovalStatusId INT=2      
     ,@PostalAddressID INT    
     ,@ServiceAddressID INT    
     ,@BookNo VARCHAR(50)    
       SELECT           
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')        
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')            
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')            
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')            
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')           
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')          
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')            
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')            
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')            
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')           
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')        
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')        
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')        
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')        
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')        
     ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')        
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')        
     ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')        
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')    
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')    
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
BEGIN TRY    
  BEGIN TRAN     
    --UPDATE POSTAL ADDRESS    
 SET @StatusText='Total address count.'       
 SET @TotalAddress=(SELECT COUNT(0)     
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1    
        )    
            
 --=====================================================================================            
 --Update book number of customer.    
 --=====================================================================================     
Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)
IF(@ExistingBookNo=@BookNo)
BEGIN
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails     
	SET BookNo=@BookNo,IsBookNoChanged=1     
	WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1
END
 --=====================================================================================            
 --Customer has only one addres and wants to update the address. //CONDITION 1//    
 --=====================================================================================        
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)    
 BEGIN    
  SET @StatusText='CONDITION 1'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END        
 --=====================================================================================    
 --Customer has one addres and wants to add new address. //CONDITION 2//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 2'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(    
                HouseNo    
                ,LandMark    
                ,StreetName    
                ,City    
                ,AreaCode    
                ,ZipCode    
                ,ModifedBy    
                ,ModifiedDate    
                ,IsCommunication    
                ,IsServiceAddress    
                ,IsActive    
                ,GlobalAccountNumber    
                )    
               VALUES (@NewServiceHouseNo           
                ,@NewServiceLandMark           
                ,@NewServiceStreet            
                ,@NewServiceCity          
                ,@NewServiceAreaCode          
             ,@NewServiceZipCode            
                ,@ModifiedBy          
                ,dbo.fn_GetCurrentDateTime()      
                ,@IsCommunicationService             
                ,1    
                ,1    
                ,@GlobalAccountNo    
                )     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo                   
 END       
     
 --=====================================================================================    
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)     
 BEGIN    
  SET @StatusText='CONDITION 3'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    IsActive=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END      
 --=====================================================================================    
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 4'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END          
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END          
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END          
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END          
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END            
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END             
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationService    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo       
 END      
 COMMIT TRAN    
END TRY    
BEGIN CATCH    
 ROLLBACK TRAN    
 SET @RowsEffected=0    
END CATCH    
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')             
END  
GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 08-JAN-2014    
-- Description: The purpose of this procedure is to get list of MeterInformation    
-- Modified By:  Faiz-ID103
-- Modified date: 08-JAN-2014    
-- Description: Added Business Unit name and Meter Dials field for new customer registration page

-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterNo VARCHAR(100)    
   ,@MeterSerialNo VARCHAR(100)    
   ,@BUID VARCHAR(50)    
   ,@CustomerTypeId INT    
       
  SELECT       
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')    
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')    
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  IF (@CustomerTypeId=3)    
   BEGIN    
    SELECT    
    (    
     SELECT    
       MeterId    
       ,MeterNo    
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType    
       ,MeterSize    
       ,MeterMultiplier    
       ,MeterBrand    
       ,MeterSerialNo    
       ,BU.BusinessUnitName  
       ,MeterDials
      FROM Tbl_MeterInformation AS M    
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID  
      WHERE M.ActiveStatusId IN(1)    
      AND (M.BU_ID=@BUID OR @BUID='')    
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails     
       WHERE MeterNumber !='' )    
       AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)    
      AND M.MeterType=1    
      AND M.MeterNo LIKE '%'+@MeterNo+'%'     
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'    
     FOR XML PATH('MastersBE'),TYPE    
    )    
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')    
   END    
  ELSE    
   BEGIN    
    SELECT    
    (    
     SELECT    
       MeterId    
       ,MeterNo    
       ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType    
       ,MeterSize    
       ,MeterMultiplier    
       ,MeterBrand    
       ,MeterSerialNo     
       ,MeterDials
       ,BU.BusinessUnitName  
      FROM Tbl_MeterInformation AS M    
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID  
      WHERE M.ActiveStatusId IN(1)    
      AND (M.BU_ID=@BUID OR @BUID='')    
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails     
       WHERE MeterNumber !='' )    
       AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)    
      AND M.MeterType !=1    
      AND M.MeterNo LIKE '%'+@MeterNo+'%'     
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'    
     FOR XML PATH('MastersBE'),TYPE    
    )    
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')    
   END    
       
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateMeterTypeActiveStatus]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  V.Bhimaraju      
-- Create date: 14-03-2014      
-- Description: The purpose of this procedure is to update active status of MeterType     
-- Modified By : Bhargav G    
-- Modified Date : 19th Feb, 2015    
-- Modified Description :  Restricting user to deactivate the meter type when any customers are associated.        
-- Modified By : Faiz
-- Modified Date : 16-Apr-2015
-- Modified Description :  Modified to Restrict user to deactivate the meter type when any customers are associated.

-- =============================================      
ALTER PROCEDURE [dbo].[USP_UpdateMeterTypeActiveStatus]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @MeterTypeId INT    
   ,@ActiveStatus INT      
   ,@ModifiedBy VARCHAR(50)          
   ,@MeterCount int  
     
 SELECT @MeterTypeId=C.value('(MeterTypeId)[1]','INT')      
  ,@ActiveStatus=C.value('(ActiveStatusId)[1]','INT')      
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')      
 FROM @XmlDoc.nodes('MastersBE') as T(C)      
    
  --set @MeterCount=(SELECT count(0) FROM UDV_CustomerDescription WHERE MeterTypeId = @MeterTypeId);  
    set @MeterCount=(	SELECT COUNT(0)
						FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD
						JOIN Tbl_MeterInformation MI ON MI.MeterNo=CPD.MeterNumber
						WHERE MI.MeterType = @MeterTypeId
					)
 --IF NOT EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE MeterTypeId = @MeterTypeId)    
 IF(@MeterCount=0)  
  BEGIN    
   UPDATE Tbl_MMeterTypes     
    SET ActiveStatus=@ActiveStatus      
     ,ModifiedBy=@ModifiedBy      
     ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
   WHERE MeterTypeId=@MeterTypeId    
    
   SELECT 1 AS IsSuccess     
   FOR XML PATH('MastersBE')      
  END    
 ELSE    
  BEGIN    
  SELECT @MeterCount AS [Count], 0 AS IsSuccess
  FOR XML PATH('MastersBE')    
   --SELECT     
   -- COUNT(0) AS [Count]    
   -- ,0 AS IsSuccess    
   --FROM UDV_CustomerDescription    
   --WHERE MeterTypeId = @MeterTypeId    
   --FOR XML PATH('MastersBE')    
  END    
END     
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/16/2015 21:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT
		,@Rollover BIT
		,@Previous varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@Count = C.value('(TotalReadings)[1]','INT')
		,@Avg = C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@Rollover = C.value('(Rollover)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@Previous = C.value('(PreviousReading)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @ReadId INT
	Declare	 @Dials INT
	Declare	  @Multipler INT
	Declare @IsExistingRollOver bit
	Declare	   @MeterNumber varchar(50),@FirstPrevReading varchar(50),@FirstPresentValue varchar(50) 
	 ,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
	 
		,@SecoundReadingPrevReading varchar(20)
		,@AverageReading VARCHAR(50)
		 
	SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=IsRollOver,@MeterNumber=MeterNumber
	,@Dials	= LEN(PreviousReading) ,@ReadDate=ReadDate 
	FROM Tbl_CustomerReadings
	WHERE GlobalAccountNumber = @AccNum
	ORDER BY CustomerReadingId DESC

	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
		 
		 
	IF @IsExistingRollOver = 1
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
	END
	ELSE
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId    
	END
	 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	Declare @FirstReadingUsage bigint =@Usage
	Declare @SecountReadingUsage Bigint =0
	SET @FirstPrevReading   = @Previous
	SET @FirstPresentValue    =	 @Current
	
	IF @Rollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
	--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		 
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@Rollover
		)
	IF(@Rollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage+1
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@Rollover)
									
					 
				END
			
 
		END
	
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum
	

	 

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
	   
END



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/16/2015 21:12:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		IF	@Usage >0
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@IsRollover)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage+1
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@IsRollover)
									
					 
				END
			
 
		END
		
 
	
	
	
	 



	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO


