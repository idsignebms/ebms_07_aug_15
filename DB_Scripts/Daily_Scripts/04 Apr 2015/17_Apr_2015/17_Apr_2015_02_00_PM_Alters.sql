
GO
INSERT INTO TBL_FunctionalAccessPermission([Function],AccessLevels,IsActive,CreatedBy,CreatedDate)
VALUES('Assign Meter',1,0,'Admin',GETDATE())
GO
INSERT INTO TBL_FunctionalAccessPermission([Function],AccessLevels,IsActive,CreatedBy,CreatedDate)
VALUES('Change ReadToDirect',1,0,'Admin',GETDATE())
GO
GO
CREATE TABLE Tbl_AssignedMeterLogs
(
	 AssignedMeterId INT PRIMARY KEY IDENTITY(1,1)
	,GlobalAccountNo VARCHAR(50)
	,MeterNo VARCHAR(50)
	,AssignedMeterDate DATETIME
	,InitialReading DECIMAL(18,2)
	,Remarks VARCHAR(MAX)
	,ApprovalStatusId INT DEFAULT 2
	,PresentApprovalRole INT DEFAULT 0
	,NextApprovalRole INT DEFAULT 0
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)
GO
CREATE TABLE Tbl_ReadToDirectCustomerActivityLogs
(
	 ReadToDirectId INT PRIMARY KEY IDENTITY(1,1)
	,GlobalAccountNo VARCHAR(50)
	,MeterNo VARCHAR(50)
	,Remarks VARCHAR(MAX)
	,ApprovalStatusId INT DEFAULT 2
	,PresentApprovalRole INT DEFAULT 0
	,NextApprovalRole INT DEFAULT 0
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)
GO

CREATE TABLE Tbl_Audit_AssignedMeterLogs
(
	 AssignedMeterId INT 
	,GlobalAccountNo VARCHAR(50)
	,MeterNo VARCHAR(50)
	,AssignedMeterDate DATETIME
	,InitialReading DECIMAL(18,2)
	,Remarks VARCHAR(MAX)
	,ApprovalStatusId INT
	,PresentApprovalRole INT 
	,NextApprovalRole INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)
GO
CREATE TABLE Tbl_Audit_ReadToDirectCustomerActivityLogs
(
	 ReadToDirectId INT
	,GlobalAccountNo VARCHAR(50)
	,MeterNo VARCHAR(50)
	,Remarks VARCHAR(MAX)
	,ApprovalStatusId INT
	,PresentApprovalRole INT
	,NextApprovalRole INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)