
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetMarketersFullName_New]    Script Date: 04/17/2015 14:04:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek  
-- Create date: 17-04-2015  
-- Description: To Get FullName Of the Marketer FirstName+MiddleName+LastName  
-- =============================================  
CREATE FUNCTION [dbo].[fn_GetMarketersFullName_New]  
(  
 @FirstName VARCHAR(50),  
 @MiddleName VARCHAR(50),  
 @LastName VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @Name VARCHAR(MAX)  
  
  SET @Name = LTRIM(RTRIM(  
  CASE WHEN @FirstName IS NULL OR @FirstName = '' THEN '' ELSE  LTRIM(RTRIM(@FirstName)) END +   
  CASE WHEN @MiddleName IS NULL OR @MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@MiddleName)) END +  
  CASE WHEN @LastName IS NULL OR @LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@LastName)) END))  

 RETURN @Name  
END  
GO


