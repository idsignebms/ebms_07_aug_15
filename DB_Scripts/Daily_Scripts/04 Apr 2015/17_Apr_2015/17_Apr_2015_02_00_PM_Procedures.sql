
GO

/****** Object:  StoredProcedure [dbo].[USP_GetConsumptionDelivered]    Script Date: 04/17/2015 14:05:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Get the Consumption Delivered records.
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetConsumptionDelivered]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('ConsumptionDeliveredBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY CreatedDate DESC) AS RowNumber  
										,BU_ID
										,SU_ID
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										,ConsumptionMonth
										,ConsumptionYear
										FROM Tbl_ConsumptionDelivered  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('ConsumptionDeliveredBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('ConsumptionDeliveredListBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertConsumptionDelivered]    Script Date: 04/17/2015 14:05:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To save the Consumption Delivered record.
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @BU_ID	VARCHAR(50)
			,@SU_ID	VARCHAR(50)
			,@Capacity	DECIMAL
			,@SUPPMConsumption	DECIMAL
			,@SUCreditConsumption	Decimal
			,@TotalConsumption	DECIMAL
			,@ConsumptionMonth	INT
			,@ConsumptionYear	INT
			,@CreatedBy	VARCHAR(50)
	
	SELECT 
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL')
			,@ConsumptionMonth=C.value('(ConsumptionMonth)[1]','INT')
			,@ConsumptionYear=C.value('(ConsumptionYear)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	
	INSERT INTO Tbl_ConsumptionDelivered(
										BU_ID
										,SU_ID
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										,ConsumptionMonth
										,ConsumptionYear
										,CreatedBy
										,CreatedDate
										)
								VALUES	(
										@BU_ID
										,@SU_ID
										,@Capacity
										,@SUPPMConsumption
										,@SUCreditConsumption
										,@TotalConsumption
										,@ConsumptionMonth
										,@ConsumptionYear
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime()
										)
	SELECT @@ROWCOUNT AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_SearchCustomers_Temp]    Script Date: 04/17/2015 14:05:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Satya/Karteek 
-- Create date: 17-04-2015   
-- Description: The purpose of this procedure is to search customer      
-- =============================================      
CREATE PROCEDURE [dbo].[USP_SearchCustomers_Temp]
(      
	 @SearchString varchar(max)   
	,@StartIndex INT   
	,@EndIndex INT   
)      
AS  
  
BEGIN      

 Declare @DynamicQuery varchar(max) 

SET	@DynamicQuery='
	;WITH SearchedResult as
	(
	SELECT ROW_NUMBER() OVER(ORDER BY GlobalaccountNumber DESC) as RowNum,* 
	FROM [UDV_SearchCustomer] '+@SearchString+' 
	) 

	SELECT * FROM SearchedResult
	WHERE RowNum >= '+@StartIndex+' 
	AND RowNum  <= '+@EndIndex

--PRINT @DynamicQuery

EXECUTE(@DynamicQuery)
      
END  
   
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetConsumptionDelivered]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Get the Consumption Delivered records.
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetConsumptionDelivered]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('ConsumptionDeliveredBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY CreatedDate DESC) AS RowNumber  
										,BU_ID
										,SU_ID
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										,ConsumptionMonth
										,ConsumptionYear
										FROM Tbl_ConsumptionDelivered  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('ConsumptionDeliveredBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('ConsumptionDeliveredListBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 RD.GlobalAccountNo AS AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 -- changed by karteek end       
		 --GROUP BY RD.GlobalAccountNo
			--   ,RD.MeterNo
			--   ,RD.CreatedBy  
			--   ,CONVERT(VARCHAR(30),RD.CreatedDate,107)  
			--   ,ApproveSttus.ApprovalStatus  
			--   ,RD.Remarks  
			--   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_SearchCustomers_Temp]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Satya/Karteek 
-- Create date: 17-04-2015   
-- Description: The purpose of this procedure is to search customer      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_SearchCustomers_Temp]
(      
	 @SearchString varchar(max)   
	,@StartIndex INT   
	,@EndIndex INT   
)      
AS  
  
BEGIN      

 Declare @DynamicQuery varchar(max) 

SET	@DynamicQuery='
	;WITH SearchedResult as
	(
	SELECT ROW_NUMBER() OVER(ORDER BY GlobalaccountNumber DESC) as RowNum,* 
	FROM [UDV_SearchCustomer] '+@SearchString+' 
	) 

	SELECT * FROM SearchedResult
	WHERE RowNum >= '+@StartIndex+' 
	AND RowNum  <= '+@EndIndex

--PRINT @DynamicQuery

EXECUTE(@DynamicQuery)
      
END  
   
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertConsumptionDelivered]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To save the Consumption Delivered record.
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @BU_ID	VARCHAR(50)
			,@SU_ID	VARCHAR(50)
			,@Capacity	DECIMAL
			,@SUPPMConsumption	DECIMAL
			,@SUCreditConsumption	Decimal
			,@TotalConsumption	DECIMAL
			,@ConsumptionMonth	INT
			,@ConsumptionYear	INT
			,@CreatedBy	VARCHAR(50)
	
	SELECT 
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL')
			,@ConsumptionMonth=C.value('(ConsumptionMonth)[1]','INT')
			,@ConsumptionYear=C.value('(ConsumptionYear)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	
	INSERT INTO Tbl_ConsumptionDelivered(
										BU_ID
										,SU_ID
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										,ConsumptionMonth
										,ConsumptionYear
										,CreatedBy
										,CreatedDate
										)
								VALUES	(
										@BU_ID
										,@SU_ID
										,@Capacity
										,@SUPPMConsumption
										,@SUCreditConsumption
										,@TotalConsumption
										,@ConsumptionMonth
										,@ConsumptionYear
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime()
										)
	SELECT @@ROWCOUNT AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]
(  
@XmlDoc xml  
)
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT

	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ReadToDirectId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)			
													
			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
											 GlobalAccountNo
											,MeterNo
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											)
			
			SET @ReadToDirectId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE TBL_ReadToDirectCustomerActivity 
						SET IsLatest=0
					WHERE GlobalAccountNo = @GlobalAccountNumber
	
					INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNumber
														,@MeterNumber
														,@CreatedBy
														,DBO.fn_GetCurrentDateTime()
														)
												
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
		END
END
--AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@StatusText VARCHAR(200)='Initiation'
--		,@IsSuccessful BIT=1
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	SET @StatusText='Deserialization'
	
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	SET @StatusText='Update Statement'
	
--	UPDATE TBL_ReadToDirectCustomerActivity 
--	SET IsLatest=0
--	WHERE GlobalAccountNo = @GlobalAccountNumber
	
--	SET @StatusText='Insert Log Statement'
--	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
--												(
--												GlobalAccountNo
--												,MeterNumber
--												,Remarks
--												,CreatedBy 
--												,CreatedDate
--												,ApprovalStatusId
--												)
--										VALUES
--												(
--												@GlobalAccountNumber
--												,@MeterNumber
--												,@Reason
--												,@CreatedBy
--												,DBO.fn_GetCurrentDateTime()
--												,@ApprovalStatusId
--												)
--	SET @StatusText='Update Statement'
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--		SET ReadCodeID=1, MeterNumber = NULL
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('MastersBE'),TYPE

--END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading_New]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteekk
-- Create date: 15 Apr 2015  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading_New]  
(
	@MultiXmlDoc XML
)  
AS  
BEGIN  
  
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),       
							 CurrentReading varchar(50),      
							 ReadDate DATETIME)     
            
	INSERT INTO @TempCustomer(AccountNo ,CurrentReading,ReadDate)       
	SELECT       
		  RTRIM(LTRIM(c.value('(Account_Number)[1]','VARCHAR(50)')))       
		  ,(CASE c.value('(Current_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Current_Reading)[1]','VARCHAR(50)') END)      
		  ,CONVERT(DATETIME,c.value('(Read_Date)[1]','varchar(50)'))  
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)   
	
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.AccountNo
		,CMI.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CMI.Title,CMI.FirstName,CMI.MiddleName,CMI.LastName) as Name
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		--,COUNT(0) AS CustomerCount
	INTO #CustomersListBook
	FROM @TempCustomer T
	INNER JOIN UDV_CustomerMeterInformation CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	

	SELECT
		(     		
		SELECT 
			 CB.GlobalAccountNumber AS AccNum
			,CB.AccountNo
			,CB.OldAccountNo
			,CB.Name
			,CB.MeterNumber AS MeterNo
			,CB.MeterDials
			,CB.Decimals
			,CB.ActiveStatusId
			,CONVERT(VARCHAR(20),CB.ReadDate,106) AS ReadDate
			,CB.ReadCodeID AS ReadCodeId
			,CB.BU_ID
			,CB.MarketerId
			,(CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - CONVERT(DECIMAL(18,2),ISNULL(CustomerReadings.PresentReading,CustomerReadings.PreviousReading))) AS Usage
			,CB.CurrentReading AS PresentReading
			,ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading
			,ISNULL(CustomerReadings.PresentReading,ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))) AS PreviousReading
			,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
			,CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106) AS LatestDate
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end) as PrvExist
			,(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) <= CONVERT(DATE,CB.ReadDate) then 1 
				else 0 end) as IsExists
			--,CustomerCount
			,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
		FROM #CustomersListBook CB
		LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber
		FOR XML PATH('BillingBE'),TYPE  
		)  
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
END  

GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN  

DECLARE @MeterNumber VARCHAR(50)  
		,@ReadCodeID VARCHAR(50) 
		,@GlobalAccountNumber VARCHAR(50) 
		,@IsSuccessful BIT =1
		,@StatusText VARCHAR(200)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT

BEGIN
BEGIN TRY
	BEGIN TRAN
			
	--SET @StatusText='Deserialization'
	 SELECT  
	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
	SET @StatusText='Insert Log'
	
		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											)
											
		
	SET @StatusText='Update Statement'
	
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
	SET MeterNumber=@MeterNumber
		,ReadCodeID=@ReadCodeID
	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	SET InitialReading=@InitialReading
		,PresentReading=@InitialReading
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	SET @StatusText='Success'
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @IsSuccessful =0
END CATCH
END
	SELECT 
			@IsSuccessful AS IsSuccessful
			,@StatusText AS StatusText
	FOR XML PATH('CustomerRegistrationBE'),TYPE

END
--	DECLARE 
--		 @MeterNumber VARCHAR(50)
--		,@GlobalAccountNumber VARCHAR(50)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT
--		,@IsFinalApproval BIT = 0
--		,@FunctionId INT
--	SELECT  
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--		,@AssignedMeterDate=C.value('(AssignedMeterDate)[1]','VARCHAR(20)')
--		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
--		,@FunctionId = C.value('(FunctionId)[1]','INT')
--		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
--	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
--				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
--		BEGIN
--			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
--		END   
--	ELSE
--		BEGIN
--			DECLARE @RoleId INT, @CurrentLevel INT
--					,@PresentRoleId INT, @NextRoleId INT
--					,@AssignedMeterRequestId INT
			
--			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
--			SET @CurrentLevel = 0 -- For Stating Level			
			
--			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
--				BEGIN
--					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
--				END
					
--			SELECT @PresentRoleId = PresentRoleId 
--				,@NextRoleId = NextRoleId 
--			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
--			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											,PresentApprovalRole
--											,NextApprovalRole
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											,@PresentRoleId
--											,@NextRoleId
--											)
			
--			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
--			IF(@IsFinalApproval = 1)
--				BEGIN
					
--					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--							SET MeterNumber=@MeterNumber
--								,ReadCodeID=2
--					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--					SET InitialReading=@InitialReading
--						,PresentReading=@InitialReading
--					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
--					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
--						 AssignedMeterId
--						,GlobalAccountNo
--						,MeterNo
--						,AssignedMeterDate
--						,InitialReading
--						,Remarks
--						,ApprovalStatusId
--						,CreatedBy
--						,CreatedDate
--						,PresentApprovalRole
--						,NextApprovalRole)  
--					SELECT  
--						 AssignedMeterId
--						,GlobalAccountNo
--						,MeterNo
--						,AssignedMeterDate
--						,InitialReading
--						,Remarks  
--						,ApprovalStatusId  
--						,CreatedBy  
--						,CreatedDate  
--						,PresentApprovalRole
--						,NextApprovalRole
--					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
--				END
			
--			SELECT 1 AS IsSuccess FOR XML PATH('CustomerRegistrationBE')
--		END
--END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  NEERAJ KANOJIYA        
-- Create date: 1/APRIL/2015        
-- Description: This Procedure is used to change customer address       
-- Modified BY: Faiz-ID103  
-- Modified Date: 07-Apr-2015  
-- Description: changed the street field value from 200 to 255 characters      
-- Modified BY: Faiz-ID103  
-- Modified Date: 16-Apr-2015  
-- Description: Add the condition for IsBookNoChanged
-- =============================================      
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]        
(        
 @XmlDoc xml           
)        
AS        
BEGIN      
 DECLARE @TotalAddress INT     
   ,@IsSameAsServie BIT    
   ,@GlobalAccountNo   VARCHAR(50)          
     ,@NewPostalLandMark  VARCHAR(200)        
     ,@NewPostalStreet   VARCHAR(255)        
     ,@NewPostalCity   VARCHAR(200)        
     ,@NewPostalHouseNo   VARCHAR(200)        
     ,@NewPostalZipCode   VARCHAR(50)        
     ,@NewServiceLandMark  VARCHAR(200)        
     ,@NewServiceStreet   VARCHAR(255)        
     ,@NewServiceCity   VARCHAR(200)        
     ,@NewServiceHouseNo  VARCHAR(200)        
     ,@NewServiceZipCode  VARCHAR(50)        
     ,@NewPostalAreaCode  VARCHAR(50)        
     ,@NewServiceAreaCode  VARCHAR(50)        
     ,@NewPostalAddressID  INT        
     ,@NewServiceAddressID  INT       
     ,@ModifiedBy    VARCHAR(50)        
     ,@Details     VARCHAR(MAX)        
     ,@RowsEffected    INT        
     ,@AddressID INT       
     ,@StatusText VARCHAR(50)    
     ,@IsCommunicationPostal BIT    
     ,@IsCommunicationService BIT    
     ,@ApprovalStatusId INT=2      
     ,@PostalAddressID INT    
     ,@ServiceAddressID INT    
     ,@BookNo VARCHAR(50)    
       SELECT           
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')        
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')            
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')            
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')            
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')           
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')          
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')            
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')            
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')            
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')           
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')        
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')        
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')        
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')        
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')        
     ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')        
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')        
     ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')        
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')    
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')    
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
BEGIN TRY    
  BEGIN TRAN     
    --UPDATE POSTAL ADDRESS    
 SET @StatusText='Total address count.'       
 SET @TotalAddress=(SELECT COUNT(0)     
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1    
        )    
            
 --=====================================================================================            
 --Update book number of customer.    
 --=====================================================================================     
Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)
IF(@ExistingBookNo!=@BookNo)
BEGIN
	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails     
	SET BookNo=@BookNo,IsBookNoChanged=1     
	WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1
END
 --=====================================================================================            
 --Customer has only one addres and wants to update the address. //CONDITION 1//    
 --=====================================================================================        
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)    
 BEGIN    
  SET @StatusText='CONDITION 1'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END        
 --=====================================================================================    
 --Customer has one addres and wants to add new address. //CONDITION 2//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 2'     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(    
                HouseNo    
                ,LandMark    
                ,StreetName    
                ,City    
                ,AreaCode    
                ,ZipCode    
                ,ModifedBy    
                ,ModifiedDate    
                ,IsCommunication    
                ,IsServiceAddress    
                ,IsActive    
                ,GlobalAccountNumber    
                )    
               VALUES (@NewServiceHouseNo           
                ,@NewServiceLandMark           
                ,@NewServiceStreet            
                ,@NewServiceCity          
                ,@NewServiceAreaCode          
             ,@NewServiceZipCode            
                ,@ModifiedBy          
                ,dbo.fn_GetCurrentDateTime()      
                ,@IsCommunicationService             
                ,1    
                ,1    
                ,@GlobalAccountNo    
                )     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo                   
 END       
     
 --=====================================================================================    
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)     
 BEGIN    
  SET @StatusText='CONDITION 3'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    IsActive=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END      
 --=====================================================================================    
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 4'       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END          
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END          
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END          
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END          
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END            
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END             
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationService    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo       
 END      
 COMMIT TRAN    
END TRY    
BEGIN CATCH    
 ROLLBACK TRAN    
 SET @RowsEffected=0    
END CATCH    
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')             
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSC]    Script Date: 04/17/2015 14:05:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 01-09-2014  
-- Modified date: 03-11-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSC]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  
DECLARE @BU_ID VARCHAR(MAX),@SU_ID VARCHAR(MAX),@ServiceCenterId VARCHAR(MAX)  
    
 SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
  ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 SELECT  
 (  
  SELECT  
   CycleId  
   ,CycleName  
  FROM Tbl_Cycles CY  
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId AND CY.ActiveStatusId=1  
		AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='') 
		AND CycleId IN(SELECT DISTINCT CycleId FROM Tbl_BookNumbers WHERE ActiveStatusId = 1)  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID AND (BU.BU_ID=@BU_ID OR @BU_ID='') 
  ORDER BY CycleName ASC  
  FOR XML PATH('MastersBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
END



GO


