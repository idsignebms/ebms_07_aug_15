

Create Table Tbl_ConsumptionDelivered
(
ConsumptionDeliveredId int IDENTITY(1,1) PRIMARY KEY
,BU_ID	varchar(50)
,SU_ID	varchar(50)
,Capacity	Decimal
,SUPPMConsumption	Decimal
,SUCreditConsumption	Decimal
,TotalConsumption	Decimal
,ConsumptionMonth	Int
,ConsumptionYear	Int
,CreatedBy	varchar(50)
,CreatedDate	DateTime
,ModifiedBy	varchar(50)
,ModifiedDate	DateTime
)