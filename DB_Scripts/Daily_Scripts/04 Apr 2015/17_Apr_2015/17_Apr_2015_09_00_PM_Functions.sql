
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomersCount_Tariff]    Script Date: 04/17/2015 21:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 31 OCT 2014                        
-- Description  : THIS function is use to get customers under the selecting cycle
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetCustomersCount_Tariff]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @Result INT = 0    
		
		
  SELECT 
	@Result = COUNT(CustomerProcedureId) 
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
  JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
  WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
  AND CD.ActiveStatusId IN(1,2)
       
       
 RETURN @Result    
    
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetMarketersFullName_New]    Script Date: 04/17/2015 21:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek  
-- Create date: 17-04-2015  
-- Description: To Get FullName Of the Marketer FirstName+MiddleName+LastName  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetMarketersFullName_New]  
(  
 @FirstName VARCHAR(50),  
 @MiddleName VARCHAR(50),  
 @LastName VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @Name VARCHAR(MAX)  
  
  SET @Name = LTRIM(RTRIM(  
  CASE WHEN @FirstName IS NULL OR @FirstName = '' THEN '' ELSE  LTRIM(RTRIM(@FirstName)) END +   
  CASE WHEN @MiddleName IS NULL OR @MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@MiddleName)) END +  
  CASE WHEN @LastName IS NULL OR @LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@LastName)) END))  

 RETURN @Name  
END  
GO


