
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesBySC_HavingBooks]    Script Date: 04/17/2015 21:19:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek 
-- Create date: 17-04-2015  
-- Description: The purpose of this procedure is to get Cycles list by SC  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCyclesBySC_HavingBooks]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  
DECLARE @ServiceCenterId VARCHAR(MAX)  
    
 SELECT  
  @ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 SELECT  
 (  
  SELECT  
   CycleId  
   ,CycleName  
  FROM Tbl_Cycles CY  
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId AND CY.ActiveStatusId=1  
		AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='') 
		AND CycleId IN(SELECT DISTINCT CycleId FROM Tbl_BookNumbers WHERE ActiveStatusId = 1)  
  ORDER BY CycleName ASC  
  FOR XML PATH('MastersBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
END



GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteConsumptionDelivered]    Script Date: 04/17/2015 21:19:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To delete the Consumption Delivered record.
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @ConsumptionDeliveredId INT
			,@ModifiedBy	VARCHAR(50)
	
	SELECT 
			@ConsumptionDeliveredId=C.value('(ConsumptionDeliveredId)[1]','INT')
		    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	DELETE Tbl_ConsumptionDelivered 
	WHERE	ConsumptionDeliveredId=@ConsumptionDeliveredId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateConsumptionDelivered]    Script Date: 04/17/2015 21:19:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Update the Consumption Delivered record.
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @ConsumptionDeliveredId INT
			,@Capacity	DECIMAL
			,@SUPPMConsumption	DECIMAL
			,@SUCreditConsumption	Decimal
			,@TotalConsumption	DECIMAL
			,@ModifiedBy	VARCHAR(50)
	
	SELECT 
			@ConsumptionDeliveredId=C.value('(ConsumptionDeliveredId)[1]','INT')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL')
		    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	UPDATE Tbl_ConsumptionDelivered 
	SET		Capacity=@Capacity
			,SUPPMConsumption=@SUPPMConsumption
			,SUCreditConsumption=@SUCreditConsumption
			,TotalConsumption=(@SUPPMConsumption+@SUCreditConsumption)
			,ModifiedBy=@ModifiedBy
			,ModifiedDate=dbo.fn_GetCurrentDateTime()
	WHERE	ConsumptionDeliveredId=@ConsumptionDeliveredId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/17/2015 21:20:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  NEERAJ KANOJIYA        
-- Create date: 1/APRIL/2015        
-- Description: This Procedure is used to change customer address       
-- Modified BY: Faiz-ID103  
-- Modified Date: 07-Apr-2015  
-- Description: changed the street field value from 200 to 255 characters      
-- Modified BY: Faiz-ID103  
-- Modified Date: 16-Apr-2015  
-- Description: Add the condition for IsBookNoChanged
-- =============================================      
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]        
(        
 @XmlDoc xml           
)        
AS        
BEGIN      
 DECLARE @TotalAddress INT     
   ,@IsSameAsServie BIT    
   ,@GlobalAccountNo   VARCHAR(50)          
     ,@NewPostalLandMark  VARCHAR(200)        
     ,@NewPostalStreet   VARCHAR(255)        
     ,@NewPostalCity   VARCHAR(200)        
     ,@NewPostalHouseNo   VARCHAR(200)        
     ,@NewPostalZipCode   VARCHAR(50)        
     ,@NewServiceLandMark  VARCHAR(200)        
     ,@NewServiceStreet   VARCHAR(255)        
     ,@NewServiceCity   VARCHAR(200)        
     ,@NewServiceHouseNo  VARCHAR(200)        
     ,@NewServiceZipCode  VARCHAR(50)        
     ,@NewPostalAreaCode  VARCHAR(50)        
     ,@NewServiceAreaCode  VARCHAR(50)        
     ,@NewPostalAddressID  INT        
     ,@NewServiceAddressID  INT       
     ,@ModifiedBy    VARCHAR(50)        
     ,@Details     VARCHAR(MAX)        
     ,@RowsEffected    INT        
     ,@AddressID INT       
     ,@StatusText VARCHAR(50)    
     ,@IsCommunicationPostal BIT    
     ,@IsCommunicationService BIT    
     ,@ApprovalStatusId INT=2      
     ,@PostalAddressID INT    
     ,@ServiceAddressID INT    
     ,@BookNo VARCHAR(50)    
       SELECT           
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')        
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')            
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')            
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')            
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')           
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')          
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')            
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')            
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')            
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')           
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')        
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')        
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')        
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')        
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')        
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')        
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')        
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')    
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')        
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')    
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')    
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
BEGIN TRY    
  BEGIN TRAN     
    --UPDATE POSTAL ADDRESS    
 SET @StatusText='Total address count.'       
 SET @TotalAddress=(SELECT COUNT(0)     
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1    
        )    
            
 --=====================================================================================            
 --Update book number of customer.    
 --=====================================================================================     
Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)
IF(@ExistingBookNo!=@BookNo)
BEGIN
	
	DECLARE @OldBookNo VARCHAR(50)
	SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)

		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
										--,CustomerUniqueNo
										,OldBookNo
										,NewBookNo
										,CreatedBy
										,CreatedDate
										,ApproveStatusId
										,Remarks)
		VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),@ApprovalStatusId,@Details)	
			
		UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails     
		SET BookNo=@BookNo,IsBookNoChanged=1     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1
END
 --=====================================================================================            
 --Customer has only one addres and wants to update the address. //CONDITION 1//    
 --=====================================================================================        
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)    
 BEGIN    
  SET @StatusText='CONDITION 1'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END        
 --=====================================================================================    
 --Customer has one addres and wants to add new address. //CONDITION 2//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 2'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(    
                HouseNo    
                ,LandMark    
                ,StreetName    
                ,City    
                ,AreaCode    
                ,ZipCode    
                ,ModifedBy    
                ,ModifiedDate    
                ,IsCommunication    
                ,IsServiceAddress    
                ,IsActive    
                ,GlobalAccountNumber    
                )    
               VALUES (@NewServiceHouseNo           
                ,@NewServiceLandMark           
                ,@NewServiceStreet            
                ,@NewServiceCity          
                ,@NewServiceAreaCode          
             ,@NewServiceZipCode            
                ,@ModifiedBy          
                ,dbo.fn_GetCurrentDateTime()      
                ,@IsCommunicationService             
                ,1    
                ,1    
                ,@GlobalAccountNo    
                )     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo                   
 END       
     
 --=====================================================================================    
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)     
 BEGIN    
  SET @StatusText='CONDITION 3'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    IsActive=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END      
 --=====================================================================================    
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 4'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo


  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END          
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END          
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END          
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END          
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END            
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END             
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationService    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo       
 END      
 COMMIT TRAN    
END TRY    
BEGIN CATCH    
 ROLLBACK TRAN    
 SET @RowsEffected=0    
END CATCH    
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')             
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_SearchCustomers_Temp]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Satya/Karteek 
-- Create date: 17-04-2015   
-- Description: The purpose of this procedure is to search customer      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_SearchCustomers_Temp]
(      
	 @SearchString varchar(max)   
	,@StartIndex int 
	,@EndIndex int  
)      
AS  
  
BEGIN      

 Declare @DynamicQuery varchar(max) 

SET	@DynamicQuery='
	;WITH SearchedResult as
	(
	SELECT ROW_NUMBER() OVER(ORDER BY GlobalaccountNumber DESC) as RowNumber,COUNT(0) OVER() AS TotalRecords,* 
	FROM [UDV_SearchCustomer] '+@SearchString+' 
	) 

	SELECT * FROM SearchedResult
	WHERE RowNumber >= '+CONVERT(VARCHAR(10),@StartIndex)+'
	AND RowNumber  <= '+CONVERT(VARCHAR(10),@EndIndex)+''

--PRINT @DynamicQuery

EXECUTE (@DynamicQuery)
      
END  
   
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT   
   ,@IsSameAsServie BIT  
   ,@GlobalAccountNo   VARCHAR(50)        
     ,@NewPostalLandMark  VARCHAR(200)      
     ,@NewPostalStreet   VARCHAR(255)      
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT  
       SELECT         
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')      
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)      
BEGIN TRY  
  BEGIN TRAN   
    --UPDATE POSTAL ADDRESS  
 SET @StatusText='Total address count.'     
 SET @TotalAddress=(SELECT COUNT(0)   
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
        )  
 --=====================================================================================          
 --Customer has only one addres and wants to update the address. //CONDITION 1//  
 --=====================================================================================   
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
 BEGIN  
  SET @StatusText='CONDITION 1'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END      
 --=====================================================================================  
 --Customer has one addres and wants to add new address. //CONDITION 2//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 2'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
												 
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
                HouseNo  
                ,LandMark  
                ,StreetName  
                ,City  
                ,AreaCode  
                ,ZipCode  
                ,ModifedBy  
                ,ModifiedDate  
                ,IsCommunication  
                ,IsServiceAddress  
                ,IsActive  
                ,GlobalAccountNumber  
                )  
               VALUES (@NewServiceHouseNo         
                ,@NewServiceLandMark         
                ,@NewServiceStreet          
                ,@NewServiceCity        
                ,@NewServiceAreaCode        
                ,@NewServiceZipCode          
                ,@ModifiedBy        
                ,dbo.fn_GetCurrentDateTime()    
                ,@IsCommunicationService           
                ,1  
                ,1  
                ,@GlobalAccountNo  
                )   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo                 
 END     
   
 --=====================================================================================  
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
 BEGIN  
  SET @StatusText='CONDITION 3'  
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    IsActive=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END    
 --=====================================================================================  
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 4'
  
  INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationService  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo     
 END    
 COMMIT TRAN  
END TRY  
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @RowsEffected=0  
END CATCH  
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')           
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesBySC_HavingBooks]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek 
-- Create date: 17-04-2015  
-- Description: The purpose of this procedure is to get Cycles list by SC  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCyclesBySC_HavingBooks]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  
DECLARE @ServiceCenterId VARCHAR(MAX)  
    
 SELECT  
  @ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 SELECT  
 (  
  SELECT  
   CycleId  
   ,CycleName  
  FROM Tbl_Cycles CY  
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId AND CY.ActiveStatusId=1  
		AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='') 
		AND CycleId IN(SELECT DISTINCT CycleId FROM Tbl_BookNumbers WHERE ActiveStatusId = 1)  
  ORDER BY CycleName ASC  
  FOR XML PATH('MastersBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSC]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 01-09-2014  
-- Modified date: 03-11-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSC]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  
DECLARE @BU_ID VARCHAR(MAX),@SU_ID VARCHAR(MAX),@ServiceCenterId VARCHAR(MAX)  
    
 SELECT  
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
  ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)  
   
 SELECT  
 (  
  SELECT  
   CycleId  
   ,CycleName  
  FROM Tbl_Cycles CY  
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId AND CY.ActiveStatusId=1  
		AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='') 
		--AND CycleId IN(SELECT DISTINCT CycleId FROM Tbl_BookNumbers WHERE ActiveStatusId = 1)  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID AND (BU.BU_ID=@BU_ID OR @BU_ID='') 
  ORDER BY CycleName ASC  
  FOR XML PATH('MastersBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_Bookwise]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_Bookwise] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
	dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
	,MeterNumber as CustomerExistingMeterNumber
	,MeterDials As CustomerExistingMeterDials
	,Decimals as Decimals
	,InitialReading as	InitialReading
	,AccountNo
	,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeId = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
----------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			CustomerBills.BillGeneratedDate as EstimatedBillDate,
			0 AS IsActiveMonth,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExists,
			ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--') as LatestDate,
			CustomerList.Name,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			CustomerReadings.IsTamper,
			0 as Decimals,
			'' AS BillNo,
			ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading,
			CustomerReadings.TotalReadings,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then NULL else CustomerReadings.PresentReading end) 
				else NULL End as  PresentReading,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end   as PrvExist,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end AS Usage,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading else CustomerReadings.PreviousReading end)  	
				else isnull(CustomerList.InitialReading,0) End as PreviousReading,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CustomerBills.BillGeneratedDate) then CONVERT(VARCHAR(10),CustomerBills.ReadType) else '--' end)  LastBillReadType
			,CustomerList.BookNo,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	 
			LEFT JOIN	  
			#CustomerLatestBills as CustomerBills 
			ON CustomerBills.GlobalAccountNumber=CustomerList.GlobalAccountNumber
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table  #CustomersListBook   
DROP Table  #CustomerLatestBills
DROP Table  #CusotmersWithMaxBillID
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetConsumptionDelivered]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Get the Consumption Delivered records.
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetConsumptionDelivered]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
   ,@ConsumptionMonth INT
   ,@ConsumptionYear INT
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
   ,@ConsumptionMonth = C.value('(ConsumptionMonth)[1]','INT')  
   ,@ConsumptionYear = C.value('(ConsumptionYear)[1]','INT')  
  FROM @XmlDoc.nodes('ConsumptionDeliveredBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY CD.CreatedDate DESC) AS RowNumber  
										,ConsumptionDeliveredId
										,BusinessUnitName
										,ServiceUnitName
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										--,ConsumptionMonth
										--,ConsumptionYear
										,(dbo.fn_GetMonthName(ConsumptionMonth) +' - '+CONVERT(VARCHAR(20),ConsumptionYear)) AS ConsumptionMontYear
										FROM Tbl_ConsumptionDelivered CD
										Join Tbl_ServiceUnits SU ON SU.SU_ID=CD.SU_ID
										Join Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
										Where CD.ConsumptionMonth = @ConsumptionMonth
											  AND CD.ConsumptionYear = @ConsumptionYear	
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('ConsumptionDeliveredBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('ConsumptionDeliveredBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertConsumptionDelivered]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To save the Consumption Delivered record.
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @BU_ID	VARCHAR(50)
			,@SU_ID	VARCHAR(50)
			,@Capacity	DECIMAL
			,@SUPPMConsumption	DECIMAL
			,@SUCreditConsumption	Decimal
			,@TotalConsumption	DECIMAL
			,@ConsumptionMonth	INT
			,@ConsumptionYear	INT
			,@CreatedBy	VARCHAR(50)
	
	SELECT 
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL')
			,@ConsumptionMonth=C.value('(ConsumptionMonth)[1]','INT')
			,@ConsumptionYear=C.value('(ConsumptionYear)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	
	INSERT INTO Tbl_ConsumptionDelivered(
										BU_ID
										,SU_ID
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										,ConsumptionMonth
										,ConsumptionYear
										,CreatedBy
										,CreatedDate
										)
								VALUES	(
										@BU_ID
										,@SU_ID
										,@Capacity
										,@SUPPMConsumption
										,@SUCreditConsumption
										,(@SUPPMConsumption + @SUCreditConsumption)
										,@ConsumptionMonth
										,@ConsumptionYear
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime()
										)
	SELECT @@ROWCOUNT AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)

	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END

			SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

			--SELECT @PresentRoleId = PresentRoleId 
			--		,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)

			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterReading) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterInitialReading) END
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterReading) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterInitialReading) END
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,@PreviousReading
						,@OldMeterReading
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterReading) END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(DECIMAL(18,2),@NewMeterInitialReading) END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteConsumptionDelivered]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To delete the Consumption Delivered record.
-- =============================================
ALTER PROCEDURE [dbo].[USP_DeleteConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @ConsumptionDeliveredId INT
			,@ModifiedBy	VARCHAR(50)
	
	SELECT 
			@ConsumptionDeliveredId=C.value('(ConsumptionDeliveredId)[1]','INT')
		    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	DELETE Tbl_ConsumptionDelivered 
	WHERE	ConsumptionDeliveredId=@ConsumptionDeliveredId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateConsumptionDelivered]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Update the Consumption Delivered record.
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @ConsumptionDeliveredId INT
			,@Capacity	DECIMAL
			,@SUPPMConsumption	DECIMAL
			,@SUCreditConsumption	Decimal
			,@TotalConsumption	DECIMAL
			,@ModifiedBy	VARCHAR(50)
	
	SELECT 
			@ConsumptionDeliveredId=C.value('(ConsumptionDeliveredId)[1]','INT')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL')
		    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	UPDATE Tbl_ConsumptionDelivered 
	SET		Capacity=@Capacity
			,SUPPMConsumption=@SUPPMConsumption
			,SUCreditConsumption=@SUCreditConsumption
			,TotalConsumption=(@SUPPMConsumption+@SUCreditConsumption)
			,ModifiedBy=@ModifiedBy
			,ModifiedDate=dbo.fn_GetCurrentDateTime()
	WHERE	ConsumptionDeliveredId=@ConsumptionDeliveredId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
     
	--SELECT(  
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy  
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 -- changed by karteek end    
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 04/17/2015 21:20:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 RD.GlobalAccountNo AS AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 -- changed by karteek end       
		 --GROUP BY RD.GlobalAccountNo
			--   ,RD.MeterNo
			--   ,RD.CreatedBy  
			--   ,CONVERT(VARCHAR(30),RD.CreatedDate,107)  
			--   ,ApproveSttus.ApprovalStatus  
			--   ,RD.Remarks  
			--   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO


