
GO

/****** Object:  View [dbo].[UDV_SearchCustomer]    Script Date: 04/17/2015 21:14:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
ALTER VIEW [dbo].[UDV_SearchCustomer]   
AS  
 
	SELECT  
		CPD.GlobalAccountNumber,
		CD.AccountNo,
		CD.OldAccountNo,
		dbo.fn_GetCustomerFullName_New( 
				CD.Title,  
				CD.FirstName,  
				CD.MiddleName,  
				CD.LastName) AS Name, 
		CD.HomeContactNumber,
		BusinessContactNumber,
		CD.OtherContactNumber, 
		BookDetails.BusinessUnitName,
		Dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,Service_City,'',Service_ZipCode) As ServiceAddress,
		BookDetails.ServiceUnitName,
		BookDetails.ServiceCenterName,
		TariffClasses.ClassName,		 
		CStatus.StatusName,
		CPD.IsBookNoChanged,
		CPD.MeterNumber,
		CPD.ActiveStatusId
		,BookDetails.BU_ID
		,BookDetails.SU_ID
		,BookDetails.ServiceCenterId
		,BookDetails.CycleId
		,CPD.TariffClassID	as TariffId
		,ReadCodeId
		,CAD.CreatedDate
	FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD  
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
	INNER JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails(NOLOCK) CPA ON CPA.GlobalAccountNumber=CD.GlobalAccountNumber
	INNER JOIN	 dbo.UDV_BookNumberDetails(NOLOCK) BookDetails ON BookDetails.BookNo=CPD.BookNo
	INNER JOIN	 Tbl_MCustomerStatus(NOLOCK) CStatus ON CStatus.StatusId=CD.ActiveStatusId
	INNER JOIN	 Tbl_MTariffClasses(NOLOCK) TariffClasses ON TariffClasses.ClassID=CPD.TariffClassID
	 



GO

/****** Object:  View [dbo].[UDV_CustomerMeterInformation]    Script Date: 04/17/2015 21:14:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
  
ALTER VIEW [dbo].[UDV_CustomerMeterInformation]   
AS  
 SELECT  
   CD.GlobalAccountNumber   
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,CD.KnownAs  
  ,CD.ActiveStatusId  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName  
  ,PD.ReadCodeID  
  ,PD.SortOrder  
  ,AD.Highestconsumption  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BN.CycleId  
  ,BN.CycleCode  
  ,BN.CycleName  
  ,BN.SCCode  
  ,BN.ServiceCenterId  
  ,BN.ServiceCenterName  
  ,BN.SUCode  
  ,BN.ServiceUnitName  
  ,BN.SU_ID  
  ,BN.BUCode  
  ,BN.BU_ID  
  ,BN.BusinessUnitName  
  ,MS.[StatusName] AS ActiveStatus  
  ,CD.EmailId  
  ,PD.MeterNumber   
  ,MI.MeterType AS MeterTypeId  
  ,PD.PhaseId  
  ,AD.InitialReading   
 ,AD.PresentReading   
  ,AD.AvgReading  
  ,PD.RouteSequenceNumber AS RouteSequenceNo  
  ,PD.CustomerTypeId  
 ,CD.CreatedDate  
 ,CD.CreatedBy  
 ,CD.ModifiedDate  
 ,CD.ModifedBy 
 ,MI.MeterDials
 ,MI.Decimals
 ,BN.MarketerId
 ,BN.MarketerName
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo   
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
LEFT JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo  
  
  


GO

/****** Object:  View [dbo].[UDV_BookNumberDetails]    Script Date: 04/17/2015 21:14:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
  
ALTER VIEW [dbo].[UDV_BookNumberDetails]  
AS  
 SELECT   
 BU.BU_ID,BU.BUCode,BU.BusinessUnitName  
 ,SU.SU_ID,SU.SUCode,SU.ServiceUnitName  
 ,SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName  
 ,C.CycleId,C.CycleCode,C.CycleName  
 ,B.BookNo,B.BookCode,B.ID
 ,M.MarketerId
 ,dbo.fn_GetMarketersFullName_New(M.FirstName,M.MiddleName,M.LastName) AS MarketerName   
 FROM Tbl_BookNumbers B  
 JOIN Tbl_Cycles C ON C.CycleId=B.CycleId  
 JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
 JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
 JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
 JOIN Tbl_Marketers M ON M.MarketerId = B.MarketerId
GO


