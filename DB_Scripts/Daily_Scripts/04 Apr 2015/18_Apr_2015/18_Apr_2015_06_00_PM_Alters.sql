------------------- Faiz New Menu Inserts ----------------------------------------
GO
Update Tbl_Menus set Page_Order=6 where Name='Direct Customer Average Update' and ReferenceMenuId = 4 and IsActive=1
GO
Update Tbl_Menus SET Page_Order=9 where Name='Billing Disabled Books' and ReferenceMenuId=4 and IsActive=1
GO
Insert Into Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
values('Consumption Delivered','../Billing/ConsumptionDelivered.aspx',4,8,1,1,179)
GO
Insert INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
values(1,179,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
Insert INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
values(12,179,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
------------------- Faiz Menu Updates ----------------------------------------
GO
Update Tbl_Menus set IsActive=0 where Name='Customer Import' and ReferenceMenuId=1
GO
Update Tbl_Menus set Name='Billing Messages' where Name='Add Global Message' and ReferenceMenuId=122
GO
Update Tbl_Menus set Page_Order=10 where Name='Change Customer Contact Info' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=11 where Name='Change Customer Address' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=12 where Name='Change Customer BookNo' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=13 where Name='Change Customer Tariff' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=15 where Name='Change Customer MeterNo' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=17 where Name='Change Read To Direct' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=20 where Name='Change Customer Status' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=22 where Name='Change Customer Type' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=23 where Name='Change Customer Approvals' and ReferenceMenuId=1 and IsActive=1
GO
Update Tbl_Menus set Page_Order=24 where Name='Audit Tray Report' and ReferenceMenuId=1 and IsActive=1

------------------- Jeevan Service Address Update ----------------------------------------


UPDATE CUSTOMERS.Tbl_CustomersDetail SET PostalAddressID=ServiceAddressID WHERE PostalAddressID IS NULL
GO
UPDATE CUSTOMERS.Tbl_CustomersDetail SET ServiceAddressID=PostalAddressID WHERE ServiceAddressID IS NULL
GO
UPDATE CUSTOMERS.Tbl_CustomersDetail
SET 
Service_HouseNo=CUSTOMERS.Tbl_CustomerPostalAddressDetails.HouseNo
,Service_StreetName=CUSTOMERS.Tbl_CustomerPostalAddressDetails.StreetName
,Service_City	=CUSTOMERS.Tbl_CustomerPostalAddressDetails.City
,Service_Landmark=CUSTOMERS.Tbl_CustomerPostalAddressDetails.Landmark
,Service_AreaCode=CUSTOMERS.Tbl_CustomerPostalAddressDetails.AreaCode
,Service_ZipCode=CUSTOMERS.Tbl_CustomerPostalAddressDetails.ZipCode

FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails
WHERE CUSTOMERS.Tbl_CustomersDetail.ServiceAddressID = CUSTOMERS.Tbl_CustomerPostalAddressDetails.AddressID

GO
UPDATE CUSTOMERS.Tbl_CustomersDetail
SET 
Postal_HouseNo=CUSTOMERS.Tbl_CustomerPostalAddressDetails.HouseNo
,Postal_StreetName=CUSTOMERS.Tbl_CustomerPostalAddressDetails.StreetName
,Postal_City	=CUSTOMERS.Tbl_CustomerPostalAddressDetails.City
,Postal_Landmark=CUSTOMERS.Tbl_CustomerPostalAddressDetails.Landmark
,Postal_AreaCode=CUSTOMERS.Tbl_CustomerPostalAddressDetails.AreaCode
,Postal_ZipCode=CUSTOMERS.Tbl_CustomerPostalAddressDetails.ZipCode

FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails
WHERE CUSTOMERS.Tbl_CustomersDetail.PostalAddressID = CUSTOMERS.Tbl_CustomerPostalAddressDetails.AddressID


GO

------------------- End Alters -----------------------------------------------------

GO
/****** Object:  Table [dbo].[Tbl_EditListReportSummary]    Script Date: 04/18/2015 16:19:27 ******/
SET IDENTITY_INSERT [dbo].[Tbl_EditListReportSummary] ON
INSERT [dbo].[Tbl_EditListReportSummary] ([PageId], [ReportCode], [ReportName], [PagePath], [PageOrder], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'ELR-A001', N'Meter Reding Report', N'../Reports/MeterReadingReport.aspx', 1, 1, N'admin', CAST(0x0000A46B014D11DA AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_EditListReportSummary] ([PageId], [ReportCode], [ReportName], [PagePath], [PageOrder], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'ELR-A002', N'Average Upload Report', N'../Reports/AverageUploadReport.aspx', 2, 1, N'admin', CAST(0x0000A46B014D11DD AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_EditListReportSummary] ([PageId], [ReportCode], [ReportName], [PagePath], [PageOrder], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'ELR-A003', N'Payments Report', N'../Reports/PaymentEditListReport.aspx', 3, 1, N'admin', CAST(0x0000A46B014D11DF AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_EditListReportSummary] ([PageId], [ReportCode], [ReportName], [PagePath], [PageOrder], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'ELR-A004', N'Adjustment Report', N'../Reports/AdjustmentReport.aspx', 4, 1, N'admin', CAST(0x0000A46B014D11E1 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_EditListReportSummary] OFF
