
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit_Rajaiah]    Script Date: 04/18/2015 21:16:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit_Rajaiah]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF ISNULL(@FromDate,'')=''
	BEGIN  
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
	END   
	IF ISNULL(@Todate,'')=''
	BEGIN  
		SET @Todate = dbo.fn_GetCurrentDateTime()  
	END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
	SELECT   CCL.AccountNo  
		,CCL.ApproveStatusId  
		,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
		,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
		,CCL.Remarks  
		,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
		,ApproveSttus.ApprovalStatus  
		,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		,COUNT(0) OVER() AS TotalChanges 
		,BU_ID,SU_ID,ServiceCenterId,CycleId,BookNo,TariffId
		INTO #tmpResults 
	FROM Tbl_CustomerActiveStatusChangeLogs CCL(NOLOCK)    
	INNER JOIN [UDV_CustomerDescription]  CustomerView(NOLOCK)  ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
     
	SELECT
	(  
		SELECT  TOP 10 AccountNo,ApproveStatusId,OldStatus,NewStatus,Remarks,TransactionDate,
		ApprovalStatus,	Name,TotalChanges
		FROM #tmpResults res 
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = res.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = res.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = res.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = res.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = res.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = res.TariffId
	 	FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
    DROP TABLE #tmpResults
END  
----------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_MeterReading]    Script Date: 04/18/2015 21:16:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Jeevan  
-- Create date: 18-04-2015    
-- Description: The purpose of this procedure is to get Books list By Cycle    
-- =============================================    
CREATE PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_MeterReading]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
    
	DECLARE @CycleId VARCHAR(MAX)     
	
	SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')    
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)    
	
	IF(@CycleId != '')    
		BEGIN    
			SELECT    
			(    
				SELECT B.BookNo    
						,(CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN B.BookCode 
							ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')') 
						END) AS BookNoWithDetails
						,COUNT(CPD.BookNo)
				FROM Tbl_BookNumbers B , CUSTOMERS.Tbl_CustomerProceduralDetails CPD
				WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
				AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)
				AND B.ActiveStatusId=1    AND CPD.BookNo=B.BookNo
				GROUP BY 
				B.BookNo,ID,B.BookCode
				HAVING COUNT(CPD.BookNo)>0
				
				ORDER BY B.BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
	ELSE    
		BEGIN    
			SELECT    
			(    
				SELECT (CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN BookCode 
							ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')') 
						END) AS BookNo      
				FROM Tbl_BookNumbers WHERE ActiveStatusId=1    
				ORDER BY BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
     
END


GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllAccNoInfoXLMeterReadings]    Script Date: 04/18/2015 21:17:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	
;With FinalResult AS  
(  
	SELECT Temp.MinReading
		  ,ISNULL(CD.ActiveStatusId,0) AS ActiveStatusId
		  ,CD.OldAccountNo
		  ,ISNULL(CD.GlobalAccountNumber,Temp.AccountNo) AS AccNum
		  --,Temp.AccountNo AS AccNum
	FROM @TempCustomer AS Temp
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail CD ON (CD.GlobalAccountNumber=Temp.AccountNo 
												OR Temp.AccountNo=CD.OldAccountNo)
)

 SELECT  
  (       
  SELECT   
        AccNum
        ,OldAccountNo
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')


--;With FinalResult AS  
--(  
--SELECT     
--	CD.GlobalAccountNumber as AccNum
--	,CD.OldAccountNo AS OldAccountNo
--	,T1.MinReading
--	,CD.ActiveStatusId AS ActiveStatusId
--	--,(SELECT ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=T1.AccountNo OR OldAccountNo=T1.AccountNo) AS ActiveStatusId
-- FROM @TempCustomer T1
--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON  T1.AccountNo=CD.GlobalAccountNumber OR  T1.AccountNo=CD.OldAccountNo
-- LEFT JOIN( SELECT  C.AccountNo AS AccNum
--	,ActiveStatusId    
--    FROM [CUSTOMERS].[Tbl_CustomerSDetail] C     
--     ) B ON T1.AccountNo=B.AccNum    
--)  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading_New]    Script Date: 04/18/2015 21:17:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteekk
-- Create date: 15 Apr 2015  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading_New]  
(
	@MultiXmlDoc XML
)  
AS  
BEGIN  
  
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),       
							 CurrentReading varchar(50),      
							 ReadDate DATETIME)     
            
	INSERT INTO @TempCustomer(AccountNo ,CurrentReading,ReadDate)       
	SELECT       
		  RTRIM(LTRIM(c.value('(Account_Number)[1]','VARCHAR(50)')))       
		  ,(CASE c.value('(Current_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Current_Reading)[1]','VARCHAR(50)') END)      
		  ,CONVERT(DATETIME,c.value('(Read_Date)[1]','varchar(50)'))  
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table') AS T(c)   
	
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.AccountNo
		,CMI.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CMI.Title,CMI.FirstName,CMI.MiddleName,CMI.LastName) as Name
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		--,COUNT(0) AS CustomerCount
	INTO #CustomersListBook
	FROM @TempCustomer T
	INNER JOIN UDV_CustomerMeterInformation CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	--and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	

	SELECT
		(     		
		SELECT 
			 CB.GlobalAccountNumber AS AccNum
			,CB.AccountNo
			,CB.OldAccountNo
			,CB.Name
			,CB.MeterNumber AS MeterNo
			,CB.MeterDials
			,CB.Decimals
			,CB.ActiveStatusId
			,CONVERT(VARCHAR(20),CB.ReadDate,106) AS ReadDate
			,CB.ReadCodeID AS ReadCodeId
			,CB.BU_ID
			,CB.MarketerId
			,(CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - CONVERT(DECIMAL(18,2),ISNULL(CustomerReadings.PresentReading,CustomerReadings.PreviousReading))) AS Usage
			,CB.CurrentReading AS PresentReading
			,ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading
			,ISNULL(CustomerReadings.PresentReading,ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))) AS PreviousReading
			,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
			,CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106) AS LatestDate
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadDate) then 1  
				else 0 end) as PrvExist
			,(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadDate) then 1 
				else 0 end) as IsExists
			,(Case when CustomerReadings.IsBilled IS NULL then 0
				else CustomerReadings.IsBilled end) as IsBilled
			--,CustomerCount
			,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
		FROM #CustomersListBook CB
		LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber
		FOR XML PATH('BillingBE'),TYPE  
		)  
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetPartialBillCustomers]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(max)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(max)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(max)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(max)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
 	Create table #PartialBillCustomers 
 	(
 	 GlobalAccountNumber varchar(50)
 	,OldAccountNo varchar(50)
 	,MeterNo    varchar(50)
 	,ClassName  varchar(50)
 	,Name    varchar(MAX)
 	,ServiceAddress	 varchar(MAX)
 	,BusinessUnitName  varchar(200)
 	,ServiceUnitName	varchar(200)   
 	,ServiceCenterName varchar(200)
 	,CustomerSortOrder int
 	,CycleName	  varchar(100)
 	,BookNumber	 varchar(100)    
 	,BookSortOrder int 
 	,Comments varchar(MAX)   
 	)
 	
 	insert into #PartialBillCustomers
	SELECT	    
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		 ,CustomerFullName AS Name 
	 	,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
	 	,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt CD
	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	
	 
	insert into #PartialBillCustomers
	SELECT 
		 CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 
	AND BD.IsPartialBill = 1
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	
	SELECT * from #PartialBillCustomers
	
	DROP table #PartialBillCustomers
END
---------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_CustomerRegistration]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	CUSTOMER REGISTRATION
-- Modified By: Faiz-ID103
-- Modified Date: 06-Mar-2015
-- Description: Added Initial Reading in Step 10 Custmer Active Details    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters   
-- Modified BY: NEERAJ KANOJIYA-ID77
-- Modified Date: 07-Apr-2015
-- Description: ADDED COMMUNICATION TYPE INPUTS
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_CustomerRegistration]
(   
 @XmlDoc xml  
)  
AS  
BEGIN  
DECLARE
				@GlobalAccountNumber  Varchar(10)
				,@AccountNo  Varchar(50)
				,@TitleTanent varchar(10)
				,@FirstNameTanent	varchar(100)
				,@MiddleNameTanent	varchar(100)
				,@LastNameTanent	varchar(100)
				,@PhoneNumberTanent	varchar(20)
				,@AlternatePhoneNumberTanent 	varchar(20)
				,@EmailIdTanent	varchar(MAX)
				,@TitleLandlord	varchar(10)
				,@FirstNameLandlord	varchar(50)
				,@MiddleNameLandlord	varchar(50)
				,@LastNameLandlord	varchar(50)
				,@KnownAs	varchar(150)
				,@HomeContactNumberLandlord	varchar(20)
				,@BusinessPhoneNumberLandlord 	varchar(20)
				,@OtherPhoneNumberLandlord	varchar(20)
				,@EmailIdLandlord	varchar(MAX)
				,@IsSameAsService	BIT
				,@IsSameAsTenent	BIT
				,@DocumentNo	varchar(50)
				,@ApplicationDate	Datetime
				,@ConnectionDate	Datetime
				,@SetupDate	Datetime
				,@IsBEDCEmployee	BIT
				,@IsVIPCustomer	BIT
				,@OldAccountNo	Varchar(20)
				,@CreatedBy	varchar(50)
				,@CustomerTypeId	INT
				,@BookNo	varchar(30)
				,@PoleID	INT
				,@MeterNumber	Varchar(50)
				,@TariffClassID	INT
				,@IsEmbassyCustomer	BIT
				,@EmbassyCode	Varchar(50)
				,@PhaseId	INT
				,@ReadCodeID	INT
				,@IdentityTypeId	INT
				,@IdentityNumber	VARCHAR(100)
				,@UploadDocumentID	varchar(MAX)
				,@CertifiedBy	varchar(50)
				,@Seal1	varchar(50)
				,@Seal2	varchar(50)
				,@ApplicationProcessedBy	varchar(50)
				,@EmployeeName	varchar(100)
				,@InstalledBy 	INT
				,@AgencyId	INT
				,@IsCAPMI	BIT
				,@MeterAmount	Decimal(18,2)
				,@InitialBillingKWh	BigInt
				,@InitialReading	BigInt
				,@PresentReading	BigInt
				,@StatusText NVARCHAR(max)
				,@CreatedDate DATETIME
				,@PostalAddressID INT
				,@Bedcinfoid INT
				,@ContactName varchar(100)
				,@HouseNoPostal	varchar(50)
				,@StreetPostal Varchar(255)
				,@CityPostaL	varchar(50)
				,@AreaPostal	INT
				,@HouseNoService	varchar(50)
				,@StreetService	varchar(255)
				,@CityService	varchar(50)
				,@AreaService	INT
				,@TenentId INT
				,@ServiceAddressID INT
				,@IdentityTypeIdList	varchar(MAX)
				,@IdentityNumberList	varchar(MAX)
				,@DocumentName 	varchar(MAX)
				,@Path 	varchar(MAX)
				,@IsValid bit=1
				,@EmployeeCode INT
				,@PZipCode VARCHAR(50)
				,@SZipCode VARCHAR(50)
				,@AccountTypeId INT
				,@ClusterCategoryId INT
				,@RouteSequenceNumber INT
				,@Length INT
				,@UDFTypeIdList	varchar(MAX)
				,@UDFValueList	varchar(MAX)
				,@IsSuccessful BIT=1
				,@MGActTypeID INT
				,@IsCommunicationPostal BIT
				,@IsCommunicationService BIT
SELECT 
				 @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')
				,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')
				,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')
				,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')
				,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')
				,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')
				,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')
				,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')
				,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')
				,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')
				,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')
				,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')
				,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')
				,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')
				,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')
				,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')
				,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')
				,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')
				,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')
				,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')
				,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')
				,@SetupDate=C.value('(SetupDate)[1]','Datetime')
				,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')
				,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')
				,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')
				,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')
				,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
				,@BookNo=C.value('(BookNo)[1]','varchar(30)')
				,@PoleID=C.value('(PoleID)[1]','INT')
				,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')
				,@TariffClassID=C.value('(TariffClassID)[1]','INT')
				,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')
				,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')
				,@PhaseId=C.value('(PhaseId)[1]','INT')
				,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')
				,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')
				,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')
				,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')
				,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')
				,@Seal1=C.value('(Seal1)[1]','varchar(50)')
				,@Seal2=C.value('(Seal2)[1]','varchar(50)')
				,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')
				,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')
				,@InstalledBy =C.value('(InstalledBy )[1]','INT')
				,@AgencyId=C.value('(AgencyId)[1]','INT')
				,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')
				,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')
				,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')
				,@InitialReading=C.value('(InitialReading)[1]','BigInt')
				,@PresentReading=C.value('(PresentReading)[1]','BigInt')
				,@HouseNoPostal	=C.value('(	HouseNoPostal	)[1]','	varchar(50)	')
				,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')
				,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')
				,@AreaPostal=C.value('(AreaPostal)[1]','INT')
				,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')
				,@StreetService=C.value('(StreetService)[1]','varchar(255)')
				,@CityService=C.value('(CityService)[1]','varchar(50)')
				,@AreaService=C.value('(AreaService)[1]','INT')
				,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')
				,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')
				,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')
				,@Path=C.value('(Path)[1]','varchar(MAX)')
				,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')
				,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')
				,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')
				,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')
				,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')
				,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')
				,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')
				,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')
				,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')
				,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')
				,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')
				
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  


--||***************************TEMP RESOURCE REMOVAL****************************||--
BEGIN TRY
	DROP TABLE #DocumentList
	DROP TABLE #UDFList
	DROP TABLE #IdentityList
END TRY
BEGIN CATCH

END CATCH
--||****************************************************************************||--
--===========================REGISTRATION STEPS===========================--

--STEP 1	:	GENERATE GLOBAL ACCOUNT NO

--STEP 2	:	GENERATE ACCOUNT NO.

--STEP 3	:	ADD Customer Postal AddressDetails

--STEP 4	:	ADD CustomerTenentDetails.

--STEP 5	:	ADD Customer Details

--STEP 6	:	ADD Customer Procedural Details

--STEP 7	:	ADD Customer Identity Details

--STEP 8	:	ADD Application Process Details

--STEP 9	:	ADD Application Process Person Details

--STEP 10	:	ADD Tbl_CustomerActiveDetails

--STEP 11	:	ADD Tbl_CustomerDocuments

--STEP 12	:	ADD Tbl_USerDefinedValueDetails

--STEP 13	:	ADD Tbl_GovtCustomers

--STEP 14	:	ADD Tbl_Paidmeterdetails

--===========================******************===========================--


--===========================MEMBERS STARTS===========================--+
SET @StatusText='SUCCESS!'

--GENERATE GLOBAL ACCOUNT NUMBER
--||***************************STEP 1****************************||--
BEGIN TRY
    SET @GlobalAccountNumber=CUSTOMERS.fn_GlobalAccountNumberGenerate();
END TRY
BEGIN CATCH
	SET @StatusText='Cannot generate global account no.';
	SET @IsValid=0;
END CATCH

--||***************************STEP 2****************************||--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		DECLARE @BU_ID VARCHAR(50)
				,@SU_ID VARCHAR(50)
				,@ServiceCenterId VARCHAR(50)
		SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId 
		FROM Tbl_BookNumbers BN 
		JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId
		JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
		JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
		JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
		 WHERE BookNo=@BookNo
		 
		SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate 
		--SET @AccountNo='00000001'
	END TRY
	BEGIN CATCH
	SET @IsValid=0;
		SET @StatusText='Cannot generate account no.';
	END CATCH
END
--GET DATE FROM UDF
SET @CreatedDate=dbo.fn_GetCurrentDateTime();

--==========================Preparation of Identity details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
	CREATE TABLE #IdentityList(
								value1 varchar(MAX)
								,value2 varchar(MAX)
								,GlobalAccountNumber VARCHAR(10)
								,CreatedDate DATETIME
								,CreatedBy VARCHAR(50)
								)
	INSERT INTO #IdentityList(
								value1
								,value2
								,GlobalAccountNumber
								,CreatedDate
								,CreatedBy
							)
	select	Value1
			,Value2
			,@GlobalAccountNumber AS GlobalAccountNumber
			,@CreatedDate  AS CreatedDate
			,@CreatedBy AS CreatedBy
	from  [dbo].[fn_splitTwo](@IdentityTypeIdList,@IdentityNumberList,'|')
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare Identity list.';
	END CATCH
END
--select * from  [dbo].[fn_splitTwo]('2|1|3|','456456|24245|645646|','|')
--==========================Preparation of Document details table============================--
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@DocumentName)+ LEN(@Path)		
		CREATE TABLE #DocumentList(
									value1 varchar(MAX)
									,value2 varchar(MAX)
									,GlobalAccountNumber VARCHAR(10)
									,ActiveStatusId INT
									,CreatedDate DATETIME
									,CreatedBy VARCHAR(50)
							)
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #DocumentList(
										GlobalAccountNumber
										,value1
										,value2
										,ActiveStatusId
										,CreatedBy
										,CreatedDate
									)
			select	@GlobalAccountNumber AS GlobalAccountNumber
					,Value1
					,Value2
					,1
					,@CreatedBy AS CreatedBy
					,@CreatedDate  AS CreatedDate
			from  [dbo].[fn_splitTwo](@DocumentName,@Path,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare document list.';
	END CATCH
END
--==========================Preparation of User Define Controls details table============================--
	
IF(@IsValid=1)
BEGIN
	BEGIN TRY
		SET @Length=LEN(@UDFTypeIdList)+ LEN(@UDFValueList)	
		CREATE TABLE #UDFList(
									UDCId	INT
									,Value	VARCHAR(150)
									,GlobalAccountNumber VARCHAR(10)
							)	
		IF(@Length>0)		
		BEGIN		
			INSERT INTO #UDFList(										
									UDCId
									,Value
									,GlobalAccountNumber
									)
			select	
									Value1
									,Value2
									,@GlobalAccountNumber AS GlobalAccountNumber
			from  [dbo].[fn_splitTwo](@UDFTypeIdList,@UDFValueList,'|')
		END
	END TRY
	BEGIN CATCH
		SET @IsValid=0;
		SET @StatusText='Cannot prepare User Define Control list.';
	END CATCH
END
--===========================MEMBERS ENDS===========================--

--===========================REGISTRATION STARTS===========================--
DECLARE @intErrorCode INT
IF(@IsValid=1)
BEGIN
BEGIN TRY
	BEGIN TRAN
		IF(@GlobalAccountNumber IS NOT NULL)
			BEGIN
				IF(@AccountNo IS NOT NULL)
					BEGIN
		--||***************************STEP 3****************************||--
				SET @StatusText='Postal Address';
				INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														,IsCommunication
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoPostal
														,@StreetPostal
														,@CityPostaL
														,@AreaPostal
														,@PZipCode
														,0
														,@CreatedDate
														,@CreatedBy
														,@IsCommunicationPostal
														)
				SET @PostalAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				
				IF(@IsSameAsService=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(
														GlobalAccountNumber
														,HouseNo
														,StreetName
														,City
														,AreaCode
														,ZipCode
														,IsServiceAddress
														,CreatedDate
														,CreatedBy
														,IsCommunication
														)
												VALUES	(
														@GlobalAccountNumber
														,@HouseNoService
														,@StreetService
														,@CityService
														,@AreaService
														,@SZipCode
														,1
														,@CreatedDate
														,@CreatedBy
														,@IsCommunicationService
														)
				SET @ServiceAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)
				END
				ELSE
					BEGIN
						SET @ServiceAddressID =@PostalAddressID
						SET @HouseNoService = @HouseNoPostal
						SET @StreetService = @StreetPostal
						SET @CityService = @CityPostaL
						SET @AreaService = @AreaPostal
						SET @SZipCode = @PZipCode
					END
				
				
				
				--||***************************STEP 4****************************||--
				SET @StatusText='Tenent Address';
				IF(@IsSameAsTenent=0)
				BEGIN
					INSERT INTO CUSTOMERS.Tbl_CustomerTenentDetails(
														GlobalAccountNumber
														,Title
														,FirstName
														,MiddleName
														,LastName
														,PhoneNumber
														,AlternatePhoneNumber
														,EmailID
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@TitleTanent
														,@FirstNameTanent
														,@MiddleNameTanent
														,@LastNameTanent
														,@PhoneNumberTanent
														,@AlternatePhoneNumberTanent
														,@EmailIdTanent
														,@CreatedDate
														,@CreatedBy

														)
					SET @TenentId=SCOPE_IDENTITY();--(SELECT TOP 1 TenentId FROM CUSTOMERS.Tbl_CustomerTenentDetails)
				END
				--||***************************STEP 5****************************||--
				SET @StatusText='Customer Details';
					INSERT INTO CUSTOMERS.Tbl_CustomersDetail (
														GlobalAccountNumber
														,AccountNo
														,Title
														,FirstName
														,MiddleName
														,LastName
														,KnownAs
														,EmailId
														,HomeContactNumber
														,BusinessContactNumber
														,OtherContactNumber
														,IsSameAsService
														,ServiceAddressID
														,PostalAddressID
														,IsSameAsTenent
														,DocumentNo
														,ApplicationDate
														,ConnectionDate
														,SetupDate
														,ActiveStatusId
														,IsBEDCEmployee
														,EmployeeCode
														,IsVIPCustomer
														,TenentId
														,OldAccountNo
														,CreatedDate
														,CreatedBy
														,Service_HouseNo	
														,Service_StreetName	
														,Service_City	
														,Service_AreaCode	
														,Service_ZipCode	
														,Postal_HouseNo	
														,Postal_StreetName	
														,Postal_City	
														,Postal_AreaCode	
														,Postal_ZipCode
														)
												values
														(
														@GlobalAccountNumber
														,@AccountNo
														,@TitleLandlord
														,@FirstNameLandlord
														,@MiddleNameLandlord
														,@LastNameLandlord
														,@KnownAs
														,@EmailIdLandlord
														,@HomeContactNumberLandlord
														,@BusinessPhoneNumberLandlord
														,@OtherPhoneNumberLandlord
														,@IsSameAsService 
														,@ServiceAddressID
														,@PostalAddressID
														,@IsSameAsTenent
														,@DocumentNo
														,@ApplicationDate
														,@ConnectionDate
														,@SetupDate
														,1
														,@IsBEDCEmployee
														,@EmployeeCode
														,@IsVIPCustomer
														,@TenentId
														,@OldAccountNo
														,@CreatedDate
														,@CreatedBy
														,@HouseNoService
														,@StreetService	
														,@CityService	
														,@AreaService	
														,@SZipCode	
														,@HouseNoPostal	
														,@StreetPostal	
														,@CityPostaL	
														,@AreaPostal	
														,@PZipCode
												)
					--||***************************STEP 6****************************||--
					SET @StatusText='Customer Procedural Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerProceduralDetails(
														 GlobalAccountNumber
														,CustomerTypeId
														,AccountTypeId
														,PoleID
														,MeterNumber
														,TariffClassID
														,IsEmbassyCustomer
														,EmbassyCode
														,PhaseId
														,ReadCodeID
														,BookNo
														,ClusterCategoryId
														,RouteSequenceNumber
														,CreatedDate
														,CreatedBy
														,ActiveStatusID
														)
												VALUES	(
														@GlobalAccountNumber
														,@CustomerTypeId
														,@AccountTypeId
														,@PoleID
														,@MeterNumber
														,@TariffClassID
														,@IsEmbassyCustomer
														,@EmbassyCode
														,@PhaseId
														,@ReadCodeID
														,@BookNo
														,@ClusterCategoryId
														,@RouteSequenceNumber
														,@CreatedDate
														,@CreatedBy
														,1
														)
					--||***************************STEP 7****************************||--
					SET @StatusText='Customer identity Details';
					IF EXISTS(SELECT 0 FROM #IdentityList)
					BEGIN
						INSERT INTO CUSTOMERS.Tbl_CustomerIdentityDetails(
															IdentityTypeId
															,IdentityNumber
															,GlobalAccountNumber
															,CreatedDate
															,CreatedBy
															)
						SELECT								*
						FROM #IdentityList
					END
					--||***************************STEP 8****************************||--
					SET @StatusText='Application Process Details';
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessDetails(
														 GlobalAccountNumber
														,CertifiedBy
														,Seal1
														,Seal2
														,ApplicationProcessedBy
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@CertifiedBy
														,@Seal1
														,@Seal2
														,@ApplicationProcessedBy
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 9****************************||--
					SET @StatusText='Application Process Person Details';
					SET @Bedcinfoid=SCOPE_IDENTITY();--(SELECT TOP 1 Bedcinfoid FROM CUSTOMERS.Tbl_ApplicationProcessDetails ORDER BY Bedcinfoid DESC)
					INSERT INTO CUSTOMERS.Tbl_ApplicationProcessPersonDetails(
														GlobalAccountNumber
														,Bedcinfoid
														--,EmployeeName
														,InstalledBy
														,AgencyId
														,ContactName
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@Bedcinfoid
														--,@EmployeeName
														,@InstalledBy
														,@AgencyId
														,@ContactName
														,@CreatedDate
														,@CreatedBy

														)
					--||***************************STEP 10****************************||--
					SET @StatusText='Customer Active Details';
					INSERT INTO CUSTOMERS.Tbl_CustomerActiveDetails(
														GlobalAccountNumber
														,IsCAPMI
														,MeterAmount
														,InitialBillingKWh
														,PresentReading
														,InitialReading--Faiz-ID103
														,CreatedDate
														,CreatedBy
														)
												VALUES	(
														@GlobalAccountNumber
														,@IsCAPMI
														,@MeterAmount
														,@InitialBillingKWh
														,@PresentReading
														,@InitialReading--Faiz-ID103
														,@CreatedDate
														,@CreatedBy
														)
					--||***************************STEP 11****************************||--
					SET @StatusText='Document Details';
					if Exists(SELECT 0 FROM #DocumentList)
					BEGIN
						INSERT INTO Tbl_CustomerDocuments	
															(
															GlobalAccountNo
															,DocumentName
															,[Path]
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
															)
										SELECT				
															GlobalAccountNumber
															,value1
															,value2
															,ActiveStatusId
															,CreatedBy
															,CreatedDate
										FROM  #DocumentList

				 						DROP TABLE #DocumentList
					 END
					--||***************************STEP 12****************************||--
					SET @StatusText='User Define List';
					if Exists(SELECT 0 FROM #UDFList)
					BEGIN
						INSERT INTO Tbl_USerDefinedValueDetails
															(
															AccountNo
															,UDCId
															,Value 
															)
										SELECT				
															GlobalAccountNumber
															,UDCId
															,Value
										FROM #UDFList

					 					
					 END
					 --||***************************STEP 13****************************||--
					SET @StatusText='Tbl_GovtCustomers';
					IF(@MGActTypeID > 0)
					BEGIN
						INSERT INTO Tbl_GovtCustomers
														(
														GlobalAccountNumber
														,MGActTypeID
														)
									values				(
														@GlobalAccountNumber
														,@MGActTypeID			
														)
					END
					 --||***************************STEP 14****************************||-- (Added By - Padmini(25th Feb 2015))
					SET @StatusText='Tbl_Paidmeterdetails';
					IF(@IsCAPMI=1)
					BEGIN
						INSERT INTO Tbl_PaidMeterDetails
														(
														AccountNo
														,MeterNo
														,MeterTypeId
														,MeterCost
														,ActiveStatusId
														,MeterAssignedDate
														,CreatedDate
														,CreatedBy
														)
									values
														(
														@GlobalAccountNumber
														,@MeterNumber
														,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)
														,@MeterAmount
														,1
														,@ConnectionDate
														,@CreatedDate
														,@CreatedBy
														)					
					END
				SET @StatusText	=''
			END
				ELSE
					BEGIN
						SET @GlobalAccountNumber = ''
						SET @IsSuccessful =0
						SET @StatusText='Problem in account number genrate.';
					END
			END
		ELSE
			BEGIN 
				SET @GlobalAccountNumber = ''
				SET @IsSuccessful =0
				SET @StatusText='Problem in global account number genrate.';
			END
	COMMIT TRAN	
END TRY	   
BEGIN CATCH
	ROLLBACK TRAN
	SET @GlobalAccountNumber = ''
	SET @IsSuccessful =0
END CATCH
END

SELECT 
	@IsSuccessful AS IsSuccessful
	,@StatusText AS StatusText 
	,@GlobalAccountNumber AS GolbalAccountNumber			
	FOR XML PATH('CustomerRegistrationBE'),TYPE
--===========================REGISTRATION ENDS===========================--
END


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNonReadCustomers]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''
				,@ActiveStatusIds varchar(max)='2,3,4'

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,[usage] AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE CD.ReadCodeID = 2 AND CR.CustomerReadingId IS NULL 
		AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
		AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
		AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,',')) 
		AND ActiveStatusId=1  and isnull(BoolDisableTypeId,1)= 1 -- Disable books and	   Active Customers

END
--------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomerLedgers_Rajaiah]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 25-09-2014  
-- Modified By: T.Karthik
-- Modified date: 28-10-2014
-- Description: The purpose of this procedure is to Get the Customers Ledger Report
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomerLedgers_Rajaiah]
(  
	@XmlDoc XML  
)  
AS  
BEGIN  
	
	Declare @AcountNo VARCHAR(50)
		,@OldAcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@ReportMonths INT
		,@CurrentDate DATETIME 
		,@BUID VARCHAR(50)=''

	SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()	 

	SELECT  
		  @AcountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@OldAcountNo = C.value('(OldAccountNo)[1]','VARCHAR(50)')   
		  ,@ReportMonths = C.value('(ReportMonths)[1]','INT') 
		  ,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C)  
	
	SELECT @AcountNo = GlobalAccountNumber FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND (BU_ID=@BUID OR @BUID='') AND ActiveStatusId=1

   IF @AcountNo IS NOT NULL
   BEGIN
			DECLARE @ResultTable TABLE(Id INT IDENTITY(1,1),CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))
			DECLARE @FinalResult TABLE(Id INT,CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))

			DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
		    
			INSERT INTO @ResultedMonths
			SELECT [YEAR],[MONTH] FROM DBO.fn_GetCurrentYearMonths_ByCurrentDate(@CurrentDate)-- ORDER BY Id DESC
									
			INSERT INTO @ResultTable (CreateDate,Particulars,Debit,Credit,Balance,Remarks)
			SELECT OpeningDate,'Opening Balance',TotalPendingAmount,0,0,''
			FROM dbo.fn_GetCustomerYear_OpeningBalance(@AcountNo)	
			

			SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
			---- Collection all the transaction Start
			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
			  BEGIN      
				SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
				
				SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
				SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

				INSERT INTO @ResultTable (CreateDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks)
				SELECT CreatedDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks FROM 
				(SELECT CONVERT(DATE,CreatedDate) AS CreatedDate
						,BillNo AS ReferenceNo
						,'Sale of energy' As Particulars
						,0 AS Debit
						,TotalBillAmountWithTax As Credit
						,0 AS Balance
						,'' As Remarks
					FROM Tbl_CustomerBills 
					WHERE AccountNo = @AcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
					--AND BillMonth = @PresentMonth AND BillYear = @PresentYear
				UNION
					SELECT 
						CONVERT(DATE,CreatedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),BillAdjustmentId) AS ReferenceNo
						,'Adjustment' As Particulars
						,0 AS Debit
						,AmountEffected As Credit,0 AS Balance,Remarks
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @AcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				UNION
					SELECT CONVERT(DATE,RecievedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),CustomerPaymentID) AS ReferenceNo
						,(SELECT BillingType FROM Tbl_MBillingTypes WHERE BillingTypeId = CP.ReceivedDevice)+' Payments' As Particulars
						,PaidAmount AS Debit
						,0 As Credit
						,0 AS Balance
						,Remarks
					FROM Tbl_CustomerPayments CP
					WHERE AccountNo = @AcountNo
					AND CONVERT(DATE,RecievedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				) AS A ORDER By CONVERT(DATE,A.CreatedDate)	
				  
				  IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
							IF(@MonthId IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Collection all the transaction END

			---- Finalise the transaction Records Start

			DECLARE @PresentTrans INT 
			SELECT TOP(1) @PresentTrans = Id FROM @ResultTable ORDER BY Id ASC    

			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultTable WHERE Id >= @PresentTrans ORDER BY Id ASC))    
			  BEGIN 
					
					IF NOT EXISTS(SELECT 0 FROM @FinalResult WHERE Id < @PresentTrans)
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 THEN ((0 - Debit)) 
								   WHEN Credit <> 0 THEN (0 + Credit)
								   ELSE 0 END)AS Balance
							,CreateDate
							,Remarks	   
						FROM @ResultTable WHERE Id = @PresentTrans
					END
				ELSE
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) - Debit))
								 WHEN Credit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) + Credit))
									END)AS Balance
							,CreateDate
							,Remarks		
						FROM @ResultTable WHERE Id = @PresentTrans
					END
					
				  IF(@PresentTrans = (SELECT TOP(1) Id FROM @ResultTable ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentTrans = (SELECT TOP(1) Id FROM @ResultTable WHERE  Id > @PresentTrans ORDER BY Id ASC)    
							IF(@PresentTrans IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Finalise the transaction Records End
		    
		   
			 SELECT   
			 (  
				 SELECT 
					 GlobalAccountNumber
					  --,KnownAs AS Name
					  ,(SELECT dbo.fn_GetCustomerFullName(GlobalAccountNumber)) AS Name
					  ,ISNULL(HomeContactNo,ISNULL(BusinessContactNo,OtherContactNo))AS MobileNo
					  ,OldAccountNo
					  ,CD.ClassName 
					  ,CD.BusinessUnitName
					  ,CD.ServiceUnitName
					  ,CD.ServiceCenterName
					  ,CD.BookNo
					  ,CD.CycleName
					  ,CD.MeterNumber
					  ,dbo.fn_GetCustomerServiceAddress(GlobalAccountNumber) AS ServiceAddress
					  ,1 AS IsSuccess
			   FROM [UDV_CustomerDescription] CD
			   WHERE GlobalAccountNumber = @AcountNo
			  FOR XML PATH('CustomerDetails'),TYPE  
			)
			,(
				SELECT 
					Particulars
					,Debit
					,Credit 
					,Balance
					,-1*Balance As Due		
					,CONVERT(VARCHAR(50),CreateDate,106) AS TransactionDate
					,ReferenceNo
					,Remarks
				FROM @FinalResult 
				--ORDER BY Id DESC
			  FOR XML PATH('CustomerLedger'),TYPE  
			 )  
			 FOR XML PATH(''),ROOT('RptCustomerLedgerInfoByXml')  
	END
    ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND BU_ID!=@BUID AND ActiveStatusId=1)
    BEGIN
		SELECT
		(
			SELECT 1 AS IsSuccess,1 AS IsCustExistsInOtherBU
		FOR XML PATH('CustomerDetails'),TYPE  
		)  
		FOR XML PATH(''),ROOT('RptCustomerLedgerInfoByXml')
    END
    ELSE
    BEGIN
		SELECT
		(
			SELECT 0 AS IsSuccess
		FOR XML PATH('CustomerDetails'),TYPE  
		)  
		FOR XML PATH(''),ROOT('RptCustomerLedgerInfoByXml')
    END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit_Rajaiah]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit_Rajaiah]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF ISNULL(@FromDate,'')=''
	BEGIN  
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
	END   
	IF ISNULL(@Todate,'')=''
	BEGIN  
		SET @Todate = dbo.fn_GetCurrentDateTime()  
	END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
	SELECT   CCL.AccountNo  
		,CCL.ApproveStatusId  
		,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
		,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
		,CCL.Remarks  
		,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
		,ApproveSttus.ApprovalStatus  
		,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		,COUNT(0) OVER() AS TotalChanges 
		,BU_ID,SU_ID,ServiceCenterId,CycleId,BookNo,TariffId
		INTO #tmpResults 
	FROM Tbl_CustomerActiveStatusChangeLogs CCL(NOLOCK)    
	INNER JOIN [UDV_CustomerDescription]  CustomerView(NOLOCK)  ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
     
	SELECT
	(  
		SELECT  TOP 10 AccountNo,ApproveStatusId,OldStatus,NewStatus,Remarks,TransactionDate,
		ApprovalStatus,	Name,TotalChanges
		FROM #tmpResults res 
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = res.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = res.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = res.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = res.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = res.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = res.TariffId
	 	FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
    DROP TABLE #tmpResults
END  
----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		IF	@Usage >0
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@IsRollover)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage+1
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@IsRollover)
									
				END
		END
		
 
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT
		,@Rollover BIT
		,@Previous varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@Count = C.value('(TotalReadings)[1]','INT')
		,@Avg = C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@Rollover = C.value('(Rollover)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@Previous = C.value('(PreviousReading)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @ReadId INT
	Declare	 @Dials INT
	Declare	  @Multipler INT
	Declare @IsExistingRollOver bit
	Declare	   @MeterNumber varchar(50),@FirstPrevReading varchar(50),@FirstPresentValue varchar(50) 
	 ,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
	 
		,@SecoundReadingPrevReading varchar(20)
		,@AverageReading VARCHAR(50)
		 
	SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=IsRollOver,@MeterNumber=MeterNumber
	,@Dials	= LEN(PreviousReading) ,@ReadDate=ReadDate 
	FROM Tbl_CustomerReadings
	WHERE GlobalAccountNumber = @AccNum
	ORDER BY CustomerReadingId DESC

		 
	IF @IsExistingRollOver = 1
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
	END
	ELSE
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId    
	END
	 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
		 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	Declare @FirstReadingUsage bigint =@Usage
	Declare @SecountReadingUsage Bigint =0
	SET @FirstPrevReading   = @Previous
	SET @FirstPresentValue    =	 @Current
	
	IF @Rollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
	--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		 
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@Rollover
		)
	IF(@Rollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage+1
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@Rollover)
									
					 
				END
			
 
		END
	
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum
	

	 

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
	   
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)

		  Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
		 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExist 
		,ReadDate   as  LatestDate 
		,AverageReading 
		,TotalReadings  
		,PreviousReading
		,PresentReading 
		,IsBilled
		,MeterNumber   as  ReadingsMeterNumber
		,IsRollOver
		,GlobalAccountNumber
		,CustomerReadingId
		INTO #CustomerTopTwoReading
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC
		
		IF ((select COUNT(0) from #CustomerTopTwoReading where IsRollOver=1) > 1)
			BEGIN

				SELECT  
					@usage=Sum(Usage),
					@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
					@IsExists = max(case when IsExist=0 then 0 else 1 end)
					,@LatestDate=max(LatestDate) 
					,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
					,@TotalReadings=max(TotalReadings)  
					,@PreviousReading = max(PreviousReading)
					,@PresentReading=min(PresentReading) 
					,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
					,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
					,@IsRollOver =1 
				FROM #CustomerTopTwoReading  AS TopTwoReadings
				Group By GlobalAccountNumber

			END
		ELSE
			BEGIN
					 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
						@IsExists = IsExist
					,@LatestDate=LatestDate 
					,@AverageReading=AverageReading 
					,@TotalReadings=TotalReadings  
					,@PreviousReading = PreviousReading
					,@PresentReading=PresentReading 
					,@IsBilled=IsBilled
					,@ReadingsMeterNumber=ReadingsMeterNumber 
					,@IsRollOver =0
					from #CustomerTopTwoReading
			END
		
		DROP table		#CustomerTopTwoReading

		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		SELECT TOP(1) @Month = BillMonth,
		@Year = BillYear ,@LastBillReadType= 
		(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @OldAcountNumber AS OldAccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,case when @MeterNumber=@ReadingsMeterNumber then  @IsExists	else 0 End   as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,(Case when CONVERT(DATE,@ReadDate) >= CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
				 ,(Case when CONVERT(DATE,@ReadDate) >= CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,@InititalReading as	 InititalReading
				 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  


GO


/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  NEERAJ KANOJIYA        
-- Create date: 1/APRIL/2015        
-- Description: This Procedure is used to change customer address       
-- Modified BY: Faiz-ID103  
-- Modified Date: 07-Apr-2015  
-- Description: changed the street field value from 200 to 255 characters      
-- Modified BY: Faiz-ID103  
-- Modified Date: 16-Apr-2015  
-- Description: Add the condition for IsBookNoChanged
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray
-- =============================================      
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]        
(        
 @XmlDoc xml           
)        
AS        
BEGIN      
 DECLARE @TotalAddress INT     
   ,@IsSameAsServie BIT    
   ,@GlobalAccountNo   VARCHAR(50)          
     ,@NewPostalLandMark  VARCHAR(200)        
     ,@NewPostalStreet   VARCHAR(255)        
     ,@NewPostalCity   VARCHAR(200)        
     ,@NewPostalHouseNo   VARCHAR(200)        
     ,@NewPostalZipCode   VARCHAR(50)        
     ,@NewServiceLandMark  VARCHAR(200)        
     ,@NewServiceStreet   VARCHAR(255)        
     ,@NewServiceCity   VARCHAR(200)        
     ,@NewServiceHouseNo  VARCHAR(200)        
     ,@NewServiceZipCode  VARCHAR(50)        
     ,@NewPostalAreaCode  VARCHAR(50)        
     ,@NewServiceAreaCode  VARCHAR(50)        
     ,@NewPostalAddressID  INT        
     ,@NewServiceAddressID  INT       
     ,@ModifiedBy    VARCHAR(50)        
     ,@Details     VARCHAR(MAX)        
     ,@RowsEffected    INT        
     ,@AddressID INT       
     ,@StatusText VARCHAR(50)    
     ,@IsCommunicationPostal BIT    
     ,@IsCommunicationService BIT    
     ,@ApprovalStatusId INT=2      
     ,@PostalAddressID INT    
     ,@ServiceAddressID INT    
     ,@BookNo VARCHAR(50)    
       SELECT           
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')        
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')            
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')            
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')            
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')           
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')          
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')            
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')            
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')            
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')           
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')        
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')        
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')        
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')        
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')        
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')        
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')        
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')    
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')        
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')    
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')    
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
BEGIN TRY    
  BEGIN TRAN     
    --UPDATE POSTAL ADDRESS    
 SET @StatusText='Total address count.'       
 SET @TotalAddress=(SELECT COUNT(0)     
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails     
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1    
        )    
            
 --=====================================================================================            
 --Update book number of customer.    
 --=====================================================================================     
Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)
IF(@ExistingBookNo!=@BookNo)
BEGIN
	
	DECLARE @OldBookNo VARCHAR(50)
	SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)

		INSERT INTO Tbl_BookNoChangeLogs( AccountNo
										--,CustomerUniqueNo
										,OldBookNo
										,NewBookNo
										,CreatedBy
										,CreatedDate
										,ApproveStatusId
										,Remarks)
		VALUES(@GlobalAccountNo,@OldBookNo,@BookNo,@ModifiedBy,dbo.fn_GetCurrentDateTime(),@ApprovalStatusId,@Details)	
			
		UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails     
		SET BookNo=@BookNo,IsBookNoChanged=1     
		WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1
END

--================== Logs For Addres Change        
  IF (@IsSameAsServie =0)
		BEGIN		
			INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
	ELSE
		BEGIN
			INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
		
 --=====================================================================================            
 --Customer has only one addres and wants to update the address. //CONDITION 1//    
 --=====================================================================================        
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)    
 BEGIN    
  SET @StatusText='CONDITION 1'
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END        
 --=====================================================================================    
 --Customer has one addres and wants to add new address. //CONDITION 2//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 2'
 
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(    
                HouseNo    
                ,LandMark    
                ,StreetName    
                ,City    
                ,AreaCode    
                ,ZipCode    
                ,ModifedBy    
                ,ModifiedDate    
                ,IsCommunication    
                ,IsServiceAddress    
                ,IsActive    
                ,GlobalAccountNumber    
                )    
               VALUES (@NewServiceHouseNo           
                ,@NewServiceLandMark           
                ,@NewServiceStreet            
                ,@NewServiceCity          
                ,@NewServiceAreaCode          
             ,@NewServiceZipCode            
                ,@ModifiedBy          
                ,dbo.fn_GetCurrentDateTime()      
                ,@IsCommunicationService             
                ,1    
                ,1    
                ,@GlobalAccountNo    
                )     
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo                   
 END       
     
 --=====================================================================================    
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)     
 BEGIN    
  SET @StatusText='CONDITION 3'
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    IsActive=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewPostalLandMark    
    ,Service_StreetName=@NewPostalStreet    
    ,Service_City=@NewPostalCity    
    ,Service_HouseNo=@NewPostalHouseNo    
    ,Service_ZipCode=@NewPostalZipCode    
    ,Service_AreaCode=@NewPostalAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@PostalAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=1    
  WHERE GlobalAccountNumber=@GlobalAccountNo    
 END      
 --=====================================================================================    
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//    
 --=====================================================================================    
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)    
 BEGIN    
  SET @StatusText='CONDITION 4'

  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END          
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END          
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END          
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END          
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END            
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END          
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationPostal         
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1     
      
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET          
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END          
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END          
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END          
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END          
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END            
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END             
      ,ModifedBy=@ModifiedBy          
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()       
      ,IsCommunication=@IsCommunicationService    
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1    
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)    
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)    
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET    
    Service_Landmark=@NewServiceLandMark    
    ,Service_StreetName=@NewServiceStreet    
    ,Service_City=@NewServiceCity    
    ,Service_HouseNo=@NewServiceHouseNo    
    ,Service_ZipCode=@NewServiceZipCode    
    ,Service_AreaCode=@NewServiceAreaCode    
    ,Postal_Landmark=@NewPostalLandMark    
    ,Postal_StreetName=@NewPostalStreet    
    ,Postal_City=@NewPostalCity    
    ,Postal_HouseNo=@NewPostalHouseNo    
    ,Postal_ZipCode=@NewPostalZipCode    
    ,Postal_AreaCode=@NewPostalAreaCode    
    ,ServiceAddressID=@ServiceAddressID    
    ,PostalAddressID=@PostalAddressID    
    ,IsSameAsService=0    
  WHERE GlobalAccountNumber=@GlobalAccountNo       
 END      
 COMMIT TRAN    
END TRY    
BEGIN CATCH    
 ROLLBACK TRAN    
 SET @RowsEffected=0    
END CATCH    
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')             
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT   
   ,@IsSameAsServie BIT  
   ,@GlobalAccountNo   VARCHAR(50)        
     ,@NewPostalLandMark  VARCHAR(200)      
     ,@NewPostalStreet   VARCHAR(255)      
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT  
       SELECT         
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')      
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)      
BEGIN TRY  
  BEGIN TRAN   
    --UPDATE POSTAL ADDRESS  
 SET @StatusText='Total address count.'     
 SET @TotalAddress=(SELECT COUNT(0)   
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
        )
        
--================== Logs For Addres Change        
  IF (@IsSameAsServie =0)
		BEGIN		
			INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewServiceHouseNo
														,@NewServiceStreet
														,@NewServiceCity
														,NULL
														,@NewServiceAreaCode
														,@NewServiceZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
	ELSE
		BEGIN
			INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
														,OldPostal_HouseNo
														,OldPostal_StreetName
														,OldPostal_City
														,OldPostal_Landmark
														,OldPostal_AreaCode
														,OldPostal_ZipCode
														,OldService_HouseNo
														,OldService_StreetName
														,OldService_City
														,OldService_Landmark
														,OldService_AreaCode
														,OldService_ZipCode
														,NewPostal_HouseNo
													    ,NewPostal_StreetName
													    ,NewPostal_City
													    ,NewPostal_Landmark
													    ,NewPostal_AreaCode
													    ,NewPostal_ZipCode
													    ,NewService_HouseNo
													    ,NewService_StreetName
													    ,NewService_City
													    ,NewService_Landmark
													    ,NewService_AreaCode
													    ,NewService_ZipCode
													    ,ApprovalStatusId
													    ,Remarks
													    ,CreatedBy
													    ,CreatedDate
													)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
												FROM CUSTOMERS.Tbl_CustomerSDetail
												WHERE GlobalAccountNumber=@GlobalAccountNo
		END
 --=====================================================================================          
 --Customer has only one addres and wants to update the address. //CONDITION 1//  
 --=====================================================================================   
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
 BEGIN  
  SET @StatusText='CONDITION 1'
 
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END      
 --=====================================================================================  
 --Customer has one addres and wants to add new address. //CONDITION 2//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 2'
												 
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
                HouseNo  
                ,LandMark  
                ,StreetName  
                ,City  
                ,AreaCode  
                ,ZipCode  
                ,ModifedBy  
                ,ModifiedDate  
                ,IsCommunication  
                ,IsServiceAddress  
                ,IsActive  
                ,GlobalAccountNumber  
                )  
               VALUES (@NewServiceHouseNo         
                ,@NewServiceLandMark         
                ,@NewServiceStreet          
                ,@NewServiceCity        
                ,@NewServiceAreaCode        
                ,@NewServiceZipCode          
                ,@ModifiedBy        
                ,dbo.fn_GetCurrentDateTime()    
                ,@IsCommunicationService           
                ,1  
                ,1  
                ,@GlobalAccountNo  
                )   
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo                 
 END     
   
 --=====================================================================================  
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
 BEGIN  
  SET @StatusText='CONDITION 3'  
     
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    IsActive=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewPostalLandMark  
    ,Service_StreetName=@NewPostalStreet  
    ,Service_City=@NewPostalCity  
    ,Service_HouseNo=@NewPostalHouseNo  
    ,Service_ZipCode=@NewPostalZipCode  
    ,Service_AreaCode=@NewPostalAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@PostalAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=1  
  WHERE GlobalAccountNumber=@GlobalAccountNo  
 END    
 --=====================================================================================  
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
 --=====================================================================================  
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
 BEGIN  
  SET @StatusText='CONDITION 4'
       
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationPostal       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
      ,ModifedBy=@ModifiedBy        
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
      ,IsCommunication=@IsCommunicationService  
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
    Service_Landmark=@NewServiceLandMark  
    ,Service_StreetName=@NewServiceStreet  
    ,Service_City=@NewServiceCity  
    ,Service_HouseNo=@NewServiceHouseNo  
    ,Service_ZipCode=@NewServiceZipCode  
    ,Service_AreaCode=@NewServiceAreaCode  
    ,Postal_Landmark=@NewPostalLandMark  
    ,Postal_StreetName=@NewPostalStreet  
    ,Postal_City=@NewPostalCity  
    ,Postal_HouseNo=@NewPostalHouseNo  
    ,Postal_ZipCode=@NewPostalZipCode  
    ,Postal_AreaCode=@NewPostalAreaCode  
    ,ServiceAddressID=@ServiceAddressID  
    ,PostalAddressID=@PostalAddressID  
    ,IsSameAsService=0  
  WHERE GlobalAccountNumber=@GlobalAccountNo     
 END    
 COMMIT TRAN  
END TRY  
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @RowsEffected=0  
END CATCH  
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')           
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_MeterReading]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Jeevan  
-- Create date: 18-04-2015    
-- Description: The purpose of this procedure is to get Books list By Cycle    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_MeterReading]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
    
	DECLARE @CycleId VARCHAR(MAX)     
	
	SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')    
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)    
	
	IF(@CycleId != '')    
		BEGIN    
			SELECT    
			(    
				SELECT B.BookNo    
						,(CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN B.BookCode 
							ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')') 
						END) AS BookNoWithDetails
						,COUNT(CPD.BookNo)
				FROM Tbl_BookNumbers B , CUSTOMERS.Tbl_CustomerProceduralDetails CPD
				WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
				AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)
				AND B.ActiveStatusId=1    AND CPD.BookNo=B.BookNo
				GROUP BY 
				B.BookNo,ID,B.BookCode
				HAVING COUNT(CPD.BookNo)>0
				
				ORDER BY B.BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
	ELSE    
		BEGIN    
			SELECT    
			(    
				SELECT (CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN BookCode 
							ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')') 
						END) AS BookNo      
				FROM Tbl_BookNumbers WHERE ActiveStatusId=1    
				ORDER BY BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
     
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
     
	--SELECT(  
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy  
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 -- changed by karteek end    
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetReadCustomers]    Script Date: 04/18/2015 21:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



 
-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

Modified Date : 09-04-2015

Description : 
In Active,Closed,Hold,Tempary CLose  Customers ,DIRECT Customer , NoPower Also- No Need to Come

 Hold -- 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

 
	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
			,@ActiveStatusIds varchar(max)='2,3,4'
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS BIGINT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_PreBillingRpt CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	AND ActiveStatusId=1  and isnull(BoolDisableTypeId,0)= 0-- Disable books and	   Active Customers
	AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy


	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	
	DROP TABLE #ReadCustomersList

END
----------------------------------------------------------------------------------------------




GO


