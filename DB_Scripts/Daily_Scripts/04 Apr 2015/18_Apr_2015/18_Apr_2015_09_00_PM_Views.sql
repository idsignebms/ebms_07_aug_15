
GO

/****** Object:  View [dbo].[UDV_SearchCustomer]    Script Date: 04/18/2015 21:13:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [dbo].[UDV_SearchCustomer]     
AS    
   
 SELECT    DISTINCT
  CPD.GlobalAccountNumber,  
  CD.AccountNo,  
  CD.OldAccountNo,  
  dbo.fn_GetCustomerFullName_New(   
    CD.Title,    
    CD.FirstName,    
    CD.MiddleName,    
    CD.LastName) AS Name,   
  CD.HomeContactNumber,  
  BusinessContactNumber,  
  CD.OtherContactNumber,   
  BookDetails.BusinessUnitName,  
  Dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,Service_City,'',Service_ZipCode) As ServiceAddress,  
  BookDetails.ServiceUnitName,  
  BookDetails.ServiceCenterName,  
  TariffClasses.ClassName,     
  CStatus.StatusName,  
  CPD.IsBookNoChanged,  
  CPD.MeterNumber,  
  CD.ActiveStatusId  
  ,BookDetails.BU_ID  
  ,BookDetails.SU_ID  
  ,BookDetails.ServiceCenterId  
  ,BookDetails.CycleId  
  ,CPD.TariffClassID as TariffId  
  ,ReadCodeId  
  ,CAD.CreatedDate  
  ,BookDetails.BookCode  
  ,BookDetails.BookSortOrder  
  ,CPD.SortOrder  
  ,CAD.InitialReading  
 FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD    
 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN  dbo.UDV_BookNumberDetails(NOLOCK) BookDetails ON BookDetails.BookNo=CPD.BookNo  
 INNER JOIN  Tbl_MCustomerStatus(NOLOCK) CStatus ON CStatus.StatusId=CD.ActiveStatusId  
 INNER JOIN  Tbl_MTariffClasses(NOLOCK) TariffClasses ON TariffClasses.ClassID=CPD.TariffClassID  
    
  
  
  
  
  
  



GO

/****** Object:  View [dbo].[UDV_BookNumberDetails]    Script Date: 04/18/2015 21:13:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




  
  
ALTER VIEW [dbo].[UDV_BookNumberDetails]  
AS  
 SELECT   
 BU.BU_ID,BU.BUCode,BU.BusinessUnitName  
 ,SU.SU_ID,SU.SUCode,SU.ServiceUnitName  
 ,SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName  
 ,C.CycleId,C.CycleCode,C.CycleName  
 ,B.BookNo,B.BookCode,B.ID
 ,M.MarketerId
 ,SortOrder as BookSortOrder
 ,dbo.fn_GetMarketersFullName_New(M.FirstName,M.MiddleName,M.LastName) AS MarketerName   
 FROM Tbl_BookNumbers B  
 JOIN Tbl_Cycles C ON C.CycleId=B.CycleId  
 JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
 JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
 JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
 JOIN Tbl_Marketers M ON M.MarketerId = B.MarketerId



GO


