
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadings_Bulk]    Script Date: 04/20/2015 20:16:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 20-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
CREATE PROCEDURE  [dbo].[USP_InsertMeterReadings_Bulk]
(
	 @XmlDoc Xml
	,@MultiXmlDoc Xml
)	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@MeterReadingFrom INT
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess INT = 0

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), TotalReadings INT, AverageReading VARCHAR(50),           
							PresentReading VARCHAR(50), Usage VARCHAR(50), AccountNo VARCHAR(50),
							IsTamper BIT, PreviousReading VARCHAR(50), IsExists BIT)
	
	INSERT INTO @TempCustomer(TotalReadings, AverageReading, PresentReading, Usage, AccountNo, IsTamper, PreviousReading, IsExists)           
	SELECT         
		 c.value('(TotalReadings)[1]','INT')
		,c.value('(AverageReading)[1]','VARCHAR(50)')
		,c.value('(PresentReading)[1]','VARCHAR(50)')
		,CONVERT(NUMERIC(20,4),c.value('(Usage)[1]','VARCHAR(50)'))
		,c.value('(AccNum)[1]','varchar(50)')
		,c.value('(IsTamper)[1]','BIT')
		,c.value('(PreviousReading)[1]','varchar(50)')
		,c.value('(IsExists)[1]','BIT')
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c) 
	
	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = Usage
				,@AccountNo = AccountNo
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = @IsExists
			 FROM @TempCustomer WHERE SNo = @SNo
			
			SELECT @Multiple = MI.MeterMultiplier
				,@MeterNumber = CPD.MeterNumber
				,@Dials=MI.MeterDials 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
			INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccountNo
			
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PreviousReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = @IsSuccess + 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			
		END
		
	SELECT @IsSuccess AS IsSuccess 
	FOR XML PATH('BillingBE')
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetOpenMonthAndYear]    Script Date: 04/20/2015 20:16:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 20-Apr-2015
-- Description:	To get the Open Month and Year
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetOpenMonthAndYear]	
AS
BEGIN
	SELECT	[Month]
			,[Year]
			,dbo.fn_GetMonthName([Month]) AS [MonthName]
	FROM	Tbl_BillingMonths
	WHERE OpenStatusId=1
	FOR XML PATH ('EstimationBE'), TYPE
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadings_Bulk]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 20-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertMeterReadings_Bulk]
(
	 @XmlDoc Xml
	,@MultiXmlDoc Xml
)	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@MeterReadingFrom INT
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess INT = 0

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), TotalReadings INT, AverageReading VARCHAR(50),           
							PresentReading VARCHAR(50), Usage VARCHAR(50), AccountNo VARCHAR(50),
							IsTamper BIT, PreviousReading VARCHAR(50), IsExists BIT)
	
	INSERT INTO @TempCustomer(TotalReadings, AverageReading, PresentReading, Usage, AccountNo, IsTamper, PreviousReading, IsExists)           
	SELECT         
		 c.value('(TotalReadings)[1]','INT')
		,c.value('(AverageReading)[1]','VARCHAR(50)')
		,c.value('(PresentReading)[1]','VARCHAR(50)')
		,CONVERT(NUMERIC(20,4),c.value('(Usage)[1]','VARCHAR(50)'))
		,c.value('(AccNum)[1]','varchar(50)')
		,c.value('(IsTamper)[1]','BIT')
		,c.value('(PreviousReading)[1]','varchar(50)')
		,c.value('(IsExists)[1]','BIT')
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c) 
	
	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = Usage
				,@AccountNo = AccountNo
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = @IsExists
			 FROM @TempCustomer WHERE SNo = @SNo
			
			SELECT @Multiple = MI.MeterMultiplier
				,@MeterNumber = CPD.MeterNumber
				,@Dials=MI.MeterDials 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
			INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccountNo
			
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PreviousReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = @IsSuccess + 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			
		END
		
	SELECT @IsSuccess AS IsSuccess 
	FOR XML PATH('BillingBE')
	
END
GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_CustomerRegistration]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 22-DEC-2014  
-- Description: CUSTOMER REGISTRATION  
-- Modified By: Faiz-ID103  
-- Modified Date: 06-Mar-2015  
-- Description: Added Initial Reading in Step 10 Custmer Active Details      
-- Modified BY: Faiz-ID103  
-- Modified Date: 07-Apr-2015  
-- Description: changed the street field value from 200 to 255 characters     
-- Modified BY: NEERAJ KANOJIYA-ID77  
-- Modified Date: 07-Apr-2015  
-- Description: ADDED COMMUNICATION TYPE INPUTS  
-- =============================================  
ALTER PROCEDURE [CUSTOMERS].[USP_CustomerRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN    
DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
      
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
  
--||***************************TEMP RESOURCE REMOVAL****************************||--  
BEGIN TRY  
 DROP TABLE #DocumentList  
 DROP TABLE #UDFList  
 DROP TABLE #IdentityList  
END TRY  
BEGIN CATCH  
  
END CATCH  
--||****************************************************************************||--  
--===========================REGISTRATION STEPS===========================--  
  
--STEP 1 : GENERATE GLOBAL ACCOUNT NO  
  
--STEP 2 : GENERATE ACCOUNT NO.  
  
--STEP 3 : ADD Customer Postal AddressDetails  
  
--STEP 4 : ADD CustomerTenentDetails.  
  
--STEP 5 : ADD Customer Details  
  
--STEP 6 : ADD Customer Procedural Details  
  
--STEP 7 : ADD Customer Identity Details  
  
--STEP 8 : ADD Application Process Details  
  
--STEP 9 : ADD Application Process Person Details  
  
--STEP 10 : ADD Tbl_CustomerActiveDetails  
  
--STEP 11 : ADD Tbl_CustomerDocuments  
  
--STEP 12 : ADD Tbl_USerDefinedValueDetails  
  
--STEP 13 : ADD Tbl_GovtCustomers  
  
--STEP 14 : ADD Tbl_Paidmeterdetails  
  
--===========================******************===========================--  
  
  
--===========================MEMBERS STARTS===========================--+  
SET @StatusText='SUCCESS!'  
  
--GENERATE GLOBAL ACCOUNT NUMBER  
--||***************************STEP 1****************************||--  
BEGIN TRY  
    SET @GlobalAccountNumber=CUSTOMERS.fn_GlobalAccountNumberGenerate();  
END TRY  
BEGIN CATCH  
 SET @StatusText='Cannot generate global account no.';  
 SET @IsValid=0;  
END CATCH  
  
--||***************************STEP 2****************************||--  
IF(@IsValid=1)  
BEGIN  
 BEGIN TRY  
  DECLARE @BU_ID VARCHAR(50)  
    ,@SU_ID VARCHAR(50)  
    ,@ServiceCenterId VARCHAR(50)  
  SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId   
  FROM Tbl_BookNumbers BN   
  JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
   WHERE BookNo=@BookNo  
     
  SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo); --- New Formate   
  --SET @AccountNo='00000001'  
 END TRY  
 BEGIN CATCH  
 SET @IsValid=0;  
  SET @StatusText='Cannot generate account no.';  
 END CATCH  
END  
--GET DATE FROM UDF  
SET @CreatedDate=dbo.fn_GetCurrentDateTime();  
  
--==========================Preparation of Identity details table============================--  
IF(@IsValid=1)  
BEGIN  
 BEGIN TRY  
 CREATE TABLE #IdentityList(  
        value1 varchar(MAX)  
        ,value2 varchar(MAX)  
        ,GlobalAccountNumber VARCHAR(10)  
        ,CreatedDate DATETIME  
        ,CreatedBy VARCHAR(50)  
        )  
 INSERT INTO #IdentityList(  
        value1  
        ,value2  
        ,GlobalAccountNumber  
        ,CreatedDate  
        ,CreatedBy  
       )  
 select Value1  
   ,Value2  
   ,@GlobalAccountNumber AS GlobalAccountNumber  
   ,@CreatedDate  AS CreatedDate  
   ,@CreatedBy AS CreatedBy  
 from  [dbo].[fn_splitTwo](@IdentityTypeIdList,@IdentityNumberList,'|')  
 END TRY  
 BEGIN CATCH  
  SET @IsValid=0;  
  SET @StatusText='Cannot prepare Identity list.';  
 END CATCH  
END  
--select * from  [dbo].[fn_splitTwo]('2|1|3|','456456|24245|645646|','|')  
--==========================Preparation of Document details table============================--  
IF(@IsValid=1)  
BEGIN  
 BEGIN TRY  
  SET @Length=LEN(@DocumentName)+ LEN(@Path)    
  CREATE TABLE #DocumentList(  
         value1 varchar(MAX)  
         ,value2 varchar(MAX)  
         ,GlobalAccountNumber VARCHAR(10)  
         ,ActiveStatusId INT  
         ,CreatedDate DATETIME  
         ,CreatedBy VARCHAR(50)  
       )  
  IF(@Length>0)    
  BEGIN    
   INSERT INTO #DocumentList(  
          GlobalAccountNumber  
          ,value1  
          ,value2  
          ,ActiveStatusId  
          ,CreatedBy  
          ,CreatedDate  
         )  
   select @GlobalAccountNumber AS GlobalAccountNumber  
     ,Value1  
     ,Value2  
     ,1  
     ,@CreatedBy AS CreatedBy  
     ,@CreatedDate  AS CreatedDate  
   from  [dbo].[fn_splitTwo](@DocumentName,@Path,'|')  
  END  
 END TRY  
 BEGIN CATCH  
  SET @IsValid=0;  
  SET @StatusText='Cannot prepare document list.';  
 END CATCH  
END  
--==========================Preparation of User Define Controls details table============================--  
   
IF(@IsValid=1)  
BEGIN  
 BEGIN TRY  
  SET @Length=LEN(@UDFTypeIdList)+ LEN(@UDFValueList)   
  CREATE TABLE #UDFList(  
         UDCId INT  
         ,Value VARCHAR(150)  
         ,GlobalAccountNumber VARCHAR(10)  
       )   
  IF(@Length>0)    
  BEGIN    
   INSERT INTO #UDFList(            
         UDCId  
         ,Value  
         ,GlobalAccountNumber  
         )  
   select   
         Value1  
         ,Value2  
         ,@GlobalAccountNumber AS GlobalAccountNumber  
   from  [dbo].[fn_splitTwo](@UDFTypeIdList,@UDFValueList,'|')  
  END  
 END TRY  
 BEGIN CATCH  
  SET @IsValid=0;  
  SET @StatusText='Cannot prepare User Define Control list.';  
 END CATCH  
END  
--===========================MEMBERS ENDS===========================--  
  
--===========================REGISTRATION STARTS===========================--  
DECLARE @intErrorCode INT  
IF(@IsValid=1)  
BEGIN  
BEGIN TRY  
 BEGIN TRAN  
  IF(@GlobalAccountNumber IS NOT NULL)  
   BEGIN  
    IF(@AccountNo IS NOT NULL)  
     BEGIN  
  --||***************************STEP 3****************************||--  
    SET @StatusText='Postal Address';  
    INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
              GlobalAccountNumber  
              ,HouseNo  
              ,StreetName  
              ,City  
              ,AreaCode  
              ,ZipCode  
              ,IsServiceAddress  
              ,CreatedDate  
              ,CreatedBy  
              ,IsCommunication  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@HouseNoPostal  
              ,@StreetPostal  
              ,@CityPostaL  
              ,CASE WHEN (@AreaPostal=0) THEN NULL ELSE @AreaPostal END  
              ,@PZipCode  
              ,0  
              ,@CreatedDate  
              ,@CreatedBy  
              ,@IsCommunicationPostal  
              )  
    SET @PostalAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)  
      
    IF(@IsSameAsService=0)  
    BEGIN  
     INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
              GlobalAccountNumber  
              ,HouseNo  
              ,StreetName  
              ,City  
              ,AreaCode  
              ,ZipCode  
              ,IsServiceAddress  
              ,CreatedDate  
              ,CreatedBy  
              ,IsCommunication  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@HouseNoService  
              ,@StreetService  
              ,@CityService  
              --,@AreaService  
              ,CASE WHEN (@AreaService=0) THEN NULL ELSE @AreaService END
              ,@SZipCode  
              ,1  
              ,@CreatedDate  
              ,@CreatedBy  
              ,@IsCommunicationService  
              )  
    SET @ServiceAddressID=SCOPE_IDENTITY();--(SELECT TOP 1 AddressID FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails ORDER BY AddressID DESC)  
    END  
    ELSE  
     BEGIN  
      SET @ServiceAddressID =@PostalAddressID  
      SET @HouseNoService = @HouseNoPostal  
      SET @StreetService = @StreetPostal  
      SET @CityService = @CityPostaL  
      --SET @AreaService = @AreaPostal  
      SET @AreaService = CASE WHEN (@AreaPostal=0) THEN NULL ELSE @AreaPostal END
      SET @SZipCode = @PZipCode  
     END  
      
      
      
    --||***************************STEP 4****************************||--  
    SET @StatusText='Tenent Address';  
    IF(@IsSameAsTenent=0)  
    BEGIN  
     INSERT INTO CUSTOMERS.Tbl_CustomerTenentDetails(  
              GlobalAccountNumber  
              ,Title  
              ,FirstName  
              ,MiddleName  
              ,LastName  
              ,PhoneNumber  
              ,AlternatePhoneNumber  
              ,EmailID  
              ,CreatedDate  
              ,CreatedBy  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@TitleTanent  
              ,@FirstNameTanent  
              ,@MiddleNameTanent  
              ,@LastNameTanent  
              ,@PhoneNumberTanent  
              ,@AlternatePhoneNumberTanent  
              ,@EmailIdTanent  
              ,@CreatedDate  
              ,@CreatedBy  
  
              )  
     SET @TenentId=SCOPE_IDENTITY();--(SELECT TOP 1 TenentId FROM CUSTOMERS.Tbl_CustomerTenentDetails)  
    END  
    --||***************************STEP 5****************************||--  
    SET @StatusText='Customer Details';  
     INSERT INTO CUSTOMERS.Tbl_CustomersDetail (  
              GlobalAccountNumber  
              ,AccountNo  
              ,Title  
              ,FirstName  
              ,MiddleName  
              ,LastName  
              ,KnownAs  
              ,EmailId  
              ,HomeContactNumber  
              ,BusinessContactNumber  
              ,OtherContactNumber  
              ,IsSameAsService  
              ,ServiceAddressID  
              ,PostalAddressID  
              ,IsSameAsTenent  
              ,DocumentNo  
              ,ApplicationDate  
              ,ConnectionDate  
              ,SetupDate  
              ,ActiveStatusId  
              ,IsBEDCEmployee  
              ,EmployeeCode  
              ,IsVIPCustomer  
              ,TenentId  
              ,OldAccountNo  
              ,CreatedDate  
              ,CreatedBy  
              ,Service_HouseNo   
              ,Service_StreetName   
              ,Service_City   
              ,Service_AreaCode   
              ,Service_ZipCode   
              ,Postal_HouseNo   
              ,Postal_StreetName   
              ,Postal_City   
              ,Postal_AreaCode   
              ,Postal_ZipCode  
              )  
            values  
              (  
              @GlobalAccountNumber  
              ,@AccountNo  
              ,@TitleLandlord  
              ,@FirstNameLandlord  
              ,@MiddleNameLandlord  
              ,@LastNameLandlord  
              ,@KnownAs  
              ,@EmailIdLandlord  
              ,@HomeContactNumberLandlord  
              ,@BusinessPhoneNumberLandlord  
              ,@OtherPhoneNumberLandlord  
              ,@IsSameAsService   
              ,@ServiceAddressID  
              ,@PostalAddressID  
              ,@IsSameAsTenent  
              ,@DocumentNo  
              ,@ApplicationDate  
              ,@ConnectionDate  
              ,@SetupDate  
              ,1  
              ,@IsBEDCEmployee  
              ,@EmployeeCode  
              ,@IsVIPCustomer  
              ,@TenentId  
              ,@OldAccountNo  
              ,@CreatedDate  
              ,@CreatedBy  
              ,@HouseNoService  
              ,@StreetService   
              ,@CityService   
              --,@AreaService   
              ,CASE WHEN (@AreaService=0) THEN NULL ELSE @AreaService END
              ,@SZipCode   
              ,@HouseNoPostal   
              ,@StreetPostal   
              ,@CityPostaL   
              --,@AreaPostal   
              ,CASE WHEN (@AreaPostal=0) THEN NULL ELSE @AreaPostal END
              ,@PZipCode  
            )  
     --||***************************STEP 6****************************||--  
     SET @StatusText='Customer Procedural Details';  
     INSERT INTO CUSTOMERS.Tbl_CustomerProceduralDetails(  
               GlobalAccountNumber  
              ,CustomerTypeId  
              ,AccountTypeId  
              ,PoleID  
              ,MeterNumber  
              ,TariffClassID  
              ,IsEmbassyCustomer  
              ,EmbassyCode  
              ,PhaseId  
              ,ReadCodeID  
              ,BookNo  
              ,ClusterCategoryId  
              ,RouteSequenceNumber  
              ,CreatedDate  
              ,CreatedBy  
              ,ActiveStatusID  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@CustomerTypeId  
              ,@AccountTypeId  
              ,@PoleID  
              ,@MeterNumber  
              ,@TariffClassID  
              ,@IsEmbassyCustomer  
              ,@EmbassyCode  
              ,@PhaseId  
              ,@ReadCodeID  
              ,@BookNo  
              ,@ClusterCategoryId  
              ,@RouteSequenceNumber  
              ,@CreatedDate  
              ,@CreatedBy  
              ,1  
              )  
     --||***************************STEP 7****************************||--  
     SET @StatusText='Customer identity Details';  
     IF EXISTS(SELECT 0 FROM #IdentityList)  
     BEGIN  
      INSERT INTO CUSTOMERS.Tbl_CustomerIdentityDetails(  
               IdentityTypeId  
               ,IdentityNumber  
               ,GlobalAccountNumber  
               ,CreatedDate  
               ,CreatedBy  
               )  
      SELECT        *  
      FROM #IdentityList  
     END  
     --||***************************STEP 8****************************||--  
     SET @StatusText='Application Process Details';  
     INSERT INTO CUSTOMERS.Tbl_ApplicationProcessDetails(  
               GlobalAccountNumber  
              ,CertifiedBy  
              ,Seal1  
              ,Seal2  
              ,ApplicationProcessedBy  
              ,CreatedDate  
              ,CreatedBy  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@CertifiedBy  
              ,@Seal1  
              ,@Seal2  
              ,@ApplicationProcessedBy  
              ,@CreatedDate  
              ,@CreatedBy  
  
              )  
     --||***************************STEP 9****************************||--  
     SET @StatusText='Application Process Person Details';  
     SET @Bedcinfoid=SCOPE_IDENTITY();--(SELECT TOP 1 Bedcinfoid FROM CUSTOMERS.Tbl_ApplicationProcessDetails ORDER BY Bedcinfoid DESC)  
     INSERT INTO CUSTOMERS.Tbl_ApplicationProcessPersonDetails(  
              GlobalAccountNumber  
              ,Bedcinfoid  
              --,EmployeeName  
              ,InstalledBy  
              ,AgencyId  
              ,ContactName  
              ,CreatedDate  
              ,CreatedBy  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@Bedcinfoid  
              --,@EmployeeName  
              ,@InstalledBy  
              ,@AgencyId  
              ,@ContactName  
              ,@CreatedDate  
              ,@CreatedBy  
  
              )  
     --||***************************STEP 10****************************||--  
     SET @StatusText='Customer Active Details';  
     INSERT INTO CUSTOMERS.Tbl_CustomerActiveDetails(  
              GlobalAccountNumber  
              ,IsCAPMI  
              ,MeterAmount  
              ,InitialBillingKWh  
              ,PresentReading  
              ,InitialReading--Faiz-ID103  
              ,CreatedDate  
              ,CreatedBy  
              )  
            VALUES (  
              @GlobalAccountNumber  
              ,@IsCAPMI  
              ,@MeterAmount  
              ,@InitialBillingKWh  
              ,@PresentReading  
              ,@InitialReading--Faiz-ID103  
              ,@CreatedDate  
              ,@CreatedBy  
              )  
     --||***************************STEP 11****************************||--  
     SET @StatusText='Document Details';  
     if Exists(SELECT 0 FROM #DocumentList)  
     BEGIN  
      INSERT INTO Tbl_CustomerDocuments   
               (  
               GlobalAccountNo  
               ,DocumentName  
               ,[Path]  
               ,ActiveStatusId  
               ,CreatedBy  
               ,CreatedDate  
               )  
          SELECT      
               GlobalAccountNumber  
               ,value1  
               ,value2  
               ,ActiveStatusId  
               ,CreatedBy  
               ,CreatedDate  
          FROM  #DocumentList  
  
           DROP TABLE #DocumentList  
      END  
     --||***************************STEP 12****************************||--  
     SET @StatusText='User Define List';  
     if Exists(SELECT 0 FROM #UDFList)  
     BEGIN  
      INSERT INTO Tbl_USerDefinedValueDetails  
               (  
               AccountNo  
               ,UDCId  
               ,Value   
               )  
          SELECT      
               GlobalAccountNumber  
               ,UDCId  
               ,Value  
          FROM #UDFList  
  
             
      END  
      --||***************************STEP 13****************************||--  
     SET @StatusText='Tbl_GovtCustomers';  
     IF(@MGActTypeID > 0)  
     BEGIN  
      INSERT INTO Tbl_GovtCustomers  
              (  
              GlobalAccountNumber  
              ,MGActTypeID  
              )  
         values    (  
              @GlobalAccountNumber  
              ,@MGActTypeID     
              )  
     END  
      --||***************************STEP 14****************************||-- (Added By - Padmini(25th Feb 2015))  
     SET @StatusText='Tbl_Paidmeterdetails';  
     IF(@IsCAPMI=1)  
     BEGIN  
      INSERT INTO Tbl_PaidMeterDetails  
              (  
              AccountNo  
              ,MeterNo  
              ,MeterTypeId  
              ,MeterCost  
              ,ActiveStatusId  
              ,MeterAssignedDate  
              ,CreatedDate  
              ,CreatedBy  
              )  
         values  
              (  
              @GlobalAccountNumber  
              ,@MeterNumber  
              ,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
              ,@MeterAmount  
              ,1  
              ,@ConnectionDate  
              ,@CreatedDate  
              ,@CreatedBy  
              )       
     END  
    SET @StatusText =''  
   END  
    ELSE  
     BEGIN  
      SET @GlobalAccountNumber = ''  
      SET @IsSuccessful =0  
      SET @StatusText='Problem in account number genrate.';  
     END  
   END  
  ELSE  
   BEGIN   
    SET @GlobalAccountNumber = ''  
    SET @IsSuccessful =0  
    SET @StatusText='Problem in global account number genrate.';  
   END  
 COMMIT TRAN   
END TRY      
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @GlobalAccountNumber = ''  
 SET @IsSuccessful =0  
END CATCH  
END  
  
SELECT   
 @IsSuccessful AS IsSuccessful  
 ,@StatusText AS StatusText   
 ,@GlobalAccountNumber AS GolbalAccountNumber     
 FOR XML PATH('CustomerRegistrationBE'),TYPE  
--===========================REGISTRATION ENDS===========================--  
END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogsToApprove]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.CustomerTypeChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldCustomerTypeId
		 ,T.NewCustomerTypeId
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.OldCustomerTypeId) AS OldCustomerType
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.NewCustomerTypeId) AS NewCustomerType
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerTypeChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsToApprove]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 8 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.NameChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,ISNULL(T.OldTitle,'--') AS OldTitle
		 ,ISNULL(T.NewTitle,'--') AS Title
		 ,ISNULL(T.OldFirstName,'--') AS OldFirstName
		 ,ISNULL(T.NewFirstName,'--') AS FirstName
		 ,ISNULL(T.OldMiddleName,'--') AS OldMiddleName
		 ,ISNULL(T.NewMiddleName,'--') AS MiddleName
		 ,ISNULL(T.OldLastName,'--') AS OldLastName
		 ,ISNULL(T.NewLastName,'--') AS LastName
		 ,OldKnownAs
		 ,NewKnownAs AS KnownAs
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerNameChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsToApprove]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 01-04-2015
-- Description: The purpose of this procedure is to get list of Tariff Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 3 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.TariffChangeRequestId
		 ,T.AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
				 ,CD.Service_Landmark    
				 ,CD.Service_City,'',    
				 CD.Service_ZipCode) As ServiceAddress
		 ,PreviousTariffId AS OldClassID
		 ,ChangeRequestedTariffId AS NewClassID
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=PreviousTariffId) AS PreviousTariffName
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=ChangeRequestedTariffId) AS ChangeRequestedTariffName
		 ,T.OldClusterCategoryId AS OldClusterTypeId
		 ,T.NewClusterCategoryId AS NewClusterTypeId
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.OldClusterCategoryId) AS PreviousClusterName
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.NewClusterCategoryId) AS ChangeRequestedClusterName
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_LCustomerTariffChangeRequest T 
	INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersExportByBookWise]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 31-10-2014
-- Modified date: 02-12-2014
-- Description:	The purpose of this procedure is to get Customers export by Book wise
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomersExportByBookWise]
(
@XmlDoc xml
)
AS
BEGIN

	DECLARE @BUID VARCHAR(50)=''
		,@SUID VARCHAR(MAX)=''
		,@SCID VARCHAR(MAX)=''
		,@CycleId VARCHAR(MAX)=''
		,@BookNos VARCHAR(MAX)=''
		
		SELECT
			@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
		
		IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	select 
		 CustomerList.AccountNo
		 , CustomerList.GlobalAccountNumber
		 ,Name
		 ,OldAccountNo
		 ,MeterNumber
		 ,BusinessUnitName
		 ,ServiceUnitName
		 ,ServiceCenterName
		 ,ID AS BookCode
		 ,' ' CurrentReading
		 ,' ' AS Remarks
		 ,' ' AS PreviousReading
		 ,ServiceAddress as [Address]
		 ,BookSortOrder
		 ,SortOrder as CustomerSortOrder
		 ,InitialReading
		 ,CycleName
		 ,ClassName
	 INTO  #TotalCustomersList
	 FROM UDV_SearchCustomer   CustomerList
	 WHERE ReadCodeId=2 AND CustomerList.ActiveStatusId=1
	 AND (CustomerList.BU_ID=@BUID OR @BUID='') 
	 AND CustomerList.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,','))
	 AND CustomerList.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) 


SELECT   max(CustomerReadingId) as CurrentReadingID , CL.GlobalAccountNumber 
INTO #ReadingsList from Tbl_CustomerReadings	 CR
INNER JOIN #TotalCustomersList CL
ON CL.GlobalAccountNumber=CR.GlobalAccountNumber
GROUP BY CL.GlobalAccountNumber	  

SELECT RL.GlobalAccountNumber,CR.PresentReading as PreviousReading
INTO #ReadingsListWIthPreviousReading
FROM #ReadingsList  RL
INNER JOIN	 Tbl_CustomerReadings CR
ON		CR.CustomerReadingId=RL.CurrentReadingID

SELECT TCL.GlobalAccountNumber
	 ,Name
	 ,OldAccountNo
	 ,MeterNumber AS MeterNo
	 ,BusinessUnitName
	 ,ServiceUnitName
	 ,ServiceCenterName
	 ,BookCode AS BookNumber
	 ,' ' CurrentReading
	 ,' ' AS Remarks
	 ,TCL.[Address]
	 ,BookSortOrder
	 ,CustomerSortOrder
	 ,ClassName
	 ,CycleName
	 ,ISNULL(RL.PreviousReading,TCL.InitialReading) as PreviousReading 
	 From   #TotalCustomersList	TCL
	LEFT JOIN  #ReadingsListWIthPreviousReading	RL
	ON	 TCL.GlobalAccountNumber=RL.GlobalAccountNumber


	DROP TABLE	 #TotalCustomersList
	DROP TABLE	 #ReadingsListWIthPreviousReading
	DROP TABLE	 #ReadingsList
		
		--SELECT CD.AccountNo
		--	  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		--	  ,ISNULL(CD.OldAccountNo,'') AS OldAccountNo
		--	  ,cd.MeterNumber AS MeterNo
		--	  ,CD.BusinessUnitName
		--	  ,CD.ServiceUnitName
		--	  ,CD.ServiceCenterName
		--	  ,CD.CycleName
		--	  ,BN.BookCode
		--	  ,'' AS CurrentReading
		--	  ,'' AS Remarks
		--	  ,(SELECT TOP(1) PreviousReading FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=CD.GlobalAccountNumber
		--			ORDER BY CustomerReadingId DESC) AS PreviousReading
		--	  ,dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber) AS [Address]
		--	  ,BN.SortOrder AS BookSortOrder
		--	  ,CD.SortOrder AS CustomerSortOrder
		-- FROM [UDV_CustomerDescription] CD
		-- --JOIN Tbl_Cycles AS C ON CD.CycleId=C.CycleId
		-- JOIN Tbl_BookNumbers BN ON CD.BookNo = BN.BookNo
		-- --JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID
		-- --JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID
		-- --JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CD.ServiceCenterId
		-- --JOIN Tbl_Cycles BC ON BC.CycleId = C.CycleId
		-- WHERE ReadCodeId=2 AND CD.ActiveStatusId=1
		-- AND (CD.BU_ID=@BUID OR @BUID='')
		-- AND (CD.SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		-- AND (CD.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		-- AND (CD.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		-- --AND (CD.BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')
		-- AND (CD.BookNo IN
		--	(SELECT BookNo FROM Tbl_BookNumbers WHERE ActiveStatusId=1
		--	 AND (BU_ID=@BUID OR @BUID='')
		--	 AND (SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		--	 AND (ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		--	 AND (CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		--	 AND (BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')) OR @BookNos='')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading BIGINT
		,@NewMeterReading BIGINT
		,@PreviousReading BIGINT
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading BIGINT
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = CAST(OldMeterReading AS NUMERIC)
		,@NewMeterReading = CAST(NewMeterReading AS NUMERIC)
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = CAST(NewMeterInitialReading AS NUMERIC)
		,@NewMeterReadingDate = NewMeterReadingDate
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage > 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,@PreviousReading
										,@OldMeterReading
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,@NewMeterInitialReading
										,@NewMeterReading
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage > 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,@PreviousReading
								,@OldMeterReading
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,@NewMeterInitialReading
								,@NewMeterReading
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD
	Inner join
	Tbl_CustomerReadings  CR
	ON CR.GlobalAccountNumber=CD.GlobalAccountNumber
	and IsBilled=0 and Usage=0
 	--AND CD.BU_ID IN(SELECT [com] AS BU_ID FROM dbo.fn_Split(@Business_Units,','))
--	AND CD.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))
--	AND CD.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))
	

END
----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml    
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,BookCode  
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'N/A') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN 'N/A' WHEN NULL THEN 'N/A' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetOpenMonthAndYear]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 20-Apr-2015
-- Description:	To get the Open Month and Year
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetOpenMonthAndYear]	
AS
BEGIN
	SELECT	[Month]
			,[Year]
			,dbo.fn_GetMonthName([Month]) AS [MonthName]
	FROM	Tbl_BillingMonths
	WHERE OpenStatusId=1
	FOR XML PATH ('EstimationBE'), TYPE
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterNoChangeLogsToApprove]    Script Date: 04/20/2015 20:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Meter NO Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 7 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.MeterInfoChangeLogId
		 ,T.AccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,OldMeterNo
		 ,NewMeterNo
		 ,OldMeterReading
		 ,NewMeterReading
		 ,CONVERT(VARCHAR(20),MeterChangedDate,106) AS MeterChangedDate
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerMeterInfoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)

		  Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
		 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExist 
		,ReadDate   as  LatestDate 
		,AverageReading 
		,TotalReadings  
		,PreviousReading
		,PresentReading 
		,IsBilled
		,MeterNumber   as  ReadingsMeterNumber
		,IsRollOver
		,GlobalAccountNumber
		,CustomerReadingId
		INTO #CustomerTopTwoReading
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC
		
		IF ((select COUNT(0) from #CustomerTopTwoReading where IsRollOver=1) > 1)
			BEGIN

				SELECT  
					@usage=Sum(Usage),
					@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
					@IsExists = max(case when IsExist=0 then 0 else 1 end)
					,@LatestDate=max(LatestDate) 
					,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
					,@TotalReadings=max(TotalReadings)  
					,@PreviousReading = max(PreviousReading)
					,@PresentReading=min(PresentReading) 
					,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
					,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
					,@IsRollOver =1 
				FROM #CustomerTopTwoReading  AS TopTwoReadings
				Group By GlobalAccountNumber

			END
		ELSE
			BEGIN
					 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
						@IsExists = IsExist
					,@LatestDate=LatestDate 
					,@AverageReading=AverageReading 
					,@TotalReadings=TotalReadings  
					,@PreviousReading = PreviousReading
					,@PresentReading=PresentReading 
					,@IsBilled=IsBilled
					,@ReadingsMeterNumber=ReadingsMeterNumber 
					,@IsRollOver =0
					from #CustomerTopTwoReading
			END
		
		DROP table		#CustomerTopTwoReading

		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		SELECT TOP(1) @Month = BillMonth,
		@Year = BillYear ,@LastBillReadType= 
		(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @OldAcountNumber AS OldAccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,case when @MeterNumber=@ReadingsMeterNumber then  @IsExists	else 0 End   as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
				 ,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,@InititalReading as	 InititalReading
				 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllAccNoInfoXLMeterReadings]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Bhimaraju V
-- Create date: 10-Oct-2014
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAllAccNoInfoXLMeterReadings] 
	(	
	@MultiXmlDoc XML	
	)
AS
BEGIN
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),         
                             MinReading varchar(50))       
                                  
 DECLARE @CustomersCount TABLE(AccountNo VARCHAR(50),Total INT)               
     
 INSERT INTO @TempCustomer(AccountNo ,MinReading)         
 SELECT         
		c.value('(Global_Account_Number)[1]','VARCHAR(50)')         
	   ,(CASE c.value('(Average_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Average_Reading)[1]','VARCHAR(50)') END)       
     
 FROM @MultiXmlDoc.nodes('//NewDataSet/Table') AS T(c)
 
	DELETE FROM @CustomersCount  
	INSERT INTO @CustomersCount  
	SELECT AccountNo,COUNT(0) As Total from @TempCustomer  
	GROUP BY AccountNo
	
;With FinalResult AS  
(  
	SELECT Temp.MinReading
		  ,ISNULL(CD.ActiveStatusId,0) AS ActiveStatusId
		  ,CD.OldAccountNo
		  ,ISNULL(CD.GlobalAccountNumber,Temp.AccountNo) AS AccNum
		  --,Temp.AccountNo AS AccNum
	FROM @TempCustomer AS Temp
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail CD ON (CD.GlobalAccountNumber=Temp.AccountNo 
												OR Temp.AccountNo=CD.OldAccountNo)
)

 SELECT  
  (       
  SELECT   
        AccNum
        ,ISNULL(OldAccountNo,'--') AS OldAccountNo
        ,MinReading AS AverageReading
        ,ActiveStatusId
   FROM FinalResult       
  FOR XML PATH('BillingBE'),TYPE    
  )    
 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_Bookwise]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_Bookwise] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
	dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
	,MeterNumber as CustomerExistingMeterNumber
	,MeterDials As CustomerExistingMeterDials
	,Decimals as Decimals
	,InitialReading as	InitialReading
	,AccountNo
	,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeId = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
----------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			CustomerBills.BillGeneratedDate as EstimatedBillDate,
			0 AS IsActiveMonth,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExists,
			ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--') as LatestDate,
			CustomerList.Name,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			CustomerReadings.IsTamper,
			0 as Decimals,
			'' AS BillNo,
			ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading,
			CustomerReadings.TotalReadings,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then NULL else CustomerReadings.PresentReading end) 
				else NULL End as  PresentReading,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end   as PrvExist,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end AS Usage,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading else CustomerReadings.PreviousReading end)  	
				else isnull(CustomerList.InitialReading,0) End as PreviousReading,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then CONVERT(VARCHAR(10),CustomerBills.ReadType) else '--' end)  LastBillReadType
			,CustomerList.BookNo,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	 
			LEFT JOIN	  
			#CustomerLatestBills as CustomerBills 
			ON CustomerBills.GlobalAccountNumber=CustomerList.GlobalAccountNumber
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table  #CustomersListBook   
DROP Table  #CustomerLatestBills
DROP Table  #CusotmersWithMaxBillID
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSCWitlBillingStaus]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================        
-- Author:  NEERAJ    
-- Create date: 10-FEB-2015           
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC with billing status.    
-- Modified date: 16-FEB-2015           
-- Description: Added new field total customers  
-- =============================================        
CREATE PROCEDURE [dbo].[USP_GetCyclesByBUSUSCWitlBillingStaus]        
(        
@XmlDoc xml        
)        
AS        
BEGIN        
        
 DECLARE @BU_ID VARCHAR(20)    
   ,@SU_ID VARCHAR(MAX)    
   ,@ServiceCenterId VARCHAR(MAX)     
   ,@BillYear VARCHAR(MAX)    
   ,@BillMonth VARCHAR(MAX)       
          
 SELECT        
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')        
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')        
   ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')        
   ,@BillYear=C.value('(BillYear)[1]','VARCHAR(20)')          
   ,@BillMonth=C.value('(BillMonth)[1]','VARCHAR(20)')          
     
     
 FROM @XmlDoc.nodes('MastersBE') as T(C)        
         
 SELECT        
 (        
    SELECT        
   CycleId        
   ,(CycleName + ' ('+CycleCode+') - ' + SC.ServiceCenterName ) as CycleName
   ,(select COUNT(0)     
     from Tbl_CustomerBills as cb     
     where  cb.BillMonth=@BillMonth     
       and cb.BillYear=@BillYear     
       AND cb.BU_ID=BU.BU_ID    
       AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
       AND CB.CycleId=CY.CycleId) AS BillGenCount    
   ,(select COUNT(0)     
     from Tbl_CustomerBillPayments as bp JOIN Tbl_CustomerBills AS CB ON bp.BillNo=cb.BillNo    
     where  cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS PaymentCount       
  ,(select COUNT(0)     
     from Tbl_CustomerBills AS CB     
     JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId    
     where cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS BillAdjustmentCount      
   ,(SELECT COUNT(0)    
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=CY.CycleId) AS TotalCustomers  
  FROM Tbl_Cycles cy    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
  WHERE CY.ActiveStatusId=1        
  AND (BU.BU_ID=@BU_ID OR @BU_ID='')        
  AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')        
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')     
  
    AND CY.CycleId IN
  (SELECT C.CycleId
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=CY.CycleId GROUP BY C.CycleId HAVING COUNT(CPD.CustomerProcedureId)>1)
     
  ORDER BY CycleName ASC       
  FOR XML PATH('MastersBE'),TYPE        
 )        
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
END 



GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerStatusChangeApproval]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Modified By: Karteek  
-- Modified Date: 04-04-2015  
-- Description: Update custoemr status change Details after approval    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_CustomerStatusChangeApproval]    
(    
 @XmlDoc XML=null    
)    
AS    
BEGIN     
SET NOCOUNT ON;  
   
 DECLARE    
   @GlobalAccountNo VARCHAR(50)     
  ,@Remarks VARCHAR(200)    
  ,@ApprovalStatusId INT    
  ,@OldStatusId INT  
  ,@NewStatusId INT  
  ,@FunctionId INT  
  ,@ModifiedBy VARCHAR(50)  
  ,@PresentRoleId INT = NULL  
  ,@NextRoleId INT = NULL  
  ,@CurrentLevel INT  
  ,@ActiveStatusChangeLogId INT  
  ,@IsFinalApproval BIT = 0  
  ,@Details VARCHAR(200) 
  ,@ChangeDate DATETIME  
  
 SELECT    
   @ActiveStatusChangeLogId = C.value('(ActiveStatusChangeLogId)[1]','INT')    
  ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')     
  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
  ,@Details = C.value('(Details)[1]','VARCHAR(200)')   
  --,@ChangeDate = C.value('(ChangeDate)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 SELECT   
   @OldStatusId = OldStatus  
  ,@NewStatusId = NewStatus  
  ,@GlobalAccountNo = AccountNo  
  ,@Remarks = Remarks  
  ,@ChangeDate = ChangeDate
 FROM Tbl_CustomerActiveStatusChangeLogs  
 WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
   
 IF(@ApprovalStatusId = 2)  -- Request Approved  
  BEGIN    
   DECLARE @CurrentRoleId INT  
   SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId  
  
   IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists  
    BEGIN  
     SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND  
         RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs   
           WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId))  
  
     SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval  
     FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)  
  
     IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)  
      BEGIN -- If Approval levels are there and If it is final approval then update tariff  
        
       UPDATE Tbl_CustomerActiveStatusChangeLogs   
       SET   -- Updating Request with Level Roles & Approval status   
         ModifiedBy = @ModifiedBy  
        ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
        ,PresentApprovalRole = @PresentRoleId  
        ,NextApprovalRole = @NextRoleId   
        ,ApproveStatusId = @ApprovalStatusId  
       WHERE AccountNo = @GlobalAccountNo   
       AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
  
       UPDATE CUSTOMERS.Tbl_CustomersDetail  
       SET         
         ActiveStatusId = @NewStatusId  
        ,ModifedBy = @ModifiedBy    
        ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
       WHERE GlobalAccountNumber = @GlobalAccountNo  
         
       INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
         ActiveStatusChangeLogId  
        ,AccountNo  
        ,OldStatus  
        ,NewStatus  
        ,ApproveStatusId  
        ,Remarks  
        ,CreatedDate  
        ,CreatedBy  
        ,PresentApprovalRole  
        ,NextApprovalRole
        ,ChangeDate)    
       VALUES(    
         @ActiveStatusChangeLogId  
        ,@GlobalAccountNo    
        ,@OldStatusId  
        ,@NewStatusId   
        ,@ApprovalStatusId  
        ,@Remarks  
        ,dbo.fn_GetCurrentDateTime()    
        ,@ModifiedBy  
        ,@PresentRoleId  
        ,@NextRoleId
        ,@ChangeDate)   
          
      END  
     ELSE -- Not a final approval  
      BEGIN  
       UPDATE Tbl_CustomerActiveStatusChangeLogs   
       SET   -- Updating Request with Level Roles  
         ModifiedBy = @ModifiedBy  
        ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
        ,PresentApprovalRole = @PresentRoleId  
        ,NextApprovalRole = @NextRoleId   
        ,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)   
       WHERE AccountNo = @GlobalAccountNo   
       AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
         
      END  
    END  
   ELSE   
    BEGIN -- No Approval Levels are there  
     UPDATE Tbl_CustomerActiveStatusChangeLogs   
     SET   -- Updating Request with Level Roles & Approval status   
       ModifiedBy = @ModifiedBy  
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
      ,PresentApprovalRole = @PresentRoleId  
      ,NextApprovalRole = @NextRoleId   
      ,ApproveStatusId = @ApprovalStatusId  
     WHERE AccountNo = @GlobalAccountNo   
     AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
  
     UPDATE CUSTOMERS.Tbl_CustomersDetail  
     SET         
       ActiveStatusId = @NewStatusId  
      ,ModifedBy = @ModifiedBy    
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
     WHERE GlobalAccountNumber = @GlobalAccountNo  
       
     INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
       ActiveStatusChangeLogId  
      ,AccountNo  
      ,OldStatus  
      ,NewStatus  
      ,ApproveStatusId  
      ,Remarks  
      ,CreatedDate  
      ,CreatedBy  
      ,PresentApprovalRole  
      ,NextApprovalRole
      ,ChangeDate)    
     VALUES(    
       @ActiveStatusChangeLogId  
      ,@GlobalAccountNo    
      ,@OldStatusId  
      ,@NewStatusId   
      ,@ApprovalStatusId  
      ,@Remarks  
      ,dbo.fn_GetCurrentDateTime()    
      ,@ModifiedBy  
      ,@PresentRoleId  
      ,@NextRoleId
      ,@ChangeDate)   
       
   END  
  
   SELECT 1 As IsSuccess    
   FOR XML PATH('ChangeBookNoBe'),TYPE  
  END    
 ELSE    
  BEGIN  -- Request Not Approved  
   IF(@ApprovalStatusId = 3) -- Request is Rejected  
    BEGIN  
     IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)  
      BEGIN -- Approval levels are there and status is rejected  
       SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND  
             RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs   
                WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId))  
  
       SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   
       FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)  
      END  
    END  
   ELSE IF(@ApprovalStatusId = 4) -- Request is Hold  
    BEGIN  
     IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)  
      SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerActiveStatusChangeLogs   
      WHERE ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
    END  
  
   -- If there are no Approval levels then Present and Next Approval roles will be updated as NULL     
   UPDATE Tbl_CustomerActiveStatusChangeLogs   
   SET     
     ModifiedBy = @ModifiedBy  
    ,ModifiedDate = dbo.fn_GetCurrentDateTime()  
    ,PresentApprovalRole = @PresentRoleId  
    ,NextApprovalRole = @NextRoleId   
    ,ApproveStatusId = @ApprovalStatusId  
    ,Remarks = @Details  
   WHERE AccountNo = @GlobalAccountNo    
   AND ActiveStatusChangeLogId = @ActiveStatusChangeLogId  
  
   SELECT 1 As IsSuccess    
   FOR XML PATH('ChangeBookNoBe'),TYPE    
  END    
  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerStatusChangeLogsToApprove]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Status Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerStatusChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 9 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.ActiveStatusChangeLogId
		 ,T.AccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldStatus AS OldStatusId
		 ,T.NewStatus AS NewStatusId
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.OldStatus) AS OldStatus
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.NewStatus) AS NewStatus
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerActiveStatusChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillingDisabledBookNo]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Bhimaraju Vanka      
-- Create date: 24-Sep-2014      
-- Modified By: T.Karthik      
-- Modified Date : 17-10-2014      
-- Description: Purpose is To get List Of Billing Disabled BookNo     
-- Modified By: Faiz-ID103  
-- Modified Date: 10-Apr-2015  
-- Description: Disable date field added        
-- Modified By: Faiz-ID103  
-- Modified Date: 20-Apr-2015  
-- Description: Modified the functionality to display the disable books for Super Admin  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBillingDisabledBookNo]    
(      
 @XmlDoc Xml      
)      
AS      
BEGIN      
      
 DECLARE  @PageNo INT      
   ,@PageSize INT      
   ,@YearId INT      
   ,@MonthId INT      
   ,@BU_ID VARCHAR(50)    
          
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')      
    ,@PageSize = C.value('(PageSize)[1]','INT')      
    ,@YearId=C.value('(YearId)[1]','INT')      
    ,@MonthId=C.value('(MonthId)[1]','INT')      
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')             
  FROM @XmlDoc.nodes('BillingDisabledBooksBe') AS T(C)       
        
  CREATE TABLE #BillingDisabledBookNos(Sno INT PRIMARY KEY IDENTITY(1,1),DisabledBookId INT)      
      
  INSERT INTO #BillingDisabledBookNos(DisabledBookId)      
   SELECT      
    DisabledBookId      
   FROM Tbl_BillingDisabledBooks      
   WHERE IsActive = 1      
   AND (YearId=@YearId OR @YearId=0)      
   AND (MonthId=@MonthId OR @MonthId=0)      
   ORDER BY CreatedDate DESC      
         
  DECLARE @Count INT=0      
  SET @Count=(SELECT COUNT(0) FROM #BillingDisabledBookNos)      
        
  SELECT      
    Sno AS RowNumber      
    ,BDB.BookNo AS BookNo  -- Faiz_ID103    
    ,BN.ID AS ID    
    ,BDB.DisabledBookId      
    ,ISNULL(Remarks,'--') AS Details      
    ,MonthId      
    ,YearId      
    ,((select dbo.fn_GetMonthName([MonthId]))+'-'+CONVERT(varchar(10), [YearId])) AS MonthYear      
    ,@Count AS TotalRecords    
    ,(CASE IsPartialBill WHEN 1 THEN 'Partial Book' ELSE DisableType END) AS DisableType    
    ,ISNULL((CONVERT(VARCHAR(20),DisableDate,106)),'--') AS DisableDate  
  FROM Tbl_BillingDisabledBooks AS BDB      
  JOIN #BillingDisabledBookNos LBDB ON BDB.DisabledBookId=LBDB.DisabledBookId      
  INNER JOIN UDV_BookNumberDetails BN on BDB.BookNo=BN.BookNo     
 AND (BN.BU_ID = @BU_ID OR @BU_ID='')-- Faiz-ID103-20-Apr-2015
  INNER JOIN TBl_MDisableType D ON D.DisableTypeId = BDB.DisableTypeId    
  --INNER JOIN Tbl_Cycles C ON C.CycleId = BN.CycleId    
  --INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = C.ServiceCenterId    
  --INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID     
  AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
 END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
    
	--SELECT(  
		 SELECT   --BookChange.AccountNo
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
		   ,(SELECT Id FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		   ,(SELECT Id FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  
		   ,BookChange.Remarks  
		   ,BookChange.CreatedBy  
		   ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		   ,CustomerView.BusinessUnitName  
		   ,CustomerView.ServiceUnitName  
		   ,CustomerView.ServiceCenterName  
		   ,CustomerView.CycleName  
		   ,CustomerView.BookCode  
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.BookSortOrder AS BookSortOrder  
		   ,CustomerView.OldAccountNo
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder    
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_BookNoChangeLogs  BookChange  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId    
		 -- changed by karteek end    	       
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@IsRollover)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage+1
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage + 1
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@IsRollover)
									
				END
		END
		
 
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 BookChange.AccountNo  
			,BookChange.ApproveStatusId
			,(SELECT Id FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
			,(SELECT Id FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo
			,BookChange.Remarks  
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId 
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--GROUP BY BookChange.AccountNo  
		--   ,BookChange.ApproveStatusId  
		--   ,BookChange.OldBookNo  
		--   ,BookChange.NewBookNo  
		--   ,BookChange.Remarks  
		--   ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107)  
		--   ,ApproveSttus.ApprovalStatus  
		--   ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBookNumberActiveStatus]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  T.Karthik      
-- Create date: 26-02-2014      
-- Description: The purpose of this procedure is to update Active status of Book Number     
-- Modified By : Bhargav G    
-- Modified Date : 17th Feb, 2015    
-- Modified Description : Restricted user to deactive the Book Number which have customers associated to it.     
-- =============================================      
ALTER PROCEDURE [dbo].[USP_UpdateBookNumberActiveStatus]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @Id VARCHAR(20)    
   ,@ActiveStatusId INT    
   ,@ModifiedBy VARCHAR(50)      
  
 SELECT @Id=C.value('(BookNo)[1]','VARCHAR(20)')      
  ,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')      
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')      
 FROM @XmlDoc.nodes('MastersBE') as T(C)      
  
 IF NOT EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE BookNo = @Id)    
  BEGIN    
   UPDATE Tbl_BookNumbers     
    SET ActiveStatusId = @ActiveStatusId    
     ,ModifiedBy = @ModifiedBy    
     ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
   WHERE BookNo=@Id  
  
   UPDATE Tbl_Audit_BookNumbers     
    SET ActiveStatusId=@ActiveStatusId    
     ,ModifiedBy=@ModifiedBy    
     ,ModifiedDate=dbo.fn_GetCurrentDateTime()    
   WHERE BookNo=@Id  
  
   SELECT 1 AS IsSuccess     
   FOR XML PATH('MastersBE')    
  END    
 ELSE    
  BEGIN    
   SELECT COUNT(0) AS [Count]    
    ,0 AS IsSuccess    
   FROM UDV_CustomerDescription    
   WHERE BookNo = @Id    
   FOR XML PATH('MastersBE')    
  END    
END  
GO

/****** Object:  StoredProcedure [dbo].[UPS_GetBillTextFiles]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <19-March-2014>        
-- Description: <GET EBILL DAILY DETAILS>        
-- =============================================        
ALTER PROCEDURE  [dbo].[UPS_GetBillTextFiles]       
(      
@XmlDoc XML     
)       
AS        
BEGIN       
    
Declare @Month INT,      
  @Year INT    
      
SELECT      
   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
       
    
 SELECT CY.ServiceCenterId ,    
   BS.BillingYear,    
   BillingMonth,    
   SCDetails.BusinessUnitName,    
   SCDetails.ServiceCenterName,      
   SCDetails.ServiceUnitName,    
   --'~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,     
   MAX(BS.BillingFile) As FilePath,      
   dbo.fn_GetMonthName(BillingMonth) as [MonthName]      
 FROM Tbl_BillingQueueSchedule BS        
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId        
 and BillGenarationStatusId=1    
 AND BS.BillingMonth=@Month     --2
 AND BS.BillingYear=@Year      --2015
 INNER JOIN      
 [UDV_ServiceCenterDetails] SCDetails      
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId      
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth       
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,      
 SCDetails.ServiceUnitName        
    
END    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				  FROM UDV_CustomerMeterInformation CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				  FROM UDV_CustomerMeterInformation  CD
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   


GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 08-JAN-2014      
-- Description: The purpose of this procedure is to get list of MeterInformation      
-- Modified By:  Faiz-ID103  
-- Modified date: 08-JAN-2014      
-- Description: Added Business Unit name and Meter Dials field for new customer registration page  
  
-- =============================================      
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @MeterNo VARCHAR(100)      
   ,@MeterSerialNo VARCHAR(100)      
   ,@BUID VARCHAR(50)      
   ,@CustomerTypeId INT      
         
  SELECT         
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')      
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')      
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')      
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')      
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  IF (@CustomerTypeId=3)      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType     
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo      
       ,BU.BusinessUnitName    
       ,MeterDials  
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID WHERE M.ActiveStatusId IN(1) AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
       WHERE MeterNumber !='' )      
       AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)      
      AND M.MeterType=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
  ELSE      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType      
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo       
       ,MeterDials  
       ,BU.BusinessUnitName    
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID    
      WHERE M.ActiveStatusId IN(1)      
      AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
       WHERE MeterNumber !='' )      
       AND M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId != 2)      
      AND M.MeterType !=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
         
END

GO

/****** Object:  StoredProcedure [dbo].[USP_MasterSearch]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Bhimaraju Vanka      
-- Create date: 15-Nov-2014      
-- Description: This is the proc about search masters using flag      
-- MODIFIED BY : NEERAJ KANOJIYA      
-- MODIFIED DATE: 17-NOV-2014      
-- Modified By: Padmini      
-- Modified Date:17-Feb-2015      
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order      
-- Modified By: Faiz      
-- Modified Date:27-Mar-2015    
-- Description: Modified BookNo and Id for flag-5 for book no search    
-- Modified By: Faiz      
-- Modified Date:03-Apr-2015  
-- Description: Added Meter Type to search with meter type in Meter Information Search      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_MasterSearch]      
 (@XmlDoc Xml=null)      
AS      
BEGIN      
 DECLARE  @BusinessUnit VARCHAR(50)      
   ,@BusinessUnitCode VARCHAR(50)      
   ,@ServiceUnit VARCHAR(50)      
   ,@ServiceUnitCode VARCHAR(50)      
   ,@ServiceCenter VARCHAR(50)      
   ,@ServiceCenterCode VARCHAR(50)      
   ,@CycleName VARCHAR(50)      
   ,@CycleCode VARCHAR(50)      
   ,@BookNo VARCHAR(50)      
   ,@BookCode VARCHAR(50)      
   ,@RouteName VARCHAR(50)      
   ,@RouteCode VARCHAR(50)      
   ,@MeterNo VARCHAR(50)      
   ,@MeterSerialNo VARCHAR(50)      
   ,@Brand VARCHAR(50)    
   ,@MeterType VARCHAR(50)  --Faiz-ID103  
   ,@Name VARCHAR(50)      
   ,@ContactNo VARCHAR(50)      
   ,@RoleId INT      
   ,@DesignationId INT      
   ,@PageNo INT      
   ,@PageSize INT      
   ,@Flag INT      
   ,@BU_ID  VARCHAR(50)      
         
 SELECT   @BusinessUnit=C.value('(BusinessUnit)[1]','VARCHAR(50)')      
   ,@BusinessUnitCode=C.value('(BusinessUnitCode)[1]','VARCHAR(50)')      
   ,@ServiceUnit=C.value('(ServiceUnit)[1]','VARCHAR(50)')      
   ,@ServiceUnitCode=C.value('(ServiceUnitCode)[1]','VARCHAR(50)')      
   ,@ServiceCenter=C.value('(ServiceCenter)[1]','VARCHAR(50)')      
   ,@ServiceCenterCode=C.value('(ServiceCenterCode)[1]','VARCHAR(50)')      
   ,@CycleName=C.value('(CycleName)[1]','VARCHAR(50)')      
   ,@CycleCode=C.value('(CycleCode)[1]','VARCHAR(50)')      
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')      
   ,@BookCode=C.value('(BookCode)[1]','VARCHAR(50)')      
   ,@RouteName=C.value('(RouteName)[1]','VARCHAR(50)')      
   ,@RouteCode=C.value('(RouteCode)[1]','VARCHAR(50)')      
   ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')      
   ,@MeterSerialNo=C.value('(MeterSerialNo)[1]','VARCHAR(50)')      
   ,@Brand=C.value('(Brand)[1]','VARCHAR(50)')       
   ,@MeterType=C.value('(MeterType)[1]','VARCHAR(50)')    --Faiz-ID103  
   ,@Name=C.value('(Name)[1]','VARCHAR(50)')      
   ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(50)')      
   ,@RoleId=C.value('(RoleId)[1]','INT')      
   ,@DesignationId=C.value('(DesignationId)[1]','INT')      
   ,@PageNo=C.value('(PageNo)[1]','INT')      
   ,@PageSize=C.value('(PageSize)[1]','INT')      
   ,@Flag=C.value('(Flag)[1]','INT')         
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')         
  FROM @XmlDoc.nodes('MasterSearchBE') as T(C)      
        
  --============================== ** Note ** ==========================      
  --@Flag = 1 --Bussiness Unit Search      
  --@Flag = 2 --Service Unit Search      
  --@Flag = 3 --Service Center Search      
  --@Flag = 4 --Cycle Search      
  --@Flag = 5 --Book Search      
  --@Flag = 6 --Route Management Search      
  --@Flag = 7 --Meter Information Search      
  --@Flag = 8 --User Search      
  --@Flag = 9 --Change Customer BU      
  --============================== ** Note ** ==========================      
        
  IF (@Flag = 1)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
    SELECT B.BU_ID      
      ,B.BusinessUnitName      
      ,ROW_NUMBER() OVER(ORDER BY B.BU_ID ASC) AS RowNumber      
      ,ISNULL(B.BUCode,'-') AS BusinessUnitCode      
      ,B.Notes      
      ,(S.StateName +' ( '+ S.StateCode +' )')  AS StateName      
      ,S.StateCode      
    FROM Tbl_BussinessUnits B      
    LEFT JOIN Tbl_States S ON S.StateCode=B.StateCode      
    WHERE       
    (ISNULL(BusinessUnitName,'') like '%'+@BusinessUnit+'%' OR @BusinessUnit='')      
    AND (ISNULL(BUCode,'') like '%'+@BusinessUnitCode+'%' OR @BusinessUnitCode='')      
    --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
    AND (B.BU_ID=@BU_ID OR @BU_ID='')      
  )        
   SELECT        
    *          
    ,(Select COUNT(0) from PagedResults) as TotalRecords      
    FROM PagedResults p      
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 2)      
  BEGIN      
  ;WITH PagedResults AS      
  (      
   SELECT  --S.SU_ID      
     S.ServiceUnitName      
     ,ROW_NUMBER() OVER(ORDER BY S.SU_ID ASC) AS RowNumber      
     ,ISNULL(S.SUCode,'--') AS ServiceUnitCode      
     ,S.Notes      
     ,B.BusinessUnitName      
     ,B.StateCode      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_ServiceUnits S      
   LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode      
   WHERE (ISNULL(ServiceUnitName,'') like '%'+@ServiceUnit+'%' OR @ServiceUnit='')      
   AND (ISNULL(SUCode,'') like '%'+@ServiceUnitCode+'%' OR @ServiceUnitCode='')      
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (B.BU_ID=@BU_ID OR @BU_ID='')      
  )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
  END      
  ELSE IF(@Flag = 3)      
  BEGIN      
  ;WITH PagedResults AS      
  (      
   SELECT  --C.ServiceCenterId      
     C.ServiceCenterName      
     ,ROW_NUMBER() OVER(ORDER BY C.ServiceCenterId ASC) AS RowNumber      
     ,C.SCCode      
     ,C.Notes      
     ,S.ServiceUnitName      
     ,B.StateCode      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_ServiceCenter C      
   LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=C.SU_ID      
   LEFT JOIN Tbl_BussinessUnits B ON S.BU_ID=B.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode      
   WHERE (ISNULL(ServiceCenterName,'') like '%'+@ServiceCenter+'%' OR @ServiceCenter='')      
   AND (ISNULL(SCCode,'') like '%'+@ServiceCenterCode+'%' OR @ServiceCenterCode='')      
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (B.BU_ID=@BU_ID OR @BU_ID='')      
  )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
  END      
  ELSE IF(@Flag = 4)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  --C.CycleId      
     ROW_NUMBER() OVER(ORDER BY C.CycleId ASC) AS RowNumber      
     ,C.CycleCode      
     ,C.CycleName      
     ,C.ContactName      
     ,C.ContactNo      
     ,C.DetailsOfCycle      
     ,B.BusinessUnitName AS BusinessUnit      
     ,S.ServiceUnitName AS ServiceUnit      
     ,SC.ServiceCenterName AS ServiceCenter      
     ,B.StateCode      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_Cycles C      
   LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId      
   LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID      
   LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode      
   WHERE (ISNULL(CycleCode,'') like '%'+@CycleCode+'%' OR @CycleCode='')      
   AND (ISNULL(CycleName,'') like '%'+@CycleName+'%' OR @CycleName='')      
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (B.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
  END      
  ELSE IF(@Flag = 5)      
  BEGIN      
  ;WITH PagedResults AS      
  (      
   SELECT  --B.BookNo --Faiz-ID103    
   B.ID AS BookNo     
     ,B.BookCode      
     ,B.Details      
     ,B.NoOfAccounts      
     ,ROW_NUMBER() OVER(ORDER BY B.BookNo ASC) AS RowNumber      
     ,C.CycleName      
     ,BU.BusinessUnitName AS BusinessUnit      
     ,SU.ServiceUnitName AS ServiceUnitName      
     ,SC.ServiceCenterName AS ServiceCenterName      
     ,BU.StateCode      
     ,BU.BusinessUnitName      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_BookNumbers B      
   LEFT JOIN Tbl_Cycles C ON C.CycleId=B.CycleId      
   LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId      
   LEFT JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID      
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode      
   WHERE (ISNULL(BookCode,'') like '%'+@BookCode+'%' OR @BookCode='')      
   AND (ISNULL(B.ID,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103    
   --AND (ISNULL(BookNo,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103    
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 6)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  R.RouteName      
     ,R.RouteCode      
     ,R.Details      
     ,ROW_NUMBER() OVER(ORDER BY R.RouteName ASC) AS RowNumber      
     ,BU.BusinessUnitName AS BusinessUnit      
     ,BU.StateCode      
   FROM Tbl_MRoutes R      
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=R.BU_ID      
   WHERE (ISNULL(RouteName,'') like '%'+@RouteName+'%' OR @RouteName='')      
   AND (ISNULL(RouteCode,'') like '%'+@RouteCode+'%' OR @RouteCode='')      
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 7)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  M.MeterNo      
     ,M.MeterSerialNo      
     ,MT.MeterType      
     ,M.MeterSize      
     ,M.MeterBrand      
     ,M.MeterRating      
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber      
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate      
     ,M.MeterDials      
     ,M.MeterDetails      
     ,M.MeterMultiplier      
     ,M.MeterId      
     ,M.BU_ID      
    ,B.BusinessUnitName      
   FROM Tbl_MeterInformation M      
   JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType AND MT.ActiveStatus=1      
   LEFT JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID       
   WHERE (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')      
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')      
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')      
   AND (ISNULL(MT.MeterType,'')=@MeterType OR @MeterType='')    --Faiz-ID103  
   AND (M.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 8)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  U.UserId AS EmployeeId    
     ,U.Name      
     ,ROW_NUMBER() OVER(ORDER BY U.UserId ASC) AS RowNumber      
     ,U.SurName      
     ,U.PrimaryContact AS ContactNo      
     ,U.PrimaryEmailId      
     ,D.DesignationName AS Designation      
     ,R.RoleName      
     ,A.Status       
   FROM Tbl_UserDetails U      
   JOIN Tbl_MDesignations D ON D.DesignationId=U.DesignationId      
   JOIN Tbl_MRoles R ON R.RoleId=U.RoleId      
   JOIN Tbl_UserLoginDetails L ON L.UserId=U.UserId      
   JOIN Tbl_MActiveStatusDetails A ON L.ActiveStatusId=A.ActiveStatusId      
   AND L.ActiveStatusId IN (1,2)      
   AND ((ISNULL(Name,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(SurName,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(PrimaryEmailId,'') like '%'+@Name+'%' OR @Name=''))      
   AND (ISNULL(PrimaryContact,'') like '%'+@ContactNo+'%' OR @ContactNo='')      
   AND (ISNULL(R.RoleId,0)=@RoleId OR @RoleId=0)      
   AND (ISNULL(D.DesignationId,0)=@DesignationId OR @DesignationId=0)         
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 9)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  M.MeterNo      
     ,M.MeterSerialNo      
     ,MT.MeterType      
     ,M.MeterSize      
     ,M.MeterBrand      
     ,M.MeterRating      
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber      
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate      
     ,M.MeterDials      
     ,M.MeterDetails      
     ,M.MeterMultiplier      
     ,M.MeterId      
     ,M.BU_ID      
    ,B.BusinessUnitName      
   FROM Tbl_MeterInformation M      
   LEFT JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType      
   JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID      
   AND M.MeterNo NOT IN (SELECT CPD.MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD WHERE CPD.MeterNumber IS Not NULL)      
   AND M.ActiveStatusId IN(1,2)      
   AND (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')      
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')      
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')    
   AND (M.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
 END      

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterInformation]    Script Date: 04/20/2015 20:17:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 08-04-2014  
-- Description: The purpose of this procedure is to get list of MeterInformation  
-- Author:  V.Bhimaraju  
-- Modified date: 04-02-2015  
-- Description: Added GlobalAccount No in this proc  
-- Author:  Faiz-ID103
-- Modified date: 20-APR-2015
-- Description: Modified the SP for active meter Type filter
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterInformation]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT  
   ,@BU_ID VARCHAR(20)  
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID= C.value('(BU_ID)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY M.ActiveStatusId ASC , M.CreatedDate DESC ) AS RowNumber  
    ,MeterId  
    ,MeterNo  
    ,M.MeterType AS MeterTypeId  
    --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType  
    ,MT.MeterType AS MeterType --Faiz-ID103
    ,MeterSize  
    ,ISNULL(MeterDetails,'---') AS Details  
    ,M.ActiveStatusId  
    ,M.CreatedBy  
    ,MeterMultiplier  
    ,MeterBrand  
    ,MeterSerialNo  
    ,MeterRating  
    ,CONVERT(VARCHAR(50),NextCalibrationDate,103) AS BillDate  
    ,ISNULL((CONVERT(VARCHAR(50),NextCalibrationDate,106)),'--') AS NextCalibrationDate  
    ,ISNULL(ServiceHoursBeforeCalibration,'--') AS ServiceHoursBeforeCalibration  
    ,MeterDials  
    ,ISNULL(Decimals,0) AS Decimals  
    ,M.BU_ID  
    ,B.BusinessUnitName  
    ,ISNULL(CPD.GlobalAccountNumber,'--') AS AccountNo  
    FROM Tbl_MeterInformation AS M  
    JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID   
    LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.MeterNumber=M.MeterNo  
    JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 --Faiz-ID103
    WHERE M.ActiveStatusId IN(1,2)  
    AND M.BU_ID=@BU_ID OR @BU_ID=''  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END  

GO


