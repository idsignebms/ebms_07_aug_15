
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsAccountNoExists_BU_Id]    Script Date: 04/21/2015 21:53:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
ALTER FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF  EXISTS(SELECT 0 FROM UDV_IsCustomerExists 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId=1 
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=1
		ELSE
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END
