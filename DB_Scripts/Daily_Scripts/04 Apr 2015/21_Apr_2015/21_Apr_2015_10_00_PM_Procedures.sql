
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsToApprove]    Script Date: 04/21/2015 21:55:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.AddressChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldPostal_HouseNo
		 ,T.NewPostal_HouseNo
		 ,T.OldPostal_City
		 ,T.NewPostal_City
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerAddressChangeLog_New T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 10-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBulkCustomerPayments]          
(          
	 @XmlDoc XML           
	,@MultiXmlDoc XML          
)          
AS          
BEGIN    

DECLARE @CreatedBy VARCHAR(50)    
	,@DocumentPath VARCHAR(MAX)          
	,@DocumentName VARCHAR(MAX)         
	,@BatchNo VARCHAR(50) 
	,@StatusText VARCHAR(200) = ''
	,@IsSuccess BIT = 0
	,@IsBillExist BIT 
	
	SELECT            
		 @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')          
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')          
		,@DocumentName = C.value('(DocumentName)[1]','VARCHAR(MAX)')        
		,@BatchNo = C.value('(BatchNo)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)    	

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), AccountNo VARCHAR(50), ReceiptNo VARCHAR(20),           
							PaymentMode VARCHAR(20), PaymentModeId INT, 
							AmountPaid DECIMAL(18,2), RecievedDate DATETIME)
	
	INSERT INTO @TempCustomer(AccountNo, ReceiptNo, PaymentMode, PaymentModeId, AmountPaid, RecievedDate)           
	SELECT           
		 T.c.value('(AccountNo)[1]','VARCHAR(50)')           
		,T.c.value('(ReceiptNO)[1]','VARCHAR(20)')           
		,T.c.value('(PaymentMode)[1]','VARCHAR(20)')          
		,T.c.value('(PaymentModeId)[1]','VARCHAR(20)')          
		,T.c.value('(PaidAmount)[1]','DECIMAL(18,2)')          
		,T.c.value('(PaymentDate)[1]','DATETIME')          
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Table1') AS T(c)    
							
	DECLARE @SNo INT, @TotalCount INT
			,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
			,@CustomerPaymentId INT
			
	DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
	SET @SNo = 1
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer    
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SELECT @PresentAccountNo = AccountNo, @PaidAmount = AmountPaid FROM @TempCustomer WHERE SNo = @SNo
					
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 AccountNo
						,ReceiptNo
						,PaymentModeId
						,@DocumentPath
						,@DocumentName
						,RecievedDate
						,2 -- For Bulk Upload
						,AmountPaid
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempCustomer WHERE AccountNo = @PresentAccountNo 
					AND SNo = @SNo
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							SET @StatusText = 'Customer Pending Bills'
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							SET @StatusText = 'Customer Bills Paymnets'
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							SET @StatusText = 'Customer Bills'
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							SET @StatusText = 'Customer Bills Paymnets'
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @StatusText = 'Success'
					SET @IsSuccess = 1
					SET @SNo = @SNo + 1
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess =0
			END CATCH		
		END
		
	SELECT   
		 @IsSuccess As IsSuccess
		,@StatusText AS StatusText
	FOR XML PATH('PaymentsBe'),TYPE
		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <25-MAR-2014>          
-- Description: Customer Payment from file upload   
-- Author:  <NEERAJ KANOJIYA>          
-- Create date: <24-MAR-2015>          
-- Description: Romove join to check customer bill in process
--Modified By K.Satya
--ModifiedDate 21-04-2015
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload]          
(  
	 @XmlDoc XML  
	,@MultiXmlDoc XML  
)          
AS          
BEGIN          
          
	DECLARE @TempCustomer TABLE(SNO INT,AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),ReceiptNO VARCHAR(50)     
		,ReceivedDate DATETIME,PaymentModeId VARCHAR(50))   
         
	DECLARE @Bu_Id VARCHAR(50)  
   
	SELECT   
		@Bu_Id = C.value('(BU_Id)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('PaymentsBe') as T(C)      
         
	INSERT INTO @TempCustomer(SNO,AccountNo ,AmountPaid,ReceiptNO,ReceivedDate,PaymentModeId)      
	SELECT             
		c.value('(SNO)[1]','INT')                         
		,c.value('(AccountNo)[1]','VARCHAR(50)')  
		,c.value('(AmountPaid)[1]','DECIMAL(18,2)')                         
		,c.value('(ReceiptNO)[1]','VARCHAR(50)')                         
		,LEFT(c.value('(ReceivedDate)[1]','VARCHAR(50)'),10)    
		,c.value('(PaymentMode)[1]','VARCHAR(50)')    
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Payments') AS T(c)    
	    
	SELECT    
	 (    
	--	SELECT     
	--		 ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
	--		,CD.GlobalAccountNumber  AS AccountNo  
	--		, CD.OldAccountNo AS OldAccountNo  
	--		,TempDetails.AmountPaid AS PaidAmount    
	--		,TempDetails.ReceiptNO    
	--		,TempDetails.ReceivedDate   
	--		,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
	--		,dbo.fn_IsDuplicatePayment(CD.GlobalAccountNumber,TempDetails.ReceivedDate,TempDetails.AmountPaid, TempDetails.ReceiptNO) AS IsDuplicate  
	--		,dbo.fn_IsAccountNoExists_BU_Id(TempDetails.AccountNo,@Bu_Id) AS IsExists    
	--		,(CASE WHEN PM.PaymentModeId IS NULL THEN CONVERT(VARCHAR(10),TempDetails.PaymentModeId)   
	--		ELSE (SELECT CONVERT(VARCHAR(10),PaymentModeId)+' - '+ PaymentMode FROM Tbl_MPaymentMode WHERE PaymentMode=PM.PaymentMode) END) AS PaymentMode    
	--		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId 
	--	FROM @TempCustomer AS TempDetails     
	--	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo  
	--	LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentMode    
		
		
						
		SELECT 
		ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
					,CD.GlobalAccountNumber  AS AccountNo  
					, CD.OldAccountNo AS OldAccountNo  
					,TempDetails.AmountPaid AS PaidAmount    
					,TempDetails.ReceiptNO    
					,TempDetails.ReceivedDate   
					,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
					,Case When (TempDetails.AmountPaid = isnull(CP.PaidAmount,0)  
					   and TempDetails.ReceiptNO = isnull(CP.ReceiptNo,0))
						 THEn 1
						 else 0 end	  IsDuplicate 
				   , case when BookDetails.BU_ID  = @Bu_Id then 1 else 0  end as IsExists 
					,CONVERT(VARCHAR(10),isnull(PM.PaymentModeId,''))+' - '+ isnull(PM.PaymentMode,'')	  as PaymentMode
					,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId 
			FROM @TempCustomer AS TempDetails     
			INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON (TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo)
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD On (CD.GlobalAccountNumber= CPD.GlobalAccountNumber )
			INNER JOIN UDV_BookNumberDetails BookDetails ON BookDetails.BookNo = CPD.BookNo
			LEFT JOIN Tbl_MPaymentMode AS PM ON TempDetails.PaymentModeId=PM.PaymentMode
			LEFT JOIN Tbl_CustomerPayments CP ON 
			CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) =	CONVERT(VARCHAR(50),CP.RecievedDate,101) 
			and (Isnull(CD.GlobalAccountNumber,0) = isnull(CP.AccountNo,0))
			
	
	
		FOR XML PATH('PaymentsList'),TYPE      
	)    
	FOR XML PATH(''),ROOT('PaymentsInfoByXml')  
	      
END   
------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [MASTERS].[USP_IsMeterAvailable_New]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Modified by : Satya  
-- Modified Date: 6-APRIL-2014    
-- DESC   : MODIFIED CONDITION WITH IS NULL  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_IsMeterAvailable_New]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterNo VARCHAR(100)    
   ,@BUID VARCHAR(50)    
   ,@CustomerTypeId INT    
       
  SELECT       
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')    
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
    
  Declare @MeterAvaiable bit = 1  
		  ,@MeterDials INT
  
  IF NOT EXISTS (SELECT M.MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2  
      AND M.ActiveStatusId IN(1)   
      AND (BU_ID=@BUID OR @BUID='')        
      AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE
   BEGIN
		SELECT @MeterDials=M.MeterDials FROM Tbl_MeterInformation M WHERE MeterNo=@MeterNo
   END
  SELECT @MeterAvaiable AS IsSuccess, @MeterDials AS MeterDials
  FOR XML PATH('MastersBE'),TYPE      
       
END    

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)

	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END

			SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

			--SELECT @PresentRoleId = PresentRoleId 
			--		,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)

			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
				,@NewMeterReadingDate
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,@PreviousReading
						,@OldMeterReading
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsToApprove]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.AddressChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldPostal_HouseNo
		 ,T.NewPostal_HouseNo
		 ,T.OldPostal_City
		 ,T.NewPostal_City
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerAddressChangeLog_New T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterNoChangeLogsToApprove]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Meter NO Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 7 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.MeterInfoChangeLogId
		 ,T.AccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,OldMeterNo
		 ,NewMeterNo
		 ,OldMeterReading
		 ,NewMeterInitialReading
		 ,CONVERT(VARCHAR(20),MeterChangedDate,106) AS MeterChangedDate
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerMeterInfoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterReadingsReport]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(50) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, U.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@TrxTypes,',')) TU ON TU.TypeId = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	  
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(AverageReading AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress AS ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading_New]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteekk
-- Create date: 15 Apr 2015  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading_New]  
(
	@MultiXmlDoc XML
)  
AS  
BEGIN  
  
	DECLARE @TempCustomer TABLE(AccountNo VARCHAR(50),       
							 CurrentReading varchar(50),      
							 ReadDate DATETIME)     
            
	INSERT INTO @TempCustomer(AccountNo ,CurrentReading,ReadDate)       
	SELECT       
		  RTRIM(LTRIM(c.value('(Account_Number)[1]','VARCHAR(50)')))       
		  ,(CASE c.value('(Current_Reading)[1]','VARCHAR(50)') WHEN '' THEN NULL ELSE c.value('(Current_Reading)[1]','VARCHAR(50)') END)      
		  ,CONVERT(DATETIME,c.value('(Read_Date)[1]','varchar(50)'))  
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table') AS T(c)   
	
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.AccountNo
		,CMI.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CMI.Title,CMI.FirstName,CMI.MiddleName,CMI.LastName) as Name
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		--,COUNT(0) AS CustomerCount
	INTO #CustomersListBook
	FROM @TempCustomer T
	INNER JOIN UDV_CustomerMeterInformation CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	--and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	

	SELECT
		(     		
		SELECT 
			 CB.GlobalAccountNumber AS AccNum
			,CB.AccountNo
			,CB.OldAccountNo
			,CB.Name
			,CB.MeterNumber AS MeterNo
			,CB.MeterDials
			,CB.Decimals
			,CB.ActiveStatusId
			,CONVERT(VARCHAR(20),CB.ReadDate,106) AS ReadDate
			,CB.ReadCodeID AS ReadCodeId
			,CB.BU_ID
			,CB.MarketerId
			,(CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - CONVERT(DECIMAL(18,2),ISNULL(CustomerReadings.PresentReading,CustomerReadings.PreviousReading))) AS Usage
			,CB.CurrentReading AS PresentReading
			,ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading
			--,ISNULL(CustomerReadings.PresentReading,ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))) AS PreviousReading
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,0) end) AS PreviousReading
			,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
			,CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106) AS LatestDate
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadDate) then 1  
				else 0 end) as PrvExist
			,(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadDate) then 1 
				else 0 end) as IsExists
			,(Case when CustomerReadings.IsBilled IS NULL then 0
				else CustomerReadings.IsBilled end) as IsBilled
			--,CustomerCount
			,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
		FROM #CustomersListBook CB
		LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber
		FOR XML PATH('BillingBE'),TYPE  
		)  
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@IsRollover)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@IsRollover)
									
				END
		END
		
 
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT
		,@Rollover BIT
		,@Previous varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@Count = C.value('(TotalReadings)[1]','INT')
		,@Avg = C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@Rollover = C.value('(Rollover)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@Previous = C.value('(PreviousReading)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @ReadId INT
	Declare	 @Dials INT
	Declare	  @Multipler INT
	Declare @IsExistingRollOver bit
	Declare	   @MeterNumber varchar(50),@FirstPrevReading varchar(50),@FirstPresentValue varchar(50) 
	 ,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
	 
		,@SecoundReadingPrevReading varchar(20)
		,@AverageReading VARCHAR(50)
		 
	SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=IsRollOver,@MeterNumber=MeterNumber
	,@Dials	= LEN(PreviousReading) ,@ReadDate=ReadDate 
	FROM Tbl_CustomerReadings
	WHERE GlobalAccountNumber = @AccNum
	ORDER BY CustomerReadingId DESC

		 
	IF @IsExistingRollOver = 1
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
	END
	ELSE
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId    
	END
	 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
		 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	Declare @FirstReadingUsage bigint =@Usage
	Declare @SecountReadingUsage Bigint =0
	SET @FirstPrevReading   = @Previous
	SET @FirstPresentValue    =	 @Current
	
	IF @Rollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
	--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		 
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@Rollover
		)
	IF(@Rollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage 
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@Rollover)
									
					 
				END
			
 
		END
	
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Previous 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum
	

	 

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
	   
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_Bookwise]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_Bookwise] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
	dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
	,MeterNumber as CustomerExistingMeterNumber
	,MeterDials As CustomerExistingMeterDials
	,Decimals as Decimals
	,InitialReading as	InitialReading
	,AccountNo
	,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeId = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
----------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			CustomerBills.BillGeneratedDate as EstimatedBillDate,
			0 AS IsActiveMonth,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExists,
			ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--') as LatestDate,
			CustomerList.Name,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			CustomerReadings.IsTamper,
			0 as Decimals,
			'' AS BillNo,
			ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading,
			CustomerReadings.TotalReadings,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then NULL else CustomerReadings.PresentReading end) 
				else NULL End as  PresentReading,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end   as PrvExist,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end AS Usage,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading else CustomerReadings.PreviousReading end)  	
				else isnull(CustomerList.InitialReading,0) End as PreviousReading,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then CONVERT(VARCHAR(10),CustomerBills.ReadType) else '--' end)  LastBillReadType
			,CustomerList.BookNo,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	 
			LEFT JOIN	  
			#CustomerLatestBills as CustomerBills 
			ON CustomerBills.GlobalAccountNumber=CustomerList.GlobalAccountNumber
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table  #CustomersListBook   
DROP Table  #CustomerLatestBills
DROP Table  #CusotmersWithMaxBillID
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/21/2015 22:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole)  
					SELECT  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadings_Bulk]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 20-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertMeterReadings_Bulk]
(
	 @XmlDoc Xml
	,@MultiXmlDoc Xml
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@MeterReadingFrom INT
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess INT = 0

	SELECT @MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		--,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
		--,@ReadDate = C.value('(ReadDate)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), TotalReadings INT, AverageReading VARCHAR(50),           
							PresentReading VARCHAR(50), Usage VARCHAR(50), AccountNo VARCHAR(50),
							IsTamper BIT, PreviousReading VARCHAR(50), IsExists BIT, ReadDate VARCHAR(50), ReadBy Varchar(50))
	
	INSERT INTO @TempCustomer(TotalReadings, AverageReading, PresentReading, Usage, AccountNo, IsTamper, PreviousReading, IsExists, ReadDate, ReadBy)           
	SELECT         
		 c.value('(TotalReadings)[1]','INT')
		,c.value('(AverageReading)[1]','VARCHAR(50)')
		,c.value('(PresentReading)[1]','VARCHAR(50)')
		,CONVERT(NUMERIC(20,4),c.value('(Usage)[1]','VARCHAR(50)'))
		,c.value('(AccNum)[1]','varchar(50)')
		,c.value('(IsTamper)[1]','BIT')
		,c.value('(PreviousReading)[1]','varchar(50)')
		,c.value('(IsExists)[1]','BIT')
		,c.value('(ReadDate)[1]','varchar(50)')
		,c.value('(ReadBy)[1]','varchar(50)')
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c) 
	
	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = Usage
				,@AccountNo = AccountNo
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
			 FROM @TempCustomer WHERE SNo = @SNo
			
			SELECT @Multiple = MI.MeterMultiplier
				,@MeterNumber = CPD.MeterNumber
				,@Dials=MI.MeterDials 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
			INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccountNo
			
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PreviousReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = @IsSuccess + 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			
		END
		
	SELECT @IsSuccess AS IsSuccess 
	FOR XML PATH('BillingBE')
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogs]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
	--SELECT(  
		 SELECT  --CMI.AccountNo
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
				,CMI.OldMeterNo
				,CMI.NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
			   ,CMI.Remarks  
			   ,CMI.OldDials  
			   ,CMI.NewDials  
			   ,CMI.CreatedBy  
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CMI.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		       ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name 
			   ,CustomerView.OldAccountNo   
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		-- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '')  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId   
		 -- changed by karteek end    
	--	FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
-----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 06-10-2014   
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015      
-- Description: The purpose of this procedure is to get limited Meter change request logs    
-- =============================================       
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''    
      
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
    
	-- start By Karteek  
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek   
      
	SELECT(    
		 SELECT TOP 10 CMI.AccountNo
			   ,CMI.OldMeterNo
			   ,CMI.NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType    
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType    
			   ,CMI.Remarks    
			   ,CMI.OldDials    
			   ,CMI.NewDials    
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate     
			   ,ApproveSttus.ApprovalStatus    
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name    
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI    
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber    
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)    
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID     
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID  
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId  
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId  
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo  
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId  
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId      
		 -- changed by karteek end     
		 GROUP BY CMI.AccountNo    
			,OldMeterTypeId
			,OldMeterNo
			,NewMeterNo
		    ,NewMeterTypeId    
		    ,CMI.Remarks    
			,CMI.OldDials    
		    ,CMI.NewDials    
		    ,CONVERT(VARCHAR(30),CMI.CreatedDate,107)    
		    ,ApproveSttus.ApprovalStatus    
		    ,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName    
		FOR XML PATH('AuditTrayList'),TYPE    
	)    
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
	
END   
---------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBookNumberDetails]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 26-02-2014    
-- Description: The purpose of this procedure is to update Book Number details    
-- ModifiedBy: V.BhimaRaju    
--Modified Date:27-02-2014    
--Modified Date:29-03-2014    
-- ModifiedBy : T.Karthik    
-- Modified date: 01-12-2014    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_UpdateBookNumberDetails]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @ID VARCHAR(20)  
 ,@ModifiedBy VARCHAR(50)  
 ,@NoOfAccounts INT  
 ,@CycleId VARCHAR(20)  
 --,@ServiceCenterIdNew VARCHAR(20)  
 ,@BookNo Varchar(20)  
 --@BU_ID VARCHAR(20)    
 --,@SU_ID VARCHAR(20)    
 ,@ServiceCenterId VARCHAR(20)    
 ,@Details VARCHAR(MAX)    
 ,@CurrentDate DATETIME ,@BookCode VARCHAR(10),@MarketerId INT    
 ,@OldServiceCenterId varchar(20)  
 SELECT @CurrentDate  = dbo.fn_GetCurrentDateTime()    
     
 SELECT    
  @ID=C.value('(BookNo)[1]','VARCHAR(20)')    
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
  ,@NoOfAccounts=C.value('(NoOfAccounts)[1]','INT')    
  ,@MarketerId=C.value('(MarketerId)[1]','INT')    
  --,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')    
  --,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(20)')    
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(20)')    
  ,@CycleId=C.value('(CycleId)[1]','VARCHAR(20)')    
  --,@BookCode=C.value('(BookCode)[1]','VARCHAR(10)')    
  ,@Details=C.value('(Details)[1]','VARCHAR(MAX)')    
  ,@OldServiceCenterId=C.value('(OldSeviceCenterID)[1]','VARCHAR(20)')  
  
  ,@BookNo = C.value('(BNO)[1]','VARCHAR(20)')   
    
 FROM @XmlDoc.nodes('MastersBE') as T(C)    
     
 --IF EXISTS(SELECT 0 FROM Tbl_BookNumbers WHERE BookCode=@BookCode AND BookNo != @BookNo AND CycleId = @CycleId)  
 IF EXISTS(SELECT 0 FROM Tbl_BookNumbers WHERE BookNo!=@BookNO AND ID = @ID AND CycleId = @CycleId)  
 BEGIN    
  SELECT 1 AS IsBookCodeExists FOR XML PATH('MastersBE')    
 END    
 ELSE    
 BEGIN    
 --===================  
 DECLARE @OldCycleId VARCHAR(50)  
   
 SELECT @OldCycleId = CycleId FROM Tbl_Cycles   
 WHERE ServiceCenterId = @OldServiceCenterId  
 AND CycleId IN(SELECT CycleId FROM Tbl_BookNumbers WHERE BookNo = @BookNO)  
   
--set @ServiceCenterIdNew = (select top 1 ServiceCenterId from Tbl_Cycles where CycleId = @CycleId)  
-- set @ServiceCenterId = (select top 1 ServiceCenterId from Tbl_Cycles   
--       where CycleId = (SELECT top 1 CycleId from Tbl_BookNumbers   
--            where ID=@ID)  
--       )  
--===================  
  IF (@ServiceCenterId = @OldServiceCenterId)  
Begin  
 SET @BookCode = (Select BookCode from Tbl_BookNumbers where BookNo = @BookNo and CycleId = @OldCycleId)   
end  
else  
Begin  
 SET @BookCode = MASTERS.fn_BookCodeGenerate(@ServiceCenterId)   
end  
    
   UPDATE Tbl_BookNumbers   
   SET 
   ID=@ID
   ,ModifiedBy=@ModifiedBy  
   ,ModifiedDate=@CurrentDate  
   ,NoOfAccounts=CASE @NoOfAccounts WHEN 0 THEN NULL ELSE @NoOfAccounts END    
    --,BU_ID=@BU_ID    
    --,SU_ID=@SU_ID    
    --,ServiceCenterId=@ServiceCenterId    
    ,Details=CASE @Details WHEN '' THEN NULL ELSE @Details END    
    ,CycleId=@CycleId    
    ,BookCode=@BookCode    
    ,MarketerId=@MarketerId    
   WHERE BookNo=@BookNo and CycleId = @OldCycleId    
   --AND ServiceCenterId=@ServiceCenterId     
       
   --INSERT INTO Tbl_Audit_BookNumbers(BookNo,ID,CycleId,ModifiedBy,ModifiedDate    
   --           ,BU_ID,SU_ID,ServiceCenterId,NoOfAccounts,Details,BookCode    
   --           ,StatusId,ActiveStatusId)    
   --       VALUES(@BookNo,NULL,@CycleId,@ModifiedBy,@CurrentDate,@BU_ID,@SU_ID,@ServiceCenterId,    
   --       (CASE @NoOfAccounts WHEN 0 THEN NULL ELSE @NoOfAccounts END),(CASE @Details WHEN '' THEN NULL ELSE @Details END),    
   --       @BookCode,'U',1) --For Audit Table Updation    
       
    
   SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')    
 END    
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsWithLimit]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014   
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
---- Description: The purpose of this procedure is to get limited Tariff change request logs  
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 TCR.AccountNo  
			,TCR.ApprovalStatusId  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
			,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		    ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
			,TCR.Remarks  
			,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_LCustomerTariffChangeRequest  TCR  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId   
		--GROUP BY TCR.AccountNo  
		--,TCR.ApprovalStatusId  
		--,PreviousTariffId  
		--,ChangeRequestedTariffId  
		--,TCR.Remarks  
		--,CONVERT(VARCHAR(30),TCR.CreatedDate,107)  
		--,ApproveSttus.ApprovalStatus  
		--,CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName  
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek  
     
	--SELECT(  
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy  
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 -- changed by karteek start  
		 --AND CustomerView.BU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @BU_ID WHEN '' THEN CustomerView.BU_ID ELSE @BU_ID END) ,','))  
		 --AND CustomerView.SU_ID IN(SELECT [com] FROM dbo.fn_Split((CASE @SU_ID WHEN '' THEN CustomerView.SU_ID ELSE @SU_ID END),','))  
		 --AND CustomerView.ServiceCenterId IN(SELECT [com] FROM dbo.fn_Split((CASE @SC_ID WHEN '' THEN CustomerView.ServiceCenterId ELSE @SC_ID END),','))  
		 --AND CustomerView.CycleId IN(SELECT [com] FROM dbo.fn_Split((CASE @CycleId WHEN '' THEN CustomerView.CycleId ELSE @CycleId END),','))  
		 --AND CustomerView.BookNo IN(SELECT [com] FROM dbo.fn_Split((CASE @BookNo WHEN '' THEN CustomerView.BookNo ELSE @BookNo END),','))  
		 --AND (CustomerView.TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffId,',')) OR @TariffId= '') 
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 -- changed by karteek end    
	--	 FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_IsExistsTariffEnergies_Cycle]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 01 Nov 2014
-- Description  : To Check the Energy charges are Exists or not to Cycles based on Month
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsExistsTariffEnergies_Cycle]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@CycleIds VARCHAR(MAX)
			,@CycleId VARCHAR(50)
			,@ToDate DATETIME
			,@MonthStart DATETIME
			,@MonthEnd DATETIME
			,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
			
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
			
	DECLARE @TempCycles TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))		
	DECLARE @NotEstimatedCycles TABLE(TariffId INT)		
			
	SET @MonthStart = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
	SET @MonthEnd = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStart))))   

	IF(@MonthEnd > @CurrentDate)
		SELECT @ToDate = @CurrentDate
	ELSE
		SET @ToDate = @MonthEnd		

	DELETE FROM @TempCycles
	DELETE FROM @NotEstimatedCycles
	
	INSERT INTO @TempCycles
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
		
	DECLARE @ValidTariffsTable TABLE(TariffId INT)		 

	INSERT INTO @ValidTariffsTable
	SELECT DISTINCT TariffClassID  FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD
	INNER JOIN Tbl_BookNumbers B ON B.BookNo = CPD.BookNo 
	INNER JOIN Tbl_Cycles CY ON CY.CycleId=B.CycleId
	INNER JOIN @TempCycles TC ON TC.CycleId=CY.CycleId
	WHERE CY.CycleId IN   (SELECT C.CycleId
	  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
	  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
	  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
	  AND C.CycleId=CY.CycleId GROUP BY C.CycleId HAVING COUNT(CPD.CustomerProcedureId)>1)


INSERT INTO @NotEstimatedCycles
	SELECT TariffId FROM @ValidTariffsTable
	EXCEPT
	SELECT EC.ClassID FROM  Tbl_LEnergyClassCharges EC
	INNER JOIN Tbl_MTariffClasses  TC ON TC.ClassID = EC.ClassID 
	WHERE IsActiveClass = 1  AND @ToDate BETWEEN FromDate AND Todate
	AND EC.ClassID IN (SELECT TariffId from @ValidTariffsTable)	

	
	IF EXISTS (SELECT 0 FROM @NotEstimatedCycles)
		BEGIN
			SELECT
			(
				SELECT 
					0 AS IsExists
					,R.TariffId
					,C.ClassName AS TariffName
				FROM @NotEstimatedCycles R
				LEFT JOIN Tbl_MTariffClasses C ON R.TariffId = C.ClassID	
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					1 AS IsExists
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSCWithBillingStatus]    Script Date: 04/21/2015 22:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================        
-- Author:  NEERAJ    
-- Create date: 10-FEB-2015           
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC with billing status.    
-- Modified date: 16-FEB-2015           
-- Description: Added new field total customers  
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSCWithBillingStatus]        
(        
@XmlDoc xml        
)        
AS        
BEGIN        
        
 DECLARE @BU_ID VARCHAR(20)    
   ,@SU_ID VARCHAR(MAX)    
   ,@ServiceCenterId VARCHAR(MAX)     
   ,@BillYear VARCHAR(MAX)    
   ,@BillMonth VARCHAR(MAX)       
          
 SELECT        
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')        
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')        
   ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')        
   ,@BillYear=C.value('(BillYear)[1]','VARCHAR(20)')          
   ,@BillMonth=C.value('(BillMonth)[1]','VARCHAR(20)')          
     
     
 FROM @XmlDoc.nodes('MastersBE') as T(C)        
         
 SELECT        
 (        
    SELECT        
   CycleId        
   ,(CycleName + ' ('+CycleCode+') - ' + SC.ServiceCenterName ) as CycleName
   ,(select COUNT(0)     
     from Tbl_CustomerBills as cb     
     where  cb.BillMonth=@BillMonth     
       and cb.BillYear=@BillYear     
       AND cb.BU_ID=BU.BU_ID    
       AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
       AND CB.CycleId=CY.CycleId) AS BillGenCount    
   ,(select COUNT(0)     
     from Tbl_CustomerBillPayments as bp JOIN Tbl_CustomerBills AS CB ON bp.BillNo=cb.BillNo    
     where  cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS PaymentCount       
  ,(select COUNT(0)     
     from Tbl_CustomerBills AS CB     
     JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId    
     where cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS BillAdjustmentCount      
   ,(SELECT COUNT(0)    
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=CY.CycleId) AS TotalCustomers  
  FROM Tbl_Cycles cy    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
  WHERE CY.ActiveStatusId=1        
  AND (BU.BU_ID=@BU_ID OR @BU_ID='')        
  AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')        
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')     
  
    AND CY.CycleId IN
  (SELECT C.CycleId
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=CY.CycleId GROUP BY C.CycleId HAVING COUNT(CPD.CustomerProcedureId)>1)
     
  ORDER BY CycleName ASC       
  FOR XML PATH('MastersBE'),TYPE        
 )        
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
END 



GO



GO
/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/22/2015 11:22:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole)  
					SELECT  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsToApprove]    Script Date: 04/22/2015 11:23:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.AddressChangeLogId
		 ,T.GlobalAccountNumber AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldPostal_HouseNo
		 ,T.NewPostal_HouseNo
		 ,T.OldPostal_City
		 ,T.NewPostal_City
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + NR.RoleName ELSE APS.ApprovalStatus + ' By ' + NR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerAddressChangeLog_New T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
