
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadings_Bulk]    Script Date: 04/21/2015 12:06:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 20-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertMeterReadings_Bulk]
(
	 @XmlDoc Xml
	,@MultiXmlDoc Xml
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@MeterReadingFrom INT
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess INT = 0

	SELECT @ReadDate = C.value('(ReadDate)[1]','varchar(50)')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), TotalReadings INT, AverageReading VARCHAR(50),           
							PresentReading VARCHAR(50), Usage VARCHAR(50), AccountNo VARCHAR(50),
							IsTamper BIT, PreviousReading VARCHAR(50), IsExists BIT)
	
	INSERT INTO @TempCustomer(TotalReadings, AverageReading, PresentReading, Usage, AccountNo, IsTamper, PreviousReading, IsExists)           
	SELECT         
		 c.value('(TotalReadings)[1]','INT')
		,c.value('(AverageReading)[1]','VARCHAR(50)')
		,c.value('(PresentReading)[1]','VARCHAR(50)')
		,CONVERT(NUMERIC(20,4),c.value('(Usage)[1]','VARCHAR(50)'))
		,c.value('(AccNum)[1]','varchar(50)')
		,c.value('(IsTamper)[1]','BIT')
		,c.value('(PreviousReading)[1]','varchar(50)')
		,c.value('(IsExists)[1]','BIT')
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c) 
	
	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = Usage
				,@AccountNo = AccountNo
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
			 FROM @TempCustomer WHERE SNo = @SNo
			
			SELECT @Multiple = MI.MeterMultiplier
				,@MeterNumber = CPD.MeterNumber
				,@Dials=MI.MeterDials 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
			INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccountNo
			
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PreviousReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = @IsSuccess + 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			
		END
		
	SELECT @IsSuccess AS IsSuccess 
	FOR XML PATH('BillingBE')
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_IsExistsTariffEnergies_Cycle]    Script Date: 04/21/2015 12:06:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 01 Nov 2014
-- Description  : To Check the Energy charges are Exists or not to Cycles based on Month
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsExistsTariffEnergies_Cycle]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@CycleIds VARCHAR(MAX)
			,@CycleId VARCHAR(50)
			,@ToDate DATETIME
			,@MonthStart DATETIME
			,@MonthEnd DATETIME
			,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
			
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
			
	DECLARE @TempCycles TABLE(Id INT IDENTITY(1,1),CycleId VARCHAR(50))		
	DECLARE @NotEstimatedCycles TABLE(TariffId INT)		
			
	SET @MonthStart = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
	SET @MonthEnd = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStart))))   

	IF(@MonthEnd > @CurrentDate)
		SELECT @ToDate = @CurrentDate
	ELSE
		SET @ToDate = @MonthEnd		

	DELETE FROM @TempCycles
	DELETE FROM @NotEstimatedCycles
	
	INSERT INTO @TempCycles
	SELECT [com] FROM dbo.fn_Split(@CycleIds,',')
		
	DECLARE @ValidTariffsTable TABLE(TariffId INT)		 

	INSERT INTO @ValidTariffsTable
	SELECT DISTINCT TariffClassID  FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD
	INNER JOIN Tbl_BookNumbers B ON B.BookNo = CPD.BookNo 
	INNER JOIN Tbl_Cycles CY ON CY.CycleId=B.CycleId
	INNER JOIN @TempCycles TC ON TC.CycleId=CY.CycleId
	WHERE CY.CycleId IN   (SELECT C.CycleId
	  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
	  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
	  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
	  AND C.CycleId=CY.CycleId GROUP BY C.CycleId HAVING COUNT(CPD.CustomerProcedureId)>1)


INSERT INTO @NotEstimatedCycles
	SELECT TariffId FROM @ValidTariffsTable
	EXCEPT
	SELECT EC.ClassID FROM  Tbl_LEnergyClassCharges EC
	INNER JOIN Tbl_MTariffClasses  TC ON TC.ClassID = EC.ClassID 
	WHERE IsActiveClass = 1  AND @ToDate BETWEEN FromDate AND Todate
	AND EC.ClassID IN (SELECT TariffId from @ValidTariffsTable)	

	
	IF EXISTS (SELECT 0 FROM @NotEstimatedCycles)
		BEGIN
			SELECT
			(
				SELECT 
					0 AS IsExists
					,R.TariffId
					,C.ClassName AS TariffName
				FROM @NotEstimatedCycles R
				LEFT JOIN Tbl_MTariffClasses C ON R.TariffId = C.ClassID	
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')    
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					1 AS IsExists
				FOR XML PATH('BillGenerationList'),TYPE    
			)    
			FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSCWithBillingStatus]    Script Date: 04/21/2015 12:06:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================        
-- Author:  NEERAJ    
-- Create date: 10-FEB-2015           
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC with billing status.    
-- Modified date: 16-FEB-2015           
-- Description: Added new field total customers  
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSCWithBillingStatus]        
(        
@XmlDoc xml        
)        
AS        
BEGIN        
        
 DECLARE @BU_ID VARCHAR(20)    
   ,@SU_ID VARCHAR(MAX)    
   ,@ServiceCenterId VARCHAR(MAX)     
   ,@BillYear VARCHAR(MAX)    
   ,@BillMonth VARCHAR(MAX)       
          
 SELECT        
    @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')        
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')        
   ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')        
   ,@BillYear=C.value('(BillYear)[1]','VARCHAR(20)')          
   ,@BillMonth=C.value('(BillMonth)[1]','VARCHAR(20)')          
     
     
 FROM @XmlDoc.nodes('MastersBE') as T(C)        
         
 SELECT        
 (        
    SELECT        
   CycleId        
   ,(CycleName + ' ('+CycleCode+') - ' + SC.ServiceCenterName ) as CycleName
   ,(select COUNT(0)     
     from Tbl_CustomerBills as cb     
     where  cb.BillMonth=@BillMonth     
       and cb.BillYear=@BillYear     
       AND cb.BU_ID=BU.BU_ID    
       AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
       AND CB.CycleId=CY.CycleId) AS BillGenCount    
   ,(select COUNT(0)     
     from Tbl_CustomerBillPayments as bp JOIN Tbl_CustomerBills AS CB ON bp.BillNo=cb.BillNo    
     where  cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS PaymentCount       
  ,(select COUNT(0)     
     from Tbl_CustomerBills AS CB     
     JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId    
     where cb.BillMonth=@BillMonth     
     and cb.BillYear=@BillYear     
     AND cb.BU_ID=BU.BU_ID      
     AND (CB.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')    
     AND CB.CycleId=CY.CycleId) AS BillAdjustmentCount      
   ,(SELECT COUNT(0)    
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=CY.CycleId) AS TotalCustomers  
  FROM Tbl_Cycles cy    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
  WHERE CY.ActiveStatusId=1        
  AND (BU.BU_ID=@BU_ID OR @BU_ID='')        
  AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')        
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')     
  
    AND CY.CycleId IN
  (SELECT C.CycleId
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD  
  JOIN Tbl_BookNumbers AS BN ON CPD.BookNo=BN.BookNo  
  JOIN Tbl_Cycles AS C ON C.CycleId=BN.CycleId  
  AND C.CycleId=CY.CycleId GROUP BY C.CycleId HAVING COUNT(CPD.CustomerProcedureId)>1)
     
  ORDER BY CycleName ASC       
  FOR XML PATH('MastersBE'),TYPE        
 )        
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
END 



GO


