
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD PresentApprovalRole INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD NextApprovalRole INT
GO

CREATE TABLE Tbl_Audit_CustomerAddressChangeLog(
	AddressChangeLogId INT,
	GlobalAccountNumber VARCHAR(50) ,
	OldPostal_HouseNo VARCHAR(100) ,
	OldPostal_StreetName VARCHAR(255) ,
	OldPostal_City VARCHAR(100) ,
	OldPostal_Landmark VARCHAR(100) ,
	OldPostal_AreaCode VARCHAR(100) ,
	OldPostal_ZipCode VARCHAR(100) ,
	NewPostal_HouseNo VARCHAR(100) ,
	NewPostal_StreetName VARCHAR(255) ,
	NewPostal_City VARCHAR(100) ,
	NewPostal_Landmark VARCHAR(100) ,
	NewPostal_AreaCode VARCHAR(100) ,
	NewPostal_ZipCode VARCHAR(100) ,
	OldService_HouseNo VARCHAR(100) ,
	OldService_StreetName VARCHAR(255) ,
	OldService_City VARCHAR(100) ,
	OldService_Landmark VARCHAR(100) ,
	OldService_AreaCode VARCHAR(100) ,
	OldService_ZipCode VARCHAR(100) ,
	NewService_HouseNo VARCHAR(100) ,
	NewService_StreetName VARCHAR(255) ,
	NewService_City VARCHAR(100) ,
	NewService_Landmark VARCHAR(100) ,
	NewService_AreaCode VARCHAR(100) ,
	NewService_ZipCode VARCHAR(100) ,
	ApprovalStatusId INT ,
	Remarks VARCHAR(MAX) ,
	CreatedBy VARCHAR(50) ,
	CreatedDate DATETIME ,
	ModifiedBy VARCHAR(50) ,
	ModifiedDate DATETIME ,
	PresentApprovalRole INT,
	NextApprovalRole INT
	)

GO
