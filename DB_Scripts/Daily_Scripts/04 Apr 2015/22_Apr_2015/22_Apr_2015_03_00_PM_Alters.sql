GO
CREATE TYPE [dbo].[TblPaymentsUpload] AS TABLE(
	 SNO INT
	,AccountNo VARCHAR(50)
	,AmountPaid DECIMAL(18,2)
	,ReceiptNO VARCHAR(50)     
	,ReceivedDate DATETIME
	,PaymentModeId VARCHAR(50)
)
GO
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD OldServiceAddressID INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD NewServiceAddressID INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD OldPostalAddressID INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD NewPostalAddressID INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD OldIsSameAsService INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD NewIsSameAsService INT
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD OldServiceAddressID INT
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD NewServiceAddressID INT
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD OldPostalAddressID INT
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD NewPostalAddressID INT
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD OldIsSameAsService INT
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD NewIsSameAsService INT
GO
