
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAddressChangeApproval]    Script Date: 04/22/2015 14:55:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerAddressChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@AddressChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT

	SELECT  
		 @AddressChangeLogId = C.value('(AddressChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNumber 
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		
		FROM Tbl_CustomerAddressChangeLog_New
		WHERE AddressChangeLogId=@AddressChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService
										)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerAddressChangeLog_New
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					AND  AddressChangeLogId= @AddressChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService
										)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
																WHERE AddressChangeLogId = @AddressChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
						WHERE AddressChangeLogId = @AddressChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerAddressChangeLog_New
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			AND AddressChangeLogId = @AddressChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload_WithDT]    Script Date: 04/22/2015 14:55:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
--Description: to validate bulk payments upload
--created By : Karteek
--created Date 22-04-2015
-- =============================================          
CREATE PROCEDURE [dbo].[USP_ValidatePaymentUpload_WithDT]          
(  
	 @Bu_Id VARCHAR(50)  
	,@TblPayments TblPaymentsUpload READONLY  
)          
AS          
BEGIN          
      
						
	SELECT 
		 ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
		,CD.GlobalAccountNumber  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,TempDetails.AmountPaid AS PaidAmount    
		,TempDetails.ReceiptNO    
		,TempDetails.ReceivedDate   
		,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
		,Case When (TempDetails.AmountPaid = isnull(CP.PaidAmount,0)  
					and TempDetails.ReceiptNO = isnull(CP.ReceiptNo,0))
			THEn 1
			else 0 end IsDuplicate 
		,case when BookDetails.BU_ID  = @Bu_Id then 1 else 0  end as IsExists 
		,CONVERT(VARCHAR(10),isnull(PM.PaymentModeId,''))+' - '+ isnull(PM.PaymentMode,'')	  as PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId 
	FROM @TblPayments AS TempDetails     
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (CD.GlobalAccountNumber= CPD.GlobalAccountNumber )
	INNER JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = CPD.BookNo
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentModeId=PM.PaymentMode
	LEFT JOIN Tbl_CustomerPayments(NOLOCK) CP ON 
	CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) =	CONVERT(VARCHAR(50),CP.RecievedDate,101) 
	and (Isnull(CD.GlobalAccountNumber,0) = isnull(CP.AccountNo,0))
			
END   
------------------------------------------------------------------------------------------------------------------

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_ValidatePaymentUpload_WithDT]    Script Date: 04/22/2015 14:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
--Description: to validate bulk payments upload
--created By : Karteek
--created Date 22-04-2015
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload_WithDT]          
(  
	 @Bu_Id VARCHAR(50)  
	,@TblPayments TblPaymentsUpload READONLY  
)          
AS          
BEGIN          
      
						
	SELECT 
		 ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
		,CD.GlobalAccountNumber  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,TempDetails.AmountPaid AS PaidAmount    
		,TempDetails.ReceiptNO    
		,TempDetails.ReceivedDate   
		,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
		,Case When (TempDetails.AmountPaid = isnull(CP.PaidAmount,0)  
					and TempDetails.ReceiptNO = isnull(CP.ReceiptNo,0))
			THEn 1
			else 0 end IsDuplicate 
		,case when BookDetails.BU_ID  = @Bu_Id then 1 else 0  end as IsExists 
		,CONVERT(VARCHAR(10),isnull(PM.PaymentModeId,''))+' - '+ isnull(PM.PaymentMode,'')	  as PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId 
	FROM @TblPayments AS TempDetails     
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (CD.GlobalAccountNumber= CPD.GlobalAccountNumber )
	INNER JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = CPD.BookNo
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentModeId=PM.PaymentMode
	LEFT JOIN Tbl_CustomerPayments(NOLOCK) CP ON 
	CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) =	CONVERT(VARCHAR(50),CP.RecievedDate,101) 
	and (Isnull(CD.GlobalAccountNumber,0) = isnull(CP.AccountNo,0))
			
END   
------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalFunctions]    Script Date: 04/22/2015 14:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 27-11-2014
-- Description:	The purpose of this procedure is to get approval functions
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalFunctions]
AS
BEGIN
	SELECT
	(
		SELECT
			[Function]
			,FunctionId
		FROM TBL_FunctionalAccessPermission
		--WHERE IsActive=1
		ORDER BY [Function] ASC
		FOR XML PATH('ApprovalsList'),TYPE
	)
	FOR XML PATH(''),ROOT('ApprovalsInfoByXml')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/22/2015 14:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie																
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole)  
					SELECT  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAddressChangeApproval]    Script Date: 04/22/2015 14:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAddressChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@AddressChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT

	SELECT  
		 @AddressChangeLogId = C.value('(AddressChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNumber 
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		
		FROM Tbl_CustomerAddressChangeLog_New
		WHERE AddressChangeLogId=@AddressChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService
										)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerAddressChangeLog_New
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					AND  AddressChangeLogId= @AddressChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService
										)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
																WHERE AddressChangeLogId = @AddressChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
						WHERE AddressChangeLogId = @AddressChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerAddressChangeLog_New
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			AND AddressChangeLogId = @AddressChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments]    Script Date: 04/22/2015 14:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 10-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBulkCustomerPayments]          
(          
	 @XmlDoc XML           
	,@MultiXmlDoc XML          
)          
AS          
BEGIN    

DECLARE @CreatedBy VARCHAR(50)    
	,@DocumentPath VARCHAR(MAX)          
	,@DocumentName VARCHAR(MAX)         
	,@BatchNo VARCHAR(50) 
	,@StatusText VARCHAR(200) = ''
	,@IsSuccess BIT = 0
	,@IsBillExist BIT 
	
	SELECT            
		 @CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')          
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')          
		,@DocumentName = C.value('(DocumentName)[1]','VARCHAR(MAX)')        
		,@BatchNo = C.value('(BatchNo)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBe') AS T(C)    	

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), AccountNo VARCHAR(50), ReceiptNo VARCHAR(20),           
							PaymentMode VARCHAR(20), PaymentModeId INT, 
							AmountPaid DECIMAL(18,2), RecievedDate DATETIME)
	
	INSERT INTO @TempCustomer(AccountNo, ReceiptNo, PaymentMode, PaymentModeId, AmountPaid, RecievedDate)           
	SELECT           
		 T.c.value('(AccountNo)[1]','VARCHAR(50)')           
		,T.c.value('(ReceiptNO)[1]','VARCHAR(20)')           
		,T.c.value('(PaymentMode)[1]','VARCHAR(20)')          
		,T.c.value('(PaymentModeId)[1]','VARCHAR(20)')          
		,T.c.value('(PaidAmount)[1]','DECIMAL(18,2)')          
		,T.c.value('(PaymentDate)[1]','DATETIME')          
	FROM @MultiXmlDoc.nodes('//MastersBEInfoByXml/Table1') AS T(c)    
							
	DECLARE @SNo INT, @TotalCount INT
			,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
			,@CustomerPaymentId INT
			
	DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
	SET @SNo = 1
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer    
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SELECT @PresentAccountNo = AccountNo, @PaidAmount = AmountPaid FROM @TempCustomer WHERE SNo = @SNo
					
					SET @StatusText = 'Customer Payments'
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 AccountNo
						,ReceiptNo
						,PaymentModeId
						,@DocumentPath
						,@DocumentName
						,RecievedDate
						,2 -- For Bulk Upload
						,AmountPaid
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempCustomer WHERE AccountNo = @PresentAccountNo 
					AND SNo = @SNo
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							SET @StatusText = 'Customer Pending Bills'
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							SET @StatusText = 'Customer Bills Paymnets'
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							SET @StatusText = 'Customer Bills'
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							SET @StatusText = 'Customer Bills Paymnets'
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					SET @StatusText = 'Customer Outstanding'
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @StatusText = 'Success'
					SET @IsSuccess = 1
					SET @SNo = @SNo + 1
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess =0
			END CATCH		
		END
		
	SELECT   
		 @IsSuccess As IsSuccess
		,@StatusText AS StatusText
	FOR XML PATH('PaymentsBe'),TYPE
		
END
GO



GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
--Description: to validate bulk payments upload
--created By : Karteek
--created Date 22-04-2015
-- =============================================          
ALTER PROCEDURE [dbo].[USP_ValidatePaymentUpload_WithDT]          
(  
	 @Bu_Id VARCHAR(50)  
	,@TblPayments TblPaymentsUpload READONLY  
)          
AS          
BEGIN          
      
						
	SELECT 
		 ROW_NUMBER() OVER (ORDER BY TempDetails.SNO) AS RowNumber    
		,CD.GlobalAccountNumber  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,TempDetails.AmountPaid AS PaidAmount    
		,TempDetails.ReceiptNO    
		,TempDetails.ReceivedDate   
		,CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) AS PaymentDate  
		,Case When (TempDetails.AmountPaid = isnull(CP.PaidAmount,0)  
					and TempDetails.ReceiptNO = isnull(CP.ReceiptNo,0))
			THEn 1
			else 0 end IsDuplicate
		--,0 AS IsDuplicate
		,case when BookDetails.BU_ID  = @Bu_Id then 1 else 0  end as IsExists 
		,CONVERT(VARCHAR(10),isnull(PM.PaymentModeId,''))+' - '+ isnull(PM.PaymentMode,'')	  as PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId 
	FROM @TblPayments AS TempDetails     
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (TempDetails.AccountNo= CD.GlobalAccountNumber OR  TempDetails.AccountNo= CD.OldAccountNo)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (CD.GlobalAccountNumber= CPD.GlobalAccountNumber )
	INNER JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = CPD.BookNo
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentModeId=PM.PaymentMode
	LEFT JOIN Tbl_CustomerPayments(NOLOCK) CP ON 
	CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101) =	CONVERT(VARCHAR(50),CP.RecievedDate,101) 
	and (Isnull(CD.GlobalAccountNumber,0) = isnull(CP.AccountNo,0))
			
END   
------------------------------------------------------------------------------------------------------------------
