GO
CREATE TYPE [dbo].[TblBulkCustomerPayments] AS TABLE(
	 RowNumber INT 
	,AccountNo VARCHAR(50)
	,OldAccountNo VARCHAR(50)
	,PaidAmount DECIMAL(18,2)
	,ReceiptNO VARCHAR(20)
	,PaymentDate DATETIME
	,ReceivedDate DATETIME
	,IsDuplicate BIT
	,IsExists BIT
	,PaymentMode VARCHAR(50)
	,PaymentModeId INT
	,IsValid BIT
)
GO
GO
CREATE TYPE TblMeterRadingUploadedData AS TABLE(
	SNO INT,
	Account_Number VARCHAR(50),
	Current_Reading VARCHAR(50),
	Read_Date DATETIME
)
GO
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=0
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=3
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=7
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=8
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=9
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=10
GO
DELETE TBL_FunctionApprovalRole WHERE FunctionId NOT IN (3,7,8,9,10)
GO