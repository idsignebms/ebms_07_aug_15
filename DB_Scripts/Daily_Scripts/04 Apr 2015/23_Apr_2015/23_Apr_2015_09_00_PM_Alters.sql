 GO
 CREATE TYPE TblMeterReadingsUpload AS TABLE
 (
	 SNo INT
	,AccNum VARCHAR(50)
	,TotalReadings INT
	,AverageReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage VARCHAR(50)
	,IsTamper BIT
	,PreviousReading VARCHAR(50)
	,IsExists BIT
	,Multiplier INT
	,ReadDate DATETIME
	,ReadBy VARCHAR(50)
 )
GO
CREATE TYPE TblConsumptionAvgData AS TABLE
(
	 SNO INT
	,Global_Account_Number VARCHAR(50)
	,Average_Reading VARCHAR(50)
)
GO
CREATE TYPE TblAvgBulkUpload AS TABLE
(
	 SNo INT
	,AccNum VARCHAR(50)
	,AverageReading VARCHAR(50)
	,CreatedBy VARCHAR(50)
)
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=12
GO