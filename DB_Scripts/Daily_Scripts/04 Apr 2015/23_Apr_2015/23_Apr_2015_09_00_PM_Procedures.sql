
GO

/****** Object:  StoredProcedure [dbo].[USP_GetConsumptionAvgUploadData_WithDT]    Script Date: 04/23/2015 21:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Karteek
-- Create date: 23 Apr 2015
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetConsumptionAvgUploadData_WithDT] 
(	
	@TblConcumption TblConsumptionAvgData READONLY	
)
AS
BEGIN
	
	SELECT Temp.Average_Reading
		  ,Temp.SNO
		  ,CD.ActiveStatusId AS ActiveStatusId
		  ,CD.OldAccountNo
		  ,CD.GlobalAccountNumber AS AccNum
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	INNER JOIN @TblConcumption AS Temp ON (CD.GlobalAccountNumber = Temp.Global_Account_Number OR Temp.Global_Account_Number = CD.OldAccountNo)

END


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerReadToDirectChangeApproval]    Script Date: 04/23/2015 21:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: Update ReadToDirect Status change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerReadToDirectChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ReadToDirectChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@MeterNo VARCHAR(50)

	SELECT  
		 @ReadToDirectChangeLogId = C.value('(ReadToDirectChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	SELECT 
		 @MeterNo =MeterNo
		,@GlobalAccountNo = GlobalAccountNo
		,@Remarks = Remarks
	FROM Tbl_ReadToDirectCustomerActivityLogs
	WHERE ReadToDirectId = @ReadToDirectChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
											WHERE ReadToDirectId = @ReadToDirectChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_ReadToDirectCustomerActivityLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND  ReadToDirectId= @ReadToDirectChangeLogId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET ReadCodeID=1, MeterNumber = NULL,
								ModifedBy=@ModifiedBy,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @GlobalAccountNo		

							
							INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
										 ReadToDirectId
										,GlobalAccountNo
										,MeterNo
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole)  
									VALUES (  
										 @ReadToDirectChangeLogId
										,@GlobalAccountNo
										,@MeterNo
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId )
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_ReadToDirectCustomerActivityLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND ReadToDirectId = @ReadToDirectChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
					UPDATE Tbl_ReadToDirectCustomerActivityLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifiedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApprovalStatusId = @ApprovalStatusId
							,Remarks = @Details
						WHERE GlobalAccountNo = @GlobalAccountNo  
						AND ReadToDirectId = @ReadToDirectChangeLogId  

					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL,
						ModifedBy=@ModifiedBy,ModifiedDate=dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @GlobalAccountNo		
					
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES (  
						 @ReadToDirectChangeLogId
						,@GlobalAccountNo
						,@MeterNo
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId )
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
																WHERE ReadToDirectId = @ReadToDirectChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
						WHERE ReadToDirectId = @ReadToDirectChangeLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_ReadToDirectCustomerActivityLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND ReadToDirectId = @ReadToDirectChangeLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]    Script Date: 04/23/2015 21:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: The purpose of this procedure is to get list of ReadToDirect Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.ReadToDirectId
		 ,T.MeterNo
		 ,T.GlobalAccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_ReadToDirectCustomerActivityLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkMeterReadings_WithDT]    Script Date: 04/23/2015 21:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 23-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
CREATE PROCEDURE  [dbo].[USP_InsertBulkMeterReadings_WithDT]
(
	 @MeterReadingFrom INT
	,@CreateBy varchar(50)
	,@TempReadings TblMeterReadingsUpload READONLY
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess BIT = 0

	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempReadings
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = ISNULL(Usage,0)
				,@AccountNo = AccNum
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
				,@Multiple = Multiplier
			 FROM @TempReadings WHERE SNo = @SNo
			 
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PresentReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			SET @Multiple = NULL
			
		END
	
	SELECT @IsSuccess AS IsSuccess 
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBookCustomersCount_ByCycles]    Script Date: 04/23/2015 21:25:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 17 OCT 2014
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles
-- mofifiedBy : Padmini
-- ModifiedDate : 26-12-2014
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetDisableBookCustomersCount_ByCycles]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE	@CycleId VARCHAR(MAX)
			,@BillMonth INT
			,@BillYear INT
			
	SELECT   		
		@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BillMonth = C.value('(BillingMonth)[1]','INT')
		,@BillYear = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	SELECT
		(
		SELECT 
			
			COUNT(CD.GlobalAccountNumber) AS CustomersCount
			
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD
		JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID
		WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))
		AND BookNo IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive = 1 AND MonthId = @BillMonth AND YearId = @BillYear)
		AND ActiveStatusId = 1
		FOR XML PATH('BillGenerationList'),TYPE
		)
	FOR XML PATH(''),ROOT('BillGenerationInfoByXml')
		
END

GO


GO
/****** Object:  StoredProcedure [dbo].[USP_InsertDirectCustomerBillReading]    Script Date: 04/23/2015 21:32:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 23 Apr 2015  
-- Description: AverageReading details are insertion if exists updation for those customers 
-- =============================================  
CREATE PROCEDURE [dbo].[USP_InsertBulkAgerageUpload_WithDT]  
(  
	@TblAvgUpload TblAvgBulkUpload READONLY  
)  
AS  
BEGIN  
  
    
	UPDATE DC   
	SET DC.AverageReading = Tc.AverageReading  
		,DC.ModifiedBy = TC.CreatedBy  
		,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
	FROM Tbl_DirectCustomersAvgReadings DC  
	INNER JOIN @TblAvgUpload TC ON TC.AccNum = DC.GlobalAccountNumber  
	WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM @TblAvgUpload)  
        
	INSERT INTO Tbl_DirectCustomersAvgReadings  
	(  
		GlobalAccountNumber  
		,AverageReading  
		,CreatedBy  
		,CreatedDate  
	)  
	SELECT TC.AccNum  
		,TC.AverageReading  
		,TC.CreatedBy  
		,dbo.fn_GetCurrentDateTime()  
	FROM Tbl_DirectCustomersAvgReadings DC 
	RIGHT JOIN @TblAvgUpload TC on TC.AccNum = DC.GlobalAccountNumber 
	WHERE GlobalAccountNumber IS NULL

	SELECT 1 AS IsSuccess   
    
END  
  

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerReadToDirectChangeApproval]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: Update ReadToDirect Status change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerReadToDirectChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ReadToDirectChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@MeterNo VARCHAR(50)

	SELECT  
		 @ReadToDirectChangeLogId = C.value('(ReadToDirectChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	SELECT 
		 @MeterNo =MeterNo
		,@GlobalAccountNo = GlobalAccountNo
		,@Remarks = Remarks
	FROM Tbl_ReadToDirectCustomerActivityLogs
	WHERE ReadToDirectId = @ReadToDirectChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
											WHERE ReadToDirectId = @ReadToDirectChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_ReadToDirectCustomerActivityLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND  ReadToDirectId= @ReadToDirectChangeLogId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET ReadCodeID=1, MeterNumber = NULL,
								ModifedBy=@ModifiedBy,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @GlobalAccountNo		

							
							INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
										 ReadToDirectId
										,GlobalAccountNo
										,MeterNo
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole)  
									VALUES (  
										 @ReadToDirectChangeLogId
										,@GlobalAccountNo
										,@MeterNo
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId )
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_ReadToDirectCustomerActivityLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND ReadToDirectId = @ReadToDirectChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
					UPDATE Tbl_ReadToDirectCustomerActivityLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifiedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApprovalStatusId = @ApprovalStatusId
							,Remarks = @Details
						WHERE GlobalAccountNo = @GlobalAccountNo  
						AND ReadToDirectId = @ReadToDirectChangeLogId  

					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL,
						ModifedBy=@ModifiedBy,ModifiedDate=dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @GlobalAccountNo		
					
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES (  
						 @ReadToDirectChangeLogId
						,@GlobalAccountNo
						,@MeterNo
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId )
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
																WHERE ReadToDirectId = @ReadToDirectChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
						WHERE ReadToDirectId = @ReadToDirectChangeLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_ReadToDirectCustomerActivityLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND ReadToDirectId = @ReadToDirectChangeLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetConsumptionAvgUploadData_WithDT]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Karteek
-- Create date: 23 Apr 2015
-- Description: Get Direct Customer BillReading details from CustomerDetails A/c exists or not table TO EXCELL
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetConsumptionAvgUploadData_WithDT] 
(	
	@TblConcumption TblConsumptionAvgData READONLY	
)
AS
BEGIN
	
	SELECT Temp.Average_Reading
		  ,Temp.SNO
		  ,CD.ActiveStatusId AS ActiveStatusId
		  ,CD.OldAccountNo
		  ,CD.GlobalAccountNumber AS AccNum
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	INNER JOIN @TblConcumption AS Temp ON (CD.GlobalAccountNumber = Temp.Global_Account_Number OR Temp.Global_Account_Number = CD.OldAccountNo)

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: The purpose of this procedure is to get list of ReadToDirect Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.ReadToDirectId
		 ,T.MeterNo
		 ,T.GlobalAccountNo AS AccountNo
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_ReadToDirectCustomerActivityLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_Bookwise]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_Bookwise] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
	dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
	,MeterNumber as CustomerExistingMeterNumber
	,MeterDials As CustomerExistingMeterDials
	,Decimals as Decimals
	,InitialReading as	InitialReading
	,AccountNo
	,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeId = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
----------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			CustomerBills.BillGeneratedDate as EstimatedBillDate,
			0 AS IsActiveMonth,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExists,
			ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--') as LatestDate,
			CustomerList.Name,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			CustomerReadings.ReadingMultiplier as Multiplier,
			CustomerReadings.IsTamper,
			0 as Decimals,
			'' AS BillNo,
			ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading,
			CustomerReadings.TotalReadings,
			--(case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
			--	then (case when ISNULL(CustomerBills.BillGeneratedDate,'')='' 
			--				then (case when CONVERT(DATE,CustomerReadings.ReadDate) <= CONVERT(DATE,CustomerBills.BillGeneratedDate) then CustomerReadings.PresentReading ELSE NULL end)
			--			when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
			--			else 0 end) 
			--	else 1 End) as  PresentReading,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
							when CONVERT(DATE,@ReadDate) <= CONVERT(DATE,CustomerBills.BillGeneratedDate) then CustomerReadings.PresentReading
							else null end) 
				else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
					when CONVERT(DATE,@ReadDate) <= CONVERT(DATE,CustomerBills.BillGeneratedDate) then CustomerReadings.PresentReading
					else NULL end)
				 End as  PresentReading,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
				else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end   as PrvExist,
			case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end AS Usage,
			--(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			--(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			(Case when CONVERT(DATE,@ReadDate) <= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsBilled,
			(Case when CONVERT(DATE,@ReadDate) <= CONVERT(DATE,CustomerBills.BillGeneratedDate) then 1 else 0 end)  IsLatestBill,
			--case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
			--	then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) 
			--				then CustomerReadings.PresentReading 
			--				else CustomerReadings.PreviousReading end)  	
			--	else isnull(CustomerList.InitialReading,0) End as PreviousReading,
			case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
				then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
							else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
				else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
					else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
				 End as  PreviousReading,
			(Case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,CustomerBills.BillGeneratedDate) then CONVERT(VARCHAR(10),CustomerBills.ReadType) else '--' end)  LastBillReadType
			,CustomerList.BookNo,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	 
			LEFT JOIN	  
			#CustomerLatestBills as CustomerBills 
			ON CustomerBills.GlobalAccountNumber=CustomerList.GlobalAccountNumber
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table  #CustomersListBook   
DROP Table  #CustomerLatestBills
DROP Table  #CusotmersWithMaxBillID
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadings_Bulk]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 20-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertMeterReadings_Bulk]
(
	 @XmlDoc Xml
	,@MultiXmlDoc Xml
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@MeterReadingFrom INT
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess INT = 0

	SELECT @MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		--,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
		--,@ReadDate = C.value('(ReadDate)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @TempCustomer TABLE(SNo INT IDENTITY(1,1), TotalReadings INT, AverageReading VARCHAR(50),           
							PresentReading VARCHAR(50), Usage VARCHAR(50), AccountNo VARCHAR(50),
							IsTamper BIT, PreviousReading VARCHAR(50), IsExists BIT, ReadDate VARCHAR(50), ReadBy Varchar(50))
	
	INSERT INTO @TempCustomer(TotalReadings, AverageReading, PresentReading, Usage, AccountNo, IsTamper, PreviousReading, IsExists, ReadDate, ReadBy)           
	SELECT         
		 c.value('(TotalReadings)[1]','INT')
		,c.value('(AverageReading)[1]','VARCHAR(50)')
		,c.value('(PresentReading)[1]','VARCHAR(50)')
		,ISNULL(c.value('(Usage)[1]','VARCHAR(50)'),0)
		,c.value('(AccNum)[1]','varchar(50)')
		,c.value('(IsTamper)[1]','BIT')
		,c.value('(PreviousReading)[1]','varchar(50)')
		,c.value('(IsExists)[1]','BIT')
		,c.value('(ReadDate)[1]','varchar(50)')
		,c.value('(ReadBy)[1]','varchar(50)')
	FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(c) 
	
	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomer
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = Usage
				,@AccountNo = AccountNo
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
			 FROM @TempCustomer WHERE SNo = @SNo
			
			SELECT @Multiple = MI.MeterMultiplier
				,@MeterNumber = CPD.MeterNumber
				,@Dials=MI.MeterDials 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
			INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccountNo
			
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PresentReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = @IsSuccess + 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			
		END
		
	SELECT @IsSuccess AS IsSuccess 
	FOR XML PATH('BillingBE')
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBillxPreviousReading_New]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SAtya
-- Create date: <7-APR-2015>
 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_UpdateBillxPreviousReading_New](@XmlDoc Xml)
	
AS
BEGIN
	DECLARE 
		 @ReadDate datetime
		,@Modified varchar(50)
		,@AccNum varchar(50)
		,@CustUn varchar(50)
		,@Usage NUMERIC(20,4)
		,@Avg VARCHAR(50)
		,@Count INT
		,@Current VARCHAR(50)
		,@IsTamper BIT
		,@MeterReadingFrom INT
		,@Rollover BIT
		,@Previous varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int=1
		,@CreateBy varchar(50)

	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@Count = C.value('(TotalReadings)[1]','INT')
		,@Avg = C.value('(AverageReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@Modified = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@Rollover = C.value('(Rollover)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@Previous = C.value('(PreviousReading)[1]','varchar(50)')
		,@ReadBy = C.value('(ReadBy)[1]','varchar(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	DECLARE @ReadId INT
	Declare	 @Dials INT
	Declare	  @Multipler INT
	Declare @IsExistingRollOver bit
	Declare	   @MeterNumber varchar(50),@FirstPrevReading varchar(50),@FirstPresentValue varchar(50) 
	 ,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
	 
		,@SecoundReadingPrevReading varchar(20)
		,@AverageReading VARCHAR(50)
		 
	SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=IsRollOver,@MeterNumber=MeterNumber
	,@Dials	= LEN(PreviousReading) ,@ReadDate=ReadDate 
	FROM Tbl_CustomerReadings
	WHERE GlobalAccountNumber = @AccNum
	ORDER BY CustomerReadingId DESC

		 
	IF @IsExistingRollOver = 1
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
	END
	ELSE
	BEGIN
		DELETE FROM	   Tbl_CustomerReadings where   CustomerReadingId=   @ReadId    
	END
	 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
		 
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	Declare @FirstReadingUsage bigint =@Usage
	Declare @SecountReadingUsage Bigint =0
	SET @FirstPrevReading   = @Previous
	SET @FirstPresentValue    =	 @Current
	
	IF @Rollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
	--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		 
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@Rollover
		)
	IF(@Rollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage 
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@Rollover)
									
					 
				END
			
 
		END
	
	
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Current 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum
	

	 

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
	   
END



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillReading_New]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillReading_New]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)

	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum

 
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);
		  
		END
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
		INSERT INTO Tbl_CustomerReadings(
		 GlobalAccountNumber
		,[ReadDate]
		,[ReadBy]
		,[PreviousReading]
		,[PresentReading]
		,[AverageReading]
		,[Usage]
		,[TotalReadingEnergies]
		,[TotalReadings]
		,[Multiplier]
		,[ReadType]
		,[CreatedBy]
		,[CreatedDate]
		,[IsTamper]
		,MeterNumber
		,[MeterReadingFrom]
		,IsRollOver)
	VALUES(
		 @AccNum
		,@ReadDate
		,@ReadBy
		,CONVERT(VARCHAR(50),@Previous)
		,@FirstPresentValue
		,@AverageReading
		,@Usage
		,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
		,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
		,@Multiple
		,2
		,@CreateBy
		,GETDATE()
		,@IsTamper
		,@MeterNumber
		,@MeterReadingFrom
		,@IsRollover)
		

	IF(@IsRollover=1)
		BEGIN
 
			IF @SecountReadingUsage != 0
				BEGIN
				--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
				 IF	 @SecountReadingUsage > 0
				 INSERT INTO Tbl_CustomerReadings(
									 GlobalAccountNumber
									,[ReadDate]
									,[ReadBy]
									,[PreviousReading]
									,[PresentReading]
									,[AverageReading]
									,[Usage]
									,[TotalReadingEnergies]
									,[TotalReadings]
									,[Multiplier]
									,[ReadType]
									,[CreatedBy]
									,[CreatedDate]
									,[IsTamper]
									,MeterNumber
									,[MeterReadingFrom]
									,IsRollOver)
								VALUES(
									 @AccNum
									,@ReadDate
									,@ReadBy
									,@SecoundReadingPrevReading
									,CONVERT(VARCHAR(50),@Current)
									,@AverageReading
									,@SecountReadingUsage
									,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
									,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
									,@Multiple
									,2
									,@CreateBy
									,GETDATE()
									,@IsTamper
									,@MeterNumber
									,@MeterReadingFrom
									,@IsRollover)
									
				END
		END
		
 
	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
	SET InitialReading = @Previous
		,PresentReading = @Current 
		,AvgReading = CONVERT(NUMERIC,@AverageReading) 
	WHERE GlobalAccountNumber = @AccNum

	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkMeterReadings_WithDT]    Script Date: 04/23/2015 21:25:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 23-04-2015
-- This is to insert or update the Meter readings bulk 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertBulkMeterReadings_WithDT]
(
	 @MeterReadingFrom INT
	,@CreateBy varchar(50)
	,@TempReadings TblMeterReadingsUpload READONLY
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess BIT = 0

	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempReadings
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = ISNULL(Usage,0)
				,@AccountNo = AccNum
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
				,@Multiple = Multiplier
			 FROM @TempReadings WHERE SNo = @SNo
			 
			IF(@IsExists = 1)
				BEGIN					
					DELETE FROM	Tbl_CustomerReadings 
					WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber = @AccountNo
											ORDER BY CustomerReadingId DESC)						
				END	
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadings(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver)	
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				)	
				
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET InitialReading = @PreviousReading
				,PresentReading = @PresentReading 
				,AvgReading = CONVERT(NUMERIC,@AverageReading) 
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @IsSuccess = 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			SET @Multiple = NULL
			
		END
	
	SELECT @IsSuccess AS IsSuccess 
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
     
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy  
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 ORDER BY TCR.CreatedDate DESC
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogs]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
		 SELECT 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			   ,CCL.Remarks
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CCL.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder
			   ,CustomerView.CycleName
		       ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder      
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		 ORDER BY CCL.CreatedDate DESC
		 -- changed by karteek end       
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek     
     
	SELECT
	(  
		SELECT  TOP 10 
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			,CCL.Remarks
			,CCL.CreatedBy
			,CustomerView.OldAccountNo
			,CustomerView.ClassName
			,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		ORDER BY CCL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  NEERAJ KANOJIYA          
-- Create date: 1/APRIL/2015          
-- Description: This Procedure is used to change customer address         
-- Modified BY: Faiz-ID103    
-- Modified Date: 07-Apr-2015    
-- Description: changed the street field value from 200 to 255 characters        
-- Modified BY: Faiz-ID103    
-- Modified Date: 16-Apr-2015    
-- Description: Add the condition for IsBookNoChanged  
-- Modified BY: Bhimaraju V  
-- Modified Date: 17-Apr-2015  
-- Description: Implemented Logs for Audit Tray   
-- Modified BY: Faiz-ID103
-- Modified Date: 23-Apr-2015  
-- Description: Added the functionality of AccountNo update on book change.
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]          
(          
 @XmlDoc xml             
)          
AS          
BEGIN        
 DECLARE @TotalAddress INT       
   ,@IsSameAsServie BIT      
   ,@GlobalAccountNo   VARCHAR(50)            
     ,@NewPostalLandMark  VARCHAR(200)          
     ,@NewPostalStreet   VARCHAR(255)          
     ,@NewPostalCity   VARCHAR(200)          
     ,@NewPostalHouseNo   VARCHAR(200)          
     ,@NewPostalZipCode   VARCHAR(50)          
     ,@NewServiceLandMark  VARCHAR(200)          
     ,@NewServiceStreet   VARCHAR(255)          
     ,@NewServiceCity   VARCHAR(200)          
     ,@NewServiceHouseNo  VARCHAR(200)          
     ,@NewServiceZipCode  VARCHAR(50)          
     ,@NewPostalAreaCode  VARCHAR(50)          
     ,@NewServiceAreaCode  VARCHAR(50)          
     ,@NewPostalAddressID  INT          
     ,@NewServiceAddressID  INT         
     ,@ModifiedBy    VARCHAR(50)          
     ,@Details     VARCHAR(MAX)          
     ,@RowsEffected    INT          
     ,@AddressID INT         
     ,@StatusText VARCHAR(50)      
     ,@IsCommunicationPostal BIT      
     ,@IsCommunicationService BIT      
     ,@ApprovalStatusId INT=2        
     ,@PostalAddressID INT      
     ,@ServiceAddressID INT      
     ,@BookNo VARCHAR(50)      
       SELECT             
    @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')          
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')              
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')              
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')              
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')             
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')            
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')              
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')              
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')              
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')             
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')          
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')          
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')          
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')          
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')          
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')          
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')          
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')      
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')          
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')      
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')      
     ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')      
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)          
BEGIN TRY      
  BEGIN TRAN       
    --UPDATE POSTAL ADDRESS      
 SET @StatusText='Total address count.'         
 SET @TotalAddress=(SELECT COUNT(0)       
      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails       
      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1      
        )      
              
 --=====================================================================================              
 --Update book number of customer.      
 --=====================================================================================       
Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
IF(@ExistingBookNo!=@BookNo)  
BEGIN  
   
 --DECLARE @OldBookNo VARCHAR(50)  
 --SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)  
  
  INSERT INTO Tbl_BookNoChangeLogs( AccountNo  
          --,CustomerUniqueNo  
          ,OldBookNo  
          ,NewBookNo  
          ,CreatedBy  
          ,CreatedDate  
          ,ApproveStatusId  
          ,Remarks)  
  VALUES(@GlobalAccountNo
		--,@OldBookNo
		,@ExistingBookNo
		,@BookNo
		,@ModifiedBy
		,dbo.fn_GetCurrentDateTime()
		,@ApprovalStatusId,@Details)   
     
  UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
  SET BookNo=@BookNo,IsBookNoChanged=1       
  WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1  
  
	  --===========Start of Updating Account Number after book change.=================(Faiz-ID103)
	  DECLARE @AccountNo Varchar(50)
		,@BU_ID VARCHAR(50)  
		,@SU_ID VARCHAR(50)  
		,@ServiceCenterId VARCHAR(50)  
	  SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId   
	  FROM Tbl_BookNumbers BN   
	  JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
	  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
	  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
	  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
	   WHERE BookNo=@BookNo  
	     
	  SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo);
	  
	  UPDATE CUSTOMERS.Tbl_CustomersDetail 
	  SET AccountNo=@AccountNo 
	  WHERE GlobalAccountNumber = @GlobalAccountNo
	  --===========END of Updating Account Number after book change.=================
END  
  
--================== Logs For Addres Change          
  IF (@IsSameAsServie =0)  
  BEGIN    
   INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber  
              ,OldPostal_HouseNo  
              ,OldPostal_StreetName  
              ,OldPostal_City  
              ,OldPostal_Landmark  
              ,OldPostal_AreaCode  
              ,OldPostal_ZipCode  
              ,OldService_HouseNo  
              ,OldService_StreetName  
              ,OldService_City  
              ,OldService_Landmark  
              ,OldService_AreaCode  
              ,OldService_ZipCode  
              ,NewPostal_HouseNo  
                 ,NewPostal_StreetName  
                 ,NewPostal_City  
                 ,NewPostal_Landmark  
                 ,NewPostal_AreaCode  
                 ,NewPostal_ZipCode  
                 ,NewService_HouseNo  
                 ,NewService_StreetName  
                 ,NewService_City  
                 ,NewService_Landmark  
                 ,NewService_AreaCode  
                 ,NewService_ZipCode  
                 ,ApprovalStatusId  
                 ,Remarks  
                 ,CreatedBy  
                 ,CreatedDate  
             )  
            SELECT   @GlobalAccountNo  
              ,Postal_HouseNo  
              ,Postal_StreetName  
              ,Postal_City  
              ,Postal_Landmark  
              ,Postal_AreaCode  
              ,Postal_ZipCode  
              ,Service_HouseNo  
              ,Service_StreetName  
              ,Service_City  
              ,Service_Landmark  
              ,Service_AreaCode  
              ,Service_ZipCode  
              ,@NewPostalHouseNo  
              ,@NewPostalStreet  
              ,@NewPostalCity  
              ,NULL  
              ,@NewPostalAreaCode  
              ,@NewPostalZipCode  
              ,@NewServiceHouseNo  
              ,@NewServiceStreet  
              ,@NewServiceCity  
              ,NULL  
              ,@NewServiceAreaCode  
              ,@NewServiceZipCode  
              ,@ApprovalStatusId  
              ,@Details  
              ,@ModifiedBy  
              ,dbo.fn_GetCurrentDateTime()  
            FROM CUSTOMERS.Tbl_CustomerSDetail  
            WHERE GlobalAccountNumber=@GlobalAccountNo  
  END  
 ELSE  
  BEGIN  
   INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber  
              ,OldPostal_HouseNo  
              ,OldPostal_StreetName  
              ,OldPostal_City  
              ,OldPostal_Landmark  
              ,OldPostal_AreaCode  
              ,OldPostal_ZipCode  
              ,OldService_HouseNo  
              ,OldService_StreetName  
              ,OldService_City  
              ,OldService_Landmark  
              ,OldService_AreaCode  
              ,OldService_ZipCode  
              ,NewPostal_HouseNo  
                 ,NewPostal_StreetName  
                 ,NewPostal_City  
                 ,NewPostal_Landmark  
                 ,NewPostal_AreaCode  
                 ,NewPostal_ZipCode  
                 ,NewService_HouseNo  
                 ,NewService_StreetName  
                 ,NewService_City  
                 ,NewService_Landmark  
                 ,NewService_AreaCode  
                 ,NewService_ZipCode  
                 ,ApprovalStatusId  
                 ,Remarks  
                 ,CreatedBy  
                 ,CreatedDate  
             )  
            SELECT   @GlobalAccountNo  
              ,Postal_HouseNo  
              ,Postal_StreetName  
              ,Postal_City  
              ,Postal_Landmark  
              ,Postal_AreaCode  
              ,Postal_ZipCode  
              ,Service_HouseNo  
              ,Service_StreetName  
              ,Service_City  
              ,Service_Landmark  
              ,Service_AreaCode  
              ,Service_ZipCode  
              ,@NewPostalHouseNo  
              ,@NewPostalStreet  
              ,@NewPostalCity  
              ,NULL  
              ,@NewPostalAreaCode  
              ,@NewPostalZipCode  
              ,@NewPostalHouseNo  
              ,@NewPostalStreet  
              ,@NewPostalCity  
              ,NULL  
              ,@NewPostalAreaCode  
              ,@NewPostalZipCode  
              ,@ApprovalStatusId  
              ,@Details  
              ,@ModifiedBy  
              ,dbo.fn_GetCurrentDateTime()  
            FROM CUSTOMERS.Tbl_CustomerSDetail  
            WHERE GlobalAccountNumber=@GlobalAccountNo  
  END  
    
 --=====================================================================================              
 --Customer has only one addres and wants to update the address. //CONDITION 1//      
 --=====================================================================================          
 IF(@TotalAddress =1 AND @IsSameAsServie = 1)      
 BEGIN      
  SET @StatusText='CONDITION 1'  
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
      ,ModifedBy=@ModifiedBy            
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
      ,IsCommunication=@IsCommunicationPostal           
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
    Service_Landmark=@NewPostalLandMark      
    ,Service_StreetName=@NewPostalStreet      
    ,Service_City=@NewPostalCity      
    ,Service_HouseNo=@NewPostalHouseNo      
    ,Service_ZipCode=@NewPostalZipCode      
    ,Service_AreaCode=@NewPostalAreaCode      
    ,Postal_Landmark=@NewPostalLandMark      
    ,Postal_StreetName=@NewPostalStreet      
    ,Postal_City=@NewPostalCity      
    ,Postal_HouseNo=@NewPostalHouseNo      
    ,Postal_ZipCode=@NewPostalZipCode      
    ,Postal_AreaCode=@NewPostalAreaCode      
    ,ServiceAddressID=@PostalAddressID      
    ,PostalAddressID=@PostalAddressID      
    ,IsSameAsService=1      
  WHERE GlobalAccountNumber=@GlobalAccountNo      
 END          
 --=====================================================================================      
 --Customer has one addres and wants to add new address. //CONDITION 2//      
 --=====================================================================================      
 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)      
 BEGIN      
  SET @StatusText='CONDITION 2'  
   
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
      ,ModifedBy=@ModifiedBy            
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
      ,IsCommunication=@IsCommunicationPostal           
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(      
                HouseNo      
                ,LandMark      
                ,StreetName      
                ,City      
                ,AreaCode      
                ,ZipCode      
                ,ModifedBy      
                ,ModifiedDate      
                ,IsCommunication      
                ,IsServiceAddress      
                ,IsActive      
                ,GlobalAccountNumber      
                )      
               VALUES (@NewServiceHouseNo             
                ,@NewServiceLandMark             
                ,@NewServiceStreet              
                ,@NewServiceCity            
                ,@NewServiceAreaCode            
             ,@NewServiceZipCode              
                ,@ModifiedBy            
                ,dbo.fn_GetCurrentDateTime()        
                ,@IsCommunicationService               
                ,1      
                ,1      
                ,@GlobalAccountNo      
                )       
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
    Service_Landmark=@NewServiceLandMark      
    ,Service_StreetName=@NewServiceStreet      
    ,Service_City=@NewServiceCity      
    ,Service_HouseNo=@NewServiceHouseNo      
    ,Service_ZipCode=@NewServiceZipCode      
    ,Service_AreaCode=@NewServiceAreaCode      
    ,Postal_Landmark=@NewPostalLandMark      
    ,Postal_StreetName=@NewPostalStreet      
    ,Postal_City=@NewPostalCity      
    ,Postal_HouseNo=@NewPostalHouseNo      
    ,Postal_ZipCode=@NewPostalZipCode      
    ,Postal_AreaCode=@NewPostalAreaCode      
    ,ServiceAddressID=@ServiceAddressID      
    ,PostalAddressID=@PostalAddressID      
    ,IsSameAsService=0      
  WHERE GlobalAccountNumber=@GlobalAccountNo                     
 END         
       
 --=====================================================================================      
 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//      
 --=====================================================================================      
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)       
 BEGIN      
  SET @StatusText='CONDITION 3'  
    
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
      ,ModifedBy=@ModifiedBy            
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
      ,IsCommunication=@IsCommunicationPostal           
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
    IsActive=0      
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
    Service_Landmark=@NewPostalLandMark      
    ,Service_StreetName=@NewPostalStreet      
    ,Service_City=@NewPostalCity      
    ,Service_HouseNo=@NewPostalHouseNo      
    ,Service_ZipCode=@NewPostalZipCode      
    ,Service_AreaCode=@NewPostalAreaCode      
    ,Postal_Landmark=@NewPostalLandMark      
    ,Postal_StreetName=@NewPostalStreet      
    ,Postal_City=@NewPostalCity      
    ,Postal_HouseNo=@NewPostalHouseNo      
    ,Postal_ZipCode=@NewPostalZipCode      
    ,Postal_AreaCode=@NewPostalAreaCode      
    ,ServiceAddressID=@PostalAddressID      
    ,PostalAddressID=@PostalAddressID      
    ,IsSameAsService=1      
  WHERE GlobalAccountNumber=@GlobalAccountNo      
 END        
 --=====================================================================================      
 --Customer alrady has tow address and wants to update both address. //CONDITION 4//      
 --=====================================================================================      
 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)      
 BEGIN      
  SET @StatusText='CONDITION 4'  
  
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
      ,ModifedBy=@ModifiedBy            
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
      ,IsCommunication=@IsCommunicationPostal           
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END            
      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END            
      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END            
      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END            
      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END              
      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END               
      ,ModifedBy=@ModifiedBy            
      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
      ,IsCommunication=@IsCommunicationService      
  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
    Service_Landmark=@NewServiceLandMark      
 ,Service_StreetName=@NewServiceStreet      
    ,Service_City=@NewServiceCity      
    ,Service_HouseNo=@NewServiceHouseNo      
    ,Service_ZipCode=@NewServiceZipCode      
    ,Service_AreaCode=@NewServiceAreaCode      
    ,Postal_Landmark=@NewPostalLandMark      
    ,Postal_StreetName=@NewPostalStreet      
    ,Postal_City=@NewPostalCity      
    ,Postal_HouseNo=@NewPostalHouseNo      
    ,Postal_ZipCode=@NewPostalZipCode      
    ,Postal_AreaCode=@NewPostalAreaCode      
    ,ServiceAddressID=@ServiceAddressID      
    ,PostalAddressID=@PostalAddressID      
    ,IsSameAsService=0      
  WHERE GlobalAccountNumber=@GlobalAccountNo         
 END        
 COMMIT TRAN      
END TRY      
BEGIN CATCH      
 ROLLBACK TRAN      
 SET @RowsEffected=0      
END CATCH      
 SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')               
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for shown in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
     
     SELECT
	(  
		SELECT TOP 10
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			,'' AS BU_ID
			,'' AS SU_ID
			,'' AS ServiceCenterId
			,'' AS CycleId
		    ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		    ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo
			,BookChange.Remarks
			,BookChange.CreatedBy
			,CustomerView.ClassName
			,CustomerView.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId 
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		ORDER BY  BookChange.CreatedBy DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml') 
	
	--SELECT
	--(  
	--	SELECT TOP 10
	--		 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
	--		-- ,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br>'+
	--		--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br>'+
	--		--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br>'+
	--		--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br>'+
	--		--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
	--		--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br />'+
	--		--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br />'+
	--		--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br />'+
	--		--  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br />'+
	--		--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
	--	   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
	--	   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

	--		,BookChange.Remarks
	--		,BookChange.CreatedBy
	--		,MTC.ClassName
	--		,CD.OldAccountNo
	--		,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
	--		,ApproveSttus.ApprovalStatus  
	--		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
	--		,COUNT(0) OVER() AS TotalChanges  
	--	FROM Tbl_BookNoChangeLogs  BookChange  
	--	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
	--	AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	--	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
	--	INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
	--	INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
	--	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
	--	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
	--	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
	--	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
	--	INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
	--	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
	--	INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
	--	INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
	--	ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

	--	FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
    
END  
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
		SELECT
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
		   ,'' AS BU_ID
			,'' AS SU_ID
			,'' AS ServiceCenterId
			,'' AS CycleId
		   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  
		   ,BookChange.Remarks  
		   ,BookChange.CreatedBy
		   ,CustomerView.ClassName
		   ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		   ,CustomerView.OldAccountNo
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder    
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder   
		 FROM Tbl_BookNoChangeLogs  BookChange  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId    
		 ORDER BY BookChange.CreatedDate DESC
		--SELECT 
		--	 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
		--	--,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br>'+
		--	--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br>'+
		--	--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br>'+
		--	--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br>'+
		--	--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
		--	--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) </br>'+
		--	--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) </br>'+
		--	--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) </br>'+
		--	--  BD.CycleName +' ( '+BD.CycleCode+ ' ) </br>'+
		--	--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
		--	,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		--   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

		--	,BookChange.Remarks
		--	,BookChange.CreatedBy
		--	,MTC.ClassName
		--	,CD.OldAccountNo
		--	,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		--	,ApproveSttus.ApprovalStatus  
		--	,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name
		--	,BD.BusinessUnitName
		--	,BD.ServiceUnitName
		--	,BD.ServiceCenterName
		--	,BD.CycleName
		--	,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
		--	,BD.BookSortOrder
		--	,BD.CustomerSortOrder
		--FROM Tbl_BookNoChangeLogs  BookChange  
		--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
		--AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		--INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		--INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		--INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		--INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		--INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		--INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBookCustomersCount_ByCycles]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 17 OCT 2014
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles
-- mofifiedBy : Padmini
-- ModifiedDate : 26-12-2014
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetDisableBookCustomersCount_ByCycles]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE	@CycleId VARCHAR(MAX)
			,@BillMonth INT
			,@BillYear INT
			
	SELECT   		
		@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BillMonth = C.value('(BillingMonth)[1]','INT')
		,@BillYear = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	SELECT
		(
		SELECT 
			
			COUNT(CD.GlobalAccountNumber) AS CustomersCount
			
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD
		JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID
		WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))
		AND BookNo IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive = 1 AND MonthId = @BillMonth AND YearId = @BillYear)
		AND ActiveStatusId = 1
		FOR XML PATH('BillGenerationList'),TYPE
		)
	FOR XML PATH(''),ROOT('BillGenerationInfoByXml')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesBySC_HavingBooks]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Karteek   
-- Create date: 17-04-2015    
-- Description: The purpose of this procedure is to get Cycles list by SC   
-- Modified date: 23-Apr-2015
-- Modified By: Faiz-ID103
-- Description: Modified the Book Name for BookName(Code)  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCyclesBySC_HavingBooks]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
    
DECLARE @ServiceCenterId VARCHAR(MAX)    
      
 SELECT    
  @ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('MastersBE') as T(C)    
     
 SELECT    
 (    
  SELECT    
   CycleId    
   ,CycleName + ' ('+CY.CycleCode+')' AS CycleName --Faiz-ID103
  FROM Tbl_Cycles CY    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId AND CY.ActiveStatusId=1    
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')   
  AND CycleId IN(SELECT DISTINCT CycleId FROM Tbl_BookNumbers WHERE ActiveStatusId = 1)    
  ORDER BY CycleName ASC    
  FOR XML PATH('MastersBE'),TYPE    
 )    
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')    
END  
  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCyclesByBUSUSC]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  V.Bhimaraju    
-- Create date: 01-09-2014    
-- Modified date: 03-11-2014    
-- Modified By: T.Karthik    
-- Description: The purpose of this procedure is to get Cycles list by BU,SU,SC    
-- Modified date: 23-Apr-2015
-- Modified By: Faiz-ID103
-- Description: Modified the Book Name for BookName(Code)

-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCyclesByBUSUSC]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
    
DECLARE @BU_ID VARCHAR(MAX),@SU_ID VARCHAR(MAX),@ServiceCenterId VARCHAR(MAX)    
      
 SELECT    
   @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')    
  ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')    
  ,@ServiceCenterId=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('MastersBE') as T(C)    
     
 SELECT    
 (    
  SELECT    
   CycleId 
   ,CycleName + ' ('+CY.CycleCode+')' AS CycleName --Faiz-ID103
  FROM Tbl_Cycles CY    
  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId AND CY.ActiveStatusId=1    
  AND (SC.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@ServiceCenterId,',')) OR @ServiceCenterId='')   
  --AND CycleId IN(SELECT DISTINCT CycleId FROM Tbl_BookNumbers WHERE ActiveStatusId = 1)    
  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID AND (SU.SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,',')) OR @SU_ID='')  
  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID AND (BU.BU_ID=@BU_ID OR @BU_ID='')   
  ORDER BY CycleName ASC    
  FOR XML PATH('MastersBE'),TYPE    
 )    
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')    
END  
  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading_WithDT]    Script Date: 04/23/2015 21:25:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteekk
-- Create date: 15 Apr 2015  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading_WithDT]  
(
	@TempCustomer TblMeterRadingUploadedData READONLY
)  
AS  
BEGIN  
  
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.AccountNo
		,CMI.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CMI.Title,CMI.FirstName,CMI.MiddleName,CMI.LastName) as Name
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.Current_Reading
		,T.Read_Date
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		--,COUNT(0) AS CustomerCount
	INTO #CustomersListBook
	FROM @TempCustomer T
	INNER JOIN UDV_CustomerMeterInformation CMI ON (CMI.OldAccountNo = T.Account_Number OR CMI.GlobalAccountNumber = T.Account_Number)
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	--and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
		
		SELECT 
			 CB.GlobalAccountNumber AS AccNum
			,CB.AccountNo
			,CB.OldAccountNo
			,CB.Name
			,CB.MeterNumber AS MeterNo
			,CB.MeterDials
			,CB.Decimals
			,CustomerReadings.ReadingMultiplier
			,CB.ActiveStatusId
			,CONVERT(VARCHAR(20),CB.Read_Date,106) AS ReadDate
			,CB.ReadCodeID AS ReadCodeId
			,CB.BU_ID
			,CB.MarketerId
			,(CONVERT(DECIMAL(18,2),ISNULL(CB.Current_Reading,0)) - CONVERT(DECIMAL(18,2),ISNULL(CustomerReadings.PresentReading,CustomerReadings.PreviousReading))) AS Usage
			,CB.Current_Reading AS PresentReading
			,ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading
			--,ISNULL(CustomerReadings.PresentReading,ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))) AS PreviousReading
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.Read_Date) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end) AS PreviousReading
			,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
			,CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106) AS LatestDate
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.Read_Date) then 1  
				else 0 end) as PrvExist
			,(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.Read_Date) then 1 
				else 0 end) as IsExists
			,(Case when CustomerReadings.IsBilled IS NULL then 0
				else CustomerReadings.IsBilled end) as IsBilled
			--,CustomerCount
			,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
		FROM #CustomersListBook CB
		LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber
		 
END  

GO



GO
/****** Object:  StoredProcedure [dbo].[UPS_GetBillTextFiles]    Script Date: 04/24/2015 10:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <19-March-2014>        
-- Description: <GET EBILL DAILY DETAILS>        
-- =============================================        
ALTER PROCEDURE  [dbo].[UPS_GetBillTextFiles]       
(      
@XmlDoc XML     
)       
AS        
BEGIN       
    
Declare @Month INT,      
  @Year INT    
      
SELECT      
   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
       
    
 SELECT CY.ServiceCenterId ,    
   BS.BillingYear,    
   BillingMonth,    
   SCDetails.BusinessUnitName,    
   SCDetails.ServiceCenterName,      
   SCDetails.ServiceUnitName,    
   --'~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,     
   MAX(BS.BillingFile) As FilePath,      
   dbo.fn_GetMonthName(BillingMonth) as [MonthName]      
 FROM Tbl_BillingQueueSchedule BS        
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId        
 and BillGenarationStatusId=4    
 AND BS.BillingMonth=@Month     --2
 AND BS.BillingYear=@Year      --2015
 INNER JOIN      
 [UDV_ServiceCenterDetails] SCDetails      
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId      
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth       
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,      
 SCDetails.ServiceUnitName        
    
END    
