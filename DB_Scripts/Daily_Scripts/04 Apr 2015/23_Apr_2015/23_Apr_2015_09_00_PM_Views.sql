
GO

/****** Object:  View [dbo].[UDV_BookNumberDetails]    Script Date: 04/23/2015 21:22:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







  
  
ALTER VIEW [dbo].[UDV_BookNumberDetails]  
AS  
 SELECT   
 BU.BU_ID,BU.BUCode,BU.BusinessUnitName  
 ,SU.SU_ID,SU.SUCode,SU.ServiceUnitName  
 ,SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName  
 ,C.CycleId,C.CycleCode,C.CycleName  
 ,B.BookNo,B.BookCode,B.ID
 ,M.MarketerId
 ,B.SortOrder as BookSortOrder
 ,dbo.fn_GetMarketersFullName_New(M.FirstName,M.MiddleName,M.LastName) AS MarketerName   
 FROM Tbl_BookNumbers B 
 JOIN Tbl_Cycles C ON C.CycleId=B.CycleId  
 JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
 JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
 JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
 JOIN Tbl_Marketers M ON M.MarketerId = B.MarketerId
 


GO


