GO
CREATE TYPE TblPaymentUploads_TempType AS TABLE
(
	 SNO INT
	,AccountNo VARCHAR(50)
	,AmountPaid VARCHAR(50)
	,ReceiptNO VARCHAR(50)
	,ReceivedDate VARCHAR(50)
	,PaymentMode VARCHAR(50)
)
GO
CREATE TABLE Temp_TblPaymentsUpload
(
	 UploadId BIGINT IDENTITY(1,1)
	,AccountNo VARCHAR(50)
	,AmountPaid DECIMAL(20,2)
	,ReceiptNO VARCHAR(50)
	,ReceivedDate DATETIME
	,PaymentMode VARCHAR(50)
	,IsUploaded BIT DEFAULT 0
	,BatchNo INT
	,BatchDate DATETIME
	,CreatedBy VARCHAR(50)
	,DocumentPath VARCHAR(MAX)
	,DocumentName VARCHAR(MAX)
	,CreatedDate DATETIME
)
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD IsPostalCommunication BIT 
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New
ADD IsServiceComunication BIT 
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD IsPostalCommunication BIT 
GO
ALTER TABLE Tbl_Audit_CustomerAddressChangeLog
ADD IsServiceComunication BIT 
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1
WHERE FunctionId=6
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1
WHERE FunctionId=11
GO
