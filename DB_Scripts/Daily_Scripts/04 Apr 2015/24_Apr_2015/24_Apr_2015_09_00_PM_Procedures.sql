GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments_WithDT_Temp]    Script Date: 04/24/2015 21:07:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 24-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
CREATE PROCEDURE [dbo].[USP_InsertBulkCustomerPayments_WithDT_Temp]          
(          
	 @CreatedBy VARCHAR(50)           
	,@DocumentPath VARCHAR(MAX)
	,@DocumentName VARCHAR(MAX)  
	,@BatchNo VARCHAR(50)
	,@TempCustomer TblPaymentUploads_TempType READONLY  
)          
AS          
BEGIN    
	
	INSERT INTO Temp_TblPaymentsUpload
	(
		 AccountNo 
		,AmountPaid 
		,ReceiptNO 
		,ReceivedDate 
		,PaymentMode
		,BatchNo 
		,BatchDate 
		,CreatedBy 
		,DocumentPath 
		,DocumentName 
		,CreatedDate 
	)
	SELECT 
		 AccountNo 
		,AmountPaid 
		,ReceiptNO 
		,ReceivedDate 
		,PaymentMode 
		,@BatchNo
		,dbo.fn_GetCurrentDateTime()
		,@CreatedBy
		,@DocumentPath
		,@DocumentName
		,dbo.fn_GetCurrentDateTime()
	FROM @TempCustomer	
		
	SELECT 1 As IsSuccess
		 		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAssignMeterChangeApproval]    Script Date: 04/24/2015 21:07:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: Update AssignMeter change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerAssignMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@AssignMeterChangeId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(MAX) 

	SELECT  
		 @AssignMeterChangeId = C.value('(AssignMeterChangeId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(MAX)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
DECLARE
		 @GlobalAccountNo VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@AssignedMeterDate DATETIME
		,@InitialReading DECIMAL(18,2)
		,@Remarks VARCHAR(MAX)
		--,@PresentApprovalRole INT
		--,@NextApprovalRole INT
		,@CreatedBy VARCHAR(50)
		,@CreatedDate DATETIME
	
	SELECT 
		@GlobalAccountNo	 =GlobalAccountNo 
       ,@MeterNo             =MeterNo 
       ,@AssignedMeterDate   =AssignedMeterDate 
       ,@InitialReading      =InitialReading
       ,@Remarks             =Remarks
       ,@CreatedBy           =CreatedBy 
       ,@CreatedDate         =CreatedDate
	FROM Tbl_AssignedMeterLogs
	WHERE AssignedMeterId = @AssignMeterChangeId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_AssignedMeterLogs 
											WHERE AssignedMeterId = @AssignMeterChangeId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
									SET MeterNumber=@MeterNo
										,ReadCodeID=2
							WHERE GlobalAccountNumber = @GlobalAccountNo
			
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							SET InitialReading=@InitialReading
								,PresentReading=@InitialReading
							WHERE GlobalAccountNumber=@GlobalAccountNo

							
							INSERT INTO Tbl_Audit_AssignedMeterLogs(  
										 AssignedMeterId
										,GlobalAccountNo
										,MeterNo
										,AssignedMeterDate
										,InitialReading
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole)  
									VALUES(  
										 @AssignMeterChangeId
										,@GlobalAccountNo
										,@MeterNo
										,@AssignedMeterDate
										,@InitialReading
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId
										)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_AssignedMeterLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNo = @GlobalAccountNo  
					AND AssignedMeterId = @AssignMeterChangeId 

					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNo
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNo
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
					WHERE GlobalAccountNumber=@GlobalAccountNo
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES(  
						 @AssignMeterChangeId
						,@GlobalAccountNo
						,@MeterNo
						,@AssignedMeterDate
						,@InitialReading
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy  
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId
						)
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_AssignedMeterLogs 
																WHERE AssignedMeterId = @AssignMeterChangeId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AssignedMeterLogs 
						WHERE AssignedMeterId = @AssignMeterChangeId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AssignedMeterLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND AssignedMeterId = @AssignMeterChangeId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAssignMeterLogsToApprove]    Script Date: 04/24/2015 21:07:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerAssignMeterLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.AssignedMeterId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,CONVERT(VARCHAR(50),T.AssignedMeterDate,103) AS MeterAssignedDate
		 ,T.InitialReading
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_AssignedMeterLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 04/24/2015 21:07:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
		
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0)) as Usage
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	  
	INNER JOIN Tbl_Cycles Cycles On	Cycles.ServiceCenterId=@ServiceCenterID and Cycles.CycleId=BN.CycleId
	GROUP BY CR.GlobalAccountNumber
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT
	(
		SELECT ClassID
			,SUM(case when isnull(ReadCodeID,0)=2 then 1 else 0 end) as TotalReadCustomers
			,SUM(case when isnull(ReadCodeID,0)=1 then 1 else 0 end) as EstimatedCustomers
			,TC.ClassName
			,CC.CategoryName
			,CC.ClusterCategoryId 
			,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
			,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
			,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
			,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
			,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
			,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
			,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
			,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
			,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
			,Cycles.CycleId 
			,Cycles.CycleName
		FROM Tbl_MTariffClasses(NOLOCK)	TC
		INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
		INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
		INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
		CROSS JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC
		LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
				and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
				And ES.BillingMonth = @Month -- Need To Modify	 
				AND ES.BillingYear = @Year	 -- Need To Modify
		Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
		ORDER BY Cycles.CycleId,CC.ClusterCategoryId
		FOR XML PATH('EstimationDetails'),TYPE
	)
	,
	(
		SELECT	
			 @Capacity as CapacityInKVA
			,@SUPPMConsumption as SUPPMConsumption
			,@SUCreditConsumption as SUCreditConsumption
		FOR XML PATH('EstimatedUsageDetails'),TYPE 
	)
	FOR XML PATH(''),ROOT('EstimationInfoByXml')
	
	DROP TABLE #ReadCustomersList
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 04/24/2015 21:07:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
CREATE PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      
      
      SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
      SEt @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
    SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
      
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)  
      
    SELECT TOP(1) @LastPaidDate = RecievedDate  
  ,@LastPaidAmount = PaidAmount   
 FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo  
 ORDER BY CustomerPaymentID DESC  
      
      
SELECT(      
  
  SELECT     
    dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
   ,GlobalAccountNumber AS AccountNo    
   ,OldAccountNo 
   ,MeterNumber AS MeterNo
   ,DocumentNo    
   ,ClassName    
   ,BusinessUnitName    
   ,ServiceUnitName    
   ,ServiceCenterName    
   ,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
   ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
   ,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
   ,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
   ,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
   ,@ReadCode AS ReadCodeId   
  FROM [UDV_CustomerDescription] CD   
  --JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
  --JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
  --JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
  WHERE GlobalAccountNumber = @AccountNo    
  FOR XML PATH('Customerdetails'),TYPE    
 )    
 ,    
 (
	 SELECT TOP(1)  
	   CB.BillNo  
	  ,CustomerBillId AS CustomerBillID  
	  ,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
	  ,ISNULL(CB.PresentReading,'0') AS PresentReading  
	  ,ReadType  
	  ,Usage AS Consumption  
	  ,NetEnergyCharges  
	  ,NetFixedCharges  
	  ,TotalBillAmount  
	  ,VAT  
	  ,TotalBillAmountWithTax  
	  ,NetArrears  
	  ,TotalBillAmountWithArrears   
	  ,CD.GlobalAccountNumber AS AccountNo  
	  ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
	  ,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
			   ,CD.Service_Landmark      
			   ,CD.Service_City,''      
			   ,CD.Service_ZipCode) AS ServiceAddress  
	  ,CAD.OutStandingAmount AS TotalDueAmount  
	  ,Dials AS MeterDials  
	  ,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
	  ,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate  
	  ,@LastPaidAmount AS LastPaidAmount  
	  ,COUNT(0) OVER() AS TotalRecords  
	  ,BillMonth
	  ,BillYear
	  ,@ReadCode AS ReadCodeId 
	  ,1 AS IsSuccess
	 FROM Tbl_CustomerBills CB  
	 INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
	   AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
	 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
	 FOR XML PATH('CustomerBillDetails'),TYPE                    
)                  
FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
END       

GO
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkCustomerPayments_WithDT_Temp]    Script Date: 04/24/2015 21:08:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:		Karteek     
-- Create date: 24-04-2015   
-- Description: Insert Customer Bulk Payments. 
-- =============================================          
ALTER PROCEDURE [dbo].[USP_InsertBulkCustomerPayments_WithDT_Temp]          
(          
	 @CreatedBy VARCHAR(50)           
	,@DocumentPath VARCHAR(MAX)
	,@DocumentName VARCHAR(MAX)  
	,@BatchNo VARCHAR(50)
	,@TempCustomer TblPaymentUploads_TempType READONLY  
)          
AS          
BEGIN    
	
	INSERT INTO Temp_TblPaymentsUpload
	(
		 AccountNo 
		,AmountPaid 
		,ReceiptNO 
		,ReceivedDate 
		,PaymentMode
		,BatchNo 
		,BatchDate 
		,CreatedBy 
		,DocumentPath 
		,DocumentName 
		,CreatedDate 
	)
	SELECT 
		 AccountNo 
		,AmountPaid 
		,ReceiptNO 
		,ReceivedDate 
		,PaymentMode 
		,@BatchNo
		,dbo.fn_GetCurrentDateTime()
		,@CreatedBy
		,@DocumentPath
		,@DocumentName
		,dbo.fn_GetCurrentDateTime()
	FROM @TempCustomer	
		
	SELECT 1 As IsSuccess
		 		
END
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerReadToDirectChangeApproval]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: Update ReadToDirect Status change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerReadToDirectChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ReadToDirectChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@MeterNo VARCHAR(50)

	SELECT  
		 @ReadToDirectChangeLogId = C.value('(ReadToDirectChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	SELECT 
		 @MeterNo =MeterNo
		,@GlobalAccountNo = GlobalAccountNo
		,@Remarks = Remarks
	FROM Tbl_ReadToDirectCustomerActivityLogs
	WHERE ReadToDirectId = @ReadToDirectChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
											WHERE ReadToDirectId = @ReadToDirectChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
							
							UPDATE TBL_ReadToDirectCustomerActivity 
								SET IsLatest=0
							WHERE GlobalAccountNo = @GlobalAccountNo
							
							INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNo
														,@MeterNo
														,@ModifiedBy
														,DBO.fn_GetCurrentDateTime()
														)
														
							UPDATE Tbl_ReadToDirectCustomerActivityLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND  ReadToDirectId= @ReadToDirectChangeLogId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET ReadCodeID=1, MeterNumber = NULL,
								ModifedBy=@ModifiedBy,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @GlobalAccountNo		

							
							INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
										 ReadToDirectId
										,GlobalAccountNo
										,MeterNo
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole)  
									VALUES (  
										 @ReadToDirectChangeLogId
										,@GlobalAccountNo
										,@MeterNo
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId )
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_ReadToDirectCustomerActivityLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND ReadToDirectId = @ReadToDirectChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
					UPDATE TBL_ReadToDirectCustomerActivity 
								SET IsLatest=0
						WHERE GlobalAccountNo = @GlobalAccountNo
						
						INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNo
														,@MeterNo
														,@ModifiedBy
														,DBO.fn_GetCurrentDateTime()
														)
					
					UPDATE Tbl_ReadToDirectCustomerActivityLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifiedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApprovalStatusId = @ApprovalStatusId
							,Remarks = @Details
						WHERE GlobalAccountNo = @GlobalAccountNo  
						AND ReadToDirectId = @ReadToDirectChangeLogId 					
						
							
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL,
						ModifedBy=@ModifiedBy,ModifiedDate=dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @GlobalAccountNo		
					
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES (  
						 @ReadToDirectChangeLogId
						,@GlobalAccountNo
						,@MeterNo
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId )
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
																WHERE ReadToDirectId = @ReadToDirectChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_ReadToDirectCustomerActivityLogs 
						WHERE ReadToDirectId = @ReadToDirectChangeLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_ReadToDirectCustomerActivityLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND ReadToDirectId = @ReadToDirectChangeLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]
(  
@XmlDoc xml  
)
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT

	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ReadToDirectId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)			
													
			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
											 GlobalAccountNo
											,MeterNo
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											)
			
			SET @ReadToDirectId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE TBL_ReadToDirectCustomerActivity 
						SET IsLatest=0
					WHERE GlobalAccountNo = @GlobalAccountNumber
	
					INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNumber
														,@MeterNumber
														,@CreatedBy
														,DBO.fn_GetCurrentDateTime()
														)
												
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
		END
END
--AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@StatusText VARCHAR(200)='Initiation'
--		,@IsSuccessful BIT=1
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	SET @StatusText='Deserialization'
	
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	SET @StatusText='Update Statement'
	
--	UPDATE TBL_ReadToDirectCustomerActivity 
--	SET IsLatest=0
--	WHERE GlobalAccountNo = @GlobalAccountNumber
	
--	SET @StatusText='Insert Log Statement'
--	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
--												(
--												GlobalAccountNo
--												,MeterNumber
--												,Remarks
--												,CreatedBy 
--												,CreatedDate
--												,ApprovalStatusId
--												)
--										VALUES
--												(
--												@GlobalAccountNumber
--												,@MeterNumber
--												,@Reason
--												,@CreatedBy
--												,DBO.fn_GetCurrentDateTime()
--												,@ApprovalStatusId
--												)
--	SET @StatusText='Update Statement'
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--		SET ReadCodeID=1, MeterNumber = NULL
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('MastersBE'),TYPE

--END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsWithLimit]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid   
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo+' - '+CustomerView.GlobalAccountNumber) AS AccountNo  
			   ,CNL.CreatedBy  
			   ,CNL.Remarks  
			   ,CNL.OldTitle  
			   ,CNL.NewTitle  
			   ,CNL.OldFirstName AS OldName  
			   ,CNL.NewFirstName AS [NewName]  
			   ,CNL.OldMiddleName  
			   ,CNL.NewMiddleName  
			   ,CNL.OldLastName  
			   ,CNL.NewLastName  
			   ,CNL.OldKnownAs AS OldSurName  
			   ,CNL.NewKnownAs AS NewSurName  
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus
			   ,COUNT(0) OVER() AS TotalChanges  
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName			   
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId 
		 -- changed by karteek end      
		 ORDER BY CNL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAssignMeterChangeApproval]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: Update AssignMeter change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAssignMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@AssignMeterChangeId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(MAX) 

	SELECT  
		 @AssignMeterChangeId = C.value('(AssignMeterChangeId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(MAX)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
DECLARE
		 @GlobalAccountNo VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@AssignedMeterDate DATETIME
		,@InitialReading DECIMAL(18,2)
		,@Remarks VARCHAR(MAX)
		--,@PresentApprovalRole INT
		--,@NextApprovalRole INT
		,@CreatedBy VARCHAR(50)
		,@CreatedDate DATETIME
	
	SELECT 
		@GlobalAccountNo	 =GlobalAccountNo 
       ,@MeterNo             =MeterNo 
       ,@AssignedMeterDate   =AssignedMeterDate 
       ,@InitialReading      =InitialReading
       ,@Remarks             =Remarks
       ,@CreatedBy           =CreatedBy 
       ,@CreatedDate         =CreatedDate
	FROM Tbl_AssignedMeterLogs
	WHERE AssignedMeterId = @AssignMeterChangeId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_AssignedMeterLogs 
											WHERE AssignedMeterId = @AssignMeterChangeId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
									SET MeterNumber=@MeterNo
										,ReadCodeID=2
							WHERE GlobalAccountNumber = @GlobalAccountNo
			
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							SET InitialReading=@InitialReading
								,PresentReading=@InitialReading
							WHERE GlobalAccountNumber=@GlobalAccountNo

							
							INSERT INTO Tbl_Audit_AssignedMeterLogs(  
										 AssignedMeterId
										,GlobalAccountNo
										,MeterNo
										,AssignedMeterDate
										,InitialReading
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole)  
									VALUES(  
										 @AssignMeterChangeId
										,@GlobalAccountNo
										,@MeterNo
										,@AssignedMeterDate
										,@InitialReading
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId
										)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_AssignedMeterLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNo = @GlobalAccountNo  
					AND AssignedMeterId = @AssignMeterChangeId 

					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNo
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNo
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
					WHERE GlobalAccountNumber=@GlobalAccountNo
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					VALUES(  
						 @AssignMeterChangeId
						,@GlobalAccountNo
						,@MeterNo
						,@AssignedMeterDate
						,@InitialReading
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy  
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId
						)
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_AssignedMeterLogs 
																WHERE AssignedMeterId = @AssignMeterChangeId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AssignedMeterLogs 
						WHERE AssignedMeterId = @AssignMeterChangeId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AssignedMeterLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND AssignedMeterId = @AssignMeterChangeId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
		
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0)) as Usage
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	  
	INNER JOIN Tbl_Cycles Cycles On	Cycles.ServiceCenterId=@ServiceCenterID and Cycles.CycleId=BN.CycleId
	GROUP BY CR.GlobalAccountNumber
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT
	(
		SELECT ClassID
			,SUM(case when isnull(ReadCodeID,0)=2 then 1 else 0 end) as TotalReadCustomers
			,SUM(case when isnull(ReadCodeID,0)=1 then 1 else 0 end) as EstimatedCustomers
			,TC.ClassName
			,CC.CategoryName
			,CC.ClusterCategoryId 
			,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
			,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
			,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
			,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
			,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
			,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
			,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
			,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
			,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
			,Cycles.CycleId 
			,Cycles.CycleName
		FROM Tbl_MTariffClasses(NOLOCK)	TC
		INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
		INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
		INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
		CROSS JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC
		LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
				and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
				And ES.BillingMonth = @Month -- Need To Modify	 
				AND ES.BillingYear = @Year	 -- Need To Modify
		Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
		ORDER BY Cycles.CycleId,CC.ClusterCategoryId
		FOR XML PATH('EstimationDetails'),TYPE
	)
	,
	(
		SELECT	
			 @Capacity as CapacityInKVA
			,@SUPPMConsumption as SUPPMConsumption
			,@SUCreditConsumption as SUCreditConsumption
		FOR XML PATH('EstimatedUsageDetails'),TYPE 
	)
	FOR XML PATH(''),ROOT('EstimationInfoByXml')
	
	DROP TABLE #ReadCustomersList
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 
ALTER PROCEDURE  [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	--Insert xml data into temp table   
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo


							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END


							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId


							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------


							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   


							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1


						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3 or isnull(@BookDisableType,0) =1	 
						   BEGIN
						   GOTO Loop_End;
						   END    


							 

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										

										IF	  @DisableDate IS NOT NULL   and   DATEDIFF(DAY, @DisableDate ,@LastBillGenerated) < 15
											BEGIN
											   SET	 @FixedCharges=0
											END
										ELSE
											BEGIN
											INSERT INTO @tblFixedCharges(ClassID ,Amount)
											SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
											SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
												
											END
									 
							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAssignMeterLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAssignMeterLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.AssignedMeterId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,CONVERT(VARCHAR(50),T.AssignedMeterDate,103) AS MeterAssignedDate
		 ,T.InitialReading
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_AssignedMeterLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_GetBillTextFilesForCustomers]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  <NEERAJ KANOJIYA>            
-- Create date: <19-March-2014>            
-- Description: <GET EBILL DAILY DETAILS>            
-- =============================================            
ALTER PROCEDURE  [dbo].[UPS_GetBillTextFilesForCustomers]           
(          
@XmlDoc XML         
)           
AS            
BEGIN           
        
 Declare	@Month INT,
			@Year INT,
			@BU_ID VARCHAR(50)     
      
           
 SELECT          
  @Month = C.value('(Month)[1]','INT')          
  ,@Year = C.value('(Year)[1]','INT')       
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')       
 FROM @XmlDoc.nodes('BillGenerationBe') as T(C)           
  
    
 SELECT CB.AccountNo AS GlobalAccountNumber  
   ,CD.OldAccountNo AS OldAccountNo  
   ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
   ,dbo.fn_GetCustomerServiceAddress_New(CPAD.HouseNo,CPAD.StreetName,CPAD.Landmark,CPAD.City,CPAD.Details,CPAD.ZipCode) AS ServiceAddress  
   ,RC.ReadCode AS ReadType  
   ,TC.ClassName AS Tariff  
   ,BD.BillFilePath AS FilePath  
 FROM Tbl_BillDetails BD  
 JOIN Tbl_CustomerBills CB ON CB.CustomerBillId = BD.CustomerBillID   
        AND CB.BillMonth=@Month  
        AND CB.BillYear=@Year
        AND CB.BU_ID = @BU_ID  -----'BEDC_BU_0004'--
 JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = CB.AccountNo  
 LEFT JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId  
 LEFT JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId  
 LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails CPAD   
      ON CPAD.GlobalAccountNumber = CB.AccountNo   
      AND CPAD.IsActive=1  
      
  --SELECT CY.ServiceCenterId ,        
  --  BS.BillingYear,        
  --  BillingMonth,        
  --  SCDetails.BusinessUnitName,        
  --  SCDetails.ServiceCenterName,          
  --  SCDetails.ServiceUnitName,        
  --  '~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,         
  --  --MAX(BS.BillingFile) As FilePath,          
  --  dbo.fn_GetMonthName(BillingMonth) as [MonthName]          
  --FROM Tbl_BillingQueueSchedule BS            
  --inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId            
  --and BillGenarationStatusId=1        
  --AND BS.BillingMonth=2--@Month         
  --AND BS.BillingYear=2015--@Year          
  --INNER JOIN          
  --[UDV_ServiceCenterDetails] SCDetails          
  --ON SCDetails.ServiceCenterId=CY.ServiceCenterId          
  --Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth           
  --,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,          
  --SCDetails.ServiceUnitName            
         
END 
GO

/****** Object:  StoredProcedure [MASTERS].[USP_Re-AssignMeter]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 5-MARCH-2015  
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_Re-AssignMeter]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
		  @MeterNumber VARCHAR(50)    
		  ,@ReadCodeID VARCHAR(50)   
		  ,@GlobalAccountNumber VARCHAR(50)   
		  ,@IsSuccessful BIT =1  
		  ,@ApprovalStatusId INT
		  ,@IsFinalApproval BIT = 0
		  ,@FunctionId INT
		  ,@Reason VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50)
		  ,@InitialReading INT
     
  SELECT    
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
 
 	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			SET @InitialReading=(SELECT ISNULL(ISNULL(PresentReading,0),ISNULL(InitialReading,0)) FROM CUSTOMERS.Tbl_CustomerActiveDetails
									WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,dbo.fn_GetCurrentDateTime()
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN					
				IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
					BEGIN
					
						UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
							 SET MeterNumber=@MeterNumber  
							  ,ReadCodeID=@ReadCodeID  
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						INSERT INTO Tbl_Audit_AssignedMeterLogs(  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks
								,ApprovalStatusId
								,CreatedBy
								,CreatedDate
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks  
								,ApprovalStatusId  
								,CreatedBy  
								,CreatedDate  
								,PresentApprovalRole
								,NextApprovalRole
							FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
						
						SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')	
					END
				ELSE
					BEGIN
						SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
					END
				END
		END
END
		
--DECLARE   
--		  @MeterNumber VARCHAR(50)    
--		  ,@ReadCodeID VARCHAR(50)   
--		  ,@GlobalAccountNumber VARCHAR(50)   
--		  ,@IsSuccessful BIT =1  
--		  ,@StatusText VARCHAR(200)
     
--BEGIN  
--BEGIN TRY  
-- BEGIN TRAN  
     
-- SET @StatusText='Deserialization'  
--  SELECT    
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
--		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    
--  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
-- IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
--	 BEGIN 
--		 SET @StatusText='Update Statement'  
		   
--		 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
--		 SET MeterNumber=@MeterNumber  
--		  ,ReadCodeID=@ReadCodeID  
--		 WHERE GlobalAccountNumber = @GlobalAccountNumber  
		   	   
--		 SET @StatusText='Success'
--	 END
-- ELSE 
--	BEGIN
--		SET @StatusText='Meter is in DeActivate state.'
--		SET @IsSuccessful =0  
--	END 
-- COMMIT TRAN   
--END TRY      
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @IsSuccessful =0  
--END CATCH  
--END  
-- SELECT   
--   @IsSuccessful AS IsSuccessful  
--   ,@StatusText AS StatusText  
-- FOR XML PATH('CustomerRegistrationBE'),TYPE  
  
--END  
--------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME  
      
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
 IF(@GlobalAccountNumber !='')  
 BEGIN  
     
     
         
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=ServiceCenterId  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)  
      BEGIN  
         
         
       SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)  
        ,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo   
       FROM Tbl_CustomerBills(NOLOCK)    
       WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month  
       DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId  
  
  
       DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId  
       SELECT @PaidAmount=SUM(PaidAmount)  
       FROM Tbl_CustomerBillPayments(NOLOCK)    
       WHERE BillNo=@CusotmerPreviousBillNo  
  
  
       IF @PaidAmount IS NOT NULL  
       BEGIN  
              
        SET @OutStandingAmount=@OutStandingAmount+@PaidAmount  
        UPDATE Tbl_CustomerBillPayments  
        SET BillNo=NULL  
        WHERE BillNo=@RegenCustomerBillId  
       END  
  
  
       ----------------------Update Readings as is billed =0 ----------------------------  
       UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId  
       -- Insert Paid Meter Payments  
       DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId  
  
  
       update CUSTOMERS.Tbl_CustomerActiveDetails  
       SET OutStandingAmount=@OutStandingAmount  
       where GlobalAccountNumber=@GlobalAccountNumber  
       ----------------------------------------------------------------------------------  
  
  
       --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
       DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId  
       -------------------------------------------------------------------------------------  
         
       --------------------------------------------------------------------------------------  
       --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
       --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
       --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
         
       ---------------------------------------------------------------------------------------  
       --Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
       ---------------------------------------------------------------------------------------  
      END  
        
          
           
         --IF  isnull(@ActiveStatusId,0) != 1   or  isnull(@ActiveStatusId,0) !=2    or isnull(@CustomerTypeID,0) = 3    
         --BEGIN  
         --RETURN  
         --END      
  
  
       select   @IspartialClose=IsPartialBill,  
          @BookDisableType=DisableTypeId,  
          @DisableDate=DisableDate  
       from Tbl_BillingDisabledBooks     
       where BookNo=@BookNo and YearId=@Year and MonthId=@Month and IsActive=1  
  
  
       IF isnull(@BookDisableType,0)   = 1  
         RETURN  
  
       IF @ReadCodeId=2 --   
       BEGIN  
        SELECT  @PreviousReading=PreviousReading,  
          @CurrentReading = PresentReading,@Usage=Usage,  
          @LastBillGenerated=LastBillGenerated,  
          @PreviousBalance =Previousbalance,  
          @BalanceUnits=BalanceUsage,  
          @IsFromReading=IsFromReading,  
          @PrevCustomerBillId=CustomerBillId,  
          @PreviousBalance=PreviousBalance,  
          @Multiplier=Multiplier,  
          @ReadDate=ReadDate  
        FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)  
  
        IF @IsFromReading=1  
        BEGIN  
          SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing  
          IF @Usage<@BalanceUnits  
          BEGIN  
           SET @ActualUsage=@Usage  
           SET @Usage=0  
           SET @RemaningBalanceUsage=@BalanceUnits-@Usage  
           SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing  
          END  
          ELSE  
          BEGIN  
           SET @ActualUsage=@Usage  
           SET @Usage=@Usage-@BalanceUnits  
           SET @RemaningBalanceUsage=0  
          END  
        END  
        ELSE  
        BEGIN  
          SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing  
          SET @ActualUsage=@Usage  
          SET @RemaningBalanceUsage=@Usage+@BalanceUnits  
        END  
       END  
       ELSE -- -- Direct customer  
       BEGIN  
  
        set @IsEstimatedRead =1  
        -- Get balance usage of the customer  
        SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId,   
             @PreviousBalance=PreviousBalance  
        FROM Tbl_CustomerBills (NOLOCK)        
        WHERE AccountNo = @GlobalAccountNumber         
        ORDER BY CustomerBillId DESC   
          
        IF @PreviousBalance IS NULL  
        BEGIN  
         SET @PreviousBalance=@OpeningBalance  
        END  
  
        SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)  
        SET @ReadType=1 -- Direct  
        SET @ActualUsage=@Usage  
       END  
  
      IF @Usage<>0  
       SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId)   
      ELSE  
       SET @EnergyCharges=0  
         
      SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01')   
      DECLARE @tblFixedCharges AS TABLE(ClassID int,Amount Decimal(18,2))  
      ------------------------------------------------------------------------------------   
      IF  isnull(@IspartialClose,0) =   1  or  isnull(@ActiveStatusId,0) =2  
       BEGIN  
        SET @FixedCharges=0  
       END  
      ELSE   
       BEGIN  
  
          IF @BookDisableType = 2  -- Temparary Close  
          BEGIN  
           SET  @EnergyCharges=0  
          END     
              INSERT INTO @tblFixedCharges(ClassID ,Amount)  
            SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);  
            SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges   
          --IF  @DisableDate !=NULL and  DATEDIFF(DAY, @DisableDate,@LastBillGenerated) >= 15  
          -- BEGIN  
          --    SET  @FixedCharges=0  
          -- END  
          --ELSE  
          -- BEGIN  
            
          -- END  
            
        END  
      ------------------------------------------------------------------------------------------------------------------------------------  
      SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)  
      IF @GetPaidMeterBalance>0  
      BEGIN  
         
       IF @GetPaidMeterBalance<@FixedCharges  
       BEGIN  
        SET @GetPaidMeterBalanceAfterBill=0  
        SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance  
        SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing  
       END  
       ELSE  
       BEGIN  
        SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges  
        SET @FixedCharges=0  
       END  
      END  
      ------------------------  
      -- Caluclate tax here  
        
      SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )  
  
  
       IF  @PrevCustomerBillId IS NULL   
       BEGIN  
          
          
        SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments   
          WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0   
             
       END  
      ELSE  
       BEGIN  
        SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments   
          WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId      
         
       END  
        
      IF @AdjustmentAmount IS NULL  
       SET @AdjustmentAmount=0  
        
      set @NetArrears=@OutStandingAmount-@AdjustmentAmount  
      --SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)  
        
      SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)  
        
      SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)  
        
        
        
      SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage)   
       FROM Tbl_CustomerBills (NOLOCK)        
       WHERE AccountNo = @GlobalAccountNumber  
       Group BY CustomerBillId         
       ORDER BY CustomerBillId DESC   
         
      if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00  
       SET @AverageUsageForNewBill=@Usage  
        
      if @RemainingBalanceUnits IS NULL  
       set @RemainingBalanceUnits=0  
         
       -------------------------------------------------------------------------------------------------------  
      SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))  
  
      FROM Tbl_LEnergyClassCharges   
      WHERE ClassID =@TariffId  
      AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   
      
       FOR XML PATH(''), TYPE)  
       .value('.','NVARCHAR(MAX)'),1,0,'')  )   
         
       
         
         
      -------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------  
        
       select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)  
      
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmountWithTax     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
      
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
 SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)  
 END  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAddressChangeApproval]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAddressChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@AddressChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT

	SELECT  
		 @AddressChangeLogId = C.value('(AddressChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@TempAddressId INT,
		@TempServiceId INT
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNumber 
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		FROM Tbl_CustomerAddressChangeLog_New
		WHERE AddressChangeLogId=@AddressChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerAddressChangeLog_New
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					AND  AddressChangeLogId= @AddressChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedDate
									,CreatedBy)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
																WHERE AddressChangeLogId = @AddressChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
						WHERE AddressChangeLogId = @AddressChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerAddressChangeLog_New
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			AND AddressChangeLogId = @AddressChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerInBillProcess]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Neeraj Kanojiya>  
-- Create date: <15-MAY-2014>  
-- Description: <CHECK CUSTOMER IS IN BILLING PROCESS.>  
-- Modified By : Padmini  
--Modified Date:27-12-2014  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_IsCustomerInBillProcess]  
(  
 --Input parameters as an xml file.  
 @XmlDoc XML  
)  
AS  
BEGIN  
 DECLARE   
   @AccountNo VARCHAR(50)  
     ,@BillGenarationStatusId int  
     ,@Status bit=1  
     ,@Flag int=1  
     ,@Count int=0  
     ,@CycleId Varchar(50)  
     ,@BookNO Varchar(50)  
     --,@FeederId Varchar(50)  
     ,@OpenStatusId INT   
     ,@Year VARCHAR(10)  
     ,@Month VARCHAR(2)  
       
 SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
     ,@CycleId = C.value('(CycleId)[1]','VARCHAR(50)')  
     ,@Flag = C.value('(Flag)[1]','INT')  
     ,@BookNo = C.value('(BookNo)[1]','Varchar(50)')  
     --,@FeederId = C.value('(FeederId)[1]','Varchar(50)')  
     ,@Year = C.value('(BillYear)[1]','Varchar(10)')  
     ,@Month = C.value('(BillMonth)[1]','Varchar(2)')  
 FROM @XmlDoc.nodes('MastersBE') AS T(C)   
   
 --Checking based on one Account number  
 IF(@Flag=1)  
  BEGIN  
   SET @BillGenarationStatusId =  (SELECT TOP 1 BillGenarationStatusId FROM Tbl_BillingQueeCustomers WHERE AccountNo=@AccountNo AND BillGenarationStatusId!=4 Order BY BillQueueCustomerId DESC)  
   IF(@BillGenarationStatusId =4 OR @BillGenarationStatusId IS NULL )  
    set @Status=0  
  END  
 --Checking based on one cycle id  
 ELSE IF(@Flag=2)  
  BEGIN  
   SET @Count=  
   --(SELECT count(0) BillGenarationStatusId FROM Tbl_BillingQueeCustomers   
   --WHERE AccountNo IN(SELECT A.AccountNo FROM tbl_CustomerDetails as a   
   --      JOIN Tbl_BookNumbers as b on a.BookNo=b.Bookno   
   --      WHERE B.CycleId = @CycleId)  
   (SELECT count(0) BillGenarationStatusId FROM Tbl_BillingQueeCustomers   
   WHERE AccountNo IN(SELECT A.GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] as a   
         JOIN Tbl_BookNumbers as b on a.BookNo=b.Bookno   
         WHERE B.CycleId = @CycleId)  
   AND BillGenarationStatusId != 4)  
   if(@count =0 OR @count IS NULL )  
   SET @Status=0     
  END  
 --Checking based on one book number  
 ELSE IF(@Flag=3)  
  BEGIN  
   SET @Count=  
   --(SELECT count(0) BillGenarationStatusId FROM Tbl_BillingQueeCustomers   
   --      WHERE AccountNo IN(SELECT   
   --      accountno from tbl_customerdetails WHERE   
   --      bookno=@BookNo)  
   (SELECT count(0) BillGenarationStatusId FROM Tbl_BillingQueeCustomers   
         WHERE AccountNo IN(SELECT   
         GlobalAccountNumber from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE   
         bookno=@BookNo)  
   AND BillGenarationStatusId != 4)  
   IF(@count =0 OR @count IS NULL )  
    SET @Status=0     
  END  
 ----Checking based on feeder Id  
 --ELSE IF(@Flag=4)  
 -- BEGIN  
 --  SET @Count=  
 --  (SELECT count(0) BillGenarationStatusId FROM Tbl_BillingQueeCustomers   
 --        WHERE AccountNo IN(SELECT   
 --        accountno from tbl_customerdetails WHERE   
 --        FeederId=@FeederId)  
 --  AND BillGenarationStatusId != 4)  
 --  IF(@count =0 OR @count IS NULL )  
 --   SET @Status=0     
 -- END  
  
 --Checking based on one Account and Billing Opening Status number  
 IF(@Flag=5)  
  BEGIN  
   SET @BillGenarationStatusId =  (SELECT TOP 1 BillGenarationStatusId FROM Tbl_BillingQueeCustomers   
                    WHERE AccountNo=@AccountNo AND [Year]=@Year   
                    AND [MONTH]=@Month AND BillGenarationStatusId!=4)  
   IF(@BillGenarationStatusId =1 OR @BillGenarationStatusId IS NULL )  
    BEGIN  
     set @Status=0  
     SET @OpenStatusId= (SELECT OPENSTATUSID FROM Tbl_BillingMonths WHERE [MONTH]=@Month AND [YEAR]=@Year)              
    END  
  END  
 --Retruning result as xml for MasterBE Class  
 SELECT @Status AS [Status],@OpenStatusId AS BillOpenStatusID FOR XML PATH('MastersBE')  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      
      
      SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
      SEt @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
    SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
      
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)  
      
    SELECT TOP(1) @LastPaidDate = RecievedDate  
  ,@LastPaidAmount = PaidAmount   
 FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo  
 ORDER BY CustomerPaymentID DESC  
      
      
SELECT(      
  
  SELECT     
    dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
   ,GlobalAccountNumber AS AccountNo    
   ,OldAccountNo 
   ,MeterNumber AS MeterNo
   ,DocumentNo    
   ,ClassName    
   ,BusinessUnitName    
   ,ServiceUnitName    
   ,ServiceCenterName    
   ,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
   ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
   ,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
   ,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
   ,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
   ,@ReadCode AS ReadCodeId   
  FROM [UDV_CustomerDescription] CD   
  --JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
  --JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
  --JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
  WHERE GlobalAccountNumber = @AccountNo    
  FOR XML PATH('Customerdetails'),TYPE    
 )    
 ,    
 (
	 SELECT TOP(1)  
	   CB.BillNo  
	  ,CustomerBillId AS CustomerBillID  
	  ,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
	  ,ISNULL(CB.PresentReading,'0') AS PresentReading  
	  ,ReadType  
	  ,Usage AS Consumption  
	  ,NetEnergyCharges  
	  ,NetFixedCharges  
	  ,TotalBillAmount  
	  ,VAT  
	  ,TotalBillAmountWithTax  
	  ,NetArrears  
	  ,TotalBillAmountWithArrears   
	  ,CD.GlobalAccountNumber AS AccountNo  
	  ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
	  ,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
			   ,CD.Service_Landmark      
			   ,CD.Service_City,''      
			   ,CD.Service_ZipCode) AS ServiceAddress  
	  ,CAD.OutStandingAmount AS TotalDueAmount  
	  ,Dials AS MeterDials  
	  ,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
	  ,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate  
	  ,@LastPaidAmount AS LastPaidAmount  
	  ,COUNT(0) OVER() AS TotalRecords  
	  ,BillMonth
	  ,BillYear
	  ,@ReadCode AS ReadCodeId 
	  ,1 AS IsSuccess
	 FROM Tbl_CustomerBills CB  
	 INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
	   AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
	 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
	 FOR XML PATH('CustomerBillDetails'),TYPE                    
)                  
FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 6 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		   ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		  ,T.AddressChangeLogId
		  ,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		  ,CD.OldAccountNo AS OldAccountNumber 
		  ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldPostal_HouseNo,T.OldPostal_StreetName,
			T.OldPostal_Landmark,T.OldPostal_City,T.OldPostal_AreaCode,T.OldPostal_ZipCode) AS OldPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewPostal_HouseNo,T.NewPostal_StreetName,
		    T.NewPostal_Landmark,T.NewPostal_City,T.NewPostal_AreaCode,T.NewPostal_ZipCode) AS NewPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldService_HouseNo,T.OldService_StreetName,
			 T.OldService_Landmark,T.OldService_City,T.OldService_AreaCode,T.OldService_ZipCode) AS OldServiceAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewService_HouseNo,T.NewService_StreetName,
		   T.NewService_Landmark,T.NewService_City,T.NewService_AreaCode,T.NewService_ZipCode) AS NewServiceAddress
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerAddressChangeLog_New T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole,
							IsPostalCommunication,
							IsServiceComunication)  
					SELECT  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole,
							IsPostalCommunication,
							IsServiceComunication
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: The purpose of this procedure is to get list of ReadToDirect Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 12 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.ReadToDirectId
		 ,T.MeterNo
		  ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_ReadToDirectCustomerActivityLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterNoChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Meter NO Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 7 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.MeterInfoChangeLogId
		 ,(CD.AccountNo+' - '+  T.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,OldMeterNo
		 ,NewMeterNo
		 ,OldMeterReading
		 ,NewMeterInitialReading
		 ,CONVERT(VARCHAR(20),MeterChangedDate,106) AS MeterChangedDate
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerMeterInfoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.CustomerTypeChangeLogId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldCustomerTypeId
		 ,T.NewCustomerTypeId
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.OldCustomerTypeId) AS OldCustomerType
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.NewCustomerTypeId) AS NewCustomerType
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerTypeChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 8 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.NameChangeLogId
		,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber  
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,ISNULL(T.OldTitle,'--') AS OldTitle
		 ,ISNULL(T.NewTitle,'--') AS Title
		 ,ISNULL(T.OldFirstName,'--') AS OldFirstName
		 ,ISNULL(T.NewFirstName,'--') AS FirstName
		 ,ISNULL(T.OldMiddleName,'--') AS OldMiddleName
		 ,ISNULL(T.NewMiddleName,'--') AS MiddleName
		 ,ISNULL(T.OldLastName,'--') AS OldLastName
		 ,ISNULL(T.NewLastName,'--') AS LastName
		 ,OldKnownAs
		 ,NewKnownAs AS KnownAs
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerNameChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 01-04-2015
-- Description: The purpose of this procedure is to get list of Tariff Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 3 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.TariffChangeRequestId
		,(CD.AccountNo+' - '+  T.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
				 ,CD.Service_Landmark    
				 ,CD.Service_City,'',    
				 CD.Service_ZipCode) As ServiceAddress
		 ,PreviousTariffId AS OldClassID
		 ,ChangeRequestedTariffId AS NewClassID
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=PreviousTariffId) AS PreviousTariffName
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=ChangeRequestedTariffId) AS ChangeRequestedTariffName
		 ,T.OldClusterCategoryId AS OldClusterTypeId
		 ,T.NewClusterCategoryId AS NewClusterTypeId
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.OldClusterCategoryId) AS PreviousClusterName
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.NewClusterCategoryId) AS ChangeRequestedClusterName
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_LCustomerTariffChangeRequest T 
	INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerStatusChangeLogsToApprove]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Status Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerStatusChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 9 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.ActiveStatusChangeLogId
		 ,(CD.AccountNo+' - '+  T.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldStatus AS OldStatusId
		 ,T.NewStatus AS NewStatusId
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.OldStatus) AS OldStatus
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.NewStatus) AS NewStatus
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerActiveStatusChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[UPS_GetBillTextFiles]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <NEERAJ KANOJIYA>        
-- Create date: <19-March-2014>        
-- Description: <GET EBILL DAILY DETAILS>        
-- =============================================        
ALTER PROCEDURE  [dbo].[UPS_GetBillTextFiles]       
(      
@XmlDoc XML     
)       
AS        
BEGIN       
    
Declare @Month INT,      
  @Year INT    
      
SELECT      
   @Month = C.value('(Month)[1]','INT')      
   ,@Year = C.value('(Year)[1]','INT')      
  FROM @XmlDoc.nodes('BillGenerationBe') as T(C)       
       
    
 SELECT CY.ServiceCenterId ,    
   BS.BillingYear,    
   BillingMonth,    
   SCDetails.BusinessUnitName,    
   SCDetails.ServiceCenterName,      
   SCDetails.ServiceUnitName,    
   --'~\GeneratedBills\Tel_HYD01\Tel_HYD_SU_01\Tel_HYD_SC_01\February2015\Tel_HYD_BG_01.txt' AS FilePath,     
   MAX(BS.BillingFile) As FilePath,      
   dbo.fn_GetMonthName(BillingMonth) as [MonthName]      
 FROM Tbl_BillingQueueSchedule BS        
 inner join Tbl_Cycles CY on    BS.CycleId=cy.CycleId        
 and BillGenarationStatusId=4    
 AND BS.BillingMonth=@Month     --2
 AND BS.BillingYear=@Year      --2015
 INNER JOIN      
 [UDV_ServiceCenterDetails] SCDetails      
 ON SCDetails.ServiceCenterId=CY.ServiceCenterId      
 Group by CY.ServiceCenterId ,  BS.BillingYear,BillingMonth       
 ,SCDetails.BusinessUnitName,SCDetails.ServiceCenterName,      
 SCDetails.ServiceUnitName        
    
END    

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBulkAgerageUpload_WithDT]    Script Date: 04/24/2015 21:08:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 23 Apr 2015  
-- Description: AverageReading details are insertion if exists updation for those customers 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_InsertBulkAgerageUpload_WithDT]  
(  
	@TblAvgUpload TblAvgBulkUpload READONLY  
)  
AS  
BEGIN  
  
    
	UPDATE DC   
	SET DC.AverageReading = Tc.AverageReading  
		,DC.ModifiedBy = TC.CreatedBy  
		,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
	FROM Tbl_DirectCustomersAvgReadings DC  
	INNER JOIN @TblAvgUpload TC ON TC.AccNum = DC.GlobalAccountNumber  
	WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM @TblAvgUpload)  
        
	INSERT INTO Tbl_DirectCustomersAvgReadings  
	(  
		GlobalAccountNumber  
		,AverageReading  
		,CreatedBy  
		,CreatedDate  
	)  
	SELECT TC.AccNum  
		,TC.AverageReading  
		,TC.CreatedBy  
		,dbo.fn_GetCurrentDateTime()  
	FROM Tbl_DirectCustomersAvgReadings DC 
	RIGHT JOIN @TblAvgUpload TC on TC.AccNum = DC.GlobalAccountNumber 
	WHERE GlobalAccountNumber IS NULL

	SELECT 1 AS IsSuccess   
    
END  
  
GO




