GO
DROP TABLE Tbl_BookNoChangeLogs
GO
CREATE TABLE Tbl_BookNoChangeLogs(
	 BookNoChangeLogId INT IDENTITY(1,1)
	,GlobalAccountNo VARCHAR(50)
	,OldBU_ID VARCHAR(20)
	,NewBU_ID VARCHAR(20)
	,OldSU_ID VARCHAR(20)
	,NewSU_ID VARCHAR(20)
	,OldSC_ID VARCHAR(20)
	,NewSC_ID VARCHAR(20)
	,OldCycleId VARCHAR(20)
	,NewCycleId VARCHAR(20)
	,OldBookNo VARCHAR(20)
	,NewBookNo VARCHAR(20)
	,OldPostal_HouseNo VARCHAR(100) 
	,OldPostal_StreetName VARCHAR(255) 
	,OldPostal_City VARCHAR(100) 
	,OldPostal_Landmark VARCHAR(100) 
	,OldPostal_AreaCode VARCHAR(100) 
	,OldPostal_ZipCode VARCHAR(100) 
	,NewPostal_HouseNo VARCHAR(100) 
	,NewPostal_StreetName VARCHAR(255) 
	,NewPostal_City VARCHAR(100) 
	,NewPostal_Landmark VARCHAR(100) 
	,NewPostal_AreaCode VARCHAR(100) 
	,NewPostal_ZipCode VARCHAR(100) 
	,OldService_HouseNo VARCHAR(100) 
	,OldService_StreetName VARCHAR(255) 
	,OldService_City VARCHAR(100) 
	,OldService_Landmark VARCHAR(100) 
	,OldService_AreaCode VARCHAR(100) 
	,OldService_ZipCode VARCHAR(100) 
	,NewService_HouseNo VARCHAR(100) 
	,NewService_StreetName VARCHAR(255) 
	,NewService_City VARCHAR(100) 
	,NewService_Landmark VARCHAR(100) 
	,NewService_AreaCode VARCHAR(100) 
	,NewService_ZipCode VARCHAR(100) 
	,ApprovalStatusId INT DEFAULT 0
	,PresentApprovalRole INT DEFAULT 0
	,NextApprovalRole INT DEFAULT 0
	,OldServiceAddressID INT 
	,NewServiceAddressID INT 
	,OldPostalAddressID INT 
	,NewPostalAddressID INT 
	,OldIsSameAsService INT 
	,NewIsSameAsService INT 
	,IsPostalCommunication BIT 
	,IsServiceComunication BIT 
	,Remarks VARCHAR(MAX) 
	,CreatedBy VARCHAR(50) 
	,CreatedDate DATETIME 
	,ModifiedBy VARCHAR(50) 
	,ModifiedDate DATETIME
)
GO
CREATE TABLE Tbl_Audit_BookNoChangeLogs(
	 BookNoChangeLogId INT
	,GlobalAccountNo VARCHAR(50)
	,OldBU_ID VARCHAR(20)
	,NewBU_ID VARCHAR(20)
	,OldSU_ID VARCHAR(20)
	,NewSU_ID VARCHAR(20)
	,OldSC_ID VARCHAR(20)	
	,NewSC_ID VARCHAR(20)
	,OldCycleId VARCHAR(20)
	,NewCycleId VARCHAR(20)	
	,OldBookNo VARCHAR(20)
	,NewBookNo VARCHAR(20)
	,OldPostal_HouseNo VARCHAR(100) 
	,OldPostal_StreetName VARCHAR(255) 
	,OldPostal_City VARCHAR(100) 
	,OldPostal_Landmark VARCHAR(100) 
	,OldPostal_AreaCode VARCHAR(100) 
	,OldPostal_ZipCode VARCHAR(100) 
	,NewPostal_HouseNo VARCHAR(100) 
	,NewPostal_StreetName VARCHAR(255) 
	,NewPostal_City VARCHAR(100) 
	,NewPostal_Landmark VARCHAR(100) 
	,NewPostal_AreaCode VARCHAR(100) 
	,NewPostal_ZipCode VARCHAR(100) 
	,OldService_HouseNo VARCHAR(100) 
	,OldService_StreetName VARCHAR(255) 
	,OldService_City VARCHAR(100) 
	,OldService_Landmark VARCHAR(100) 
	,OldService_AreaCode VARCHAR(100) 
	,OldService_ZipCode VARCHAR(100) 
	,NewService_HouseNo VARCHAR(100) 
	,NewService_StreetName VARCHAR(255) 
	,NewService_City VARCHAR(100) 
	,NewService_Landmark VARCHAR(100) 
	,NewService_AreaCode VARCHAR(100) 
	,NewService_ZipCode VARCHAR(100) 
	,ApprovalStatusId INT DEFAULT 0
	,PresentApprovalRole INT DEFAULT 0
	,NextApprovalRole INT DEFAULT 0
	,OldServiceAddressID INT 
	,NewServiceAddressID INT 
	,OldPostalAddressID INT 
	,NewPostalAddressID INT 
	,OldIsSameAsService INT 
	,NewIsSameAsService INT 
	,IsPostalCommunication BIT 
	,IsServiceComunication BIT 
	,Remarks VARCHAR(MAX) 
	,CreatedBy VARCHAR(50) 
	,CreatedDate DATETIME 
	,ModifiedBy VARCHAR(50) 
	,ModifiedDate DATETIME
)
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1
WHERE FunctionId=4
GO 
Update Tbl_Menus SET Name = 'Customer Bill Payments' WHERE [Path] = '../Masters/CustomerBillPayment.aspx' AND MenuId = 78
GO 
Update Tbl_Menus SET Name = 'Customer Advance Payments' WHERE [Path] = '../Billing/PaymentsToAccountNo.aspx' AND MenuId = 23
GO