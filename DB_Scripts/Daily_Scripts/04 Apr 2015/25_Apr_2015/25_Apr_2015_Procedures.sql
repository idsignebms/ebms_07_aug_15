
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerBookNoChangeApproval]    Script Date: 04/27/2015 11:41:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerBookNoChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @BookNoChangeLogId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ApprovalStatusId INT

	SELECT  
		 @BookNoChangeLogId = C.value('(BookNoChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@OldBU_ID VARCHAR(20),
		@NewBU_ID VARCHAR(20),
		@OldSU_ID VARCHAR(20),
		@NewSU_ID VARCHAR(20),
		@OldSC_ID VARCHAR(20),	
		@NewSC_ID VARCHAR(20),
		@OldCycleId VARCHAR(20),
		@NewCycleId VARCHAR(20),	
		@OldBookNo VARCHAR(20),
		@NewBookNo VARCHAR(20),
		@TempAddressId INT,
		@TempServiceId INT,
		@AccountNo VARCHAR(50)	
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNo
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		,@OldBU_ID					 = OldBU_ID 
		,@NewBU_ID					 = NewBU_ID 
		,@OldSU_ID					 = OldSU_ID 
		,@NewSU_ID					 = NewSU_ID 
		,@OldSC_ID					 = OldSC_ID 
		,@NewSC_ID					 = NewSC_ID 
		,@OldCycleId				 = OldCycleId
		,@NewCycleId				 = NewCycleId
		,@OldBookNo					 = OldBookNo
		,@NewBookNo					 = NewBookNo
		FROM Tbl_BookNoChangeLogs
		WHERE BookNoChangeLogId=@BookNoChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_BookNoChangeLogs 
											WHERE BookNoChangeLogId = @BookNoChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
							
							
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNumber
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET BookNo=@NewBookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1
					
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN							
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_BookNoChangeLogs
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNo = @GlobalAccountNumber
					AND  BookNoChangeLogId= @BookNoChangeLogId
					
					SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewBookNo);

					UPDATE CUSTOMERS.Tbl_CustomersDetail 
						SET AccountNo=@AccountNo 
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET BookNo=@NewBookNo,IsBookNoChanged=1       
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_BookNoChangeLogs(
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
					/*--Postal Address Details Insertion Based on IsSameAsService
					if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
					old Data will be deleted from the address table for that account no*/
					IF ((SELECT NewIsSameAsService FROM Tbl_BookNoChangeLogs
						WHERE GlobalAccountNo =@GlobalAccountNumber)=1)
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
							(GlobalAccountNumber
							,HouseNo
							,StreetName
							,City
							,Landmark
							,Details
							,AreaCode
							,IsServiceAddress
							,ZipCode
							,IsCommunication
							,IsActive
							,CreatedDate
							,CreatedBy)
							VALUES
							(@GlobalAccountNumber
							,@NewPostal_HouseNo
							,@NewPostal_StreetName
							,@NewPostal_City
							,@NewPostal_Landmark
							,NULL
							,@NewPostal_AreaCode
							,1
							,@NewPostal_ZipCode
							,1
							,1
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime())
				
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,ServiceAddressID=@TempAddressId
									,IsSameAsService=1
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
					ELSE
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewPostal_HouseNo
										,@NewPostal_StreetName
										,@NewPostal_City
										,@NewPostal_Landmark
										,NULL
										,@NewPostal_AreaCode
										,0
										,@NewPostal_ZipCode
										,@IsPostalCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
							
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,IsSameAsService=0
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewService_HouseNo
										,@NewService_StreetName
										,@NewService_City
										,@NewService_Landmark
										,NULL
										,@NewService_AreaCode
										,1
										,@NewService_ZipCode
										,@IsServiceCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
										
							SET	@TempServiceId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET ServiceAddressID=@TempServiceId
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
			END
			
				SELECT 1 As IsSuccess  
				FOR XML PATH('ChangeCustomerTypeBE'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_BookNoChangeLogs 
																WHERE BookNoChangeLogId = @BookNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_BookNoChangeLogs 
						WHERE BookNoChangeLogId = @BookNoChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_BookNoChangeLogs
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNumber  
			AND BookNoChangeLogId = @BookNoChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[UPS_BulkUploadCustomerPayments]    Script Date: 04/27/2015 11:41:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UPS_BulkUploadCustomerPayments]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Tbl_TestJobForCustomerPayments(JobName,RunDateTime)
	SELECT 'CustomerPayments',GETDATE()
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBookNoChangeLogsToApprove]    Script Date: 04/27/2015 11:41:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerBookNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.BookNoChangeLogId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldPostal_HouseNo,T.OldPostal_StreetName,
			T.OldPostal_Landmark,T.OldPostal_City,T.OldPostal_AreaCode,T.OldPostal_ZipCode) AS OldPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewPostal_HouseNo,T.NewPostal_StreetName,
		    T.NewPostal_Landmark,T.NewPostal_City,T.NewPostal_AreaCode,T.NewPostal_ZipCode) AS NewPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldService_HouseNo,T.OldService_StreetName,
			 T.OldService_Landmark,T.OldService_City,T.OldService_AreaCode,T.OldService_ZipCode) AS OldServiceAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewService_HouseNo,T.NewService_StreetName,
		   T.NewService_Landmark,T.NewService_City,T.NewService_AreaCode,T.NewService_ZipCode) AS NewServiceAddress
 		 ,('BU : '+OldBN.BusinessUnitName +' ( '+OldBN.BUCode+ ' ) </br> SU : '+
		  OldBN.ServiceUnitName +' ( '+OldBN.SUCode+ ' ) </br> SC : '+
		  OldBN.ServiceCenterName +' ( '+OldBN.SCCode+ ' ) </br> Cycle : '+
		  OldBN.CycleName +' ( '+OldBN.CycleCode+ ' ) </br> Book : '+
		  OldBN.ID +' ( '+OldBN.BookCode+ ' )' ) AS OldBookNo
		  
		,('BU : '+NewBN.BusinessUnitName +' ( '+NewBN.BUCode+ ' ) </br> SU : '+
		  NewBN.ServiceUnitName +' ( '+NewBN.SUCode+ ' ) </br> SC : '+
		  NewBN.ServiceCenterName +' ( '+NewBN.SCCode+ ' ) </br> Cycle : '+
		  NewBN.CycleName +' ( '+NewBN.CycleCode+ ' ) </br> Book : '+
		  NewBN.ID +' ( '+NewBN.BookCode+ ' )' ) AS NewBookNo

		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_BookNoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN UDV_BookNumberDetails OldBN ON OldBN.BookNo = T.OldBookNo
	INNER JOIN UDV_BookNumberDetails NewBN ON NewBN.BookNo = T.NewBookNo
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
END
--------------------------------------------------------------------------------------------------
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 04/27/2015 11:43:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for shown in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
  
  SELECT
	(  
		SELECT TOP 10
			 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br> Cycle : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br /> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br /> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br /> Cycle : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br /> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
 --    SELECT
	--(  
	--	SELECT TOP 10
	--		 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
	--		,'' AS BU_ID
	--		,'' AS SU_ID
	--		,'' AS ServiceCenterId
	--		,'' AS CycleId
	--	    ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
	--	    ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo
	--		,BookChange.Remarks
	--		,BookChange.CreatedBy
	--		,CustomerView.ClassName
	--		,CustomerView.OldAccountNo
	--		,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
	--		,ApproveSttus.ApprovalStatus  
	--		,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
	--		,COUNT(0) OVER() AS TotalChanges  
	--	FROM Tbl_BookNoChangeLogs  BookChange  
	--	INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.GlobalAccountNo = CustomerView.GlobalAccountNumber  
	--	AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	--	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
	--	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
	--	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
	--	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
	--	INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
	--	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId 
	--	INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
	--	ORDER BY  BookChange.CreatedDate DESC
	--	FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
    
END  
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 04/27/2015 11:43:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
		SELECT
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
		   ,'' AS BU_ID
			,'' AS SU_ID
			,'' AS ServiceCenterId
			,'' AS CycleId
		   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  
		   ,BookChange.Remarks  
		   ,BookChange.CreatedBy
		   ,CustomerView.ClassName
		   ,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		   ,CustomerView.OldAccountNo
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder    
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder   
		 FROM Tbl_BookNoChangeLogs  BookChange  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.GlobalAccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId    
		 ORDER BY BookChange.CreatedDate DESC
		--SELECT 
		--	 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
		--	--,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br>'+
		--	--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br>'+
		--	--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br>'+
		--	--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br>'+
		--	--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
		--	--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) </br>'+
		--	--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) </br>'+
		--	--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) </br>'+
		--	--  BD.CycleName +' ( '+BD.CycleCode+ ' ) </br>'+
		--	--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
		--	,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		--   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

		--	,BookChange.Remarks
		--	,BookChange.CreatedBy
		--	,MTC.ClassName
		--	,CD.OldAccountNo
		--	,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		--	,ApproveSttus.ApprovalStatus  
		--	,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name
		--	,BD.BusinessUnitName
		--	,BD.ServiceUnitName
		--	,BD.ServiceCenterName
		--	,BD.CycleName
		--	,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
		--	,BD.BookSortOrder
		--	,BD.CustomerSortOrder
		--FROM Tbl_BookNoChangeLogs  BookChange  
		--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
		--AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		--INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		--INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		--INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		--INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		--INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		--INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerBookNoChangeApproval]    Script Date: 04/27/2015 11:43:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerBookNoChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @BookNoChangeLogId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ApprovalStatusId INT

	SELECT  
		 @BookNoChangeLogId = C.value('(BookNoChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@OldBU_ID VARCHAR(20),
		@NewBU_ID VARCHAR(20),
		@OldSU_ID VARCHAR(20),
		@NewSU_ID VARCHAR(20),
		@OldSC_ID VARCHAR(20),	
		@NewSC_ID VARCHAR(20),
		@OldCycleId VARCHAR(20),
		@NewCycleId VARCHAR(20),	
		@OldBookNo VARCHAR(20),
		@NewBookNo VARCHAR(20),
		@TempAddressId INT,
		@TempServiceId INT,
		@AccountNo VARCHAR(50)	
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNo
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		,@OldBU_ID					 = OldBU_ID 
		,@NewBU_ID					 = NewBU_ID 
		,@OldSU_ID					 = OldSU_ID 
		,@NewSU_ID					 = NewSU_ID 
		,@OldSC_ID					 = OldSC_ID 
		,@NewSC_ID					 = NewSC_ID 
		,@OldCycleId				 = OldCycleId
		,@NewCycleId				 = NewCycleId
		,@OldBookNo					 = OldBookNo
		,@NewBookNo					 = NewBookNo
		FROM Tbl_BookNoChangeLogs
		WHERE BookNoChangeLogId=@BookNoChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_BookNoChangeLogs 
											WHERE BookNoChangeLogId = @BookNoChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
							
							
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNumber
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET BookNo=@NewBookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1
					
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN							
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_BookNoChangeLogs
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
					WHERE GlobalAccountNo = @GlobalAccountNumber
					AND  BookNoChangeLogId= @BookNoChangeLogId
					
					SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewBookNo);

					UPDATE CUSTOMERS.Tbl_CustomersDetail 
						SET AccountNo=@AccountNo 
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET BookNo=@NewBookNo,IsBookNoChanged=1       
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_BookNoChangeLogs(
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
					/*--Postal Address Details Insertion Based on IsSameAsService
					if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
					old Data will be deleted from the address table for that account no*/
					IF ((SELECT NewIsSameAsService FROM Tbl_BookNoChangeLogs
						WHERE GlobalAccountNo =@GlobalAccountNumber)=1)
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
							(GlobalAccountNumber
							,HouseNo
							,StreetName
							,City
							,Landmark
							,Details
							,AreaCode
							,IsServiceAddress
							,ZipCode
							,IsCommunication
							,IsActive
							,CreatedDate
							,CreatedBy)
							VALUES
							(@GlobalAccountNumber
							,@NewPostal_HouseNo
							,@NewPostal_StreetName
							,@NewPostal_City
							,@NewPostal_Landmark
							,NULL
							,@NewPostal_AreaCode
							,1
							,@NewPostal_ZipCode
							,1
							,1
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime())
				
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,ServiceAddressID=@TempAddressId
									,IsSameAsService=1
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
					ELSE
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewPostal_HouseNo
										,@NewPostal_StreetName
										,@NewPostal_City
										,@NewPostal_Landmark
										,NULL
										,@NewPostal_AreaCode
										,0
										,@NewPostal_ZipCode
										,@IsPostalCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
							
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,IsSameAsService=0
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewService_HouseNo
										,@NewService_StreetName
										,@NewService_City
										,@NewService_Landmark
										,NULL
										,@NewService_AreaCode
										,1
										,@NewService_ZipCode
										,@IsServiceCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
										
							SET	@TempServiceId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET ServiceAddressID=@TempServiceId
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
			END
			
				SELECT 1 As IsSuccess  
				FOR XML PATH('ChangeCustomerTypeBE'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_BookNoChangeLogs 
																WHERE BookNoChangeLogId = @BookNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_BookNoChangeLogs 
						WHERE BookNoChangeLogId = @BookNoChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_BookNoChangeLogs
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNo = @GlobalAccountNumber  
			AND BookNoChangeLogId = @BookNoChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBookNoChangeLogsToApprove]    Script Date: 04/27/2015 11:43:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBookNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.BookNoChangeLogId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldPostal_HouseNo,T.OldPostal_StreetName,
			T.OldPostal_Landmark,T.OldPostal_City,T.OldPostal_AreaCode,T.OldPostal_ZipCode) AS OldPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewPostal_HouseNo,T.NewPostal_StreetName,
		    T.NewPostal_Landmark,T.NewPostal_City,T.NewPostal_AreaCode,T.NewPostal_ZipCode) AS NewPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldService_HouseNo,T.OldService_StreetName,
			 T.OldService_Landmark,T.OldService_City,T.OldService_AreaCode,T.OldService_ZipCode) AS OldServiceAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewService_HouseNo,T.NewService_StreetName,
		   T.NewService_Landmark,T.NewService_City,T.NewService_AreaCode,T.NewService_ZipCode) AS NewServiceAddress
 		 ,('BU : '+OldBN.BusinessUnitName +' ( '+OldBN.BUCode+ ' ) </br> SU : '+
		  OldBN.ServiceUnitName +' ( '+OldBN.SUCode+ ' ) </br> SC : '+
		  OldBN.ServiceCenterName +' ( '+OldBN.SCCode+ ' ) </br> Cycle : '+
		  OldBN.CycleName +' ( '+OldBN.CycleCode+ ' ) </br> Book : '+
		  OldBN.ID +' ( '+OldBN.BookCode+ ' )' ) AS OldBookNo
		  
		,('BU : '+NewBN.BusinessUnitName +' ( '+NewBN.BUCode+ ' ) </br> SU : '+
		  NewBN.ServiceUnitName +' ( '+NewBN.SUCode+ ' ) </br> SC : '+
		  NewBN.ServiceCenterName +' ( '+NewBN.SCCode+ ' ) </br> Cycle : '+
		  NewBN.CycleName +' ( '+NewBN.CycleCode+ ' ) </br> Book : '+
		  NewBN.ID +' ( '+NewBN.BookCode+ ' )' ) AS NewBookNo

		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_BookNoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN UDV_BookNumberDetails OldBN ON OldBN.BookNo = T.OldBookNo
	INNER JOIN UDV_BookNumberDetails NewBN ON NewBN.BookNo = T.NewBookNo
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/27/2015 11:43:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  NEERAJ KANOJIYA          
-- Create date: 1/APRIL/2015          
-- Description: This Procedure is used to change customer address         
-- Modified BY: Faiz-ID103    
-- Modified Date: 07-Apr-2015    
-- Description: changed the street field value from 200 to 255 characters        
-- Modified BY: Faiz-ID103    
-- Modified Date: 16-Apr-2015    
-- Description: Add the condition for IsBookNoChanged  
-- Modified BY: Bhimaraju V  
-- Modified Date: 17-Apr-2015  
-- Description: Implemented Logs for Audit Tray   
-- Modified BY: Faiz-ID103
-- Modified Date: 23-Apr-2015  
-- Description: Added the functionality of AccountNo update on book change.
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]          
(          
 @XmlDoc xml             
)          
AS          
BEGIN        
 DECLARE  @TotalAddress INT
	     ,@IsSameAsServie BIT
	     ,@GlobalAccountNo   VARCHAR(50)
		 ,@NewPostalLandMark  VARCHAR(200)=NULL
		 ,@NewPostalStreet   VARCHAR(255)
		 ,@NewPostalCity   VARCHAR(200)
		 ,@NewPostalHouseNo   VARCHAR(200)
		 ,@NewPostalZipCode   VARCHAR(50)
		 ,@NewServiceLandMark  VARCHAR(200)=NULL
		 ,@NewServiceStreet   VARCHAR(255)
		 ,@NewServiceCity   VARCHAR(200)
		 ,@NewServiceHouseNo  VARCHAR(200)
		 ,@NewServiceZipCode  VARCHAR(50)
		 ,@NewPostalAreaCode  VARCHAR(50)
		 ,@NewServiceAreaCode  VARCHAR(50)
		 ,@NewPostalAddressID  INT
		 ,@NewServiceAddressID  INT
		 ,@ModifiedBy    VARCHAR(50)
		 ,@Details     VARCHAR(MAX)
		 ,@RowsEffected    INT
		 ,@AddressID INT
		 ,@StatusText VARCHAR(50)
		 ,@IsCommunicationPostal BIT
		 ,@IsCommunicationService BIT
		 ,@ApprovalStatusId INT=2
		 ,@PostalAddressID INT
		 ,@ServiceAddressID INT
		 ,@BookNo VARCHAR(50)
		 ,@IsFinalApproval BIT
		 ,@FunctionId INT

	SELECT
		@GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')          
		--,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')              
		 ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')              
		 ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')              
		 ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')             
		 ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')            
		 --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')              
		 ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')              
		 ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')              
		 ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')             
		 ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')          
		 ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')          
		 ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')          
		 ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')          
		 ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')          
		 ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')          
		 ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')          
		 ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')      
		 ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')          
		 ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')      
		 ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')      
		 ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		 ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		 ,@FunctionId = C.value('(FunctionId)[1]','INT')
	  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)          


IF((SELECT COUNT(0)FROM Tbl_BookNoChangeLogs 
				WHERE GlobalAccountNo = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
	BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
	END
ELSE
	BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@BookNoChangeLogId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			DECLARE @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
			IF(@ExistingBookNo!=@BookNo)  
					BEGIN
					
						DECLARE @OldBU_ID VARCHAR(20)
								,@NewBU_ID VARCHAR(20)
								,@OldSU_ID VARCHAR(20)
								,@NewSU_ID VARCHAR(20)
								,@OldSC_ID VARCHAR(20)	
								,@NewSC_ID VARCHAR(20)
								,@OldCycleId VARCHAR(20)
								,@NewCycleId VARCHAR(20)
								,@OldBookNo VARCHAR(20)
								,@NewBookNo VARCHAR(20)
						  --Getting Old And New Book Details
						SELECT @OldBU_ID=BU_ID,@OldSU_ID=SU_ID,@OldSC_ID=ServiceCenterId,
									@OldCycleId=CycleId,@OldBookNo=BookNo
						   FROM UDV_BookNumberDetails
						  WHERE BookNo=@ExistingBookNo
						  
						SELECT @NewBU_ID=BU_ID,@NewSU_ID=SU_ID,@NewSC_ID=ServiceCenterId,
									@NewCycleId=CycleId,@NewBookNo=BookNo
						   FROM UDV_BookNumberDetails
						  WHERE BookNo=@BookNo
						  --Getting Old And New Book Details		  

						IF (@IsSameAsServie =0)
							BEGIN
								INSERT INTO Tbl_BookNoChangeLogs(GlobalAccountNo
																	,OldPostal_HouseNo
																	,OldPostal_StreetName
																	,OldPostal_City
																	,OldPostal_Landmark
																	,OldPostal_AreaCode
																	,OldPostal_ZipCode
																	,OldService_HouseNo
																	,OldService_StreetName
																	,OldService_City
																	,OldService_Landmark
																	,OldService_AreaCode
																	,OldService_ZipCode
																	,NewPostal_HouseNo
																	,NewPostal_StreetName
																	,NewPostal_City
																	,NewPostal_Landmark
																	,NewPostal_AreaCode
																	,NewPostal_ZipCode
																	,NewService_HouseNo
																	,NewService_StreetName
																	,NewService_City
																	,NewService_Landmark
																	,NewService_AreaCode
																	,NewService_ZipCode
																	,ApprovalStatusId
																	,Remarks
																	,CreatedBy
																	,CreatedDate
																	,PresentApprovalRole
																	,NextApprovalRole
																	,OldPostalAddressID
																	,NewPostalAddressID
																	,OldServiceAddressID
																	,NewServiceAddressID
																	,OldIsSameAsService
																	,NewIsSameAsService
																	,IsPostalCommunication
																	,IsServiceComunication
																	,OldBU_ID
																	,OldSU_ID
																	,OldSC_ID
																	,OldCycleId
																	,OldBookNo
																	,NewBU_ID
																	,NewSU_ID
																	,NewSC_ID
																	,NewCycleId
																	,NewBookNo
																)
															SELECT   @GlobalAccountNo
																	,Postal_HouseNo
																	,Postal_StreetName
																	,Postal_City
																	,Postal_Landmark
																	,Postal_AreaCode
																	,Postal_ZipCode
																	,Service_HouseNo
																	,Service_StreetName
																	,Service_City
																	,Service_Landmark
																	,Service_AreaCode
																	,Service_ZipCode
																	,@NewPostalHouseNo
																	,@NewPostalStreet
																	,@NewPostalCity
																	,NULL
																	,@NewPostalAreaCode
																	,@NewPostalZipCode
																	,@NewServiceHouseNo
																	,@NewServiceStreet
																	,@NewServiceCity
																	,NULL
																	,@NewServiceAreaCode
																	,@NewServiceZipCode
																	,@ApprovalStatusId
																	,@Details
																	,@ModifiedBy
																	,dbo.fn_GetCurrentDateTime()
																	,@PresentRoleId
																	,@NextRoleId
																	,PostalAddressID
																	,@NewPostalAddressID
																	,ServiceAddressID
																	,@NewServiceAddressID
																	,IsSameAsService
																	,@IsSameAsServie
																	,@IsCommunicationPostal
																	,@IsCommunicationService
																	,@OldBU_ID
																	,@OldSU_ID
																	,@OldSC_ID
																	,@OldCycleId
																	,@OldBookNo
																	,@NewBU_ID
																	,@NewSU_ID
																	,@NewSC_ID
																	,@NewCycleId
																	,@NewBookNo
															FROM CUSTOMERS.Tbl_CustomerSDetail
															WHERE GlobalAccountNumber=@GlobalAccountNo
								SET @BookNoChangeLogId = SCOPE_IDENTITY()							
							END
						ELSE
							BEGIN
								INSERT INTO Tbl_BookNoChangeLogs( 
															 GlobalAccountNo
															,OldPostal_HouseNo
															,OldPostal_StreetName
															,OldPostal_City
															,OldPostal_Landmark
															,OldPostal_AreaCode
															,OldPostal_ZipCode
															,OldService_HouseNo
															,OldService_StreetName
															,OldService_City
															,OldService_Landmark
															,OldService_AreaCode
															,OldService_ZipCode
															,NewPostal_HouseNo
															,NewPostal_StreetName
															,NewPostal_City
															,NewPostal_Landmark
															,NewPostal_AreaCode
															,NewPostal_ZipCode
															,NewService_HouseNo
															,NewService_StreetName
															,NewService_City
															,NewService_Landmark
															,NewService_AreaCode
															,NewService_ZipCode
															,ApprovalStatusId
															,Remarks
															,CreatedBy
															,CreatedDate
															,PresentApprovalRole
															,NextApprovalRole
															,OldPostalAddressID
															,NewPostalAddressID
															,OldServiceAddressID
															,NewServiceAddressID
															,OldIsSameAsService
															,NewIsSameAsService
															,IsPostalCommunication
															,IsServiceComunication
															,OldBU_ID
															,OldSU_ID
															,OldSC_ID
															,OldCycleId
															,OldBookNo
															,NewBU_ID
															,NewSU_ID
															,NewSC_ID
															,NewCycleId
															,NewBookNo
															)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
														,@PresentRoleId
														,@NextRoleId
														,PostalAddressID
														,@NewPostalAddressID
														,ServiceAddressID
														,@NewServiceAddressID
														,IsSameAsService
														,@IsSameAsServie
														,@IsCommunicationPostal
														,@IsCommunicationService
														,@OldBU_ID
														,@OldSU_ID
														,@OldSC_ID
														,@OldCycleId
														,@OldBookNo
														,@NewBU_ID
														,@NewSU_ID
														,@NewSC_ID
														,@NewCycleId
														,@NewBookNo
									FROM CUSTOMERS.Tbl_CustomerSDetail
									WHERE GlobalAccountNumber=@GlobalAccountNo
								SET @BookNoChangeLogId = SCOPE_IDENTITY()							
							END
						
						IF(@IsFinalApproval = 1)
						BEGIN
							BEGIN TRY  
								  BEGIN TRAN   
									--UPDATE POSTAL ADDRESS  
								 SET @StatusText='Total address count.'     
								 SET @TotalAddress=(SELECT COUNT(0)   
									  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
									  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
										)
								 --=====================================================================================          
								 --Customer has only one addres and wants to update the address. //CONDITION 1//  
								 --=====================================================================================   
								 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
									BEGIN  
								  SET @StatusText='CONDITION 1'
								 
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewPostalLandMark  
									,Service_StreetName=@NewPostalStreet  
									,Service_City=@NewPostalCity  
									,Service_HouseNo=@NewPostalHouseNo  
									,Service_ZipCode=@NewPostalZipCode  
									,Service_AreaCode=@NewPostalAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@PostalAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=1  
								  WHERE GlobalAccountNumber=@GlobalAccountNo  
								 END      
								 --=====================================================================================  
								 --Customer has one addres and wants to add new address. //CONDITION 2//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
									BEGIN  
								  SET @StatusText='CONDITION 2'
																				 
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
												HouseNo  
												,LandMark  
												,StreetName  
												,City  
												,AreaCode  
												,ZipCode  
												,ModifedBy  
												,ModifiedDate  
												,IsCommunication  
												,IsServiceAddress  
												,IsActive  
												,GlobalAccountNumber  
												)  
											   VALUES (@NewServiceHouseNo         
												,@NewServiceLandMark         
												,@NewServiceStreet          
												,@NewServiceCity        
												,@NewServiceAreaCode        
												,@NewServiceZipCode          
												,@ModifiedBy        
												,dbo.fn_GetCurrentDateTime()    
												,@IsCommunicationService           
												,1  
												,1  
												,@GlobalAccountNo  
												)   
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewServiceLandMark  
									,Service_StreetName=@NewServiceStreet  
									,Service_City=@NewServiceCity  
									,Service_HouseNo=@NewServiceHouseNo  
									,Service_ZipCode=@NewServiceZipCode  
									,Service_AreaCode=@NewServiceAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@ServiceAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo                 
								 END     
								   
								 --=====================================================================================  
								 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
									BEGIN  
								  SET @StatusText='CONDITION 3'  
								     
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									IsActive=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewPostalLandMark  
									,Service_StreetName=@NewPostalStreet  
									,Service_City=@NewPostalCity  
									,Service_HouseNo=@NewPostalHouseNo  
									,Service_ZipCode=@NewPostalZipCode  
									,Service_AreaCode=@NewPostalAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@PostalAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=1  
								  WHERE GlobalAccountNumber=@GlobalAccountNo  
								 END    
								 --=====================================================================================  
								 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
									BEGIN  
								  SET @StatusText='CONDITION 4'
								       
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
									  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
									  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
									  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
									  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
									  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationService  
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewServiceLandMark  
									,Service_StreetName=@NewServiceStreet  
									,Service_City=@NewServiceCity  
									,Service_HouseNo=@NewServiceHouseNo  
									,Service_ZipCode=@NewServiceZipCode  
									,Service_AreaCode=@NewServiceAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@ServiceAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo     
								 END    
								 COMMIT TRAN  
								END TRY  
								BEGIN CATCH  
								 ROLLBACK TRAN  
								 SET @RowsEffected=0  
								END CATCH
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
								SET BookNo=@BookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1 
							
							DECLARE @AccountNo VARCHAR(50)	
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNo
							
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
									BookNoChangeLogId,
									GlobalAccountNo,
									OldPostal_HouseNo,
									OldPostal_StreetName,
									OldPostal_City,
									OldPostal_Landmark,
									OldPostal_AreaCode,
									OldPostal_ZipCode,
									NewPostal_HouseNo,
									NewPostal_StreetName,
									NewPostal_City,
									NewPostal_Landmark,
									NewPostal_AreaCode,
									NewPostal_ZipCode,
									OldService_HouseNo,
									OldService_StreetName,
									OldService_City,
									OldService_Landmark,
									OldService_AreaCode,
									OldService_ZipCode,
									NewService_HouseNo,
									NewService_StreetName,
									NewService_City,
									NewService_Landmark,
									NewService_AreaCode,
									NewService_ZipCode,
									ApprovalStatusId,
									Remarks,
									CreatedBy,
									CreatedDate,
									ModifiedBy,
									ModifiedDate,
									PresentApprovalRole,
									NextApprovalRole,
									IsPostalCommunication,
									IsServiceComunication,
									OldBU_ID,
									OldSU_ID,
									OldSC_ID,
									OldCycleId,
									OldBookNo,
									NewBU_ID,
									NewSU_ID,
									NewSC_ID,
									NewCycleId,
									NewBookNo
									)  
							SELECT  
									BookNoChangeLogId,
									GlobalAccountNo,
									OldPostal_HouseNo,
									OldPostal_StreetName,
									OldPostal_City,
									OldPostal_Landmark,
									OldPostal_AreaCode,
									OldPostal_ZipCode,
									NewPostal_HouseNo,
									NewPostal_StreetName,
									NewPostal_City,
									NewPostal_Landmark,
									NewPostal_AreaCode,
									NewPostal_ZipCode,
									OldService_HouseNo,
									OldService_StreetName,
									OldService_City,
									OldService_Landmark,
									OldService_AreaCode,
									OldService_ZipCode,
									NewService_HouseNo,
									NewService_StreetName,
									NewService_City,
									NewService_Landmark,
									NewService_AreaCode,
									NewService_ZipCode,
									ApprovalStatusId,
									Remarks,
									CreatedBy,
									CreatedDate,
									ModifiedBy,
									ModifiedDate,
									PresentApprovalRole,
									NextApprovalRole,
									IsPostalCommunication,
									IsServiceComunication,
									OldBU_ID,
									OldSU_ID,
									OldSC_ID,
									OldCycleId,
									OldBookNo,
									NewBU_ID,
									NewSU_ID,
									NewSC_ID,
									NewCycleId,
									NewBookNo
							FROM Tbl_BookNoChangeLogs WHERE BookNoChangeLogId = @BookNoChangeLogId
							
							END
					
						SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
					END
			ELSE
				BEGIN
					SELECT 0 As IsSuccess FOR XML PATH('CustomerRegistrationBE')
				END
	END
END
 

--BEGIN TRY      
--  BEGIN TRAN       
--    --UPDATE POSTAL ADDRESS      
-- SET @StatusText='Total address count.'         
-- SET @TotalAddress=(SELECT COUNT(0)       
--      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails       
--      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1      
--        )      
              
-- --=====================================================================================              
-- --Update book number of customer.      
-- --=====================================================================================       
--Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
--IF(@ExistingBookNo!=@BookNo)  
--BEGIN
-- --DECLARE @OldBookNo VARCHAR(50)  
-- --SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)  
  
--  INSERT INTO Tbl_BookNoChangeLogs( AccountNo  
--          --,CustomerUniqueNo  
--          ,OldBookNo  
--          ,NewBookNo  
--          ,CreatedBy  
--          ,CreatedDate  
--          ,ApproveStatusId  
--          ,Remarks)  
--  VALUES(@GlobalAccountNo
--		--,@OldBookNo
--		,@ExistingBookNo
--		,@BookNo
--		,@ModifiedBy
--		,dbo.fn_GetCurrentDateTime()
--		,@ApprovalStatusId,@Details)   
     
--  UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
--  SET BookNo=@BookNo,IsBookNoChanged=1       
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1  
  
--	  --===========Start of Updating Account Number after book change.=================(Faiz-ID103)
--	  DECLARE @AccountNo Varchar(50)
--		,@BU_ID VARCHAR(50)  
--		,@SU_ID VARCHAR(50)  
--		,@ServiceCenterId VARCHAR(50)  
--	  SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId   
--	  FROM Tbl_BookNumbers BN   
--	  JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
--	  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
--	  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
--	  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
--	   WHERE BookNo=@BookNo  
	     
--	  SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo);
	  
--	  UPDATE CUSTOMERS.Tbl_CustomersDetail 
--	  SET AccountNo=@AccountNo 
--	  WHERE GlobalAccountNumber = @GlobalAccountNo
--	  --===========END of Updating Account Number after book change.=================
--END  
  
----================== Logs For Addres Change          
--  IF (@IsSameAsServie =0)  
--  BEGIN    
--   INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber  
--              ,OldPostal_HouseNo  
--              ,OldPostal_StreetName  
--              ,OldPostal_City  
--              ,OldPostal_Landmark  
--              ,OldPostal_AreaCode  
--              ,OldPostal_ZipCode  
--              ,OldService_HouseNo  
--              ,OldService_StreetName  
--              ,OldService_City  
--              ,OldService_Landmark  
--              ,OldService_AreaCode  
--              ,OldService_ZipCode  
--              ,NewPostal_HouseNo  
--                 ,NewPostal_StreetName  
--                 ,NewPostal_City  
--                 ,NewPostal_Landmark  
--                 ,NewPostal_AreaCode  
--                 ,NewPostal_ZipCode  
--                 ,NewService_HouseNo  
--                 ,NewService_StreetName  
--                 ,NewService_City  
--                 ,NewService_Landmark  
--                 ,NewService_AreaCode  
--                 ,NewService_ZipCode  
--                 ,ApprovalStatusId  
--                 ,Remarks  
--                 ,CreatedBy  
--                 ,CreatedDate  
--             )  
--            SELECT   @GlobalAccountNo  
--              ,Postal_HouseNo  
--              ,Postal_StreetName  
--              ,Postal_City  
--              ,Postal_Landmark  
--              ,Postal_AreaCode  
--              ,Postal_ZipCode  
--              ,Service_HouseNo  
--              ,Service_StreetName  
--              ,Service_City  
--              ,Service_Landmark  
--              ,Service_AreaCode  
--              ,Service_ZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@NewServiceHouseNo  
--              ,@NewServiceStreet  
--              ,@NewServiceCity  
--              ,NULL  
--              ,@NewServiceAreaCode  
--              ,@NewServiceZipCode  
--              ,@ApprovalStatusId  
--              ,@Details  
--              ,@ModifiedBy  
--              ,dbo.fn_GetCurrentDateTime()  
--            FROM CUSTOMERS.Tbl_CustomerSDetail  
--            WHERE GlobalAccountNumber=@GlobalAccountNo  
--  END  
-- ELSE  
--  BEGIN  
--   INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber  
--              ,OldPostal_HouseNo  
--              ,OldPostal_StreetName  
--              ,OldPostal_City  
--              ,OldPostal_Landmark  
--              ,OldPostal_AreaCode  
--              ,OldPostal_ZipCode  
--              ,OldService_HouseNo  
--              ,OldService_StreetName  
--              ,OldService_City  
--              ,OldService_Landmark  
--              ,OldService_AreaCode  
--              ,OldService_ZipCode  
--              ,NewPostal_HouseNo  
--                 ,NewPostal_StreetName  
--                 ,NewPostal_City  
--                 ,NewPostal_Landmark  
--                 ,NewPostal_AreaCode  
--                 ,NewPostal_ZipCode  
--                 ,NewService_HouseNo  
--                 ,NewService_StreetName  
--                 ,NewService_City  
--                 ,NewService_Landmark  
--                 ,NewService_AreaCode  
--                 ,NewService_ZipCode  
--                 ,ApprovalStatusId  
--                 ,Remarks  
--                 ,CreatedBy  
--                 ,CreatedDate  
--             )  
--            SELECT   @GlobalAccountNo  
--              ,Postal_HouseNo  
--              ,Postal_StreetName  
--              ,Postal_City  
--              ,Postal_Landmark  
--              ,Postal_AreaCode  
--              ,Postal_ZipCode  
--              ,Service_HouseNo  
--              ,Service_StreetName  
--              ,Service_City  
--              ,Service_Landmark  
--              ,Service_AreaCode  
--              ,Service_ZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@ApprovalStatusId  
--              ,@Details  
--              ,@ModifiedBy  
--              ,dbo.fn_GetCurrentDateTime()  
--            FROM CUSTOMERS.Tbl_CustomerSDetail  
--            WHERE GlobalAccountNumber=@GlobalAccountNo  
--  END  
    
-- --=====================================================================================              
-- --Customer has only one addres and wants to update the address. //CONDITION 1//      
-- --=====================================================================================          
-- IF(@TotalAddress =1 AND @IsSameAsServie = 1)      
-- BEGIN      
--  SET @StatusText='CONDITION 1'  
    
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewPostalLandMark      
--    ,Service_StreetName=@NewPostalStreet      
--    ,Service_City=@NewPostalCity      
--    ,Service_HouseNo=@NewPostalHouseNo      
--    ,Service_ZipCode=@NewPostalZipCode      
--    ,Service_AreaCode=@NewPostalAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@PostalAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=1      
--  WHERE GlobalAccountNumber=@GlobalAccountNo      
-- END          
-- --=====================================================================================      
-- --Customer has one addres and wants to add new address. //CONDITION 2//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)      
-- BEGIN      
--  SET @StatusText='CONDITION 2'  
   
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(      
--                HouseNo      
--                ,LandMark      
--                ,StreetName      
--                ,City      
--                ,AreaCode      
--                ,ZipCode      
--                ,ModifedBy      
--                ,ModifiedDate      
--                ,IsCommunication      
--                ,IsServiceAddress      
--                ,IsActive      
--                ,GlobalAccountNumber      
--                )      
--               VALUES (@NewServiceHouseNo             
--                ,@NewServiceLandMark             
--                ,@NewServiceStreet              
--                ,@NewServiceCity            
--                ,@NewServiceAreaCode            
--             ,@NewServiceZipCode              
--                ,@ModifiedBy            
--                ,dbo.fn_GetCurrentDateTime()        
--                ,@IsCommunicationService               
--                ,1      
--                ,1      
--                ,@GlobalAccountNo      
--                )       
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewServiceLandMark      
--    ,Service_StreetName=@NewServiceStreet      
--    ,Service_City=@NewServiceCity      
--    ,Service_HouseNo=@NewServiceHouseNo      
--    ,Service_ZipCode=@NewServiceZipCode      
--    ,Service_AreaCode=@NewServiceAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@ServiceAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo                     
-- END         
       
-- --=====================================================================================      
-- --Customer alrady has tow address and wants to in-active service. //CONDITION 3//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)       
-- BEGIN      
--  SET @StatusText='CONDITION 3'  
    
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    IsActive=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewPostalLandMark      
--    ,Service_StreetName=@NewPostalStreet      
--    ,Service_City=@NewPostalCity      
--    ,Service_HouseNo=@NewPostalHouseNo      
--    ,Service_ZipCode=@NewPostalZipCode      
--    ,Service_AreaCode=@NewPostalAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@PostalAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=1      
--  WHERE GlobalAccountNumber=@GlobalAccountNo      
-- END        
-- --=====================================================================================      
-- --Customer alrady has tow address and wants to update both address. //CONDITION 4//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)      
-- BEGIN      
--  SET @StatusText='CONDITION 4'  
  
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END            
--      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END            
--      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END            
--      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END            
--      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END              
--      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END               
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationService      
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewServiceLandMark      
-- ,Service_StreetName=@NewServiceStreet      
--    ,Service_City=@NewServiceCity      
--    ,Service_HouseNo=@NewServiceHouseNo      
--    ,Service_ZipCode=@NewServiceZipCode      
--    ,Service_AreaCode=@NewServiceAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@ServiceAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo         
-- END        
-- COMMIT TRAN      
--END TRY      
--BEGIN CATCH      
-- ROLLBACK TRAN      
-- SET @RowsEffected=0      
--END CATCH      
-- SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')               
--END 

GO

/****** Object:  StoredProcedure [dbo].[UPS_BulkUploadCustomerPayments]    Script Date: 04/27/2015 11:43:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[UPS_BulkUploadCustomerPayments]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Tbl_TestJobForCustomerPayments(JobName,RunDateTime)
	SELECT 'CustomerPayments',GETDATE()
	
END

GO


