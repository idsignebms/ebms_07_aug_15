
-----------------------------------------------
--Readings

CREATE TABLE Tbl_ReadingUploadBatches		
(
RUploadBatchId INT PRIMARY KEY IDENTITY(1,1)
,BatchNo VARCHAR(12)
,BatchDate DATETIME	
,TotalCustomers INT
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,LastModifyDate DATETIME
,LastModifyBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,Notes VARCHAR(200)
,IsClosedBatch BIT DEFAULT 0

)
GO

-----------------------------------------------

create table Tbl_ReadingUploadFiles		
(
RUploadFileId INT PRIMARY KEY IDENTITY(1,1)
,RUploadBatchId INT REFERENCES Tbl_ReadingUploadBatches(RUploadBatchId)
,FilePath VARCHAR(200)
,TotalCustomers INT
,TotalSucessTransactions INT
,TotalFailureTransactions INT
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,CreatedDate DATETIME 
,IsTrasactionsCompleted BIT DEFAULT 0

)

GO

-----------------------------------------------

CREATE TABLE Tbl_ReadingTransactions		
(
ReadingTransactionId INT primary key identity(1,1)
,PUploadFileId INT references Tbl_ReadingUploadFiles(RUploadFileId)
,SNO VARCHAR(50)
,AccountNo VARCHAR(100)
,CurrentReading VARCHAR(100)
,ReadingDate VARCHAR(100)
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)

)

GO

------------------------------------------------

CREATE TABLE Tbl_ReadingSucessTransactions		
(
ReadingSucessTransactionID INT primary key identity(1,1)
,PUploadFileId INT references Tbl_ReadingUploadFiles(RUploadFileId)
,SNO VARCHAR(50)
,AccountNo VARCHAR(100)
,CurrentReading VARCHAR(100)
,ReadingDate VARCHAR(100)
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,Comments VARCHAR(100)

)

GO 

------------------------------------------------

create table Tbl_ReadingFailureTransactions		
(
ReadingFailureransactionID INT primary key identity(1,1)
,PUploadFileId INT references Tbl_ReadingUploadFiles(RUploadFileId)
,SNO VARCHAR(50)
,AccountNo VARCHAR(100) 
,CurrentReading VARCHAR(100)
,ReadingDate VARCHAR(100)
,CreatedDate DATETIME
,CreatedBy VARCHAR(50)  NULL REFERENCES Tbl_UserLoginDetails(UserId)
,Comments VARCHAR(100)

)

GO

--Payments

CREATE TABLE Tbl_PaymentUploadBatches		
(
PUploadBatchId INT PRIMARY KEY IDENTITY(1,1)
,BatchNo VARCHAR(12)
,BatchDate DATETIME
,TotalCustomers INT
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,LastModifyDate DATETIME
,LastModifyBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,Notes VARCHAR(200)
,IsClosedBatch BIT DEFAULT 0

)
GO
---------------------------------


 CREATE TABLE Tbl_PaymentUploadFiles		
(

PUploadFileId INT PRIMARY KEY IDENTITY(1,1)
,PUploadBatchId INT REFERENCES  Tbl_PaymentUploadBatches(PUploadBatchId)
,FilePath VARCHAR(200)
,TotalCustomers INT
,TotalSucessTransactions INT
,TotalFailureTransactions INT
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,CreatedDate DATETIME
,IsTrasactionsCompleted BIT DEFAULT 0

)
GO
 
--------------------------------------

create table Tbl_PaymentTransactions		
(
PaymentTransactionId INT PRIMARY KEY IDENTITY(1,1)
,PUploadFileId INT references Tbl_PaymentUploadFiles(PUploadFileId)
,SNO VARCHAR(50)
,AccountNo VARCHAR(100)
,AmountPaid VARCHAR(100)
,ReceiptNo VARCHAR(100)
,ReceivedDate VARCHAR(100)
,PaymentMode VARCHAR(100)
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)

)

GO
---------------------------------------

create table Tbl_PaymentSucessTransactions		
(
PaymentSucessTransactionID INT PRIMARY KEY IDENTITY(1,1)
,PUploadFileId INT REFERENCES Tbl_PaymentUploadFiles(PUploadFileId)
,SNO VARCHAR(50)
,AccountNo VARCHAR(100)
,AmountPaid VARCHAR(100)
,ReceiptNo VARCHAR(100)
,ReceivedDate VARCHAR(100)
,PaymentMode VARCHAR(100)
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,Comments VARCHAR(100)

)
GO

----------------------------------------------
CREATE TABLE Tbl_PaymentFailureTransactions		
(
PaymentFailureransactionID INT PRIMARY KEY IDENTITY(1,1)	
,PUploadFileId INT references Tbl_PaymentUploadFiles(PUploadFileId)
,SNO VARCHAR(50)
,AccountNo VARCHAR(100)
,AmountPaid VARCHAR(100)
,ReceiptNo VARCHAR(100)
,ReceivedDate VARCHAR(100)
,PaymentMode VARCHAR(100)
,CreatedDate DATETIME
,CreatedBy VARCHAR(50) NULL REFERENCES Tbl_UserLoginDetails(UserId)
,Comments VARCHAR(100)
)

GO
