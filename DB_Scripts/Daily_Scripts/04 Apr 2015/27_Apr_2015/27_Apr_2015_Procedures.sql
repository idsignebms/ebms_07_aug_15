
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentBatchProcesFailures]    Script Date: 04/27/2015 21:27:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Batch Proces Failures Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPaymentBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@PUploadFileId INT 
			,@PUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@PUploadFileId = C.value('(PUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	SELECT @PUploadBatchId = PUploadBatchId FROM Tbl_PaymentUploadFiles WHERE PUploadFileId = @PUploadFileId

	SELECT 
			PUF.TotalFailureTransactions,
			PUB.BatchNo,
			--PUB.BatchDate,
			CONVERT(VARCHAR(24),PUB.BatchDate,106) AS BatchDate,
			PUB.Notes
	FROM Tbl_PaymentUploadFiles PUF
	JOIN Tbl_PaymentUploadBatches PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId
	WHERE PUB.PUploadBatchId =2-- @PUploadBatchId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			AmountPaid,
			ReceiptNo,
			ReceivedDate,
			PaymentMode,
			Comments
		FROM Tbl_PaymentFailureTransactions
		WHERE PUploadFileId = @PUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertPaymentBatchUploadDetails]    Script Date: 04/27/2015 21:27:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:27-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for Payment uploads
-- =============================================            
CREATE PROCEDURE [dbo].[USP_InsertPaymentBatchUploadDetails]
(
	@BatchNo int,            
	@BatchName varchar(50) ,            
	@BatchDate varchar(50) ,            
	@CreatedBy varchar(50),             
	@Description VARCHAR(MAX), 
	@BU_ID VARCHAR(50),       
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempPayments TblPaymentUploads_TempType READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @PUploadBatchId INT, @PUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo 
						AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate) AND BU_ID=@BU_ID))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           
					INSERT INTO Tbl_BatchDetails(            
						BatchNo,             
						BatchDate,           
						CreatedBy,            
						CreatedDate,           
						BatchStatus,  
						BU_ID,   
						[Description],
						BatchTotal  
					)            
						VALUES(            
						@BatchNo,     
						@BatchDate,           
						@CreatedBy,                
						dbo.fn_GetCurrentDateTime(), 
						1,            
						@BU_ID,
						CASE @Description WHEN '' THEN NULL ELSE @Description END, 
						0   
					)   
					
					INSERT INTO Tbl_PaymentUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @PUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_PaymentUploadFiles
					(
						 PUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @PUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_PaymentTransactions
					(
						 PUploadFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNo
						,ReceivedDate
						,PaymentMode
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @PUploadFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNO
						,ReceivedDate
						,PaymentMode
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempPayments
					         
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ClosePaymentUploadBatch]    Script Date: 04/27/2015 21:27:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to Close Payment Upload Batch record
-- =============================================
CREATE PROCEDURE [dbo].[USP_ClosePaymentUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
		
	SELECT       
		 @PUploadBatchId = C.value('(PUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	UPDATE Tbl_PaymentUploadBatches 
	SET IsClosedBatch = 1
	WHERE PUploadBatchId = @PUploadBatchId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('PaymentsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentUploadBatches]    Script Date: 04/27/2015 21:27:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Upload Batches Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPaymentUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY PUB.IsClosedBatch ASC , PUB.CreatedDate DESC ) AS RowNumber,  
				PUB.PUploadBatchId,
				PUB.BatchNo,
				PUB.BatchDate,
				--CONVERT(VARCHAR(24),PUB.CreatedDate,106) AS BatchDate,
				PUB.TotalCustomers,
				PUB.Notes,
				PUF.TotalSucessTransactions,
				PUF.TotalFailureTransactions,
				PUF.PUploadFileId
		FROM Tbl_PaymentUploadBatches PUB
		JOIN Tbl_PaymentUploadFiles PUF ON PUF.PUploadBatchId = PUB.PUploadBatchId
		WHERE PUB.IsClosedBatch = 0 AND (PUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('PaymentsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('PaymentsBatchBEInfoByXml')     
	
END

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 04/27/2015 21:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
		
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0)) as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	  
	INNER JOIN Tbl_Cycles Cycles On	Cycles.ServiceCenterId=@ServiceCenterID and Cycles.CycleId=BN.CycleId
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT
	(
		SELECT ClassID
			,SUM(case when isnull(ReadCodeID,0)=2 then 1 else 0 end) as TotalReadCustomers
			,SUM(case when isnull(ReadCodeID,0)=1 then 1 else 0 end) as EstimatedCustomers
			,TC.ClassName
			,CC.CategoryName
			,CC.ClusterCategoryId 
			,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
			,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
			,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
			,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
			,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
			,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
			,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
			,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
			,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
			,Cycles.CycleId 
			,Cycles.CycleName
		FROM Tbl_MTariffClasses(NOLOCK)	TC
		INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
		INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN   MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
		INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
		--CROSS JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC
		LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
				and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
				And ES.BillingMonth = @Month -- Need To Modify	 
				AND ES.BillingYear = @Year	 -- Need To Modify
		Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
		ORDER BY Cycles.CycleId,CC.ClusterCategoryId
		FOR XML PATH('EstimationDetails'),TYPE
	)
	,
	(
		SELECT	
			 @Capacity as CapacityInKVA
			,@SUPPMConsumption as SUPPMConsumption
			,@SUCreditConsumption as SUCreditConsumption
		FOR XML PATH('EstimatedUsageDetails'),TYPE 
	)
	FOR XML PATH(''),ROOT('EstimationInfoByXml')
	
	DROP TABLE #ReadCustomersList
	
END
