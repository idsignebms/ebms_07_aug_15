GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Bulk Payments','../Billing/PaymentBulkUpload.aspx',6,20,1,1,180)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,180,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,180,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Bulk Payments Status','../Billing/PaymentBatchProcessStatus.aspx',6,21,1,1,181)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,181,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,181,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
GO
sp_RENAME 'Tbl_ReadingFailureTransactions.PUploadFileId' , 'RUploadFileId', 'COLUMN'
GO
sp_RENAME 'Tbl_ReadingTransactions.PUploadFileId' , 'RUploadFileId', 'COLUMN'
GO
sp_RENAME 'Tbl_ReadingSucessTransactions.PUploadFileId' , 'RUploadFileId', 'COLUMN'
GO
CREATE TYPE TblMeterReadingsBulkUpload AS TABLE
(
	 SNO VARCHAR(10)
	,Account_Number VARCHAR(50)
	,Current_Reading VARCHAR(50)
	,Read_Date VARCHAR(50)
)
GO

ALTER TABLE Tbl_PaymentSucessTransactions
ALTER COLUMN Comments VARCHAR(MAX)
GO
ALTER TABLE Tbl_PaymentFailureTransactions
ALTER COLUMN Comments VARCHAR(MAX)
GO