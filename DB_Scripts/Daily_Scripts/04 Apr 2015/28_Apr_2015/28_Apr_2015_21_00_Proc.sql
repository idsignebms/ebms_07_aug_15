GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 04/28/2015 20:56:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND PaidAmount = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNO) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
											AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,(SELECT TOP(1) BatchNo FROM Tbl_PaymentUploadBatches PUB WHERE PUB.PUploadBatchId = PUF.PUploadBatchId) AS BatchNo
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (TempDetails.AccountNo = CD.GlobalAccountNumber OR TempDetails.AccountNo = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (CD.GlobalAccountNumber = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = CPD.BookNo
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions)
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			PRINT 'BEGIN TRAN'
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					PRINT 'Invalid Account Number'
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					PRINT 'Customer in Another BU'
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
			
			PRINT 'insert the Failure Transactions'
			-- TO insert the Failure Transactions 	
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #PaymentValidation
			WHERE IsValid = 0
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table
			DELETE FROM Tbl_PaymentTransactions 
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
			
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
			
			
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			PRINT 'insert valid'
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchNo
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
							SELECT @PresentAccountNo = AccountNo,
								@PaidAmount = PaidAmount,
								@ReceiptNo = ReceiptNo,
								@DocumentPath = FilePath,
								@RecievedDate = PaymentDate,
								@BatchNo = BatchNo,
								@PaymentModeId = PaymentModeId,
								@CreatedBy = CreatedBy
							FROM #ValidPayments 
							WHERE RowNumber = @SNo
							
							INSERT INTO Tbl_CustomerPayments
							(
								 AccountNo
								,ReceiptNo
								,PaymentMode
								,DocumentPath
								,RecievedDate
								,PaymentType
								,PaidAmount
								,BatchNo
								,ActivestatusId
								,CreatedBy
								,CreatedDate
							)
							VALUES
							(
								 @PresentAccountNo
								,@ReceiptNo
								,@PaymentModeId
								,@DocumentPath
								,@RecievedDate
								,2 -- For Bulk Upload
								,@PaidAmount
								,@BatchNo
								,1
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							)					
							
							SET @CustomerPaymentId = SCOPE_IDENTITY()
							
							DELETE FROM @PaidBills
							
							SET @IsBillExist = 0
							
							IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
								BEGIN
									SET @IsBillExist = 1
								END
							
							IF(@IsBillExist = 1)
								BEGIN
									
									INSERT INTO @PaidBills
									(
										 CustomerBillId
										,BillNo
										,PaidAmount
										,BillStatus
									)
									SELECT 
										 CustomerBillId
										,BillNo
										,PaidAmount
										,BillStatus
									FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
									
									INSERT INTO Tbl_CustomerBillPayments
									(
										 CustomerPaymentId
										,PaidAmount
										,BillNo
										,CustomerBillId
										,CreatedBy
										,CreatedDate
									)
									SELECT 
										 @CustomerPaymentId
										,PaidAmount
										,BillNo
										,CustomerBillId
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime()
									FROM @PaidBills
									
									UPDATE CB
									SET CB.ActiveStatusId = PB.BillStatus
										,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
									FROM Tbl_CustomerBills CB
									INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
								END
							ELSE
								BEGIN
									
									INSERT INTO Tbl_CustomerBillPayments
									(
										 CustomerPaymentId
										,PaidAmount
										,BillNo
										,CustomerBillId
										,CreatedBy
										,CreatedDate
									)
									VALUES( 
										 @CustomerPaymentId
										,@PaidAmount
										,NULL
										,NULL
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime())
								END
								
							UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
							SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
							WHERE GlobalAccountNumber = @PresentAccountNo
							
							SET @SNo = @SNo + 1
							
							SET @PresentAccountNo = NULL
							SET @PaidAmount = NULL
							SET @ReceiptNo = NULL
							SET @DocumentPath = NULL
							SET @RecievedDate = NULL
							SET @BatchNo = NULL
							SET @PaymentModeId = NULL
							SET @CreatedBy = NULL
								
				END
				PRINT 'insert valid trans'
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,AccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid.'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadingBatchUploadDetails]    Script Date: 04/28/2015 20:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Bhimaraju V
-- Modified Date:28-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for MerterReading uploads
-- =============================================            
CREATE PROCEDURE [dbo].[USP_InsertMeterReadingBatchUploadDetails]
(
	@BatchNo INT,            
	@BatchName VARCHAR(50) ,            
	@BatchDate VARCHAR(50) ,            
	@CreatedBy VARCHAR(50),             
	@Description VARCHAR(MAX),
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TblMeterReadingsBulkUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @RUploadBatchId INT, @RUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_ReadingUploadBatches WHERE BatchNo=@BatchNo 
						AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           

					INSERT INTO Tbl_ReadingUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @RUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingUploadFiles
					(
						 RUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @RUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @RUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingTransactions
					(
						 RUploadFileId
						,SNO
						,AccountNo
						,CurrentReading
						,ReadingDate
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @RUploadFileId
						,SNO
						,Account_Number
						,Current_Reading
						,Read_Date
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertReadingUploadTransactionDetails]    Script Date: 04/28/2015 20:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Faiz
-- Modified Date: 28-Apr-2015    
-- Description: The purpose of this procedure is to get file upload details No for Reading uploads
-- =============================================            
CREATE PROCEDURE [dbo].[USP_InsertReadingUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TblMeterReadingsBulkUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	IF EXISTS(SELECT 0 FROM Tbl_ReadingUploadBatches WHERE BatchNo = @BatchNo)
		BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_ReadingUploadFiles
					(
						 RUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingTransactions
					(
						 RUploadFileId
						,SNO
						,AccountNo
						,CurrentReading
						,ReadingDate
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,Account_Number
						,Current_Reading
						,Read_Date
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
		END
	ELSE
		BEGIN
			SET @IsSuccess = 0
		END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingBatchProcesFailures]    Script Date: 04/28/2015 20:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Batch Proces Failures Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetReadingBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
			--,@RUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	--SELECT @RUploadBatchId = RUploadBatchId FROM Tbl_ReadingUploadFiles WHERE RUploadFileId = @RUploadFileId

	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_ReadingUploadFiles RUF
	JOIN Tbl_ReadingUploadBatches RUB ON RUB.RUploadBatchId = RUF.RUploadBatchId
	WHERE RUF.RUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			CurrentReading,
			ReadingDate,
			Comments
		FROM Tbl_ReadingFailureTransactions
		WHERE RUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_CloseReadingUploadBatch]    Script Date: 04/28/2015 20:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 28-Apr-2015
-- Description:	This pupose of this procedure is to Close Reading Upload Batch record
-- =============================================
CREATE PROCEDURE [dbo].[USP_CloseReadingUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @RUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
		
	SELECT       
		 @RUploadBatchId = C.value('(RUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	UPDATE Tbl_ReadingUploadBatches 
	SET IsClosedBatch = 1
	WHERE RUploadBatchId = @RUploadBatchId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ReadingsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingUploadBatches]    Script Date: 04/28/2015 20:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 28-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Upload Batches Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetReadingUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY RUB.IsClosedBatch ASC , RUB.CreatedDate DESC ) AS RowNumber,  
				RUB.RUploadBatchId,
				RUB.BatchNo,
				CONVERT(VARCHAR(20),RUB.BatchDate,106) AS StrBatchDate,
				--CONVERT(VARCHAR(24),RUB.CreatedDate,106) AS BatchDate,
				RUF.TotalCustomers,
				RUB.Notes,
				RUF.TotalSucessTransactions,
				RUF.TotalFailureTransactions,
				RUF.RUploadFileId
		FROM Tbl_ReadingUploadBatches RUB
		JOIN Tbl_ReadingUploadFiles RUF ON RUF.RUploadBatchId = RUB.RUploadBatchId
		WHERE RUB.IsClosedBatch = 0 AND (RUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('ReadingsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('ReadingsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertPaymentUploadTransactionDetails]    Script Date: 04/28/2015 20:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:28-04-2015    
-- Description: The purpose of this procedure is to get file upload details No for Payment uploads
-- =============================================            
CREATE PROCEDURE [dbo].[USP_InsertPaymentUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempPayments TblPaymentUploads_TempType READONLY 
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	IF EXISTS(SELECT 0 FROM Tbl_BatchDetails WHERE BatchNo = @BatchNo)
		BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_PaymentUploadFiles
					(
						 PUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_PaymentTransactions
					(
						 PUploadFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNo
						,ReceivedDate
						,PaymentMode
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNO
						,ReceivedDate
						,PaymentMode
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempPayments
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
		END
	ELSE
		BEGIN
			SET @IsSuccess = 0
		END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPagePermisions]    Script Date: 04/28/2015 20:57:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz-ID103>      
-- Create date: <09-JAN-2015>      
-- Description: <Retriving Master and Child page list for Role Permisions.>      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetPagePermisions] 
(
@XmlDoc XML=null
)
AS      
 BEGIN     
 DECLARE	@AccessLevelsId INT
			,@RoleId INT

 SELECT     
	@AccessLevelsID = C.value('(AccessLevelsID)[1]','INT') 
	,@RoleId = C.value('(RoleId)[1]','INT') 
	FROM @XmlDoc.nodes('AdminBE') AS T(C)    
 
 Select (    
 --List for Master Menu Item    
     
     
   SELECT [MenuId]    
      ,[Name]    
      --,[Path]    
      ,[Menu_Order]    
      ,[ReferenceMenuId]    
      --,[Icon1]    
      --,[Icon2]    
      ,[Page_Order]    
      ,[IsActive]    
      --,[ActiveHeader]    
      --,[Menu_Image]    
   FROM Tbl_Menus WHERE Menu_Order IS NOT NULL AND IsActive= 1 ORDER BY Menu_Order    
   FOR XML PATH('AdminListBE'),TYPE    
    ),    
    (    
 --List for Child Menu Item    
    SELECT [MenuId]    
      ,[Name]    
      --,[Path]    
      ,[Menu_Order]    
      ,(CASE [ReferenceMenuId] WHEN 122 THEN 7 ELSE [ReferenceMenuId] END) AS [ReferenceMenuId]  
      --,[Icon1]    
      --,[Icon2]    
      ,[Page_Order]    
      ,[IsActive]    
      --,[ActiveHeader]    
      --,[Menu_Image]    
      ,ISNULL(Tbl_Menus.AccessLevelID,0) AS AccessId  
   FROM Tbl_Menus   
  LEFT JOIN Tbl_MMenuAccessLevels ON Tbl_Menus.AccessLevelID = Tbl_MMenuAccessLevels.AccessLevelID  
   WHERE Menu_Order IS NULL  
   AND IsActive= 1 ORDER BY ReferenceMenuId    
   FOR XML PATH('AdminListBE_1'),TYPE    
   )    
   FOR XML PATH(''),ROOT('AdminBEInfoByXml')          
 END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 04/28/2015 20:57:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND PaidAmount = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNO) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
											AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,(SELECT TOP(1) BatchNo FROM Tbl_PaymentUploadBatches PUB WHERE PUB.PUploadBatchId = PUF.PUploadBatchId) AS BatchNo
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (TempDetails.AccountNo = CD.GlobalAccountNumber OR TempDetails.AccountNo = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (CD.GlobalAccountNumber = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = CPD.BookNo
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions)
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			PRINT 'BEGIN TRAN'
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					PRINT 'Invalid Account Number'
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					PRINT 'Customer in Another BU'
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
			
			PRINT 'insert the Failure Transactions'
			-- TO insert the Failure Transactions 	
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #PaymentValidation
			WHERE IsValid = 0
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table
			DELETE FROM Tbl_PaymentTransactions 
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
			
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
			
			
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			PRINT 'insert valid'
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchNo
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
							SELECT @PresentAccountNo = AccountNo,
								@PaidAmount = PaidAmount,
								@ReceiptNo = ReceiptNo,
								@DocumentPath = FilePath,
								@RecievedDate = PaymentDate,
								@BatchNo = BatchNo,
								@PaymentModeId = PaymentModeId,
								@CreatedBy = CreatedBy
							FROM #ValidPayments 
							WHERE RowNumber = @SNo
							
							INSERT INTO Tbl_CustomerPayments
							(
								 AccountNo
								,ReceiptNo
								,PaymentMode
								,DocumentPath
								,RecievedDate
								,PaymentType
								,PaidAmount
								,BatchNo
								,ActivestatusId
								,CreatedBy
								,CreatedDate
							)
							VALUES
							(
								 @PresentAccountNo
								,@ReceiptNo
								,@PaymentModeId
								,@DocumentPath
								,@RecievedDate
								,2 -- For Bulk Upload
								,@PaidAmount
								,@BatchNo
								,1
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							)					
							
							SET @CustomerPaymentId = SCOPE_IDENTITY()
							
							DELETE FROM @PaidBills
							
							SET @IsBillExist = 0
							
							IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
								BEGIN
									SET @IsBillExist = 1
								END
							
							IF(@IsBillExist = 1)
								BEGIN
									
									INSERT INTO @PaidBills
									(
										 CustomerBillId
										,BillNo
										,PaidAmount
										,BillStatus
									)
									SELECT 
										 CustomerBillId
										,BillNo
										,PaidAmount
										,BillStatus
									FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
									
									INSERT INTO Tbl_CustomerBillPayments
									(
										 CustomerPaymentId
										,PaidAmount
										,BillNo
										,CustomerBillId
										,CreatedBy
										,CreatedDate
									)
									SELECT 
										 @CustomerPaymentId
										,PaidAmount
										,BillNo
										,CustomerBillId
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime()
									FROM @PaidBills
									
									UPDATE CB
									SET CB.ActiveStatusId = PB.BillStatus
										,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
									FROM Tbl_CustomerBills CB
									INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
								END
							ELSE
								BEGIN
									
									INSERT INTO Tbl_CustomerBillPayments
									(
										 CustomerPaymentId
										,PaidAmount
										,BillNo
										,CustomerBillId
										,CreatedBy
										,CreatedDate
									)
									VALUES( 
										 @CustomerPaymentId
										,@PaidAmount
										,NULL
										,NULL
										,@CreatedBy
										,dbo.fn_GetCurrentDateTime())
								END
								
							UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
							SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
							WHERE GlobalAccountNumber = @PresentAccountNo
							
							SET @SNo = @SNo + 1
							
							SET @PresentAccountNo = NULL
							SET @PaidAmount = NULL
							SET @ReceiptNo = NULL
							SET @DocumentPath = NULL
							SET @RecievedDate = NULL
							SET @BatchNo = NULL
							SET @PaymentModeId = NULL
							SET @CreatedBy = NULL
								
				END
				PRINT 'insert valid trans'
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,AccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid.'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetRolesList]    Script Date: 04/28/2015 20:57:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 10-01-2015  
-- Description: To Get Roles Details  
 
-- Modified By:  Faiz - ID103  
-- Modified date: 18-Apr-2015
-- Description: Added new Field AccessLevels and LoignUserRoleID
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetRolesList]  
(
@XmlDoc XML=null
)
AS  
BEGIN  
DECLARE @AccessLevelsId INT

 SELECT     
	@AccessLevelsID = C.value('(AccessLevelsID)[1]','INT') 
	FROM @XmlDoc.nodes('AdminBE') AS T(C)    
  
 SELECT (  
  SELECT RoleId,RoleName FROM Tbl_MRoles WHERE IsActive = 1  
  FOR XML PATH('AdminListBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('AdminBEInfoByXml')  
END  
  
---------------------------------------------------------------------------  
GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 04/28/2015 20:57:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=3)
		BEGIN
			IF ((SELECT TOP 1 0 FROM Tbl_LCustomerTariffChangeRequest WHERE ApprovalStatusId NOT IN(2,3))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_BookNoChangeLogs WHERE ApprovalStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_CustomerAddressChangeLog_New WHERE ApprovalStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_CustomerNameChangeLogs WHERE ApproveStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_CustomerActiveStatusChangeLogs WHERE ApproveStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_CustomerTypeChangeLogs WHERE ApprovalStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT TOP 1 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ApprovalStatusId NOT IN(2,3))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsToApprove]    Script Date: 04/28/2015 20:57:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 8 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.NameChangeLogId
		,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber  
		 --,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,dbo.fn_GetCustomerFullName_New(T.OldTitle,T.OldFirstName,T.OldMiddleName,T.OldLastName) AS [OldName]
		 ,dbo.fn_GetCustomerFullName_New(T.NewTitle,T.NewFirstName,T.NewMiddleName,T.NewLastName) AS [NewName]
		 --,OldKnownAs
		 --,NewKnownAs AS KnownAs
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.NextApprovalRole IS NULL OR (T.NextApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 'true'
				ELSE 'false'
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerNameChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
		
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0)) as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	  
	INNER JOIN Tbl_Cycles Cycles On	Cycles.ServiceCenterId=@ServiceCenterID and Cycles.CycleId=BN.CycleId
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT
	(
		SELECT ClassID
			,SUM(case when isnull(ReadCodeID,0)=2 then 1 else 0 end) as TotalReadCustomers
			,SUM(case when isnull(ReadCodeID,0)=1 then 1 else 0 end) as EstimatedCustomers
			,TC.ClassName
			,CC.CategoryName
			,CC.ClusterCategoryId 
			,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
			,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
			,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
			,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
			,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
			,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
			,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
			,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
			,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
			,Cycles.CycleId 
			,Cycles.CycleName
		FROM Tbl_MTariffClasses(NOLOCK)	TC
		INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
		INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN   MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
		INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
		--CROSS JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC
		LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
				and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
				And ES.BillingMonth = @Month -- Need To Modify	 
				AND ES.BillingYear = @Year	 -- Need To Modify
		Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
		ORDER BY Cycles.CycleId,CC.ClusterCategoryId
		FOR XML PATH('EstimationDetails'),TYPE
	)
	,
	(
		SELECT	
			 @Capacity as CapacityInKVA
			,@SUPPMConsumption as SUPPMConsumption
			,@SUCreditConsumption as SUCreditConsumption
		FOR XML PATH('EstimatedUsageDetails'),TYPE 
	)
	FOR XML PATH(''),ROOT('EstimationInfoByXml')
	
	DROP TABLE #ReadCustomersList
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersLog]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersLog]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT 				
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,(CAST(AM.InitialReading AS INT )) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks
			   ,CONVERT(VARCHAR(50),AM.ModifiedDate,107) AS LastTransactionDate  
			   ,AM.CreatedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
			   ,CustomerView.BookSortOrder  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 ORDER BY AM.CreatedDate DESC
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersWithLimit]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,CAST(AM.InitialReading AS INT) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(30),AM.ModifiedDate,107) AS LastTransactionDate   
			   ,ApproveSttus.ApprovalStatus
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName			     
			   ,AM.Remarks  
			   ,AM.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 ORDER BY AM.CreatedDate DESC
		 -- changed by karteek end       
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogs]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
	--SELECT(  
		 SELECT  --CMI.AccountNo
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' ) ') AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' ) ') AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
			   ,CMI.Remarks  
			   ,CMI.CreatedBy
			   ,CustomerView.ClassName  
			   ,CONVERT(VARCHAR(30),CMI.ModifiedDate,107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CMI.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name 
			   ,CustomerView.OldAccountNo   
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end    
END  
-----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 06-10-2014   
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015      
-- Description: The purpose of this procedure is to get limited Meter change request logs    
-- =============================================       
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''    
      
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
    
	-- start By Karteek  
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek   
      
	SELECT(    
		 SELECT TOP 10 			
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' ) ') AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' ) ') AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType    
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType    
			   ,CMI.Remarks
			   ,CMI.CreatedBy
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate     
			   ,CONVERT(VARCHAR(30),CMI.ModifiedDate,107) AS LastTransactionDate     
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate     
			   ,ApproveSttus.ApprovalStatus    
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name    
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI    
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber    
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)    
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID     
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID  
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId  
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId  
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo  
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId  
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end     
		FOR XML PATH('AuditTrayList'),TYPE    
	)    
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
	
END   
---------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsWithLimit]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid   
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo+' - '+CustomerView.GlobalAccountNumber) AS AccountNo  
			   ,CNL.CreatedBy  
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
				--,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName AS OldName  
			   --,CNL.NewFirstName AS [NewName]  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   --,CNL.OldKnownAs AS OldSurName  
			   --,CNL.NewKnownAs AS NewSurName  
			   ,CONVERT(VARCHAR(30),CNL.ModifiedDate,107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus
			   ,COUNT(0) OVER() AS TotalChanges  
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName			   
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId 
		 -- changed by karteek end      
		 ORDER BY CNL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogs]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
		 SELECT  
			   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
			   --,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName  
			   --,CNL.NewFirstName  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   --,CNL.OldKnownAs  
			   --,CNL.NewKnownAs  
			   ,CONVERT(VARCHAR(30),CNL.ModifiedDate,107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CNL.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
			   ,CustomerView.ClassName 
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId   
		 ORDER BY CNL.CreatedDate DESC
END 
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillingDisabledBookNo]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Bhimaraju Vanka      
-- Create date: 24-Sep-2014      
-- Modified By: T.Karthik      
-- Modified Date : 17-10-2014      
-- Description: Purpose is To get List Of Billing Disabled BookNo     
-- Modified By: Faiz-ID103  
-- Modified Date: 10-Apr-2015  
-- Description: Disable date field added        
-- Modified By: Faiz-ID103  
-- Modified Date: 20-Apr-2015  
-- Description: Modified the functionality to display the disable books for Super Admin       
-- Modified By: Faiz-ID103  
-- Modified Date: 28-Apr-2015  
-- Description: Modified the functionality to display BU,SU,SC and Book Group 
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBillingDisabledBookNo]    
(      
 @XmlDoc Xml      
)      
AS      
BEGIN      
      
 DECLARE  @PageNo INT      
   ,@PageSize INT      
   ,@YearId INT      
   ,@MonthId INT      
   ,@BU_ID VARCHAR(50)    
          
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')      
    ,@PageSize = C.value('(PageSize)[1]','INT')      
    ,@YearId=C.value('(YearId)[1]','INT')      
    ,@MonthId=C.value('(MonthId)[1]','INT')      
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')             
  FROM @XmlDoc.nodes('BillingDisabledBooksBe') AS T(C)       
        
  CREATE TABLE #BillingDisabledBookNos(Sno INT PRIMARY KEY IDENTITY(1,1),DisabledBookId INT)      
      
  INSERT INTO #BillingDisabledBookNos(DisabledBookId)      
   SELECT      
    DisabledBookId      
   FROM Tbl_BillingDisabledBooks      
   WHERE IsActive = 1      
   AND (YearId=@YearId OR @YearId=0)      
   AND (MonthId=@MonthId OR @MonthId=0)      
   ORDER BY CreatedDate DESC      
         
  DECLARE @Count INT=0      
  SET @Count=(SELECT COUNT(0) FROM #BillingDisabledBookNos)      
        
  SELECT      
    Sno AS RowNumber      
    ,BDB.BookNo AS BookNo	-- Faiz_ID103
    ,BN.BusinessUnitName	-- Faiz_ID103 -28-Apr-2015
    ,BN.ServiceUnitName		-- Faiz_ID103 -28-Apr-2015
    ,BN.ServiceCenterName	-- Faiz_ID103 -28-Apr-2015
    ,BN.CycleName			-- Faiz_ID103 -28-Apr-2015
    ,BN.ID AS ID    
    ,BDB.DisabledBookId      
    ,ISNULL(Remarks,'--') AS Details      
    ,MonthId      
    ,YearId      
    ,((select dbo.fn_GetMonthName([MonthId]))+'-'+CONVERT(varchar(10), [YearId])) AS MonthYear      
    ,@Count AS TotalRecords    
    ,(CASE IsPartialBill WHEN 1 THEN 'Partial Book' ELSE DisableType END) AS DisableType    
    ,ISNULL((CONVERT(VARCHAR(20),DisableDate,106)),'--') AS DisableDate  
  FROM Tbl_BillingDisabledBooks AS BDB      
  JOIN #BillingDisabledBookNos LBDB ON BDB.DisabledBookId=LBDB.DisabledBookId      
  INNER JOIN UDV_BookNumberDetails BN on BDB.BookNo=BN.BookNo     
 AND (BN.BU_ID = @BU_ID OR @BU_ID='')-- Faiz-ID103-20-Apr-2015
  INNER JOIN TBl_MDisableType D ON D.DisableTypeId = BDB.DisableTypeId    
  --INNER JOIN Tbl_Cycles C ON C.CycleId = BN.CycleId    
  --INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = C.ServiceCenterId    
  --INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID     
  AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
 END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml    
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,BookCode  
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadingBatchUploadDetails]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Bhimaraju V
-- Modified Date:28-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for MerterReading uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertMeterReadingBatchUploadDetails]
(
	@BatchNo INT,            
	@BatchName VARCHAR(50) ,            
	@BatchDate VARCHAR(50) ,            
	@CreatedBy VARCHAR(50),             
	@Description VARCHAR(MAX),
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TblMeterReadingsBulkUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @RUploadBatchId INT, @RUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_ReadingUploadBatches WHERE BatchNo=@BatchNo 
						AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           

					INSERT INTO Tbl_ReadingUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @RUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingUploadFiles
					(
						 RUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @RUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @RUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingTransactions
					(
						 RUploadFileId
						,SNO
						,AccountNo
						,CurrentReading
						,ReadingDate
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @RUploadFileId
						,SNO
						,Account_Number
						,Current_Reading
						,Read_Date
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertReadingUploadTransactionDetails]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Faiz
-- Modified Date: 28-Apr-2015    
-- Description: The purpose of this procedure is to get file upload details No for Reading uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertReadingUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TblMeterReadingsBulkUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	IF EXISTS(SELECT 0 FROM Tbl_ReadingUploadBatches WHERE BatchNo = @BatchNo)
		BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_ReadingUploadFiles
					(
						 RUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingTransactions
					(
						 RUploadFileId
						,SNO
						,AccountNo
						,CurrentReading
						,ReadingDate
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,Account_Number
						,Current_Reading
						,Read_Date
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
		END
	ELSE
		BEGIN
			SET @IsSuccess = 0
		END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingBatchProcesFailures]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetReadingBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
			--,@RUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	--SELECT @RUploadBatchId = RUploadBatchId FROM Tbl_ReadingUploadFiles WHERE RUploadFileId = @RUploadFileId

	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_ReadingUploadFiles RUF
	JOIN Tbl_ReadingUploadBatches RUB ON RUB.RUploadBatchId = RUF.RUploadBatchId
	WHERE RUF.RUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			CurrentReading,
			ReadingDate,
			Comments
		FROM Tbl_ReadingFailureTransactions
		WHERE RUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTariffDetails]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTariffDetails]  
 (  
 @XmlDoc XML=null  
)  
AS  
BEGIN   
 SET NOCOUNT ON;  
 DECLARE  
   @AccountNo VARCHAR(50)   
  ,@CreatedBy VARCHAR(50)  
  ,@Remarks VARCHAR(50)  
  ,@Flag INT  
  ,@ApprovalStatusId INT  
  ,@OldClassID INT  
  ,@NewClassID INT  
  ,@TariffChangeRequestId INT  
  ,@FunctionId INT
  ,@ModifiedBy VARCHAR(50)
  ,@PresentRoleId INT=NULL
  ,@NextRoleId INT=NULL
  ,@CurrentLevel INT

 SELECT   
       @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
      ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
      ,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
      ,@Flag = C.value('(Flag)[1]','INT')  
      ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
      ,@OldClassID = C.value('(OldClassID)[1]','INT')  
      ,@NewClassID = C.value('(NewClassID)[1]','INT')  
      ,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
      ,@FunctionId=C.value('(FunctionId)[1]','INT')
      ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
 IF(@Flag = 1)  -- Approval Permission Required
 BEGIN  
	   IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo=@AccountNo AND ApprovalStatusId=1)  
		BEGIN  -- Pending Request already exists
			SELECT 1 AS ApprovalProcess  
		END  
	   ELSE  -- New Request
	   BEGIN  
	   		 DECLARE @RoleId INT,@IsFinalApproval BIT=0
			 SET @RoleId=(SELECT RoleId FROM Tbl_UserDetails WHERE UserId=@CreatedBy) -- Approving User RoleId

             SELECT 
				@CurrentLevel=[Level] 
				,@IsFinalApproval= (CASE IsFinalApproval
										 WHEN 1 THEN (CASE WHEN UserIds IS NOT NULL 
														  THEN (CASE WHEN EXISTS(SELECT 0 FROM dbo.fn_Split(UserIds,',') WHERE @CreatedBy=com)
																	THEN 1 END)
														  ELSE 1 END)
														 END)
				FROM TBL_FunctionApprovalRole WHERE FunctionId=@FunctionId AND
				RoleId=@RoleId
				
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel-1,1)
			
			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
						  AccountNo  
						 ,PreviousTariffId  
						 ,ChangeRequestedTariffId  
						 ,CreatedBy  
						 ,CreatedDate  
						 ,Remarks  
						 ,ApprovalStatusId
						 ,PresentApprovalRole
						 ,NextApprovalRole
						 ,ModifiedBy
						 ,ModifiedDate)  
					  VALUES(  
						@AccountNo  
						,@OldClassID  
						,@NewClassID  
						,@CreatedBy  
						,dbo.fn_GetCurrentDateTime()  
						,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
						,CASE @IsFinalApproval WHEN 1 THEN 2 ELSE @ApprovalStatusId END
						,@PresentRoleId
						,@NextRoleId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime())  
						
			  IF(@IsFinalApproval=1)
			  BEGIN
					--UPDATE Tbl_CustomerDetails SET       
					--		  TariffId= @NewClassID  
					--		  ,ModifiedBy = @CreatedBy  
					--		  ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
					--		WHERE AccountNo = @AccountNo  

                    	UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
							  TariffClassID= @NewClassID  
							  ,ModifedBy = @CreatedBy  
							  ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
			  END
						
			  SELECT 1 As IsSuccess,@IsFinalApproval AS IsFinalApproval  
			  FOR XML PATH('TariffManagementBE'),TYPE		
	   END  
 END  
 ELSE IF(@Flag = 2)  -- Tariff Approval Page
  BEGIN  
		IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			 DECLARE @CurrentRoleId INT
			 SET @CurrentRoleId=(SELECT RoleId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy) -- Approving User RoleId
			 
			 IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId=@FunctionId) -- If Approval Levels Exists
			  BEGIN
				  SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId=@FunctionId AND
						RoleId=(SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest WHERE TariffChangeRequestId=@TariffChangeRequestId))
						
					SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
					 
					 IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId=@FunctionId AND RoleId=@CurrentRoleId)=1)
					  BEGIN -- If Approval levels are there and If it is final approval then update tariff
					  
						  UPDATE Tbl_LCustomerTariffChangeRequest SET   -- Updating Request with Level Roles & Approval status 
							ModifiedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole=@PresentRoleId
							,NextApprovalRole=@NextRoleId 
							,ApprovalStatusId=@ApprovalStatusId
						 WHERE AccountNo=@AccountNo   
						 --AND ApprovalStatusId=1  
						 AND TariffChangeRequestId=@TariffChangeRequestId  
					 
							-- UPDATE Tbl_CustomerDetails SET       
							--  TariffId= @NewClassID  
							--  ,ModifiedBy = @ModifiedBy  
							--  ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
							--WHERE AccountNo = @AccountNo 
							
                    	UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
							  TariffClassID= @NewClassID  
							  ,ModifedBy = @CreatedBy  
							  ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo  
					  END
					  ELSE -- Not a final approval
					  BEGIN
						  UPDATE Tbl_LCustomerTariffChangeRequest SET   -- Updating Request with Level Roles 
								ModifiedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole=@PresentRoleId
								,NextApprovalRole=@NextRoleId
								,ApprovalStatusId=(CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
							 WHERE AccountNo=@AccountNo   
							 --AND ApprovalStatusId=1  
							 AND TariffChangeRequestId=@TariffChangeRequestId
					  END
			  END
			  ELSE 
			  BEGIN -- No Approval Levels are there
				  UPDATE Tbl_LCustomerTariffChangeRequest SET  
					 ApprovalStatusId=@ApprovalStatusId   
					,ModifiedBy=@ModifiedBy
					,ModifiedDate=dbo.fn_GetCurrentDateTime()
				 WHERE AccountNo=@AccountNo   
				 --AND ApprovalStatusId=1  
				 AND TariffChangeRequestId=@TariffChangeRequestId  
			  
					--UPDATE Tbl_CustomerDetails SET       
					--  TariffId= @NewClassID  
					--  ,ModifiedBy = @ModifiedBy  
					--  ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
					--WHERE AccountNo = @AccountNo  
					
                           UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
							  TariffClassID= @NewClassID  
							  ,ModifedBy = @CreatedBy  
							  ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
			  END
			  			  
			   SELECT 1 As IsSuccess  
			   FOR XML PATH('TariffManagementBE'),TYPE  
		END  
		ELSE  
		BEGIN  -- Request Not Approved
		  IF(@ApprovalStatusId=3) -- Request is Rejected
			 BEGIN
				  IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId=@FunctionId AND @ApprovalStatusId=3)
				  BEGIN -- Approval levels are there and status is rejected
					  SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId=@FunctionId AND
							RoleId=(SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest WHERE TariffChangeRequestId=@TariffChangeRequestId))
							
						SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
				  END
			 END
		 ELSE IF(@ApprovalStatusId=4) -- Request is Hold
			 BEGIN
				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId=@FunctionId)
					SELECT @PresentRoleId=PresentApprovalRole,@NextRoleId=NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest WHERE TariffChangeRequestId=@TariffChangeRequestId
			 END
				
			  -- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
			  UPDATE Tbl_LCustomerTariffChangeRequest SET 
					ApprovalStatusId = @ApprovalStatusId   
					,ModifiedBy = @ModifiedBy  
					,ModifiedDate= dbo.fn_GetCurrentDateTime()  
					,PresentApprovalRole=@PresentRoleId
					,NextApprovalRole=@NextRoleId
					,Remarks=@Remarks
			  WHERE AccountNo=@AccountNo   
			  AND TariffChangeRequestId=@TariffChangeRequestId  
			  
			   SELECT 1 As IsSuccess  
			   FOR XML PATH('TariffManagementBE'),TYPE  
		 END  
  END  
  ELSE  -- Approval Permission Not Required
  BEGIN  
  
	  INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					  AccountNo  
					 ,PreviousTariffId  
					 ,ChangeRequestedTariffId  
					 ,CreatedBy  
					 ,CreatedDate  
					 ,Remarks  
					 ,ApprovalStatusId
					 ,ModifiedBy
					 ,ModifiedDate)  
				  VALUES(  
					@AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,@ApprovalStatusId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  ) 
                
		--UPDATE Tbl_CustomerDetails SET       
		--	  TariffId=@NewClassID  
  --           ,ModifiedBy = @CreatedBy  
  --           ,ModifiedDate= dbo.fn_GetCurrentDateTime()  
  --         WHERE AccountNo = @AccountNo  
      

        UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				TariffClassID= @NewClassID  
				,ModifedBy = @CreatedBy  
				,ModifiedDate= dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
      SELECT 1 As IsSuccess  
      FOR XML PATH('TariffManagementBE'),TYPE  
  END  
   
END  
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)

	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END

			SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

			--SELECT @PresentRoleId = PresentRoleId 
			--		,@NextRoleId = NextRoleId 
			--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,0,1)

			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,@PresentRoleId
				,@NextRoleId
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,@PreviousReading
						,@OldMeterReading
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_CloseReadingUploadBatch]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 28-Apr-2015
-- Description:	This pupose of this procedure is to Close Reading Upload Batch record
-- =============================================
ALTER PROCEDURE [dbo].[USP_CloseReadingUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @RUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
		
	SELECT       
		 @RUploadBatchId = C.value('(RUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	UPDATE Tbl_ReadingUploadBatches 
	SET IsClosedBatch = 1
	WHERE RUploadBatchId = @RUploadBatchId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ReadingsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerActiveStatus]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 07-10-2014      
-- Description: The purpose of this procedure is to Update Customer ActiveStausLog   
   --ModifiedBy : Padmini  
   --Modified Date :05-02-2015  
-- Modified By: Karteek  
-- Modified Date: 03-04-2015   
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerActiveStatus]  
(  
 @XmlDoc xml      
)  
AS  
BEGIN  
 DECLARE     
   @GlobalAccountNumber VARCHAR(50)   
  ,@ModifiedBy VARCHAR(50)  
  ,@ActiveStatusId INT  
  ,@Details VARCHAR(MAX)  
  ,@ApprovalStatusId INT  
  ,@IsFinalApproval BIT = 0  
  ,@FunctionId INT  
  ,@ChangeDate VARCHAR(10)
          
 SELECT       
   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
  ,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')  
  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')    
  ,@FunctionId = C.value('(FunctionId)[1]','INT')    
  ,@ChangeDate = C.value('(ChangeDate)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 IF((SELECT COUNT(0)FROM Tbl_CustomerActiveStatusChangeLogs   
   WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN(1,4)) > 0)  
  BEGIN  
   SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
  END     
 ELSE  
  BEGIN  
   DECLARE @RoleId INT, @CurrentLevel INT  
     ,@PresentRoleId INT, @NextRoleId INT  
     ,@StatusChangeRequestId INT  
     
   SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId  
   SET @CurrentLevel = 0 -- For Stating Level     
     
   IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
    BEGIN  
     SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId  
    END  
       
   SELECT @PresentRoleId = PresentRoleId   
    ,@NextRoleId = NextRoleId   
   FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)  
       
   INSERT INTO Tbl_CustomerActiveStatusChangeLogs(   
     AccountNo  
    ,OldStatus  
    ,NewStatus  
    ,CreatedBy  
    ,CreatedDate  
    ,ChangeDate
    ,ApproveStatusId  
    ,Remarks  
    ,PresentApprovalRole  
    ,NextApprovalRole
    ,ModifiedBy
    ,ModifiedDate)  
   SELECT GlobalAccountNumber  
    ,ActiveStatusId  
    ,@ActiveStatusId  
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
    ,@ChangeDate 
    ,@ApprovalStatusId  
    ,@Details  
    ,@PresentRoleId  
    ,@NextRoleId
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
   FROM [CUSTOMERS].[Tbl_CustomerSDetail]     
   WHERE GlobalAccountNumber = @GlobalAccountNumber    
  
   SET @StatusChangeRequestId = SCOPE_IDENTITY()  
     
   IF(@IsFinalApproval = 1)  
    BEGIN  
       
     UPDATE CUSTOMERS.Tbl_CustomersDetail  
     SET         
       ActiveStatusId = @ActiveStatusId  
      ,ModifedBy = @ModifiedBy    
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
     WHERE GlobalAccountNumber = @GlobalAccountNumber   
       
     INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole
      ,ModifiedBy
      ,ModifiedDate)    
     SELECT    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,@ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole
      ,ModifiedBy
      ,ModifiedDate
     FROM Tbl_CustomerActiveStatusChangeLogs WHERE ActiveStatusChangeLogId = @StatusChangeRequestId  
       
    END  
      
   SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
  
  END   
END  
GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_Re-AssignMeter]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 5-MARCH-2015  
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_Re-AssignMeter]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
		  @MeterNumber VARCHAR(50)    
		  ,@ReadCodeID VARCHAR(50)   
		  ,@GlobalAccountNumber VARCHAR(50)   
		  ,@IsSuccessful BIT =1  
		  ,@ApprovalStatusId INT
		  ,@IsFinalApproval BIT = 0
		  ,@FunctionId INT
		  ,@Reason VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50)
		  ,@InitialReading INT
     
  SELECT    
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
 
 	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			SET @InitialReading=(SELECT ISNULL(ISNULL(PresentReading,0),ISNULL(InitialReading,0)) FROM CUSTOMERS.Tbl_CustomerActiveDetails
									WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,dbo.fn_GetCurrentDateTime()
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN					
				IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
					BEGIN
					
						UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
							 SET MeterNumber=@MeterNumber  
							  ,ReadCodeID=@ReadCodeID  
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						INSERT INTO Tbl_Audit_AssignedMeterLogs(  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks
								,ApprovalStatusId
								,CreatedBy
								,CreatedDate
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate)  
							SELECT  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks  
								,ApprovalStatusId  
								,CreatedBy  
								,CreatedDate  
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
							FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
						
						SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')	
					END
				ELSE
					BEGIN
						SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
					END
				END
		END
END
		
--DECLARE   
--		  @MeterNumber VARCHAR(50)    
--		  ,@ReadCodeID VARCHAR(50)   
--		  ,@GlobalAccountNumber VARCHAR(50)   
--		  ,@IsSuccessful BIT =1  
--		  ,@StatusText VARCHAR(200)
     
--BEGIN  
--BEGIN TRY  
-- BEGIN TRAN  
     
-- SET @StatusText='Deserialization'  
--  SELECT    
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
--		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    
--  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
-- IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
--	 BEGIN 
--		 SET @StatusText='Update Statement'  
		   
--		 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
--		 SET MeterNumber=@MeterNumber  
--		  ,ReadCodeID=@ReadCodeID  
--		 WHERE GlobalAccountNumber = @GlobalAccountNumber  
		   	   
--		 SET @StatusText='Success'
--	 END
-- ELSE 
--	BEGIN
--		SET @StatusText='Meter is in DeActivate state.'
--		SET @IsSuccessful =0  
--	END 
-- COMMIT TRAN   
--END TRY      
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @IsSuccessful =0  
--END CATCH  
--END  
-- SELECT   
--   @IsSuccessful AS IsSuccessful  
--   ,@StatusText AS StatusText  
-- FOR XML PATH('CustomerRegistrationBE'),TYPE  
  
--END  
--------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]
(  
@XmlDoc xml  
)
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT

	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ReadToDirectId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)			
													
			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
											 GlobalAccountNo
											,MeterNo
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											)
			
			SET @ReadToDirectId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE TBL_ReadToDirectCustomerActivity 
						SET IsLatest=0
					WHERE GlobalAccountNo = @GlobalAccountNumber
	
					INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNumber
														,@MeterNumber
														,@CreatedBy
														,DBO.fn_GetCurrentDateTime()
														)
												
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)  
					SELECT  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
		END
END
--AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@StatusText VARCHAR(200)='Initiation'
--		,@IsSuccessful BIT=1
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	SET @StatusText='Deserialization'
	
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	SET @StatusText='Update Statement'
	
--	UPDATE TBL_ReadToDirectCustomerActivity 
--	SET IsLatest=0
--	WHERE GlobalAccountNo = @GlobalAccountNumber
	
--	SET @StatusText='Insert Log Statement'
--	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
--												(
--												GlobalAccountNo
--												,MeterNumber
--												,Remarks
--												,CreatedBy 
--												,CreatedDate
--												,ApprovalStatusId
--												)
--										VALUES
--												(
--												@GlobalAccountNumber
--												,@MeterNumber
--												,@Reason
--												,@CreatedBy
--												,DBO.fn_GetCurrentDateTime()
--												,@ApprovalStatusId
--												)
--	SET @StatusText='Update Statement'
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--		SET ReadCodeID=1, MeterNumber = NULL
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('MastersBE'),TYPE

--END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerName]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 29-09-2014      
-- Modified By: T.Karthik  
-- Modified Date: 27-12-2014  
-- Modified By: M.Padmini  
-- Modified Date: 21-01-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Bhimaraju.V  
-- Modified Date: 04-02-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Karteek
-- Modified Date: 02-04-2015  
-- Description: The purpose i have changed this to inserting changes in Tbl_CustomerNameChangeLogs only
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerName]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @AccountNo VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50)  
		,@NewTitle VARCHAR(10)  
		,@NewFirstName VARCHAR(50)  
		,@NewMiddleName VARCHAR(50)  
		,@NewLastName VARCHAR(50)  
		,@NewKnownAs VARCHAR(150)  
		,@ModifiedBy VARCHAR(50)  
		,@Details VARCHAR(MAX)  
		,@ApprovalStatusId INT 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
    
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@GlobalAccountNumber = C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@NewTitle = C.value('(Title)[1]','VARCHAR(10)')  
		,@NewFirstName = C.value('(FirstName)[1]','VARCHAR(50)')  
		,@NewMiddleName = C.value('(MiddleName)[1]','VARCHAR(50)')  
		,@NewLastName = C.value('(LastName)[1]','VARCHAR(50)')  
		,@NewKnownAs = C.value('(KnownAs)[1]','VARCHAR(150)')  
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
	IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApproveStatusId IN (1,4)) = 0)  
		BEGIN  		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
		
			INSERT INTO Tbl_CustomerNameChangeLogs(  
				 GlobalAccountNumber  
				,AccountNo  
				,OldTitle  
				,OldFirstName  
				,OldMiddleName  
				,OldLastName  
				,OldKnownas  
				,NewTitle  
				,NewFirstName  
				,NewMiddleName  
				,NewLastName  
				,NewKnownas  
				,CreatedBy  
				,CreatedDate
				,ModifedBy  
				,ModifiedDate
				,ApproveStatusId  
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole) 
			SELECT @GlobalAccountNumber  
				,AccountNo  
				,CASE Title WHEN '' THEN NULL ELSE Title END   -- title was missing --Faiz-ID103 
				,CASE FirstName WHEN '' THEN NULL ELSE FirstName END   
				,CASE MiddleName WHEN '' THEN NULL ELSE MiddleName END  
				,CASE LastName WHEN '' THEN NULL ELSE LastName END    
				,CASE Knownas WHEN '' THEN NULL ELSE Knownas END    
				,CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
				,CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
				,CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END   
				,CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END     
				,CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()
				,@ApprovalStatusId  
				,@Details  
				,@PresentRoleId
				,@NextRoleId
			FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			SET @NameChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 Title = (CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END) 
						,FirstName = (CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END)  
						,MiddleName = (CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END)  
						,LastName = (CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END)  
						,KnownAs = (CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END)  
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerNameChangeLogs(  
						 NameChangeLogId
						,GlobalAccountNumber  
						,AccountNo  
						,OldTitle  
						,OldFirstName  
						,OldMiddleName  
						,OldLastName  
						,OldKnownas  
						,NewTitle  
						,NewFirstName  
						,NewMiddleName  
						,NewLastName  
						,NewKnownas  
						,CreatedBy  
						,CreatedDate
						,ModifedBy
						,ModifiedDate
						,ApproveStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 NameChangeLogId
						,GlobalAccountNumber  
						,AccountNo  
						,OldTitle  
						,OldFirstName  
						,OldMiddleName  
						,OldLastName  
						,OldKnownas  
						,NewTitle  
						,NewFirstName  
						,NewMiddleName  
						,NewLastName  
						,NewKnownas  
						,CreatedBy  
						,CreatedDate
						,ModifedBy
						,ModifiedDate
						,ApproveStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_CustomerNameChangeLogs WHERE NameChangeLogId = @NameChangeRequestId
					
				END
  
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
    
END  
  
---------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,ModifiedBy
																,ModifiedDate
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,ModifiedBy
																,ModifiedDate
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@ApprovalStatusId
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,@PresentRoleId
																,@NextRoleId
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()

														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole,
							IsPostalCommunication,
							IsServiceComunication) 
					SELECT  
							AddressChangeLogId,
							GlobalAccountNumber,
							OldPostal_HouseNo,
							OldPostal_StreetName,
							OldPostal_City,
							OldPostal_Landmark,
							OldPostal_AreaCode,
							OldPostal_ZipCode,
							NewPostal_HouseNo,
							NewPostal_StreetName,
							NewPostal_City,
							NewPostal_Landmark,
							NewPostal_AreaCode,
							NewPostal_ZipCode,
							OldService_HouseNo,
							OldService_StreetName,
							OldService_City,
							OldService_Landmark,
							OldService_AreaCode,
							OldService_ZipCode,
							NewService_HouseNo,
							NewService_StreetName,
							NewService_City,
							NewService_Landmark,
							NewService_AreaCode,
							NewService_ZipCode,
							ApprovalStatusId,
							Remarks,
							CreatedBy,
							CreatedDate,
							ModifiedBy,
							ModifiedDate,
							PresentApprovalRole,
							NextApprovalRole,
							IsPostalCommunication,
							IsServiceComunication
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 04/28/2015 20:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  NEERAJ KANOJIYA          
-- Create date: 1/APRIL/2015          
-- Description: This Procedure is used to change customer address         
-- Modified BY: Faiz-ID103    
-- Modified Date: 07-Apr-2015    
-- Description: changed the street field value from 200 to 255 characters        
-- Modified BY: Faiz-ID103    
-- Modified Date: 16-Apr-2015    
-- Description: Add the condition for IsBookNoChanged  
-- Modified BY: Bhimaraju V  
-- Modified Date: 17-Apr-2015  
-- Description: Implemented Logs for Audit Tray   
-- Modified BY: Faiz-ID103
-- Modified Date: 23-Apr-2015  
-- Description: Added the functionality of AccountNo update on book change.
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]          
(          
 @XmlDoc xml             
)          
AS          
BEGIN        
 DECLARE  @TotalAddress INT
	     ,@IsSameAsServie BIT
	     ,@GlobalAccountNo   VARCHAR(50)
		 ,@NewPostalLandMark  VARCHAR(200)=NULL
		 ,@NewPostalStreet   VARCHAR(255)
		 ,@NewPostalCity   VARCHAR(200)
		 ,@NewPostalHouseNo   VARCHAR(200)
		 ,@NewPostalZipCode   VARCHAR(50)
		 ,@NewServiceLandMark  VARCHAR(200)=NULL
		 ,@NewServiceStreet   VARCHAR(255)
		 ,@NewServiceCity   VARCHAR(200)
		 ,@NewServiceHouseNo  VARCHAR(200)
		 ,@NewServiceZipCode  VARCHAR(50)
		 ,@NewPostalAreaCode  VARCHAR(50)
		 ,@NewServiceAreaCode  VARCHAR(50)
		 ,@NewPostalAddressID  INT
		 ,@NewServiceAddressID  INT
		 ,@ModifiedBy    VARCHAR(50)
		 ,@Details     VARCHAR(MAX)
		 ,@RowsEffected    INT
		 ,@AddressID INT
		 ,@StatusText VARCHAR(50)
		 ,@IsCommunicationPostal BIT
		 ,@IsCommunicationService BIT
		 ,@ApprovalStatusId INT=2
		 ,@PostalAddressID INT
		 ,@ServiceAddressID INT
		 ,@BookNo VARCHAR(50)
		 ,@IsFinalApproval BIT
		 ,@FunctionId INT

	SELECT
		@GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')          
		--,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')              
		 ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')              
		 ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')              
		 ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')             
		 ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')            
		 --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')              
		 ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')              
		 ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')              
		 ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')             
		 ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')          
		 ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')          
		 ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')          
		 ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')          
		 ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')          
		 ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')          
		 ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')          
		 ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')      
		 ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')          
		 ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')      
		 ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')      
		 ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		 ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		 ,@FunctionId = C.value('(FunctionId)[1]','INT')
	  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)          


IF((SELECT COUNT(0)FROM Tbl_BookNoChangeLogs 
				WHERE GlobalAccountNo = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
	BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
	END
ELSE
	BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@BookNoChangeLogId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			DECLARE @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
			IF(@ExistingBookNo!=@BookNo)  
					BEGIN
					
						DECLARE @OldBU_ID VARCHAR(20)
								,@NewBU_ID VARCHAR(20)
								,@OldSU_ID VARCHAR(20)
								,@NewSU_ID VARCHAR(20)
								,@OldSC_ID VARCHAR(20)	
								,@NewSC_ID VARCHAR(20)
								,@OldCycleId VARCHAR(20)
								,@NewCycleId VARCHAR(20)
								,@OldBookNo VARCHAR(20)
								,@NewBookNo VARCHAR(20)
						  --Getting Old And New Book Details
						SELECT @OldBU_ID=BU_ID,@OldSU_ID=SU_ID,@OldSC_ID=ServiceCenterId,
									@OldCycleId=CycleId,@OldBookNo=BookNo
						   FROM UDV_BookNumberDetails
						  WHERE BookNo=@ExistingBookNo
						  
						SELECT @NewBU_ID=BU_ID,@NewSU_ID=SU_ID,@NewSC_ID=ServiceCenterId,
									@NewCycleId=CycleId,@NewBookNo=BookNo
						   FROM UDV_BookNumberDetails
						  WHERE BookNo=@BookNo
						  --Getting Old And New Book Details		  

						IF (@IsSameAsServie =0)
							BEGIN
								INSERT INTO Tbl_BookNoChangeLogs(GlobalAccountNo
																	,OldPostal_HouseNo
																	,OldPostal_StreetName
																	,OldPostal_City
																	,OldPostal_Landmark
																	,OldPostal_AreaCode
																	,OldPostal_ZipCode
																	,OldService_HouseNo
																	,OldService_StreetName
																	,OldService_City
																	,OldService_Landmark
																	,OldService_AreaCode
																	,OldService_ZipCode
																	,NewPostal_HouseNo
																	,NewPostal_StreetName
																	,NewPostal_City
																	,NewPostal_Landmark
																	,NewPostal_AreaCode
																	,NewPostal_ZipCode
																	,NewService_HouseNo
																	,NewService_StreetName
																	,NewService_City
																	,NewService_Landmark
																	,NewService_AreaCode
																	,NewService_ZipCode
																	,ApprovalStatusId
																	,Remarks
																	,CreatedBy
																	,CreatedDate
																	,PresentApprovalRole
																	,NextApprovalRole
																	,OldPostalAddressID
																	,NewPostalAddressID
																	,OldServiceAddressID
																	,NewServiceAddressID
																	,OldIsSameAsService
																	,NewIsSameAsService
																	,IsPostalCommunication
																	,IsServiceComunication
																	,OldBU_ID
																	,OldSU_ID
																	,OldSC_ID
																	,OldCycleId
																	,OldBookNo
																	,NewBU_ID
																	,NewSU_ID
																	,NewSC_ID
																	,NewCycleId
																	,NewBookNo
																	,ModifiedBy
																	,ModifiedDate
																)
															SELECT   @GlobalAccountNo
																	,Postal_HouseNo
																	,Postal_StreetName
																	,Postal_City
																	,Postal_Landmark
																	,Postal_AreaCode
																	,Postal_ZipCode
																	,Service_HouseNo
																	,Service_StreetName
																	,Service_City
																	,Service_Landmark
																	,Service_AreaCode
																	,Service_ZipCode
																	,@NewPostalHouseNo
																	,@NewPostalStreet
																	,@NewPostalCity
																	,NULL
																	,@NewPostalAreaCode
																	,@NewPostalZipCode
																	,@NewServiceHouseNo
																	,@NewServiceStreet
																	,@NewServiceCity
																	,NULL
																	,@NewServiceAreaCode
																	,@NewServiceZipCode
																	,@ApprovalStatusId
																	,@Details
																	,@ModifiedBy
																	,dbo.fn_GetCurrentDateTime()
																	,@PresentRoleId
																	,@NextRoleId
																	,PostalAddressID
																	,@NewPostalAddressID
																	,ServiceAddressID
																	,@NewServiceAddressID
																	,IsSameAsService
																	,@IsSameAsServie
																	,@IsCommunicationPostal
																	,@IsCommunicationService
																	,@OldBU_ID
																	,@OldSU_ID
																	,@OldSC_ID
																	,@OldCycleId
																	,@OldBookNo
																	,@NewBU_ID
																	,@NewSU_ID
																	,@NewSC_ID
																	,@NewCycleId
																	,@NewBookNo
																	,@ModifiedBy
																	,dbo.fn_GetCurrentDateTime()
															FROM CUSTOMERS.Tbl_CustomerSDetail
															WHERE GlobalAccountNumber=@GlobalAccountNo
								SET @BookNoChangeLogId = SCOPE_IDENTITY()							
							END
						ELSE
							BEGIN
								INSERT INTO Tbl_BookNoChangeLogs( 
															 GlobalAccountNo
															,OldPostal_HouseNo
															,OldPostal_StreetName
															,OldPostal_City
															,OldPostal_Landmark
															,OldPostal_AreaCode
															,OldPostal_ZipCode
															,OldService_HouseNo
															,OldService_StreetName
															,OldService_City
															,OldService_Landmark
															,OldService_AreaCode
															,OldService_ZipCode
															,NewPostal_HouseNo
															,NewPostal_StreetName
															,NewPostal_City
															,NewPostal_Landmark
															,NewPostal_AreaCode
															,NewPostal_ZipCode
															,NewService_HouseNo
															,NewService_StreetName
															,NewService_City
															,NewService_Landmark
															,NewService_AreaCode
															,NewService_ZipCode
															,ApprovalStatusId
															,Remarks
															,CreatedBy
															,CreatedDate
															,PresentApprovalRole
															,NextApprovalRole
															,OldPostalAddressID
															,NewPostalAddressID
															,OldServiceAddressID
															,NewServiceAddressID
															,OldIsSameAsService
															,NewIsSameAsService
															,IsPostalCommunication
															,IsServiceComunication
															,OldBU_ID
															,OldSU_ID
															,OldSC_ID
															,OldCycleId
															,OldBookNo
															,NewBU_ID
															,NewSU_ID
															,NewSC_ID
															,NewCycleId
															,NewBookNo
															,ModifiedBy
															,ModifiedDate
															)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@ApprovalStatusId
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
														,@PresentRoleId
														,@NextRoleId
														,PostalAddressID
														,@NewPostalAddressID
														,ServiceAddressID
														,@NewServiceAddressID
														,IsSameAsService
														,@IsSameAsServie
														,@IsCommunicationPostal
														,@IsCommunicationService
														,@OldBU_ID
														,@OldSU_ID
														,@OldSC_ID
														,@OldCycleId
														,@OldBookNo
														,@NewBU_ID
														,@NewSU_ID
														,@NewSC_ID
														,@NewCycleId
														,@NewBookNo
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
									FROM CUSTOMERS.Tbl_CustomerSDetail
									WHERE GlobalAccountNumber=@GlobalAccountNo
								SET @BookNoChangeLogId = SCOPE_IDENTITY()							
							END
						
						IF(@IsFinalApproval = 1)
						BEGIN
							BEGIN TRY  
								  BEGIN TRAN   
									--UPDATE POSTAL ADDRESS  
								 SET @StatusText='Total address count.'     
								 SET @TotalAddress=(SELECT COUNT(0)   
									  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
									  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
										)
								 --=====================================================================================          
								 --Customer has only one addres and wants to update the address. //CONDITION 1//  
								 --=====================================================================================   
								 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
									BEGIN  
								  SET @StatusText='CONDITION 1'
								 
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewPostalLandMark  
									,Service_StreetName=@NewPostalStreet  
									,Service_City=@NewPostalCity  
									,Service_HouseNo=@NewPostalHouseNo  
									,Service_ZipCode=@NewPostalZipCode  
									,Service_AreaCode=@NewPostalAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@PostalAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=1  
								  WHERE GlobalAccountNumber=@GlobalAccountNo  
								 END      
								 --=====================================================================================  
								 --Customer has one addres and wants to add new address. //CONDITION 2//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
									BEGIN  
								  SET @StatusText='CONDITION 2'
																				 
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
												HouseNo  
												,LandMark  
												,StreetName  
												,City  
												,AreaCode  
												,ZipCode  
												,ModifedBy  
												,ModifiedDate  
												,IsCommunication  
												,IsServiceAddress  
												,IsActive  
												,GlobalAccountNumber  
												)  
											   VALUES (@NewServiceHouseNo         
												,@NewServiceLandMark         
												,@NewServiceStreet          
												,@NewServiceCity        
												,@NewServiceAreaCode        
												,@NewServiceZipCode          
												,@ModifiedBy        
												,dbo.fn_GetCurrentDateTime()    
												,@IsCommunicationService           
												,1  
												,1  
												,@GlobalAccountNo  
												)   
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewServiceLandMark  
									,Service_StreetName=@NewServiceStreet  
									,Service_City=@NewServiceCity  
									,Service_HouseNo=@NewServiceHouseNo  
									,Service_ZipCode=@NewServiceZipCode  
									,Service_AreaCode=@NewServiceAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@ServiceAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo                 
								 END     
								   
								 --=====================================================================================  
								 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
									BEGIN  
								  SET @StatusText='CONDITION 3'  
								     
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									IsActive=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewPostalLandMark  
									,Service_StreetName=@NewPostalStreet  
									,Service_City=@NewPostalCity  
									,Service_HouseNo=@NewPostalHouseNo  
									,Service_ZipCode=@NewPostalZipCode  
									,Service_AreaCode=@NewPostalAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@PostalAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=1  
								  WHERE GlobalAccountNumber=@GlobalAccountNo  
								 END    
								 --=====================================================================================  
								 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
									BEGIN  
								  SET @StatusText='CONDITION 4'
								       
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
									  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
									  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
									  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
									  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
									  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationService  
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewServiceLandMark  
									,Service_StreetName=@NewServiceStreet  
									,Service_City=@NewServiceCity  
									,Service_HouseNo=@NewServiceHouseNo  
									,Service_ZipCode=@NewServiceZipCode  
									,Service_AreaCode=@NewServiceAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@ServiceAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo     
								 END    
								 COMMIT TRAN  
								END TRY  
								BEGIN CATCH  
								 ROLLBACK TRAN  
								 SET @RowsEffected=0  
								END CATCH
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
								SET BookNo=@BookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1 
							
							DECLARE @AccountNo VARCHAR(50)	
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNo
							
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
									BookNoChangeLogId,
									GlobalAccountNo,
									OldPostal_HouseNo,
									OldPostal_StreetName,
									OldPostal_City,
									OldPostal_Landmark,
									OldPostal_AreaCode,
									OldPostal_ZipCode,
									NewPostal_HouseNo,
									NewPostal_StreetName,
									NewPostal_City,
									NewPostal_Landmark,
									NewPostal_AreaCode,
									NewPostal_ZipCode,
									OldService_HouseNo,
									OldService_StreetName,
									OldService_City,
									OldService_Landmark,
									OldService_AreaCode,
									OldService_ZipCode,
									NewService_HouseNo,
									NewService_StreetName,
									NewService_City,
									NewService_Landmark,
									NewService_AreaCode,
									NewService_ZipCode,
									ApprovalStatusId,
									Remarks,
									CreatedBy,
									CreatedDate,
									ModifiedBy,
									ModifiedDate,
									PresentApprovalRole,
									NextApprovalRole,
									IsPostalCommunication,
									IsServiceComunication,
									OldBU_ID,
									OldSU_ID,
									OldSC_ID,
									OldCycleId,
									OldBookNo,
									NewBU_ID,
									NewSU_ID,
									NewSC_ID,
									NewCycleId,
									NewBookNo
									)  
							SELECT  
									BookNoChangeLogId,
									GlobalAccountNo,
									OldPostal_HouseNo,
									OldPostal_StreetName,
									OldPostal_City,
									OldPostal_Landmark,
									OldPostal_AreaCode,
									OldPostal_ZipCode,
									NewPostal_HouseNo,
									NewPostal_StreetName,
									NewPostal_City,
									NewPostal_Landmark,
									NewPostal_AreaCode,
									NewPostal_ZipCode,
									OldService_HouseNo,
									OldService_StreetName,
									OldService_City,
									OldService_Landmark,
									OldService_AreaCode,
									OldService_ZipCode,
									NewService_HouseNo,
									NewService_StreetName,
									NewService_City,
									NewService_Landmark,
									NewService_AreaCode,
									NewService_ZipCode,
									ApprovalStatusId,
									Remarks,
									CreatedBy,
									CreatedDate,
									ModifiedBy,
									ModifiedDate,
									PresentApprovalRole,
									NextApprovalRole,
									IsPostalCommunication,
									IsServiceComunication,
									OldBU_ID,
									OldSU_ID,
									OldSC_ID,
									OldCycleId,
									OldBookNo,
									NewBU_ID,
									NewSU_ID,
									NewSC_ID,
									NewCycleId,
									NewBookNo
							FROM Tbl_BookNoChangeLogs WHERE BookNoChangeLogId = @BookNoChangeLogId
							
							END
					
						SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
					END
			ELSE
				BEGIN
					SELECT 0 As IsSuccess FOR XML PATH('CustomerRegistrationBE')
				END
	END
END
 

--BEGIN TRY      
--  BEGIN TRAN       
--    --UPDATE POSTAL ADDRESS      
-- SET @StatusText='Total address count.'         
-- SET @TotalAddress=(SELECT COUNT(0)       
--      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails       
--      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1      
--        )      
              
-- --=====================================================================================              
-- --Update book number of customer.      
-- --=====================================================================================       
--Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
--IF(@ExistingBookNo!=@BookNo)  
--BEGIN
-- --DECLARE @OldBookNo VARCHAR(50)  
-- --SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)  
  
--  INSERT INTO Tbl_BookNoChangeLogs( AccountNo  
--          --,CustomerUniqueNo  
--          ,OldBookNo  
--          ,NewBookNo  
--          ,CreatedBy  
--          ,CreatedDate  
--          ,ApproveStatusId  
--          ,Remarks)  
--  VALUES(@GlobalAccountNo
--		--,@OldBookNo
--		,@ExistingBookNo
--		,@BookNo
--		,@ModifiedBy
--		,dbo.fn_GetCurrentDateTime()
--		,@ApprovalStatusId,@Details)   
     
--  UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
--  SET BookNo=@BookNo,IsBookNoChanged=1       
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1  
  
--	  --===========Start of Updating Account Number after book change.=================(Faiz-ID103)
--	  DECLARE @AccountNo Varchar(50)
--		,@BU_ID VARCHAR(50)  
--		,@SU_ID VARCHAR(50)  
--		,@ServiceCenterId VARCHAR(50)  
--	  SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId   
--	  FROM Tbl_BookNumbers BN   
--	  JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
--	  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
--	  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
--	  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
--	   WHERE BookNo=@BookNo  
	     
--	  SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo);
	  
--	  UPDATE CUSTOMERS.Tbl_CustomersDetail 
--	  SET AccountNo=@AccountNo 
--	  WHERE GlobalAccountNumber = @GlobalAccountNo
--	  --===========END of Updating Account Number after book change.=================
--END  
  
----================== Logs For Addres Change          
--  IF (@IsSameAsServie =0)  
--  BEGIN    
--   INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber  
--              ,OldPostal_HouseNo  
--              ,OldPostal_StreetName  
--              ,OldPostal_City  
--              ,OldPostal_Landmark  
--              ,OldPostal_AreaCode  
--              ,OldPostal_ZipCode  
--              ,OldService_HouseNo  
--              ,OldService_StreetName  
--              ,OldService_City  
--              ,OldService_Landmark  
--              ,OldService_AreaCode  
--              ,OldService_ZipCode  
--              ,NewPostal_HouseNo  
--                 ,NewPostal_StreetName  
--                 ,NewPostal_City  
--                 ,NewPostal_Landmark  
--                 ,NewPostal_AreaCode  
--                 ,NewPostal_ZipCode  
--                 ,NewService_HouseNo  
--                 ,NewService_StreetName  
--                 ,NewService_City  
--                 ,NewService_Landmark  
--                 ,NewService_AreaCode  
--                 ,NewService_ZipCode  
--                 ,ApprovalStatusId  
--                 ,Remarks  
--                 ,CreatedBy  
--                 ,CreatedDate  
--             )  
--            SELECT   @GlobalAccountNo  
--              ,Postal_HouseNo  
--              ,Postal_StreetName  
--              ,Postal_City  
--              ,Postal_Landmark  
--              ,Postal_AreaCode  
--              ,Postal_ZipCode  
--              ,Service_HouseNo  
--              ,Service_StreetName  
--              ,Service_City  
--              ,Service_Landmark  
--              ,Service_AreaCode  
--              ,Service_ZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@NewServiceHouseNo  
--              ,@NewServiceStreet  
--              ,@NewServiceCity  
--              ,NULL  
--              ,@NewServiceAreaCode  
--              ,@NewServiceZipCode  
--              ,@ApprovalStatusId  
--              ,@Details  
--              ,@ModifiedBy  
--              ,dbo.fn_GetCurrentDateTime()  
--            FROM CUSTOMERS.Tbl_CustomerSDetail  
--            WHERE GlobalAccountNumber=@GlobalAccountNo  
--  END  
-- ELSE  
--  BEGIN  
--   INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber  
--              ,OldPostal_HouseNo  
--              ,OldPostal_StreetName  
--              ,OldPostal_City  
--              ,OldPostal_Landmark  
--              ,OldPostal_AreaCode  
--              ,OldPostal_ZipCode  
--              ,OldService_HouseNo  
--              ,OldService_StreetName  
--              ,OldService_City  
--              ,OldService_Landmark  
--              ,OldService_AreaCode  
--              ,OldService_ZipCode  
--              ,NewPostal_HouseNo  
--                 ,NewPostal_StreetName  
--                 ,NewPostal_City  
--                 ,NewPostal_Landmark  
--                 ,NewPostal_AreaCode  
--                 ,NewPostal_ZipCode  
--                 ,NewService_HouseNo  
--                 ,NewService_StreetName  
--                 ,NewService_City  
--                 ,NewService_Landmark  
--                 ,NewService_AreaCode  
--                 ,NewService_ZipCode  
--                 ,ApprovalStatusId  
--                 ,Remarks  
--                 ,CreatedBy  
--                 ,CreatedDate  
--             )  
--            SELECT   @GlobalAccountNo  
--              ,Postal_HouseNo  
--              ,Postal_StreetName  
--              ,Postal_City  
--              ,Postal_Landmark  
--              ,Postal_AreaCode  
--              ,Postal_ZipCode  
--              ,Service_HouseNo  
--              ,Service_StreetName  
--              ,Service_City  
--              ,Service_Landmark  
--              ,Service_AreaCode  
--              ,Service_ZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@ApprovalStatusId  
--              ,@Details  
--              ,@ModifiedBy  
--              ,dbo.fn_GetCurrentDateTime()  
--            FROM CUSTOMERS.Tbl_CustomerSDetail  
--            WHERE GlobalAccountNumber=@GlobalAccountNo  
--  END  
    
-- --=====================================================================================              
-- --Customer has only one addres and wants to update the address. //CONDITION 1//      
-- --=====================================================================================          
-- IF(@TotalAddress =1 AND @IsSameAsServie = 1)      
-- BEGIN      
--  SET @StatusText='CONDITION 1'  
    
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewPostalLandMark      
--    ,Service_StreetName=@NewPostalStreet      
--    ,Service_City=@NewPostalCity      
--    ,Service_HouseNo=@NewPostalHouseNo      
--    ,Service_ZipCode=@NewPostalZipCode      
--    ,Service_AreaCode=@NewPostalAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@PostalAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=1      
--  WHERE GlobalAccountNumber=@GlobalAccountNo      
-- END          
-- --=====================================================================================      
-- --Customer has one addres and wants to add new address. //CONDITION 2//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)      
-- BEGIN      
--  SET @StatusText='CONDITION 2'  
   
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(      
--                HouseNo      
--                ,LandMark      
--                ,StreetName      
--                ,City      
--                ,AreaCode      
--                ,ZipCode      
--                ,ModifedBy      
--                ,ModifiedDate      
--                ,IsCommunication      
--                ,IsServiceAddress      
--                ,IsActive      
--                ,GlobalAccountNumber      
--                )      
--               VALUES (@NewServiceHouseNo             
--                ,@NewServiceLandMark             
--                ,@NewServiceStreet              
--                ,@NewServiceCity            
--                ,@NewServiceAreaCode            
--             ,@NewServiceZipCode              
--                ,@ModifiedBy            
--                ,dbo.fn_GetCurrentDateTime()        
--                ,@IsCommunicationService               
--                ,1      
--                ,1      
--                ,@GlobalAccountNo      
--                )       
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewServiceLandMark      
--    ,Service_StreetName=@NewServiceStreet      
--    ,Service_City=@NewServiceCity      
--    ,Service_HouseNo=@NewServiceHouseNo      
--    ,Service_ZipCode=@NewServiceZipCode      
--    ,Service_AreaCode=@NewServiceAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@ServiceAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo                     
-- END         
       
-- --=====================================================================================      
-- --Customer alrady has tow address and wants to in-active service. //CONDITION 3//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)       
-- BEGIN      
--  SET @StatusText='CONDITION 3'  
    
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    IsActive=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewPostalLandMark      
--    ,Service_StreetName=@NewPostalStreet      
--    ,Service_City=@NewPostalCity      
--    ,Service_HouseNo=@NewPostalHouseNo      
--    ,Service_ZipCode=@NewPostalZipCode      
--    ,Service_AreaCode=@NewPostalAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@PostalAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=1      
--  WHERE GlobalAccountNumber=@GlobalAccountNo      
-- END        
-- --=====================================================================================      
-- --Customer alrady has tow address and wants to update both address. //CONDITION 4//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)      
-- BEGIN      
--  SET @StatusText='CONDITION 4'  
  
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END            
--      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END            
--      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END            
--      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END            
--      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END              
--      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END               
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationService      
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewServiceLandMark      
-- ,Service_StreetName=@NewServiceStreet      
--    ,Service_City=@NewServiceCity      
--    ,Service_HouseNo=@NewServiceHouseNo      
--    ,Service_ZipCode=@NewServiceZipCode      
--    ,Service_AreaCode=@NewServiceAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@ServiceAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo         
-- END        
-- COMMIT TRAN      
--END TRY      
--BEGIN CATCH      
-- ROLLBACK TRAN      
-- SET @RowsEffected=0      
--END CATCH      
-- SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')               
--END 

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerType]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Karteek   
-- Create date: 03-Apr-2015      
-- Description: The purpose of this procedure is to Update Customer Type 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerType]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @GlobalAccountNumber VARCHAR(50)   
		,@CustomerTypeId INT 
		,@ModifiedBy VARCHAR(50)  
		,@Reason VARCHAR(MAX)  
		,@ApprovalStatusId INT  
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
    
	SELECT @GlobalAccountNumber=C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')  
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')   
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)  
  
		   
	IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
				WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeCustomerTypeBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
					
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
			INSERT INTO Tbl_CustomerTypeChangeLogs( 
				 GlobalAccountNumber
				,OldCustomerTypeId
				,NewCustomerTypeId
				,CreatedBy
				,CreatedDate
				,ApprovalStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,ModifiedBy
				,ModifiedDate)
			SELECT GlobalAccountNumber
				,CustomerTypeId
				,@CustomerTypeId
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,@ApprovalStatusId	
				,@Reason
				,@PresentRoleId
				,@NextRoleId
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()			   
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails		
			WHERE GlobalAccountNumber = @GlobalAccountNumber		
			
			SET @TypeChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails
					SET       
						 CustomerTypeId = @CustomerTypeId 
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerTypeChangeLogs(  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber  
						,OldCustomerTypeId  
						,NewCustomerTypeId  
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)  
					SELECT  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber  
						,OldCustomerTypeId  
						,NewCustomerTypeId  
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_CustomerTypeChangeLogs WHERE CustomerTypeChangeLogId = @TypeChangeRequestId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeCustomerTypeBE')
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CustomerView.ClassName
			   ,CustomerView.OldAccountNo
			   ,CONVERT(VARCHAR(30),RD.ModifiedDate,107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 ORDER BY RD.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectLogs]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),RD.ModifiedDate,107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 ORDER BY RD.CreatedDate DESC
		 -- changed by karteek end 
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek     
     
	SELECT
	(  
		SELECT  TOP 10 
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			,CCL.Remarks
			,CCL.CreatedBy
			,CustomerView.OldAccountNo
			,CustomerView.ClassName
			,CONVERT(VARCHAR(30),CCL.ModifiedDate,107) AS LastTransactionDate   
			,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		ORDER BY CCL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogs]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	--SELECT(  
		 SELECT
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		   ,CONVERT(VARCHAR(30),CC.ModifiedDate,107) AS LastTransactionDate   
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,CC.Remarks  
		   ,CC.CreatedBy
		   ,CustomerView.ClassName
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder      
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 ORDER BY CC.CreatedBy DESC
		 -- changed by karteek end    
END
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeWithLimit]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid

-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
			   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
			   ,CONVERT(VARCHAR(30),CC.ModifiedDate,107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CC.Remarks  
			   ,CC.CreatedBy
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId 
		 -- changed by karteek end       
			ORDER BY CC.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogs]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
		 SELECT 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			   ,CCL.Remarks
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),CCL.ModifiedDate,107) AS LastTransactionDate
			   ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate
			   ,ApproveSttus.ApprovalStatus  
			   ,CCL.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder
			   ,CustomerView.CycleName
		       ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder      
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		 ORDER BY CCL.CreatedDate DESC
		 -- changed by karteek end       
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingUploadBatches]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 28-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetReadingUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY RUB.IsClosedBatch ASC , RUB.CreatedDate DESC ) AS RowNumber,  
				RUB.RUploadBatchId,
				RUB.BatchNo,
				CONVERT(VARCHAR(20),RUB.BatchDate,106) AS StrBatchDate,
				--CONVERT(VARCHAR(24),RUB.CreatedDate,106) AS BatchDate,
				RUF.TotalCustomers,
				RUB.Notes,
				RUF.TotalSucessTransactions,
				RUF.TotalFailureTransactions,
				RUF.RUploadFileId
		FROM Tbl_ReadingUploadBatches RUB
		JOIN Tbl_ReadingUploadFiles RUF ON RUF.RUploadBatchId = RUB.RUploadBatchId
		WHERE RUB.IsClosedBatch = 0 AND (RUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('ReadingsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('ReadingsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsWithLimit]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014   
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
---- Description: The purpose of this procedure is to get limited Tariff change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			,CustomerView.OldAccountNo
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
			,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		    ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
			,TCR.Remarks
			,CONVERT(VARCHAR(50),TCR.ModifiedDate,107) AS LastTransactionDate
			,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate
			,TCR.CreatedBy
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_LCustomerTariffChangeRequest  TCR  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId   
		ORDER BY TCR.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
     
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy
		   ,CONVERT(VARCHAR(50),TCR.ModifiedDate,107) AS LastTransactionDate
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 ORDER BY TCR.CreatedDate DESC
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertPaymentUploadTransactionDetails]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:28-04-2015    
-- Description: The purpose of this procedure is to get file upload details No for Payment uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertPaymentUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempPayments TblPaymentUploads_TempType READONLY 
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	IF EXISTS(SELECT 0 FROM Tbl_BatchDetails WHERE BatchNo = @BatchNo)
		BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_PaymentUploadFiles
					(
						 PUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_PaymentTransactions
					(
						 PUploadFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNo
						,ReceivedDate
						,PaymentMode
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNO
						,ReceivedDate
						,PaymentMode
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempPayments
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
		END
	ELSE
		BEGIN
			SET @IsSuccess = 0
		END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentUploadBatches]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY PUB.IsClosedBatch ASC , PUB.CreatedDate DESC ) AS RowNumber,  
				PUB.PUploadBatchId,
				PUB.BatchNo,
				CONVERT(VARCHAR(20),PUB.BatchDate,106) AS StrBatchDate,
				--CONVERT(VARCHAR(24),PUB.CreatedDate,106) AS BatchDate,
				PUF.TotalCustomers,
				PUB.Notes,
				PUF.TotalSucessTransactions,
				PUF.TotalFailureTransactions,
				PUF.PUploadFileId
		FROM Tbl_PaymentUploadBatches PUB
		JOIN Tbl_PaymentUploadFiles PUF ON PUF.PUploadBatchId = PUB.PUploadBatchId
		WHERE PUB.IsClosedBatch = 0 AND (PUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('PaymentsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('PaymentsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentBatchProcesFailures]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@PUploadFileId INT 
			--,@PUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@PUploadFileId = C.value('(PUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	--SELECT @PUploadBatchId = PUploadBatchId FROM Tbl_PaymentUploadFiles WHERE PUploadFileId = @PUploadFileId

	SELECT 
			PUF.TotalFailureTransactions,
			PUF.TotalCustomers,
			PUB.BatchNo,
			--PUB.BatchDate,
			CONVERT(VARCHAR(24),PUB.BatchDate,106) AS BatchDate,
			PUB.Notes
	FROM Tbl_PaymentUploadFiles PUF
	JOIN Tbl_PaymentUploadBatches PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId
	WHERE PUF.PUploadFileId = @PUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			AmountPaid,
			ReceiptNo,
			ReceivedDate,
			PaymentMode,
			Comments
		FROM Tbl_PaymentFailureTransactions
		WHERE PUploadFileId = @PUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
		SELECT
			 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br> Cycle : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br /> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br /> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br /> Cycle : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br /> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CONVERT(VARCHAR(50),BookChange.ModifiedDate,107) AS LastTransactionDate
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,BD.BusinessUnitName
			,BD.ServiceUnitName
			,BD.ServiceCenterName
			,BD.CycleName
			,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
			,BD.BookSortOrder
			,'' AS CustomerSortOrder
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		--SELECT 
		--	 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
		--	--,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br>'+
		--	--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br>'+
		--	--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br>'+
		--	--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br>'+
		--	--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
		--	--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) </br>'+
		--	--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) </br>'+
		--	--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) </br>'+
		--	--  BD.CycleName +' ( '+BD.CycleCode+ ' ) </br>'+
		--	--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
		--	,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		--   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

		--	,BookChange.Remarks
		--	,BookChange.CreatedBy
		--	,MTC.ClassName
		--	,CD.OldAccountNo
		--	,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		--	,ApproveSttus.ApprovalStatus  
		--	,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name
		--	,BD.BusinessUnitName
		--	,BD.ServiceUnitName
		--	,BD.ServiceCenterName
		--	,BD.CycleName
		--	,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
		--	,BD.BookSortOrder
		--	,BD.CustomerSortOrder
		--FROM Tbl_BookNoChangeLogs  BookChange  
		--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
		--AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		--INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		--INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		--INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		--INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		--INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		--INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 04/28/2015 20:57:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for shown in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
  
  SELECT
	(  
		SELECT TOP 10
			 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) </br> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) </br> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) </br> Cycle : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) </br> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br /> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br /> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br /> Cycle : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br /> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CONVERT(VARCHAR(50),BookChange.ModifiedDate,107) AS LastTransactionDate
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
     
 --    SELECT
	--(  
	--	SELECT TOP 10
	--		 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
	--		,'' AS BU_ID
	--		,'' AS SU_ID
	--		,'' AS ServiceCenterId
	--		,'' AS CycleId
	--	    ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
	--	    ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo
	--		,BookChange.Remarks
	--		,BookChange.CreatedBy
	--		,CustomerView.ClassName
	--		,CustomerView.OldAccountNo
	--		,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
	--		,ApproveSttus.ApprovalStatus  
	--		,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
	--		,COUNT(0) OVER() AS TotalChanges  
	--	FROM Tbl_BookNoChangeLogs  BookChange  
	--	INNER JOIN [UDV_CustomerDescription]  CustomerView ON BookChange.GlobalAccountNo = CustomerView.GlobalAccountNumber  
	--	AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
	--	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
	--	INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
	--	INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
	--	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
	--	INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
	--	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId 
	--	INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
	--	ORDER BY  BookChange.CreatedDate DESC
	--	FOR XML PATH('AuditTrayList'),TYPE  
	--)  
	--FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')
    
END  
----------------------------------------------------------------------------------------------


GO

