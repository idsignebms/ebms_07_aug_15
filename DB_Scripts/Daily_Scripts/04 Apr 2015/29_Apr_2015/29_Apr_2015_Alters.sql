GO
UPDATE Tbl_BillingDisabledBooks
SET IsPartialBill = 0
WHERE IsPartialBill IS NULL
GO
ALTER TABLE Tbl_BillingDisabledBooks
ALTER COLUMN IsPartialBill BIT NOT NULL
GO
ALTER TABLE Tbl_BillingDisabledBooks 
ADD CONSTRAINT IsPartialBillDefault0 DEFAULT 0 FOR IsPartialBill
GO
GO
ALTER TABLE Tbl_ReadingSucessTransactions
ALTER COLUMN Comments VARCHAR(MAX)
GO
ALTER TABLE Tbl_ReadingFailureTransactions
ALTER COLUMN Comments VARCHAR(MAX)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Bulk Meter Reading Upload','../Billing/MeterReadingBulkUpload.aspx',4,22,1,1,182)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,182,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,182,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Bulk Meter Reading Upload Status','../Billing/ReadingBatchProcesStatus.aspx',4,23,1,1,183)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,183,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,183,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
UPDATE Tbl_Menus SET IsActive=0 WHERE Name='Payment Uploads' AND ReferenceMenuId=6
GO
UPDATE Tbl_Menus SET IsActive=0 WHERE Name='Meter Reading Upload' AND ReferenceMenuId=4
GO
