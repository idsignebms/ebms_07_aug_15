GO
/****** Object:  Table [dbo].[Tbl_MMenuAccessLevels]    Script Date: 04/30/2015 16:22:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MMenuAccessLevels](
	[AccessLevelID] [int] IDENTITY(1,1) NOT NULL,
	[AccessLevelName] [varchar](50) NOT NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AccessLevelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MMenuAccessLevels] ON
INSERT [dbo].[Tbl_MMenuAccessLevels] ([AccessLevelID], [AccessLevelName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'BusinessUnit Level', N'superadmin', CAST(0x0000A47E00B8F087 AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_MMenuAccessLevels] ([AccessLevelID], [AccessLevelName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'ServiceUnit Level', N'superadmin', CAST(0x0000A47E00B8F088 AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_MMenuAccessLevels] ([AccessLevelID], [AccessLevelName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'ServiceCenter Level', N'superadmin', CAST(0x0000A47E00B8F089 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MMenuAccessLevels] OFF

GO

ALTER TABLE Tbl_Menus
ADD AccessLevelID INT