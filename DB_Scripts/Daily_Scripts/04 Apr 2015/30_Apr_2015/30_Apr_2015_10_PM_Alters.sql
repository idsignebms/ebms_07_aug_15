GO
CREATE TYPE TypeCustomerAverageUpload AS TABLE
(
	 SNO VARCHAR(20)
	,Global_Account_Number VARCHAR(50)
	,Average_Reading VARCHAR(50)
)
GO
GO
UPDATE Tbl_Menus SET IsActive = 0 WHERE ReferenceMenuId=6 AND Name='Customer Bill Payments'
GO
UPDATE Tbl_Menus SET IsActive = 0 WHERE Name = 'Add Cash Offices' AND ReferenceMenuId = 122
GO
CREATE TABLE Tbl_PaymentsLogs
(
PaymentLogId	BIGINT PRIMARY KEY IDENTITY(1,1),
GlobalAccountNumber VARCHAR(50)  NOT NULL,
AccountNo VARCHAR(50)  NOT NULL,
ReceiptNumber VARCHAR(20) ,
PaymentMode INT  NOT NULL,
PaidAmount DECIMAL(18,2)  NOT NULL,
PadiDate DATETIME  NOT NULL,
PresentApprovalRole	INT  NOT NULL,
NextApprovalRole	INT  NOT NULL,
ApproveStatusId	INT  NOT NULL,
Remarks	VARCHAR(MAX),
CreatedBy VARCHAR(50)  NOT NULL,
CreatedDate DATETIME  NOT NULL,
ModifedBy VARCHAR(50),
ModifiedDate	DATETIME,
CONSTRAINT Tbl_PaymentsLogs_GlobalAccountNumber_FK FOREIGN KEY ( GlobalAccountNumber ) 
REFERENCES CUSTOMERS.Tbl_CustomersDetail(GlobalAccountNumber),
CONSTRAINT Tbl_PaymentsLogs_PaymentMode_FK FOREIGN KEY (PaymentMode) 
REFERENCES Tbl_MPaymentMode(PaymentModeId),
CONSTRAINT Tbl_PaymentsLogs_ApproveStatusId_FK FOREIGN KEY (ApproveStatusId) 
REFERENCES Tbl_MApprovalStatus(ApprovalStatusId)
)
GO
CREATE TABLE Tbl_MeterReadingsLogs
(
MeterReadingLogId	 BIGINT PRIMARY KEY IDENTITY(1,1),
GlobalAccountNumber VARCHAR(50)  NOT NULL,
AccountNo VARCHAR(50)  NOT NULL,
MeterNumber VARCHAR(20),
PreviousReading VARCHAR(50),
PresentReading VARCHAR(50),
Multiplier INT,
AverageUsage NUMERIC(20, 2),
Usage NUMERIC(20, 2),
ReadDate DATETIME,
ReadBy VARCHAR(50),
ReadType INT  NOT NULL,
IsTamper BIT,
TamperRemarks VARCHAR(200),
MeterReadingFromId INT,
IsRollOver BIT,
PresentApprovalRole	INT  NOT NULL,
NextApprovalRole INT  NOT NULL,
ApproveStatusId	INT  NOT NULL,
Remarks VARCHAR(MAX),
CreatedBy VARCHAR(50)  NOT NULL,
CreatedDate DATETIME  NOT NULL,
ModifedBy VARCHAR(50),
ModifiedDate	DATETIME
CONSTRAINT Tbl_MeterReadingsLogs_GlobalAccountNumber_FK FOREIGN KEY ( GlobalAccountNumber ) 
REFERENCES CUSTOMERS.Tbl_CustomersDetail(GlobalAccountNumber),
CONSTRAINT Tbl_MeterReadingsLogs_ApproveStatusId_FK FOREIGN KEY (ApproveStatusId) 
REFERENCES Tbl_MApprovalStatus(ApprovalStatusId),
CONSTRAINT Tbl_MeterReadingsLogs_MeterReadingFromId_FK FOREIGN KEY (MeterReadingFromId) 
REFERENCES MASTERS.Tbl_MMeterReadingsFrom(MeterReadingFromId)
)
GO
CREATE TABLE Tbl_AdjustmentsLogs
(
AdjustmentLogId BIGINT PRIMARY KEY IDENTITY(1,1),
GlobalAccountNumber VARCHAR(50)  NOT NULL,
AccountNo	VARCHAR(50)  NOT NULL,
BillNumber VARCHAR(50),
BillAdjustmentTypeId INT  NOT NULL,
MeterNumber VARCHAR(20),
PreviousReading	VARCHAR(20),
CurrentReadingAfterAdjustment VARCHAR(50),
CurrentReadingBeforeAdjustment VARCHAR(50),
AdjustedUnits DECIMAL(18,2),
TaxEffected DECIMAL(18,2),
TotalAmountEffected DECIMAL(18,2),
EnergryCharges DECIMAL(18,2),
AdditionalCharges DECIMAL(18,2),
PresentApprovalRole INT  NOT NULL,
NextApprovalRole INT  NOT NULL,
ApproveStatusId	INT  NOT NULL,
Remarks	VARCHAR(MAX),
CreatedBy VARCHAR(50)  NOT NULL,
CreatedDate DATETIME  NOT NULL,
ModifedBy VARCHAR(50),
ModifiedDate	DATETIME,
CONSTRAINT Tbl_AdjustmentsLogs_GlobalAccountNumber_FK FOREIGN KEY ( GlobalAccountNumber ) 
REFERENCES CUSTOMERS.Tbl_CustomersDetail(GlobalAccountNumber),
CONSTRAINT Tbl_AdjustmentsLogs_ApproveStatusId_FK FOREIGN KEY (ApproveStatusId) 
REFERENCES Tbl_MApprovalStatus(ApprovalStatusId),
CONSTRAINT Tbl_AdjustmentsLogs_BillAdjustmentTypeId_FK FOREIGN KEY (BillAdjustmentTypeId) 
REFERENCES Tbl_BillAdjustmentType(BATID)
)
GO
CREATE TABLE Tbl_DirectCustomersAverageChangeLogs
(
AverageChangeLogId BIGINT PRIMARY KEY IDENTITY(1,1),
GlobalAccountNumber VARCHAR(50)  NOT NULL,
AccountNo	VARCHAR(50)  NOT NULL,
PreviousAverage DECIMAL(18,2),
NewAverage DECIMAL(18,2),
PresentApprovalRole	INT  NOT NULL,
NextApprovalRole INT  NOT NULL,
ApproveStatusId	INT  NOT NULL,
Remarks	VARCHAR(MAX),
CreatedBy VARCHAR(50)  NOT NULL,
CreatedDate DATETIME  NOT NULL,
ModifedBy VARCHAR(50),
ModifiedDate	DATETIME,
CONSTRAINT Tbl_DirectCustomersAverageChangeLogs_GlobalAccountNumber_FK FOREIGN KEY ( GlobalAccountNumber ) 
REFERENCES CUSTOMERS.Tbl_CustomersDetail(GlobalAccountNumber),
CONSTRAINT Tbl_DirectCustomersAverageChangeLogs_ApproveStatusId_FK FOREIGN KEY (ApproveStatusId) 
REFERENCES Tbl_MApprovalStatus(ApprovalStatusId)
)
GO
