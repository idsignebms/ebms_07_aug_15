
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAverageBatchProcesFailures]    Script Date: 04/30/2015 22:16:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : Karteek
-- Modified Date:30-04-2015 
-- Description:	This pupose of this procedure is to get Average Batch Proces Failures Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCustomerAverageBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_AverageUploadFiles(NOLOCK) RUF
	JOIN Tbl_AverageUploadBatches(NOLOCK) RUB ON RUB.AvgUploadBatchId = RUF.AvgUploadBatchId
	WHERE RUF.AvgUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			AverageReading,
			Comments,
			ReadingFailureransactionID
		FROM Tbl_AverageFailureTransactions(NOLOCK)
		WHERE PUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_CloseCustomerAverageUploadBatch]    Script Date: 04/30/2015 22:16:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : Karteek
-- Modified Date:30-04-2015 
-- Description:	This pupose of this procedure is to Close Average Upload Batch record
-- =============================================
CREATE PROCEDURE [dbo].[USP_CloseCustomerAverageUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @RUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
		
	SELECT       
		 @RUploadBatchId = C.value('(RUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	UPDATE Tbl_AverageUploadBatches 
	SET IsClosedBatch = 1
	WHERE AvgUploadBatchId = @RUploadBatchId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ReadingsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertAverageUploadTransactionDetails]    Script Date: 04/30/2015 22:16:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:30-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for Average uploads
-- =============================================            
CREATE PROCEDURE [dbo].[USP_InsertAverageUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TypeCustomerAverageUpload READONLY
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	--IF EXISTS(SELECT 0 FROM Tbl_ReadingUploadBatches WHERE BatchNo = @BatchNo)
	--	BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_AverageUploadFiles
					(
						 AvgUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_AverageTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,Global_Account_Number
						,Average_Reading
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
	--	END
	--ELSE
	--	BEGIN
	--		SET @IsSuccess = 0
	--	END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAverageUploadBatches]    Script Date: 04/30/2015 22:16:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 30-Apr-2015
-- Description:	This pupose of this procedure is to get Average Upload Batches Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCustomerAverageUploadBatches] 
(
	@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY RUB.IsClosedBatch ASC , RUB.CreatedDate DESC ) AS RowNumber,  
				RUB.AvgUploadBatchId,
				RUB.BatchNo,
				CONVERT(VARCHAR(20),RUB.BatchDate,106) AS StrBatchDate,
				RUF.TotalCustomers,
				RUB.Notes,
				RUF.TotalSucessTransactions,
				RUF.TotalFailureTransactions,
				RUF.AvgUploadFileId
		FROM Tbl_AverageUploadBatches(NOLOCK) RUB
		JOIN Tbl_AverageUploadFiles(NOLOCK) RUF ON RUF.AvgUploadBatchId = RUB.AvgUploadBatchId
		WHERE RUB.IsClosedBatch = 0 AND (RUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('ReadingsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('ReadingsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertAverageConsumptionBatchUploadDetails]    Script Date: 04/30/2015 22:16:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:30-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for Average uploads
-- =============================================            
CREATE PROCEDURE [dbo].[USP_InsertAverageConsumptionBatchUploadDetails]
(
	@BatchNo int,            
	@BatchName varchar(50) ,            
	@BatchDate varchar(50) ,            
	@CreatedBy varchar(50),             
	@Description VARCHAR(MAX), 
	--@BU_ID VARCHAR(50),       
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TypeCustomerAverageUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @PUploadBatchId INT, @PUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_AverageUploadBatches WHERE BatchNo=@BatchNo 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120)))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_AverageUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @PUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_AverageUploadFiles
					(
						 AvgUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @PUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_AverageTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @PUploadFileId
						,SNO
						,Global_Account_Number
						,Average_Reading
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTariffDetails_New]    Script Date: 04/30/2015 22:17:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTariffDetails_New]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
  
	DECLARE  
		 @AccountNo VARCHAR(50)   
		,@CreatedBy VARCHAR(50)  
		,@Remarks VARCHAR(50)  
		,@Flag INT  
		,@ApprovalStatusId INT  
		,@OldClassID INT  
		,@NewClassID INT  
		,@TariffChangeRequestId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = 0
		,@NextRoleId INT = 0
		,@CurrentLevel INT
		,@OldClusterTypeId INT  
		,@NewClusterTypeId INT 
		,@IsFinalApproval BIT = 0 

	SELECT   
		 @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@Flag = C.value('(Flag)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@OldClassID = C.value('(OldClassID)[1]','INT')  
		,@NewClassID = C.value('(NewClassID)[1]','INT')  
		,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@OldClusterTypeId = C.value('(OldClusterTypeId)[1]','INT')  
		,@NewClusterTypeId = C.value('(ClusterTypeId)[1]','INT') 
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT') 
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
   
	IF(@Flag = 1)  -- Approval Permission Required
	BEGIN  
		IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo = @AccountNo AND ApprovalStatusId = 1)  
			BEGIN  -- Pending Request already exists
				SELECT 1 AS ApprovalProcess  
			END  
		ELSE  -- New Request
			BEGIN  
				DECLARE @RoleId INT--,@IsFinalApproval BIT=0
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
				SET @CurrentLevel = 0
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
					BEGIN
						SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
					END
				
				--SELECT 
				--	 @CurrentLevel = [Level] 
				--	,@IsFinalApproval = (CASE IsFinalApproval
				--						 WHEN 1 THEN (CASE WHEN UserIds IS NOT NULL 
				--											THEN (CASE WHEN EXISTS(SELECT 0 FROM dbo.fn_Split(UserIds,',') WHERE @CreatedBy = com) 
				--														THEN 1 END)
				--						 ELSE 1 END)
				--						 END)
				--FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId 
				--	AND	RoleId = @RoleId

				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

				INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					 AccountNo  
					,PreviousTariffId  
					,ChangeRequestedTariffId 
					,OldClusterCategoryId
					,NewClusterCategoryId 
					,CreatedBy  
					,CreatedDate  
					,Remarks  
					,ApprovalStatusId
					,PresentApprovalRole
					,NextApprovalRole)  
				VALUES(  
					 @AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@OldClusterTypeId
					,@NewClusterTypeId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,1 -- Process
					,@PresentRoleId
					,@NextRoleId)
					
				--SET @TariffChangeRequestId = SCOPE_IDENTITY()

				--IF(@IsFinalApproval = 1)
				--	BEGIN
				--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
				--		SET       
				--			 TariffClassID = @NewClassID
				--			,ClusterCategoryId = @NewClusterTypeId  
				--			,ModifedBy = @CreatedBy  
				--			,ModifiedDate = dbo.fn_GetCurrentDateTime()  
				--		WHERE GlobalAccountNumber = @AccountNo 
						
				--		INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				--			 TariffChangeRequestId
				--			,AccountNo  
				--			,PreviousTariffId  
				--			,ChangeRequestedTariffId 
				--			,OldClusterCategoryId
				--			,NewClusterCategoryId 
				--			,CreatedBy  
				--			,CreatedDate  
				--			,Remarks  
				--			,ApprovalStatusId
				--			,PresentApprovalRole
				--			,NextApprovalRole)  
				--		VALUES(  
				--			 @TariffChangeRequestId
				--			,@AccountNo  
				--			,@OldClassID  
				--			,@NewClassID  
				--			,@OldClusterTypeId
				--			,@NewClusterTypeId
				--			,@CreatedBy  
				--			,dbo.fn_GetCurrentDateTime()  
				--			,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				--			,@ApprovalStatusId
				--			,@PresentRoleId
				--			,@NextRoleId)
				--	END

				SELECT 1 As IsSuccess--,@IsFinalApproval AS IsFinalApproval  
				FOR XML PATH('TariffManagementBE'),TYPE		
			END  
		END  
	ELSE IF(@Flag = 2)  -- Tariff Approval Page
		BEGIN  
			IF(@ApprovalStatusId = 2)  -- Request Approved
				BEGIN  
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
						BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
											RoleId = (SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
													WHERE TariffChangeRequestId = @TariffChangeRequestId))

							SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

							IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApprovalStatusId = @ApprovalStatusId
									WHERE --AccountNo = @AccountNo   
									--AND 
									TariffChangeRequestId = @TariffChangeRequestId  

									UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
									SET       
										 TariffClassID = @NewClassID
										,ClusterCategoryId = @NewClusterTypeId  
										,ModifedBy = @ModifiedBy  
										,ModifiedDate = dbo.fn_GetCurrentDateTime()  
									WHERE GlobalAccountNumber = @AccountNo  
									
									INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
										 TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId  
										,ChangeRequestedTariffId 
										,OldClusterCategoryId
										,NewClusterCategoryId 
										,CreatedBy  
										,CreatedDate  
										,Remarks  
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole)  
									SELECT   
										 @TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId
										,ChangeRequestedTariffId  
										,OldClusterCategoryId
										,NewClusterCategoryId
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,Remarks 
										,ApprovalStatusId
										,@PresentRoleId
										,@NextRoleId
									FROM Tbl_LCustomerTariffChangeRequest
									WHERE --AccountNo = @AccountNo   
									--AND 
									TariffChangeRequestId = @TariffChangeRequestId 
								END
							ELSE -- Not a final approval
								BEGIN
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,Remarks=@Remarks
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId
										,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
									WHERE --AccountNo = @AccountNo 
									--AND 
									TariffChangeRequestId = @TariffChangeRequestId
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							UPDATE Tbl_LCustomerTariffChangeRequest 
							SET  
								 ApprovalStatusId = @ApprovalStatusId   
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE --AccountNo = @AccountNo  
							--AND 
							TariffChangeRequestId = @TariffChangeRequestId  

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET       
								 TariffClassID = @NewClassID  
								,ClusterCategoryId = @NewClusterTypeId 
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
								 TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId  
								,ChangeRequestedTariffId 
								,OldClusterCategoryId
								,NewClusterCategoryId 
								,CreatedBy  
								,CreatedDate  
								,Remarks  
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT   
								 @TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId
								,ChangeRequestedTariffId  
								,OldClusterCategoryId
								,NewClusterCategoryId
								,@ModifiedBy  
								,dbo.fn_GetCurrentDateTime()  
								,Remarks 
								,ApprovalStatusId
								,@PresentRoleId
								,@NextRoleId
							FROM Tbl_LCustomerTariffChangeRequest
							WHERE --AccountNo = @AccountNo   
							--AND 
							TariffChangeRequestId = @TariffChangeRequestId 
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
			ELSE  
				BEGIN  -- Request Not Approved
					IF(@ApprovalStatusId = 3) -- Request is Rejected
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
								BEGIN -- Approval levels are there and status is rejected
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
															RoleId = (SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
																		WHERE TariffChangeRequestId = @TariffChangeRequestId))

									SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
									FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
								END
						END
					ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
								SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
								WHERE TariffChangeRequestId = @TariffChangeRequestId
						END

					-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
					UPDATE Tbl_LCustomerTariffChangeRequest SET 
						 ApprovalStatusId = @ApprovalStatusId   
						,ModifiedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId
						,Remarks = @Remarks
					WHERE --AccountNo = @AccountNo   
					--AND 
					TariffChangeRequestId = @TariffChangeRequestId  

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
		END  
	ELSE  -- Approval Permission Not Required
		BEGIN  

			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
				 AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId
				,OldClusterCategoryId
				,NewClusterCategoryId
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @AccountNo  
				,@OldClassID  
				,@NewClassID
				,@OldClusterTypeId
				,@NewClusterTypeId  
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId
				,@PresentRoleId
				,@NextRoleId) 
				
			SET @TariffChangeRequestId = SCOPE_IDENTITY()

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				 TariffClassID = @NewClassID
				,ClusterCategoryId = @NewClusterTypeId  
				,ModifedBy = @CreatedBy  
				,ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
			
			INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				 TariffChangeRequestId
				,AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId 
				,OldClusterCategoryId
				,NewClusterCategoryId 
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @TariffChangeRequestId
				,@AccountNo  
				,@OldClassID  
				,@NewClassID  
				,@OldClusterTypeId
				,@NewClusterTypeId
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId
				,@PresentRoleId
				,@NextRoleId)
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('TariffManagementBE'),TYPE  
		END		
END  
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertAverageUploadTransactionDetails]    Script Date: 04/30/2015 22:17:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:30-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for Average uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertAverageUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TypeCustomerAverageUpload READONLY
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	--IF EXISTS(SELECT 0 FROM Tbl_ReadingUploadBatches WHERE BatchNo = @BatchNo)
	--	BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_AverageUploadFiles
					(
						 AvgUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_AverageTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,Global_Account_Number
						,Average_Reading
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
	--	END
	--ELSE
	--	BEGIN
	--		SET @IsSuccess = 0
	--	END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAverageUploadBatches]    Script Date: 04/30/2015 22:17:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 30-Apr-2015
-- Description:	This pupose of this procedure is to get Average Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerAverageUploadBatches] 
(
	@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY RUB.IsClosedBatch ASC , RUB.CreatedDate DESC ) AS RowNumber,  
				RUB.AvgUploadBatchId,
				RUB.BatchNo,
				CONVERT(VARCHAR(20),RUB.BatchDate,106) AS StrBatchDate,
				RUF.TotalCustomers,
				RUB.Notes,
				RUF.TotalSucessTransactions,
				RUF.TotalFailureTransactions,
				RUF.AvgUploadFileId
		FROM Tbl_AverageUploadBatches(NOLOCK) RUB
		JOIN Tbl_AverageUploadFiles(NOLOCK) RUF ON RUF.AvgUploadBatchId = RUB.AvgUploadBatchId
		WHERE RUB.IsClosedBatch = 0 AND (RUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('ReadingsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('ReadingsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAverageBatchProcesFailures]    Script Date: 04/30/2015 22:17:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : Karteek
-- Modified Date:30-04-2015 
-- Description:	This pupose of this procedure is to get Average Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerAverageBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_AverageUploadFiles(NOLOCK) RUF
	JOIN Tbl_AverageUploadBatches(NOLOCK) RUB ON RUB.AvgUploadBatchId = RUF.AvgUploadBatchId
	WHERE RUF.AvgUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			AverageReading,
			Comments,
			ReadingFailureransactionID
		FROM Tbl_AverageFailureTransactions(NOLOCK)
		WHERE PUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertAverageConsumptionBatchUploadDetails]    Script Date: 04/30/2015 22:17:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:30-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for Average uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertAverageConsumptionBatchUploadDetails]
(
	@BatchNo int,            
	@BatchName varchar(50) ,            
	@BatchDate varchar(50) ,            
	@CreatedBy varchar(50),             
	@Description VARCHAR(MAX), 
	--@BU_ID VARCHAR(50),       
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TypeCustomerAverageUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @PUploadBatchId INT, @PUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_AverageUploadBatches WHERE BatchNo=@BatchNo 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120)))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_AverageUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @PUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_AverageUploadFiles
					(
						 AvgUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @PUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_AverageTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @PUploadFileId
						,SNO
						,Global_Account_Number
						,Average_Reading
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_CloseCustomerAverageUploadBatch]    Script Date: 04/30/2015 22:17:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : Karteek
-- Modified Date:30-04-2015 
-- Description:	This pupose of this procedure is to Close Average Upload Batch record
-- =============================================
ALTER PROCEDURE [dbo].[USP_CloseCustomerAverageUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @RUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
		
	SELECT       
		 @RUploadBatchId = C.value('(RUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	UPDATE Tbl_AverageUploadBatches 
	SET IsClosedBatch = 1
	WHERE AvgUploadBatchId = @RUploadBatchId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ReadingsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 04/30/2015 22:17:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 
ALTER PROCEDURE  [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        


	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
						--,@ReadDate =
						--,@Multiplier  
						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
								,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo


							IF @PaidAmount IS NOT NULL
							BEGIN
												
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END


							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId


							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------


							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
							
							--------------------------------------------------------------------------------------
							--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
							--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
							--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
							
							---------------------------------------------------------------------------------------
							--Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    
							---------------------------------------------------------------------------------------
						END
						
						  
						   


							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1


						   --IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3 or isnull(@BookDisableType,0) =1	 
						   --BEGIN
						   --GOTO Loop_End;
						   --END    


							 

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
											SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
											SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									

										--IF	  @DisableDate IS NOT NULL   and   DATEDIFF(DAY, @DisableDate ,@LastBillGenerated) < 15
										--	BEGIN
										--	   SET	 @FixedCharges=0
										--	END
										--ELSE
										--	BEGIN
														
										--	END
									 
							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )


							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
						ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount-@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +' \n '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
				
							FOR XML PATH(''), TYPE)
						 .value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
					
							
							
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmountWithTax   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterReadingBatchUploadDetails]    Script Date: 04/30/2015 22:17:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Bhimaraju V
-- Modified Date:28-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for MerterReading uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertMeterReadingBatchUploadDetails]
(
	@BatchNo INT,            
	@BatchName VARCHAR(50) ,            
	@BatchDate VARCHAR(50) ,            
	@CreatedBy VARCHAR(50),             
	@Description VARCHAR(MAX),
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TblMeterReadingsBulkUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @RUploadBatchId INT, @RUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_ReadingUploadBatches WHERE BatchNo=@BatchNo 
						--AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate))
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           

					INSERT INTO Tbl_ReadingUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @RUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingUploadFiles
					(
						 RUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @RUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @RUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingTransactions
					(
						 RUploadFileId
						,SNO
						,AccountNo
						,CurrentReading
						,ReadingDate
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @RUploadFileId
						,SNO
						,Account_Number
						,Current_Reading
						,Read_Date
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertPaymentBatchUploadDetails]    Script Date: 04/30/2015 22:17:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Karteek
-- Modified Date:27-04-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No for Payment uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertPaymentBatchUploadDetails]
(
	@BatchNo int,            
	@BatchName varchar(50) ,            
	@BatchDate varchar(50) ,            
	@CreatedBy varchar(50),             
	@Description VARCHAR(MAX), 
	@BU_ID VARCHAR(50),       
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempPayments TblPaymentUploads_TempType READONLY 
)      
AS            
BEGIN            

	DECLARE @IsExists BIT = 0, @IsSuccess BIT = 0
	DECLARE @PUploadBatchId INT, @PUploadFileId INT
	    
	IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails WHERE BatchNo=@BatchNo 
						--AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate) 
						AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
						AND BU_ID=@BU_ID))            
		BEGIN 
			BEGIN TRY
				BEGIN TRAN           
					INSERT INTO Tbl_BatchDetails(            
						BatchNo,             
						BatchDate,           
						CreatedBy,            
						CreatedDate,           
						BatchStatus,  
						BU_ID,   
						[Description],
						BatchTotal  
					)            
						VALUES(            
						@BatchNo,     
						@BatchDate,           
						@CreatedBy,                
						dbo.fn_GetCurrentDateTime(), 
						1,            
						@BU_ID,
						CASE @Description WHEN '' THEN NULL ELSE @Description END, 
						0   
					)   
					
					INSERT INTO Tbl_PaymentUploadBatches
					(
						 BatchNo
						,BatchDate
						,TotalCustomers
						,CreatedBy
						,CreatedDate
						,LastModifyDate
						,LastModifyBy
						,Notes
					)
					VALUES
					(
						 @BatchNo
						,@BatchDate
						,@TotalCustomers
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,dbo.fn_GetCurrentDateTime()
						,@CreatedBy
						,@Description
					)
					
					SET @PUploadBatchId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_PaymentUploadFiles
					(
						 PUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PUploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @PUploadFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_PaymentTransactions
					(
						 PUploadFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNo
						,ReceivedDate
						,PaymentMode
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @PUploadFileId
						,SNO
						,AccountNo
						,AmountPaid
						,ReceiptNO
						,ReceivedDate
						,PaymentMode
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempPayments
					         
					SET @IsSuccess = 1 
					SET @IsExists = 0
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
				SET @IsExists = 0
			END CATCH		
		END            
	ELSE 
		BEGIN           
			SET @IsSuccess = 0 
			SET @IsExists = 1
		END
		
	SELECT @IsSuccess AS IsSuccess, @IsExists AS IsExists
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBatchDetails]    Script Date: 04/30/2015 22:17:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-03-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertBatchDetails](@XmlDoc XML)      
AS            
BEGIN            
	DECLARE  @BatchNo int,            
		 @BatchName varchar(50) ,            
		 @BatchDate datetime ,            
		 @CashOffice INT ,            
		 @CashierID VARCHAR(50) ,            
		 @CreatedBy varchar(50),            
		 @BatchTotal NUMERIC(20,4),            
		 @Description VARCHAR(MAX), 
		 @BU_ID VARCHAR(50),       
		 @Flag INT            
             
	SELECT             
		 @BatchNo=C.value('(BatchNo)[1]','int')            
		,@BatchName=C.value('(BatchName)[1]','varchar(50)')            
		,@BatchDate=C.value('(BatchDate)[1]','DATETIME')            
		,@CashOffice = C.value('(OfficeId)[1]','INT')            
		,@CashierID = C.value('(CashierId)[1]','VARCHAR(50)')             
		,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')            
		,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')            
		,@Description=C.value('(Description)[1]','VARCHAR(MAX)')            
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')            
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)         
 --TEMP INSERTING FOR APPROVAL           
	IF(@Flag=1)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo AND BatchStatus = 1))            
				BEGIN            
					INSERT INTO Tbl_BatchDetailsApproval(            
					BatchNo,            
					BatchName,            
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,            
					[Description]
					)            
					VALUES(            
					@BatchNo,            
					Case  @BatchName when '' then null else @BatchName end,  
					@BatchDate,  
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					Case  @CreatedBy when '' then null else @CreatedBy end,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					CASE @Description WHEN '' THEN NULL ELSE @Description END
					)          
					SELECT 1 AS IsSuccess, @@IDENTITY AS BatchId  FOR XML PATH('BillingBE')            
				END            
			ELSE
				BEGIN            
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate,BatchName  
						,BatchNo  
						,BatchTotal  
						,BatchId
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0))   
					FROM Tbl_CustomerPaymentsApproval            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetailsApproval   
					WHERE BatchNo=@BatchNo   
					FOR XML PATH('BillingBE') 
				END 
		END      
	IF(@Flag=2)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails 
								WHERE BatchNo=@BatchNo 
								AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
								--AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
								AND BU_ID=@BU_ID))            
				BEGIN            
					INSERT INTO Tbl_BatchDetails(            
					BatchNo,             
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,  
					BU_ID,   
					[Description]  
					)            
					VALUES(            
					@BatchNo,     
					@BatchDate,            
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					@CreatedBy,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					@BU_ID,
					CASE @Description WHEN '' THEN NULL ELSE @Description END       
					)            
					SELECT 1 AS IsSuccess, @@IDENTITY AS BatchId  FOR XML PATH('BillingBE')            
				END            
			ELSE 
				BEGIN           
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate  
						,BatchName  
						,BatchNo  
						,BatchTotal  
						,BatchId
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetails   
					WHERE BatchNo=@BatchNo 
					AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
					--AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
					AND BU_ID=@BU_ID
					FOR XML PATH('BillingBE') 
				END 
		END      
END  
------------------------------------------------------------------------------------------

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 04/30/2015 22:19:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND PaidAmount = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNO) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
											AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,(SELECT BatchID FROM Tbl_BatchDetails(NOLOCK)
			WHERE BatchNo = (SELECT TOP(1) BatchNo FROM Tbl_PaymentUploadBatches(NOLOCK) PUB 
							WHERE PUB.PUploadBatchId = PUF.PUploadBatchId)) AS BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.OldAccountNo = TempDetails.AccountNo
	--((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
	--					OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				--UPDATE B
				--SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				--FROM Tbl_BatchDetails B
				--INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END