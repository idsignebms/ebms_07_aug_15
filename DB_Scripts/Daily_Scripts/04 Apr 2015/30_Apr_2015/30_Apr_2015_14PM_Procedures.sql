GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentUploadBatches]    Script Date: 04/30/2015 14:04:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY PUB.IsClosedBatch ASC , PUB.CreatedDate DESC ) AS RowNumber,  
				PUB.PUploadBatchId,
				PUB.BatchNo,
				CONVERT(VARCHAR(20),PUB.BatchDate,106) AS StrBatchDate,
				--CONVERT(VARCHAR(24),PUB.CreatedDate,106) AS BatchDate,
				PUF.TotalCustomers,
				PUB.Notes,
				PUF.TotalSucessTransactions,
				PUF.TotalFailureTransactions,
				PUF.PUploadFileId
		FROM Tbl_PaymentUploadBatches(NOLOCK) PUB
		JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadBatchId = PUB.PUploadBatchId
		WHERE PUB.IsClosedBatch = 0 AND (PUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('PaymentsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('PaymentsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentBatchProcesFailures]    Script Date: 04/30/2015 14:04:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@PUploadFileId INT 
			--,@PUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@PUploadFileId = C.value('(PUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	--SELECT @PUploadBatchId = PUploadBatchId FROM Tbl_PaymentUploadFiles WHERE PUploadFileId = @PUploadFileId

	SELECT 
			PUF.TotalFailureTransactions,
			PUF.TotalCustomers,
			PUB.BatchNo,
			--PUB.BatchDate,
			CONVERT(VARCHAR(24),PUB.BatchDate,106) AS BatchDate,
			PUB.Notes
	FROM Tbl_PaymentUploadFiles(NOLOCK) PUF
	JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId
	WHERE PUF.PUploadFileId = @PUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			PaymentFailureransactionID,
			AccountNo,
			AmountPaid,
			ReceiptNo,
			ReceivedDate,
			PaymentMode,
			Comments
		FROM Tbl_PaymentFailureTransactions(NOLOCK)
		WHERE PUploadFileId = @PUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterNoChangeLogsToApprove]    Script Date: 04/30/2015 14:04:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Meter NO Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 7 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.MeterInfoChangeLogId
		 ,(CD.AccountNo+' - '+  T.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,OldMeterNo
		 ,NewMeterNo
		 ,OldMeterReading
		 ,CONVERT(VARCHAR(20),CAST(NewMeterInitialReading AS NUMERIC)) AS NewMeterInitialReading
		 ,CONVERT(VARCHAR(20),MeterChangedDate,106) AS MeterChangedDate
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerMeterInfoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 04/30/2015 14:04:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
		AND T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
			
	SELECT 
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - CONVERT(DECIMAL(18,2),ISNULL(CustomerReadings.PresentReading,CustomerReadings.PreviousReading))) AS Usage
		,CB.CurrentReading AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(Case when CustomerReadings.IsBilled IS NULL then 0
			else CustomerReadings.IsBilled end) as IsBilled
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
											AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Billed'
					WHERE IsBilled = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
						
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = @PreviousReading
						,PresentReading = @PresentReading 
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 04/30/2015 14:04:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND PaidAmount = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNO) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
											AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,(SELECT BatchID FROM Tbl_BatchDetails(NOLOCK)
			WHERE BatchNo = (SELECT TOP(1) BatchNo FROM Tbl_PaymentUploadBatches(NOLOCK) PUB 
							WHERE PUB.PUploadBatchId = PUF.PUploadBatchId)) AS BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON ((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
						OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (CD.GlobalAccountNumber = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = CPD.BookNo
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				UPDATE B
				SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				FROM Tbl_BatchDetails B
				INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsToApprove]    Script Date: 04/30/2015 14:04:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 01-04-2015
-- Description: The purpose of this procedure is to get list of Tariff Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 3 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.TariffChangeRequestId
		,(CD.AccountNo+' - '+  T.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
				 ,CD.Service_Landmark    
				 ,CD.Service_City,'',    
				 CD.Service_ZipCode) As ServiceAddress
		 ,PreviousTariffId AS OldClassID
		 ,ChangeRequestedTariffId AS NewClassID
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=PreviousTariffId) AS PreviousTariffName
		 ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=ChangeRequestedTariffId) AS ChangeRequestedTariffName
		 ,T.OldClusterCategoryId AS OldClusterTypeId
		 ,T.NewClusterCategoryId AS NewClusterTypeId
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.OldClusterCategoryId) AS PreviousClusterName
		 ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = T.NewClusterCategoryId) AS ChangeRequestedClusterName
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_LCustomerTariffChangeRequest T 
	INNER JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogsToApprove]    Script Date: 04/30/2015 14:04:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 10 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.CustomerTypeChangeLogId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldCustomerTypeId
		 ,T.NewCustomerTypeId
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.OldCustomerTypeId) AS OldCustomerType
		 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId = T.NewCustomerTypeId) AS NewCustomerType
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerTypeChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerStatusChangeLogsToApprove]    Script Date: 04/30/2015 14:04:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04-04-2015
-- Description: The purpose of this procedure is to get list of Status Requested for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerStatusChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 9 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.AccountNo ASC) AS RowNumber
		 ,T.ActiveStatusChangeLogId
		 ,(CD.AccountNo+' - '+  T.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.OldStatus AS OldStatusId
		 ,T.NewStatus AS NewStatusId
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.OldStatus) AS OldStatus
		 ,(SELECT StatusName FROM Tbl_MCustomerStatus WHERE StatusId = T.NewStatus) AS NewStatus
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerActiveStatusChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.AccountNo AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]    Script Date: 04/30/2015 14:04:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 23-04-2015
-- Description: The purpose of this procedure is to get list of ReadToDirect Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerReadToDirectChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 12 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.ReadToDirectId
		 ,T.MeterNo
		  ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_ReadToDirectCustomerActivityLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsToApprove]    Script Date: 04/30/2015 14:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 8 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		 ,T.NameChangeLogId
		,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo AS OldAccountNumber  
		 --,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,dbo.fn_GetCustomerFullName_New(T.OldTitle,T.OldFirstName,T.OldMiddleName,T.OldLastName) AS [OldName]
		 ,dbo.fn_GetCustomerFullName_New(T.NewTitle,T.NewFirstName,T.NewMiddleName,T.NewLastName) AS [NewName]
		 ,OldKnownAs
		 ,NewKnownAs AS KnownAs
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerNameChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBookNoChangeLogsToApprove]    Script Date: 04/30/2015 14:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBookNoChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.BookNoChangeLogId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldPostal_HouseNo,T.OldPostal_StreetName,
			T.OldPostal_Landmark,T.OldPostal_City,T.OldPostal_AreaCode,T.OldPostal_ZipCode) AS OldPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewPostal_HouseNo,T.NewPostal_StreetName,
		    T.NewPostal_Landmark,T.NewPostal_City,T.NewPostal_AreaCode,T.NewPostal_ZipCode) AS NewPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldService_HouseNo,T.OldService_StreetName,
			 T.OldService_Landmark,T.OldService_City,T.OldService_AreaCode,T.OldService_ZipCode) AS OldServiceAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewService_HouseNo,T.NewService_StreetName,
		   T.NewService_Landmark,T.NewService_City,T.NewService_AreaCode,T.NewService_ZipCode) AS NewServiceAddress
 		 ,('BU : '+OldBN.BusinessUnitName +' ( '+OldBN.BUCode+ ' ) </br> SU : '+
		  OldBN.ServiceUnitName +' ( '+OldBN.SUCode+ ' ) </br> SC : '+
		  OldBN.ServiceCenterName +' ( '+OldBN.SCCode+ ' ) </br> Cycle : '+
		  OldBN.CycleName +' ( '+OldBN.CycleCode+ ' ) </br> Book : '+
		  OldBN.ID +' ( '+OldBN.BookCode+ ' )' ) AS OldBookNo
		  
		,('BU : '+NewBN.BusinessUnitName +' ( '+NewBN.BUCode+ ' ) </br> SU : '+
		  NewBN.ServiceUnitName +' ( '+NewBN.SUCode+ ' ) </br> SC : '+
		  NewBN.ServiceCenterName +' ( '+NewBN.SCCode+ ' ) </br> Cycle : '+
		  NewBN.CycleName +' ( '+NewBN.CycleCode+ ' ) </br> Book : '+
		  NewBN.ID +' ( '+NewBN.BookCode+ ' )' ) AS NewBookNo

		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_BookNoChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN UDV_BookNumberDetails OldBN ON OldBN.BookNo = T.OldBookNo
	INNER JOIN UDV_BookNumberDetails NewBN ON NewBN.BookNo = T.NewBookNo
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAssignMeterLogsToApprove]    Script Date: 04/30/2015 14:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAssignMeterLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		  ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNo ASC) AS RowNumber
		 ,T.AssignedMeterId
		 ,(CD.AccountNo+' - '+  T.GlobalAccountNo) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,T.MeterNo
		 ,CONVERT(VARCHAR(50),T.AssignedMeterDate,107) AS MeterAssignedDate
		 ,CAST(T.InitialReading AS INT) AS InitialReading
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
			THEN 1
			ELSE 0
			END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_AssignedMeterLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNo
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsToApprove]    Script Date: 04/30/2015 14:04:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: The purpose of this procedure is to get list of Type Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = 6 AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
		   ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		  ,T.AddressChangeLogId
		  ,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		  ,CD.OldAccountNo AS OldAccountNumber 
		  ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldPostal_HouseNo,T.OldPostal_StreetName,
			T.OldPostal_Landmark,T.OldPostal_City,T.OldPostal_AreaCode,T.OldPostal_ZipCode) AS OldPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewPostal_HouseNo,T.NewPostal_StreetName,
		    T.NewPostal_Landmark,T.NewPostal_City,T.NewPostal_AreaCode,T.NewPostal_ZipCode) AS NewPostalAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.OldService_HouseNo,T.OldService_StreetName,
			 T.OldService_Landmark,T.OldService_City,T.OldService_AreaCode,T.OldService_ZipCode) AS OldServiceAddress
		  ,dbo.fn_GetCustomerServiceAddress_New(T.NewService_HouseNo,T.NewService_StreetName,
		   T.NewService_Landmark,T.NewService_City,T.NewService_AreaCode,T.NewService_ZipCode) AS NewServiceAddress
		 ,ISNULL(T.Remarks,'--') As Details
		 ,(CASE WHEN T.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
			THEN 1
			ELSE 0
			END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
	FROM Tbl_CustomerAddressChangeLog_New T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND T.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    Script Date: 04/30/2015 14:04:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 06-10-2014   
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015      
-- Description: The purpose of this procedure is to get limited Meter change request logs    
-- =============================================       
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''    
      
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
    
	-- start By Karteek  
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek   
      
	SELECT(    
		 SELECT TOP 10 			
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' ) <br/>'+CMI.OldMeterReading) AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' ) <br/>'+ CONVERT(VARCHAR(10),CAST(ISNULL(NewMeterInitialReading,0) AS NUMERIC))) AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType    
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType    
			   ,CMI.Remarks
			   ,CMI.CreatedBy
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate     
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate     
			   ,ApproveSttus.ApprovalStatus    
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name    
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI    
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber    
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)    
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID     
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID  
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId  
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId  
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo  
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId  
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end     
		FOR XML PATH('AuditTrayList'),TYPE    
	)    
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
	
END   
---------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogs]    Script Date: 04/30/2015 14:04:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
	--SELECT(  
		 SELECT  --CMI.AccountNo
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' ) <br/>'+CMI.OldMeterReading) AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' ) <br/>'+ CONVERT(VARCHAR(10),CAST(ISNULL(NewMeterInitialReading,0) AS NUMERIC))) AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType  
			   ,CMI.Remarks  
			   ,CMI.CreatedBy
			   ,CustomerView.ClassName  
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CMI.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name 
			   ,CustomerView.OldAccountNo   
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end    
END  
-----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentEditListReport]    Script Date: 04/30/2015 14:04:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT 
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, TotalAmountEffected
		, MR.Name AS AdjustmentName
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@AdjustmentTypes,',')) TU ON TU.TypeId = CR.BillAdjustmentType 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,AdjustmentName
			,TotalRecords
			,BusinessUnitName 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteBatchPayment]    Script Date: 04/30/2015 14:05:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Faiz - ID103>  
-- Create date: <26-DEC-2014>  
-- Description: <The purpose of this procedure is to Delete BatchDetaisl (BatchPaymentStatus)>  
-- =============================================  
 
ALTER PROCEDURE [dbo].[USP_DeleteBatchPayment]   
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
 DECLARE	@BatchNo INT
			,@TotalPaidAmount DECIMAL=0 
			
	SELECT  @BatchNo=C.value('(BatchId)[1]','INT')      
	FROM	@XmlDoc.nodes('BatchStatusBE') AS T(C)     

	SET @TotalPaidAmount=ISNULL((	SELECT	SUM(PaidAmount) 
									FROM	Tbl_CustomerPayments 
									WHERE	BatchNo=@BatchNo 
									GROUP BY BatchNo),0)
	
	IF(@TotalPaidAmount=0)
	BEGIN
		DELETE Tbl_BatchDetails WHERE BatchID=@BatchNO 
	END
		
    SELECT @@ROWCOUNT AS RowsEffected 
    FOR XML PATH('BatchStatusBE')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingBatchProcesFailures]    Script Date: 04/30/2015 14:05:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetReadingBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
			--,@RUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	--SELECT @RUploadBatchId = RUploadBatchId FROM Tbl_ReadingUploadFiles WHERE RUploadFileId = @RUploadFileId

	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_ReadingUploadFiles RUF
	JOIN Tbl_ReadingUploadBatches RUB ON RUB.RUploadBatchId = RUF.RUploadBatchId
	WHERE RUF.RUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			CurrentReading,
			ReadingDate,
			Comments,
			ReadingFailureransactionID
		FROM Tbl_ReadingFailureTransactions
		WHERE RUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertReadingUploadTransactionDetails]    Script Date: 04/30/2015 14:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Faiz
-- Modified Date: 28-Apr-2015    
-- Description: The purpose of this procedure is to get file upload details No for Reading uploads
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertReadingUploadTransactionDetails]
(
	@BatchNo INT,            
	@UploadBatchId INT,          
	@CreatedBy varchar(50),  
	@TotalCustomers INT,
	@FilePath VARCHAR(MAX),
	@TempReadings TblMeterReadingsBulkUpload READONLY 
)      
AS            
BEGIN            

	DECLARE @IsSuccess BIT = 0
	DECLARE @UploladFileId INT
	    
	--IF EXISTS(SELECT 0 FROM Tbl_ReadingUploadBatches WHERE BatchNo = @BatchNo)
	--	BEGIN
			BEGIN TRY
				BEGIN TRAN           
					
					INSERT INTO Tbl_ReadingUploadFiles
					(
						 RUploadBatchId
						,FilePath
						,TotalCustomers
						,TotalSucessTransactions
						,TotalFailureTransactions
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @UploadBatchId
						,@FilePath
						,@TotalCustomers
						,0
						,0
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @UploladFileId = SCOPE_IDENTITY()
					
					INSERT INTO Tbl_ReadingTransactions
					(
						 RUploadFileId
						,SNO
						,AccountNo
						,CurrentReading
						,ReadingDate
						,CreatedBy
						,CreatedDate
					)
					SELECT
						 @UploladFileId
						,SNO
						,Account_Number
						,Current_Reading
						,Read_Date
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @TempReadings
					         
					SET @IsSuccess = 1 
					
				COMMIT TRAN	
			END TRY	   
			BEGIN CATCH
				ROLLBACK TRAN
				SET @IsSuccess = 0
			END CATCH		
	--	END
	--ELSE
	--	BEGIN
	--		SET @IsSuccess = 0
	--	END
	SELECT @IsSuccess AS IsSuccess
	 
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentEditListReport]    Script Date: 04/30/2015 14:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@TrxFrom = '')  
		BEGIN  
			SELECT @TrxFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50))   
					FROM Tbl_MPaymentType 
					WHERE ActiveStatusID = 1  
					FOR XML PATH(''), TYPE)  
					.value('.','NVARCHAR(MAX)'),1,1,'')  
		END  
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, PT.PaymentType AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS PaymentType FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.PaymentType = CR.PaymentType   
	INNER JOIN Tbl_MPaymentType PT ON PT.PaymentTypeID = CR.PaymentType 
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchPaymentStatus]    Script Date: 04/30/2015 14:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-DEC-2014>
-- Description:	<Retriving Batch Payment Status (Open/Close)>
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchPaymentStatus]
AS
	BEGIN
		SELECT   ROW_NUMBER() OVER(ORDER BY BD.BatchDate) AS RowNumber 
			,BD.BatchNo
			,BD.BatchID 
			,BD.BatchTotal
			,ISNULL(CONVERT(VARCHAR(50),BD.BatchDate,106),'--') AS BatchDate
			--,bd.BatchDate
			,ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0) AS PaidAmount
			--,(CASE 
			--	WHEN BD.BatchTotal<>(ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0) ) 
			--	THEN 'Open' 
			--	else 'Close' 
			--end) AS BatchStatus
			,BU_ID AS BUID
		FROM Tbl_BatchDetails AS BD
		WHERE BD.BatchTotal>(ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0))
		order by BD.BatchDate
	END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAverageUploadReport]    Script Date: 04/30/2015 14:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAverageUploadReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')		 		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SU.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	SELECT
	(
		SELECT RowNumber
			,CAST(AverageReading AS INT) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAllInfoXLMeterReading_WithDT]    Script Date: 04/30/2015 14:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteekk
-- Create date: 15 Apr 2015  
-- Description: <Get BillReading details from customerreading table TO EXCELL>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAllInfoXLMeterReading_WithDT]  
(
	@TempCustomer TblMeterRadingUploadedData READONLY
)  
AS  
BEGIN  
  
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.AccountNo
		,CMI.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CMI.Title,CMI.FirstName,CMI.MiddleName,CMI.LastName) as Name
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.Current_Reading
		,T.Read_Date
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		--,COUNT(0) AS CustomerCount
	INTO #CustomersListBook
	FROM @TempCustomer T
	INNER JOIN UDV_CustomerMeterInformation CMI ON (CMI.OldAccountNo = T.Account_Number OR CMI.GlobalAccountNumber = T.Account_Number)
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	--and CustomerReadingInner.IsBilled=0
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
		
		SELECT 
			 CB.GlobalAccountNumber AS AccNum
			,CB.AccountNo
			,CB.OldAccountNo
			,CB.Name
			,CB.MeterNumber AS MeterNo
			,CB.MeterDials
			,CB.Decimals
			,CustomerReadings.ReadingMultiplier
			,CB.ActiveStatusId
			,CONVERT(VARCHAR(20),CB.Read_Date,106) AS ReadDate
			,CB.ReadCodeID AS ReadCodeId
			,CB.BU_ID
			,CB.MarketerId
			,(CONVERT(DECIMAL(18,2),ISNULL(CB.Current_Reading,0)) - CONVERT(DECIMAL(18,2),ISNULL(CustomerReadings.PresentReading,CustomerReadings.PreviousReading))) AS Usage
			,CB.Current_Reading AS PresentReading
			,ISNULL(CustomerReadings.AverageReading,'0.00') AS AverageReading
			--,ISNULL(CustomerReadings.PresentReading,ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))) AS PreviousReading
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.Read_Date) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end) AS PreviousReading
			,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
			,CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106) AS LatestDate
			,(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.Read_Date) then 1  
				else 0 end) as PrvExist
			,(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.Read_Date) then 1 
				else 0 end) as IsExists
			,(Case when CustomerReadings.IsBilled IS NULL then 0
				else CustomerReadings.IsBilled end) as IsBilled
			--,CustomerCount
			,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
		FROM #CustomersListBook CB
		LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber
		 
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterReadingsReport]    Script Date: 04/30/2015 14:05:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX)   
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
   
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, UD.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, CD.GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) U ON U.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@TrxTypes,',')) TU ON TU.TypeId = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails UD ON UD.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
					AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
					
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(AverageReading AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress AS ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentEntryListByBatchNo]    Script Date: 04/30/2015 14:05:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  Neeraj Kanojiya        
-- Create date: 11-04-2014        
-- Modified date : 16-04-2014        
-- Description: The purpose of this procedure is to Get PaymentEntry list by Batch no        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetPaymentEntryListByBatchNo]         
(        
@XmlDoc xml        
)        
AS        
BEGIN        
	DECLARE @BatchNo INT        
		,@Flag INT    
		    
	SELECT        
		@BatchNo=C.value('(BatchId)[1]','INT'),        
		@Flag=C.value('(Flag)[1]','INT')        
	FROM @XmlDoc.nodes('MastersBE') AS T(C)  
	       
	IF(@Flag=1)        
		BEGIN        
			SELECT        
			(        
				SELECT         
					 CP.AccountNo,CP.CustomerPaymentID,cbp.BillNo
					,Convert(decimal(18,2), cbp.PaidAmount) AS BillTotal   
					,CP.PaidAmount,cbp.CustomerBillPaymentId as CustomerBPID        
				FROM Tbl_CustomerPaymentsApproval AS CP 
				LEFT JOIN Tbl_CustomerBillPaymentsApproval cbp ON CP.CustomerPaymentID = cbp.CustomerPaymentId       
				WHERE CP.BatchNo = @BatchNo AND CP.ActivestatusId = 1         
				ORDER BY CP.CreatedDate DESC        
				FOR XML PATH('MastersBE'),TYPE        
			)        
			FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
		END        
	IF(@Flag=2)        
		BEGIN        
			SELECT        
			(        
				SELECT         
					 CP.AccountNo,CP.CustomerPaymentID,cbp.BillNo
					,Convert(decimal(18,2), cbp.PaidAmount) AS BillTotal                  
					,CP.PaidAmount,cbp.CustomerBillPaymentId as CustomerBPID        
				FROM Tbl_CustomerPayments AS CP 
				LEFT JOIN Tbl_CustomerBillPayments cbp on CP.CustomerPaymentID = cbp.CustomerPaymentId        
				WHERE CP.BatchNo = @BatchNo AND CP.ActivestatusId = 1         
				ORDER BY CP.CreatedDate DESC        
				FOR XML PATH('MastersBE'),TYPE        
			)        
			FOR XML PATH(''),ROOT('MastersBEInfoByXml')        
		END         
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBatchDetails]    Script Date: 04/30/2015 14:05:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By : Neeraj Kanojiya     
-- Modified Date:14-03-2015    
-- Description: The purpose of this procedure is to get batch details by Batch No    
-- =============================================            
ALTER PROCEDURE [dbo].[USP_InsertBatchDetails](@XmlDoc XML)      
AS            
BEGIN            
	DECLARE  @BatchNo int,            
		 @BatchName varchar(50) ,            
		 @BatchDate datetime ,            
		 @CashOffice INT ,            
		 @CashierID VARCHAR(50) ,            
		 @CreatedBy varchar(50),            
		 @BatchTotal NUMERIC(20,4),            
		 @Description VARCHAR(MAX), 
		 @BU_ID VARCHAR(50),       
		 @Flag INT            
             
	SELECT             
		 @BatchNo=C.value('(BatchNo)[1]','int')            
		,@BatchName=C.value('(BatchName)[1]','varchar(50)')            
		,@BatchDate=C.value('(BatchDate)[1]','DATETIME')            
		,@CashOffice = C.value('(OfficeId)[1]','INT')            
		,@CashierID = C.value('(CashierId)[1]','VARCHAR(50)')             
		,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')            
		,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')            
		,@Description=C.value('(Description)[1]','VARCHAR(MAX)')            
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')            
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)         
 --TEMP INSERTING FOR APPROVAL           
	IF(@Flag=1)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetailsApproval WHERE BatchNo=@BatchNo AND BatchStatus = 1))            
				BEGIN            
					INSERT INTO Tbl_BatchDetailsApproval(            
					BatchNo,            
					BatchName,            
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,            
					[Description]
					)            
					VALUES(            
					@BatchNo,            
					Case  @BatchName when '' then null else @BatchName end,  
					@BatchDate,  
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					Case  @CreatedBy when '' then null else @CreatedBy end,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					CASE @Description WHEN '' THEN NULL ELSE @Description END
					)          
					SELECT 1 AS IsSuccess, @@IDENTITY AS BatchId  FOR XML PATH('BillingBE')            
				END            
			ELSE
				BEGIN            
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate,BatchName  
						,BatchNo  
						,BatchTotal  
						,BatchId
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0))   
					FROM Tbl_CustomerPaymentsApproval            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetailsApproval   
					WHERE BatchNo=@BatchNo   
					FOR XML PATH('BillingBE') 
				END 
		END      
	IF(@Flag=2)        
		BEGIN        
			IF(NOT EXISTS(SELECT BatchNo FROM Tbl_BatchDetails 
								WHERE BatchNo=@BatchNo 
								--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
								AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
								AND BU_ID=@BU_ID))            
				BEGIN            
					INSERT INTO Tbl_BatchDetails(            
					BatchNo,             
					BatchDate,            
					CashOffice,            
					CashierID,            
					CreatedBy,            
					CreatedDate,            
					BatchTotal,            
					BatchStatus,  
					BU_ID,   
					[Description]  
					)            
					VALUES(            
					@BatchNo,     
					@BatchDate,            
					Case  @CashOffice when 0 then null else @CashOffice end,            
					Case  @CashierID when '' then null else @CashierID end,             
					@CreatedBy,                
					dbo.fn_GetCurrentDateTime(),               
					@BatchTotal,            
					1,            
					@BU_ID,
					CASE @Description WHEN '' THEN NULL ELSE @Description END       
					)            
					SELECT 1 AS IsSuccess, @@IDENTITY AS BatchId  FOR XML PATH('BillingBE')            
				END            
			ELSE 
				BEGIN           
					SELECT 0 as IsSuccess  
						,BatchStatus AS BatchStatusId  
						,BatchDate  
						,BatchName  
						,BatchNo  
						,BatchTotal  
						,BatchId
						,(SELECT CONVERT(DECIMAL(20,2),BatchTotal - ISNULL(SUM(ISNULL(PaidAmount,0)),0)) FROM Tbl_CustomerPayments            
					WHERE ActivestatusId=1 AND BatchNo=@BatchNo) as BatchPendingAmount            
					FROM Tbl_BatchDetails   
					WHERE BatchNo=@BatchNo 
					--AND CONVERT(CHAR(4), BatchDate, 100) + CONVERT(CHAR(4), BatchDate, 120) =  CONVERT(CHAR(4),CONVERT( DATE,@BatchDate), 100) + CONVERT(CHAR(4), CONVERT( DATE,@BatchDate), 120) 
					AND CONVERT(DATE,BatchDate) = CONVERT(DATE,@BatchDate)
					AND BU_ID=@BU_ID
					FOR XML PATH('BillingBE') 
				END 
		END      
END  
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 04/30/2015 14:05:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for shown in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
  
  SELECT
	(  
		SELECT TOP 10
			 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/> Cycle : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br /> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br /> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br /> Cycle : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br /> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CONVERT(VARCHAR(50),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS LastTransactionDate
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
END  
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 04/30/2015 14:05:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
		SELECT
			 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/> Cycle : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br /> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br /> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br /> Cycle : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br /> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CONVERT(VARCHAR(50),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS LastTransactionDate
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,BD.BusinessUnitName
			,BD.ServiceUnitName
			,BD.ServiceCenterName
			,BD.CycleName
			,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
			,BD.BookSortOrder
			,'' AS CustomerSortOrder
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		--SELECT 
		--	 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
		--	--,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/>'+
		--	--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/>'+
		--	--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/>'+
		--	--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/>'+
		--	--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
		--	--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br/>'+
		--	--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br/>'+
		--	--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br/>'+
		--	--  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br/>'+
		--	--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
		--	,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		--   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

		--	,BookChange.Remarks
		--	,BookChange.CreatedBy
		--	,MTC.ClassName
		--	,CD.OldAccountNo
		--	,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		--	,ApproveSttus.ApprovalStatus  
		--	,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name
		--	,BD.BusinessUnitName
		--	,BD.ServiceUnitName
		--	,BD.ServiceCenterName
		--	,BD.CycleName
		--	,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
		--	,BD.BookSortOrder
		--	,BD.CustomerSortOrder
		--FROM Tbl_BookNoChangeLogs  BookChange  
		--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
		--AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		--INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		--INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		--INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		--INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		--INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		--INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsWithLimit]    Script Date: 04/30/2015 14:05:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid   
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo+' - '+CustomerView.GlobalAccountNumber) AS AccountNo  
			   ,CNL.CreatedBy  
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
				--,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName AS OldName  
			   --,CNL.NewFirstName AS [NewName]  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   ,CNL.OldKnownAs AS OldSurName  
			   ,CNL.NewKnownAs AS NewSurName  
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus
			   ,COUNT(0) OVER() AS TotalChanges  
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName			   
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId 
		 -- changed by karteek end      
		 ORDER BY CNL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogs]    Script Date: 04/30/2015 14:05:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
		 SELECT  
			   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
			   --,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName  
			   --,CNL.NewFirstName  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   ,CNL.OldKnownAs  AS OldSurName 
			   ,CNL.NewKnownAs  AS NewSurName 
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CNL.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
			   ,CustomerView.ClassName 
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId   
		 ORDER BY CNL.CreatedDate DESC
END 
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 04/30/2015 14:05:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
     
		 SELECT   --TCR.AccountNo  
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS LastTransactionDate
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 ORDER BY TCR.CreatedDate DESC
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsWithLimit]    Script Date: 04/30/2015 14:05:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014   
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
---- Description: The purpose of this procedure is to get limited Tariff change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			,CustomerView.OldAccountNo
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
			,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		    ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
			,TCR.Remarks
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS LastTransactionDate
			,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate
			,TCR.CreatedBy
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_LCustomerTariffChangeRequest  TCR  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId   
		ORDER BY TCR.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 04/30/2015 14:05:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CustomerView.ClassName
			   ,CustomerView.OldAccountNo
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 ORDER BY RD.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectLogs]    Script Date: 04/30/2015 14:05:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 ORDER BY RD.CreatedDate DESC
		 -- changed by karteek end 
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeWithLimit]    Script Date: 04/30/2015 14:05:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid

-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
			   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		       ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CC.Remarks  
			   ,CC.CreatedBy
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId 
		 -- changed by karteek end       
			ORDER BY CC.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogs]    Script Date: 04/30/2015 14:05:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	--SELECT(  
		 SELECT
		   (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate   
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,CC.Remarks  
		   ,CC.CreatedBy
		   ,CustomerView.ClassName
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder      
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 ORDER BY CC.CreatedBy DESC
		 -- changed by karteek end    
END
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogs]    Script Date: 04/30/2015 14:05:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
		 SELECT 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			   ,CCL.Remarks
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS LastTransactionDate
			   ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate
			   ,CONVERT(VARCHAR(30),CCL.ChangeDate,107) AS StatusChangedDate
			   ,ApproveSttus.ApprovalStatus  
			   ,CCL.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder
			   ,CustomerView.CycleName
		       ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder      
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		 ORDER BY CCL.CreatedDate DESC
		 -- changed by karteek end       
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit]    Script Date: 04/30/2015 14:05:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek     
     
	SELECT
	(  
		SELECT  TOP 10 
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			,CCL.Remarks
			,CCL.CreatedBy
			,CustomerView.OldAccountNo
			,CustomerView.ClassName
			,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS LastTransactionDate
			,CONVERT(VARCHAR(30),CCL.ChangeDate,107) AS StatusChangedDate
			,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		ORDER BY CCL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetConsumptionDelivered]    Script Date: 04/30/2015 14:05:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Get the Consumption Delivered records.
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetConsumptionDelivered]
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE	@PageNo INT  
		   ,@PageSize INT    
		   ,@ConsumptionMonth INT
		   ,@ConsumptionYear INT
		   ,@BU_ID VARCHAR(50)				
  SELECT     
		    @PageNo = C.value('(PageNo)[1]','INT')  
		   ,@PageSize = C.value('(PageSize)[1]','INT')   
		   ,@ConsumptionMonth = C.value('(ConsumptionMonth)[1]','INT')  
		   ,@ConsumptionYear = C.value('(ConsumptionYear)[1]','INT') 
		   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('ConsumptionDeliveredBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY CD.CreatedDate DESC) AS RowNumber  
										,ConsumptionDeliveredId
										,BusinessUnitName
										,ServiceUnitName
										,Capacity
										,SUPPMConsumption
										,SUCreditConsumption
										,TotalConsumption
										--,ConsumptionMonth
										--,ConsumptionYear
										,(dbo.fn_GetMonthName(ConsumptionMonth) +' - '+CONVERT(VARCHAR(20),ConsumptionYear)) AS ConsumptionMontYear
										FROM Tbl_ConsumptionDelivered CD
										Join Tbl_ServiceUnits SU ON SU.SU_ID=CD.SU_ID
										Join Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID 
										AND CD.BU_ID=@BU_ID OR @BU_ID=''
										Where CD.ConsumptionMonth = @ConsumptionMonth
											  AND CD.ConsumptionYear = @ConsumptionYear	
												--AND CD.BU_ID=@BU_ID OR @BU_ID=''
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('ConsumptionDeliveredBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('ConsumptionDeliveredBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersLog]    Script Date: 04/30/2015 14:05:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersLog]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 SELECT 				
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,(CAST(AM.InitialReading AS INT )) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks
			   ,CONVERT(VARCHAR(50),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS LastTransactionDate  
			   ,AM.CreatedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
			   ,CustomerView.BookSortOrder  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 ORDER BY AM.CreatedDate DESC
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersWithLimit]    Script Date: 04/30/2015 14:05:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,CAST(AM.InitialReading AS INT) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(50),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS LastTransactionDate  
			   ,ApproveSttus.ApprovalStatus
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName			     
			   ,AM.Remarks  
			   ,AM.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 ORDER BY AM.CreatedDate DESC
		 -- changed by karteek end       
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsWithLimit]    Script Date: 04/30/2015 14:05:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014 
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get limited Customer Address change logs  
-- Modified By : Bhimaraju V
-- Modified Date: 22-Apr-2015
--Desc : Getting Single field instead of all
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END  
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
   
  
   SELECT(  
		 SELECT TOP 10  
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
			 ,dbo.fn_GetCustomerServiceAddress_New(OldPostal_HouseNo,OldPostal_StreetName,
			 OldPostal_Landmark,OldPostal_City,OldPostal_AreaCode,OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewPostal_HouseNo,NewPostal_StreetName,
			 NewPostal_Landmark,NewPostal_City,NewPostal_AreaCode,NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(OldService_HouseNo,OldService_StreetName,
			 OldService_Landmark,OldService_City,OldService_AreaCode,OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewService_HouseNo,NewService_StreetName,
			 NewService_Landmark,NewService_City,NewService_AreaCode,NewService_ZipCode) AS NewServiceAddress			  
			 ,CA.Remarks
			 ,CustomerView.ClassName
			 ,CustomerView.OldAccountNo
			 ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS LastTransactionDate   
			 ,ApproveSttus.ApprovalStatus  
			 ,CA.CreatedBy  
			 ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			 ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_CustomerAddressChangeLog_New CA        
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId   
		 -- changed by karteek end    
		ORDER BY CA.CreatedDate DESC
		FOR XML PATH('AuditTrayAddressList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
END  
-------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogs]    Script Date: 04/30/2015 14:05:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By : Bhimaraju V
-- Modified Date: 22-Apr-2015
--Desc : Getting Single field instead of all
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END    
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
	SELECT(  
		SELECT    
			  (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
			 ,dbo.fn_GetCustomerServiceAddress_New(OldPostal_HouseNo,OldPostal_StreetName,
			 OldPostal_Landmark,OldPostal_City,OldPostal_AreaCode,OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewPostal_HouseNo,NewPostal_StreetName,
			 NewPostal_Landmark,NewPostal_City,NewPostal_AreaCode,NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(OldService_HouseNo,OldService_StreetName,
			 OldService_Landmark,OldService_City,OldService_AreaCode,OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewService_HouseNo,NewService_StreetName,
			 NewService_Landmark,NewService_City,NewService_AreaCode,NewService_ZipCode) AS NewServiceAddress			  
			 ,CA.Remarks  
			 ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS LastTransactionDate   
			 ,ApproveSttus.ApprovalStatus  
			 ,CA.CreatedBy
			 ,CustomerView.ClassName
			 ,CustomerView.BusinessUnitName
			 ,CustomerView.ServiceUnitName
			 ,CustomerView.ServiceCenterName
			 ,CustomerView.SortOrder AS CustomerSortOrder  
			 ,CustomerView.CycleName
		     ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
		     ,CustomerView.BookSortOrder     
			 ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		     ,CustomerView.OldAccountNo 
		 FROM Tbl_CustomerAddressChangeLog_New CA   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId     
		 ORDER BY CA.CreatedDate DESC
		 -- changed by karteek end  
		FOR XML PATH('AuditTrayAddressList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
    
END 
---------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTariffDetails_New]    Script Date: 04/30/2015 14:05:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTariffDetails_New]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
  
	DECLARE  
		 @AccountNo VARCHAR(50)   
		,@CreatedBy VARCHAR(50)  
		,@Remarks VARCHAR(50)  
		,@Flag INT  
		,@ApprovalStatusId INT  
		,@OldClassID INT  
		,@NewClassID INT  
		,@TariffChangeRequestId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@OldClusterTypeId INT  
		,@NewClusterTypeId INT 
		,@IsFinalApproval BIT = 0 

	SELECT   
		 @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@Flag = C.value('(Flag)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@OldClassID = C.value('(OldClassID)[1]','INT')  
		,@NewClassID = C.value('(NewClassID)[1]','INT')  
		,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@OldClusterTypeId = C.value('(OldClusterTypeId)[1]','INT')  
		,@NewClusterTypeId = C.value('(ClusterTypeId)[1]','INT') 
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT') 
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
   SET @AccountNo=(SELECT AccountNo FROM Tbl_LCustomerTariffChangeRequest 
					WHERE TariffChangeRequestId=@TariffChangeRequestId)
   
	IF(@Flag = 1)  -- Approval Permission Required
	BEGIN  
		IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo = @AccountNo AND ApprovalStatusId = 1)  
			BEGIN  -- Pending Request already exists
				SELECT 1 AS ApprovalProcess  
			END  
		ELSE  -- New Request
			BEGIN  
				DECLARE @RoleId INT--,@IsFinalApproval BIT=0
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
				SET @CurrentLevel = 0
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
					BEGIN
						SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
					END
				
				--SELECT 
				--	 @CurrentLevel = [Level] 
				--	,@IsFinalApproval = (CASE IsFinalApproval
				--						 WHEN 1 THEN (CASE WHEN UserIds IS NOT NULL 
				--											THEN (CASE WHEN EXISTS(SELECT 0 FROM dbo.fn_Split(UserIds,',') WHERE @CreatedBy = com) 
				--														THEN 1 END)
				--						 ELSE 1 END)
				--						 END)
				--FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId 
				--	AND	RoleId = @RoleId

				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

				INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					 AccountNo  
					,PreviousTariffId  
					,ChangeRequestedTariffId 
					,OldClusterCategoryId
					,NewClusterCategoryId 
					,CreatedBy  
					,CreatedDate  
					,Remarks  
					,ApprovalStatusId
					,PresentApprovalRole
					,NextApprovalRole)  
				VALUES(  
					 @AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@OldClusterTypeId
					,@NewClusterTypeId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,1 -- Process
					,@PresentRoleId
					,@NextRoleId)
					
				--SET @TariffChangeRequestId = SCOPE_IDENTITY()

				--IF(@IsFinalApproval = 1)
				--	BEGIN
				--		UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
				--		SET       
				--			 TariffClassID = @NewClassID
				--			,ClusterCategoryId = @NewClusterTypeId  
				--			,ModifedBy = @CreatedBy  
				--			,ModifiedDate = dbo.fn_GetCurrentDateTime()  
				--		WHERE GlobalAccountNumber = @AccountNo 
						
				--		INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				--			 TariffChangeRequestId
				--			,AccountNo  
				--			,PreviousTariffId  
				--			,ChangeRequestedTariffId 
				--			,OldClusterCategoryId
				--			,NewClusterCategoryId 
				--			,CreatedBy  
				--			,CreatedDate  
				--			,Remarks  
				--			,ApprovalStatusId
				--			,PresentApprovalRole
				--			,NextApprovalRole)  
				--		VALUES(  
				--			 @TariffChangeRequestId
				--			,@AccountNo  
				--			,@OldClassID  
				--			,@NewClassID  
				--			,@OldClusterTypeId
				--			,@NewClusterTypeId
				--			,@CreatedBy  
				--			,dbo.fn_GetCurrentDateTime()  
				--			,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				--			,@ApprovalStatusId
				--			,@PresentRoleId
				--			,@NextRoleId)
				--	END

				SELECT 1 As IsSuccess--,@IsFinalApproval AS IsFinalApproval  
				FOR XML PATH('TariffManagementBE'),TYPE		
			END  
		END  
	ELSE IF(@Flag = 2)  -- Tariff Approval Page
		BEGIN  
			IF(@ApprovalStatusId = 2)  -- Request Approved
				BEGIN  
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
						BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
											RoleId = (SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
													WHERE TariffChangeRequestId = @TariffChangeRequestId))

							SELECT @PresentRoleId = PresentRoleId,@NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)

							IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApprovalStatusId = @ApprovalStatusId
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId  

									UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
									SET       
										 TariffClassID = @NewClassID
										,ClusterCategoryId = @NewClusterTypeId  
										,ModifedBy = @ModifiedBy  
										,ModifiedDate = dbo.fn_GetCurrentDateTime()  
									WHERE GlobalAccountNumber = @AccountNo  
									
									INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
										 TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId  
										,ChangeRequestedTariffId 
										,OldClusterCategoryId
										,NewClusterCategoryId 
										,CreatedBy  
										,CreatedDate  
										,Remarks  
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole)  
									SELECT   
										 @TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId
										,ChangeRequestedTariffId  
										,OldClusterCategoryId
										,NewClusterCategoryId
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,Remarks 
										,ApprovalStatusId
										,@PresentRoleId
										,@NextRoleId
									FROM Tbl_LCustomerTariffChangeRequest
									WHERE AccountNo = @AccountNo   
									AND TariffChangeRequestId = @TariffChangeRequestId 
								END
							ELSE -- Not a final approval
								BEGIN
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId
										,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
									WHERE AccountNo = @AccountNo 
									AND TariffChangeRequestId = @TariffChangeRequestId
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							UPDATE Tbl_LCustomerTariffChangeRequest 
							SET  
								 ApprovalStatusId = @ApprovalStatusId   
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE AccountNo = @AccountNo  
							AND TariffChangeRequestId = @TariffChangeRequestId  

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET       
								 TariffClassID = @NewClassID  
								,ClusterCategoryId = @NewClusterTypeId 
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
								 TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId  
								,ChangeRequestedTariffId 
								,OldClusterCategoryId
								,NewClusterCategoryId 
								,CreatedBy  
								,CreatedDate  
								,Remarks  
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT   
								 @TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId
								,ChangeRequestedTariffId  
								,OldClusterCategoryId
								,NewClusterCategoryId
								,@ModifiedBy  
								,dbo.fn_GetCurrentDateTime()  
								,Remarks 
								,ApprovalStatusId
								,@PresentRoleId
								,@NextRoleId
							FROM Tbl_LCustomerTariffChangeRequest
							WHERE AccountNo = @AccountNo   
							AND TariffChangeRequestId = @TariffChangeRequestId 
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
			ELSE  
				BEGIN  -- Request Not Approved
					IF(@ApprovalStatusId = 3) -- Request is Rejected
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
								BEGIN -- Approval levels are there and status is rejected
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
															RoleId = (SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
																		WHERE TariffChangeRequestId = @TariffChangeRequestId))

									SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
									FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
								END
						END
					ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
								SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
								WHERE TariffChangeRequestId = @TariffChangeRequestId
						END

					-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
					UPDATE Tbl_LCustomerTariffChangeRequest SET 
						 ApprovalStatusId = @ApprovalStatusId   
						,ModifiedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId
						,Remarks = @Remarks
					WHERE AccountNo = @AccountNo   
					AND TariffChangeRequestId = @TariffChangeRequestId  

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
		END  
	ELSE  -- Approval Permission Not Required
		BEGIN  

			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
				 AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId
				,OldClusterCategoryId
				,NewClusterCategoryId
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId)  
			VALUES(  
				 @AccountNo  
				,@OldClassID  
				,@NewClassID
				,@OldClusterTypeId
				,@NewClusterTypeId  
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId) 
				
			SET @TariffChangeRequestId = SCOPE_IDENTITY()

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				 TariffClassID = @NewClassID
				,ClusterCategoryId = @NewClusterTypeId  
				,ModifedBy = @CreatedBy  
				,ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
			
			INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				 TariffChangeRequestId
				,AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId 
				,OldClusterCategoryId
				,NewClusterCategoryId 
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @TariffChangeRequestId
				,@AccountNo  
				,@OldClassID  
				,@NewClassID  
				,@OldClusterTypeId
				,@NewClusterTypeId
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,@ApprovalStatusId
				,@PresentRoleId
				,@NextRoleId)
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('TariffManagementBE'),TYPE  
		END  
		
END  
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 04/30/2015 14:05:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
        
  DECLARE @GlobalAccountNumber VARCHAR(50)  
 
	SET @GlobalAccountNumber =(SELECT GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail 
			WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo)

	IF( @GlobalAccountNumber IS NOT NULL)
		BEGIN
			IF ((SELECT [dbo].[fn_IsAccountNoExists_BU_Id](@GlobalAccountNumber,@BU_ID)) = 1)
				BEGIN
						SELECT  
						 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
						,ClassName  
						,OutStandingAmount AS DueAmount  --Commented By Raja-ID065
						,GlobalAccountNumber AS AccountNo
						,'success' AS StatusText
					FROM [UDV_CustomerDescription] 
					WHERE GlobalAccountNumber = @GlobalAccountNumber AND (BU_ID = @BU_ID OR @BU_ID = '')
					FOR XML PATH('MastersBE')  
				END
			ELSE
				BEGIN
					SELECT 'Customer doesn''t exist in selected Bussiness Unit' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
		END
	ELSE
		BEGIN
			SELECT 'Customer doesn''t exist' AS StatusText FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBU_New]    Script Date: 04/30/2015 14:05:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustExistsByBU_New]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	--SET	@AccountNo= '12500450'
	--SET @BUID='BEDC_BU_0003'   
	--SET	 @Flag=	 ''
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	
	SELECT @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
	FROM  UDV_IsCustomerExists WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
			IF @CustomerBusinessUnitID =  @BUID OR @BUID =''    
			BEGIN
				IF @Flag=1
				BEGIN
					--PRINT 'Sucess and No Need to check the ActiveStatusID of the Customers'
					SELECT 1 AS IsSuccess
					FOR XML PATH('RptCustomerLedgerBe')
				END
				ELSE
				BEGIN
					IF	 @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3 
						BEGIN
							--PRINT 'Sucess and Active  Customer'
							SELECT 1 AS IsSuccess
							FOR XML PATH('RptCustomerLedgerBe') 
						END
					ELSE
						BEGIN							 
							--PRINT 'Failure In Active Status'
							SELECT 0 AS IsSuccess
							FOR XML PATH('RptCustomerLedgerBe') 
						END
				END
			END
			ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU 
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CloseReadingUploadBatch]    Script Date: 04/30/2015 14:05:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 28-Apr-2015
-- Description:	This pupose of this procedure is to Close Reading Upload Batch record
-- =============================================
ALTER PROCEDURE [dbo].[USP_CloseReadingUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @RUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
		
	SELECT       
		 @RUploadBatchId = C.value('(RUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	UPDATE Tbl_ReadingUploadBatches 
	SET IsClosedBatch = 1
	WHERE RUploadBatchId = @RUploadBatchId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ReadingsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[IsCustExistsByBU_New]    Script Date: 04/30/2015 14:05:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================
ALTER PROCEDURE [dbo].[IsCustExistsByBU_New]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	--SET	@AccountNo= '12500450'
	--SET @BUID='BEDC_BU_0003'   
	--SET	 @Flag=	 ''
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	
	SELECT @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
	FROM  UDV_IsCustomerExists WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 1 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
			IF @CustomerBusinessUnitID =  @BUID OR @BUID =''    
			BEGIN
				IF @Flag=1
				BEGIN
					--PRINT 'Sucess and No Need to check the ActiveStatusID of the Customers'
					SELECT 1 AS IsSuccess
					FOR XML PATH('RptCustomerLedgerBe')
				END
				ELSE
				BEGIN
					IF	 @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3 
						BEGIN
							--PRINT 'Sucess and Active  Customer'
							SELECT 1 AS IsSuccess
							FOR XML PATH('RptCustomerLedgerBe') 
						END
					ELSE
						BEGIN							 
							--PRINT 'Failure In Active Status'
							SELECT 0 AS IsSuccess
							FOR XML PATH('RptCustomerLedgerBe') 
						END
				END
			END
			ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU 
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO


