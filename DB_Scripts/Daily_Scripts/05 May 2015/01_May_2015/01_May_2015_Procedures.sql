
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerPaymentsApproval]    Script Date: 05/01/2015 20:26:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 01-05-2015
-- Description: Update Payments change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerPaymentsApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@AccountNo VARCHAR(50) 
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@PaymentLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@ReceiptNumber VARCHAR(20)
		,@PaymentMode INT
		,@PaidAmount  DECIMAL(18,2)
		,@PadiDate DATETIME
		,@CustomerPaymentId INT
		,@PaymentFromId INT
		,@DocumentPath VARCHAR(MAX)
		,@Cashier VARCHAR(50)
		,@CashOffice INT
		,@DocumentName VARCHAR(MAX)
		,@PaymentType INt
		,@BatchNo INT
		

	SELECT  
		 @PaymentLogId = C.value('(PaymentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	SELECT			
		@GlobalAccountNo=GlobalAccountNumber
		,@AccountNo=AccountNo
		,@ReceiptNumber=ReceiptNumber
		,@PaymentMode=PaymentMode
		,@PaidAmount=PaidAmount
		,@PadiDate=PadiDate
		,@Remarks=Remarks
		,@PaymentFromId=PaymentFromId
		,@DocumentPath=DocumentPath
		,@Cashier=Cashier
		,@CashOffice=CashOffice
		,@DocumentName=DocumentName
		,@PaymentType=PaymentType
		,@BatchNo=BatchNo
	FROM Tbl_PaymentsLogs
	WHERE PaymentLogId=@PaymentLogId

	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_PaymentsLogs 
											WHERE PaymentLogId = @PaymentLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
					DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
					UPDATE Tbl_PaymentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND PaymentLogId = @PaymentLogId 
						
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @AccountNo
						,@ReceiptNumber
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PadiDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()		
					
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@GlobalAccountNo,@PaidAmount) 
					
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					UPDATE CB 
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]	
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					
					INSERT INTO Tbl_Audit_PaymentsLogs(	
						PaymentLogId
						,GlobalAccountNumber 
						,AccountNo 
						,ReceiptNumber 
						,PaymentMode  
						,PaidAmount 
						,PadiDate  
						,PresentApprovalRole  
						,NextApprovalRole  
						,ApproveStatusId  
						,Remarks 
						,CreatedBy 
						,CreatedDate
						,PaymentFromId)  
					VALUES(  
						 @PaymentLogId
						,@GlobalAccountNo
						,@AccountNo 
						,@ReceiptNumber 
						,@PaymentMode  
						,@PaidAmount 
						,@PadiDate  
						,@PresentRoleId  
						,@NextRoleId  
						,@ApprovalStatusId  
						,@Remarks 
						,@ModifiedBy 
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_PaymentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND PaymentLogId = @PaymentLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_PaymentsLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND PaymentLogId = @PaymentLogId 

					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @AccountNo
						,@ReceiptNumber
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PadiDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()					
				
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@GlobalAccountNo,@PaidAmount) 
					
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					UPDATE CB 
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]	
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					INSERT INTO Tbl_Audit_PaymentsLogs(	
						PaymentLogId
						,GlobalAccountNumber 
						,AccountNo 
						,ReceiptNumber 
						,PaymentMode  
						,PaidAmount 
						,PadiDate  
						,PresentApprovalRole  
						,NextApprovalRole  
						,ApproveStatusId  
						,Remarks 
						,CreatedBy 
						,CreatedDate
						,PaymentFromId)  
					VALUES(  
						 @PaymentLogId
						,@GlobalAccountNo
						,@AccountNo 
						,@ReceiptNumber 
						,@PaymentMode  
						,@PaidAmount 
						,@PadiDate  
						,@PresentRoleId  
						,@NextRoleId  
						,@ApprovalStatusId  
						,@Remarks 
						,@ModifiedBy 
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_PaymentsLogs 
																WHERE PaymentLogId = @PaymentLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_PaymentsLogs 
						WHERE PaymentLogId = @PaymentLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_PaymentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND PaymentLogId = @PaymentLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBookCustomersCount_ByCycles_New]    Script Date: 05/01/2015 20:26:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Karteek
-- Create date  : 01 May 2015
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetDisableBookCustomersCount_ByCycles_New]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE	@CycleId VARCHAR(MAX)
			,@BillMonth INT
			,@BillYear INT
			
	SELECT   		
		@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BillMonth = C.value('(BillingMonth)[1]','INT')
		,@BillYear = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
		SELECT 
			COUNT(GlobalAccountNumber) AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD
		INNER JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID AND CD.ActiveStatusId = 1
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CD.BookNo
			AND BN.CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN Tbl_BillingDisabledBooks DB ON DB.BookNo = BN.BookNo AND DB.IsActive = 1 
			AND DB.MonthId = @BillMonth AND DB.YearId = @BillYear
		FOR XML PATH('BillGenerationBe')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffDetails_ByCycles]    Script Date: 05/01/2015 20:26:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author  : Karteek 
-- Create date  : 01 MAY 2015  
-- Description  : To Get the Tariff wise customers and usage under the selected FeederId and Cycles  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetTariffDetails_ByCycles]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE   
          --@FeederId VARCHAR(50)  
    @CycleIds VARCHAR(MAX)  
   ,@Year INT  
   ,@Month INT  
     
 SELECT       
  --@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')  
   @CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@Year = C.value('(Year)[1]','INT')  
  ,@Month = C.value('(Month)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
   Declare @CustomerStatus varchar(max) = '1,2'
   
  
select CR.GlobalAccountNumber,SUM(isnull(Usage,0))as Usage
INTO #ReadCustomersList
from Tbl_CustomerReadings CR
INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))   
GROUP BY CR.GlobalAccountNumber,ClusterCategoryId


 SELECT  
  ( 
	  
		SELECT 
			ClassID AS TariffId,
			TC.ClassName AS TariffName
			--,SUM(case when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3 
			--		then (case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 
			--									else (case when isnull(BDB.DisableTypeId,2) =1 then 0 else 1 end) end)
			--		else 0 end) as [Read]
			--,SUM(case when (CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and (isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
			--			then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
			--			then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end ) as TotalDirectCustomers			
			,(SUM(case when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3 
					then (case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 
												else (case when isnull(BDB.DisableTypeId,2) =1 then 0 else 1 end) end)
					else 0 end)	 
				+
				SUM(case when (CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and (isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
						then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
						then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )) as TotalRecords
			--,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
			--	(case when isnull(BDB.IsPartialBill,1)=1 then 
			--	ISNULL(CR.Usage,CAD.AvgReading) else 0 end ) else 0 end) TotalUsageForReadCustomers
			--,SUM(isnull(DCAVG.AverageReading,isnull(CAD.AvgReading,0))) as TotalUsageForDirectCustomers
			--,SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) as TotalNonReadCustomersUsage	 
			--,SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) as NonUploadedCustomersUsage	 
			,(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
				(case when isnull(BDB.IsPartialBill,1)=1 then 
				ISNULL(CR.Usage,CAD.AvgReading) else 0 end ) else 0 end) 
				+
				SUM(isnull(DCAVG.AverageReading,isnull(CAD.AvgReading,0)))
				+
				SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end)
				+
				SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end)
			) as Usage
		from Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
		inner Join Tbl_MTariffClasses(NOLOCK) TC on CPD.TariffClassID=TC.ClassID	  
			and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
		INNER JOIN	CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo	  
			AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  
		Left JOIN Tbl_BillingDisabledBooks  BDB ON isnull(BDB.BookNo,0)=BN.BookNo and isnull(BDB.IsActive,0)=1 
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1	 
		Group By  TC.ClassID,TC.ClassName  
		ORDER BY   TC.ClassID
	FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')  
   
END  

GO


/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 05/01/2015 20:26:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT ISNULL(Temp.AverageReading,0) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,CD.GlobalAccountNumber AS AccNum
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
									AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
			when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
			else 0 end) as IsBUValidate 
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions WHERE AccountNo = (ISNULL(CD.GlobalAccountNumber,0)) GROUP BY AccountNo) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions AS Temp
	LEFT JOIN  CUSTOMERS.Tbl_CustomersDetail CD ON (CD.GlobalAccountNumber = Temp.AccountNo OR Temp.AccountNo = CD.OldAccountNo) 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the given AverageReading is valid or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AverageReading <= 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
					
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,AccNum
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #AverageReadings
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber  
			WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerPaymentsApproval]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 01-05-2015
-- Description: Update Payments change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerPaymentsApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@AccountNo VARCHAR(50) 
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@PaymentLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@ReceiptNumber VARCHAR(20)
		,@PaymentMode INT
		,@PaidAmount  DECIMAL(18,2)
		,@PadiDate DATETIME
		,@CustomerPaymentId INT
		,@PaymentFromId INT
		,@DocumentPath VARCHAR(MAX)
		,@Cashier VARCHAR(50)
		,@CashOffice INT
		,@DocumentName VARCHAR(MAX)
		,@PaymentType INt
		,@BatchNo INT
		

	SELECT  
		 @PaymentLogId = C.value('(PaymentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	SELECT			
		@GlobalAccountNo=GlobalAccountNumber
		,@AccountNo=AccountNo
		,@ReceiptNumber=ReceiptNumber
		,@PaymentMode=PaymentMode
		,@PaidAmount=PaidAmount
		,@PadiDate=PadiDate
		,@Remarks=Remarks
		,@PaymentFromId=PaymentFromId
		,@DocumentPath=DocumentPath
		,@Cashier=Cashier
		,@CashOffice=CashOffice
		,@DocumentName=DocumentName
		,@PaymentType=PaymentType
		,@BatchNo=BatchNo
	FROM Tbl_PaymentsLogs
	WHERE PaymentLogId=@PaymentLogId

	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_PaymentsLogs 
											WHERE PaymentLogId = @PaymentLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
				
					DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
					
					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
					UPDATE Tbl_PaymentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND PaymentLogId = @PaymentLogId 
						
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @AccountNo
						,@ReceiptNumber
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PadiDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()		
					
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@GlobalAccountNo,@PaidAmount) 
					
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					UPDATE CB 
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]	
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					
					INSERT INTO Tbl_Audit_PaymentsLogs(	
						PaymentLogId
						,GlobalAccountNumber 
						,AccountNo 
						,ReceiptNumber 
						,PaymentMode  
						,PaidAmount 
						,PadiDate  
						,PresentApprovalRole  
						,NextApprovalRole  
						,ApproveStatusId  
						,Remarks 
						,CreatedBy 
						,CreatedDate
						,PaymentFromId)  
					VALUES(  
						 @PaymentLogId
						,@GlobalAccountNo
						,@AccountNo 
						,@ReceiptNumber 
						,@PaymentMode  
						,@PaidAmount 
						,@PadiDate  
						,@PresentRoleId  
						,@NextRoleId  
						,@ApprovalStatusId  
						,@Remarks 
						,@ModifiedBy 
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_PaymentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND PaymentLogId = @PaymentLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_PaymentsLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND PaymentLogId = @PaymentLogId 

					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @AccountNo
						,@ReceiptNumber
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PadiDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()					
				
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@GlobalAccountNo,@PaidAmount) 
					
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					UPDATE CB 
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]	
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					INSERT INTO Tbl_Audit_PaymentsLogs(	
						PaymentLogId
						,GlobalAccountNumber 
						,AccountNo 
						,ReceiptNumber 
						,PaymentMode  
						,PaidAmount 
						,PadiDate  
						,PresentApprovalRole  
						,NextApprovalRole  
						,ApproveStatusId  
						,Remarks 
						,CreatedBy 
						,CreatedDate
						,PaymentFromId)  
					VALUES(  
						 @PaymentLogId
						,@GlobalAccountNo
						,@AccountNo 
						,@ReceiptNumber 
						,@PaymentMode  
						,@PaidAmount 
						,@PadiDate  
						,@PresentRoleId  
						,@NextRoleId  
						,@ApprovalStatusId  
						,@Remarks 
						,@ModifiedBy 
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_PaymentsLogs 
																WHERE PaymentLogId = @PaymentLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_PaymentsLogs 
						WHERE PaymentLogId = @PaymentLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_PaymentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND PaymentLogId = @PaymentLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBookCustomersCount_ByCycles_New]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Karteek
-- Create date  : 01 May 2015
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetDisableBookCustomersCount_ByCycles_New]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE	@CycleId VARCHAR(MAX)
			,@BillMonth INT
			,@BillYear INT
			
	SELECT   		
		@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BillMonth = C.value('(BillingMonth)[1]','INT')
		,@BillYear = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
		SELECT 
			COUNT(GlobalAccountNumber) AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD
		INNER JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID AND CD.ActiveStatusId = 1
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CD.BookNo
			AND BN.CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN Tbl_BillingDisabledBooks DB ON DB.BookNo = BN.BookNo AND DB.IsActive = 1 
			AND DB.MonthId = @BillMonth AND DB.YearId = @BillYear
		FOR XML PATH('BillGenerationBe')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails]  
(  
@XmlDoc XML  
)  
AS  
BEGIN  
 DECLARE @Months VARCHAR(MAX)  
   ,@Years VARCHAR(MAX)   
   ,@ReadTypeId INT  
   ,@Bu_Ids VARCHAR(MAX)  
   ,@Su_Ids VARCHAR(MAX) 
   ,@Cycles VARCHAR(MAX)  
   ,@TariffIds VARCHAR(MAX)  
   ,@PageNo INT    
   ,@PageSize INT        
   
 SELECT  
  @BU_IDs = C.value('(BusinessUnitName)[1]','VARCHAR(MAX)')  
  ,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
  ,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
  ,@Months = C.value('(MonthName)[1]','VARCHAR(MAX)')  
  ,@Years = C.value('(YearName)[1]','VARCHAR(MAX)')  
  ,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
  ,@PageNo = C.value('(PageNo)[1]','INT')    
  ,@PageSize = C.value('(PageSize)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') as T(C)  
   
   CREATE TABLE #CustomerBills(Sno INT PRIMARY KEY IDENTITY(1,1),BillNo VARCHAR(50))
   
   INSERT INTO #CustomerBills(BillNo)
		  SELECT   
			   BillNo  
		  FROM Tbl_CustomerBills CB   
		  WHERE BU_ID IN(SELECT [com] FROM dbo.fn_Split(@Bu_Ids,','))  
		  AND SU_ID IN(SELECT [com] FROM dbo.fn_Split(@Su_Ids,','))  
		  AND CycleId IN (SELECT [com] FROM dbo.fn_Split(@Cycles,','))  
		  AND TariffId IN(SELECT [com] FROM dbo.fn_Split(@TariffIds,','))  
		  AND BillYear IN (SELECT [com] FROM dbo.fn_Split(@Years,','))  
		  AND BillMonth IN (SELECT [com] FROM dbo.fn_Split(@Months,','))  
		  AND (ReadType = @ReadTypeId OR @ReadTypeId = 0)  
		  ORDER BY AccountNo ASC
 
		 DECLARE @Count INT=0
		 SELECT @Count=COUNT(0) FROM #CustomerBills
		 
		  SELECT   
			   Sno AS RowNumber    	      
			   ,AccountNo  
			   ,ServiceAddress  
			   ,TariffId  
			   ,ClassName AS TariffName  
			   ,CB.BillNo  
			   ,CONVERT(DECIMAL(18,2),TotalBillAmountWithTax) AS TotalBillAmountWithTax  
			   ,CONVERT(VARCHAR(20),BillGeneratedDate,106) AS ReadDate  
			   ,BillMonth  
			   ,BillYear   
			   ,BT.BillingType AS BillProcessType
			   ,@Count as TotalRecords 
			   ,MeterNo
			   ,NetEnergyCharges
			   ,NetFixedCharges
			   ,TotalBillAmount
			   ,NetArrears
			   ,VAT
			   ,VATPercentage
			   ,PaymentLastDate
			   ,PaidAmount
			   ,TotalBillAmountWithArrears
			   ,Usage
			   ,ReadCode
		  FROM Tbl_CustomerBills CB   
		  JOIN #CustomerBills LCB ON CB.BillNo COLLATE DATABASE_DEFAULT=LCB.BillNo COLLATE DATABASE_DEFAULT
		  LEFT JOIN Tbl_MBillingTypes BT ON CB.ReadType = BT.BillingTypeId  
		  LEFT JOIN Tbl_MTariffClasses T ON T.ClassID = CB.TariffId  
		  LEFT JOIN Tbl_MReadCodes RC ON CB.ReadCodeId = RC.ReadCodeId
		  ORDER BY Sno ASC
     
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillPreviousReading_NEW]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- Author:  <RamaDevi M>  
-- Create date: <25-MAR-2014>  
-- Description: <Get BillReading details from customerreading table>  
-- ModifiedBy:  Suresh Kumar D
-- ModifiedBy : T.Karthik
-- Modified date : 17-10-2014
-- Modified Date: 18-12-2014
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- ModifiedBy : T.Karthik
-- Modified date : 30-12-2014
-- ModifiedBy : SATYA
-- Modified date : 06-APRIL-2014
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillPreviousReading_NEW] (@XmlDoc xml)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

  
IF(@Type='account')  
	BEGIN	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @PrvExist int =1
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists int
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)

		  Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
		 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as IsExist 
		,ReadDate   as  LatestDate 
		,AverageReading 
		,TotalReadings  
		,PreviousReading
		,PresentReading 
		,IsBilled
		,MeterNumber   as  ReadingsMeterNumber
		,IsRollOver
		,GlobalAccountNumber
		,CustomerReadingId
		INTO #CustomerTopTwoReading
		from Tbl_CustomerReadings
		where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
		Order By CustomerReadingId DESC
		
		IF ((select COUNT(0) from #CustomerTopTwoReading where IsRollOver=1) > 1)
			BEGIN

				SELECT  
					@usage=Sum(Usage),
					@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
					@IsExists = max(case when IsExist=0 then 0 else 1 end)
					,@LatestDate=max(LatestDate) 
					,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
					,@TotalReadings=max(TotalReadings)  
					,@PreviousReading = max(PreviousReading)
					,@PresentReading=min(PresentReading) 
					,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
					,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
					,@IsRollOver =1 
				FROM #CustomerTopTwoReading  AS TopTwoReadings
				Group By GlobalAccountNumber

			END
		ELSE
			BEGIN
					 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
						@IsExists = IsExist
					,@LatestDate=LatestDate 
					,@AverageReading=AverageReading 
					,@TotalReadings=TotalReadings  
					,@PreviousReading = PreviousReading
					,@PresentReading=PresentReading 
					,@IsBilled=IsBilled
					,@ReadingsMeterNumber=ReadingsMeterNumber 
					,@IsRollOver =0
					from #CustomerTopTwoReading
			END
		
		DROP table		#CustomerTopTwoReading

		select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		SELECT TOP(1) @Month = BillMonth,
		@Year = BillYear ,@LastBillReadType= 
		(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		=CB.ReadCodeId)
		,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		DECLARE @IsLatestBill BIT = 0 	

		IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		BEGIN
			SET @IsLatestBill = 1
		END 
		ELSE
		  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
				 @AcountNumber AS AccountNo,
				 @OldAcountNumber AS OldAccountNo,
				 @Name   AS Name,
				 @usage as Usage
				 ,@RouteName   as RouteNum
				 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
				 ,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS 
				 IsActiveMonth
				 ,@IsTamper as IsTamper
				 ,case when @MeterNumber=@ReadingsMeterNumber then  @IsExists	else 0 End   as IsExists -- Check
				 ,convert(varchar(11),@LatestDate,105) as LatestDate
				 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
				 ,@MeterNumber as MeterNo
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
				 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
				 ,case when @IsExists =1 then 1 else 0 end as 	PrvExist 
				 ,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
				 ,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
				 ,@LastBillReadType as LastBillReadType
				 ,ISNULL(@InititalReading,0) as	 InititalReading
				 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
		)
		 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  
	END
ELSE IF(@Type = 'route')  
	BEGIN  
		CREATE TABLE #MeterReadRouteWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadRouteWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerDescription where RouteSequenceNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC

		--;WITH PagedResults AS  
				--  (  
			SELECT
			(
				  SELECT  
				   --C.CustomerUniqueNo AS CustomerUniqueNo 
				   OldAccountNo 
				  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
				  ,Sno AS RowNumber
				  ,(SELECT COUNT(0) FROM #MeterReadRouteWise) AS TotalRecords       
				  ,C.GlobalAccountNumber AS AccNum  
				  ,C.AccountNo AS  AccountNo
				  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
						WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
						AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
						ORDER BY CustomerBillId DESC) AS EstimatedBillDate
					,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
					,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
							THEN 1 ELSE 0 END) AS IsExists 
					,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
							THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber   COLLATE DATABASE_DEFAULT = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
							 ELSE '--' END) AS LatestDate  
					  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
					  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
					  ,C.MeterNumber AS MeterNo  
					  ,M.MeterDials AS MeterDials
					  ,R.IsTamper
					  ,M.Decimals AS Decimals  
					  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
							-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 
					  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
					  ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
					  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
					  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
						   IS NULL  
						   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
						   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR 
						   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
							 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
						  END) AS AverageReading  
					   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
					   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
								CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
								
					  -- ,CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
						 --CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					  -- IS NULL  
					  -- THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0))  
					  -- ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.CustomerUniqueNo=C.CustomerUniqueNo AND   
					  --CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END  
					  --AS PreviousReading  
					  --,dbo.fn_GetCustPrevReadingByReadDate(C.CustomerUniqueNo,@ReadDate) AS PreviousReading
					  
					 --,(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
						--WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						-- CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) order by CustomerReadingId desc) AS PresentReading    
					,CASE WHEN (SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) IS NULL THEN C.InitialReading ELSE R.Usage END AS PresentReading
					--,ISNULL((SELECT dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)),C.InitialReading) AS PresentReading	 
					  --,CASE WHEN R.PresentReading IS NULL THEN  CONVERT(VARCHAR(50),ISNULL(C.InitialReading,0)) ELSE R.PresentReading END AS PresentReading  
					  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
					  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
					  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
							WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
							CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
					  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
					  ,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
					  ,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
					  ,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
					FROM UDV_CustomerDescription C   
					JOIN #MeterReadRouteWise MRC ON C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =MRC.AccountNo     COLLATE DATABASE_DEFAULT 
					AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
					LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
					JOIN Tbl_MRoutes T On T.RouteId=C.RouteSequenceNo  
					left join Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
					and R.CustomerReadingId=(SELECT TOP 1 CustomerReadingId from Tbl_CustomerReadings   
					where GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate ) = CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					--where RouteNo = @AccNum  AND C.ReadCodeId = 2 AND M.MeterDials > 0
		  			AND M.MeterDials > 0
				  --)  
				FOR XML PATH('BillingBE'),TYPE
				)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  				  
	END  
 ELSE IF(@Type = 'BookNo')  
	BEGIN  
		CREATE TABLE #MeterReadBookWise(Sno INT IDENTITY(1,1),AccountNo VARCHAR(50))

		INSERT INTO #MeterReadBookWise
			SELECT GlobalAccountNumber
			FROM UDV_CustomerMeterInformation where BookNo = @AccNum  AND ReadCodeId = 2 
			AND (BU_ID=@BUID OR @BUID='') 
			AND ActiveStatusId=1 -- Only Active Customers
			ORDER BY CreatedDate ASC
		--;WITH PagedResults AS  
		--  (  
	SELECT(  
		  SELECT  
			--C.CustomerUniqueNo AS CustomerUniqueNo  
		  --, ROW_NUMBER() OVER(ORDER BY C.CustomerUniqueNo) AS RowNumber  
		  Sno AS RowNumber 
		  ,(SELECT COUNT(0) FROM #MeterReadBookWise) AS TotalRecords   
		  ,OldAccountNo
		  ,C.GlobalAccountNumber AS AccNum  
		  ,C.AccountNo AS AccountNo
		  ,(SELECT TOP (1) CONVERT(VARCHAR(50),BillGeneratedDate,106)	FROM Tbl_CustomerBills 
				WHERE CONVERT(DATE,BillGeneratedDate) >= CONVERT(DATE,@ReadDate) 
				AND AccountNo = C.GlobalAccountNumber AND ReadCodeId = 3
				ORDER BY CustomerBillId DESC) AS EstimatedBillDate
			,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, C.GlobalAccountNumber )) AS IsActiveMonth  
			,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerReadings CR 
			WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND CONVERT(DATE,CR.ReadDate) > CONVERT(DATE,@ReadDate))
					THEN 1 ELSE 0 END) AS IsExists 
			,(CASE WHEN EXISTS(SELECT 0 FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = C.GlobalAccountNumber)  
					THEN  CONVERT(VARCHAR(20),(SELECT TOP(1) CR.ReadDate FROM Tbl_CustomerReadings CR 
					WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  = C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT ORDER BY CR.ReadDate DESC),5)  
					 ELSE '--' END) AS LatestDate  
			  --,dbo.fn_GetCustomerFullName(C.GlobalAccountNumber) AS Name
			  ,dbo.fn_GetCustomerFullName_New(C.Title,C.FirstName,C.MiddleName,C.LastName) AS Name  
			  ,C.MeterNumber AS MeterNo  
			  ,M.MeterDials AS MeterDials
			  ,R.IsTamper
			  ,M.Decimals AS Decimals  
			  --,(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.AccountNumber = C.AccountNo AND   
					-- CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC) AS BillNo 			
			  ,dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate) AS BillNo
			 -- ,(CASE WHEN T.AvgMaxLimit is null THEN 0 ELSE T.AvgMaxLimit END) AS RouteNum  
			  ,(CASE WHEN (SELECT TOP(1) CR.PresentReading FROM Tbl_CustomerReadings CR 
			  WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate) order by CR.ReadDate desc)  
				   IS NULL  
				   THEN CONVERT(VARCHAR(50),ISNULL(C.AvgReading,0))  
				   ELSE (SELECT TOP(1) ISNULL(AverageReading,'0') FROM Tbl_CustomerReadings CR
				   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber   COLLATE DATABASE_DEFAULT AND   
					 CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.ReadDate desc)  
				  END) AS AverageReading  
			   ,ISNULL((SELECT TOP 1 CR.TotalReadings FROM Tbl_CustomerReadings CR 
			   WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber  COLLATE DATABASE_DEFAULT  AND   
						CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  ORDER BY CR.ReadDate DESC),0)AS TotalReadings  
			
			  ,dbo.fn_GetCustPresentReadingByReadDate(C.GlobalAccountNumber,@ReadDate,C.MeterNumber) AS PresentReading
			  ,(CASE WHEN R.PresentReading IS NULL THEN 0 ELSE 1 END) AS PrvExist  
			  ,CASE WHEN R.Usage IS NULL THEN 0 ELSE R.Usage END AS Usage  
			  ,(SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings CR
					WHERE CR.GlobalAccountNumber=C.GlobalAccountNumber AND   
					CONVERT(DATE,ReadDate )=CONVERT(DATE, @ReadDate)  ORDER BY CustomerReadingId DESC) AS IsBilled 
			  ,(SELECT dbo.fn_IsCustomerHaveLatestBill_ReadDate(C.GlobalAccountNumber, CONVERT(DATE,@ReadDate))) AS IsLatestBill 
			,dbo.fn_GetCurrentReading_ByAdjustments(dbo.fn_GetCustBillNoByReadDate(C.GlobalAccountNumber,@ReadDate)
			,dbo.fn_GetCustPrevReadingByReadDate_New(C.GlobalAccountNumber,@ReadDate,C.MeterNumber)) AS PreviousReading
			,dbo.fn_LastBillGeneratedReadType(C.GlobalAccountNumber) AS LastBillReadType
			FROM UDV_CustomerDescription C   
			JOIN #MeterReadBookWise MBC ON C.GlobalAccountNumber COLLATE DATABASE_DEFAULT =MBC.AccountNo   COLLATE DATABASE_DEFAULT
			AND Sno between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			LEFT JOIN Tbl_MeterInformation M ON M.MeterNo=C.MeterNumber  
		--	JOIN Tbl_MRoutes T On T.RouteId=C.RouteNo  
			LEFT JOIN Tbl_CustomerReadings R on R.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =C.GlobalAccountNumber    COLLATE DATABASE_DEFAULT 
			AND R.CustomerReadingId = (SELECT TOP 1 CustomerReadingId FROM Tbl_CustomerReadings   
									WHERE GlobalAccountNumber = C.GlobalAccountNumber AND   
									CONVERT(DATE,ReadDate) = CONVERT(DATE, @ReadDate) 
									ORDER BY CustomerReadingId DESC)  
			--WHERE BookNo = @AccNum  AND C.ReadCodeId = 2 
			AND M.MeterDials > 0 --- Here @AccNum Variable having the BookNo
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	END   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails_New]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails_New]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) = 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) = 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) = 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) = '2,3,4,5,7,8,9'
		,@PageNo INT = 1
		,@PageSize INT = 100  
		
	SELECT  
		 @BU_IDs = C.value('(BusinessUnitName)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(MonthName)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(YearName)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,CB.AccountNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
		,CB.ServiceAddress
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  
	INNER JOIN Tbl_MBillingTypes BT ON CB.ReadType = BT.BillingTypeId AND
	(CB.ReadType = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN Tbl_MTariffClasses T ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY AccountNo ASC


	SELECT   
		 RowNumber   	      
		,BillNo  
		,ServiceAddress  
		,TariffId  
		,TariffName  
		,TotalBillAmountWithTax  
		,ReadDate  
		,BillMonth  
		,BillYear   
		,BillProcessType
		,TotalRecords 
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
---------------------------------------------------------------------------------------------------
 

select  AccountNo  
,MeterNo,
ServiceAddress,
NetEnergyCharges,
NetFixedCharges,
TotalBillAmount,
TotalBillAmountWithTax,
NetArrears,
VAT,
VATPercentage,
PaymentLastDate,
PaidAmount,
TotalBillAmountWithArrears
,Usage,
TotalBillAmountWithTax,
ReadCodeId
from Tbl_CustomerBills
GlobalAccountNumber,MeterNU

 
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
			
	SELECT 
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END)) AS Usage
		,CB.CurrentReading AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(Case when CustomerReadings.IsBilled IS NULL then 0
			else CustomerReadings.IsBilled end) as IsBilled
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
											AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Billed'
					WHERE IsBilled = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
						
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = @PreviousReading
						,PresentReading = @PresentReading 
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
    SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo
	ORDER BY CustomerPaymentID DESC
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		,ReadType
		,Usage AS Consumption
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 
		,CD.GlobalAccountNumber AS AccountNo
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
											,CD.Service_Landmark    
											,CD.Service_City,''    
											,CD.Service_ZipCode) AS ServiceAddress
		,CAD.OutStandingAmount AS TotalDueAmount
		,Dials AS MeterDials
		,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal  
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,COUNT(0) OVER() AS TotalRecords
	FROM Tbl_CustomerBills CB
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber
			AND CD.GlobalAccountNumber = @AccountNo	AND ISNULL(CB.PaymentStatusID,2) = 2
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo 
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffDetails_ByCycles]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author  : Karteek 
-- Create date  : 01 MAY 2015  
-- Description  : To Get the Tariff wise customers and usage under the selected FeederId and Cycles  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffDetails_ByCycles]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE   
          --@FeederId VARCHAR(50)  
    @CycleIds VARCHAR(MAX)  
   ,@Year INT  
   ,@Month INT  
     
 SELECT       
  --@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')  
   @CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@Year = C.value('(Year)[1]','INT')  
  ,@Month = C.value('(Month)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
   Declare @CustomerStatus varchar(max) = '1,2'
   
  
select CR.GlobalAccountNumber,SUM(isnull(Usage,0))as Usage
INTO #ReadCustomersList
from Tbl_CustomerReadings CR
INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))   
GROUP BY CR.GlobalAccountNumber,ClusterCategoryId


 SELECT  
  ( 
	  
		SELECT 
			ClassID AS TariffId,
			TC.ClassName AS TariffName
			--,SUM(case when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3 
			--		then (case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 
			--									else (case when isnull(BDB.DisableTypeId,2) =1 then 0 else 1 end) end)
			--		else 0 end) as [Read]
			--,SUM(case when (CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and (isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
			--			then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
			--			then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end ) as TotalDirectCustomers			
			,(SUM(case when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3 
					then (case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 
												else (case when isnull(BDB.DisableTypeId,2) =1 then 0 else 1 end) end)
					else 0 end)	 
				+
				SUM(case when (CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and (isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
						then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
						then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )) as TotalRecords
			--,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
			--	(case when isnull(BDB.IsPartialBill,1)=1 then 
			--	ISNULL(CR.Usage,CAD.AvgReading) else 0 end ) else 0 end) TotalUsageForReadCustomers
			--,SUM(isnull(DCAVG.AverageReading,isnull(CAD.AvgReading,0))) as TotalUsageForDirectCustomers
			--,SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) as TotalNonReadCustomersUsage	 
			--,SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) as NonUploadedCustomersUsage	 
			,(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
				(case when isnull(BDB.IsPartialBill,1)=1 then 
				ISNULL(CR.Usage,CAD.AvgReading) else 0 end ) else 0 end) 
				+
				SUM(isnull(DCAVG.AverageReading,isnull(CAD.AvgReading,0)))
				+
				SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end)
				+
				SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end)
			) as Usage
		from Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
		inner Join Tbl_MTariffClasses(NOLOCK) TC on CPD.TariffClassID=TC.ClassID	  
			and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
		INNER JOIN	CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo	  
			AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  
		Left JOIN Tbl_BillingDisabledBooks  BDB ON isnull(BDB.BookNo,0)=BN.BookNo and isnull(BDB.IsActive,0)=1 
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1	 
		Group By  TC.ClassID,TC.ClassName  
		ORDER BY   TC.ClassID
	FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')  
   
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 05/01/2015 20:30:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT ISNULL(Temp.AverageReading,0) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,CD.GlobalAccountNumber AS AccNum
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
									AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
			when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
			else 0 end) as IsBUValidate 
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions WHERE AccountNo = (ISNULL(CD.GlobalAccountNumber,0)) GROUP BY AccountNo) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions AS Temp
	LEFT JOIN  CUSTOMERS.Tbl_CustomersDetail CD ON (CD.GlobalAccountNumber = Temp.AccountNo OR Temp.AccountNo = CD.OldAccountNo) 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the given AverageReading is valid or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AverageReading <= 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
					
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,AccNum
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #AverageReadings
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber  
			WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalFunctions]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 27-11-2014
-- Description:	The purpose of this procedure is to get approval functions
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalFunctions]
AS
BEGIN
	SELECT
	(
		SELECT
			[Function]
			,FunctionId
		FROM TBL_FunctionalAccessPermission
		WHERE IsActive=1
		ORDER BY [Function] ASC
		FOR XML PATH('ApprovalsList'),TYPE
	)
	FOR XML PATH(''),ROOT('ApprovalsInfoByXml')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAverageBatchProcesFailures]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : Karteek
-- Modified Date:30-04-2015 
-- Description:	This pupose of this procedure is to get Average Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerAverageBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_AverageUploadFiles(NOLOCK) RUF
	JOIN Tbl_AverageUploadBatches(NOLOCK) RUB ON RUB.AvgUploadBatchId = RUF.AvgUploadBatchId
	WHERE RUF.AvgUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			AverageReading,
			Comments,
			AvgFailureransactionID
		FROM Tbl_AverageFailureTransactions(NOLOCK)
		WHERE AvgUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAverageUploadBatches]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 30-Apr-2015
-- Description:	This pupose of this procedure is to get Average Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerAverageUploadBatches] 
(
	@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY RUB.IsClosedBatch ASC , RUB.CreatedDate DESC ) AS RowNumber,  
				RUB.AvgUploadBatchId,
				RUB.BatchNo,
				CONVERT(VARCHAR(20),RUB.BatchDate,106) AS StrBatchDate,
				RUF.TotalCustomers,
				RUB.Notes,
				RUF.TotalSucessTransactions,
				RUF.TotalFailureTransactions,
				RUF.AvgUploadFileId,
				RUF.FilePath, --Faiz-ID103
				CONVERT(VARCHAR(20),RUF.CreatedDate,106) AS StrCreatedDate --Faiz-ID103
		FROM Tbl_AverageUploadBatches(NOLOCK) RUB
		JOIN Tbl_AverageUploadFiles(NOLOCK) RUF ON RUF.AvgUploadBatchId = RUB.AvgUploadBatchId
		WHERE RUB.IsClosedBatch = 0 AND (RUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('ReadingsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('ReadingsBatchBEInfoByXml')     
	
END

GO


/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
		
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0)) as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	  
	INNER JOIN Tbl_Cycles Cycles On	Cycles.ServiceCenterId=@ServiceCenterID and Cycles.CycleId=BN.CycleId
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT
	(
		SELECT ClassID
			,
			SUM(
					case 
					when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
					then

					case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 else
																		case 
																		when  
																		isnull(BDB.DisableTypeId,2) =1 
																		then 0  
																		else 1 
																		end
					end
					else 
					0
					end
					)
			
			as TotalReadCustomers
			,
			SUM ( case 
		    when 
			(CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and
			(isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
			then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
			then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )  
			 as EstimatedCustomers
			,TC.ClassName
			,CC.CategoryName
			,CC.ClusterCategoryId 
			,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
			,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
			,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
			,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
			,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
			,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
			,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
			,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
			,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
			,Cycles.CycleId 
			,Cycles.CycleName
		FROM Tbl_MTariffClasses(NOLOCK)	TC
		INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
		INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN   MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
		INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
		LEFT JOIN Tbl_BillingDisabledBooks BDB
		ON  BDB.BookNo=BN.BookNo and IsActive=1
		LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
				and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
				And ES.BillingMonth = @Month -- Need To Modify	 
				AND ES.BillingYear = @Year	 -- Need To Modify
		Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
		ORDER BY Cycles.CycleId,CC.ClusterCategoryId
		FOR XML PATH('EstimationDetails'),TYPE
	)
	,
	(
		SELECT	
			 @Capacity as CapacityInKVA
			,@SUPPMConsumption as SUPPMConsumption
			,@SUCreditConsumption as SUCreditConsumption
		FOR XML PATH('EstimatedUsageDetails'),TYPE 
	)
	FOR XML PATH(''),ROOT('EstimationInfoByXml')
	
	DROP TABLE #ReadCustomersList
	
END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDirectCustomerAverageUpload]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @GlobalAccountNumber VARCHAR(50)
			,@AccountNumber VARCHAR(50)
			,@AverageReading DECIMAL(18,2)
			,@ModifiedBy VARCHAR(50)
			,@ApprovalStatusId INT 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)  
	
	SELECT  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
			,@AccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@AverageReading = C.value('(AverageReading)[1]','DECIMAL(18,2)') 
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)') 
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
	FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	IF((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApproveStatusId IN (1,4)) = 0) 
	BEGIN
		DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
				BEGIN
					SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
	
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1)
			
			INSERT INTO Tbl_DirectCustomersAverageChangeLogs
			(GlobalAccountNumber
			,AccountNo
			,PreviousAverage
			,NewAverage
			,PresentApprovalRole
			,NextApprovalRole
			,ApproveStatusId
			,Remarks
			,CreatedBy
			,CreatedDate)
			
			SELECT @GlobalAccountNumber
						,@AccountNumber
						,ISNULL((SELECT AverageReading FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNumber),0)
						,@AverageReading
						,@PresentRoleId
						,@NextRoleId
						,@ApprovalStatusId
						,@Details
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						
			IF(@IsFinalApproval = 1)
				BEGIN
				IF EXISTS (SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
				BEGIN
					--Print 'Avg reading for customer exiting- update the avg reading'
					Update Tbl_DirectCustomersAvgReadings
					SET AverageReading=@AverageReading
						,ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber=@GlobalAccountNumber
				END
				ELSE
				BEGIN
					--Print 'Avg reading for customer Not exiting- Insert the avg reading'
					Insert Into Tbl_DirectCustomersAvgReadings(
								GlobalAccountNumber
								,AverageReading
								,ActiveStatusId
								,CreatedBy
								,CreatedDate
								)
					Values		(@GlobalAccountNumber
								,@AverageReading
								,1
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								)
				END
				END
				SELECT 1 AS IsSuccess FOR XML PATH('DirectCustomerAverageUploadBE')  
	END
	ELSE
	BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('DirectCustomerAverageUploadBE')  
	END
	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNonReadCustomers]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''
			 

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

SELECT CD.GlobalAccountNumber,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,CD.InitialBillingKWh AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON CD.ServiceCenterId =Service_Centers.SC_ID
	AND ActiveStatusId=1  and isnull(IsPartialBook,1)= 1
	and CustomerTypeId <>3
	AND CD.ReadCodeID = 2 
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON
	CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE   CR.CustomerReadingId IS NULL 
	
END
--------------------------------------------------------------------------------------------






GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNoBillsCustomers]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- PrepaidCustomers
	SET	@BookDisableType = '1'

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
	SELECT   
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress AS ServiceAddress
		,
		CASE
		when   CustomerStatus.CustomerStatusID=3 then 'Hold Customer' else
		case when CustomerStatus.CustomerStatusID=4 then'Closed Customer' 
		else 'Pre-Paid Customer'
		end
		END
		AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO  #NoBillCustomersList
	FROM  UDV_PrebillingRpt(NOLOCK) CD	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
 	INNER JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus 
 	ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
	LEFT JOIN Tbl_MeterInformation(NOLOCK) MI ON CD.MeterNumber = MI.MeterNo and MI.MeterType = 1
	WHERE
	  (CustomerStatus.CustomerStatusID IS NUll OR  MI.MeterType IS NULL)
	
	insert into #NoBillCustomersList 
	SELECT  
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress  AS ServiceAddress
		,'BookDisable No Power' AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
 
	FROM UDV_PrebillingRpt(NOLOCK) CD
	INNER JOIN Tbl_BillingDisabledBooks(NOLOCK) BD ON ISNULL(CD.BookNo,0) = BD.BookNo
	AND BD.DisableTypeId IN(SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,','))
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID	--INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId     
  SELECT *		 FROm #NoBillCustomersList
END
--------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetReadCustomers]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

Modified Date : 09-04-2015

Description : 
In Active,Closed,Hold,Tempary CLose  Customers ,DIRECT Customer , NoPower Also- No Need to Come

 Hold -- 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

 
	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
			,@ActiveStatusIds varchar(max)='2,3,4'
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS BIGINT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings(NOLOCK) CR
	INNER JOIN UDV_PreBillingRpt(NOLOCK) CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	AND ActiveStatusId=1  and isnull(IsPartialBook,1)= 1-- Disable books and	   Active Customers
	and CD.CustomerTypeId <>3
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID 
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy
    and ISNULL(CD.IsPartialBook,1)=1

	SELECT Tem.GlobalAccountNumber,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,MIN(Tem.PreviousReading) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,MAX(Tem.PresentReading) AS	 PresentReading
		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	
	DROP TABLE #ReadCustomersList		 

END
----------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingUploadBatches]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 28-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetReadingUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY RUB.IsClosedBatch ASC , RUB.CreatedDate DESC ) AS RowNumber,  
				RUB.RUploadBatchId,
				RUB.BatchNo,
				CONVERT(VARCHAR(20),RUB.BatchDate,106) AS StrBatchDate,
				--CONVERT(VARCHAR(24),RUB.CreatedDate,106) AS BatchDate,
				RUF.TotalCustomers,
				RUB.Notes,
				RUF.TotalSucessTransactions,
				RUF.TotalFailureTransactions,
				RUF.RUploadFileId,
				RUF.FilePath, --Faiz-ID103
				CONVERT(VARCHAR(20),RUF.CreatedDate,106) AS StrCreatedDate --Faiz-ID103
		FROM Tbl_ReadingUploadBatches RUB
		JOIN Tbl_ReadingUploadFiles RUF ON RUF.RUploadBatchId = RUB.RUploadBatchId
		WHERE RUB.IsClosedBatch = 0 AND (RUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('ReadingsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('ReadingsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgUploadedCustoemrs]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,ROUND(DAVG.AverageReading,0) AS Usage
	FROM
	UDV_PrebillingRpt CD(NOLOCK)
	INNER JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG 
	ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	and  CD.ReadCodeID = 1 
	AND CD.ActiveStatusId=1  and isnull(CD.IsPartialBook,0)=0 
	-- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------




GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentUploadBatches]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Upload Batches Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentUploadBatches] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT    
			,@PageSize INT 
			,@CreatedBy VARCHAR(50)   
		
	SELECT       
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	;WITH PagedResults AS    
	(    
		SELECT 
				ROW_NUMBER() OVER(ORDER BY PUB.IsClosedBatch ASC , PUB.CreatedDate DESC ) AS RowNumber,  
				PUB.PUploadBatchId,
				PUB.BatchNo,
				CONVERT(VARCHAR(20),PUB.BatchDate,106) AS StrBatchDate,
				--CONVERT(VARCHAR(24),PUB.CreatedDate,106) AS BatchDate,
				PUF.TotalCustomers,
				PUB.Notes,
				PUF.TotalSucessTransactions,
				PUF.TotalFailureTransactions,
				PUF.PUploadFileId,
				PUF.FilePath, --Faiz-ID103
				CONVERT(VARCHAR(20),PUF.CreatedDate,106) AS StrCreatedDate --Faiz-ID103
		FROM Tbl_PaymentUploadBatches(NOLOCK) PUB
		JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadBatchId = PUB.PUploadBatchId
		WHERE PUB.IsClosedBatch = 0 AND (PUB.CreatedBy = @CreatedBy OR LastModifyBy = @CreatedBy)
	)    
      
	SELECT     
    (    
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize    
		FOR XML PATH('PaymentsBatchBE'),TYPE    
	)    
	FOR XML PATH(''),ROOT('PaymentsBatchBEInfoByXml')     
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetPartialBillCustomers]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(max)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(max)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(max)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(max)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
 	Create table #PartialBillCustomers 
 	(
 	 GlobalAccountNumber varchar(50)
 	,OldAccountNo varchar(50)
 	,MeterNo    varchar(50)
 	,ClassName  varchar(50)
 	,Name    varchar(MAX)
 	,ServiceAddress	 varchar(MAX)
 	,BusinessUnitName  varchar(200)
 	,ServiceUnitName	varchar(200)   
 	,ServiceCenterName varchar(200)
 	,CustomerSortOrder int
 	,CycleName	  varchar(100)
 	,BookNumber	 varchar(100)    
 	,BookSortOrder int 
 	,Comments varchar(MAX)   
 	)
 	
 insert into #PartialBillCustomers
	SELECT	    
		CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		 ,CustomerFullName AS Name 
	 	,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
	 	,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt(NOLOCK) CD
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
 	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	
	
	 
	insert into #PartialBillCustomers
	SELECT 
		 CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
	 
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 
	AND BD.IsPartialBill = 1
 
	
	SELECT * from #PartialBillCustomers
	
	DROP table #PartialBillCustomers
END
---------------------------------------------------------------------------------------------




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT Top 1 CBILLS.AccountNo,CBILLS.AverageReading as  LastBillAverage,CBILLS.Usage, 
	CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
		 INTO #CustomerBillswithAverage
		 from   Tbl_CustomerBills(NOLOCK)	CBILLS
	INNER JOIN  UDV_PreBillingRpt(NOLOCK) CD ON CBILLS.AccountNo = CD.GlobalAccountNumber 
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
		
	
	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.Name 
		,CD.ServiceAddress AS ServiceAddress
		,MIN(CR.PreviousReading) AS PreviousReading 
		,MAX (CR.ReadDate) AS PreviousReadDate
		,MAX(CR.PresentReading) AS	 PresentReading
		,CAST(SUM(CR.usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(CR.CreatedDate) AS LatestTransactionDate
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CustomerSortOrder 
		,CD.CycleName
		,BookNumber
		,CD.BookSortOrder 
	FROM Tbl_CustomerReadings CR
	INNER JOIN #CustomerBillswithAverage CD ON	   CD.GlobalAccountNumber=CR.GlobalAccountNumber
	GROUP BY CD.GlobalAccountNumber,CD.OldAccountNo,
		CD.MeterNumber,CD.ClassName,
		CD.Name,
		CD.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CD.BookSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
		, CD.Usage
		,CustomerSortOrder
    HAVING 
	CAST(SUM(CR.usage) AS DECIMAL(18,2)) 
	between   
	 CD.Usage- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	 CD.Usage +((30*CONVERT(Decimal(18,2),AverageReading)/100))
    
    DELETE FROM		 #CustomerBillswithAverage
 	
END
----------------------------------------------------------------------------------------------------
 




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
	FROM
	UDV_PrebillingRpt(NOLOCK)  CD
	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID   and  CD.ReadCodeID = 1	AND CD.ActiveStatusId=1  
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	 AND  isnull(CD.BoolDisableTypeId,0)=0

	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	WHERE isnull(DAVG.AvgReadingId,0)=0 
       -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT CD.GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD(NOLOCK)
	Inner join
	Tbl_CustomerReadings(NOLOCK)  CR
	ON CR.GlobalAccountNumber=CD.GlobalAccountNumber
	and IsBilled=0 and Usage=0
 	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
 	
END
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentBatchProcesFailures]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Payment Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@PUploadFileId INT 
			--,@PUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@PUploadFileId = C.value('(PUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	--SELECT @PUploadBatchId = PUploadBatchId FROM Tbl_PaymentUploadFiles WHERE PUploadFileId = @PUploadFileId

	SELECT 
			PUF.TotalFailureTransactions,
			PUF.TotalCustomers,
			PUB.BatchNo,
			--PUB.BatchDate,
			CONVERT(VARCHAR(24),PUB.BatchDate,106) AS BatchDate,
			PUB.Notes
	FROM Tbl_PaymentUploadFiles(NOLOCK) PUF
	JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId
	WHERE PUF.PUploadFileId = @PUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			PaymentFailureransactionID,
			AccountNo,
			AmountPaid,
			ReceiptNo,
			ReceivedDate,
			PaymentMode,
			Comments
		FROM Tbl_PaymentFailureTransactions(NOLOCK)
		WHERE PUploadFileId = @PUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingBatchProcesFailures]    Script Date: 05/01/2015 20:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetReadingBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
			--,@RUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	--SELECT @RUploadBatchId = RUploadBatchId FROM Tbl_ReadingUploadFiles WHERE RUploadFileId = @RUploadFileId

	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_ReadingUploadFiles RUF
	JOIN Tbl_ReadingUploadBatches RUB ON RUB.RUploadBatchId = RUF.RUploadBatchId
	WHERE RUF.RUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			CurrentReading,
			ReadingDate,
			Comments,
			ReadingFailureransactionID
		FROM Tbl_ReadingFailureTransactions
		WHERE RUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 05/01/2015 20:30:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND PaidAmount = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNO) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
											AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
				when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
				else 0 end) as IsBUValidate 
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,(SELECT BatchID FROM Tbl_BatchDetails(NOLOCK)
			WHERE BatchNo = (SELECT TOP(1) BatchNo FROM Tbl_PaymentUploadBatches(NOLOCK) PUB 
							WHERE PUB.PUploadBatchId = PUF.PUploadBatchId)) AS BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.OldAccountNo = TempDetails.AccountNo
	--((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
	--					OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END
			
			-- TO check whether the customer related that user BU or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				--UPDATE B
				--SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				--FROM Tbl_BatchDetails B
				--INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END
GO


