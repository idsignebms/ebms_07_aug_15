
CREATE TABLE Tbl_Audit_PaymentsLogs(
	PaymentLogId BIGINT,
	GlobalAccountNumber VARCHAR(50) ,
	AccountNo VARCHAR(50) ,
	ReceiptNumber VARCHAR(20) ,
	PaymentMode INT ,
	PaidAmount DECIMAL(18, 2) ,
	PadiDate DATETIME ,
	PresentApprovalRole INT ,
	NextApprovalRole INT ,
	ApproveStatusId INT ,
	Remarks VARCHAR(MAX) ,
	CreatedBy VARCHAR(50) ,
	CreatedDate DATETIME ,
	ModifedBy VARCHAR(50) ,
	ModifiedDate DATETIME ,
)

GO
CREATE TABLE MASTERS.Tbl_MPaymentsFrom
(
	 PaymentFromId INT IDENTITY(1,1) PRIMARY KEY
	,PaymentFrom VARCHAR(500)
)
GO
INSERT INTO MASTERS.Tbl_MPaymentsFrom (PaymentFrom) VALUES('AdvancePayment')
GO
INSERT INTO MASTERS.Tbl_MPaymentsFrom (PaymentFrom) VALUES('CustomerWiseBillPayment')
GO
INSERT INTO MASTERS.Tbl_MPaymentsFrom (PaymentFrom) VALUES('BatchWisePayment')
GO
ALTER TABLE Tbl_PaymentsLogs
ADD PaymentFromId INT
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD PaymentFromId INT
GO

ALTER TABLE Tbl_PaymentsLogs
ADD DocumentPath VARCHAR(MAX)
GO
ALTER TABLE Tbl_PaymentsLogs
ADD Cashier VARCHAR(50)
GO
ALTER TABLE Tbl_PaymentsLogs
ADD CashOffice INT
GO
ALTER TABLE Tbl_PaymentsLogs
ADD DocumentName VARCHAR(MAX)
GO
ALTER TABLE Tbl_PaymentsLogs
ADD BatchNo INT
GO
ALTER TABLE Tbl_PaymentsLogs
ADD PaymentType INT
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD DocumentPath VARCHAR(MAX)
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD Cashier VARCHAR(50)
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD CashOffice INT
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD DocumentName VARCHAR(MAX)
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD BatchNo INT
GO
ALTER TABLE Tbl_Audit_PaymentsLogs
ADD PaymentType INT
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Average Consumption Bulk Upload','../Billing/AverageConsumptionBulkUpload.aspx',4,24,1,1,184)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,184,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,184,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Average Consumption Bulk Upload Status','../Billing/AverageConsumptionBulkUploadStatus.aspx',4,25,1,1,185)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,185,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,185,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
UPDATE Tbl_Menus SET IsActive = 0  WHERE  Name = 'Customers Average Consumption Upload' AND ReferenceMenuId=4
GO