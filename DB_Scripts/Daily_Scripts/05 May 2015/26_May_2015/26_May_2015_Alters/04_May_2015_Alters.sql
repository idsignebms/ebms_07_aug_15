GO
CREATE TABLE Tbl_CustomerReadingApprovalLogs
(
	 CustomerReadingLogId	int primary key identity(1,1)
	,ReadDate	datetime
	,ReadBy	varchar(50)
	,PreviousReading	varchar(50)
	,PresentReading	varchar(50)
	,Usage	numeric
	,TotalReadingEnergies	decimal
	,TotalReadings	int
	,Multiplier	int
	,ReadType	int
	,AverageReading	varchar(50)
	,GlobalAccountNumber	varchar(50)
	,IsTamper	bit
	,CreatedBy	varchar(50)
	,CreatedDate	datetime
	,ModifiedBy	varchar(50)
	,ModifiedDate	datetime
	,MeterNumber	varchar(50)
	,MeterReadingFrom	int
	,IsRollOver	bit
	,ApproveStatusId INT
	,PresentApprovalRole INT
	,NextApprovalRole INT
)
GO


--ALTER TABLE Tbl_PaymentUploadBatches
--ADD BU_ID VARCHAR(50)
--GO
--ALTER TABLE Tbl_ReadingUploadBatches
--ADD BU_ID VARCHAR(50)
--GO
--ALTER TABLE Tbl_AverageUploadBatches
--ADD BU_ID VARCHAR(50)
--GO