ALTER TABLE Tbl_CustomerReadingApprovalLogs
ADD Remarks VARCHAR(MAX)

GO
CREATE TABLE Tbl_Audit_CustomerReadingApprovalLogs
(
	 AuditCustomerReadingLogId	int primary key identity(1,1)
	,ReadDate	datetime
	,ReadBy	varchar(50)
	,PreviousReading	varchar(50)
	,PresentReading	varchar(50)
	,Usage	numeric
	,TotalReadingEnergies	decimal(18,2)
	,TotalReadings	int
	,Multiplier	int
	,ReadType	int
	,AverageReading	varchar(50)
	,GlobalAccountNumber	varchar(50)
	,IsTamper	bit
	,CreatedBy	varchar(50)
	,CreatedDate	datetime
	,MeterNumber	varchar(50)
	,MeterReadingFrom	int
	,IsRollOver	bit
	,ApproveStatusId	int
	,PresentApprovalRole	int
	,NextApprovalRole	int
	,Remarks	varchar(MAX)
)

GO

CREATE TABLE Tbl_Audit_AdjustmentsLogs(
	AdjustmentLogId BIGINT,
	GlobalAccountNumber VARCHAR(50) ,
	AccountNo VARCHAR(50) ,
	BillNumber VARCHAR(50) ,
	BillAdjustmentTypeId INT ,
	MeterNumber VARCHAR(20) ,
	PreviousReading VARCHAR(20) ,
	CurrentReadingAfterAdjustment VARCHAR(50) ,
	CurrentReadingBeforeAdjustment VARCHAR(50) ,
	AdjustedUnits DECIMAL(18, 2) ,
	TaxEffected DECIMAL(18, 2) ,
	TotalAmountEffected DECIMAL(18, 2) ,
	EnergryCharges DECIMAL(18, 2) ,
	AdditionalCharges DECIMAL(18, 2) ,
	PresentApprovalRole INT ,
	NextApprovalRole INT ,
	ApproveStatusId INT ,
	Remarks VARCHAR(MAX) ,
	CreatedBy VARCHAR(50) ,
	CreatedDate DATETIME ,
	ModifedBy VARCHAR(50) ,
	ModifiedDate DATETIME ,
)

GO
INSERT [dbo].[TBL_FunctionalAccessPermission] ([Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'Meter Readings', 0, 0, N'superadmin', CAST(0x0000A48B00A7C7A6 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TBL_FunctionalAccessPermission] ([Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'Direct Customer Average Upload', 2, 1, N'superadmin', CAST(0x0000A48B00A7C7A7 AS DateTime), N'Admin', CAST(0x0000A48B0101E9BF AS DateTime))
GO
UPDATE TBL_FunctionalAccessPermission SET IsActive=1 WHERE FunctionId=2