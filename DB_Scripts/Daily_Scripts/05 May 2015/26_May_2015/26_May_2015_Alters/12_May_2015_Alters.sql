ALTER TABLE Tbl_BillAdjustments ADD CreatedBy VARCHAR(50)
ALTER TABLE Tbl_BillAdjustments ADD ModifiedBy VARCHAR(50)
ALTER TABLE Tbl_BillAdjustments ADD ModifiedDate DATETIME
ALTER TABLE Tbl_BillAdjustmentDetails ADD CreatedBy VARCHAR(50)
ALTER TABLE Tbl_BillAdjustmentDetails ADD CreatedDate DATETIME
ALTER TABLE Tbl_BillAdjustmentDetails ADD ModifiedBy VARCHAR(50)
ALTER TABLE Tbl_BillAdjustmentDetails ADD ModifiedDate DATETIME

ALTER TABLE Tbl_BATCH_ADJUSTMENT ADD BillAdjustmentTypeId BIGINT