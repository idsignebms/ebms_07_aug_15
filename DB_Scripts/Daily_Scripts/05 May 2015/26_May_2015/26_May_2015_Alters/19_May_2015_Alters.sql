
--GO
--ALTER TABLE Tbl_AdjustmentsLogs ADD  BatchId bigint
--GO
ALTER TABLE Tbl_CustomerNameChangeLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_CustomerNameChangeLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_CustomerAddressChangeLog_New ADD IsLocked BIT
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_DirectCustomersAverageChangeLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_DirectCustomersAverageChangeLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_ReadToDirectCustomerActivityLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_ReadToDirectCustomerActivityLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_AssignedMeterLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_AssignedMeterLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_BookNoChangeLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_BookNoChangeLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_CustomerActiveStatusChangeLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_CustomerActiveStatusChangeLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_CustomerTypeChangeLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_CustomerTypeChangeLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_LCustomerTariffChangeRequest ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_LCustomerTariffChangeRequest ADD IsLocked BIT
GO
ALTER TABLE Tbl_PaymentsLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_PaymentsLogs ADD IsLocked BIT
GO
ALTER TABLE Tbl_AdjustmentsLogs ADD CurrentApprovalLevel INT
GO
ALTER TABLE Tbl_AdjustmentsLogs ADD IsLocked BIT
-------------------------------------------------------------------------------
--GO
--ALTER TABLE Tbl_BillAdjustments ADD CreatedBy VARCHAR(50)
--GO
--ALTER TABLE Tbl_BillAdjustments ADD ModifiedBy VARCHAR(50)
--GO
--ALTER TABLE Tbl_BillAdjustments ADD ModifiedDate DATETIME
--GO
--ALTER TABLE Tbl_BillAdjustmentDetails ADD CreatedBy VARCHAR(50)
--GO
--ALTER TABLE Tbl_BillAdjustmentDetails ADD CreatedDate DATETIME
--GO
--ALTER TABLE Tbl_BillAdjustmentDetails ADD ModifiedBy VARCHAR(50)
--GO
--ALTER TABLE Tbl_BillAdjustmentDetails ADD ModifiedDate DATETIME
--GO
--ALTER TABLE Tbl_BATCH_ADJUSTMENT ADD BillAdjustmentTypeId BIGINT
--GO
--------------------------------------------------------------------------------

GO
ALTER TABLE TBL_FunctionApprovalRole
ADD BU_ID VARCHAR(20) REFERENCES Tbl_BussinessUnits(BU_ID) 
GO
UPDATE TBL_FunctionApprovalRole SET BU_ID = 'BEDC_BU_0001' WHERE BU_ID IS NULL
GO    
ALTER TABLE TBL_FunctionApprovalRole
	ALTER COLUMN BU_ID VARCHAR(20) NOT NULL
GO
	
GO
ALTER TABLE TBL_FunctionApprovalRole
ADD IsActive BIT DEFAULT 1 
GO
UPDATE TBL_FunctionApprovalRole SET IsActive = 1  
GO
---------------------------------------------------------------------------------

GO
ALTER TABLE Tbl_MRoles
ADD AccessLevelID INT
GO
ALTER TABLE Tbl_MRoles
ADD SecuredAccess BIT DEFAULT 0
GO








GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Pre Billing Report','../Reports/PreBillingReport.aspx',8,25,1,1,186)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,186,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,186,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('General Reports','../Reports/GeneralReports.aspx',8,27,1,1,187)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,187,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,187,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
UPDATE Tbl_Menus SET Page_Order = 26 WHERE Name = 'Edit List' AND MenuId = 151 AND ReferenceMenuId = 8
GO
------------------------------------------------------------------------------------------------------------

GO
UPDATE Tbl_Menus SET IsActive = 0 WHERE ReferenceMenuId = 8 AND IsActive = 1 AND [path] NOT IN('../Reports/CustomerOutStandingReport.aspx'
,	'../Reports/CustomerLedger.aspx'
,	'../Reports/AccountsWithDebitBalenceReport.aspx'
,	'../Reports/AccountsWithCreditBalance.aspx'
,	'../Reports/PaymentsReport.aspx'
,	'../Reports/CustomerWithNoMeter.aspx'
,	'../Reports/AccountWithOutFeeder.aspx'
,	'../Reports/AccountsWithNoNameOrAddress.aspx'
,	'../Reports/CustomerWithNoMeter.aspx'
,	'../Reports/CustomerBills.aspx'
,	'../Reports/UnBilledCustomersReport.aspx'
	
,	'../Reports/AgedCustomersAccountReceivableReport.aspx'
,	'../Reports/UsageConsumeReport.aspx'
,	'../Reports/NoUsageCustomersReport.aspx'
,	'../Reports/TariffBUReport.aspx'
,	'../Reports/RptBillingStatistics.aspx'
,	'../Reports/RptBillingStatusForSCByBUCustomerType.aspx'
,	'../Reports/RptCustomerStatisticsByTariffClass.aspx'
,	'../Reports/RptGetCustomersStatisticsAndTariffClassesWithAmount.aspx'
,	'../Reports/RptCustomersBillingStatisticsInfo.aspx'
,	'../Reports/BillingStatusReoprt.aspx'
,	'../Reports/RptCustomerStatisticsByTariffClass.aspx'

,	'../Reports/AccountsOnContinuousEstimations.aspx'
)
AND MenuId not IN(186,187,151)

GO


GO

UPDATE Tbl_Menus 
set ReferenceMenuId = 187
 WHERE ReferenceMenuId = 8 
 AND [path] IN('../Reports/CustomerOutStandingReport.aspx'
,	'../Reports/CustomerLedger.aspx'
,	'../Reports/AccountsWithDebitBalenceReport.aspx'
,	'../Reports/AccountsWithCreditBalance.aspx'
,	'../Reports/PaymentsReport.aspx'
,	'../Reports/CustomerWithNoMeter.aspx'
,	'../Reports/AccountWithOutFeeder.aspx'
,	'../Reports/AccountsWithNoNameOrAddress.aspx'
,	'../Reports/CustomerWithNoMeter.aspx'
,	'../Reports/CustomerBills.aspx'
,	'../Reports/UnBilledCustomersReport.aspx'
	
,	'../Reports/AgedCustomersAccountReceivableReport.aspx'
,	'../Reports/UsageConsumeReport.aspx'
,	'../Reports/NoUsageCustomersReport.aspx'
,	'../Reports/TariffBUReport.aspx'

,	'../Reports/RptBillingStatistics.aspx'
,	'../Reports/RptBillingStatusForSCByBUCustomerType.aspx'
,	'../Reports/RptCustomerStatisticsByTariffClass.aspx'
,	'../Reports/RptGetCustomersStatisticsAndTariffClassesWithAmount.aspx'
,	'../Reports/RptCustomersBillingStatisticsInfo.aspx'
,	'../Reports/BillingStatusReoprt.aspx'
,	'../Reports/RptCustomerStatisticsByTariffClass.aspx'

,	'../Reports/AccountsOnContinuousEstimations.aspx'
)
GO
----------------------------------------------------------------------------------------------------------

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Customer Outstanding Report','../Reports/CustomerOutStandingReport.aspx',187,23,0,1,188)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,188,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,188,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Usage Consume Report','../Reports/UsageConsumeReport.aspx',187,24,1,1,189)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,189,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,189,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Billing Statistics','../Reports/RptBillingStatistics.aspx',187,25,1,1,190)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,190,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,190,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Billing Status For SC By BU Customer Type','../Reports/RptBillingStatusForSCByBUCustomerType.aspx',187,26,1,1,191)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,191,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,191,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Customer Statistics By Tariff Class','../Reports/RptCustomerStatisticsByTariffClass.aspx',187,27,1,1,192)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,192,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,192,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Customer Statistics By Tariff Class With Description','../Reports/RptGetCustomersStatisticsAndTariffClassesWithAmount.aspx',187,28,1,1,193)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,193,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,193,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Customer Bill Statistics Info','../Reports/RptCustomersBillingStatisticsInfo.aspx',187,29,1,1,194)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,194,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,194,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Billing Status Report','../Reports/BillingStatusReoprt.aspx',187,30,1,1,195)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,195,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,195,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Customer Statistics By Tariff','../Reports/RptCustomerStatisticsByTariffClass.aspx',187,31,1,1,196)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,196,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,196,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Read Customers Report','../Reports/RptCustomersWithMeter.aspx',187,32,1,1,197)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,197,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,197,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

------------------------------------------------------------------------------------------------------------


GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Meter Reding Report','../Reports/MeterReadingReport.aspx',151,1,1,1,198)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,198,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,198,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Average Upload Report','../Reports/AverageUploadReport.aspx',151,2,1,1,199)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,199,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,199,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Payments Report','../Reports/PaymentEditListReport.aspx',151,3,1,1,200)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,200,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,200,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Adjustment Report','../Reports/AdjustmentReport.aspx',151,4,1,1,201)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,201,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,201,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
------------------------------------------------------------------------------------------------------------

GO 
	UPDATE Tbl_Menus 
	SET [Path] = '../Reports/ReportsSummary.aspx'
	WHERE MenuId = 187
GO

------------------------------------------------------------------------------------------------------------
GO
Update Tbl_Menus set AccessLevelID=3 where MenuId in (10	,
13	,
17	,
18	,
19	,
20	,
21	,
22	,
23	,
24	,
37	,
176	,
178	,
180	,
181	,
182	,
184	,
185	,
187	,
189	,
98	,
99	,
100	,
102	,
106	,
110	,
111	,
112	,
113	,
115	,
118	,
122	,
183	,
45	,
51	,
58	,
59	,
60	,
75	,
77	,
78	,
198	,
199	,
200	,
201	,
151	,
152	,
153	,
154	
)
GO
Update Tbl_Menus set AccessLevelID=2 where MenuId in (12	,
25	,
179	,
186	,
197	,
103	,
107	,
108	,
109	,
114	,
117	,
40	,
44	,
157	,
160	,
161	,
162	,
163	,
164	
)
GO
Update Tbl_Menus set AccessLevelID=1 where MenuId in (26	,
190	,
191	,
192	,
193	,
194	,
195	,
196	,
82	,
83	,
88	,
89	,
90	,
92	,
93	,
95	,
104	,
105	,
119	,
120	,
121	,
150	,
167	,
168	,
169	,
170	,
171	,
172	,
173	,
174	,
175	,
177	
)
GO
------------------------------------------------------------------------------------
GO
UPDATE Tbl_MRoles SET AccessLevelID=1 WHERE RoleId IN (1,2,12)
GO
UPDATE Tbl_MRoles SET AccessLevelID=2 WHERE RoleId IN (3)
GO
UPDATE Tbl_MRoles SET AccessLevelID=3 WHERE RoleId NOT IN (1,2,3,12)
GO
-------------------------------------------------------------------------------------
GO
UPDATE TBL_MENUS SET PATH ='../Billing/ReadingBatchProcessStatus.aspx' WHERE MENUID=183
GO