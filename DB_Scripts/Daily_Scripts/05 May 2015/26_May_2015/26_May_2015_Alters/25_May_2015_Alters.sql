
UPDATE Tbl_EditListReportSummary
SET ReportName = 'Meter Reading Report'
WHERE ReportCode = 'ELR-A001'



GO
ALTER TABLE Tbl_BATCH_ADJUSTMENT ADD CONSTRAINT dt_Tbl_BATCH_ADJUSTMENT DEFAULT 1 FOR BatchStatusID
GO

GO
UPDATE Tbl_Menus SET ReferenceMenuId=6, Page_Order=22 WHERE MenuId=202 AND Name='Payment Batch Reopen'
GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId,AccessLevelID)
VALUES('Adjustment Batch Reopen','../Billing/AdjustmentBatchReopen.aspx',6,23,1,1,203,3)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,203,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,203,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO