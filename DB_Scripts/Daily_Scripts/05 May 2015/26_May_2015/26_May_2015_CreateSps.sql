
GO

/****** Object:  StoredProcedure [dbo].[USP_ReopenBatchAdjustment]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 19-May-2015
-- Description: The purpose of this procedure is to Reopen the Closed batch (BatchAdjustmentStatus)  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ReopenBatchAdjustment]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BatchID INT  
		
     
 SELECT  @BatchID=C.value('(BatchId)[1]','INT')        
 FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
       

	 UPDATE Tbl_BATCH_ADJUSTMENT   
	 SET  BatchStatusID = 1
	 WHERE BatchID=@BatchID  
  
	 SELECT 1 AS RowsEffected   
	 FOR XML PATH('BatchStatusBE')
END  
  
  
  -----------------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_GetReopenBatchAdjustments]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <25-May-2015>
-- Description:	<Retriving close adjustment batches for Reopen >
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetReopenBatchAdjustments]
(
	@XmlDoc XML
)
AS
	BEGIN
		DECLARE @BU_ID VARCHAR(50)
				,@PageNo INT
				,@PageSize INT
		
		SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')   
				,@PageNo = C.value('(PageNo)[1]','INT')
				,@PageSize = C.value('(PageSize)[1]','INT')	         
		FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
		
		;WITH PagedResults AS
		(
		
		SELECT   ROW_NUMBER() OVER(ORDER BY BA.BatchDate) AS RowNumber 
					,BA.BatchID
					,BA.BatchNo
					,ISNULL(CONVERT(VARCHAR(50),BA.BatchDate,106),'--') AS BatchDate
					,BA.BatchTotal
					,ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0) AS PaidAmount
					,BU_ID AS BUID
			FROM	Tbl_BATCH_ADJUSTMENT AS BA
			WHERE	BatchStatusID = 2
					AND BU_ID=@BU_ID
					
		)
		SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults PR
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			ORDER BY PR.BatchDate
	END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookwiseCUstomerReadings]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : Karteek
-- Modified date : 21-May-2015
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBookwiseCUstomerReadings] 
(
	@XmlDoc xml 
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
		dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
		,MeterNumber as CustomerExistingMeterNumber
		,MeterDials As CustomerExistingMeterDials
		,Decimals as Decimals
		,InitialReading as	InitialReading
		,AccountNo
		,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeID = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadingApprovalLogs   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND ApproveStatusId IN(1,2)
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,ISNULL(CustomerReadingInner.IsLocked,0) AS IsLocked
	,CustomerReadingInner.ApproveStatusId
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadingApprovalLogs CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadIDMain
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND IsBilled = 0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadingsMain
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDMain	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId 
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------

	--Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	--INTO #CusotmersWithMaxBillID    
	--from Tbl_CustomerBills CustomerBillInnner 
	--INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	--Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 --select CustomerBillMain.AccountNo as GlobalAccountNumber
	 --,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 --INTO #CustomerLatestBills  
	 --from  Tbl_CustomerBills As CustomerBillMain
	 --INNER JOIN 
	 --#CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 --on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end)
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end)
				ELSE (Case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) END) as IsExists,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				ELSE ISNULL(CONVERT(VARCHAR(20),CustomerReadingsMain.ReadDate,106),'--') END) as LatestDate,
			CustomerList.Name,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN 1
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN 2
				ELSE 3 END) as ApproveStatusId,
			--CustomerReadings.IsLocked,
			--CustomerReadings.ApproveStatusId,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN CustomerReadings.ReadingMultiplier
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN CustomerReadings.ReadingMultiplier
				ELSE CustomerReadingsMain.ReadingMultiplier END) as Multiplier,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN CustomerReadings.IsTamper
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN CustomerReadings.IsTamper
				ELSE CustomerReadingsMain.IsTamper END) as IsTamper,
			0 as Decimals,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				ELSE ISNULL(CustomerReadingsMain.AverageReading,'0.00') END) as AverageReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 THEN CustomerReadings.TotalReadings
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN CustomerReadings.TotalReadings
				ELSE CustomerReadings.TotalReadings END) as TotalReadings,
			(CASE WHEN CustomerReadings.IsLocked = 0 
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else NULL end)
								 End)
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else NULL end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
									else NULL end)
								 End) END) as PresentReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
								else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end)
				ELSE 0 END) as PrvExist,
			(CASE WHEN CustomerReadings.IsLocked = 0 
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				ELSE (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadingsMain.Usage,0) end) END) as Usage,
			(CASE WHEN CustomerReadings.IsLocked = 0 
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading							
											else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
									else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End) END) as PreviousReading
			,CustomerList.BookNo
			,CustomerList.InitialReading
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerLatestReadingsMain	  As CustomerReadingsMain
			ON
			CustomerList.GlobalAccountNumber=CustomerReadingsMain.GlobalAccountNumber	
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table #CustomerLatestReadingsMain
DROP Table #CusotmersWithMaxReadIDMain
DROP Table  #CustomersListBook  
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountwiseMeterReadings]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 21 May 2015
-- Description: <Get BillReading details from customerreading table>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetAccountwiseMeterReadings] 
(
	@XmlDoc xml
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists bit = 0
		Declare	  @PrvExists bit = 0
		Declare	  @IsInProcess bit = 0
		Declare	  @IsNoReadings bit = 0
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		DECLARE @MaxCustomerReadinglogId INT,@LogIsLocked BIT,@LogApprovalStatusId INT
		
		SELECT TOP 1 @MaxCustomerReadinglogId = CustomerReadingLogId
			,@LogIsLocked = IsLocked
			,@LogApprovalStatusId = ApproveStatusId
		FROM Tbl_CustomerReadingApprovalLogs 
		WHERE GlobalAccountNumber = @GlobalAcountNumber
		ORDER BY CustomerReadingLogId DESC
		
		IF(@LogIsLocked = 0)
			BEGIN
				SET @IsExists = 1 
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingLogs
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 0
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingLogs where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@IsExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingLogs  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingLogs
					END
			END
		ELSE IF(@LogIsLocked = 1 AND @LogApprovalStatusId = 1)
			BEGIN
				SET @IsInProcess = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingApprove
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 1 AND ApproveStatusId = 1
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingApprove where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@IsExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingApprove  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingApprove
					END
			END
		ELSE 
			BEGIN
				SET @IsNoReadings = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,IsBilled
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingId
				INTO #CustomerTopTwoReadingMain
				from Tbl_CustomerReadings
				where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
				Order By CustomerReadingId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingMain where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@IsExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingMain  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
							 @PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=IsBilled
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingMain
					END
			END
			
		--select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		--SELECT TOP(1) @Month = BillMonth,
		--@Year = BillYear ,@LastBillReadType= 
		--(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		--=CB.ReadCodeId)
		--,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		--WHERE AccountNo = @GlobalAcountNumber  
		--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		--SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		--CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		--DECLARE @IsLatestBill BIT = 0 	

		--IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		--BEGIN
		--	SET @IsLatestBill = 1
		--END 
		--ELSE
		--  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
			 @AcountNumber AS AccountNo,
			 @OldAcountNumber AS OldAccountNo,
			 @Name   AS Name,
			 @usage as Usage
			 ,@RouteName   as RouteNum
			 --,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
			 --,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS IsActiveMonth
			 ,@IsTamper as IsTamper
			 ,@IsExists	as IsExists 
			 ,@IsInProcess as IsApprovalProces
			 ,@IsNoReadings AS IsActive
			 ,convert(varchar(11),@LatestDate,105) as LatestDate
			 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
			 ,@MeterNumber as MeterNo
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
			 ,@PrvExists as IsPrvExists  
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
			 --,@LastBillReadType as LastBillReadType
			 ,ISNULL(@InititalReading,0) as	 InititalReading
			 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
	)
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  


END  


GO

/****** Object:  StoredProcedure [dbo].[USP_ReopenBatchPayment]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 19-May-2015
-- Description: The purpose of this procedure is to Reopen the Closed batch (BatchPaymentStatus)  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ReopenBatchPayment]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BatchNo INT  
		
     
 SELECT  @BatchNo=C.value('(BatchNo)[1]','INT')        
 FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
       

	 UPDATE Tbl_BatchDetails   
	 SET  BatchStatus = 1
	 WHERE BatchID=@BatchNo  
  
	 SELECT 1 AS RowsEffected   
	 FOR XML PATH('BatchStatusBE')
END  
  
  
  -----------------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_GetReopenBatchPayemts]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <18-May-2015>
-- Description:	<Retriving close batches for Reopen >
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetReopenBatchPayemts]
(
	@XmlDoc XML
)
AS
	BEGIN
		DECLARE @BU_ID VARCHAR(50)
				,@PageNo INT
				,@PageSize INT
		
		SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')   
				,@PageNo = C.value('(PageNo)[1]','INT')
				,@PageSize = C.value('(PageSize)[1]','INT')	         
		FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
		
		;WITH PagedResults AS
		(
		SELECT   ROW_NUMBER() OVER(ORDER BY BD.BatchDate) AS RowNumber 
			,BD.BatchNo
			,BD.BatchID 
			,BD.BatchTotal
			,ISNULL(CONVERT(VARCHAR(50),BD.BatchDate,106),'--') AS BatchDate
			,ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchID),0) AS PaidAmount
			,BU_ID AS BUID
		FROM Tbl_BatchDetails AS BD
		WHERE BD.BatchStatus = 2
		AND BU_ID=@BU_ID
		)
		SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults PR
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			ORDER BY PR.BatchDate
	END


GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================

CREATE PROCEDURE [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @GlobalAccountNo VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	DECLARE @IsDisabledBook BIT 
	
	SELECT @CustomerBusinessUnitID = U.BU_ID,@CustomerActiveStatusID = ISNULL(U.ActiveStatusId,0)  
		,@GlobalAccountNo = U.GlobalAccountNumber
		,@IsDisabledBook = (CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN IsPartialBill IS NULL THEN 1 ELSE 0 END)
								ELSE 0 END)
	FROM  UDV_IsCustomerExists U
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = U.BookNo
	WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo OR MeterNumber=@AccountNo)
	  	
	IF @GlobalAccountNo IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
		IF @CustomerBusinessUnitID = @BUID    
			BEGIN
				IF @CustomerActiveStatusID = 1 OR @CustomerActiveStatusID=2 
					BEGIN
						--PRINT 'Sucess and Active  Customer'
						SELECT 1 AS IsSuccess,
							@GlobalAccountNo AS AccountNo,
							@IsDisabledBook AS CustIsNotActive
						FOR XML PATH('RptCustomerLedgerBe') 
					END
				ELSE
					BEGIN							 
						--PRINT 'Failure In Active Status'
						SELECT 0 AS IsSuccess
						FOR XML PATH('RptCustomerLedgerBe') 
					END
			END
		ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU ,
						@IsDisabledBook AS CustIsNotActive
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterDecimalValues]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <RamaDevi M>    
-- Create date: <25-MAR-2014>    
-- Description: <Get Meter details>    
-- ModifiedBy:  Suresh Kumar D  
-- ModifiedBy: Faiz ID103
-- ModifiedDate: <17-DEC-2014>
-- Modified By : Padmini
-- Modified Date : 26-12-2014 
-- Modified By : Karteek
-- Modified Date : 13-05-2015 
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetMeterDecimalValues](@XmlDoc XML)  
   
AS  
BEGIN  
 DECLARE @Account VARCHAR(100) 
   
 SELECT   
  @Account = C.value('(AccNum)[1]','VARCHAR(100)')  
 FROM @XmlDoc.nodes('BillingBE') as T(C)
 
  IF EXISTS(SELECT M.MeterNo from [CUSTOMERS].[Tbl_CustomerProceduralDetails] C 
            INNER JOIN Tbl_MeterInformation M on  C.MeterNumber=M.MeterNo AND C.GlobalAccountNumber = @Account)	
		BEGIN  
			SELECT  
				MeterDials as MeterDials  
				,Decimals as Decimals
				,ReadCodeId   --To chk user read code 
			FROM Tbl_MeterInformation M 
			INNER JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] C on  C.MeterNumber = M.MeterNo 
				AND  C.GlobalAccountNumber = @Account 
			FOR XML PATH('BillingBE'),Type  
		END  
 ELSE  
	  BEGIN  
		   SELECT   
			6 as MeterDials  
			,2 as Decimals
			,1 as ReadCodeId--To chk user read code  
		   FOR XML PATH('BillingBE'),Type  
	  END  
  END
  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentBatchProcesSuccess]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Modified By : FAIZ
-- Modified Date: 12-May-2015
-- Description:	This pupose of this procedure is to get Payment Batch Proces Success Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPaymentBatchProcesSuccess]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@PUploadFileId INT 
			--,@PUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@PUploadFileId = C.value('(PUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
  
	--SELECT @PUploadBatchId = PUploadBatchId FROM Tbl_PaymentUploadFiles WHERE PUploadFileId = @PUploadFileId

	SELECT 
			PUF.TotalSucessTransactions AS TotalSuccessTransactions,
			PUF.TotalCustomers,
			PUB.BatchNo,
			--PUB.BatchDate,
			CONVERT(VARCHAR(24),PUB.BatchDate,106) AS BatchDate,
			PUB.Notes
	FROM Tbl_PaymentUploadFiles(NOLOCK) PUF
	JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId
	WHERE PUF.PUploadFileId = @PUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			PaymentSucessTransactionID,
			AccountNo,
			AmountPaid,
			ReceiptNo,
			ReceivedDate,
			PaymentMode,
			Comments
		FROM Tbl_PaymentSucessTransactions(NOLOCK)
		WHERE PUploadFileId = @PUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingBatchProcessSuccess]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : FAIZ
-- Modified Date: 12-May-2015
-- Description:	This pupose of this procedure is to get Reading Batch Proces success Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetReadingBatchProcessSuccess]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
			--,@RUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	--SELECT @RUploadBatchId = RUploadBatchId FROM Tbl_ReadingUploadFiles WHERE RUploadFileId = @RUploadFileId

	SELECT 
			RUF.TotalSucessTransactions AS TotalSuccessTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_ReadingUploadFiles RUF
	JOIN Tbl_ReadingUploadBatches RUB ON RUB.RUploadBatchId = RUF.RUploadBatchId
	WHERE RUF.RUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			CurrentReading,
			ReadingDate,
			Comments,
			ReadingSucessTransactionID
		FROM Tbl_ReadingSucessTransactions
		WHERE RUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAvgConsumptionSuccess]    Script Date: 05/26/2015 20:07:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Modified By : FAIZ
-- Modified Date: 12-May-2015
-- Description:	This pupose of this procedure is to get Average Batch Proces Success Detials  
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAvgConsumptionSuccess]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	SELECT 
			RUF.TotalSucessTransactions AS TotalSuccessTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_AverageUploadFiles(NOLOCK) RUF
	JOIN Tbl_AverageUploadBatches(NOLOCK) RUB ON RUB.AvgUploadBatchId = RUF.AvgUploadBatchId
	WHERE RUF.AvgUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			AverageReading,
			--Comments,
			AvgSucessTransactionID
		FROM Tbl_AverageSucessTransactions(NOLOCK)
		WHERE AvgUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithMeter]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAccountWithMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptReadCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.GlobalAccountNumber +' - '+CD.AccountNo) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,CD.MeterNumber
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=2
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  ,MeterNumber
				  ,CONVERT(VARCHAR(20),ConnectionDate,106) AS ConnectionDate
				  ,@Count As TotalRecords 
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 11-05-2015
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType] 
(      
@XmlDoc Xml=null      
)  
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	
	SELECT @Month = C.value('(Month)[1]','INT')      
	   ,@Year = C.value('(Year)[1]','INT')      
	   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
  
    -- Insert statements for procedure here
    SELECT	CT.CustomerType,
			CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			ISNULL(CBills.TotalBillAmountWithTax,0) AS AmountBilled,
			CD.ActiveStatus AS CustomerStatus,
			RC.ReadCode,
			ISNULL(CBills.PaidAmount,0) AS TotalCollection,
			ISNULL(CBills.AdjustmentAmmount,0) AS TotalAdjustment,
			ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance
			INTO #tmpCustomerServices
			FROM tbl_customerbills CBills(NOLOCK)  
			INNER  JOIN UDV_CustomerPDFReport CD(NOLOCK) ON 
					CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
					AND CD.GlobalAccountNumber=CBills.AccountNo
			INNER JOIN Tbl_MCustomerTypes CT(NOLOCK) ON CT.CustomerTypeId=CD.CustomerTypeId
			INNER JOIN Tbl_MReadCodes RC(NOLOCK) ON RC.ReadCodeId=CBills.ReadCodeId 

		SELECT 
			CustomerType,
			SUM(ISNULL(Active,0) ) AS Active,
			SUM(ISNULL(InActive,0)) AS InActive,
			SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
			SUM(EnergyBilled) AS EnergyBilled ,
			SUM(RevenueBilled) As RevenueBilled,
			SUM(AmountBilled) As AmountBilled,
			SUM(ISNULL(Minimum,0) ) AS [MIN FC],
			SUM(ISNULL([Read],0)) AS [Read],
			SUM(ISNULL(Estimate,0) ) AS EST,
			SUM(ISNULL(Direct,0)) AS Direct,
			SUM(ISNULL(NoOfBilled,0)) AS NoOfBilled,  
			SUM(ISNULL(Response,0)) AS Response,
			SUM(TotalCollection) AS TotalCollection,
			SUM(TotalAdjustment) AS TotalAdjustment,
			SUM(OpeningBalance) AS OpeningBalance,
			SUM(ClossingBalance) AS ClossingBalance
		INTO #tmpCustomerServicesInfo
		FROM
		(
			SELECT	CustomerType,
				COUNT(CustomerStatus) AS TotalCustomerTariff,		
				CustomerStatus,
				SUM(EnergyBilled) AS EnergyBilled, 
				SUM(RevenueBilled) AS RevenueBilled,
				SUM(AmountBilled) AS AmountBilled,
				COUNT(ReadCode) AS ReadCodeType,
				ReadCode,
				COUNT(AmountBilled) AS NoOfBilled,  
				COUNT(TotalCollection) AS Response,
				SUM(TotalCollection) AS TotalCollection,
				SUM(TotalAdjustment) AS TotalAdjustment,
				SUM(OpeningBalance) AS OpeningBalance,
				SUM(ClossingBalance) AS ClossingBalance
			FROM #tmpCustomerServices
			GROUP BY CustomerStatus,CustomerType,ReadCode
		) ListOfCustomers
		PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
		) AS  ListOfCustomersT
		PIVOT (SUM(ReadCodeType) FOR ReadCode IN(Direct,[Read],Estimate,Minimum)) AS  ListOfReadCodeType
		GROUP BY CustomerType
		ORDER BY CustomerType

	
	
	--==========================================================================================================
	DECLARE @tblCustomerServices AS TABLE
	(
		ID INT IDENTITY(1,1),
		CustomerType VARCHAR(100),
		Active INT,
		InActive INT,
		TotalPopulation INT,
		EnergyBilled DECIMAL(18,2),
		RevenueBilled DECIMAL(18,2),
		AmountBilled DECIMAL(18,2),
		[MIN FC] INT,
		[Read] INT,
		EST INT, 
		Direct INT,
		NoOfBilled INT,
		Response INT,
		TotalCollection DECIMAL(18,2),
		TotalAdjustment DECIMAL(18,2),
		OpeningBalance DECIMAL(18,2),
		ClossingBalance DECIMAL(18,2)
	)
	
	INSERT INTO @tblCustomerServices(CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance)
	SELECT CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance
	FROM #tmpCustomerServicesInfo
	
	INSERT INTO @tblCustomerServices(CustomerType,Active,	InActive,TotalPopulation,
		EnergyBilled ,RevenueBilled,AmountBilled,
		[MIN FC],	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		TotalCollection,TotalAdjustment,OpeningBalance,ClossingBalance)
	SELECT 'SUB Total' AS CustomerType,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		SUM(TotalPopulation) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(AmountBilled) As AmountBilled,
		SUM(ISNULL([MIN FC],0) ) AS [MIN FC],
		SUM(ISNULL([Read],0)) AS [Read],
		SUM(ISNULL(EST,0) ) AS EST,
		SUM(ISNULL(Direct,0)) AS Direct,
		SUM(ISNULL(NoOfBilled,0)) AS NoOfBilled,  
		SUM(ISNULL(Response,0)) AS Response,
		SUM(TotalCollection) AS TotalCollection,
		SUM(TotalAdjustment) AS TotalAdjustment,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) AS ClossingBalance		
	FROM #tmpCustomerServicesInfo

	SELECT CustomerType,Active,	TotalPopulation,
		'' AS EnergyDelivered,
		CONVERT(VARCHAR(20),CAST(EnergyBilled AS MONEY),-1) AS EnergyBilled ,
		CONVERT(VARCHAR(20),CAST(RevenueBilled AS MONEY),-1) AS RevenueBilled,
		CONVERT(VARCHAR(20),CAST(AmountBilled AS MONEY),-1) AS AmountBilled,
		'' AS WAT, '' AS PIM,[MIN FC] AS MINFC,	[Read],	EST,Direct,
		NoOfBilled, 	Response,
		CONVERT(VARCHAR(20),CAST(TotalCollection AS MONEY),-1) AS TotalCollection,
		CONVERT(VARCHAR(20),CAST(TotalAdjustment AS MONEY),-1) AS TotalAdjustment,
		CONVERT(VARCHAR(20),CAST(OpeningBalance AS MONEY),-1) AS OpeningBalance,
		CONVERT(VARCHAR(20),CAST(ClossingBalance AS MONEY),-1) AS ClossingBalance
	FROM @tblCustomerServices
	
	DROP TABLE #tmpCustomerServices
	DROP TABLE #tmpCustomerServicesInfo

	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffBUPivotReport_Old]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.RamaDevi  
-- Create date: 12-04-2014
--ModifiedBy: Bhimaraju.V
--ModifiedDate: 04-Aug-2014
-- Modified By: T.Karthik
-- Modified Date: 29-10-2014
-- Description: To get Pivot Table for TariffBu Reports  
----Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetTariffBUPivotReport_Old]  
   
AS  
BEGIN  
 DECLARE @query VARCHAR(4000);  
DECLARE @tariff VARCHAR(max);  
SELECT  @tariff = STUFF(  
      ( SELECT    
                        '],[' + ltrim((T.ClassName))  
                        --FROM    Tbl_CustomerDetails C  
						FROM    [UDV_CustomerDescription] C --Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]
                        JOIN  
                        Tbl_MTariffClasses T ON T.ClassID=C.TariffId  
                        GROUP BY T.ClassName
                       --ORDER BY   
                       --'],[' + ltrim(str(ClassID))  
                        FOR XML PATH('')  
                        ), 1, 2, ''  
                        ) + ']'  
SET @query = 
--SELECT(   
'
SELECT pvt.*,T.Total FROM  
(  
    SELECT
     --BU_ID AS BusinessUnit
    (SELECT BusinessUnitName FROM Tbl_BussinessUnits WHERE BU_ID=C.BU_ID) AS BusinessUnit
    , COUNT(0) AS tot
    ,T.ClassName
    --FROM Tbl_CustomerDetails C  
	FROM [UDV_CustomerDescription] C  --Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]
 JOIN  
 Tbl_MTariffClasses T ON T.ClassID=C.TariffId       
    GROUP BY BU_ID,TariffId,T.ClassName  
     
)t  
PIVOT ( sum(tot)  FOR ClassName  
IN ('+@tariff+')) AS pvt  
JOIN( SELECT 
		--BU_ID
		(SELECT BusinessUnitName FROM Tbl_BussinessUnits BU WHERE BU.BU_ID=CD.BU_ID) AS BU_ID
		,COUNT(0) AS Total  
    FROM [UDV_CustomerDescription] CD GROUP BY BU_ID) AS T ON pvt.BusinessUnit = T.BU_ID   
'
    --FOR XML PATH('''+'Consumer'+'''),TYPE)FOR XML PATH('''+'''),ROOT('''+'ConsumerBeInfoByXml'+''')  

--SET @query =  
--'SELECT(  
--SELECT pvt.*,T.Total FROM  
--(  
--    SELECT 
--    (SELECT BusinessUnitName FROM Tbl_BussinessUnits WHERE BU_ID=C.BU_ID) AS BusinessUnit
--    , COUNT(0) AS tot
--    ,T.ClassName      
--    FROM Tbl_CustomerDetails C  
-- JOIN  
-- Tbl_MTariffClasses T ON T.ClassID=C.TariffId       
--    GROUP BY BU_ID,TariffId,T.ClassName  
     
--)t  
--PIVOT ( sum(tot)  FOR ClassName  
--IN ('+@tariff+')) AS pvt  
--JOIN( SELECT BU_ID,COUNT(0) AS Total  
--    FROM Tbl_CustomerDetails GROUP BY BU_ID) AS T ON pvt.BusinessUnit = T.BU_ID   
--    FOR XML PATH('''+'Consumer'+'''),TYPE)FOR XML PATH('''+'''),ROOT('''+'ConsumerBeInfoByXml'+''')  
--'
  
  
   
 --PRINT(@query)  
   
EXECUTE (@query)  
END  
--------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithoutMeter_New_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya
-- 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAccountWithoutMeter_New_Rajaiah]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(ServiceCenterId)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('ConsumerBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.CycleName
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=1
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
			  ,Name
			  ,GlobalAccountNumber
			  ,[ServiceAddress]
			  ,ClassName
			  ,OldAccountNo
			  ,CustomerSortOrder
			  ,BookNo
			  ,BusinessUnitName
			  ,ServiceUnitName
			  ,ServiceCenterName
			  ,CycleName
			  ,BookNumber
			  ,BookSortOrder
			  ,@Count As TotalRecords 
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails_New_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
CREATE PROCEDURE[dbo].[USP_GetCustomerBillsDetails_New_Rajaiah]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) = 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) = 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) = 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) = '2,3,4,5,7,8,9'
		,@PageNo INT = 1
		,@PageSize INT = 100  
		
	SELECT  
		 @BU_IDs = C.value('(BusinessUnitName)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(MonthName)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(YearName)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress 
		,CD.MeterNumber AS MeterNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CB.NetEnergyCharges
		,CB.NetFixedCharges
		,CB.TotalBillAmount
		,CB.VAT
		,CB.VATPercentage
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CB.NetArrears
		,CB.TotalBillAmountWithArrears
		,(CASE CD.ReadCodeID WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadCode
		,CB.Usage
		,CB.PaidAmount
		,CONVERT(VARCHAR(20),CB.PaymentLastDate,106) AS PaymentLastDate
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
		,CB.ServiceAddress
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  (NOLOCK)
	INNER JOIN Tbl_MBillingTypes BT (NOLOCK) ON CB.ReadType = BT.BillingTypeId AND
		(CB.ReadType = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN UDV_CustomerPDFReport CD (NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo 
	INNER JOIN Tbl_MTariffClasses T (NOLOCK) ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY CB.AccountNo ASC


	SELECT   
		 RowNumber 
		,GlobalAccountNumber 
		,OldAccountNo 	
		,Name      
		,ServiceAddress
		,MeterNo
		,CONVERT(VARCHAR(20),CAST(NetEnergyCharges AS MONEY),-1) AS NetEnergyCharges
		,CONVERT(VARCHAR(20),CAST(NetFixedCharges AS MONEY),-1) AS NetFixedCharges
		,CONVERT(VARCHAR(20),CAST(TotalBillAmount AS MONEY),-1) AS TotalBillAmount
		,CONVERT(VARCHAR(20),CAST(VAT AS MONEY),-1) AS VAT
		,CONVERT(VARCHAR(20),CAST(VATPercentage AS MONEY),-1) AS VATPercentage
		,BillNo    
		,TariffId  
		,TariffName  
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithTax AS MONEY),-1) AS TotalBillAmountWithTax  
		,CONVERT(VARCHAR(20),CAST(NetArrears AS MONEY),-1) AS NetArrears
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithArrears AS MONEY),-1) AS TotalBillAmountWithArrears
		,Usage
		,ReadCode
		,ReadDate  
		,PaymentLastDate
		,BillMonth  
		,BillYear   
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,BillProcessType
		,TotalRecords 
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
 

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersNoNameOrAddList_New_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 16-Sep-2014  
-- Description: Get The Details of The Customers With out Having No Name Or Address  
-- Modified By : Karthik  
-- Modified Date : 27-12-2014  
-- Modified By : Padmini  
-- Modified Date : 01/12/2015  
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015     
-- =============================================  
CREATE PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList_New_Rajaiah]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageSize INT  
   ,@PageNo INT  
   ,@BU_ID VARCHAR(MAX)  
   ,@SU_ID VARCHAR(MAX)  
   ,@SC_ID VARCHAR(MAX)    
   ,@CycleId VARCHAR(MAX)  
   ,@BookNo VARCHAR(MAX)     
     
 SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('ReportsBe') AS T(C)  
   
   
 IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SU_ID = '')  
  BEGIN  
   SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50))   
      FROM Tbl_ServiceUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SC_ID = '')  
  BEGIN  
   SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50))   
      FROM Tbl_ServiceCenter  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@CycleId = '')  
  BEGIN  
   SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50))   
      FROM Tbl_Cycles  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@BookNo = '')  
  BEGIN  
   SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50))   
      FROM Tbl_BookNumbers  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
   
 ;WITH PagedResults AS  
 (  
  SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber  
    ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
    ,CD.GlobalAccountNumber AS GlobalAccountNumber  
    ,CD.AccountNo AS AccountNo  
    ,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode)) As ServiceAddress  
    ,ISNULL(OldAccountNo,'--') AS OldAccountNo  
    ,CD.CycleName  
    ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber  
    ,CD.SortOrder AS CustomerSortOrder  
    ,CD.BookSortOrder  
    ,CD.BusinessUnitName  
    ,CD.ServiceUnitName  
    ,CD.ServiceCenterName  
    FROM UDV_CustomerDescription CD  
    INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
      AND CD.ActiveStatusId=1 AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName='')  
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD(NOLOCK) ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber   
      AND PAD.IsServiceAddress = 0 AND (PAD.StreetName IS NULL OR PAD.StreetName = ''   
               OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
               OR PAD.City IS NULL OR PAD.City = '')  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD(NOLOCK) ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber   
      AND SAD.IsServiceAddress = 1 AND (SAD.StreetName IS NULL OR SAD.StreetName = ''   
               OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
               OR SAD.City IS NULL OR SAD.City = '')   
 )  
   
 --SELECT   
 --(  
  SELECT  
    RowNumber  
   ,Name  
   ,GlobalAccountNumber  
   ,AccountNo  
   ,ServiceAddress  
   ,BusinessUnitName  
   ,OldAccountNo  
   ,CycleName  
   ,BookNumber  
   ,CustomerSortOrder  
   ,BookSortOrder  
   ,BusinessUnitName  
   ,ServiceUnitName  
   ,ServiceCenterName  
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
  FROM PagedResults p  
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
 -- FOR XML PATH('Reports'),TYPE  
 --)  
 --FOR XML PATH(''),ROOT('ReportsBeInfoByXml')  
END  
-------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetNoUsageCustomersReport_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 04-11-2014
-- Description:	The purpose of this procedure is to get No Usage Customers Report
-- Modified By	: Neeraj Kanojiya
-- Modified Date: 11-Nov-14
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:31-Mar-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetNoUsageCustomersReport_Rajaiah]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @BUID VARCHAR(MAX)
		,@SUID VARCHAR(MAX)
		,@SCID VARCHAR(MAX)
		,@Cycles VARCHAR(MAX)
		,@BookNos VARCHAR(MAX)
		,@FromDate VARCHAR(50)
		,@ToDate VARCHAR(50)
		,@PageSize INT  
		,@PageNo INT  
		
	SELECT
		@BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)

	IF ISNULL(@FromDate,'') = ''
	BEGIN  
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60 
	END 
	IF ISNULL(@Todate,'') = ''
	BEGIN  
		SET @Todate = dbo.fn_GetCurrentDateTime()  
	END 
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH LIST AS
	(	
		SELECT 
			 ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber) AS RowNumber 
			,GlobalAccountNumber
			,CD.AccountNo
			,CD.OldAccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) AS ServiceAddress
			,MeterNumber AS MeterNo
			,CONVERT(VARCHAR(25),CB.BillGeneratedDate,107) AS BillGeneratedDate
			,CD.ClassName AS Tariff
			,ISNULL(CB.NetFixedCharges,0) AS AdditionalCharges
			,CB.TotalBillAmountWithArrears AS TotalBilledAmount
			,RC.ReadCode
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.CycleName 
			,CD.BusinessUnitName
			,CD.ServiceUnitName
			,CD.ServiceCenterName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.BookCode
			,ISNULL(CD.OutStandingAmount,0) AS OutStandingAmount
			,COUNT(0) OVER() AS TotalRecords
		FROM [UDV_CustomerDescription] AS CD
		INNER JOIN Tbl_CustomerBills AS CB(NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo AND CD.ActiveStatusId IN(1,2) AND ISNULL(CB.Usage,0) = 0
				AND CD.ActiveStatusId = 1 AND CD.CylceActiveStatusId = 1 
				AND (CD.CreatedDate BETWEEN @FromDate AND @ToDate)
		INNER JOIN Tbl_MReadCodes AS RC(NOLOCK) ON CB.ReadCodeId = RC.ReadCodeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
	)

	SELECT 
		 RowNumber
		,GlobalAccountNumber
		,AccountNo
		,OldAccountNo
		,Name
		,ServiceAddress 
		,MeterNo
		,BillGeneratedDate
		,Tariff
		,AdditionalCharges
		,TotalBilledAmount
		,ReadCode
		,CustomerSortOrder
		,BookSortOrder
		,CycleName 
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,BookNumber
		,BookCode
		,OutStandingAmount
		,TotalRecords
	FROM LIST
	WHERE RowNumber BETWEEN (@PageNo-1) * @PageSize + 1 AND @PageNo * @PageSize

END
------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetPaymets_New_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetPaymets_New_Rajaiah]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
		
	IF ISNULL(@FromDate,'') =''    
	BEGIN    
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
	END 
	IF ISNULL(@Todate,'') =''
	BEGIN    
		SET @Todate = dbo.fn_GetCurrentDateTime()    
	END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,100),'--') AS RecievedDate
			,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD(NOLOCK)	
		INNER JOIN Tbl_CustomerPayments CP(NOLOCK) ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
		INNER JOIN Tbl_MBillingTypes BT(NOLOCK) ON CP.ReceivedDevice = BT.BillingTypeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		
	)
	
	SELECT	 
		 RowNumber
		,GlobalAccountNumber AS AccountNo
		,Name
		,ServiceAddress
		,BusinessUnitName
		,ServiceCenterName
		,ServiceUnitName
		,CycleName
		,BookNumber
		,CustomerSortOrder
		,BookSortOrder
		,ReceivedDevice
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,RecievedDate
		,CONVERT(VARCHAR(20),CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(TotalDueAmount) FROM PagedResults) AS MONEY),-1) AS TotalDue
		,(Select COUNT(0) from PagedResults) as TotalRecords
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(PaidAmount) FROM PagedResults) AS MONEY),-1) AS TotalReceivedAmount
	FROM PagedResults p
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetLevels]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 09-May-2015
-- Description:	The purpose of this procedure is to get the access levels
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetLevels]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @FunctionId INT
			,@AccessLevels INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	
	SELECT @AccessLevels = AccessLevels
	FROM TBL_FunctionalAccessPermission FAP
	JOIN TBL_FunctionApprovalRole FAR ON FAP.FunctionId = FAR.FunctionId AND FAR.IsActive = 1
	WHERE FAP.FunctionId = @FunctionId
	
	SELECT ISNULL(@AccessLevels,0) AS AccessLevels  
	FOR XML PATH('ApprovalBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithCreditBal]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 23-Aug-2014
-- Description:	Get The Details of Credit Balance Of The Customers
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithCreditBal]
(
	@XmlDoc xml
)
AS
BEGIN

		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCreditBe') AS T(C)
	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(		
		SELECT ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber				
			,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
			--,CD.AccountNo AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
			,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount		
			,ISNULL(CONVERT(VARCHAR(50),(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)) ,106),'--') AS LastPaidDate
			,ISNULL(CAD.OutStandingAmount,0) As OverPayAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalBillsAmount(CD.GlobalAccountNumber)),0) As TotalBillsAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalPaidAmount(CD.GlobalAccountNumber)),0) As TotalPaidAmount
			,BN.BookCode AS BookCode
			,CD.OldAccountNo
			,C.CycleName
			,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
			,PD.SortOrder AS CustomerSortOrder
			,BN.SortOrder AS BookSortOrder
			,BU.BusinessUnitName
			,SU.ServiceUnitName
			,SC.ServiceCenterName
			,COUNT(0) OVER() AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			AND CAD.OutStandingAmount < 0
		INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo 
		INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId 
		INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId
		INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID 
		INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID
		INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID 		
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
	)
	
		SELECT
			 RowNumber
			,GlobalAccountNumber
			--,AccountNo
			,Name
			,ServiceAddress
			,CONVERT(VARCHAR,CAST(LastPaidAmount AS MONEY),-1) AS LastPaidAmount	
			,LastPaidDate
			,CONVERT(VARCHAR,CAST(OverPayAmount AS MONEY),-1) AS OverPayAmount
			,CONVERT(VARCHAR,CAST(TotalBillsAmount AS MONEY),-1) AS TotalBillsAmount
			,CONVERT(VARCHAR,CAST(TotalPaidAmount AS MONEY),-1) AS TotalPaidAmount
			,BookCode
			,OldAccountNo
			,CycleName
			,BookNumber 
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,CONVERT(VARCHAR,CAST((SELECT SUM(OverPayAmount) FROM PagedResults) AS MONEY),-1) AS TotalCreditBalance				
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	
END
------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithDebitBal]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 09-05-2015
-- Description:	Get The Details of Debit Balence Of The Customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithDebitBal]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)			
			,@BookNo VARCHAR(MAX)
			,@MinAmt VARCHAR(50)
			,@MaxAmt VARCHAR(50)
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@MinAmt=C.value('(MinAmt)[1]','VARCHAR(50)')
			,@MaxAmt=C.value('(MaxAmt)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('RptDebitBe') AS T(C)
	
	
	IF(@MinAmt = '')
		SET @MinAmt = (SELECT MIN(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK))
	IF(@MaxAmt = '')
		SET @MaxAmt = (SELECT MAX(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK))
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits(NOLOCK) 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits(NOLOCK) 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
			
	;WITH PagedResults AS
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
				--,A.AccountNo AS AccountNo
				,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
				,OutStandingAmount AS TotalDueAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(A.AccountNo)),'--') AS LastPaidDate
				,CD.OldAccountNo
				,C.CycleName
				,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
				,PD.SortOrder AS CustomerSortOrder
				,BN.SortOrder AS BookSortOrder
				,BU.BusinessUnitName AS BusinessUnitName
				,SU.ServiceUnitName AS ServiceUnitName
				,SC.ServiceCenterName AS ServiceCenterName
				,COUNT(0) OVER() AS TotalRecords
		 FROM Tbl_CustomerBills(NOLOCK) A
		 INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber = A.AccountNo
		 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber AND CAD.OutStandingAmount >= 0
				AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		 INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo  
		 INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId     
		 INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
		 INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID
		 INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID 
		 INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		 
	)
	

		SELECT	 
			 RowNumber
			--,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,CONVERT(VARCHAR,CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
			,CONVERT(VARCHAR,CAST(LastPaidAmount AS MONEY),-1) AS LastPaidAmount
			,LastPaidDate
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(TotalDueAmount,0)) FROM PagedResults) AS MONEY),-1) AS DueAmount
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomerLedgers]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Karteek
-- Create date: 09 May 2015
-- Description: The purpose of this procedure is to Get the Customers Ledger Report by account no
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomerLedgers]
(  
	@XmlDoc XML  
)  
AS  
BEGIN  
	
	Declare @AcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@ReportMonths INT
		,@CurrentDate DATETIME 
		,@BUID VARCHAR(50)=''
		,@GlobalAcountNo VARCHAR(50)

	SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()	 

	SELECT  
		  @AcountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)')   
		  ,@ReportMonths = C.value('(ReportMonths)[1]','INT') 
		  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('LedgerBe') as T(C)  
	
	SELECT @GlobalAcountNo = GlobalAccountNumber FROM [UDV_CustomerDescription](NOLOCK) CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND (BU_ID=@BUID OR @BUID='') AND ActiveStatusId=1

   IF @GlobalAcountNo IS NOT NULL
   BEGIN
			DECLARE @ResultTable TABLE(Id INT IDENTITY(1,1),CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))
			DECLARE @FinalResult TABLE(Id INT,CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))

			DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
		    
			INSERT INTO @ResultedMonths
			SELECT [YEAR],[MONTH] FROM DBO.fn_GetCurrentYearMonths_ByCurrentDate(@CurrentDate)-- ORDER BY Id DESC
									
			INSERT INTO @ResultTable (CreateDate,Particulars,Debit,Credit,Balance,Remarks)
			SELECT OpeningDate,'Opening Balance',TotalPendingAmount,0,0,''
			FROM dbo.fn_GetCustomerYear_OpeningBalance(@GlobalAcountNo)	
			

			SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
			---- Collection all the transaction Start
			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
			  BEGIN      
				SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
				
				SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
				SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

				INSERT INTO @ResultTable (CreateDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks)
				SELECT CreatedDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks FROM 
				(SELECT CONVERT(DATE,CreatedDate) AS CreatedDate
						,BillNo AS ReferenceNo
						,'Sale of energy' As Particulars
						,0 AS Debit
						,TotalBillAmountWithTax As Credit
						,0 AS Balance
						,'' As Remarks
					FROM Tbl_CustomerBills(NOLOCK)
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
					--AND BillMonth = @PresentMonth AND BillYear = @PresentYear
				UNION
					SELECT 
						CONVERT(DATE,CreatedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),BillAdjustmentId) AS ReferenceNo
						,'Adjustment' As Particulars
						,0 AS Debit
						,AmountEffected As Credit,0 AS Balance,Remarks
					FROM Tbl_BillAdjustments(NOLOCK) 
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				UNION
					SELECT CONVERT(DATE,RecievedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),CustomerPaymentID) AS ReferenceNo
						,(SELECT BillingType FROM Tbl_MBillingTypes WHERE BillingTypeId = CP.ReceivedDevice)+' Payments' As Particulars
						,PaidAmount AS Debit
						,0 As Credit
						,0 AS Balance
						,Remarks
					FROM Tbl_CustomerPayments(NOLOCK) CP
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,RecievedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				) AS A ORDER By CONVERT(DATE,A.CreatedDate)	
				  
				  IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
							IF(@MonthId IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Collection all the transaction END

			---- Finalise the transaction Records Start

			DECLARE @PresentTrans INT 
			SELECT TOP(1) @PresentTrans = Id FROM @ResultTable ORDER BY Id ASC    

			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultTable WHERE Id >= @PresentTrans ORDER BY Id ASC))    
			  BEGIN 
					
					IF NOT EXISTS(SELECT 0 FROM @FinalResult WHERE Id < @PresentTrans)
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 THEN ((0 - Debit)) 
								   WHEN Credit <> 0 THEN (0 + Credit)
								   ELSE 0 END)AS Balance
							,CreateDate
							,Remarks	   
						FROM @ResultTable WHERE Id = @PresentTrans
					END
				ELSE
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) - Debit))
								 WHEN Credit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) + Credit))
									END)AS Balance
							,CreateDate
							,Remarks		
						FROM @ResultTable WHERE Id = @PresentTrans
					END
					
				  IF(@PresentTrans = (SELECT TOP(1) Id FROM @ResultTable ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentTrans = (SELECT TOP(1) Id FROM @ResultTable WHERE  Id > @PresentTrans ORDER BY Id ASC)    
							IF(@PresentTrans IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Finalise the transaction Records End
		    
		   
				 SELECT 
					  (GlobalAccountNumber + ' - ' + AccountNo) AS GlobalAccountNumber
					  --,KnownAs AS Name
					  ,GlobalAccountNumber AS AccountNo
					  ,(SELECT dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)) AS Name
					  ,ISNULL(ISNULL(HomeContactNo,ISNULL(BusinessContactNo,OtherContactNo)),'--') AS MobileNo
					  ,ISNULL(OldAccountNo,'--') AS OldAccountNo
					  ,CD.ClassName 
					  ,ISNULL(EmailId,'--') AS EmailId
					  ,CD.BusinessUnitName
					  ,CD.ServiceUnitName
					  ,CD.ServiceCenterName
					  ,CD.BookNo
					  ,CD.CycleName
					  ,ISNULL(CD.MeterNumber,'--') AS MeterNumber
					  ,dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,
										Service_City,'',Service_ZipCode) AS ServiceAddress
					  ,1 AS IsSuccess
					  ,CONVERT(VARCHAR,CAST(ISNULL(AvgReading,0) AS DECIMAL(18,2)),-1) AS AvgReading
					  ,CONVERT(VARCHAR,CAST(OutStandingAmount AS MONEY),-1) AS OutStandingAmount
					  ,CD.ReadCodeID
					  ,ReadCode
					  ,0 AS IsCustExistsInOtherBU
			   FROM [UDV_CustomerDescription](NOLOCK) CD
			   INNER JOIN Tbl_MReadCodes(NOLOCK) RC ON RC.ReadCodeId = CD.ReadCodeID
					AND GlobalAccountNumber = @GlobalAcountNo
			
				SELECT 
					 ROW_NUMBER() OVER(ORDER BY Particulars) AS RowNumber
					,Particulars
					,CONVERT(VARCHAR,CAST(ISNULL(Debit,0) AS MONEY),-1) AS Debit
					,CONVERT(VARCHAR,CAST(ISNULL(Credit,0) AS MONEY),-1) AS Credit
					,CONVERT(VARCHAR,CAST(ISNULL(Balance,0) AS MONEY),-1) AS Balance
					,CONVERT(VARCHAR,CAST((-1*ISNULL(Balance,0)) AS MONEY),-1) As Due		
					,CONVERT(VARCHAR,CONVERT(VARCHAR(50),CreateDate,106)) AS TransactionDate
					,ReferenceNo
					,Remarks
				FROM @FinalResult 
			
	END
    ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND BU_ID!=@BUID AND ActiveStatusId=1)
    BEGIN
		
			SELECT 1 AS IsSuccess,1 AS IsCustExistsInOtherBU
	
    END
    ELSE
    BEGIN
		
			SELECT 0 AS IsSuccess
	
    END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetRolesByBU]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Faiz-ID103
-- Create date: 08-May-2015  
-- Description: To Get Roles Details By BU
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetRolesByBU]
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
 DECLARE	@RoleId INT
			,@BU_ID VARCHAR(50)  
 SELECT @RoleId=C.value('(RoleId)[1]','INT') 
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)') 
 FROM @XmlDoc.nodes('UserManagementBE') AS T(C)  
  
 SELECT (  
		SELECT R.RoleId,RoleName
		FROM Tbl_MRoles R    
		JOIN Tbl_UserDetails U on U.RoleId =R.RoleId 
		JOIN Tbl_UserLoginDetails ULD ON ULD.UserId = U.UserId AND ULD.ActiveStatusId = 1
		JOIN Tbl_UserBusinessUnits UB ON UB.UserId = U.UserId AND (UB.BU_ID = @BU_ID OR @BU_ID = '')
		WHERE (R.RoleId=@RoleId OR @RoleId='')  
				AND R.RoleId !=12 AND IsActive=1  
		GROUP BY R.RoleId,RoleName
  FOR XML PATH('UserManagementBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetUsersByBU]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 08-May-2015
-- Description:	The purpose of this procedure is to get Users by BU
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetUsersByBU]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @BU_ID VARCHAR(50)
			,@RoleId INT
	SELECT
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
		,@RoleId=C.value('(RoleId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	SELECT
	(
		SELECT	UB.UserId,
				UserDetailsId
				--,BU_ID 
		FROM Tbl_UserBusinessUnits UB
		LEFT JOIN Tbl_UserLoginDetails ULD ON ULD.UserId = UB.BU_ID 
					AND ULD.ActiveStatusId = 1
		JOIN Tbl_UserDetails UD ON UD.UserId = UB.UserId AND RoleId = @RoleId
		WHERE BU_ID = @BU_ID AND UB.ActiveStatusId = 1 
		ORDER BY UserId ASC
		FOR XML PATH('ApprovalsList'),TYPE
	)
	FOR XML PATH(''),ROOT('ApprovalsInfoByXml')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 08 May 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
CREATE PROCEDURE [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount 
			,@BookGroup = CycleName
			,@BookName = BookNo
			,@Tariff = ClassName
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY CustomerPaymentID DESC
    
    SELECT 
    (
		SELECT CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
			,@LastPaidAmount AS LastPaidAmount
			,@GlobalAccountNo AS AccountNo
			,@Name AS Name
			,@ServiceAddress AS ServiceAddress
			,@OutstandingAmount AS OutStandingAmount
			,@BookGroup AS BookGroup 
			,@BookName AS BookName
			,@Tariff AS Tariff
		FOR XML PATH('Customerdetails'),TYPE 
    )
    ,
    (
		SELECT TOP(1)
			 CB.BillNo
			,CustomerBillId AS CustomerBillID
			,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
			,CB.BillYear        
			,CB.BillMonth        
			,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
			,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
			,ISNULL(CB.PreviousReading,'0') AS PreviousReading
			,ISNULL(CB.PresentReading,'0') AS PresentReading
			,Usage AS Consumption
			,Dials AS MeterDials
			,NetEnergyCharges
			,NetFixedCharges
			,TotalBillAmount
			,VAT
			,TotalBillAmountWithTax
			,NetArrears
			,TotalBillAmountWithArrears 
			,CB.AccountNo AS AccountNo
			,CB.PaidAmount AS LastPaidAmount
		FROM Tbl_CustomerBills CB 
		WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
		ORDER BY CB.CustomerBillId DESC
		FOR XML PATH('CustomerBillDetails'),TYPE 
    )
    FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')            
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentLogsToApprov_New]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerAdjustmentLogsToApprov_New]
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)  
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			,T.AdjustmentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			,T.BillNumber
			,T.BillAdjustmentTypeId
			,T.MeterNumber 
			,CASE WHEN T.BillAdjustmentTypeId = 1 THEN T.PreviousReading ELSE '--' END AS PreviousReading
			,CASE WHEN T.BillAdjustmentTypeId = 1 THEN T.CurrentReadingAfterAdjustment ELSE '--'  END AS CurrentReadingAfterAdjustment
			,CASE WHEN T.BillAdjustmentTypeId = 1 THEN T.CurrentReadingBeforeAdjustment ELSE '--'   END AS CurrentReadingBeforeAdjustment
			,CASE WHEN T.BillAdjustmentTypeId = 1 THEN T.CurrentReadingBeforeAdjustment ELSE '--'   END AS AdjustedUnits
			,T.TaxEffected
			,T.TotalAmountEffected
			,T.EnergryCharges
			,T.AdditionalCharges
			,T.Remarks AS Details
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_AdjustmentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	INNER JOIN Tbl_BillAdjustmentType BAT ON BAT.Name= T.BillAdjustmentTypeId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	
END
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_IsProcessedMeterReadingApproval]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 06-05-2015
-- Description: To get that Customer meter reading approval is processed or not
-- =============================================  
CREATE PROCEDURE [dbo].[USP_IsProcessedMeterReadingApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @LastApprovedLogId INT  
		,@LastProcessedLogId INT
		,@GlobalAccountNumber VARCHAR(50)
		,@IsProcess BIT = 11

	SELECT  
		 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)

	SELECT @LastApprovedLogId = CustomerReadingLogId FROM Tbl_CustomerReadingApprovalLogs 
		WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApproveStatusId = 2

	SELECT @LastProcessedLogId = CustomerReadingLogId FROM Tbl_CustomerReadingApprovalLogs 
		WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApproveStatusId = 1

	IF(@LastApprovedLogId > @LastProcessedLogId)
		BEGIN
			SET @IsProcess = 0
		END
 
	SELECT @IsProcess AS IsApprovalInProcess
	FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerReadingsChangeApproval]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Karteek
-- Modified Date: 05-05-2015
-- Description: update the status of the meter readings approval 
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerReadingsChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT  
		,@MeterReadingLogId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@IsFinalApproval BIT = 0
		,@IsRollOver BIT 
		,@Details VARCHAR(200) 
		,@MeterReadingFromId INT
		,@GlobalAccountNo VARCHAR(50)
		,@BU_ID VARCHAR(50) 
	SELECT  
		 @MeterReadingLogId = C.value('(MeterReadingLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@IsRollOver = C.value('(IsRollOver)[1]','BIT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
		,@MeterReadingFromId = C.value('(MeterReadingFromId)[1]','INT')
		,@GlobalAccountNo = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--						RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerReadingApprovalLogs 
					--								WHERE CustomerReadingLogId = @MeterReadingLogId))
					
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerReadingApprovalLogs 
											WHERE CustomerReadingLogId = @MeterReadingLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					--DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
							SET @IsFinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @IsFinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					SET @IsFinalApproval = 1
				END
				
			IF(@IsFinalApproval = 1)
				BEGIN
					--DECLARE @ReadId INT
					--Declare @IsExistingRollOver bit
					--DECLARE @IsExits BIT
					
					--SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=CR.IsRollOver
					--	,@IsExits = (Case when CONVERT(DATE,CR.ReadDate) >= CONVERT(DATE,CRA.ReadDate) then 1 else 0 end)
					--FROM Tbl_CustomerReadings CR
					--INNER JOIN Tbl_CustomerReadingApprovalLogs CRA ON CRA.GlobalAccountNumber = CR.GlobalAccountNumber
					--	AND CR.GlobalAccountNumber = @GlobalAccountNo AND CRA.CustomerReadingLogId = @MeterReadingLogId
					--	AND CR.IsBilled = 0
					--ORDER BY CustomerReadingId DESC
					
					--IF(@IsExits = 1)	
					--	BEGIN 
					--		IF @IsExistingRollOver = 1
					--			BEGIN
					--				DELETE FROM	Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
					--			END
					--		ELSE
					--			BEGIN
					--				DELETE FROM	Tbl_CustomerReadings where   CustomerReadingId =   @ReadId    
					--			END	
					--	END
					
					IF(@IsRollOver = 1)
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId != @MeterReadingLogId AND CustomerReadingLogId != @MeterReadingLogId+1)
							AND ApproveStatusId = 1
																											
							INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole)	
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)
							
							INSERT INTO Tbl_CustomerReadings(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver)
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)
							
							DECLARE @MaxPreviousReading VARCHAR(20), @MaxPresentReading VARCHAR(20), @MaxAverageReading VARCHAR(20)
							
							SELECT
								 @MaxPreviousReading = MAX(CR.PreviousReading)
								,@MaxPresentReading = MIN(CR.PresentReading) 
								,@MaxAverageReading = MAX(CR.AverageReading) 
							FROM Tbl_CustomerReadingApprovalLogs CR 
							WHERE CR.GlobalAccountNumber = @GlobalAccountNo 
							AND (CR.CustomerReadingLogId = @MeterReadingLogId OR CR.CustomerReadingLogId = @MeterReadingLogId+1)
															
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							SET InitialReading = @MaxPreviousReading
								,PresentReading = @MaxPresentReading
								,AvgReading = CONVERT(NUMERIC,@MaxAverageReading)
							WHERE GlobalAccountNumber = @GlobalAccountNo 
						END	
					ELSE
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId != @MeterReadingLogId
							AND ApproveStatusId = 1
							
							INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole)	
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId
							
							INSERT INTO Tbl_CustomerReadings(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver)
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId
									
							UPDATE CD
							SET CD.InitialReading = CR.PreviousReading
								,CD.PresentReading = CR.PresentReading 
								,CD.AvgReading = CONVERT(NUMERIC,CR.AverageReading)
							FROM CUSTOMERS.Tbl_CustomerActiveDetails CD
							INNER JOIN Tbl_CustomerReadingApprovalLogs CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber
									AND CR.GlobalAccountNumber = @GlobalAccountNo 
									AND CR.CustomerReadingLogId = @MeterReadingLogId
						END
				END
			ELSE
				BEGIN
					IF(@IsRollOver = 1)
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)  
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)	
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3 
								,CurrentApprovalLevel = 0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId != @MeterReadingLogId AND CustomerReadingLogId != @MeterReadingLogId+1)
							AND ApproveStatusId = 1														
						END	
					ELSE
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)  
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId	
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3 
								,CurrentApprovalLevel = 0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId != @MeterReadingLogId
							AND ApproveStatusId = 1												
						END	
				END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerReadingApprovalLogs
							--									WHERE CustomerReadingLogId = @MeterReadingLogId))
							
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerReadingApprovalLogs 
																WHERE CustomerReadingLogId = @MeterReadingLogId )

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerReadingApprovalLogs 
						WHERE CustomerReadingLogId = @MeterReadingLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			IF(@IsRollOver = 1)
				BEGIN
					UPDATE Tbl_CustomerReadingApprovalLogs 
					SET   
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,Remarks = @Details
						--,CurrentApprovalLevel=0
						,IsLocked=1
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)	
				END
			ELSE 
				BEGIN
					UPDATE Tbl_CustomerReadingApprovalLogs 
					SET   
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,Remarks = @Details
						--,CurrentApprovalLevel=0
						,IsLocked=1
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND CustomerReadingLogId = @MeterReadingLogId 
				END
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBookwiseMeterReadings_WithDT]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 04-05-2015
-- This is to insert the meter readings bookwise bulk into logs 
-- =============================================
CREATE PROCEDURE  [dbo].[USP_InsertBookwiseMeterReadings_WithDT]
(
	 @MeterReadingFrom INT
	,@CreateBy varchar(50)
	,@BU_ID VARCHAR(50)
	,@IsFinalApproval BIT
	,@FunctionId INT
	,@ActiveStatusId INT
	,@TempReadings TblMeterReadingsUpload READONLY
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess BIT = 0

	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT 
		,@CurrentApprovalLevel INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempReadings
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
							BEGIN
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
							END
						ELSE
							BEGIN
								DECLARE @UserDetailedId INT
								SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
								SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
							END
							
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
												
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = ISNULL(Usage,0)
				,@AccountNo = AccNum
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
				,@Multiple = Multiplier
			 FROM @TempReadings WHERE SNo = @SNo
			 
			IF(@IsExists = 1)
				BEGIN
					DECLARE @ReadId INT
					Declare @IsExistingRollOver bit
					
					SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
					FROM Tbl_CustomerReadingApprovalLogs
					WHERE GlobalAccountNumber = @AccountNo
					ORDER BY CustomerReadingLogId DESC
						 
					IF @IsExistingRollOver = 1
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
					END
					ELSE
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
					END	
				END
			 
			SELECT @MeterNumber = MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @AccountNo 
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)		
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				,(CASE WHEN @IsFinalApproval = 1 THEN 2 ELSE @ActiveStatusId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @PresentRoleId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @NextRoleId END)
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				
			IF(@IsFinalApproval = 1)
				BEGIN
				
					INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver
						,ApproveStatusId
						,PresentApprovalRole
						,NextApprovalRole)
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						,@ActiveStatusId
						,(CASE WHEN @PresentRoleId = 0 THEN @RoleId ELSE @PresentRoleId END)
						,(CASE WHEN @NextRoleId = 0 THEN @RoleId ELSE @NextRoleId END))
				
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = @PreviousReading
						,PresentReading = @PresentReading 
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
				END
				
			SET @IsSuccess = 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			SET @Multiple = NULL
			
		END
	
	SELECT @IsSuccess AS IsSuccess 
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithDebitBal_New_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 05-June-2014
-- Description:	Get The Details of Debit Balence Of The Customers
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetAccountsWithDebitBal_New_Rajaiah]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)			
			,@BookNo VARCHAR(MAX)
			,@MinAmt VARCHAR(50)
			,@MaxAmt VARCHAR(50)
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@MinAmt=C.value('(MinAmt)[1]','VARCHAR(50)')
			,@MaxAmt=C.value('(MaxAmt)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	
	IF(@MinAmt = '')
		SET @MinAmt = (SELECT MIN(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails)
	IF(@MaxAmt = '')
		SET @MaxAmt = (SELECT MAX(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails)
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
			
	;WITH PagedResults AS
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
				,A.AccountNo AS AccountNo
				,CD.GlobalAccountNumber AS GlobalAccountNumber
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
				,OutStandingAmount AS TotalDueAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(A.AccountNo)),0) AS LastPaidAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(A.AccountNo)),'--') AS LastPaidDate
				,CD.OldAccountNo
				,C.CycleName
				,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
				,PD.SortOrder AS CustomerSortOrder
				,BN.SortOrder AS BookSortOrder
				,BU.BusinessUnitName AS BusinessUnitName
				,SU.ServiceUnitName AS ServiceUnitName
				,SC.ServiceCenterName AS ServiceCenterName
				,COUNT(0) OVER() AS TotalRecords
		 FROM Tbl_CustomerBills A
		 INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = A.AccountNo
		 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
		 INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
		 INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
		 INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
		 INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
		 INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		 
	)
	
	--SELECT 
	--(
		SELECT	 
			 RowNumber
			,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,TotalDueAmount
			,LastPaidAmount
			,LastPaidDate
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(SELECT SUM(TotalDueAmount) FROM PagedResults ) AS DueAmount
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	--	FOR XML PATH('Reports'),TYPE
	--)
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')

END
--------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithCreditBal_New_Rajaiah]    Script Date: 05/26/2015 20:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 23-Aug-2014
-- Description:	Get The Details of Credit Balance Of The Customers
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetAccountsWithCreditBal_New_Rajaiah]
(
	@XmlDoc xml
)
AS
BEGIN

		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('ReportsBe') AS T(C)
	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(		
		SELECT ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber				
			,CD.GlobalAccountNumber AS GlobalAccountNumber
			,CD.AccountNo AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
			,(SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)) AS LastPaidAmount		
			,ISNULL(CONVERT(VARCHAR(50),(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)) ,106),'--') AS LastPaidDate
			,CAD.OutStandingAmount As OverPayAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalBillsAmount(CD.GlobalAccountNumber)),0) As TotalBillsAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalPaidAmount(CD.GlobalAccountNumber)),0) As TotalPaidAmount
			,BN.BookCode AS BookCode
			,CD.OldAccountNo
			,C.CycleName
			,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
			,PD.SortOrder AS CustomerSortOrder
			,BN.SortOrder AS BookSortOrder
			,BU.BusinessUnitName
			,SU.ServiceUnitName
			,SC.ServiceCenterName
			,COUNT(0) OVER() AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomersDetail CD(NOLOCK) 
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD(NOLOCK) ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD(NOLOCK) ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			AND CAD.OutStandingAmount < 0
		INNER JOIN dbo.Tbl_BookNumbers AS BN(NOLOCK) ON BN.BookNo = PD.BookNo 
		INNER JOIN dbo.Tbl_Cycles AS C(NOLOCK) ON C.CycleId = BN.CycleId 
		INNER JOIN dbo.Tbl_ServiceCenter AS SC(NOLOCK) ON SC.ServiceCenterId = C.ServiceCenterId
		INNER JOIN dbo.Tbl_ServiceUnits AS SU(NOLOCK) ON SU.SU_ID = SC.SU_ID 
		INNER JOIN dbo.Tbl_BussinessUnits AS BU(NOLOCK) ON BU.BU_ID = SU.BU_ID
		INNER JOIN dbo.Tbl_MTariffClasses AS TC(NOLOCK) ON PD.TariffClassID = TC.ClassID 		
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
	)
	
	--SELECT 
	--(
		SELECT
			 RowNumber
			,GlobalAccountNumber
			,AccountNo
			,Name
			,ServiceAddress
			,LastPaidAmount		
			,LastPaidDate
			,OverPayAmount
			,TotalBillsAmount
			,TotalPaidAmount
			,BookCode
			,OldAccountNo
			,CycleName
			,BookNumber 
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,(SELECT SUM(OverPayAmount) FROM PagedResults) AS TotalCreditBalance				
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	--	FOR XML PATH('Reports'),TYPE
	--)
	--FOR XML PATH(''),ROOT('ReportsBeInfoByXml')

END
------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingLogs]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04 may 2015
-- Description: TO insert the customer meter readings into the logs table
-- =============================================   
CREATE PROCEDURE [dbo].[USP_InsertCustomerMeterReadingLogs]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
		
		,@IsFinalApproval BIT
		,@FunctionId INT
		,@ActiveStatusId INT
		,@IsExists BIT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
		,@IsFinalApproval=C.value('(IsFinalApproval)[1]','bit')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')
		,@IsExists = C.value('(IsExists)[1]','BIT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	DECLARE @RoleId INT, @CurrentLevel INT
			,@PresentRoleId INT, @NextRoleId INT
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level			
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
		BEGIN
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
					BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
					END
				ELSE
					BEGIN
						DECLARE @UserDetailedId INT
						SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
						SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					END
					
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				ELSE
				BEGIN
					SET @CurrentApprovalLevel = @CurrentLevel+1
				END 
										
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
	
		END
	ELSE
		BEGIN
			SET @IsFinalApproval=1
			SET @PresentRoleId=0
			SET @NextRoleId=0
			SET @CurrentLevel=0	
			SET @CurrentApprovalLevel =0
		END
			
	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum
	
	IF(@IsExists = 1)
		BEGIN
			DECLARE @ReadId INT
			Declare @IsExistingRollOver bit
			
			SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
			FROM Tbl_CustomerReadingApprovalLogs
			WHERE GlobalAccountNumber = @AccNum
			ORDER BY CustomerReadingLogId DESC
				 
			IF @IsExistingRollOver = 1
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
			END
			ELSE
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
			END	
		END
	
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	 
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);		  
		END
		
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
	
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)	
			VALUES(
				 @AccNum
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@Previous)
				,@FirstPresentValue
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,@IsRollover
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				

			IF(@IsRollover=1)
				BEGIN
		 
					IF @SecountReadingUsage != 0
						BEGIN
						--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
						 IF	 @SecountReadingUsage > 0
										 INSERT INTO Tbl_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole
											,CurrentApprovalLevel
											,IsLocked)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,@SecoundReadingPrevReading
											,CONVERT(VARCHAR(50),@Current)
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
											
						END
				END
				
		IF(@IsFinalApproval = 1)
			BEGIN		
			
				INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver
					,ApproveStatusId
					,PresentApprovalRole
					,NextApprovalRole)	
				VALUES(
					@AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),@Previous)
					,@FirstPresentValue
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover
					,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)	
				
				INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver)
				VALUES(
					 @AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),@Previous)
					,@FirstPresentValue
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover)
					

				IF(@IsRollover=1)
					BEGIN
			 
						IF @SecountReadingUsage != 0
							BEGIN
							--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
							 IF	 @SecountReadingUsage > 0
								 BEGIN
									INSERT INTO Tbl_CustomerReadings(
												 GlobalAccountNumber
												,[ReadDate]
												,[ReadBy]
												,[PreviousReading]
												,[PresentReading]
												,[AverageReading]
												,[Usage]
												,[TotalReadingEnergies]
												,[TotalReadings]
												,[Multiplier]
												,[ReadType]
												,[CreatedBy]
												,[CreatedDate]
												,[IsTamper]
												,MeterNumber
												,[MeterReadingFrom]
												,IsRollOver)
											VALUES(
												 @AccNum
												,@ReadDate
												,@ReadBy
												,@SecoundReadingPrevReading
												,CONVERT(VARCHAR(50),@Current)
												,@AverageReading
												,@SecountReadingUsage
												,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
												,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
												,@Multiple
												,2
												,@CreateBy
												,GETDATE()
												,@IsTamper
												,@MeterNumber
												,@MeterReadingFrom
												,@IsRollover)
												
										INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,@SecoundReadingPrevReading
											,CONVERT(VARCHAR(50),@Current)
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)
								END												
							END
					END
					
			 
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
				SET InitialReading = @Previous
					,PresentReading = @Current 
					,AvgReading = CONVERT(NUMERIC,@AverageReading) 
				WHERE GlobalAccountNumber = @AccNum
			END
		
	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected  
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected   
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected  
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = @AdjustmentLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff with parent classes>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses] 
	(
	@XmlDoc XML
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			Cd.ClassName AS Tariff,
			CD.ActiveStatus AS CustomerStatus,
			ISNULL(CBills.LastPaymentTotal,0) AS RevenueCollected,
			ISNULL(CBills.TotalBillAmountWithTax,0) AS AmountBilled,
			ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance,
			MTC.Description AS TariffDescription,
			--CD.RefClassID 
			CD.TariffId As RefClassID
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerPDFReport CD(NOLOCK) ON 
						CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
						AND CD.GlobalAccountNumber=CBills.AccountNo
	--INNER JOIN Tbl_MTariffClasses MTC(NOLOCK) ON MTC.ClassID=CD.RefClassID 
	INNER JOIN Tbl_MTariffClasses MTC(NOLOCK) ON MTC.ClassID=CD.TariffId 

	SELECT RefClassID, 
		TariffDescription,
		Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(AmountBilled) AS AmountBilled,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) As ClossingBalance
		INTO #tmpCustomerStatisticsInfo
	FROM
	(
		SELECT	RefClassID,
			TariffDescription,
			Tariff, 
			COUNT(CustomerStatus) AS TotalCustomerTariff,
			CustomerStatus,
			SUM(EnergyBilled) AS EnergyBilled, 
			SUM(RevenueBilled) AS RevenueBilled,
			SUM(RevenueCollected) AS RevenueCollected,
			SUM(AmountBilled) AS AmountBilled,
			SUM(OpeningBalance) AS OpeningBalance,
			SUM(ClossingBalance) AS ClossingBalance
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff,TariffDescription,RefClassID 
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff,TariffDescription,RefClassID 
	ORDER BY Tariff

	
	SELECT ROW_NUMBER() OVER(ORDER BY RefClassID  ASC) AS ID,
		RefClassID ,
		TariffDescription,
		'SUB Total' AS Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		--SUM (ISNULL(Hold,0) ) AS Hold,
		SUM(ISNULL(TotalPopulation,0)) AS TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(AmountBilled) AS AmountBilled,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) As ClossingBalance
		INTO #tmpCustomerStatisticsInfoSubTotal
	FROM #tmpCustomerStatisticsInfo
	GROUP BY TariffDescription,RefClassID 
	
	--==========================================================================================================
	DECLARE @tblCustomerStatistics AS TABLE
	(
		ID INT IDENTITY(1,1),
		RefClassID INT,
		TariffDescription VARCHAR(8000),
		Tariff VARCHAR(300),
		Active INT,
		InActive INT,
		--Hold INT,
		TotalPopulation INT,
		EnergyBilled DECIMAL(18,2),
		RevenueBilled DECIMAL(18,2),
		RevenueCollected DECIMAL(18,2),		
		AmountBilled DECIMAL(18,2),
		OpeningBalance DECIMAL(18,2),
		ClossingBalance DECIMAL(18,2)
	)
	DECLARE @index INT, @rwCount INT,@RefClassID INT 
	SET @index=0
	SELECT @rwCount=COUNT(ID) FROM #tmpCustomerStatisticsInfoSubTotal
	
	--SELECT * FROM #tmpCustomerStatisticsInfoSubTotal
	
	-- Loop for caluclating sub total
	WHILE @index<@rwCount
	BEGIN
		SET @index=@index+1
		
		SELECT @RefClassID=RefClassID FROM #tmpCustomerStatisticsInfoSubTotal WHERE ID=@index
		
		INSERT INTO @tblCustomerStatistics(RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance)
		SELECT RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance
		FROM #tmpCustomerStatisticsInfo WHERE RefClassID=@RefClassID
		
		INSERT INTO @tblCustomerStatistics(RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance)
		SELECT RefClassID,	'' AS TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance
		FROM #tmpCustomerStatisticsInfoSubTotal WHERE ID=@index

	END
	
	INSERT INTO @tblCustomerStatistics(RefClassID,	TariffDescription,	Tariff,Active,InActive,--Hold INT,
		TotalPopulation,EnergyBilled,	RevenueBilled,RevenueCollected,AmountBilled,
		OpeningBalance,	ClossingBalance)
	SELECT 0 AS RefClassID ,
		'' AS TariffDescription,
		'GRAND Total' AS Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		SUM(ISNULL(TotalPopulation,0)) AS TotalPopulation,
		SUM(EnergyBilled) AS EnergyBilled ,
		SUM(RevenueBilled) As RevenueBilled,
		SUM(RevenueCollected) As RevenueCollected,
		SUM(AmountBilled) AS AmountBilled,
		SUM(OpeningBalance) AS OpeningBalance,
		SUM(ClossingBalance) As ClossingBalance		
	FROM #tmpCustomerStatisticsInfoSubTotal 
	

	SELECT ID,TariffDescription,Tariff AS TariffClass,Active,InActive,--Hold INT,
		TotalPopulation,
		CONVERT(VARCHAR(20),CAST(EnergyBilled AS MONEY),-1) AS EnergyBilled,	
		CONVERT(VARCHAR(20),CAST(RevenueBilled AS MONEY),-1) AS RevenueBilled,
		CONVERT(VARCHAR(20),CAST(RevenueCollected AS MONEY),-1) AS RevenueCollected,
		CONVERT(VARCHAR(20),CAST(AmountBilled AS MONEY),-1) AS AmountBilled,
		CONVERT(VARCHAR(20),CAST(OpeningBalance AS MONEY),-1) AS OpeningBalance,	
		CONVERT(VARCHAR(20),CAST(ClossingBalance AS MONEY),-1) AS ClossingBalance
	FROM @tblCustomerStatistics
	ORDER BY ID
	--==========================================================================================================

	DROP TABLE #tmpCustomerStatistics	
	DROP TABLE #tmpCustomerStatisticsInfo
	DROP TABLE #tmpCustomerStatisticsInfoSubTotal

END



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariff]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariff] 
	(
	@XmlDoc XML
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    
    -- Insert statements for procedure here
	SELECT	CD.GlobalAccountNumber,
			CBills.Usage,
			ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,
			ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,
			Cd.ClassName AS Tariff,
			CD.ActiveStatus AS CustomerStatus,
			ISNULL(CBills.LastPaymentTotal,0) AS RevenueCollected,
			ISNULL(CD.OutStandingAmount,0) AS ClossingBalance
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerPDFReport CD(NOLOCK) ON 
	CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
	AND CD.GlobalAccountNumber=CBills.AccountNo

	SELECT Tariff,
		SUM(ISNULL(Active,0) ) AS Active,
		SUM(ISNULL(InActive,0)) AS InActive,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled ,
		CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  As RevenueBilled,
		CONVERT(VARCHAR(20),CAST(SUM(RevenueCollected)  AS MONEY),-1) As RevenueCollected,
		CONVERT(VARCHAR(20),CAST(SUM(ClossingBalance) AS MONEY),-1)  As ClossingBalance
	FROM
	(
		SELECT	Tariff, 
			COUNT(CustomerStatus) AS TotalCustomerTariff,
			CustomerStatus,
			SUM(EnergyBilled) AS EnergyBilled, 
			SUM(RevenueBilled) AS RevenueBilled,
			SUM(RevenueCollected) AS RevenueCollected,
			SUM(ClossingBalance) AS ClossingBalance
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff
	ORDER BY Tariff

	DROP TABLE #tmpCustomerStatistics
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  <Author,,Rajaiah>    
-- Create date: <Create Date,,03/14/2015>    
-- Description: <Description,,Billing status for customer service center>    
-- =============================================      
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter]   
 (  
 @XmlDoc XML=null    
 )   
AS    
BEGIN    
 
 SET NOCOUNT ON;    
 DECLARE @Month INT,  
   @Year INT,  
   @BU_ID VARCHAR(100)    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
  ,@Year = C.value('(Year)[1]','INT')    
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    
    
    -- Insert statements for procedure here    
SELECT CT.CustomerType,    
   CD.GlobalAccountNumber,    
   CBills.Usage,    
   ISNULL(CBills.NetEnergyCharges,0) AS EnergyBilled,    
   ISNULL(CBills.NetEnergyCharges,0) + ISNULL(CBills.NetFixedCharges,0) AS RevenueBilled,    
   Cd.ServiceCenterName AS ServiceCenter,    
   CD.ActiveStatus AS CustomerStatus,    
   RC.ReadCode,    
   ISNULL(CBills.PaidAmount,0) AS Payment,    
   ISNULL(CBills.PreviousBalance,0) AS OpeningBalance,    
   ISNULL(CD.OutStandingAmount,0) AS ClossingBalance    
   INTO #tmpCustomerServices    
 FROM tbl_customerbills CBills(NOLOCK)      
 INNER  JOIN UDV_CustomerPDFReport CD(NOLOCK) ON     
   CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID    
   AND CD.GlobalAccountNumber=CBills.AccountNo    
 INNER JOIN Tbl_MCustomerTypes CT(NOLOCK) ON CT.CustomerTypeId=CD.CustomerTypeId    
 INNER JOIN Tbl_MReadCodes RC(NOLOCK) ON RC.ReadCodeId=CBills.ReadCodeId     
    
 SELECT 
  CustomerType,    
  ServiceCenter,    
  SUM(ISNULL(Active,0) ) AS Active,    
  SUM(ISNULL(InActive,0)) AS InActive,  
  SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,    
  SUM(EnergyBilled) AS EnergyBilled ,    
  SUM(RevenueBilled) As RevenueBilled,    
  SUM(ISNULL(Minimum,0) ) AS [MIN FC],    
  SUM(ISNULL([Read],0)) AS [Read],    
  SUM(ISNULL(Estimate,0) ) AS EST,    
  SUM(ISNULL(Direct,0)) AS Direct,    
  SUM(Payment) AS Payment,    
  SUM(OpeningBalance) AS OpeningBalance,    
  SUM(ClossingBalance) AS ClossingBalance    
  INTO #tmpCustomerServicesInfo    
 FROM    
 (    
  SELECT CustomerType,    
   ServiceCenter,     
   COUNT(CustomerStatus) AS TotalCustomerTariff,      
   CustomerStatus,    
   SUM(EnergyBilled) AS EnergyBilled,     
   SUM(RevenueBilled) AS RevenueBilled,    
   COUNT(ReadCode) AS ReadCodeType,    
   ReadCode,    
   SUM(Payment) AS Payment,    
   SUM(OpeningBalance) AS OpeningBalance,    
   SUM(ClossingBalance) AS ClossingBalance    
  FROM #tmpCustomerServices    
  GROUP BY CustomerStatus,ServiceCenter,CustomerType,ReadCode    
 ) ListOfCustomers    
 PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)    
 ) AS  ListOfCustomersT    
 PIVOT (SUM(ReadCodeType) FOR ReadCode IN(Direct,[Read],Estimate,Minimum)) AS  ListOfReadCodeType    
 GROUP BY ServiceCenter,CustomerType    
 ORDER BY CustomerType,ServiceCenter    
    
    
 SELECT ROW_NUMBER() OVER(ORDER BY CustomerType ASC) AS ID,    
  CustomerType,    
  'SUB Total' AS ServiceCenter,    
  SUM(ISNULL(Active,0) ) AS Active,    
  SUM(ISNULL(InActive,0)) AS InActive,  
  SUM(ISNULL(TotalPopulation,0)) as TotalPopulation,    
  SUM(EnergyBilled) AS EnergyBilled ,    
  SUM(RevenueBilled) As RevenueBilled,    
  SUM(ISNULL([MIN FC],0) ) AS [MIN FC],    
  SUM(ISNULL([Read],0)) AS [Read],    
 SUM(ISNULL(EST,0) ) AS EST,    
  SUM(ISNULL(Direct,0)) AS Direct,    
  SUM(Payment) AS Payment,    
  SUM(OpeningBalance) AS OpeningBalance,    
  SUM(ClossingBalance) AS ClossingBalance    
  INTO #tmpCustomerServicesInfoSubTotal    
 FROM #tmpCustomerServicesInfo    
 GROUP BY CustomerType    
    
 --==========================================================================================================    
 DECLARE @tblCustomerServices AS TABLE    
 (    
  ID INT IDENTITY(1,1),    
  CustomerType VARCHAR(100),    
  ServiceCenter VARCHAR(300),    
  Active INT,    
  InActive INT,    
  --Hold INT,    
  TotalPopulation INT,    
  EnergyBilled DECIMAL(18,2),    
  RevenueBilled DECIMAL(18,2),    
  [MIN FC] INT,    
  [Read] INT,    
  EST INT,     
  Direct INT,    
  Payment DECIMAL(18,2),    
  OpeningBalance DECIMAL(18,2),    
  ClossingBalance DECIMAL(18,2)    
 )    
 DECLARE @index INT, @rwCount INT,@CustomerType VARCHAR(100)     
 SET @index=0    
 SELECT @rwCount=COUNT(ID) FROM #tmpCustomerServicesInfoSubTotal    
    
 -- Loop for caluclating sub total    
 WHILE @index<@rwCount    
 BEGIN    
  SET @index=@index+1    
      
  SELECT @CustomerType=CustomerType FROM #tmpCustomerServicesInfoSubTotal WHERE ID=@index    
      
  INSERT INTO @tblCustomerServices(CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,Direct,Payment,    
  OpeningBalance,ClossingBalance)    
  SELECT CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,    
  Direct,Payment,OpeningBalance,ClossingBalance    
  FROM #tmpCustomerServicesInfo WHERE CustomerType=@CustomerType    
      
  INSERT INTO @tblCustomerServices(CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,Direct,Payment,    
  OpeningBalance,ClossingBalance)    
  SELECT CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,    
  Direct,Payment,OpeningBalance,ClossingBalance    
  FROM #tmpCustomerServicesInfoSubTotal WHERE ID=@index    
    
 END    
     
 INSERT INTO @tblCustomerServices(CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
 TotalPopulation,EnergyBilled,RevenueBilled,[MIN FC],[Read],EST,Direct,Payment,    
 OpeningBalance,ClossingBalance)    
 SELECT '' AS CustomerType,    
  'GRAND Total' AS ServiceCenter,    
  SUM(ISNULL(Active,0) ) AS Active,    
  SUM(ISNULL(InActive,0)) AS InActive, 
  SUM(ISNULL(TotalPopulation,0)) as TotalPopulation,    
  SUM(EnergyBilled) AS EnergyBilled ,    
  SUM(RevenueBilled) As RevenueBilled,    
  SUM(ISNULL([MIN FC],0) ) AS [MIN FC],    
  SUM(ISNULL([Read],0)) AS [Read],    
  SUM(ISNULL(EST,0) ) AS EST,    
  SUM(ISNULL(Direct,0)) AS Direct,    
  SUM(Payment) AS Payment,    
  SUM(OpeningBalance) AS OpeningBalance,    
  SUM(ClossingBalance) AS ClossingBalance    
 FROM #tmpCustomerServicesInfoSubTotal     
     
    
 SELECT ID,CustomerType,ServiceCenter,Active,InActive,--Hold INT,    
  TotalPopulation,
   CONVERT(VARCHAR(20),CAST(EnergyBilled AS MONEY),-1) AS EnergyBilled,
   CONVERT(VARCHAR(20),CAST(RevenueBilled AS MONEY),-1) AS RevenueBilled,
  [MIN FC] AS MINFC,[Read],EST,    
  Direct,
  CONVERT(VARCHAR(20),CAST(Payment AS MONEY),-1) AS Payment,
  CONVERT(VARCHAR(20),CAST(OpeningBalance AS MONEY),-1) AS OpeningBalance,
  CONVERT(VARCHAR(20),CAST(ClossingBalance AS MONEY),-1) AS ClossingBalance     
 FROM @tblCustomerServices    
 ORDER BY ID    
 --==========================================================================================================    
    
 DROP TABLE #tmpCustomerServices    
 DROP TABLE #tmpCustomerServicesInfo    
 DROP TABLE #tmpCustomerServicesInfoSubTotal    
    
     
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillMonths]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Faiz-ID103
-- Create date: 02-May-2015
-- Description: The purpose of this procedure is to get list of Bill Months
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillMonths]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @Year INT  
   
 SELECT   
  @Year = C.value('(Year)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
  SELECT   
    (  
   SELECT DISTINCT  
    [Month]  
    ,(SELECT dbo.[fn_GetMonthName]([Month])) AS [MonthName]  
   FROM Tbl_BillingMonths  
   WHERE [YEAR] = @Year  AND OpenStatusId = 2--Only Closed Months
   FOR XML PATH('BillGenerationList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
    
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillYears]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Faiz-ID103
-- Create date: 02-May-2015
-- Description: The purpose of this procedure is to get list of Bill Years  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillYears]  
AS  
BEGIN  
   
  SELECT   
    (  
   SELECT DISTINCT  [Year]      
   FROM Tbl_BillingMonths  
   WHERE OpenStatusId = 2--Only Closed Years
   ORDER BY [Year]
   FOR XML PATH('BillGenerationList'),TYPE  
  )  
  FOR XML PATH(''),ROOT('BillGenerationInfoByXml')   
    
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerPaymentsApproval]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 01-05-2015
-- Description: Update Payments change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerPaymentsApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@AccountNo VARCHAR(50) 
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@PaymentLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@ReceiptNumber VARCHAR(20)
		,@PaymentMode INT
		,@PaidAmount  DECIMAL(18,2)
		,@PadiDate DATETIME
		,@CustomerPaymentId INT
		,@PaymentFromId INT
		,@DocumentPath VARCHAR(MAX)
		,@Cashier VARCHAR(50)
		,@CashOffice INT
		,@DocumentName VARCHAR(MAX)
		,@PaymentType INt
		,@BatchNo INT
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @PaymentLogId = C.value('(PaymentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	SELECT			
		@GlobalAccountNo=GlobalAccountNumber
		,@AccountNo=AccountNo
		,@ReceiptNumber=ReceiptNumber
		,@PaymentMode=PaymentMode
		,@PaidAmount=PaidAmount
		,@PadiDate=PadiDate
		,@Remarks=Remarks
		,@PaymentFromId=PaymentFromId
		,@DocumentPath=DocumentPath
		,@Cashier=Cashier
		,@CashOffice=CashOffice
		,@DocumentName=DocumentName
		,@PaymentType=PaymentType
		,@BatchNo=BatchNo
	FROM Tbl_PaymentsLogs
	WHERE PaymentLogId=@PaymentLogId

	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_PaymentsLogs 
					--						WHERE PaymentLogId = @PaymentLogId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_PaymentsLogs 
											WHERE  PaymentLogId = @PaymentLogId)

				
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END

					
				
					DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
					
				IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
					UPDATE Tbl_PaymentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND PaymentLogId = @PaymentLogId 
						
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @GlobalAccountNo
						,@ReceiptNumber
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PadiDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()		
					
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@GlobalAccountNo,@PaidAmount) 
					
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					UPDATE CB 
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]	
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					
					INSERT INTO Tbl_Audit_PaymentsLogs(	
						PaymentLogId
						,GlobalAccountNumber 
						,AccountNo 
						,ReceiptNumber 
						,PaymentMode  
						,PaidAmount 
						,PadiDate  
						,PresentApprovalRole  
						,NextApprovalRole  
						,ApproveStatusId  
						,Remarks 
						,CreatedBy 
						,CreatedDate
						,PaymentFromId)  
					VALUES(  
						 @PaymentLogId
						,@GlobalAccountNo
						,@AccountNo 
						,@ReceiptNumber 
						,@PaymentMode  
						,@PaidAmount 
						,@PadiDate  
						,@PresentRoleId  
						,@NextRoleId  
						,@ApprovalStatusId  
						,@Remarks 
						,@ModifiedBy 
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_PaymentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
									,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND PaymentLogId = @PaymentLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_PaymentsLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
							,CurrentApprovalLevel= 0
						,IsLocked=1
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND PaymentLogId = @PaymentLogId 

					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,Cashier
						,CashOffice
						,DocumentName
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @GlobalAccountNo
						,@ReceiptNumber
						,@PaymentMode
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PadiDate
						,@PaymentType
						,@PaidAmount
						,(CASE WHEN @BatchNo = 0 THEN NULL ELSE @BatchNo END)
						,1
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					)
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()					
				
					INSERT INTO @PaidBills
					(
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					)
					SELECT 
						 CustomerBillId
						,BillNo
						,PaidAmount
						,BillStatus
					FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@GlobalAccountNo,@PaidAmount) 
					
					INSERT INTO Tbl_CustomerBillPayments
					(
						 CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,CreatedBy
						,CreatedDate
					)
					SELECT 
						 @CustomerPaymentId
						,PaidAmount
						,BillNo
						,CustomerBillId
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					FROM @PaidBills
					
					UPDATE CB 
					SET CB.ActiveStatusId = PB.BillStatus
						,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
					FROM Tbl_CustomerBills CB
					INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
					
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]	
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @GlobalAccountNo
					
					INSERT INTO Tbl_Audit_PaymentsLogs(	
						PaymentLogId
						,GlobalAccountNumber 
						,AccountNo 
						,ReceiptNumber 
						,PaymentMode  
						,PaidAmount 
						,PadiDate  
						,PresentApprovalRole  
						,NextApprovalRole  
						,ApproveStatusId  
						,Remarks 
						,CreatedBy 
						,CreatedDate
						,PaymentFromId)  
					VALUES(  
						 @PaymentLogId
						,@GlobalAccountNo
						,@AccountNo 
						,@ReceiptNumber 
						,@PaymentMode  
						,@PaidAmount 
						,@PadiDate  
						,@PresentRoleId  
						,@NextRoleId  
						,@ApprovalStatusId  
						,@Remarks 
						,@ModifiedBy 
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						)
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_PaymentsLogs 
							--									WHERE PaymentLogId = @PaymentLogId))
									SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_PaymentsLogs 
											WHERE  PaymentLogId = @PaymentLogId)
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_PaymentsLogs 
						WHERE PaymentLogId = @PaymentLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_PaymentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND PaymentLogId = @PaymentLogId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBookCustomersCount_ByCycles_New]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: Karteek
-- Create date  : 01 May 2015
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetDisableBookCustomersCount_ByCycles_New]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE	@CycleId VARCHAR(MAX)
			,@BillMonth INT
			,@BillYear INT
			
	SELECT   		
		@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BillMonth = C.value('(BillingMonth)[1]','INT')
		,@BillYear = C.value('(BillingYear)[1]','INT')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
		SELECT 
			COUNT(GlobalAccountNumber) AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD
		INNER JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID AND CD.ActiveStatusId IN (1,2)
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CD.BookNo
			AND BN.CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN Tbl_BillingDisabledBooks DB ON DB.BookNo = BN.BookNo AND DB.IsActive = 1 
			AND DB.MonthId = @BillMonth AND DB.YearId = @BillYear
		FOR XML PATH('BillGenerationBe')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffDetails_ByCycles]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author  : Karteek 
-- Create date  : 01 MAY 2015  
-- Description  : To Get the Tariff wise customers and usage under the selected FeederId and Cycles  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffDetails_ByCycles]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE   
          --@FeederId VARCHAR(50)  
    @CycleIds VARCHAR(MAX)  
   ,@Year INT  
   ,@Month INT  
     
 SELECT       
  --@FeederId = C.value('(FeederId)[1]','VARCHAR(50)')  
   @CycleIds = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@Year = C.value('(Year)[1]','INT')  
  ,@Month = C.value('(Month)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
   Declare @CustomerStatus varchar(max) = '1,2'
   
  
select CR.GlobalAccountNumber,SUM(isnull(Usage,0))as Usage
INTO #ReadCustomersList
from Tbl_CustomerReadings CR
INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))   
GROUP BY CR.GlobalAccountNumber,ClusterCategoryId


 SELECT  
  ( 
	  
		SELECT 
			ClassID AS TariffId,
			TC.ClassName AS TariffName
			--,SUM(case when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3 
			--		then (case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 
			--									else (case when isnull(BDB.DisableTypeId,2) =1 then 0 else 1 end) end)
			--		else 0 end) as [Read]
			--,SUM(case when (CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and (isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
			--			then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
			--			then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end ) as TotalDirectCustomers			
			,(SUM(case when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3 
					then (case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 
												else (case when isnull(BDB.DisableTypeId,2) =1 then 0 else 1 end) end)
					else 0 end)	 
				+
				SUM(case when (CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and (isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
						then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
						then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )) as TotalRecords
			--,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
			--	(case when isnull(BDB.IsPartialBill,1)=1 then 
			--	ISNULL(CR.Usage,CAD.AvgReading) else 0 end ) else 0 end) TotalUsageForReadCustomers
			--,SUM(isnull(DCAVG.AverageReading,isnull(CAD.AvgReading,0))) as TotalUsageForDirectCustomers
			--,SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) as TotalNonReadCustomersUsage	 
			--,SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) as NonUploadedCustomersUsage	 
			,(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
				(case when isnull(BDB.IsPartialBill,1)=1 then 
				ISNULL(CR.Usage,CAD.AvgReading) else 0 end ) else 0 end) 
				+
				SUM(isnull(DCAVG.AverageReading,isnull(CAD.AvgReading,0)))
				+
				SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end)
				+
				SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end)
			) as Usage
		from Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
		inner Join Tbl_MTariffClasses(NOLOCK) TC on CPD.TariffClassID=TC.ClassID	  
			and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
		INNER JOIN	CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
		INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo	  
			AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  
		Left JOIN Tbl_BillingDisabledBooks  BDB ON isnull(BDB.BookNo,0)=BN.BookNo and isnull(BDB.IsActive,0)=1 
		LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
		LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1	 
		Group By  TC.ClassID,TC.ClassName  
		ORDER BY   TC.ClassID
	FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')  
   
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerDirectAverageChangeApproval]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- Description: Update name change Details of a customer after approval
-- Modified By: Bhimaraju Vanka	
-- Modified Date: 06-05-2015
-- Description: Updating Direct Cust Avg Reading Main Table
-- =============================================  
CREATE PROCEDURE [dbo].[USP_CustomerDirectAverageChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @AccountNo VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)   
		,@Remarks VARCHAR(50)  
		,@ApprovalStatusId INT  
		,@AverageReading DECIMAL(18,2)
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@AverageChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @AverageChangeLogId = C.value('(AverageChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	SELECT 
		@GlobalAccountNo = GlobalAccountNumber
		,@AccountNo = AccountNo
		,@Remarks = Remarks
		,@AverageReading = NewAverage
	FROM Tbl_DirectCustomersAverageChangeLogs
	WHERE AverageChangeLogId = @AverageChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_DirectCustomersAverageChangeLogs 
					--						WHERE AverageChangeLogId = @AverageChangeLogId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_DirectCustomersAverageChangeLogs 
											WHERE AverageChangeLogId = @AverageChangeLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

				DECLARE @FinalApproval BIT

				IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
					BEGIN
							SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
					END
				ELSE
					BEGIN
						DECLARE @UserDetailedId INT
						SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					END


					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_DirectCustomersAverageChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
									,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AverageChangeLogId = @AverageChangeLogId
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
								SET AvgReading=CONVERT(NUMERIC,@AverageReading)
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
									,ModifedBy=@ModifiedBy
							WHERE GlobalAccountNumber=@GlobalAccountNo

						
						IF EXISTS (SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNo)
						BEGIN
							--Print 'Avg reading for customer exiting- update the avg reading'
							Update Tbl_DirectCustomersAvgReadings
							SET AverageReading=@AverageReading
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNo
						END
						ELSE
						BEGIN
							--Print 'Avg reading for customer Not exiting- Insert the avg reading'
							Insert Into Tbl_DirectCustomersAvgReadings(
										GlobalAccountNumber
										,AverageReading
										,ActiveStatusId
										,CreatedBy
										,CreatedDate
										)
							Values		(
										@GlobalAccountNo
										,@AverageReading
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										)
						END

						END
					ELSE -- Not a final approval
						BEGIN
								UPDATE Tbl_DirectCustomersAverageChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AverageChangeLogId = @AverageChangeLogId  

							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
						UPDATE Tbl_DirectCustomersAverageChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel= 0
								,IsLocked=1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AverageChangeLogId = @AverageChangeLogId
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
								SET AvgReading=CONVERT(NUMERIC,@AverageReading)
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
									,ModifedBy=@ModifiedBy
							WHERE GlobalAccountNumber=@GlobalAccountNo
 
						IF EXISTS (SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNo)
						BEGIN
							--Print 'Avg reading for customer exiting- update the avg reading'
							Update Tbl_DirectCustomersAvgReadings
							SET AverageReading=@AverageReading
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNo
						END
						ELSE
						BEGIN
							--Print 'Avg reading for customer Not exiting- Insert the avg reading'
							Insert Into Tbl_DirectCustomersAvgReadings(
										GlobalAccountNumber
										,AverageReading
										,ActiveStatusId
										,CreatedBy
										,CreatedDate
										)
							Values		(
										@GlobalAccountNo
										,@AverageReading
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										)
						END
				
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_DirectCustomersAverageChangeLogs 
							--				WHERE AverageChangeLogId = @AverageChangeLogId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_DirectCustomersAverageChangeLogs 
											WHERE AverageChangeLogId = @AverageChangeLogId)
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_DirectCustomersAverageChangeLogs 
											WHERE AverageChangeLogId = @AverageChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_DirectCustomersAverageChangeLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AverageChangeLogId = @AverageChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- Modified By : Bhimaraju V
-- Modified Date:06-05-2015    
-- Description: Updating the Direct Cust Avg reading in Main Table
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT CONVERT(DECIMAL(18,2),ISNULL(Temp.AverageReading,0)) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,ISNULL(CD.GlobalAccountNumber,'') AS AccNum
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
		--							AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--	when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
		--	else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CPD.ReadCodeID
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions K
				WHERE AccountNo = (ISNULL(CD.GlobalAccountNumber,0))
				AND K.AvgUploadFileId = Temp.AvgUploadFileId GROUP BY AccountNo) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions(NOLOCK) AS Temp
	INNER JOIN Tbl_AverageUploadFiles(NOLOCK) PUF ON PUF.AvgUploadFileId = Temp.AvgUploadFileId  
	INNER JOIN Tbl_AverageUploadBatches(NOLOCK) PUB ON PUB.AvgUploadBatchId = PUF.AvgUploadBatchId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (Temp.AccountNo = CD.OldAccountNo) --CD.GlobalAccountNumber = Temp.AccountNo OR 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions(NOLOCK)) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the given AverageReading is valid or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AverageReading <= 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
					
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,AccNum
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			FROM #AverageReadings
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
				,ReadCodeID
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber
			
			--WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
	    
		    UPDATE CAD   
			SET CAD.AvgReading = Tc.AverageReading
			FROM CUSTOMERS.Tbl_CustomerActiveDetails CAD
			INNER JOIN #ValidReadings TC ON TC.AccNum = CAD.GlobalAccountNumber
			AND TC.ReadCodeID=1-- For Direct CustomersOnly updating the avg reading
		     
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentLogsToApprove]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerAdjustmentLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
      ,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			,T.AdjustmentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			,T.BillNumber
			,T.BillAdjustmentTypeId
			,T.MeterNumber 
			,T.PreviousReading
			,T.CurrentReadingAfterAdjustment
			,T.CurrentReadingBeforeAdjustment 
			,T.AdjustedUnits
			,T.TaxEffected
			,T.TotalAmountEffected
			,T.EnergryCharges
			,T.AdditionalCharges
			,T.Remarks AS Details
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_AdjustmentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	--INNER JOIN Tbl_BillAdjustmentType BAT ON BAT.Name= T.BillAdjustmentTypeId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
END
--------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPaymentLogsToApprove]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerPaymentLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
			 ,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			,T.PaymentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			,ISNULL(T.ReceiptNumber,'--') AS ReceiptNumber
			,PM.PaymentMode
			,T.PaidAmount
			,ISNULL(T.Remarks,'--') AS Details
			,CONVERT(VARCHAR(20),T.PadiDate,107) AS PaidDate
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_PaymentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	INNER JOIN Tbl_MPaymentMode PM ON PM.PaymentModeId = T.PaymentMode
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDirectCustomersAverageUploadLogsToApprove]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetDirectCustomersAverageUploadLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
			,@ApprovalRoleId INT
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
			,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
		
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			,T.AverageChangeLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			,T.PreviousAverage
			,T.NewAverage
			,ISNULL(T.Remarks,'--') AS Details
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_DirectCustomersAverageChangeLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterReadingLogsToApprove]    Script Date: 05/26/2015 20:07:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetMeterReadingLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
			,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
  
	SELECT  
		 ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		,T.CustomerReadingLogId AS MeterReadingLogId
		,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,(T.GlobalAccountNumber) AS AccountNumber
		,CD.OldAccountNo AS OldAccountNumber  
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,ISNULL(T.MeterNumber,'--') AS MeterNo
		,ISNULL(T.PreviousReading,'--') AS PreviousReading
		,ISNULL(T.PresentReading,'--') AS PresentReading
		,ISNULL(T.AverageReading,'--') AS AverageUsage
		,ISNULL(CONVERT(VARCHAR(20),T.Usage),'--') AS Usage
		,CONVERT(VARCHAR(20),T.ReadDate,107) AS ReadDate
		,ISNULL(T.Remarks,'--') As Details
		,MRF.MeterReadingFrom
		,MRF.MeterReadingFromId
		,(CASE WHEN T.NextApprovalRole IS NULL 
					THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
					ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
					END) AS [Status]
		--,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
		--		THEN 1
		--		ELSE 0
		--		END) AS IsPerformAction 
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId
				 AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
		,T.IsRollOver
	FROM Tbl_CustomerReadingApprovalLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MRF ON MRF.MeterReadingFromId= T.MeterReadingFrom
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	--WHERE T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
END
--------------------------------------------------------------------------------------------------
GO


