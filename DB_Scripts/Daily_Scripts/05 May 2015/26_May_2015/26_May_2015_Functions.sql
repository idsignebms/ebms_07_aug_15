
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithArrears_New]    Script Date: 05/26/2015 19:35:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  A Jeevan
-- Create date: 26-05-2015  
-- Description: The purpose of this function is to get the Last Bill Amount With Arrears 
-- =============================================  
CREATE FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithArrears_New]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithArrears DECIMAL(18,2)  
  
 SELECT TOP(1) 
 @BillAmountWithArrears =  ISNULL(TotalBillAmountWithArrears,0) 
 FROM Tbl_CustomerBills 
 Where AccountNo = @AccountNo   
 ORDER BY CustomerBillId DESC   
      
 RETURN @BillAmountWithArrears   
  
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetLastTransactionDateByGlobalAccountNumber]    Script Date: 05/26/2015 19:35:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 22-May-2015
-- Description:	To Get the last transaction date of the custoemr
-- =============================================
CREATE FUNCTION [dbo].[fn_GetLastTransactionDateByGlobalAccountNumber]
(
	@GlobalAccountNo VARCHAR(50)
)
RETURNS DateTime
AS
BEGIN
	--DECLARE @GlobalAccountNo VARCHAR(50) = '0000057170'
	DECLARE @ModifiedDate DateTime

	DECLARE @Tbl_TransactionDates AS Table (ModifiedDate DATETIME) 

	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNO
	
	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessPersonDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerIdentityDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerTenentDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_TamperedCustomers WHERE GlobalAccountNumber = @GlobalAccountNO
	
	--SELECT * FROM @Tbl_TransactionDates ORDER BY ModifiedDate desc
	SELECT TOP 1 @ModifiedDate=ModifiedDate FROM @Tbl_TransactionDates ORDER BY ModifiedDate DESC
	--Print  @ModifiedDate
	RETURN @ModifiedDate

END

GO

/****** Object:  UserDefinedFunction [dbo].[GetLastTransactionDateByGlobalAccountNumber]    Script Date: 05/26/2015 19:35:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 22-May-2015
-- Description:	To Get the last transaction date of the custoemr
-- =============================================
CREATE FUNCTION [dbo].[GetLastTransactionDateByGlobalAccountNumber]
(
	@GlobalAccountNo VARCHAR(50)
)
RETURNS DateTime
AS
BEGIN
	--DECLARE @GlobalAccountNo VARCHAR(50) = '0000057170'
	DECLARE @ModifiedDate DateTime

	DECLARE @Tbl_TransactionDates AS Table (ModifiedDate DATETIME) 

	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNO
	
	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessPersonDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerIdentityDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerTenentDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_TamperedCustomers WHERE GlobalAccountNumber = @GlobalAccountNO
	
	--SELECT * FROM @Tbl_TransactionDates ORDER BY ModifiedDate desc
	SELECT TOP 1 @ModifiedDate=ModifiedDate FROM @Tbl_TransactionDates ORDER BY ModifiedDate DESC
	--Print  @ModifiedDate
	RETURN @ModifiedDate

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_IsAccountNoExists_BU_Id_Payments]    Script Date: 05/26/2015 19:35:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
CREATE FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id_Payments]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF  EXISTS(SELECT 0 FROM UDV_IsCustomerExists 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId <> 4 
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=1
		ELSE
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetOutstandingAmount]    Script Date: 05/26/2015 19:35:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 12-May-2015
-- Description:	The purpose of this function is to get outstanding amount.
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOutstandingAmount]
(
	@GlobalAccountNo varchar(50)
)
RETURNS Decimal(18,4)
AS
BEGIN
	
	DECLARE @OutstandingAmount DECIMAL(18,4)

	SELECT @OutstandingAmount=OutStandingAmount 
	From CUSTOMERS.Tbl_CustomerActiveDetails 
	WHERE GlobalAccountNumber = @GlobalAccountNo
	
	RETURN @OutstandingAmount

END

GO



GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithArrears_New]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  A Jeevan
-- Create date: 26-05-2015  
-- Description: The purpose of this function is to get the Last Bill Amount With Arrears 
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithArrears_New]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithArrears DECIMAL(18,2)  
  
 SELECT TOP(1) 
 @BillAmountWithArrears =  ISNULL(TotalBillAmountWithArrears,0) 
 FROM Tbl_CustomerBills 
 Where AccountNo = @AccountNo   
 ORDER BY CustomerBillId DESC   
      
 RETURN @BillAmountWithArrears   
  
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithArrears]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author   : M.Padmini  
-- Create date: 20-Feb-15
-- Description: This function is used to get customer previousbillamountwithArrears.
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithArrears]  
(  
@GlobalAccountNo VARCHAR(50)
,@BillMonth INT
,@BillYear  INT
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithTax DECIMAL(18,2)
  ,@MaxCustomerBillId INT 
  ,@BillCount INT
  --,@PresDate DATE
  --,@PrevDate DATE
  
  --      SET @PresDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		--SET @PrevDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		
	 --================ Getting 2nd heighest CustomerBillId===================
	 SET @BillCount= (SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
	 IF(@BillCount>=2)
	 BEGIN
		 SELECT  TOP 1 @MaxCustomerBillId= MAX(CustomerBillId)
		 FROM Tbl_CustomerBills   
		 WHERE AccountNo =@GlobalAccountNo 
		 AND CustomerBillId <(SELECT MAX(CustomerBillId) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
		 GROUP BY CustomerBillId
		 ORDER BY CustomerBillId DESC 
		 
		 --================= Getting Previous Bill Amount===============
		 
		 SELECT @BillAmountWithTax= TotalBillAmountWithArrears FROM Tbl_CustomerBills
		 WHERE CustomerBillId=@MaxCustomerBillId
		 AND AccountNo=@GlobalAccountNo
		 --AND BillMonth<DATEPART(MONTH, DATEADD(MONTH,+1,CONVERT(DATE, '2014-'+convert(VARCHAR(2),@BillMonth)+'-01'))) 
		 --AND BillYear=@BillYear
	 END
	 ELSE
	 BEGIN
		SET @BillAmountWithTax=(SELECT OpeningBalance FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber=@GlobalAccountNo)
	 END
 SET @BillAmountWithTax = ISNULL(@BillAmountWithTax,0) 
  
      
 RETURN @BillAmountWithTax   
  
END  



GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetLastTransactionDateByGlobalAccountNumber]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 22-May-2015
-- Description:	To Get the last transaction date of the custoemr
-- =============================================
ALTER FUNCTION [dbo].[fn_GetLastTransactionDateByGlobalAccountNumber]
(
	@GlobalAccountNo VARCHAR(50)
)
RETURNS DateTime
AS
BEGIN
	--DECLARE @GlobalAccountNo VARCHAR(50) = '0000057170'
	DECLARE @ModifiedDate DateTime

	DECLARE @Tbl_TransactionDates AS Table (ModifiedDate DATETIME) 

	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNO
	
	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessPersonDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerIdentityDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerTenentDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_TamperedCustomers WHERE GlobalAccountNumber = @GlobalAccountNO
	
	--SELECT * FROM @Tbl_TransactionDates ORDER BY ModifiedDate desc
	SELECT TOP 1 @ModifiedDate=ModifiedDate FROM @Tbl_TransactionDates ORDER BY ModifiedDate DESC
	--Print  @ModifiedDate
	RETURN @ModifiedDate

END

GO

/****** Object:  UserDefinedFunction [dbo].[GetLastTransactionDateByGlobalAccountNumber]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 22-May-2015
-- Description:	To Get the last transaction date of the custoemr
-- =============================================
ALTER FUNCTION [dbo].[GetLastTransactionDateByGlobalAccountNumber]
(
	@GlobalAccountNo VARCHAR(50)
)
RETURNS DateTime
AS
BEGIN
	--DECLARE @GlobalAccountNo VARCHAR(50) = '0000057170'
	DECLARE @ModifiedDate DateTime

	DECLARE @Tbl_TransactionDates AS Table (ModifiedDate DATETIME) 

	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNO
	
	-- INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	--SELECT ModifiedDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_ApplicationProcessPersonDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerIdentityDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomerTenentDetails WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_CustomersDetail WHERE GlobalAccountNumber = @GlobalAccountNO
	
	 INSERT INTO @Tbl_TransactionDates(ModifiedDate) 
	SELECT ISNULL(ModifiedDate,CreatedDate) FROM CUSTOMERS.Tbl_TamperedCustomers WHERE GlobalAccountNumber = @GlobalAccountNO
	
	--SELECT * FROM @Tbl_TransactionDates ORDER BY ModifiedDate desc
	SELECT TOP 1 @ModifiedDate=ModifiedDate FROM @Tbl_TransactionDates ORDER BY ModifiedDate DESC
	--Print  @ModifiedDate
	RETURN @ModifiedDate

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaidAmount]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the Last Paid Amount  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastPaidAmount]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
   ,@LastPaidAmount DECIMAL(18,2)  
  
 SELECT TOP(1) --@LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106),  
 @LastPaidAmount = PaidAmount FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY RecievedDate DESC    , CustomerPaymentID DESC
 --ORDER BY CreatedDate DESC --Raja-ID065  
  
 SET @LastPaidAmount = ISNULL(@LastPaidAmount,0)  
      
 RETURN @LastPaidAmount   
  
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaymentDate]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the Last Payment Date  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastPaymentDate]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
  
 -- SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,103) FROM Tbl_CustomerPayments -- comented by Faiz-ID103
 SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106) FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY RecievedDate DESC  , CustomerPaymentID DESC
 --ORDER BY CreatedDate DESC-- Raja -ID065  
    
 RETURN @LastPaymentDate   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  UserDefinedFunction [dbo].[fn_IsAccountNoExists_BU_Id_Payments]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
ALTER FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id_Payments]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF  EXISTS(SELECT 0 FROM UDV_IsCustomerExists 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId <> 4 
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=1
		ELSE
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_CheckIsPageAccess]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 02-May-2014
-- Description:	The purpose of this function is to check Page Access Or Not
-- Author:		Faiz-ID103
-- Create date: 11-May-2015
-- Description:	Report validation as per new functionality is added
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckIsPageAccess]
(
			 @RoleId INT
			,@PageName VARCHAR(MAX)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
			,@ReferenceMenuId INT
			
			--Getting referance menu id for reports --Faiz-ID103
			SET @ReferenceMenuId = (SELECT Top 1 ReferenceMenuId from Tbl_Menus WHERE [Path] = '../Reports/'+@PageName )
	
		IF EXISTS(	SELECT 
				-- P.MenuId 
				--,M.Name
				----,M.MenuId
				--,M.Path
				--,P.Role_Id
				--,P.IsActive
				TOP(1) P.IsActive
				
			FROM Tbl_NewPagePermissions P
			JOIN Tbl_Menus M ON M.MenuId=P.MenuId
			WHERE path like '%'+@PageName+'%' 
				AND P.IsActive=1 
				AND P.Role_Id=@RoleId
				AND M.IsActive=1)
				BEGIN
				SET @IsExists=1
				END
		--Checking if the page belongs to any sub report starts		--Faiz-ID103
		ELSE IF Exists (SELECT 0 FROM Tbl_NewPagePermissions P
						JOIN Tbl_Menus M ON M.MenuId=P.MenuId
						WHERE [Path] = '../Reports/'+@PageName 
						AND ReferenceMenuId = @ReferenceMenuId 
						AND P.IsActive=1 
						AND P.Role_Id=@RoleId
						AND M.IsActive=1  )
		BEGIN
				SET @IsExists=1
		END
		--Checking if the page belongs to any sub report starts		--
		ELSE
		BEGIN
		SET @IsExists=0
		END
				
	
	RETURN @IsExists

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetOutstandingAmount]    Script Date: 05/26/2015 19:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 12-May-2015
-- Description:	The purpose of this function is to get outstanding amount.
-- =============================================
ALTER FUNCTION [dbo].[fn_GetOutstandingAmount]
(
	@GlobalAccountNo varchar(50)
)
RETURNS Decimal(18,4)
AS
BEGIN
	
	DECLARE @OutstandingAmount DECIMAL(18,4)

	SELECT @OutstandingAmount=OutStandingAmount 
	From CUSTOMERS.Tbl_CustomerActiveDetails 
	WHERE GlobalAccountNumber = @GlobalAccountNo
	
	RETURN @OutstandingAmount

END

GO

/****** Object:  UserDefinedFunction [MASTERS].[fn_GenCustomerAccountNo]    Script Date: 05/26/2015 19:37:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Author:		NEERAJ KANOJIYA

-- Create date: 263-DEC-2014

-- DESCRIPTION:	THE PURPOSE OF THIS FUNCTION IS TO GENERATE CUSTOMER ACCOUNTNO IN NEW CUSTOMER TABLE
--Modified By : Bhimaraju V
--ModifiedDate: 13-Feb-15
--Modified By : Bhimaraju V
--ModifiedDate: 13-Feb-15
--Desc : Adding BookGroup(Cycle) also
-- =============================================

ALTER FUNCTION [MASTERS].[fn_GenCustomerAccountNo]

(

 @BU_ID VARCHAR(20)
,@SU_Id VARCHAR(20)
,@SC_Id VARCHAR(20)
,@CycleId VARCHAR(20)
,@BookNo VARCHAR(20)
)

RETURNS VARCHAR(50)

AS

BEGIN

	DECLARE  @Count INT
			,@UniqueId VARCHAR(50)
			,@Prefix VARCHAR(20)
			,@PrefixLength INT
			,@Id VARCHAR(50)
			,@BookCode VARCHAR(10)
			,@ServiceUnitCode VARCHAR(10)
			,@ServiceCenterCode VARCHAR(10)
			,@BusinessUnitCode VARCHAR(10)
			,@CycleCode VARCHAR(10)

		

	SELECT @BusinessUnitCode = ISNULL(BUCode,'') FROM Tbl_BussinessUnits WHERE BU_ID = @BU_ID

	SELECT @ServiceUnitCode = ISNULL(SUCode,'') FROM Tbl_ServiceUnits WHERE SU_ID = @SU_Id

	SELECT @ServiceCenterCode = ISNULL(SCCode,'') FROM Tbl_ServiceCenter	 WHERE ServiceCenterId = @SC_Id
	SELECT @CycleCode = ISNULL(CycleCode,'') FROM Tbl_Cycles	 WHERE CycleId = @CycleId
	SELECT @BookCode = ISNULL(BookCode,0) FROM Tbl_BookNumbers WHERE BookNo = @BookNo

	SET @UniqueId=@BusinessUnitCode+@ServiceUnitCode+@ServiceCenterCode+@CycleCode+@BookCode
	RETURN @UniqueId	

	

	--SELECT @Prefix = RIGHT('0000'+LEFT(@BusinessUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceCenterCode,2),2)+RIGHT('0000'+LEFT(@BookCode,3),3)--'BEDC_BU_'

	--SELECT @Count = COUNT(0) FROM CUSTOMERS.Tbl_CustomersDetail WHERE AccountNo LIKE @Prefix+'%'

	--SELECT @Id = MAX(AccountNo) FROM CUSTOMERS.Tbl_CustomersDetail WHERE AccountNo LIKE @Prefix+'%'-- retrives last inserted application no

	

	--SELECT @PrefixLength = LEN(@Prefix)



	--IF(@Count!=0)----first if

	--BEGIN

	

	--	DECLARE @Num VARCHAR(50),@Char VARCHAR(50),@Temp INT

	   	   

	--	SET @Num = SUBSTRING(@Id,@PrefixLength+1,5)	

	--	SET @Char = SUBSTRING(@Id,@PrefixLength+1,1)	

	--	SET @Temp = ASCII(@Char)

		

	--	IF(@Temp>=65)-- Contains A,B...

	--		SET @Num = SUBSTRING(@Id,@PrefixLength+2,5)	

						

	--	IF(CONVERT(VARCHAR(10),@Num) = '99999' OR CONVERT(VARCHAR(10),@Num)='9999')----checks ID last 4 digits are equal to 9999

	--		BEGIN---second if

	--			IF(CONVERT(VARCHAR(10),@Char)='9')

	--				SET @Temp=64

	--			ELSE

	--				SET @Temp =ASCII(@Char)

	--			IF((@Temp >= 64 AND @Temp <= 90) OR (@Temp >=97 AND @Temp <= 122))

	--				BEGIN---third if

					

	--					SET @Temp = @Temp+1---assign as next character like a--b b--c

	--					SELECT @UniqueId = @Prefix+ CONVERT(VARCHAR(50),CHAR(@Temp))+'0001'

						 

	--				END---end of third if

				

	--		END----end of second if

	--	ELSE---if ID last 4 digits are not equal to 9999

	--		BEGIN---else for second if

	--			SET @Num = @Num+1--increments by 1 of last 4 digits of ID.

	--			IF(@Temp>=65)--Contains A,B...

	--				SELECT @UniqueId =  @Prefix+@Char+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),4)

	--			ELSE

	--				SELECT @UniqueId =  @Prefix+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),5)

				

	--		END---end of else for second if

	--END

	--ELSE---else for first if

	--BEGIN	

	--	SELECT @UniqueId = @Prefix+'00001'---if ID exists		

	--END	

	

	

	--RETURN @UniqueId



END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 05/26/2015 19:37:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Satya
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
 
 
Declare @NewAverage decimal(18,2 ),@TotalRecords INT
    SELECT	Top 1
         @NewAverage=AverageReading  
       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
 
 SELECT	@TotalRecords =COUNT(0)       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   
   IF(@TotalRecords>0) 
		SET @NewAverage = (isnull(@NewAverage,0)+@usage)/2
	ELSE
		SET @NewAverage = @usage
			
	RETURN @NewAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAddressFormat_bill]    Script Date: 05/26/2015 19:37:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya
-- Create date: 10-Mar-2015
-- Description:	The purpose of this function is to get the Customer Service Address

-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerAddressFormat_bill]
(

@HouseNo VARCHAR(50)
,@StreetName VARCHAR(50)
--,@Landmark VARCHAR(50)
,@City VARCHAR(50)
--,@Details VARCHAR(50)
,@zipcode VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)


			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN @HouseNo IS NULL OR @HouseNo = '' THEN '' ELSE LTRIM(RTRIM(@HouseNo)) + ',   '  END + 
				CASE WHEN @StreetName IS NULL OR @StreetName = '' THEN '' ELSE  LTRIM(RTRIM(@StreetName)) + ',' END + 
				--CASE WHEN @Landmark IS NULL OR @Landmark = '' THEN '' ELSE LTRIM(RTRIM(@Landmark)) + ', '  END + 
				CASE WHEN @City IS NULL OR @City = '' THEN '' ELSE  LTRIM(RTRIM(@City)) + ',  ' END  + 
				--CASE WHEN @Details IS NULL OR @Details = '' THEN '' ELSE  LTRIM(RTRIM(@Details))+ ', ' END   + 
				CASE WHEN @zipcode IS NULL OR @zipcode = '' THEN '' ELSE  LTRIM(RTRIM(@zipcode))+ ', ' END )) 
			 
	RETURN @ServiceAddress
END


GO



GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentApprovalRoles]    Script Date: 05/26/2015 19:35:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 17 NOV 2014
-- Modified date : 19-11-2014
-- Description:	The purpose of this function is to Get approval roles
--=============================================
ALTER FUNCTION [dbo].[fn_GetCurrentApprovalRoles]
(
@FunctionId INT
,@CurrentLevel INT
,@IsForward BIT
,@BU_ID VARCHAR(50)
)
RETURNS @ResultTemp TABLE(PresentRoleId INT,NextRoleId INT)
AS
BEGIN

	DECLARE @PresentRoleId INT=0
			,@NextRoleId INT=0
			
	DECLARE @Temp TABLE([Level] INT,FunctionId INT,RoleId INT)
	DELETE FROM @ResultTemp 
	DELETE FROM @Temp 

	IF(@IsForward = 1)
		BEGIN
			INSERT INTO  @Temp 
			SELECT 
				TOP 2 [Level],FunctionId,RoleId
			FROM TBL_FunctionApprovalRole 
			WHERE FunctionId = @FunctionId
			AND [Level] > @CurrentLevel
			AND BU_ID=@BU_ID
			ORDER BY [Level] ASC


			SELECT TOP(1) @PresentRoleId = RoleId FROM @Temp ORDER BY [Level] ASC
			SELECT TOP(1) @NextRoleId = RoleId FROM @Temp ORDER BY [Level] DESC
		END
	ELSE
		BEGIN
			INSERT INTO  @Temp 
			SELECT 
				TOP 2 [Level],FunctionId,RoleId
			FROM TBL_FunctionApprovalRole 
			WHERE FunctionId = @FunctionId
			AND [Level] in( @CurrentLevel,@CurrentLevel+1)
			AND BU_ID=@BU_ID
			ORDER BY [Level] ASC


			SELECT TOP(1) @PresentRoleId = RoleId FROM @Temp ORDER BY [Level] ASC 
			SELECT TOP(1) @NextRoleId = RoleId FROM @Temp ORDER BY [Level] DESC
		END	

	INSERT INTO @ResultTemp
	SELECT @PresentRoleId,@NextRoleId 	
		
	RETURN 

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetNormalCustomerConsumption]    Script Date: 05/26/2015 19:35:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,>
-- Description:	<Description,, Generate Normal customer consumption>
-- =============================================

--SELECT *	FROM fn_GetNormalCustomerConsumption('0001329260')

--select* from [fn_GetNormalCustomerConsumption]('0000026917',0)

   
			
			
ALTER FUNCTION [dbo].[fn_GetNormalCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50),
	@OpeningBalance decimal(18,2)	
)
RETURNS @CustomerConsumption TABLE 
(
	-- Add the column definitions for the TABLE variable here
	 GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage DECIMAL(18,2)
	,LastBillGenerated DATETIME
	,BalanceUsage BIT
	,IsFromReading INT
	,CustomerBillId INT
	,PreviousBalance Decimal(18,2)
	,Multiplier INt
	,ReadDate datetime
)
AS
BEGIN
	DECLARE @ResultConsumption VARCHAR(1000),@LastBillGenerated DATETIME,@MeterMultiplier INT, @BalanceUsage INT,@CustomerBillId Int,@PreviousBalance decimal(18,2),@ReadDate Datetime,@LastBillCurrentReading varchar(50)
	
		declare @CustomerReadings TABLE (
			-- Add the column definitions for the TABLE variable here
			 GlobalAccountNumber VARCHAR(50)
			,PreviousReading VARCHAR(50)
			,PresentReading VARCHAR(50)
			,Usage DECIMAL(18,2)
			,LastBillGenerated DATETIME
			,BalanceUsage BIT
			,IsFromReading INT
			,CustomerBillId INT
			,PreviousBalance Decimal(18,2)
			,Multiplier INt
			,ReadDate datetime
			,CustomerReadingId INT 
			,IsRollOver Bit
		)
	-- Add the T-SQL statements to compute the return value here
	-- If reading available
	IF EXISTS(SELECT GlobalAccountNumber FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0)
	BEGIN
		
		 
		SELECT @MeterMultiplier=(SELECT top (1)  MeterMultiplier FROM Tbl_MeterInformation(NOLOCK)    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber))    
		
		SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@BalanceUsage=BalanceUsage
		,@CustomerBillId=CustomerBillId,@PreviousBalance=PreviousBalance
		,@LastBillCurrentReading=PresentReading
		FROM Tbl_CustomerBills (NOLOCK)    
		WHERE AccountNo = @GlobalAccountNumber     
		ORDER BY CustomerBillId DESC   	
			
			IF @PreviousBalance IS NULL
				BEGIN
					select @PreviousBalance=@OpeningBalance
				END
			if @BalanceUsage IS NULL
				SET @BalanceUsage=0
			if @CustomerBillId IS NULL
				SET @CustomerBillId=0
		-- Fill the table variable with the rows for your result set
		INSERT INTO @CustomerReadings(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate
		,CustomerReadingId,IsRollOver)
		SELECT GlobalAccountNumber
			,PreviousReading AS PreviousReading
			,PresentReading AS PresentReading
			,Usage*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,ReadDate
			,CustomerReadingId
			,IsRollOver
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		
		INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate)
		SELECT GlobalAccountNumber
			,case when @LastBillCurrentReading IS NULL then (select top 1 PreviousReading from @CustomerReadings order by CustomerReadingId asc)  else @LastBillCurrentReading end AS PreviousReading
			,(select top 1 PresentReading from @CustomerReadings order by CustomerReadingId asc) AS PresentReading	   
			,sum(isnull(Usage,0))*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,Max(ReadDate)
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		group by GlobalAccountNumber
				
	
	END
	ELSE -- If reading not available
	BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 
		SELECT @IsFirstTimeCustomer=dbo.fn_IsFirstTimeCustomerForBilling(@GlobalAccountNumber)
		
		IF @IsFirstTimeCustomer=0
		BEGIN
		
		 
 		
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT TOP(1) AccountNo AS GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,AverageReading AS Usage
				,BillGeneratedDate AS LastBillGenerated
				,isnull(BalanceUsage,0)
				,0 AS IsFromReading
				,CustomerBillId
				,PreviousBalance
				 
			FROM Tbl_CustomerBills (NOLOCK) 
			WHERE AccountNo = @GlobalAccountNumber 
			ORDER BY CustomerBillId DESC
		END
		ELSE
		BEGIN
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,
			PresentReading,Usage,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,case when AvgReading IS NULL THen  InitialBillingKWh else AvgReading end AS Usage
				,0 AS  BalanceUsage 
				,0 AS IsFromReading
				,0
				,@OpeningBalance
				
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
		END		
	
	END	
	
	RETURN  
	
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint_Customerwise]    Script Date: 05/26/2015 19:35:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint_Customerwise]
(  
 @GlobalAcountNumber VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (
  
   
  SELECT         
	CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth+1)) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [BillingMonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
	 ,convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  )  AS Usage              
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate    
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	,7 as [LastDueDays]
	,CONVERT(VARCHAR(11),BillGeneratedDate+7,106) as LastDueDate
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID  
     where AccountNo=@GlobalAcountNumber and BillMonth=@BillMonth and BillYear=@BillYear
  );

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 05/26/2015 19:35:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth+1)) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [BillingMonthName]           
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,  convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  ) 
		AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
   
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	  ,7 as [LastDueDays]
	  ,	CONVERT(VARCHAR(11),BillGeneratedDate+7,106)	 as LastDueDate
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID 
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );

GO


