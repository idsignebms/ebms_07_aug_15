
GO

/****** Object:  View [dbo].[UDV_CustomerPDFReport]    Script Date: 05/26/2015 19:28:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[UDV_CustomerPDFReport]    
AS    
SELECT     CD.GlobalAccountNumber, CD.AccountNo, CD.OldAccountNo, 
					(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName, CD.KnownAs, 
                      PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.ReadCodeID, PD.SortOrder, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, PD.MeterNumber, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, CD.ConnectionDate, CAD.PresentReading,     
                      PD.CustomerTypeId, CD.ServiceAddressID   
   ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
				 ,CD.Service_Landmark  
				 ,CD.Service_City,'',  
				CD.Service_ZipCode) AS [ServiceAddress] 
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
   ,BN.Details AS BookDetails        
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId 
    
    
    
    
----------------------------------------------------------------------------------------------------



GO



GO

/****** Object:  View [dbo].[UDV_SearchCustomer]    Script Date: 05/26/2015 19:33:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER VIEW [dbo].[UDV_SearchCustomer]     
AS    
   
 SELECT    DISTINCT
  CPD.GlobalAccountNumber,  
  CD.AccountNo,  
  CD.OldAccountNo,  
  dbo.fn_GetCustomerFullName_New(   
    CD.Title,    
    CD.FirstName,    
    CD.MiddleName,    
    CD.LastName) AS Name,   
  CD.HomeContactNumber,  
  BusinessContactNumber,  
  CD.OtherContactNumber,   
  BookDetails.BusinessUnitName,  
  Dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,Service_City,'',Service_ZipCode) As ServiceAddress,  
  BookDetails.ServiceUnitName,  
  BookDetails.ServiceCenterName,  
  TariffClasses.ClassName,     
  CStatus.StatusName,  
  CPD.IsBookNoChanged,  
  CPD.MeterNumber,  
  CD.ActiveStatusId  
  ,BookDetails.BU_ID  
  ,BookDetails.SU_ID  
  ,BookDetails.ServiceCenterId  
  ,BookDetails.CycleId  
  ,CPD.TariffClassID as TariffId  
  ,ReadCodeId  
  ,CAD.CreatedDate  
  ,BookDetails.BookCode  
  ,BookDetails.ID  
  ,BookDetails.BookSortOrder  
  ,CPD.SortOrder  
  ,CAD.InitialReading
  ,BookDetails.CycleName  
 FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD    
 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN  dbo.UDV_BookNumberDetails(NOLOCK) BookDetails ON BookDetails.BookNo=CPD.BookNo  
 INNER JOIN  Tbl_MCustomerStatus(NOLOCK) CStatus ON CStatus.StatusId=CD.ActiveStatusId  
 INNER JOIN  Tbl_MTariffClasses(NOLOCK) TariffClasses ON TariffClasses.ClassID=CPD.TariffClassID 
 INNER JOIN TBL_BussinessUnits AS BU ON BookDetails.BU_ID =BU.BU_ID AND BU.ActiveStatusId=1-- Neeraj ID-077 18-May-15

GO

/****** Object:  View [dbo].[UDV_CustomerPDFReport]    Script Date: 05/26/2015 19:33:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[UDV_CustomerPDFReport]    
AS    
SELECT     CD.GlobalAccountNumber, CD.AccountNo, CD.OldAccountNo, 
					(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName, CD.KnownAs, 
                      PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.ReadCodeID, PD.SortOrder, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, PD.MeterNumber, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, CD.ConnectionDate, CAD.PresentReading,     
                      PD.CustomerTypeId, CD.ServiceAddressID   
   ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
				 ,CD.Service_Landmark  
				 ,CD.Service_City,'',  
				CD.Service_ZipCode) AS [ServiceAddress] 
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
   ,BN.Details AS BookDetails        
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId 
    
    
    
    
----------------------------------------------------------------------------------------------------



GO

/****** Object:  View [dbo].[UDV_RptCustomerAndBookInfo]    Script Date: 05/26/2015 19:33:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
ALTER VIEW [dbo].[UDV_RptCustomerAndBookInfo]      
AS      
 SELECT CD.GlobalAccountNumber    
 ,CD.AccountNo    
 ,CD.OldAccountNo    
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName    
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
             ,CD.Service_Landmark    
             ,CD.Service_City,'',    
             CD.Service_ZipCode) AS [ServiceAddress]     
   ,PD.SortOrder    
   ,BN.SortOrder AS BookSortOrder    
   ,CD.ConnectionDate    
   ,PD.PoleID      
   ,PD.TariffClassID AS TariffId      
   ,TC.ClassName      
   ,PD.ReadCodeID      
   ,BN.BookNo      
   ,BN.BookCode    
   ,BN.ID AS BookId      
   ,BU.BU_ID      
   ,BU.BusinessUnitName      
   ,BU.BUCode      
   ,SU.SU_ID      
   ,SU.ServiceUnitName      
   ,SU.SUCode      
   ,SC.ServiceCenterId      
   ,SC.ServiceCenterName      
   ,SC.SCCode      
   ,C.CycleId      
   ,C.CycleName      
   ,C.CycleCode 
   ,PD.MeterNumber   
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD     
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber    
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo     
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId     
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID     
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID    
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID     
----------------------------------------------------------------------------------------------------    
GO

/****** Object:  View [dbo].[UDV_PrebillingRpt]    Script Date: 05/26/2015 19:33:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








  
  
ALTER VIEW [dbo].[UDV_PrebillingRpt]    
  
AS   
 SELECT CD.GlobalAccountNumber  
 ,CD.AccountNo  
 ,CD.OldAccountNo  
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode) AS [ServiceAddress]   
   ,PD.SortOrder  
   ,BN.SortOrder AS BookSortOrder  
   ,CD.ConnectionDate  
   ,PD.PoleID    
   ,PD.TariffClassID AS TariffId    
   ,TC.ClassName    
   ,PD.ReadCodeID    
   ,BN.BookNo    
   ,BN.BookCode  
   ,BN.ID AS BookId    
   ,BU.BU_ID    
   ,BU.BusinessUnitName    
   ,BU.BUCode    
   ,SU.SU_ID    
   ,SU.ServiceUnitName    
   ,SU.SUCode    
   ,SC.ServiceCenterId    
   ,SC.ServiceCenterName    
   ,SC.SCCode    
   ,C.CycleId    
   ,C.CycleName    
   ,C.CycleCode  
   ,Pd.MeterNumber
   ,PD.ActiveStatusId   
   ,CAD.InitialBillingKWh as DeafaultUsage  
   ,CustomerStatus.StatusId as CustomerStatusID  
   ,PD.IsEmbassyCustomer
   , BDB.DisableTypeId  as BoolDisableTypeId
   ,BDB.IsPartialBill  as IsPartialBook
   ,InitialBillingKWh
   ,CustomerTypeId
      
  FROM CUSTOMERS.Tbl_CustomerSDetail(NOLOCK) AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER Join CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID  
  INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID  
  INNER JOIn Tbl_MCustomerStatus(NOLOCK) AS CustomerStatus ON   CD.ActiveStatusId = CustomerStatus.StatusId
  LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON BDB.BookNo=Pd.BookNo  and IsActive=1 
----------------------------------------------------------------------------------------------------  
  








GO


