
GO

/****** Object:  StoredProcedure [dbo].[USP_AdjustmentForNoBill]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment
--Modified By  : Bhimaraju 
--Created Date : 02-May-2015    
--Description : Logs And Approvals insertion
--===================================         
ALTER PROCEDURE [dbo].[USP_AdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
    @BillAdjustmentId INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)    
   ,@TotalAmountEffected DECIMAL(18,2)    
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT
   ,@FunctionId INT
   ,@CreatedBy VARCHAR(50)
   ,@IsFinalApproval BIT
   ,@BU_ID VARCHAR(50)
  ,@CurrentApprovalLevel INT =0 
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
   ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')   
   ,@FunctionId = C.value('(FunctionId)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')    
   ,@TotalAmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       

	IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
				WHERE GlobalAccountNumber = @AccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillAdjustmentsBe')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AdjustmentRequestId INT
					,@Forward INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
						SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
				INSERT INTO Tbl_AdjustmentsLogs
							(		 GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId
									,CreatedBy 
									,CreatedDate
									,CurrentApprovalLevel
									,IsLocked
							)
				SELECT   @AccountNo
						,AccountNo
						,NULL--BillNumber
						,@BillAdjustmentType
						,MeterNumber
						,InitialReading
						,NULL--CurrentReadingAfterAdjustment
						,NULL--CurrentReadingBeforeAdjustment
						,NULL--AdjustedUnits
						,NULL--TaxEffected
						,@TotalAmountEffected
						,NULL-- EnergryCharges 
						,NULL--	AdditionalCharges
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
						,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM UDV_CustomerDescription
				WHERE GlobalAccountNumber=@AccountNo
				
				SET @AdjustmentRequestId = SCOPE_IDENTITY()
					
				IF(@IsFinalApproval = 1)
				BEGIN
			  						
				   INSERT INTO Tbl_BillAdjustments(
					 AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected   
					 )           
					VALUES(         
					  @AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@TotalAmountEffected  
					 )      
				   SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
   
				   INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
					  SELECT @BillAdjustmentId,@AmountEffected
					
					--OutStanding Amount is Updating Start
			
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=ISNULL(OutStandingAmount,0) - @TotalAmountEffected
					WHERE GlobalAccountNumber=@AccountNo
					--OutStanding Amount is Updating End
										  
				   INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
				   SELECT		     AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
				   FROM Tbl_AdjustmentsLogs
				   WHERE AdjustmentLogId=@AdjustmentRequestId
					
				END
			   --SELECT (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
			     SELECT 1 As IsSuccess    
			  FOR XML PATH('BillAdjustmentsBe'),TYPE    
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsList]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 11-08-2014   
-- Modified date: 28-10-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get customer  Details By AccountNo,MeterNo  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsList]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
  ,@OldAccountNo VARCHAR(50)  
  ,@MeterNo VARCHAR(50)  
  ,@BUID VARCHAR(50)=''  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
  ,@OldAccountNo=C.value('(OldAccountNo)[1]','VARCHAR(50)')    
  ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')    
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)    
     
  IF EXISTS(SELECT 0 FROM UDV_IsCustomerExists WHERE (BU_ID=@BUID OR @BUID='')  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
 BEGIN  
   SELECT    
      CD.GlobalAccountNumber AS AccountNo  
     ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name   
     ,(SELECT dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber)) AS ServiceAddress  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber)),'--') AS LastBillDate  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)),'--') As LastPaidDate  
     ,CAD.OutStandingAmount AS DueAmount  
     ,CPD.TariffClassID AS ClassID  
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=CPD.TariffClassID) AS Tariff  
     ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CPD.ClusterCategoryId) AS ClusterType
     ,(SELECT ClusterCategoryId FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CPD.ClusterCategoryId) AS ClusterTypeId
     ,1 AS IsSuccess  
   FROM CUSTOMERS.Tbl_CustomersDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
  INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber
  INNER JOIN UDV_BookNumberDetails BD ON CPD.BookNo=BD.BookNo
   WHERE (CD.GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='')    
   AND (BU_ID=@BUID OR @BUID='') 
   FOR XML PATH('TariffManagementBE')    
 END  
 ELSE IF EXISTS(SELECT 0 FROM UDV_IsCustomerExists WHERE BU_ID!=@BUID  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
  BEGIN  
   SELECT 1 AS IsCustExistsInOtherBU,1 AS IsSuccess FOR XML PATH('TariffManagementBE')   
  END  
 ELSE  
  SELECT 0 AS IsSuccess FOR XML PATH('TariffManagementBE')   
END   

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBatchTotal]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 16-04-2014    
-- Description: The purpose of this procedure is to Update Batch TOTAL    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_UpdateBatchTotal]     
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BatchId INT    
		,@ModifiedBy VARCHAR(50)    
		,@BatchTotal NUMERIC(20,4)
		,@Flag INT
		,@IsSuccess BIT = 0    
	     
	SELECT    
		 @BatchId=C.value('(BatchId)[1]','INT')    
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')    
		,@BatchTotal=C.value('(BatchTotal)[1]','NUMERIC(20,4)')    
		,@Flag=C.value('(Flag)[1]','INT')    
	FROM @XmlDoc.nodes('BillingBE') as T(C)  
	  
	 IF(@Flag = 1)  --Updating in temp table
		BEGIN
			UPDATE Tbl_BatchDetailsApproval 
			SET BatchTotal = BatchTotal - @BatchTotal 
			,BatchStatus=2  
			,ModifiedBy = @ModifiedBy
			,ModifiedDate= dbo.fn_GetCurrentDateTime()       
			WHERE BatchID = @BatchId    
			SET @IsSuccess = 1    
		END
	 ELSE IF(@Flag = 2) --Updating in base table
		BEGIN
			UPDATE Tbl_BatchDetails SET 
					BatchTotal = BatchTotal - @BatchTotal 
					,BatchStatus=2  
					,ModifiedBy = @ModifiedBy
					,ModifiedDate= dbo.fn_GetCurrentDateTime()    
					WHERE BatchID = @BatchId   
			SET @IsSuccess = 1
		END
		SELECT @IsSuccess As IsSuccess
		FOR XML PATH('BillingBE'),TYPE	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_BillAdjustment]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <22-JULY-2014>
-- Description:	<INSERT BILL ADJUSTMENT DETAILS>
-- Modified By : Padmini
-- Modified Date : 26-12-2014
-- =============================================
ALTER PROCEDURE [dbo].[USP_Insert_BillAdjustment]
(
	@XmlDoc xml=null
	,@MultiXmlDoc XML
)
AS
BEGIN	
		DECLARE
			@BillAdjustmentId INT
			,@CustomerBillId INT
			,@AccountNo VARCHAR(50)
			,@CustomerId VARCHAR(50)
			,@AmountEffected DECIMAL(18,2)
			,@TaxEffected DECIMAL(18,2)
			,@TotalAmountEffected DECIMAL(18,2)
			,@AdjustedUnits INT
			,@BillAdjustmentType INT
			,@BatchNo INT
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@ApprovalStatusId INT 
			,@Details VARCHAR(MAX)  
			,@EnergyCharges DECIMAL(18,2)
			,@AdditionalCharges DECIMAL(18,2)
			,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
		SELECT
			@CustomerBillId = C.value('(CustomerBillId)[1]','INT')
			,@AccountNo	= C.value('(AccountNo)[1]','VARCHAR(50)')
			,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')
			,@TaxEffected = C.value('(TaxEffected)[1]','DECIMAL(18,2)')
			,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)')
			,@BillAdjustmentType	= C.value('(BillAdjustmentType)[1]','INT')
			,@BatchNo = C.value('(BatchNo)[1]','INT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
			,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
			,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)')
				,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C) 

		
 		IF((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				INSERT INTO Tbl_BillAdjustments(
					CustomerBillId
					,AccountNo
					,CustomerId
					,AmountEffected
					,BillAdjustmentType
					,TaxEffected
					,TotalAmountEffected
					,BatchNo
					,CreatedBy
					,CreatedDate
					) 						
				VALUES(					
					@CustomerBillId
					,@AccountNo
					,@CustomerId
					,@AmountEffected
					,@BillAdjustmentType
					,@TaxEffected
					,@TotalAmountEffected
					,@BatchNo
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					)		
				SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
				INSERT INTO Tbl_BillAdjustmentDetails
										(
										BillAdjustmentId
										,AdditionalChargesID
										,AdditionalCharges
										,EnergryCharges
										,CreatedBy
										,CreatedDate
										)
				SELECT       
					@BillAdjustmentId 
					,C.value('(ChargeID)[1]','INT')       
					,C.value('(AmountEffected)[1]','DECIMAL(18,2)')      
					,@EnergyCharges
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
				 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(C)    	
				
				--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
				,ModifedBy=@ModifiedBy
				,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
				 
				--Updating the adjustment amount in Bill
				UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
				WHERE BillNo=@CustomerBillId
				 
			END
			SELECT 
					(CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess
				FOR XML PATH('BillAdjustmentsBe'),TYPE
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
 	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate 
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill
							UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							WHERE BillNo=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
   SET @NewConsumption=@Consumption-@AdjustedUnits;  
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * 5)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @NewConsumption END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @NewConsumption END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE BillNo=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillPayments]    Script Date: 05/27/2015 18:59:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 10 Apr 2015
-- Description:	To insert the customer bill payments
-- Modified By:	Bhimaraju V
-- Modified date: 01 May 2015
-- Description:	Approvals and Logs are implemented
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillPayments]
(
	@XmlDoc XML
)
AS
BEGIN
	
	DECLARE @AccountNo VARCHAR(50)  
		,@ReceiptNo VARCHAR(20)  
		,@PaymentMode INT  
		,@DocumentPath VARCHAR(MAX)  
		,@DocumentName VARCHAR(MAX) 
		,@Cashier VARCHAR(50)  
		,@CashOffice INT  
		,@CreatedBy VARCHAR(50)  
		,@BatchId INT  
		,@PaidAmount DECIMAL(20,4)  
		,@PaymentRecievedDate VARCHAR(20)  
		,@CustomerPaymentId INT  
		,@EffectedRows INT  
		,@PaymentType INT
		,@IsCustomerExists BIT = 0
		,@IsSuccess BIT = 0
		,@StatusText VARCHAR(200) = ''
		,@FunctionId INT
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT
		,@PaymentFromId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT =1
		
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@ReceiptNo = C.value('(ReceiptNo)[1]','VARCHAR(20)')  
		,@PaymentMode = C.value('(PaymentModeId)[1]','INT')  
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
		,@DocumentName = C.value('(Document)[1]','VARCHAR(MAX)') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@BatchId = C.value('(BatchId)[1]','INT')  
		,@PaidAmount = C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
		,@PaymentRecievedDate = C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
		,@PaymentType = C.value('(PaymentType)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')
		,@PaymentFromId = C.value('(PaymentFromId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') AS T(C) 
	
	IF(@AccountNo IS NOT NULL)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SET @StatusText = 'Customer Payments'
					DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@PaymentLogId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		IF EXISTS( SELECT PaymentLogId FROM Tbl_PaymentsLogs WHERE GlobalAccountNumber=@AccountNo AND ReceiptNumber=@ReceiptNo )
			BEGIN
						SET @IsCustomerExists = 0
						SET @IsSuccess = 0
						SET @StatusText = 'Payment with same Receipt number already Exists'
			END
		ELSE
			BEGIN
				
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
					END
					
				INSERT INTO Tbl_PaymentsLogs(
							GlobalAccountNumber 
							,AccountNo 
							,ReceiptNumber 
							,PaymentMode  
							,PaidAmount 
							,PadiDate  
							,PresentApprovalRole  
							,NextApprovalRole  
							,ApproveStatusId
							,CreatedBy 
							,CreatedDate
							,PaymentFromId
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,PaymentType
							,BatchNo
							,CurrentApprovalLevel
							,IsLocked
							)
				SELECT   @AccountNo
						,AccountNo
						,@ReceiptNo
						,@PaymentMode
						,@PaidAmount
						,@PaymentRecievedDate
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PaymentType
						,@BatchId
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
						,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM UDV_CustomerDescription
				WHERE GlobalAccountNumber=@AccountNo
				
				SET @PaymentLogId = SCOPE_IDENTITY()
						
				IF(@IsFinalApproval = 1)
					BEGIN
						
						INSERT INTO Tbl_CustomerPayments
						(
							 AccountNo
							,ReceiptNo
							,PaymentMode
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,RecievedDate
							,PaymentType
							,PaidAmount
							,BatchNo
							,ActivestatusId
							,CreatedBy
							,CreatedDate
						)
						VALUES
						(
							 @AccountNo
							,@ReceiptNo
							,@PaymentMode
							,@DocumentPath
							,@Cashier
							,@CashOffice
							,@DocumentName
							,@PaymentRecievedDate
							,@PaymentType
							,@PaidAmount
							,(CASE WHEN @BatchId = 0 THEN NULL ELSE @BatchId END)
							,1
							,@CreatedBy
							,dbo.fn_GetCurrentDateTime()
						)
						
						SET @CustomerPaymentId = SCOPE_IDENTITY()
						
						DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
						
						SET @StatusText = 'Customer Pending Bills'
						INSERT INTO @PaidBills
						(
							 CustomerBillId
							,BillNo
							,PaidAmount
							,BillStatus
						)
						SELECT 
							 CustomerBillId
							,BillNo
							,PaidAmount
							,BillStatus
						FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@AccountNo,@PaidAmount) 
						
						SET @StatusText = 'Customer Bills Paymnets'
						INSERT INTO Tbl_CustomerBillPayments
						(
							 CustomerPaymentId
							,PaidAmount
							,BillNo
							,CustomerBillId
							,CreatedBy
							,CreatedDate
						)
						SELECT 
							 @CustomerPaymentId
							,PaidAmount
							,BillNo
							,CustomerBillId
							,@CreatedBy
							,dbo.fn_GetCurrentDateTime()
						FROM @PaidBills
						
						SET @StatusText = 'Customer Bills'
						UPDATE CB 
						SET CB.ActiveStatusId = PB.BillStatus
							,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
						FROM Tbl_CustomerBills CB
						INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
						
						SET @StatusText = 'Customer Outstanding'
						UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
						SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
						WHERE GlobalAccountNumber = @AccountNo
						
						SET @StatusText= 'Audit Table'
						
						INSERT INTO Tbl_Audit_PaymentsLogs(	
							PaymentLogId
							,GlobalAccountNumber 
							,AccountNo 
							,ReceiptNumber 
							,PaymentMode  
							,PaidAmount 
							,PadiDate  
							,PresentApprovalRole  
							,NextApprovalRole  
							,ApproveStatusId  
							,Remarks 
							,CreatedBy 
							,CreatedDate  
							,ModifedBy 
							,ModifiedDate
							,PaymentFromId
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,PaymentType
							,BatchNo)  
						SELECT  
							 PaymentLogId
							,GlobalAccountNumber 
							,AccountNo 
							,ReceiptNumber 
							,PaymentMode  
							,PaidAmount 
							,PadiDate  
							,PresentApprovalRole  
							,NextApprovalRole  
							,ApproveStatusId  
							,Remarks 
							,CreatedBy 
							,CreatedDate  
							,ModifedBy 
							,ModifiedDate
							,PaymentFromId
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,PaymentType
							,BatchNo
						FROM Tbl_PaymentsLogs WHERE PaymentLogId = @PaymentLogId
						
					END
						
						SET @IsCustomerExists = 1
						SET @IsSuccess = 1
						SET @StatusText = 'Success'
						END
						
						
						
					COMMIT TRAN	
				END TRY	   
				BEGIN CATCH
					ROLLBACK TRAN
					SET @IsCustomerExists = 0
					SET @IsSuccess =0
				END CATCH			
			END
	
	SELECT  
		 @IsCustomerExists AS IsCustomerExists  
		,@IsSuccess AS IsSuccess  
		,@StatusText AS StatusText
	FOR XML PATH ('MastersBE') 
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTariffDetails_New]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka  
-- Create date: 12-Aug-2014  
-- Modified By: T.Karthik
-- Modified Date: 28-11-2014
-- Description: Update Tariff Details of a customer after approval  
-- Modified By: Karteek
-- Modified Date: 01-04-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTariffDetails_New]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
  
	DECLARE  
		 @AccountNo VARCHAR(50)   
		,@CreatedBy VARCHAR(50)  
		,@Remarks VARCHAR(50)  
		,@Flag INT  
		,@ApprovalStatusId INT  
		,@OldClassID INT  
		,@NewClassID INT  
		,@TariffChangeRequestId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = 0
		,@NextRoleId INT = 0
		,@CurrentLevel INT
		,@OldClusterTypeId INT  
		,@NewClusterTypeId INT 
		,@IsFinalApproval BIT = 0 
		,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT		
		
	SELECT   
		 @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@Remarks = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@Flag = C.value('(Flag)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@OldClassID = C.value('(OldClassID)[1]','INT')  
		,@NewClassID = C.value('(NewClassID)[1]','INT')  
		,@TariffChangeRequestId = C.value('(TariffChangeRequestId)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@OldClusterTypeId = C.value('(OldClusterTypeId)[1]','INT')  
		,@NewClusterTypeId = C.value('(ClusterTypeId)[1]','INT') 
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('TariffManagementBE') as T(C)         
   
  DECLARE @CurrentRoleId INT 
   
	IF(@Flag = 1)  -- Approval Permission Required
	BEGIN  
		IF EXISTS(SELECT 0 FROM Tbl_LCustomerTariffChangeRequest WHERE AccountNo = @AccountNo AND ApprovalStatusId = 1)  
			BEGIN  -- Pending Request already exists
				SELECT 1 AS ApprovalProcess  
			END  
		ELSE  -- New Request
			BEGIN  
				DECLARE @RoleId INT--,@IsFinalApproval BIT=0
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
				SET @CurrentLevel = 0
				
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
				INSERT INTO Tbl_LCustomerTariffChangeRequest(  
					 AccountNo  
					,PreviousTariffId  
					,ChangeRequestedTariffId 
					,OldClusterCategoryId
					,NewClusterCategoryId 
					,CreatedBy  
					,CreatedDate  
					,Remarks  
					,ApprovalStatusId
					,PresentApprovalRole
					,NextApprovalRole
					,CurrentApprovalLevel
				,IsLocked
					)  
				VALUES(  
					 @AccountNo  
					,@OldClassID  
					,@NewClassID  
					,@OldClusterTypeId
					,@NewClusterTypeId
					,@CreatedBy  
					,dbo.fn_GetCurrentDateTime()  
					,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
					,1 -- Process
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
					,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
					)

				SELECT 1 As IsSuccess--,@IsFinalApproval AS IsFinalApproval  
				FOR XML PATH('TariffManagementBE'),TYPE		
			END  
		END  
	ELSE IF(@Flag = 2)  -- Tariff Approval Page
		BEGIN  
			IF(@ApprovalStatusId = 2)  -- Request Approved
				BEGIN  
					
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive=1) -- If Approval Levels Exists
						BEGIN
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--				RoleId = (SELECT PresentApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
							--						WHERE TariffChangeRequestId = @TariffChangeRequestId))
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_LCustomerTariffChangeRequest 
											WHERE TariffChangeRequestId = @TariffChangeRequestId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

					DECLARE @FinalApproval BIT
				
					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN

							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END

							IF(@FinalApproval= 1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApprovalStatusId = CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
										,CurrentApprovalLevel=0
								,IsLocked = 1
									WHERE --AccountNo = @AccountNo   
									--AND 
									TariffChangeRequestId = @TariffChangeRequestId  

									UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
									SET       
										 TariffClassID = @NewClassID
										,ClusterCategoryId = @NewClusterTypeId  
										,ModifedBy = @ModifiedBy  
										,ModifiedDate = dbo.fn_GetCurrentDateTime()  
									WHERE GlobalAccountNumber = @AccountNo  
									
									INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
										 TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId  
										,ChangeRequestedTariffId 
										,OldClusterCategoryId
										,NewClusterCategoryId 
										,CreatedBy  
										,CreatedDate  
										,Remarks  
										,ApprovalStatusId
										,PresentApprovalRole
										,NextApprovalRole)  
									SELECT   
										 @TariffChangeRequestId
										,AccountNo  
										,PreviousTariffId
										,ChangeRequestedTariffId  
										,OldClusterCategoryId
										,NewClusterCategoryId
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,Remarks 
										,ApprovalStatusId
										,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
										,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
									FROM Tbl_LCustomerTariffChangeRequest
									WHERE --AccountNo = @AccountNo   
									--AND 
									TariffChangeRequestId = @TariffChangeRequestId 
								END
							ELSE -- Not a final approval
								BEGIN
									UPDATE Tbl_LCustomerTariffChangeRequest 
									SET   -- Updating Request with Level Roles 
										 ModifiedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,Remarks=@Remarks
										,PresentApprovalRole = CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
										,NextApprovalRole = CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
										,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
										,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
									WHERE --AccountNo = @AccountNo 
									--AND 
									TariffChangeRequestId = @TariffChangeRequestId
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							UPDATE Tbl_LCustomerTariffChangeRequest 
							SET  
								 ApprovalStatusId = @ApprovalStatusId   
								,ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,CurrentApprovalLevel= 0
						,IsLocked=1
							WHERE --AccountNo = @AccountNo  
							--AND 
							TariffChangeRequestId = @TariffChangeRequestId  

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET       
								 TariffClassID = @NewClassID  
								,ClusterCategoryId = @NewClusterTypeId 
								,ModifedBy = @ModifiedBy  
								,ModifiedDate = dbo.fn_GetCurrentDateTime()  
							WHERE GlobalAccountNumber = @AccountNo 
							
							INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
								 TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId  
								,ChangeRequestedTariffId 
								,OldClusterCategoryId
								,NewClusterCategoryId 
								,CreatedBy  
								,CreatedDate  
								,Remarks  
								,ApprovalStatusId
								,PresentApprovalRole
								,NextApprovalRole)  
							SELECT   
								 @TariffChangeRequestId
								,AccountNo  
								,PreviousTariffId
								,ChangeRequestedTariffId  
								,OldClusterCategoryId
								,NewClusterCategoryId
								,@ModifiedBy  
								,dbo.fn_GetCurrentDateTime()  
								,Remarks 
								,ApprovalStatusId
								,@PresentRoleId
								,@NextRoleId
							FROM Tbl_LCustomerTariffChangeRequest
							WHERE --AccountNo = @AccountNo   
							--AND 
							TariffChangeRequestId = @TariffChangeRequestId 
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
			ELSE  
				BEGIN  -- Request Not Approved
					IF(@ApprovalStatusId = 3) -- Request is Rejected
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
								BEGIN -- Approval levels are there and status is rejected
									--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									--						RoleId = (SELECT NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
									--									WHERE TariffChangeRequestId = @TariffChangeRequestId))
										SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_LCustomerTariffChangeRequest 
											WHERE TariffChangeRequestId = @TariffChangeRequestId)


									SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
									FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
								END
						END
					ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
						BEGIN
							IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
								SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_LCustomerTariffChangeRequest 
								WHERE TariffChangeRequestId = @TariffChangeRequestId
						END

					-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL
					UPDATE Tbl_LCustomerTariffChangeRequest SET 
						 ApprovalStatusId = @ApprovalStatusId   
						,ModifiedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId
						,Remarks = @Remarks
				,IsLocked=1
					WHERE --AccountNo = @AccountNo   
					--AND 
					TariffChangeRequestId = @TariffChangeRequestId  

					SELECT 1 As IsSuccess  
					FOR XML PATH('TariffManagementBE'),TYPE  
				END  
		END  
	ELSE  -- Approval Permission Not Required
		BEGIN  

			INSERT INTO Tbl_LCustomerTariffChangeRequest(  
				 AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId
				,OldClusterCategoryId
				,NewClusterCategoryId
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)  
			VALUES(  
				 @AccountNo  
				,@OldClassID  
				,@NewClassID
				,@OldClusterTypeId
				,@NewClusterTypeId  
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END) 
				
			SET @TariffChangeRequestId = SCOPE_IDENTITY()

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] SET       
				 TariffClassID = @NewClassID
				,ClusterCategoryId = @NewClusterTypeId  
				,ModifedBy = @CreatedBy  
				,ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE GlobalAccountNumber = @AccountNo 
			
			INSERT INTO Tbl_Audit_CustomerTariffChangeRequest(  
				 TariffChangeRequestId
				,AccountNo  
				,PreviousTariffId  
				,ChangeRequestedTariffId 
				,OldClusterCategoryId
				,NewClusterCategoryId 
				,CreatedBy  
				,CreatedDate  
				,Remarks  
				,ApprovalStatusId
				,PresentApprovalRole
				,NextApprovalRole)  
			VALUES(  
				 @TariffChangeRequestId
				,@AccountNo  
				,@OldClassID  
				,@NewClassID  
				,@OldClusterTypeId
				,@NewClusterTypeId
				,@CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
				,CASE @Remarks WHEN '' THEN NULL ELSE @Remarks END  
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END) 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('TariffManagementBE'),TYPE  
		END		
END  
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT	
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		 ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
						DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]
(  
@XmlDoc xml  
)
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT		

	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
	  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ReadToDirectId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
											
			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
											 GlobalAccountNo
											,MeterNo
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											)
			
			SET @ReadToDirectId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE TBL_ReadToDirectCustomerActivity 
						SET IsLatest=0
					WHERE GlobalAccountNo = @GlobalAccountNumber
	
					INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNumber
														,@MeterNumber
														,@CreatedBy
														,DBO.fn_GetCurrentDateTime()
														)
												
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)  
					SELECT  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
		END
END
--AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@StatusText VARCHAR(200)='Initiation'
--		,@IsSuccessful BIT=1
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	SET @StatusText='Deserialization'
	
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	SET @StatusText='Update Statement'
	
--	UPDATE TBL_ReadToDirectCustomerActivity 
--	SET IsLatest=0
--	WHERE GlobalAccountNo = @GlobalAccountNumber
	
--	SET @StatusText='Insert Log Statement'
--	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
--												(
--												GlobalAccountNo
--												,MeterNumber
--												,Remarks
--												,CreatedBy 
--												,CreatedDate
--												,ApprovalStatusId
--												)
--										VALUES
--												(
--												@GlobalAccountNumber
--												,@MeterNumber
--												,@Reason
--												,@CreatedBy
--												,DBO.fn_GetCurrentDateTime()
--												,@ApprovalStatusId
--												)
--	SET @StatusText='Update Statement'
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--		SET ReadCodeID=1, MeterNumber = NULL
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('MastersBE'),TYPE

--END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerType]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Karteek   
-- Create date: 03-Apr-2015      
-- Description: The purpose of this procedure is to Update Customer Type 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerType]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @GlobalAccountNumber VARCHAR(50)   
		,@CustomerTypeId INT 
		,@ModifiedBy VARCHAR(50)  
		,@Reason VARCHAR(MAX)  
		,@ApprovalStatusId INT  
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
    
	SELECT @GlobalAccountNumber=C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')  
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')   
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)  
  
		   
	IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
				WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeCustomerTypeBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@TypeChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
				DECLARE @Forward INT
				SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					
				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_CustomerTypeChangeLogs( 
				 GlobalAccountNumber
				,OldCustomerTypeId
				,NewCustomerTypeId
				,CreatedBy
				,CreatedDate
				,ApprovalStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked)
			SELECT GlobalAccountNumber
				,CustomerTypeId
				,@CustomerTypeId
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
				,@Reason
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()	
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END		   
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails		
			WHERE GlobalAccountNumber = @GlobalAccountNumber		
			
			SET @TypeChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails
					SET       
						 CustomerTypeId = @CustomerTypeId 
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerTypeChangeLogs(  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber  
						,OldCustomerTypeId  
						,NewCustomerTypeId  
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)  
					SELECT  
						 CustomerTypeChangeLogId
						,GlobalAccountNumber  
						,OldCustomerTypeId  
						,NewCustomerTypeId  
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_CustomerTypeChangeLogs WHERE CustomerTypeChangeLogId = @TypeChangeRequestId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeCustomerTypeBE')
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersBookNo_New]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  NEERAJ KANOJIYA          
-- Create date: 1/APRIL/2015          
-- Description: This Procedure is used to change customer address         
-- Modified BY: Faiz-ID103    
-- Modified Date: 07-Apr-2015    
-- Description: changed the street field value from 200 to 255 characters        
-- Modified BY: Faiz-ID103    
-- Modified Date: 16-Apr-2015    
-- Description: Add the condition for IsBookNoChanged  
-- Modified BY: Bhimaraju V  
-- Modified Date: 17-Apr-2015  
-- Description: Implemented Logs for Audit Tray   
-- Modified BY: Faiz-ID103
-- Modified Date: 23-Apr-2015  
-- Description: Added the functionality of AccountNo update on book change.
-- =============================================        
ALTER PROCEDURE [dbo].[USP_ChangeCustomersBookNo_New]          
(          
 @XmlDoc xml             
)          
AS          
BEGIN        
 DECLARE  @TotalAddress INT
	     ,@IsSameAsServie BIT
	     ,@GlobalAccountNo   VARCHAR(50)
		 ,@NewPostalLandMark  VARCHAR(200)=NULL
		 ,@NewPostalStreet   VARCHAR(255)
		 ,@NewPostalCity   VARCHAR(200)
		 ,@NewPostalHouseNo   VARCHAR(200)
		 ,@NewPostalZipCode   VARCHAR(50)
		 ,@NewServiceLandMark  VARCHAR(200)=NULL
		 ,@NewServiceStreet   VARCHAR(255)
		 ,@NewServiceCity   VARCHAR(200)
		 ,@NewServiceHouseNo  VARCHAR(200)
		 ,@NewServiceZipCode  VARCHAR(50)
		 ,@NewPostalAreaCode  VARCHAR(50)
		 ,@NewServiceAreaCode  VARCHAR(50)
		 ,@NewPostalAddressID  INT
		 ,@NewServiceAddressID  INT
		 ,@ModifiedBy    VARCHAR(50)
		 ,@Details     VARCHAR(MAX)
		 ,@RowsEffected    INT
		 ,@AddressID INT
		 ,@StatusText VARCHAR(50)
		 ,@IsCommunicationPostal BIT
		 ,@IsCommunicationService BIT
		 ,@ApprovalStatusId INT=2
		 ,@PostalAddressID INT
		 ,@ServiceAddressID INT
		 ,@BookNo VARCHAR(50)
		 ,@IsFinalApproval BIT
		 ,@FunctionId INT
					,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT  

	SELECT
		@GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')          
		--,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')              
		 ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')              
		 ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')              
		 ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')             
		 ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')            
		 --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')              
		 ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')              
		 ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')              
		 ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')             
		 ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')          
		 ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')          
		 ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')          
		 ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')          
		 ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')          
		 ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')          
		 ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')          
		 ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')      
		 ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')          
		 ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')      
		 ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')      
		 ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		 ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		 ,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)          


IF((SELECT COUNT(0)FROM Tbl_BookNoChangeLogs 
				WHERE GlobalAccountNo = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
	BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
	END
ELSE
	BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@BookNoChangeLogId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
					
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 

					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			
			DECLARE @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
			IF(@ExistingBookNo!=@BookNo)  
					BEGIN
					
						DECLARE @OldBU_ID VARCHAR(20)
								,@NewBU_ID VARCHAR(20)
								,@OldSU_ID VARCHAR(20)
								,@NewSU_ID VARCHAR(20)
								,@OldSC_ID VARCHAR(20)	
								,@NewSC_ID VARCHAR(20)
								,@OldCycleId VARCHAR(20)
								,@NewCycleId VARCHAR(20)
								,@OldBookNo VARCHAR(20)
								,@NewBookNo VARCHAR(20)
						  --Getting Old And New Book Details
						SELECT @OldBU_ID=BU_ID,@OldSU_ID=SU_ID,@OldSC_ID=ServiceCenterId,
									@OldCycleId=CycleId,@OldBookNo=BookNo
						   FROM UDV_BookNumberDetails
						  WHERE BookNo=@ExistingBookNo
						  
						SELECT @NewBU_ID=BU_ID,@NewSU_ID=SU_ID,@NewSC_ID=ServiceCenterId,
									@NewCycleId=CycleId,@NewBookNo=BookNo
						   FROM UDV_BookNumberDetails
						  WHERE BookNo=@BookNo
						  --Getting Old And New Book Details		  

						IF (@IsSameAsServie =0)
							BEGIN
								INSERT INTO Tbl_BookNoChangeLogs(GlobalAccountNo
																	,OldPostal_HouseNo
																	,OldPostal_StreetName
																	,OldPostal_City
																	,OldPostal_Landmark
																	,OldPostal_AreaCode
																	,OldPostal_ZipCode
																	,OldService_HouseNo
																	,OldService_StreetName
																	,OldService_City
																	,OldService_Landmark
																	,OldService_AreaCode
																	,OldService_ZipCode
																	,NewPostal_HouseNo
																	,NewPostal_StreetName
																	,NewPostal_City
																	,NewPostal_Landmark
																	,NewPostal_AreaCode
																	,NewPostal_ZipCode
																	,NewService_HouseNo
																	,NewService_StreetName
																	,NewService_City
																	,NewService_Landmark
																	,NewService_AreaCode
																	,NewService_ZipCode
																	,ApprovalStatusId
																	,Remarks
																	,CreatedBy
																	,CreatedDate
																	,PresentApprovalRole
																	,NextApprovalRole
																	,OldPostalAddressID
																	,NewPostalAddressID
																	,OldServiceAddressID
																	,NewServiceAddressID
																	,OldIsSameAsService
																	,NewIsSameAsService
																	,IsPostalCommunication
																	,IsServiceComunication
																	,OldBU_ID
																	,OldSU_ID
																	,OldSC_ID
																	,OldCycleId
																	,OldBookNo
																	,NewBU_ID
																	,NewSU_ID
																	,NewSC_ID
																	,NewCycleId
																	,NewBookNo
																	,ModifiedBy
																	,ModifiedDate
																	,CurrentApprovalLevel
																	,IsLocked
																	
																)
															SELECT   @GlobalAccountNo
																	,Postal_HouseNo
																	,Postal_StreetName
																	,Postal_City
																	,Postal_Landmark
																	,Postal_AreaCode
																	,Postal_ZipCode
																	,Service_HouseNo
																	,Service_StreetName
																	,Service_City
																	,Service_Landmark
																	,Service_AreaCode
																	,Service_ZipCode
																	,@NewPostalHouseNo
																	,@NewPostalStreet
																	,@NewPostalCity
																	,NULL
																	,@NewPostalAreaCode
																	,@NewPostalZipCode
																	,@NewServiceHouseNo
																	,@NewServiceStreet
																	,@NewServiceCity
																	,NULL
																	,@NewServiceAreaCode
																	,@NewServiceZipCode
																	,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																	,@Details
																	,@ModifiedBy
																	,dbo.fn_GetCurrentDateTime()
																	,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																	,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																	,PostalAddressID
																	,@NewPostalAddressID
																	,ServiceAddressID
																	,@NewServiceAddressID
																	,IsSameAsService
																	,@IsSameAsServie
																	,@IsCommunicationPostal
																	,@IsCommunicationService
																	,@OldBU_ID
																	,@OldSU_ID
																	,@OldSC_ID
																	,@OldCycleId
																	,@OldBookNo
																	,@NewBU_ID
																	,@NewSU_ID
																	,@NewSC_ID
																	,@NewCycleId
																	,@NewBookNo
																	,@ModifiedBy
																	,dbo.fn_GetCurrentDateTime()
																	,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																	,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
															FROM CUSTOMERS.Tbl_CustomerSDetail
															WHERE GlobalAccountNumber=@GlobalAccountNo
								SET @BookNoChangeLogId = SCOPE_IDENTITY()							
							END
						ELSE
							BEGIN
								INSERT INTO Tbl_BookNoChangeLogs( 
															 GlobalAccountNo
															,OldPostal_HouseNo
															,OldPostal_StreetName
															,OldPostal_City
															,OldPostal_Landmark
															,OldPostal_AreaCode
															,OldPostal_ZipCode
															,OldService_HouseNo
															,OldService_StreetName
															,OldService_City
															,OldService_Landmark
															,OldService_AreaCode
															,OldService_ZipCode
															,NewPostal_HouseNo
															,NewPostal_StreetName
															,NewPostal_City
															,NewPostal_Landmark
															,NewPostal_AreaCode
															,NewPostal_ZipCode
															,NewService_HouseNo
															,NewService_StreetName
															,NewService_City
															,NewService_Landmark
															,NewService_AreaCode
															,NewService_ZipCode
															,ApprovalStatusId
															,Remarks
															,CreatedBy
															,CreatedDate
															,PresentApprovalRole
															,NextApprovalRole
															,OldPostalAddressID
															,NewPostalAddressID
															,OldServiceAddressID
															,NewServiceAddressID
															,OldIsSameAsService
															,NewIsSameAsService
															,IsPostalCommunication
															,IsServiceComunication
															,OldBU_ID
															,OldSU_ID
															,OldSC_ID
															,OldCycleId
															,OldBookNo
															,NewBU_ID
															,NewSU_ID
															,NewSC_ID
															,NewCycleId
															,NewBookNo
															,ModifiedBy
															,ModifiedDate
															,CurrentApprovalLevel
															,IsLocked
															)
												SELECT   @GlobalAccountNo
														,Postal_HouseNo
														,Postal_StreetName
														,Postal_City
														,Postal_Landmark
														,Postal_AreaCode
														,Postal_ZipCode
														,Service_HouseNo
														,Service_StreetName
														,Service_City
														,Service_Landmark
														,Service_AreaCode
														,Service_ZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,@NewPostalHouseNo
														,@NewPostalStreet
														,@NewPostalCity
														,NULL
														,@NewPostalAreaCode
														,@NewPostalZipCode
														,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
														,@Details
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
														,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
														,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
														,PostalAddressID
														,@NewPostalAddressID
														,ServiceAddressID
														,@NewServiceAddressID
														,IsSameAsService
														,@IsSameAsServie
														,@IsCommunicationPostal
														,@IsCommunicationService
														,@OldBU_ID
														,@OldSU_ID
														,@OldSC_ID
														,@OldCycleId
														,@OldBookNo
														,@NewBU_ID
														,@NewSU_ID
														,@NewSC_ID
														,@NewCycleId
														,@NewBookNo
														,@ModifiedBy
														,dbo.fn_GetCurrentDateTime()
														,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
														,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
									FROM CUSTOMERS.Tbl_CustomerSDetail
									WHERE GlobalAccountNumber=@GlobalAccountNo
								SET @BookNoChangeLogId = SCOPE_IDENTITY()							
							END
						
						IF(@IsFinalApproval = 1)
						BEGIN
							BEGIN TRY  
								  BEGIN TRAN   
									--UPDATE POSTAL ADDRESS  
								 SET @StatusText='Total address count.'     
								 SET @TotalAddress=(SELECT COUNT(0)   
									  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
									  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
										)
								 --=====================================================================================          
								 --Customer has only one addres and wants to update the address. //CONDITION 1//  
								 --=====================================================================================   
								 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
									BEGIN  
								  SET @StatusText='CONDITION 1'
								 
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewPostalLandMark  
									,Service_StreetName=@NewPostalStreet  
									,Service_City=@NewPostalCity  
									,Service_HouseNo=@NewPostalHouseNo  
									,Service_ZipCode=@NewPostalZipCode  
									,Service_AreaCode=@NewPostalAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@PostalAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=1  
								  WHERE GlobalAccountNumber=@GlobalAccountNo  
								 END      
								 --=====================================================================================  
								 --Customer has one addres and wants to add new address. //CONDITION 2//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
									BEGIN  
								  SET @StatusText='CONDITION 2'
																				 
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
												HouseNo  
												,LandMark  
												,StreetName  
												,City  
												,AreaCode  
												,ZipCode  
												,ModifedBy  
												,ModifiedDate  
												,IsCommunication  
												,IsServiceAddress  
												,IsActive  
												,GlobalAccountNumber  
												)  
											   VALUES (@NewServiceHouseNo         
												,@NewServiceLandMark         
												,@NewServiceStreet          
												,@NewServiceCity        
												,@NewServiceAreaCode        
												,@NewServiceZipCode          
												,@ModifiedBy        
												,dbo.fn_GetCurrentDateTime()    
												,@IsCommunicationService           
												,1  
												,1  
												,@GlobalAccountNo  
												)   
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewServiceLandMark  
									,Service_StreetName=@NewServiceStreet  
									,Service_City=@NewServiceCity  
									,Service_HouseNo=@NewServiceHouseNo  
									,Service_ZipCode=@NewServiceZipCode  
									,Service_AreaCode=@NewServiceAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@ServiceAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo                 
								 END     
								   
								 --=====================================================================================  
								 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
									BEGIN  
								  SET @StatusText='CONDITION 3'  
								     
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									IsActive=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewPostalLandMark  
									,Service_StreetName=@NewPostalStreet  
									,Service_City=@NewPostalCity  
									,Service_HouseNo=@NewPostalHouseNo  
									,Service_ZipCode=@NewPostalZipCode  
									,Service_AreaCode=@NewPostalAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@PostalAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=1  
								  WHERE GlobalAccountNumber=@GlobalAccountNo  
								 END    
								 --=====================================================================================  
								 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
								 --=====================================================================================  
								 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
									BEGIN  
								  SET @StatusText='CONDITION 4'
								       
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
									  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
									  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
									  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
									  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
									  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationPostal       
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
								    
								  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
									LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
									  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
									  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
									  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
									  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
									  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
									  ,ModifedBy=@ModifiedBy        
									  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
									  ,IsCommunication=@IsCommunicationService  
								  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
								  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
								  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
								  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewServiceLandMark  
									,Service_StreetName=@NewServiceStreet  
									,Service_City=@NewServiceCity  
									,Service_HouseNo=@NewServiceHouseNo  
									,Service_ZipCode=@NewServiceZipCode  
									,Service_AreaCode=@NewServiceAreaCode  
									,Postal_Landmark=@NewPostalLandMark  
									,Postal_StreetName=@NewPostalStreet  
									,Postal_City=@NewPostalCity  
									,Postal_HouseNo=@NewPostalHouseNo  
									,Postal_ZipCode=@NewPostalZipCode  
									,Postal_AreaCode=@NewPostalAreaCode  
									,ServiceAddressID=@ServiceAddressID  
									,PostalAddressID=@PostalAddressID  
									,IsSameAsService=0  
								  WHERE GlobalAccountNumber=@GlobalAccountNo     
								 END    
								 COMMIT TRAN  
								END TRY  
								BEGIN CATCH  
								 ROLLBACK TRAN  
								 SET @RowsEffected=0  
								END CATCH
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
								SET BookNo=@BookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1 
							
							DECLARE @AccountNo VARCHAR(50)	
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewCycleId,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNo
							
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
									BookNoChangeLogId,
									GlobalAccountNo,
									OldPostal_HouseNo,
									OldPostal_StreetName,
									OldPostal_City,
									OldPostal_Landmark,
									OldPostal_AreaCode,
									OldPostal_ZipCode,
									NewPostal_HouseNo,
									NewPostal_StreetName,
									NewPostal_City,
									NewPostal_Landmark,
									NewPostal_AreaCode,
									NewPostal_ZipCode,
									OldService_HouseNo,
									OldService_StreetName,
									OldService_City,
									OldService_Landmark,
									OldService_AreaCode,
									OldService_ZipCode,
									NewService_HouseNo,
									NewService_StreetName,
									NewService_City,
									NewService_Landmark,
									NewService_AreaCode,
									NewService_ZipCode,
									ApprovalStatusId,
									Remarks,
									CreatedBy,
									CreatedDate,
									ModifiedBy,
									ModifiedDate,
									PresentApprovalRole,
									NextApprovalRole,
									IsPostalCommunication,
									IsServiceComunication,
									OldBU_ID,
									OldSU_ID,
									OldSC_ID,
									OldCycleId,
									OldBookNo,
									NewBU_ID,
									NewSU_ID,
									NewSC_ID,
									NewCycleId,
									NewBookNo
									)  
							SELECT  
									BookNoChangeLogId,
									GlobalAccountNo,
									OldPostal_HouseNo,
									OldPostal_StreetName,
									OldPostal_City,
									OldPostal_Landmark,
									OldPostal_AreaCode,
									OldPostal_ZipCode,
									NewPostal_HouseNo,
									NewPostal_StreetName,
									NewPostal_City,
									NewPostal_Landmark,
									NewPostal_AreaCode,
									NewPostal_ZipCode,
									OldService_HouseNo,
									OldService_StreetName,
									OldService_City,
									OldService_Landmark,
									OldService_AreaCode,
									OldService_ZipCode,
									NewService_HouseNo,
									NewService_StreetName,
									NewService_City,
									NewService_Landmark,
									NewService_AreaCode,
									NewService_ZipCode,
									ApprovalStatusId,
									Remarks,
									CreatedBy,
									CreatedDate,
									ModifiedBy,
									ModifiedDate,
									PresentApprovalRole,
									NextApprovalRole,
									IsPostalCommunication,
									IsServiceComunication,
									OldBU_ID,
									OldSU_ID,
									OldSC_ID,
									OldCycleId,
									OldBookNo,
									NewBU_ID,
									NewSU_ID,
									NewSC_ID,
									NewCycleId,
									NewBookNo
							FROM Tbl_BookNoChangeLogs WHERE BookNoChangeLogId = @BookNoChangeLogId
							
							END
					
						SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
					END
			ELSE
				BEGIN
					SELECT 0 As IsSuccess FOR XML PATH('CustomerRegistrationBE')
				END
	END
END
 

--BEGIN TRY      
--  BEGIN TRAN       
--    --UPDATE POSTAL ADDRESS      
-- SET @StatusText='Total address count.'         
-- SET @TotalAddress=(SELECT COUNT(0)       
--      FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails       
--      WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1      
--        )      
              
-- --=====================================================================================              
-- --Update book number of customer.      
-- --=====================================================================================       
--Declare @ExistingBookNo varchar(50)=(select BookNo from CUSTOMERS.Tbl_CustomerProceduralDetails where GlobalAccountNumber=@GlobalAccountNo and ActiveStatusId=1)  
--IF(@ExistingBookNo!=@BookNo)  
--BEGIN
-- --DECLARE @OldBookNo VARCHAR(50)  
-- --SET @OldBookNo = (SELECT BookNo FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@GlobalAccountNo)  
  
--  INSERT INTO Tbl_BookNoChangeLogs( AccountNo  
--          --,CustomerUniqueNo  
--          ,OldBookNo  
--          ,NewBookNo  
--          ,CreatedBy  
--          ,CreatedDate  
--          ,ApproveStatusId  
--          ,Remarks)  
--  VALUES(@GlobalAccountNo
--		--,@OldBookNo
--		,@ExistingBookNo
--		,@BookNo
--		,@ModifiedBy
--		,dbo.fn_GetCurrentDateTime()
--		,@ApprovalStatusId,@Details)   
     
--  UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails       
--  SET BookNo=@BookNo,IsBookNoChanged=1       
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND ActiveStatusId=1  
  
--	  --===========Start of Updating Account Number after book change.=================(Faiz-ID103)
--	  DECLARE @AccountNo Varchar(50)
--		,@BU_ID VARCHAR(50)  
--		,@SU_ID VARCHAR(50)  
--		,@ServiceCenterId VARCHAR(50)  
--	  SELECT @BU_ID = BU.BU_ID,@SU_ID= SU.SU_ID, @ServiceCenterId=SC.ServiceCenterId   
--	  FROM Tbl_BookNumbers BN   
--	  JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  
--	  JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
--	  JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
--	  JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
--	   WHERE BookNo=@BookNo  
	     
--	  SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@BU_ID,@SU_ID,@ServiceCenterId,@BookNo);
	  
--	  UPDATE CUSTOMERS.Tbl_CustomersDetail 
--	  SET AccountNo=@AccountNo 
--	  WHERE GlobalAccountNumber = @GlobalAccountNo
--	  --===========END of Updating Account Number after book change.=================
--END  
  
----================== Logs For Addres Change          
--  IF (@IsSameAsServie =0)  
--  BEGIN    
--   INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber  
--              ,OldPostal_HouseNo  
--              ,OldPostal_StreetName  
--              ,OldPostal_City  
--              ,OldPostal_Landmark  
--              ,OldPostal_AreaCode  
--              ,OldPostal_ZipCode  
--              ,OldService_HouseNo  
--              ,OldService_StreetName  
--              ,OldService_City  
--              ,OldService_Landmark  
--              ,OldService_AreaCode  
--              ,OldService_ZipCode  
--              ,NewPostal_HouseNo  
--                 ,NewPostal_StreetName  
--                 ,NewPostal_City  
--                 ,NewPostal_Landmark  
--                 ,NewPostal_AreaCode  
--                 ,NewPostal_ZipCode  
--                 ,NewService_HouseNo  
--                 ,NewService_StreetName  
--                 ,NewService_City  
--                 ,NewService_Landmark  
--                 ,NewService_AreaCode  
--                 ,NewService_ZipCode  
--                 ,ApprovalStatusId  
--                 ,Remarks  
--                 ,CreatedBy  
--                 ,CreatedDate  
--             )  
--            SELECT   @GlobalAccountNo  
--              ,Postal_HouseNo  
--              ,Postal_StreetName  
--              ,Postal_City  
--              ,Postal_Landmark  
--              ,Postal_AreaCode  
--              ,Postal_ZipCode  
--              ,Service_HouseNo  
--              ,Service_StreetName  
--              ,Service_City  
--              ,Service_Landmark  
--              ,Service_AreaCode  
--              ,Service_ZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@NewServiceHouseNo  
--              ,@NewServiceStreet  
--              ,@NewServiceCity  
--              ,NULL  
--              ,@NewServiceAreaCode  
--              ,@NewServiceZipCode  
--              ,@ApprovalStatusId  
--              ,@Details  
--              ,@ModifiedBy  
--              ,dbo.fn_GetCurrentDateTime()  
--            FROM CUSTOMERS.Tbl_CustomerSDetail  
--            WHERE GlobalAccountNumber=@GlobalAccountNo  
--  END  
-- ELSE  
--  BEGIN  
--   INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber  
--              ,OldPostal_HouseNo  
--              ,OldPostal_StreetName  
--              ,OldPostal_City  
--              ,OldPostal_Landmark  
--              ,OldPostal_AreaCode  
--              ,OldPostal_ZipCode  
--              ,OldService_HouseNo  
--              ,OldService_StreetName  
--              ,OldService_City  
--              ,OldService_Landmark  
--              ,OldService_AreaCode  
--              ,OldService_ZipCode  
--              ,NewPostal_HouseNo  
--                 ,NewPostal_StreetName  
--                 ,NewPostal_City  
--                 ,NewPostal_Landmark  
--                 ,NewPostal_AreaCode  
--                 ,NewPostal_ZipCode  
--                 ,NewService_HouseNo  
--                 ,NewService_StreetName  
--                 ,NewService_City  
--                 ,NewService_Landmark  
--                 ,NewService_AreaCode  
--                 ,NewService_ZipCode  
--                 ,ApprovalStatusId  
--                 ,Remarks  
--                 ,CreatedBy  
--                 ,CreatedDate  
--             )  
--            SELECT   @GlobalAccountNo  
--              ,Postal_HouseNo  
--              ,Postal_StreetName  
--              ,Postal_City  
--              ,Postal_Landmark  
--              ,Postal_AreaCode  
--              ,Postal_ZipCode  
--              ,Service_HouseNo  
--              ,Service_StreetName  
--              ,Service_City  
--              ,Service_Landmark  
--              ,Service_AreaCode  
--              ,Service_ZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@NewPostalHouseNo  
--              ,@NewPostalStreet  
--              ,@NewPostalCity  
--              ,NULL  
--              ,@NewPostalAreaCode  
--              ,@NewPostalZipCode  
--              ,@ApprovalStatusId  
--              ,@Details  
--              ,@ModifiedBy  
--              ,dbo.fn_GetCurrentDateTime()  
--            FROM CUSTOMERS.Tbl_CustomerSDetail  
--            WHERE GlobalAccountNumber=@GlobalAccountNo  
--  END  
    
-- --=====================================================================================              
-- --Customer has only one addres and wants to update the address. //CONDITION 1//      
-- --=====================================================================================          
-- IF(@TotalAddress =1 AND @IsSameAsServie = 1)      
-- BEGIN      
--  SET @StatusText='CONDITION 1'  
    
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewPostalLandMark      
--    ,Service_StreetName=@NewPostalStreet      
--    ,Service_City=@NewPostalCity      
--    ,Service_HouseNo=@NewPostalHouseNo      
--    ,Service_ZipCode=@NewPostalZipCode      
--    ,Service_AreaCode=@NewPostalAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@PostalAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=1      
--  WHERE GlobalAccountNumber=@GlobalAccountNo      
-- END          
-- --=====================================================================================      
-- --Customer has one addres and wants to add new address. //CONDITION 2//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)      
-- BEGIN      
--  SET @StatusText='CONDITION 2'  
   
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(      
--                HouseNo      
--                ,LandMark      
--                ,StreetName      
--                ,City      
--                ,AreaCode      
--                ,ZipCode      
--                ,ModifedBy      
--                ,ModifiedDate      
--                ,IsCommunication      
--                ,IsServiceAddress      
--                ,IsActive      
--                ,GlobalAccountNumber      
--                )      
--               VALUES (@NewServiceHouseNo             
--                ,@NewServiceLandMark             
--                ,@NewServiceStreet              
--                ,@NewServiceCity            
--                ,@NewServiceAreaCode            
--             ,@NewServiceZipCode              
--                ,@ModifiedBy            
--                ,dbo.fn_GetCurrentDateTime()        
--                ,@IsCommunicationService               
--                ,1      
--                ,1      
--                ,@GlobalAccountNo      
--                )       
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewServiceLandMark      
--    ,Service_StreetName=@NewServiceStreet      
--    ,Service_City=@NewServiceCity      
--    ,Service_HouseNo=@NewServiceHouseNo      
--    ,Service_ZipCode=@NewServiceZipCode      
--    ,Service_AreaCode=@NewServiceAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@ServiceAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo                     
-- END         
       
-- --=====================================================================================      
-- --Customer alrady has tow address and wants to in-active service. //CONDITION 3//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)       
-- BEGIN      
--  SET @StatusText='CONDITION 3'  
    
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    IsActive=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewPostalLandMark      
--    ,Service_StreetName=@NewPostalStreet      
--    ,Service_City=@NewPostalCity      
--    ,Service_HouseNo=@NewPostalHouseNo      
--    ,Service_ZipCode=@NewPostalZipCode      
--    ,Service_AreaCode=@NewPostalAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@PostalAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=1      
--  WHERE GlobalAccountNumber=@GlobalAccountNo      
-- END        
-- --=====================================================================================      
-- --Customer alrady has tow address and wants to update both address. //CONDITION 4//      
-- --=====================================================================================      
-- ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)      
-- BEGIN      
--  SET @StatusText='CONDITION 4'  
  
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END            
--      ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END            
--      ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END            
--      ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END            
--      ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END              
--      ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END            
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationPostal           
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1       
        
--  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET            
--    LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END            
--      ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END            
--      ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END            
--      ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END            
--      ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END              
--      ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END               
--      ,ModifedBy=@ModifiedBy            
--      ,ModifiedDate=dbo.fn_GetCurrentDateTime()         
--      ,IsCommunication=@IsCommunicationService      
--  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1      
--  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)      
--  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)      
--  UPDATE CUSTOMERS.Tbl_CustomersDetail SET      
--    Service_Landmark=@NewServiceLandMark      
-- ,Service_StreetName=@NewServiceStreet      
--    ,Service_City=@NewServiceCity      
--    ,Service_HouseNo=@NewServiceHouseNo      
--    ,Service_ZipCode=@NewServiceZipCode      
--    ,Service_AreaCode=@NewServiceAreaCode      
--    ,Postal_Landmark=@NewPostalLandMark      
--    ,Postal_StreetName=@NewPostalStreet      
--    ,Postal_City=@NewPostalCity      
--    ,Postal_HouseNo=@NewPostalHouseNo      
--    ,Postal_ZipCode=@NewPostalZipCode      
--    ,Postal_AreaCode=@NewPostalAreaCode      
--    ,ServiceAddressID=@ServiceAddressID      
--    ,PostalAddressID=@PostalAddressID      
--    ,IsSameAsService=0      
--  WHERE GlobalAccountNumber=@GlobalAccountNo         
-- END        
-- COMMIT TRAN      
--END TRY      
--BEGIN CATCH      
-- ROLLBACK TRAN      
-- SET @RowsEffected=0      
--END CATCH      
-- SELECT 1 AS RowsEffected,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')               
--END 

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerActiveStatus]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 07-10-2014      
-- Description: The purpose of this procedure is to Update Customer ActiveStausLog   
   --ModifiedBy : Padmini  
   --Modified Date :05-02-2015  
-- Modified By: Karteek  
-- Modified Date: 03-04-2015   
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerActiveStatus]  
(  
 @XmlDoc xml      
)  
AS  
BEGIN  
 DECLARE     
   @GlobalAccountNumber VARCHAR(50)   
  ,@ModifiedBy VARCHAR(50)  
  ,@ActiveStatusId INT  
  ,@Details VARCHAR(MAX)  
  ,@ApprovalStatusId INT  
  ,@IsFinalApproval BIT = 0  
  ,@FunctionId INT  
  ,@ChangeDate VARCHAR(10)
  ,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
          
 SELECT       
   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
  ,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')  
  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')    
  ,@FunctionId = C.value('(FunctionId)[1]','INT')    
  ,@ChangeDate = C.value('(ChangeDate)[1]','VARCHAR(10)')    
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 IF((SELECT COUNT(0)FROM Tbl_CustomerActiveStatusChangeLogs   
   WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN(1,4)) > 0)  
  BEGIN  
   SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
  END     
 ELSE  
  BEGIN  
   DECLARE @RoleId INT, @CurrentLevel INT  
     ,@PresentRoleId INT, @NextRoleId INT  
     ,@StatusChangeRequestId INT  
     
   SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId  
   SET @CurrentLevel = 0 -- For Stating Level     
     
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
					
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
       
   INSERT INTO Tbl_CustomerActiveStatusChangeLogs(   
     AccountNo  
    ,OldStatus  
    ,NewStatus  
    ,CreatedBy  
    ,CreatedDate  
    ,ChangeDate
    ,ApproveStatusId  
    ,Remarks  
    ,PresentApprovalRole  
    ,NextApprovalRole
    ,ModifiedBy
    ,ModifiedDate
	,CurrentApprovalLevel
	,IsLocked
    )  
   SELECT GlobalAccountNumber  
    ,ActiveStatusId  
    ,@ActiveStatusId  
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
    ,@ChangeDate 
    ,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
    ,@Details  
    ,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
	,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
    ,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
	,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
   FROM [CUSTOMERS].[Tbl_CustomerSDetail]     
   WHERE GlobalAccountNumber = @GlobalAccountNumber    
  
   SET @StatusChangeRequestId = SCOPE_IDENTITY()  
     
   IF(@IsFinalApproval = 1)  
    BEGIN  
       
     UPDATE CUSTOMERS.Tbl_CustomersDetail  
     SET         
       ActiveStatusId = @ActiveStatusId  
      ,ModifedBy = @ModifiedBy    
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
     WHERE GlobalAccountNumber = @GlobalAccountNumber   
       
     INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole
      ,ModifiedBy
      ,ModifiedDate)    
     SELECT    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,@ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole
      ,ModifiedBy
      ,ModifiedDate
     FROM Tbl_CustomerActiveStatusChangeLogs WHERE ActiveStatusChangeLogId = @StatusChangeRequestId  
       
    END  
      
   SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
  
  END   
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDirectCustomerAverageUpload]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload
-- Author:		Bhimaraju V
-- Create date: 06-May-2015
-- Description:	To Update Average Reading For a Direct Cust in a Main Table 
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @GlobalAccountNumber VARCHAR(50)
			,@AccountNumber VARCHAR(50)
			,@AverageReading DECIMAL(18,2)
			,@ModifiedBy VARCHAR(50)
			,@ApprovalStatusId INT 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)  
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
	SELECT  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)') 
			,@AccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@AverageReading = C.value('(AverageReading)[1]','DECIMAL(18,2)') 
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)') 
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
	FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	IF((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApproveStatusId IN (1,4)) = 0) 
	BEGIN
		DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
							
				DECLARE @Forward INT
				SET @Forward=1
				IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
					BEGIN
							SET @CurrentLevel=1	
							SET @CurrentApprovalLevel =1
							SET @Forward=0
					END
					ELSE
					BEGIN
						SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					
				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_DirectCustomersAverageChangeLogs
			(GlobalAccountNumber
			,AccountNo
			,PreviousAverage
			,NewAverage
			,PresentApprovalRole
			,NextApprovalRole
			,ApproveStatusId
			,Remarks
			,CreatedBy
			,CreatedDate
			,CurrentApprovalLevel
			,IsLocked
			)
			
			SELECT @GlobalAccountNumber
						,@AccountNumber
						,ISNULL((SELECT AverageReading FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNumber),0)
						,@AverageReading
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
						,@Details
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
						,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
			IF(@IsFinalApproval = 1)
				BEGIN
				IF EXISTS (SELECT 0 FROM Tbl_DirectCustomersAvgReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
				BEGIN
					--Print 'Avg reading for customer exiting- update the avg reading'
					Update Tbl_DirectCustomersAvgReadings
					SET AverageReading=@AverageReading
						,ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					--Updating Direct Customer Avg Reading in main table
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET AvgReading=CONVERT(NUMERIC,@AverageReading)
						,ModifedBy=@ModifiedBy
						,ModifiedDate=dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber=@GlobalAccountNumber
				END
				ELSE
				BEGIN
					--Print 'Avg reading for customer Not exiting- Insert the avg reading'
					Insert Into Tbl_DirectCustomersAvgReadings(
								GlobalAccountNumber
								,AverageReading
								,ActiveStatusId
								,CreatedBy
								,CreatedDate
								)
					Values		(@GlobalAccountNumber
								,@AverageReading
								,1
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
					--Updating Direct Customer Avg Reading in main table
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET AvgReading=@AverageReading
						,ModifedBy=@ModifiedBy
						,ModifiedDate=dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					
				END
				END
				SELECT 1 AS IsSuccess FOR XML PATH('DirectCustomerAverageUploadBE')  
	END
	ELSE
	BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('DirectCustomerAverageUploadBE')  
	END
	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,@OldMeterReading
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,@PreviousReading
						,@OldMeterReading
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,@NewMeterInitialReading
						,@NewMeterReading
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
	,@BU_ID VARCHAR(50)  
	,@CurrentApprovalLevel INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT
							,@CurrentLevel INT
							,@PresentRoleId INT
							,@NextRoleId INT
							,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			--IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
			--	BEGIN
			--		SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
			--	END
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
							
				
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
			
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,CurrentApprovalLevel
																,IsLocked
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,CurrentApprovalLevel
																,IsLocked
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						 
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId
							,GlobalAccountNumber
							,OldPostal_HouseNo
							,OldPostal_StreetName
							,OldPostal_City
							,OldPostal_Landmark
							,OldPostal_AreaCode
							,OldPostal_ZipCode
							,NewPostal_HouseNo
							,NewPostal_StreetName
							,NewPostal_City
							,NewPostal_Landmark
							,NewPostal_AreaCode
							,NewPostal_ZipCode
							,OldService_HouseNo
							,OldService_StreetName
							,OldService_City
							,OldService_Landmark
							,OldService_AreaCode
							,OldService_ZipCode
							,NewService_HouseNo
							,NewService_StreetName
							,NewService_City
							,NewService_Landmark
							,NewService_AreaCode
							,NewService_ZipCode
							,ApprovalStatusId
							,Remarks
							,CreatedBy
							,CreatedDate
							,ModifiedBy
							,ModifiedDate
							,PresentApprovalRole
							,NextApprovalRole
							,IsPostalCommunication
							,IsServiceComunication
							) 
					SELECT  
							AddressChangeLogId
							,GlobalAccountNumber
							,OldPostal_HouseNo
							,OldPostal_StreetName
							,OldPostal_City
							,OldPostal_Landmark
							,OldPostal_AreaCode
							,OldPostal_ZipCode
							,NewPostal_HouseNo
							,NewPostal_StreetName
							,NewPostal_City
							,NewPostal_Landmark
							,NewPostal_AreaCode
							,NewPostal_ZipCode
							,OldService_HouseNo
							,OldService_StreetName
							,OldService_City
							,OldService_Landmark
							,OldService_AreaCode
							,OldService_ZipCode
							,NewService_HouseNo
							,NewService_StreetName
							,NewService_City
							,NewService_Landmark
							,NewService_AreaCode
							,NewService_ZipCode
							,ApprovalStatusId
							,Remarks
							,CreatedBy
							,CreatedDate
							,ModifiedBy
							,ModifiedDate
							,PresentApprovalRole
							,NextApprovalRole
							,IsPostalCommunication
							,IsServiceComunication
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerName]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 29-09-2014      
-- Modified By: T.Karthik  
-- Modified Date: 27-12-2014  
-- Modified By: M.Padmini  
-- Modified Date: 21-01-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Bhimaraju.V  
-- Modified Date: 04-02-2015  
-- Description: The purpose of this procedure is to Update Customer Name  
-- Modified By: Karteek
-- Modified Date: 02-04-2015  
-- Description: The purpose i have changed this to inserting changes in Tbl_CustomerNameChangeLogs only
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerName]  
(      
	@XmlDoc xml      
)  
AS      
BEGIN  
       
	DECLARE @AccountNo VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50)  
		,@NewTitle VARCHAR(10)  
		,@NewFirstName VARCHAR(50)  
		,@NewMiddleName VARCHAR(50)  
		,@NewLastName VARCHAR(50)  
		,@NewKnownAs VARCHAR(150)  
		,@ModifiedBy VARCHAR(50)  
		,@Details VARCHAR(MAX)  
		,@ApprovalStatusId INT 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT =0
    
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@GlobalAccountNumber = C.value('GlobalAccountNumber[1]','VARCHAR(50)')  
		,@NewTitle = C.value('(Title)[1]','VARCHAR(10)')  
		,@NewFirstName = C.value('(FirstName)[1]','VARCHAR(50)')  
		,@NewMiddleName = C.value('(MiddleName)[1]','VARCHAR(50)')  
		,@NewLastName = C.value('(LastName)[1]','VARCHAR(50)')  
		,@NewKnownAs = C.value('(KnownAs)[1]','VARCHAR(150)')  
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
	IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @GlobalAccountNumber  
					AND ApproveStatusId IN (1,4)) = 0)  
		BEGIN  		
			DECLARE @RoleId INT
			,@CurrentLevel INT
			,@PresentRoleId INT
			,@NextRoleId INT
			,@NameChangeRequestId INT
			,@Forward INT
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
						SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	

			INSERT INTO Tbl_CustomerNameChangeLogs(  
				 GlobalAccountNumber  
				,AccountNo  
				,OldTitle  
				,OldFirstName  
				,OldMiddleName  
				,OldLastName  
				,OldKnownas  
				,NewTitle  
				,NewFirstName  
				,NewMiddleName  
				,NewLastName  
				,NewKnownas  
				,CreatedBy  
				,CreatedDate
				,ApproveStatusId  
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked
				) 
			SELECT @GlobalAccountNumber  
				,AccountNo  
				,CASE Title WHEN '' THEN NULL ELSE Title END   -- title was missing --Faiz-ID103 
				,CASE FirstName WHEN '' THEN NULL ELSE FirstName END   
				,CASE MiddleName WHEN '' THEN NULL ELSE MiddleName END  
				,CASE LastName WHEN '' THEN NULL ELSE LastName END    
				,CASE Knownas WHEN '' THEN NULL ELSE Knownas END    
				,CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END    
				,CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END    
				,CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END   
				,CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END     
				,CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END    
				,@ModifiedBy  
				,dbo.fn_GetCurrentDateTime()
				, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
				,@Details  
				, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
			FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @GlobalAccountNumber  
			
			SET @NameChangeRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomersDetail
					SET       
						 Title = (CASE @NewTitle WHEN '' THEN NULL ELSE @NewTitle END) 
						,FirstName = (CASE @NewFirstName WHEN '' THEN NULL ELSE @NewFirstName END)  
						,MiddleName = (CASE @NewMiddleName WHEN '' THEN NULL ELSE @NewMiddleName END)  
						,LastName = (CASE @NewLastName WHEN '' THEN NULL ELSE @NewLastName END)  
						,KnownAs = (CASE @NewKnownAs WHEN '' THEN NULL ELSE @NewKnownAs END)  
						,ModifedBy = @ModifiedBy  
						,ModifiedDate = dbo.fn_GetCurrentDateTime()  
					WHERE GlobalAccountNumber = @GlobalAccountNumber 
					
					INSERT INTO Tbl_Audit_CustomerNameChangeLogs(  
						 NameChangeLogId
						,GlobalAccountNumber  
						,AccountNo  
						,OldTitle  
						,OldFirstName  
						,OldMiddleName  
						,OldLastName  
						,OldKnownas  
						,NewTitle  
						,NewFirstName  
						,NewMiddleName  
						,NewLastName  
						,NewKnownas  
						,CreatedBy  
						,CreatedDate
						,ModifedBy
						,ModifiedDate
						,ApproveStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole)  
					SELECT  
						 NameChangeLogId
						,GlobalAccountNumber  
						,AccountNo  
						,OldTitle  
						,OldFirstName  
						,OldMiddleName  
						,OldLastName  
						,OldKnownas  
						,NewTitle  
						,NewFirstName  
						,NewMiddleName  
						,NewLastName  
						,NewKnownas  
						,CreatedBy  
						,CreatedDate
						,ModifedBy
						,ModifiedDate
						,ApproveStatusId  
						,Remarks
						,PresentApprovalRole
						,NextApprovalRole
					FROM Tbl_CustomerNameChangeLogs WHERE NameChangeLogId = @NameChangeRequestId
					
				END
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
    
END  
  
---------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_MeterReadingsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchAdjustmentStatus]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-DEC-2014>
-- Description:	<Retriving Batch Adjustment Status (Open/Close)>
-- Author:		<Faiz-ID103>
-- Create date: <13-May-2015>
-- Description:	Added the BU_ID for filtering the records
-- Modified By : Bhimaraju Vanka
-- Desc : Added Batch Status Id Condition
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchAdjustmentStatus]
(
	@XmlDoc XML
)
AS
	BEGIN
		DECLARE @BU_ID VARCHAR(50)
				,@PageNo INT
				,@PageSize INT
		
		SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')     
				,@PageNo = C.value('(PageNo)[1]','INT')
				,@PageSize = C.value('(PageSize)[1]','INT')	      
		FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH PagedResults AS
		(
			SELECT   ROW_NUMBER() OVER(ORDER BY BA.BatchDate) AS RowNumber 
					,BA.BatchID
					,BA.BatchNo
					,ISNULL(CONVERT(VARCHAR(50),BA.BatchDate,106),'--') AS BatchDate
					,BA.BatchTotal
					,ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0) AS PaidAmount
					,BU_ID AS BUID
			FROM	Tbl_BATCH_ADJUSTMENT AS BA
			WHERE	ISNULL(BatchStatusID,1) = 1
					--BatchTotal>(ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0))
					--AND BU_ID=@BU_ID
					AND BU_ID=(SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		)
			SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults PR
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			ORDER BY PR.BatchDate
	END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchPaymentStatus]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-DEC-2014>
-- Description:	<Retriving Batch Payment Status (Open/Close)>
-- Modified By : Bhimaraju Vanka
-- Desc : Added Batch Status Id Condition
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchPaymentStatus]
(
	@XmlDoc XML
)
AS
	BEGIN
		DECLARE @BU_ID VARCHAR(MAX)
				,@PageNo INT
				,@PageSize INT
		
		SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')    
				,@PageNo = C.value('(PageNo)[1]','INT')
				,@PageSize = C.value('(PageSize)[1]','INT')	    
		FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
		
		
		IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		;WITH PagedResults AS
		(
		SELECT   ROW_NUMBER() OVER(ORDER BY BD.BatchDate) AS RowNumber 
			,BD.BatchNo
			,BD.BatchID 
			,BD.BatchTotal
			,ISNULL(CONVERT(VARCHAR(50),BD.BatchDate,106),'--') AS BatchDate
			--,bd.BatchDate
			,ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchID),0) AS PaidAmount
			--,(CASE 
			--	WHEN BD.BatchTotal<>(ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0) ) 
			--	THEN 'Open' 
			--	else 'Close' 
			--end) AS BatchStatus
			,BU_ID AS BUID
		FROM Tbl_BatchDetails AS BD
		WHERE ISNULL(BD.BatchStatus,1) = 1 -- Faiz
		--BD.BatchTotal>(ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0))
		--AND BU_ID=@BU_ID
		AND BU_ID=(SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))

		)
			SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults pr
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		order by pr.BatchDate
		
	END


GO

/****** Object:  StoredProcedure [dbo].[USP_IsAdjustmentBacthesClose_Month]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 18 OCT 2014
-- Description  : To Check the Adjustment related old batches are close not to on Month
-- Modified By: T.karthik
-- Modified date: 30-12-2014
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsAdjustmentBacthesClose_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@BU_ID VARCHAR(MAX)
	DECLARE @TempTable TABLE(BatchID INT,BatchNo INT,BatchTotal DECIMAL(18,2),UsedBatchTotal DECIMAL(18,2),BatchStatusId INT)
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	INSERT INTO @TempTable(BatchID,BatchNo,BatchTotal,UsedBatchTotal,BatchStatusId)			
	SELECT
		 BatchID
		 ,BatchNo
		,BatchTotal
		,(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments CP  WITH (NOLOCK) WHERE BatchNo IS NOT NULL AND CP.BatchNo = BD.BatchID)
		,BatchStatusID
	FROM Tbl_BATCH_ADJUSTMENT BD WITH (NOLOCK)
	WHERE BU_ID=(SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	AND BatchStatusID = 1

	IF EXISTS(SELECT 0 FROM @TempTable) -- WHERE ISNULL(BatchStatusId,1) = 1 BatchTotal >= ISNULL(UsedBatchTotal,0)
		BEGIN
			SELECT 1 AS IsExists,SUBSTRING(
					(SELECT ','+CONVERT(VARCHAR(MAX),BatchNo) FROM @TempTable-- WHERE ISNULL(BatchStatusId,1) = 1 --BatchTotal >= ISNULL(UsedBatchTotal,0)
					 FOR XML PATH('')
						),2,10000000000) AS OpenBatches
			FOR XML PATH('BillGenerationBe'),TYPE			
		END
	ELSE
		BEGIN
			SELECT 0 AS IsExists
			FOR XML PATH('BillGenerationBe'),TYPE
		END	
END


GO

/****** Object:  StoredProcedure [dbo].[USP_IsPaymentsBacthesClose_Month]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date  : 18 OCT 2014
-- Description  : To Check the Payments related old batches are close not to on Month
-- Modified By: T.karthik
-- Modified date: 30-12-2014
-- Modified By : Jeevan Amunuri
-- Modified Date : 30-05-2015 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsPaymentsBacthesClose_Month]
(
	@XmlDoc XML
)
AS
BEGIN
	DECLARE @Month INT 
			,@Year INT 
			,@BU_ID VARCHAR(MAX)
	DECLARE @TempTable TABLE(BatchNo INT,BatchTotal DECIMAL(18,2),UsedBatchTotal DECIMAL(18,2),BatchStatusId INT)
		
	SELECT   		
		@Month = C.value('(BillingMonth)[1]','INT')
		,@Year = C.value('(BillingYear)[1]','INT')
		,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	INSERT INTO @TempTable(BatchNo,BatchTotal,UsedBatchTotal,BatchStatusId)			
	SELECT
		 BatchNo
		,BatchTotal
		,(SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments CP WITH (NOLOCK) WHERE BatchNo IS NOT NULL AND CP.BatchNo = BD.BatchNo)
		,BatchStatus
	FROM Tbl_BatchDetails BD WITH (NOLOCK)
	WHERE BU_ID=(SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	AND ISNULL(BatchStatus,1) = 1
	
	IF EXISTS(SELECT 0 FROM @TempTable) -- WHERE ISNULL(BatchStatusId,1) = 1 BatchTotal >= ISNULL(UsedBatchTotal,0)
		BEGIN
			SELECT 
				1 AS IsExists
				,SUBSTRING(
					(SELECT ','+CONVERT(VARCHAR(MAX),BatchNo) FROM @TempTable --WHERE ISNULL(BatchStatusId,1) = 1 --BatchTotal >= ISNULL(UsedBatchTotal,0)
					FOR XML PATH('')
						),2,10000000000) AS OpenBatches
			FOR XML PATH('BillGenerationBe'),TYPE			
		END
	ELSE
		BEGIN
			SELECT 0 AS IsExists
			FOR XML PATH('BillGenerationBe'),TYPE
		END	
END


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 

 
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd   .Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						IF	 isnull(@GetPaidMeterBalanceAfterBill,0) <>0
						BEGIN
						
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
 
						update Tbl_PaidMeterDetails
						SET OutStandingAmount =@PaidMeterDeductedAmount -isnull(OutStandingAmount,0)  
						WHERE AccountNo = @GlobalAccountNumber	 -- and ActiveStatusId=1
					 	
						END
						 
				
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 05/27/2015 18:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=ServiceCenterId  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE AccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
      
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 where AccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount
	END
END  
				 

GO


