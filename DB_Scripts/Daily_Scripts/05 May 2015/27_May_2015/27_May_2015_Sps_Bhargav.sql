
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMeterReadingLogsToApprove]    Script Date: 05/27/2015 17:08:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterReadingLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
			,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
  
	SELECT  
		 ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
		,T.CustomerReadingLogId AS MeterReadingLogId
		,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
		,(T.GlobalAccountNumber) AS AccountNumber
		,CD.OldAccountNo AS OldAccountNumber  
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
		,ISNULL(T.MeterNumber,'--') AS MeterNo
		,ISNULL(T.PreviousReading,'--') AS PreviousReading
		,ISNULL(T.PresentReading,'--') AS PresentReading
		,ISNULL(T.AverageReading,'--') AS AverageUsage
		,ISNULL(CONVERT(VARCHAR(20),T.Usage),'--') AS Usage
		,CONVERT(VARCHAR(20),T.ReadDate,107) AS ReadDate
		,ISNULL(T.Remarks,'--') As Details
		,MRF.MeterReadingFrom
		,MRF.MeterReadingFromId
		,(CASE WHEN T.NextApprovalRole IS NULL 
					THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
					ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
					END) AS [Status]
		--,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
		--		THEN 1
		--		ELSE 0
		--		END) AS IsPerformAction 
		,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId
				 AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,T.PresentApprovalRole
		,T.NextApprovalRole
		,T.IsRollOver
	FROM Tbl_CustomerReadingApprovalLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MRF ON MRF.MeterReadingFromId= T.MeterReadingFrom
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	--WHERE T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
END
--------------------------------------------------------------------------------------------------


GO
/****** Object:  StoredProcedure [dbo].[USP_CustomerReadingsChangeApproval]    Script Date: 05/27/2015 17:09:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Modified By: Karteek
-- Modified Date: 05-05-2015
-- Description: update the status of the meter readings approval 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerReadingsChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT  
		,@MeterReadingLogId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@IsFinalApproval BIT = 0
		,@IsRollOver BIT 
		,@Details VARCHAR(200) 
		,@MeterReadingFromId INT
		,@GlobalAccountNo VARCHAR(50)
		,@BU_ID VARCHAR(50) 
	SELECT  
		 @MeterReadingLogId = C.value('(MeterReadingLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@IsRollOver = C.value('(IsRollOver)[1]','BIT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
		,@MeterReadingFromId = C.value('(MeterReadingFromId)[1]','INT')
		,@GlobalAccountNo = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--						RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerReadingApprovalLogs 
					--								WHERE CustomerReadingLogId = @MeterReadingLogId))
					
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerReadingApprovalLogs 
											WHERE CustomerReadingLogId = @MeterReadingLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					--DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
							SET @IsFinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @IsFinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					SET @IsFinalApproval = 1
				END
				
			IF(@IsFinalApproval = 1)
				BEGIN
					--DECLARE @ReadId INT
					--Declare @IsExistingRollOver bit
					--DECLARE @IsExits BIT
					
					--SELECT TOP(1) @ReadId = CustomerReadingId,@IsExistingRollOver=CR.IsRollOver
					--	,@IsExits = (Case when CONVERT(DATE,CR.ReadDate) >= CONVERT(DATE,CRA.ReadDate) then 1 else 0 end)
					--FROM Tbl_CustomerReadings CR
					--INNER JOIN Tbl_CustomerReadingApprovalLogs CRA ON CRA.GlobalAccountNumber = CR.GlobalAccountNumber
					--	AND CR.GlobalAccountNumber = @GlobalAccountNo AND CRA.CustomerReadingLogId = @MeterReadingLogId
					--	AND CR.IsBilled = 0
					--ORDER BY CustomerReadingId DESC
					
					--IF(@IsExits = 1)	
					--	BEGIN 
					--		IF @IsExistingRollOver = 1
					--			BEGIN
					--				DELETE FROM	Tbl_CustomerReadings where   CustomerReadingId=   @ReadId or CustomerReadingId=@ReadId -1
					--			END
					--		ELSE
					--			BEGIN
					--				DELETE FROM	Tbl_CustomerReadings where   CustomerReadingId =   @ReadId    
					--			END	
					--	END
					
					IF(@IsRollOver = 1)
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId != @MeterReadingLogId AND CustomerReadingLogId != @MeterReadingLogId+1)
							AND ApproveStatusId = 1
																											
							INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole)	
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)
							
							INSERT INTO Tbl_CustomerReadings(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver)
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)
							
							DECLARE @MaxPreviousReading VARCHAR(20), @MaxPresentReading VARCHAR(20), @MaxAverageReading VARCHAR(20)
							
							SELECT
								 @MaxPreviousReading = MAX(CR.PreviousReading)
								,@MaxPresentReading = MIN(CR.PresentReading) 
								,@MaxAverageReading = MAX(CR.AverageReading) 
							FROM Tbl_CustomerReadingApprovalLogs CR 
							WHERE CR.GlobalAccountNumber = @GlobalAccountNo 
							AND (CR.CustomerReadingLogId = @MeterReadingLogId OR CR.CustomerReadingLogId = @MeterReadingLogId+1)
															
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							SET InitialReading = @MaxPreviousReading
								,PresentReading = @MaxPresentReading
								,AvgReading = CONVERT(NUMERIC,@MaxAverageReading)
							WHERE GlobalAccountNumber = @GlobalAccountNo 
						END	
					ELSE
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId != @MeterReadingLogId
							AND ApproveStatusId = 1
							
							INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole)	
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver
								,ApproveStatusId
								,PresentApprovalRole
								,NextApprovalRole	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId
							
							INSERT INTO Tbl_CustomerReadings(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver)
							SELECT 
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								,IsRollOver	
							FROM Tbl_CustomerReadingApprovalLogs
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId
									
							UPDATE CD
							SET CD.InitialReading = CR.PreviousReading
								,CD.PresentReading = CR.PresentReading 
								,CD.AvgReading = CONVERT(NUMERIC,CR.AverageReading)
							FROM CUSTOMERS.Tbl_CustomerActiveDetails CD
							INNER JOIN Tbl_CustomerReadingApprovalLogs CR ON CR.GlobalAccountNumber = CD.GlobalAccountNumber
									AND CR.GlobalAccountNumber = @GlobalAccountNo 
									AND CR.CustomerReadingLogId = @MeterReadingLogId
						END
				END
			ELSE
				BEGIN
					IF(@IsRollOver = 1)
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)  
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)	
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3 
								,CurrentApprovalLevel = 0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND (CustomerReadingLogId != @MeterReadingLogId AND CustomerReadingLogId != @MeterReadingLogId+1)
							AND ApproveStatusId = 1														
						END	
					ELSE
						BEGIN
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END)  
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId = @MeterReadingLogId	
							
							UPDATE Tbl_CustomerReadingApprovalLogs
							SET   -- Updating remaining requests to Rejected 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = 3 
								,CurrentApprovalLevel = 0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND CustomerReadingLogId != @MeterReadingLogId
							AND ApproveStatusId = 1												
						END	
				END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerReadingApprovalLogs
							--									WHERE CustomerReadingLogId = @MeterReadingLogId))
							
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerReadingApprovalLogs 
																WHERE CustomerReadingLogId = @MeterReadingLogId )

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerReadingApprovalLogs 
						WHERE CustomerReadingLogId = @MeterReadingLogId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			IF(@IsRollOver = 1)
				BEGIN
					UPDATE Tbl_CustomerReadingApprovalLogs 
					SET   
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,Remarks = @Details
						--,CurrentApprovalLevel=0
						,IsLocked=1
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND (CustomerReadingLogId = @MeterReadingLogId OR CustomerReadingLogId = @MeterReadingLogId+1)	
				END
			ELSE 
				BEGIN
					UPDATE Tbl_CustomerReadingApprovalLogs 
					SET   
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,Remarks = @Details
						--,CurrentApprovalLevel=0
						,IsLocked=1
					WHERE GlobalAccountNumber = @GlobalAccountNo  
					AND CustomerReadingLogId = @MeterReadingLogId 
				END
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END


GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBookwiseMeterReadings_WithDT]    Script Date: 05/27/2015 17:09:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karteek
-- Create date: 04-05-2015
-- This is to insert the meter readings bookwise bulk into logs 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertBookwiseMeterReadings_WithDT]
(
	 @MeterReadingFrom INT
	,@CreateBy varchar(50)
	,@BU_ID VARCHAR(50)
	,@IsFinalApproval BIT
	,@FunctionId INT
	,@ActiveStatusId INT
	,@TempReadings TblMeterReadingsUpload READONLY
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess BIT = 0

	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT 
		,@CurrentApprovalLevel INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempReadings
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
							BEGIN
									SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
							END
						ELSE
							BEGIN
								DECLARE @UserDetailedId INT
								SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
								SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
							END
							
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
												
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = ISNULL(Usage,0)
				,@AccountNo = AccNum
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
				,@Multiple = Multiplier
			 FROM @TempReadings WHERE SNo = @SNo
			 
			IF(@IsExists = 1)
				BEGIN
					DECLARE @ReadId INT
					Declare @IsExistingRollOver bit
					
					SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
					FROM Tbl_CustomerReadingApprovalLogs
					WHERE GlobalAccountNumber = @AccountNo
					ORDER BY CustomerReadingLogId DESC
						 
					IF @IsExistingRollOver = 1
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
					END
					ELSE
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
					END	
				END
			 
			SELECT @MeterNumber = MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @AccountNo 
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)		
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@PreviousReading)
				,@PresentReading
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				,(CASE WHEN @IsFinalApproval = 1 THEN 2 ELSE @ActiveStatusId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @PresentRoleId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @NextRoleId END)
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				
			IF(@IsFinalApproval = 1)
				BEGIN
				
					INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver
						,ApproveStatusId
						,PresentApprovalRole
						,NextApprovalRole)
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						,@ActiveStatusId
						,(CASE WHEN @PresentRoleId = 0 THEN @RoleId ELSE @PresentRoleId END)
						,(CASE WHEN @NextRoleId = 0 THEN @RoleId ELSE @NextRoleId END))
				
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = @PreviousReading
						,PresentReading = @PresentReading 
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
				END
				
			SET @IsSuccess = 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			SET @Multiple = NULL
			
		END
	
	SELECT @IsSuccess AS IsSuccess 
	
END


GO
/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingLogs]    Script Date: 05/27/2015 17:09:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 04 may 2015
-- Description: TO insert the customer meter readings into the logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingLogs]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
		
		,@IsFinalApproval BIT
		,@FunctionId INT
		,@ActiveStatusId INT
		,@IsExists BIT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
		,@IsFinalApproval=C.value('(IsFinalApproval)[1]','bit')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')
		,@IsExists = C.value('(IsExists)[1]','BIT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	DECLARE @RoleId INT, @CurrentLevel INT
			,@PresentRoleId INT, @NextRoleId INT
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level			
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
		BEGIN
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
					BEGIN
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
					END
				ELSE
					BEGIN
						DECLARE @UserDetailedId INT
						SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
						SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					END
					
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				ELSE
				BEGIN
					SET @CurrentApprovalLevel = @CurrentLevel+1
				END 
										
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
	
		END
	ELSE
		BEGIN
			SET @IsFinalApproval=1
			SET @PresentRoleId=0
			SET @NextRoleId=0
			SET @CurrentLevel=0	
			SET @CurrentApprovalLevel =0
		END
			
	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum
	
	IF(@IsExists = 1)
		BEGIN
			DECLARE @ReadId INT
			Declare @IsExistingRollOver bit
			
			SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
			FROM Tbl_CustomerReadingApprovalLogs
			WHERE GlobalAccountNumber = @AccNum
			ORDER BY CustomerReadingLogId DESC
				 
			IF @IsExistingRollOver = 1
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
			END
			ELSE
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
			END	
		END
	
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	 
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);		  
		END
		
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
	
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)	
			VALUES(
				 @AccNum
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),@Previous)
				,@FirstPresentValue
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,@IsRollover
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				

			IF(@IsRollover=1)
				BEGIN
		 
					IF @SecountReadingUsage != 0
						BEGIN
						--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
						 IF	 @SecountReadingUsage > 0
										 INSERT INTO Tbl_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole
											,CurrentApprovalLevel
											,IsLocked)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,@SecoundReadingPrevReading
											,CONVERT(VARCHAR(50),@Current)
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
											
						END
				END
				
		IF(@IsFinalApproval = 1)
			BEGIN		
			
				INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver
					,ApproveStatusId
					,PresentApprovalRole
					,NextApprovalRole)	
				VALUES(
					@AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),@Previous)
					,@FirstPresentValue
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover
					,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)	
				
				INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver)
				VALUES(
					 @AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),@Previous)
					,@FirstPresentValue
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover)
					

				IF(@IsRollover=1)
					BEGIN
			 
						IF @SecountReadingUsage != 0
							BEGIN
							--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
							 IF	 @SecountReadingUsage > 0
								 BEGIN
									INSERT INTO Tbl_CustomerReadings(
												 GlobalAccountNumber
												,[ReadDate]
												,[ReadBy]
												,[PreviousReading]
												,[PresentReading]
												,[AverageReading]
												,[Usage]
												,[TotalReadingEnergies]
												,[TotalReadings]
												,[Multiplier]
												,[ReadType]
												,[CreatedBy]
												,[CreatedDate]
												,[IsTamper]
												,MeterNumber
												,[MeterReadingFrom]
												,IsRollOver)
											VALUES(
												 @AccNum
												,@ReadDate
												,@ReadBy
												,@SecoundReadingPrevReading
												,CONVERT(VARCHAR(50),@Current)
												,@AverageReading
												,@SecountReadingUsage
												,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
												,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
												,@Multiple
												,2
												,@CreateBy
												,GETDATE()
												,@IsTamper
												,@MeterNumber
												,@MeterReadingFrom
												,@IsRollover)
												
										INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,@SecoundReadingPrevReading
											,CONVERT(VARCHAR(50),@Current)
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)
								END												
							END
					END
					
			 
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
				SET InitialReading = @Previous
					,PresentReading = @Current 
					,AvgReading = CONVERT(NUMERIC,@AverageReading) 
				WHERE GlobalAccountNumber = @AccNum
			END
		
	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END


GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountwiseMeterReadings]    Script Date: 05/27/2015 17:10:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Karteek
-- Create date: 21 May 2015
-- Description: <Get BillReading details from customerreading table>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAccountwiseMeterReadings] 
(
	@XmlDoc xml
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists bit = 0
		Declare	  @PrvExists bit = 0
		Declare	  @IsInProcess bit = 0
		Declare	  @IsNoReadings bit = 0
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		DECLARE @MaxCustomerReadinglogId INT,@LogIsLocked BIT,@LogApprovalStatusId INT
		
		SELECT TOP 1 @MaxCustomerReadinglogId = CustomerReadingLogId
			,@LogIsLocked = IsLocked
			,@LogApprovalStatusId = ApproveStatusId
		FROM Tbl_CustomerReadingApprovalLogs 
		WHERE GlobalAccountNumber = @GlobalAcountNumber
		ORDER BY CustomerReadingLogId DESC
		
		IF(@LogIsLocked = 0)
			BEGIN
				SET @IsExists = 1 
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingLogs
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 0
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingLogs where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@IsExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingLogs  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingLogs
					END
			END
		ELSE IF(@LogIsLocked = 1 AND @LogApprovalStatusId = 1)
			BEGIN
				SET @IsInProcess = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingApprove
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 1 AND ApproveStatusId = 1
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingApprove where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@IsExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingApprove  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingApprove
					END
			END
		ELSE 
			BEGIN
				SET @IsNoReadings = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,IsBilled
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingId
				INTO #CustomerTopTwoReadingMain
				from Tbl_CustomerReadings
				where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
				Order By CustomerReadingId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingMain where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@IsExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingMain  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
							 @PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=IsBilled
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingMain
					END
			END
			
		--select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		--SELECT TOP(1) @Month = BillMonth,
		--@Year = BillYear ,@LastBillReadType= 
		--(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		--=CB.ReadCodeId)
		--,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		--WHERE AccountNo = @GlobalAcountNumber  
		--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		--SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		--CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		--DECLARE @IsLatestBill BIT = 0 	

		--IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		--BEGIN
		--	SET @IsLatestBill = 1
		--END 
		--ELSE
		--  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
			 @AcountNumber AS AccountNo,
			 @OldAcountNumber AS OldAccountNo,
			 @Name   AS Name,
			 @usage as Usage
			 ,@RouteName   as RouteNum
			 --,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
			 --,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS IsActiveMonth
			 ,@IsTamper as IsTamper
			 ,@IsExists	as IsExists 
			 ,@IsInProcess as IsApprovalProces
			 ,@IsNoReadings AS IsActive
			 ,convert(varchar(11),@LatestDate,105) as LatestDate
			 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
			 ,@MeterNumber as MeterNo
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
			 ,@PrvExists as IsPrvExists  
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
			 --,@LastBillReadType as LastBillReadType
			 ,ISNULL(@InititalReading,0) as	 InititalReading
			 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
	)
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  


END  


