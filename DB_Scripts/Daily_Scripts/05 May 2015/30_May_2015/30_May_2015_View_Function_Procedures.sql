GO
/****** Object:  View [dbo].[UDV_RptCustomerAndBookInfo]    Script Date: 05/30/2015 18:02:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[UDV_RptCustomerAndBookInfo]      
AS      
 SELECT CD.GlobalAccountNumber    
 ,CD.AccountNo    
 ,CD.OldAccountNo    
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName    
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
             ,CD.Service_Landmark    
             ,CD.Service_City,'',    
             CD.Service_ZipCode) AS [ServiceAddress]     
   ,PD.SortOrder    
   ,BN.SortOrder AS BookSortOrder    
   ,CD.ConnectionDate    
   ,PD.PoleID      
   ,PD.TariffClassID AS TariffId      
   ,TC.ClassName      
   ,PD.ReadCodeID      
   ,BN.BookNo      
   ,BN.BookCode    
   ,BN.ID AS BookId      
   ,BU.BU_ID      
   ,BU.BusinessUnitName      
   ,BU.BUCode      
   ,SU.SU_ID      
   ,SU.ServiceUnitName      
   ,SU.SUCode      
   ,SC.ServiceCenterId      
   ,SC.ServiceCenterName      
   ,SC.SCCode      
   ,C.CycleId      
   ,C.CycleName      
   ,C.CycleCode 
   ,PD.MeterNumber
   ,CA.OutStandingAmount
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD
  INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CA ON CA.GlobalAccountNumber=CD.GlobalAccountNumber
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber    
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo     
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId     
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID     
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID    
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID     
----------------------------------------------------------------------------------------------------    

GO
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 05/30/2015 18:04:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]           
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,  convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  ) 
		AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
   
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	  ,7 as [LastDueDays]
	  ,	CONVERT(VARCHAR(11),BillGeneratedDate+7,106)	 as LastDueDate
	  --,(SELECT dbo.fn_GetCustomer_ADC(CB.Usage)) AS ADC
	  ,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID 
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint_Customerwise]    Script Date: 05/30/2015 18:04:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint_Customerwise]
(  
 @GlobalAcountNumber VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (
  
   
  SELECT         
	CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]             
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
	 ,convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  )  AS Usage              
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate    
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	,7 as [LastDueDays]
	,CONVERT(VARCHAR(11),BillGeneratedDate+7,106) as LastDueDate
	,(SELECT dbo.fn_GetCustomer_ADC(CB.Usage)) AS ADC
	,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID  
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
     where AccountNo=@GlobalAcountNumber and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomer_ADC]    Script Date: 05/30/2015 18:06:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 29-MAY-2015
-- Description:	The purpose of this function is to calculate average daily consumption of a customer.
-- =============================================
CREATE function [dbo].[fn_GetCustomer_ADC]
(
@Usage INT
)
RETURNS DECIMAL(18,2)
AS 
BEGIN
	RETURN CONVERT(DECIMAL(18,2),@Usage)/31
END
GO

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetDistrictMessages_ByBu_Id]    Script Date: 05/30/2015 18:07:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 22 MAY 2014
-- Description:	The purpose of this function is to get the District messages Based on BU_ID
-- =============================================

  
ALTER FUNCTION [dbo].[fn_GetDistrictMessages_ByBu_Id]
(
@BU_ID VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Message VARCHAR(MAX)
			
	
		SELECT @Message = SUBSTRING((SELECT ','+[Message]+'<\n>'
							  FROM Tbl_GlobalMessages 
							  WHERE BU_ID = @BU_ID
								AND ActiveStatusId = 1
							FOR XML PATH('')),2,200000)			
	RETURN @Message 
						   
END
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomer_ADC]    Script Date: 05/30/2015 18:07:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 29-MAY-2015
-- Description:	The purpose of this function is to calculate average daily consumption of a customer.
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomer_ADC]
(
@Usage INT
)
RETURNS DECIMAL(18,2)
AS 
BEGIN
	RETURN CONVERT(DECIMAL(18,2),@Usage)/31
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 05/30/2015 18:10:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate
					,ApprovedBy 
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					,@ModifiedBy
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill
							UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							WHERE CustomerBillId=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 05/30/2015 18:10:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
   SET @NewConsumption=@Consumption-@AdjustedUnits;  
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * 5)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @NewConsumption END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @NewConsumption END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@ModifedBy
								 ,@ModifiedBy
								 ,GETDATE()  
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected 
							 ,CreatedBy
							 ,ApprovedBy
							 ,CreatedDate  
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected 
							 ,@ModifiedBy
							 ,@ModifiedBy
							 ,GETDATE() 
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = @AdjustmentLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_BillAdjustment]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <22-JULY-2014>
-- Description:	<INSERT BILL ADJUSTMENT DETAILS>
-- Modified By : Padmini
-- Modified Date : 26-12-2014
-- =============================================
ALTER PROCEDURE [dbo].[USP_Insert_BillAdjustment]
(
	@XmlDoc xml=null
	,@MultiXmlDoc XML
)
AS
BEGIN	
		DECLARE
			@BillAdjustmentId INT
			,@CustomerBillId INT
			,@AccountNo VARCHAR(50)
			,@CustomerId VARCHAR(50)
			,@AmountEffected DECIMAL(18,2)
			,@TaxEffected DECIMAL(18,2)
			,@TotalAmountEffected DECIMAL(18,2)
			,@AdjustedUnits INT
			,@BillAdjustmentType INT
			,@BatchNo INT
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@ApprovalStatusId INT 
			,@Details VARCHAR(MAX)  
			,@EnergyCharges DECIMAL(18,2)
			,@AdditionalCharges DECIMAL(18,2)
			,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
		SELECT
			@CustomerBillId = C.value('(CustomerBillId)[1]','INT')
			,@AccountNo	= C.value('(AccountNo)[1]','VARCHAR(50)')
			,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')
			,@TaxEffected = C.value('(TaxEffected)[1]','DECIMAL(18,2)')
			,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)')
			,@BillAdjustmentType	= C.value('(BillAdjustmentType)[1]','INT')
			,@BatchNo = C.value('(BatchNo)[1]','INT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
			,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
			,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)')
				,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C) 

		
 		IF((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				INSERT INTO Tbl_BillAdjustments(
					CustomerBillId
					,AccountNo
					,CustomerId
					,AmountEffected
					,BillAdjustmentType
					,TaxEffected
					,TotalAmountEffected
					,BatchNo
					,ApprovedBy
					,CreatedBy
					,CreatedDate
					) 						
				VALUES(					
					@CustomerBillId
					,@AccountNo
					,@CustomerId
					,@AmountEffected
					,@BillAdjustmentType
					,@TaxEffected
					,@TotalAmountEffected
					,@BatchNo
					,@ModifiedBy
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					)		
				SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
				INSERT INTO Tbl_BillAdjustmentDetails
										(
										BillAdjustmentId
										,AdditionalChargesID
										,AdditionalCharges
										,EnergryCharges
										,CreatedBy
										,CreatedDate
										)
				SELECT       
					@BillAdjustmentId 
					,C.value('(ChargeID)[1]','INT')       
					,C.value('(AmountEffected)[1]','DECIMAL(18,2)')      
					,@EnergyCharges
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
				 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(C)    	
				
				--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
				,ModifedBy=@ModifiedBy
				,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
				 
				--Updating the adjustment amount in Bill
				UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
				WHERE CustomerBillId=@CustomerBillId
				 
			END
			SELECT 
					(CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess
				FOR XML PATH('BillAdjustmentsBe'),TYPE
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
 	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_AdjustmentForNoBill]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment
--Modified By  : Bhimaraju 
--Created Date : 02-May-2015    
--Description : Logs And Approvals insertion
--===================================         
ALTER PROCEDURE [dbo].[USP_AdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
    @BillAdjustmentId INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)    
   ,@TotalAmountEffected DECIMAL(18,2)    
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT
   ,@FunctionId INT
   ,@CreatedBy VARCHAR(50)
   ,@IsFinalApproval BIT
   ,@BU_ID VARCHAR(50)
  ,@CurrentApprovalLevel INT =0 
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
   ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')   
   ,@FunctionId = C.value('(FunctionId)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')    
   ,@TotalAmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       

	IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
				WHERE GlobalAccountNumber = @AccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillAdjustmentsBe')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AdjustmentRequestId INT
					,@Forward INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
						SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
				INSERT INTO Tbl_AdjustmentsLogs
							(		 GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId
									,CreatedBy 
									,CreatedDate
									,CurrentApprovalLevel
									,IsLocked
							)
				SELECT   @AccountNo
						,AccountNo
						,NULL--BillNumber
						,@BillAdjustmentType
						,MeterNumber
						,InitialReading
						,NULL--CurrentReadingAfterAdjustment
						,NULL--CurrentReadingBeforeAdjustment
						,NULL--AdjustedUnits
						,NULL--TaxEffected
						,@TotalAmountEffected
						,NULL-- EnergryCharges 
						,NULL--	AdditionalCharges
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
						,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM UDV_CustomerDescription
				WHERE GlobalAccountNumber=@AccountNo
				
				SET @AdjustmentRequestId = SCOPE_IDENTITY()
					
				IF(@IsFinalApproval = 1)
				BEGIN
			  						
				   INSERT INTO Tbl_BillAdjustments(
					 AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected
					 ,ApprovedBy
					 ,CreatedBy
					 ,CreatedDate
					 )           
					VALUES(         
					  @AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@TotalAmountEffected
					 ,@CreatedBy
					 ,@CreatedBy
					 ,GetDate()
					 )      
				   SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
   
				   INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
					  SELECT @BillAdjustmentId,@AmountEffected
					
					--OutStanding Amount is Updating Start
			
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=ISNULL(OutStandingAmount,0) - @TotalAmountEffected
					WHERE GlobalAccountNumber=@AccountNo
					--OutStanding Amount is Updating End
										  
				   INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
				   SELECT		     AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
				   FROM Tbl_AdjustmentsLogs
				   WHERE AdjustmentLogId=@AdjustmentRequestId
					
				END
			   --SELECT (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
			     SELECT 1 As IsSuccess    
			  FOR XML PATH('BillAdjustmentsBe'),TYPE    
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAddressChangeApproval]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAddressChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@AddressChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @AddressChangeLogId = C.value('(AddressChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@TempAddressId INT,
		@TempServiceId INT
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNumber 
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		FROM Tbl_CustomerAddressChangeLog_New
		WHERE AddressChangeLogId=@AddressChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId)
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
					--						WHERE AddressChangeLogId = @AddressChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					DECLARE @FinalApproval BIT

	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
			SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
					
					
					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel= CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerAddressChangeLog_New
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel = 0
						,IsLocked = 1
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					AND  AddressChangeLogId= @AddressChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber AND  AddressChangeLogId= @AddressChangeLogId)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedDate
									,CreatedBy)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
							--									WHERE AddressChangeLogId = @AddressChangeLogId))
									SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
						WHERE AddressChangeLogId = @AddressChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerAddressChangeLog_New
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked = 1
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			AND AddressChangeLogId = @AddressChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
	,@BU_ID VARCHAR(50)  
	,@CurrentApprovalLevel INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT
							,@CurrentLevel INT
							,@PresentRoleId INT
							,@NextRoleId INT
							,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			--IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
			--	BEGIN
			--		SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
			--	END
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
							
				
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,CurrentApprovalLevel
																,IsLocked
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,CurrentApprovalLevel
																,IsLocked
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						 
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId
							,GlobalAccountNumber
							,OldPostal_HouseNo
							,OldPostal_StreetName
							,OldPostal_City
							,OldPostal_Landmark
							,OldPostal_AreaCode
							,OldPostal_ZipCode
							,NewPostal_HouseNo
							,NewPostal_StreetName
							,NewPostal_City
							,NewPostal_Landmark
							,NewPostal_AreaCode
							,NewPostal_ZipCode
							,OldService_HouseNo
							,OldService_StreetName
							,OldService_City
							,OldService_Landmark
							,OldService_AreaCode
							,OldService_ZipCode
							,NewService_HouseNo
							,NewService_StreetName
							,NewService_City
							,NewService_Landmark
							,NewService_AreaCode
							,NewService_ZipCode
							,ApprovalStatusId
							,Remarks
							,CreatedBy
							,CreatedDate
							,ModifiedBy
							,ModifiedDate
							,PresentApprovalRole
							,NextApprovalRole
							,IsPostalCommunication
							,IsServiceComunication
							) 
					SELECT  
							AddressChangeLogId
							,GlobalAccountNumber
							,OldPostal_HouseNo
							,OldPostal_StreetName
							,OldPostal_City
							,OldPostal_Landmark
							,OldPostal_AreaCode
							,OldPostal_ZipCode
							,NewPostal_HouseNo
							,NewPostal_StreetName
							,NewPostal_City
							,NewPostal_Landmark
							,NewPostal_AreaCode
							,NewPostal_ZipCode
							,OldService_HouseNo
							,OldService_StreetName
							,OldService_City
							,OldService_Landmark
							,OldService_AreaCode
							,OldService_ZipCode
							,NewService_HouseNo
							,NewService_StreetName
							,NewService_City
							,NewService_Landmark
							,NewService_AreaCode
							,NewService_ZipCode
							,ApprovalStatusId
							,Remarks
							,CreatedBy
							,CreatedDate
							,ModifiedBy
							,ModifiedDate
							,PresentApprovalRole
							,NextApprovalRole
							,IsPostalCommunication
							,IsServiceComunication
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsList]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 11-08-2014   
-- Modified date: 28-10-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get customer  Details By AccountNo,MeterNo  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsList]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
  ,@OldAccountNo VARCHAR(50)  
  ,@MeterNo VARCHAR(50)  
  ,@BUID VARCHAR(50)=''  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
  ,@OldAccountNo=C.value('(OldAccountNo)[1]','VARCHAR(50)')    
  ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')    
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)    
     
  IF EXISTS(SELECT 0 FROM UDV_IsCustomerExists WHERE (BU_ID=@BUID OR @BUID='')  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
 BEGIN  
   SELECT    
      CD.GlobalAccountNumber AS AccountNo  
     ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name   
     ,(SELECT dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber)) AS ServiceAddress  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber)),'--') AS LastBillDate  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)),'--') As LastPaidDate  
     ,CAD.OutStandingAmount AS DueAmount  
     ,CPD.TariffClassID AS ClassID  
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=CPD.TariffClassID) AS Tariff  
     ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CPD.ClusterCategoryId) AS ClusterType
     ,(SELECT ClusterCategoryId FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CPD.ClusterCategoryId) AS ClusterTypeId
     ,1 AS IsSuccess  
     ,CD.OldAccountNo --Faiz-ID103
   FROM CUSTOMERS.Tbl_CustomersDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
  INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber
  INNER JOIN UDV_BookNumberDetails BD ON CPD.BookNo=BD.BookNo
   WHERE (CD.GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='')    
   AND (BU_ID=@BUID OR @BUID='') 
   FOR XML PATH('TariffManagementBE')    
 END  
 ELSE IF EXISTS(SELECT 0 FROM UDV_IsCustomerExists WHERE BU_ID!=@BUID  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
  BEGIN  
   SELECT 1 AS IsCustExistsInOtherBU,1 AS IsSuccess FOR XML PATH('TariffManagementBE')   
  END  
 ELSE  
  SELECT 0 AS IsSuccess FOR XML PATH('TariffManagementBE')   
END   

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- Modified By : Bhimaraju V
-- Modified Date:06-05-2015    
-- Description: Updating the Direct Cust Avg reading in Main Table
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
	
	-- To get the Payments data to validate
	SELECT ISNULL(Temp.AverageReading,0) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,ISNULL(CD.GlobalAccountNumber,'') AS AccNum
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
		--							AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--	when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
		--	else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CPD.ReadCodeID
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions K
				WHERE AccountNo = (ISNULL(CD.GlobalAccountNumber,0))
				AND K.AvgUploadFileId = Temp.AvgUploadFileId GROUP BY AccountNo) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions(NOLOCK) AS Temp
	INNER JOIN Tbl_AverageUploadFiles(NOLOCK) PUF ON PUF.AvgUploadFileId = Temp.AvgUploadFileId  
	INNER JOIN Tbl_AverageUploadBatches(NOLOCK) PUB ON PUB.AvgUploadBatchId = PUF.AvgUploadBatchId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (Temp.AccountNo = CD.OldAccountNo) --CD.GlobalAccountNumber = Temp.AccountNo OR 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions(NOLOCK)) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the paid amount is valid or not
			IF((SELECT 0 FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 0) > 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE ISNUMERIC(AverageReading) = 0
					
					INSERT INTO Tbl_AverageFailureTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,Comments
					)
					SELECT 
						 AvgUploadFileId
						,SNO
						,TempAccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,STUFF(Comments,1,2,'')
					FROM #AverageReadings
					WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
										
					SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
					
					-- TO delete the Failure Transactions from Reading Transaction table	
					DELETE FROM Tbl_AverageTransactions	
					WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
												WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0)
				 
					-- TO delete the Failure Transactions from temp table
					DELETE FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
						
				END				
			-- TO check whether the given AverageReading is valid or not
			
			IF((SELECT 0 FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 1) > 0)
				BEGIN								
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #AverageReadings
			WHERE IsValid = 0
								
			SELECT @FailureTransactions = ISNULL(@FailureTransactions,0) + COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
				,ReadCodeID
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber
			
			--WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
	    
		    UPDATE CAD   
			SET CAD.AvgReading = Tc.AverageReading
			FROM CUSTOMERS.Tbl_CustomerActiveDetails CAD
			INNER JOIN #ValidReadings TC ON TC.AccNum = CAD.GlobalAccountNumber
			AND TC.ReadCodeID=1-- For Direct CustomersOnly updating the avg reading
		     
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(VARCHAR(20),PaidAmount) = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNo) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
		--									AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,BD.BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	INNER JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId  
	INNER JOIN Tbl_BatchDetails(NOLOCK) BD ON BD.BatchNo = PUB.BatchNo AND BD.BatchDate = PUB.BatchDate
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.OldAccountNo = TempDetails.AccountNo
	--((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
	--					OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the paid amount is valid or not
			IF((SELECT 0 FROM #PaymentValidation WHERE ISNUMERIC(PaidAmount) = 0) > 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Amount'
					WHERE ISNUMERIC(PaidAmount) = 0
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				--UPDATE B
				--SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				--FROM Tbl_BatchDetails B
				--INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
			
	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(Case when CustomerReadings.IsBilled IS NULL then 0
			else CustomerReadings.IsBilled end) as IsBilled
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,CustomerReadingsLogs.IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) AS IsDisabledBook
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1 OR PrvExist = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Billed'
					WHERE IsBilled = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE IsRollOver = 1
					
				END
					
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					--IF(@IsExists = 1)
					--	BEGIN					
					--		DELETE FROM	Tbl_CustomerReadings 
					--		WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
					--								FROM Tbl_CustomerReadings
					--								WHERE GlobalAccountNumber = @AccountNo
					--								ORDER BY CustomerReadingId DESC)						
					--	END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),@PreviousReading)
						,@PresentReading
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = @PreviousReading
						,PresentReading = @PresentReading 
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadingBatchProcesFailures]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to get Reading Batch Proces Failures Detials  
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetReadingBatchProcesFailures]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PageNo INT   
			,@PageSize INT 
			,@RUploadFileId INT 
			--,@RUploadBatchId INT
		
	SELECT 
		 @PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@RUploadFileId = C.value('(RUploadFileId)[1]','INT') 
	FROM @XmlDoc.nodes('ReadingsBatchBE') AS T(C)    
  
	--SELECT @RUploadBatchId = RUploadBatchId FROM Tbl_ReadingUploadFiles WHERE RUploadFileId = @RUploadFileId

	SELECT 
			RUF.TotalFailureTransactions,
			RUF.TotalCustomers,
			RUB.BatchNo,
			--RUB.BatchDate,
			CONVERT(VARCHAR(24),RUB.BatchDate,106) AS BatchDate,
			RUB.Notes
	FROM Tbl_ReadingUploadFiles RUF
	JOIN Tbl_ReadingUploadBatches RUB ON RUB.RUploadBatchId = RUF.RUploadBatchId
	WHERE RUF.RUploadFileId = @RUploadFileId
	
;WITH PagedResults AS    
	(    
		SELECT 
			ROW_NUMBER() OVER(ORDER BY CreatedDate DESC ) AS RowNumber, 
			SNO,
			AccountNo,
			CurrentReading,
			ReadingDate,
			Comments,
			ReadingFailureransactionID
		FROM Tbl_ReadingFailureTransactions
		WHERE RUploadFileId = @RUploadFileId
	)
		SELECT *    
		 ,(SELECT COUNT(0) FROM PagedResults) AS TotalRecords         
		FROM PagedResults    
		WHERE RowNumber BETWEEN (@PageNo - 1) * @PageSize + 1 AND @PageNo * @PageSize 

END


GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
      
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount
	END
END  
				 

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 

 
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd   .Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN @IsEmbassyCustomer=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						IF	 isnull(@GetPaidMeterBalanceAfterBill,0) <>0
						BEGIN
						
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
 
						update Tbl_PaidMeterDetails
						SET OutStandingAmount =@PaidMeterDeductedAmount -isnull(OutStandingAmount,0)  
						WHERE AccountNo = @GlobalAccountNumber	 -- and ActiveStatusId=1
					 	
						END
						 
				
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1 where GlobalAccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo    
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmount - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomerLedgers]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Karteek
-- Create date: 09 May 2015
-- Description: The purpose of this procedure is to Get the Customers Ledger Report by account no
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomerLedgers]
(  
	@XmlDoc XML  
)  
AS  
BEGIN  
	
	Declare @AcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@ReportMonths INT
		,@CurrentDate DATETIME 
		,@BUID VARCHAR(50)=''
		,@GlobalAcountNo VARCHAR(50)

	SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()	 

	SELECT  
		  @AcountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)')   
		  ,@ReportMonths = C.value('(ReportMonths)[1]','INT') 
		  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('LedgerBe') as T(C)  
	
	SELECT @GlobalAcountNo = GlobalAccountNumber FROM [UDV_CustomerDescription](NOLOCK) CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND (BU_ID=@BUID OR @BUID='') --AND ActiveStatusId=1--Allow all customers

   IF @GlobalAcountNo IS NOT NULL
   BEGIN
			DECLARE @ResultTable TABLE(Id INT IDENTITY(1,1),CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))
			DECLARE @FinalResult TABLE(Id INT,CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))

			DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
		    
			INSERT INTO @ResultedMonths
			SELECT [YEAR],[MONTH] FROM DBO.fn_GetCurrentYearMonths_ByCurrentDate(@CurrentDate)-- ORDER BY Id DESC
									
			INSERT INTO @ResultTable (CreateDate,Particulars,Debit,Credit,Balance,Remarks)
			SELECT OpeningDate,'Opening Balance',TotalPendingAmount,0,0,''
			FROM dbo.fn_GetCustomerYear_OpeningBalance(@GlobalAcountNo)	
			

			SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
			---- Collection all the transaction Start
			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
			  BEGIN      
				SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
				
				SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
				SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

				INSERT INTO @ResultTable (CreateDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks)
				SELECT CreatedDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks FROM 
				(SELECT CONVERT(DATE,CreatedDate) AS CreatedDate
						,BillNo AS ReferenceNo
						,'Sale of energy' As Particulars
						,0 AS Debit
						,TotalBillAmountWithTax As Credit
						,0 AS Balance
						,'' As Remarks
					FROM Tbl_CustomerBills(NOLOCK)
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
					--AND BillMonth = @PresentMonth AND BillYear = @PresentYear
				UNION
					SELECT 
						CONVERT(DATE,CreatedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),BillAdjustmentId) AS ReferenceNo
						,'Adjustment' As Particulars
						,0 AS Debit
						,AmountEffected As Credit,0 AS Balance,Remarks
					FROM Tbl_BillAdjustments(NOLOCK) 
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				UNION
					SELECT CONVERT(DATE,RecievedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),CustomerPaymentID) AS ReferenceNo
						,(SELECT BillingType FROM Tbl_MBillingTypes WHERE BillingTypeId = CP.ReceivedDevice)+' Payments' As Particulars
						,PaidAmount AS Debit
						,0 As Credit
						,0 AS Balance
						,Remarks
					FROM Tbl_CustomerPayments(NOLOCK) CP
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,RecievedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				) AS A ORDER By CONVERT(DATE,A.CreatedDate)	
				  
				  IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
							IF(@MonthId IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Collection all the transaction END

			---- Finalise the transaction Records Start

			DECLARE @PresentTrans INT 
			SELECT TOP(1) @PresentTrans = Id FROM @ResultTable ORDER BY Id ASC    

			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultTable WHERE Id >= @PresentTrans ORDER BY Id ASC))    
			  BEGIN 
					
					IF NOT EXISTS(SELECT 0 FROM @FinalResult WHERE Id < @PresentTrans)
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 THEN ((0 - Debit)) 
								   WHEN Credit <> 0 THEN (0 + Credit)
								   ELSE 0 END)AS Balance
							,CreateDate
							,Remarks	   
						FROM @ResultTable WHERE Id = @PresentTrans
					END
				ELSE
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) - Debit))
								 WHEN Credit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) + Credit))
									END)AS Balance
							,CreateDate
							,Remarks		
						FROM @ResultTable WHERE Id = @PresentTrans
					END
					
				  IF(@PresentTrans = (SELECT TOP(1) Id FROM @ResultTable ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentTrans = (SELECT TOP(1) Id FROM @ResultTable WHERE  Id > @PresentTrans ORDER BY Id ASC)    
							IF(@PresentTrans IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Finalise the transaction Records End
		    
		   
				 SELECT 
					  (GlobalAccountNumber + ' - ' + AccountNo) AS GlobalAccountNumber
					  --,KnownAs AS Name
					  ,GlobalAccountNumber AS AccountNo
					  ,(SELECT dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)) AS Name
					  ,ISNULL(ISNULL(HomeContactNo,ISNULL(BusinessContactNo,OtherContactNo)),'--') AS MobileNo
					  ,ISNULL(OldAccountNo,'--') AS OldAccountNo
					  ,CD.ClassName 
					  ,ISNULL(EmailId,'--') AS EmailId
					  ,CD.BusinessUnitName
					  ,CD.ServiceUnitName
					  ,CD.ServiceCenterName
					  ,CD.BookNo
					  ,CD.CycleName
					  ,ISNULL(CD.MeterNumber,'--') AS MeterNumber
					  ,dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,
										Service_City,'',Service_ZipCode) AS ServiceAddress
					  ,1 AS IsSuccess
					  ,CONVERT(VARCHAR,CAST(ISNULL(AvgReading,0) AS DECIMAL(18,2)),-1) AS AvgReading
					  ,CONVERT(VARCHAR,CAST(OutStandingAmount AS MONEY),-1) AS OutStandingAmount
					  ,CD.ReadCodeID
					  ,ReadCode
					  ,0 AS IsCustExistsInOtherBU
			   FROM [UDV_CustomerDescription](NOLOCK) CD
			   INNER JOIN Tbl_MReadCodes(NOLOCK) RC ON RC.ReadCodeId = CD.ReadCodeID
					AND GlobalAccountNumber = @GlobalAcountNo
			
				SELECT 
					 ROW_NUMBER() OVER(ORDER BY Particulars) AS RowNumber
					,Particulars
					,CONVERT(VARCHAR,CAST(ISNULL(Debit,0) AS MONEY),-1) AS Debit
					,CONVERT(VARCHAR,CAST(ISNULL(Credit,0) AS MONEY),-1) AS Credit
					,CONVERT(VARCHAR,CAST(ISNULL(Balance,0) AS MONEY),-1) AS Balance
					,CONVERT(VARCHAR,CAST((-1*ISNULL(Balance,0)) AS MONEY),-1) As Due		
					,CONVERT(VARCHAR,CONVERT(VARCHAR(50),CreateDate,106)) AS TransactionDate
					,ReferenceNo
					,Remarks
				FROM @FinalResult 
			
	END
    ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND BU_ID!=@BUID AND ActiveStatusId=1)
    BEGIN
		
			SELECT 1 AS IsSuccess,1 AS IsCustExistsInOtherBU
	
    END
    ELSE
    BEGIN
		
			SELECT 0 AS IsSuccess
	
    END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithMeter]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptReadCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.GlobalAccountNumber +' - '+CD.AccountNo) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,CD.MeterNumber
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=2
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  ,MeterNumber
				  ,CONVERT(VARCHAR(20),ConnectionDate,106) AS ConnectionDate
				  --,@Count As TotalRecords 
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithoutMeter]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithoutMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptDirectCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.GlobalAccountNumber +' - '+CD.AccountNo) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=1
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,CONVERT(VARCHAR(20),ConnectionDate,106)AS ConnectionDate
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  --,@Count As TotalRecords
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateConsumptionDelivered]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To Update the Consumption Delivered record.
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @ConsumptionDeliveredId INT
			,@Capacity	DECIMAL(18,2)
			,@SUPPMConsumption	DECIMAL(18,2)
			,@SUCreditConsumption	DECIMAL(18,2)
			,@TotalConsumption	DECIMAL(18,2)
			,@ModifiedBy	VARCHAR(50)
	
	SELECT 
			@ConsumptionDeliveredId=C.value('(ConsumptionDeliveredId)[1]','INT')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL(18,2)')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL(18,2)')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL(18,2)')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL(18,2)')
		    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	UPDATE Tbl_ConsumptionDelivered 
	SET		Capacity=@Capacity
			,SUPPMConsumption=@SUPPMConsumption
			,SUCreditConsumption=@SUCreditConsumption
			,TotalConsumption=(@SUPPMConsumption+@SUCreditConsumption)
			,ModifiedBy=@ModifiedBy
			,ModifiedDate=dbo.fn_GetCurrentDateTime()
	WHERE	ConsumptionDeliveredId=@ConsumptionDeliveredId
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertConsumptionDelivered]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Apr-2015
-- Description:	To save the Consumption Delivered record.
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertConsumptionDelivered]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @BU_ID	VARCHAR(50)
			,@SU_ID	VARCHAR(50)
			,@Capacity	DECIMAL(18,2)
			,@SUPPMConsumption	DECIMAL(18,2)
			,@SUCreditConsumption	Decimal(18,2)
			,@TotalConsumption	DECIMAL(18,2)
			,@ConsumptionMonth	INT
			,@ConsumptionYear	INT
			,@CreatedBy	VARCHAR(50)
			,@ConsumptionDeliveredId INT = 0
	
	SELECT 
			@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(50)')
			,@Capacity=C.value('(Capacity)[1]','DECIMAL(18,2)')
			,@SUPPMConsumption=C.value('(SUPPMConsumption)[1]','DECIMAL(18,2)')
			,@SUCreditConsumption=C.value('(SUCreditConsumption)[1]','DECIMAL(18,2)')
			,@TotalConsumption=C.value('(TotalConsumption)[1]','DECIMAL(18,2)')
			,@ConsumptionMonth=C.value('(ConsumptionMonth)[1]','INT')
			,@ConsumptionYear=C.value('(ConsumptionYear)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ConsumptionDeliveredBE') as T(C)	   
	
	SELECT @ConsumptionDeliveredId=ISNULL(ConsumptionDeliveredId,0) FROM Tbl_ConsumptionDelivered 
									WHERE SU_ID=@SU_ID AND ConsumptionMonth=@ConsumptionMonth 
									AND ConsumptionYear=@ConsumptionYear	
	
	IF (@ConsumptionDeliveredId=0)
	BEGIN
		INSERT INTO Tbl_ConsumptionDelivered(
											BU_ID
											,SU_ID
											,Capacity
											,SUPPMConsumption
											,SUCreditConsumption
											,TotalConsumption
											,ConsumptionMonth
											,ConsumptionYear
											,CreatedBy
											,CreatedDate
											)
									VALUES	(
											@BU_ID
											,@SU_ID
											,@Capacity
											,@SUPPMConsumption
											,@SUCreditConsumption
											,(@SUPPMConsumption + @SUCreditConsumption)
											,@ConsumptionMonth
											,@ConsumptionYear
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											)
		SELECT @@ROWCOUNT AS RowsEffected
		FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	END
	ELSE
	BEGIN
		SELECT 1 AS ConsumptionRecordExists, 
				@ConsumptionDeliveredId AS ConsumptionDeliveredId
		FOR XML PATH('ConsumptionDeliveredBE'),TYPE	
	END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookwiseCUstomerReadings]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : Karteek
-- Modified date : 21-May-2015
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookwiseCUstomerReadings] 
(
	@XmlDoc xml 
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  


	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
		dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
		,MeterNumber as CustomerExistingMeterNumber
		,MeterDials As CustomerExistingMeterDials
		,Decimals as Decimals
		,InitialReading as	InitialReading
		,AccountNo
		,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeID = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId=1  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadingApprovalLogs   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND ApproveStatusId IN(1,2)
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,ISNULL(CustomerReadingInner.IsLocked,0) AS IsLocked
	,CustomerReadingInner.ApproveStatusId
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadingApprovalLogs CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadIDMain
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND IsBilled = 0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadingsMain
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDMain	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId 
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------

	--Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	--INTO #CusotmersWithMaxBillID    
	--from Tbl_CustomerBills CustomerBillInnner 
	--INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	--Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 --select CustomerBillMain.AccountNo as GlobalAccountNumber
	 --,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 --INTO #CustomerLatestBills  
	 --from  Tbl_CustomerBills As CustomerBillMain
	 --INNER JOIN 
	 --#CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 --on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) 
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end)
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end)
				ELSE (Case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) END) as IsExists,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				ELSE ISNULL(CONVERT(VARCHAR(20),CustomerReadingsMain.ReadDate,106),'--') END) as LatestDate,
			CustomerList.Name,
			--(CASE WHEN CustomerReadings.IsLocked = 0 AND CustomerReadings.ApproveStatusId = 1 THEN 1
			--	WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
			--		THEN 2
			--	ELSE 3 END) as ApproveStatusId,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) THEN 1
				WHEN CustomerReadings.ApproveStatusId = 1 
					THEN 2
				ELSE 3 END) as ApproveStatusId,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.ReadingMultiplier
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN CustomerReadings.ReadingMultiplier
				ELSE CustomerReadingsMain.ReadingMultiplier END) as Multiplier,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.IsTamper
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN CustomerReadings.IsTamper
				ELSE CustomerReadingsMain.IsTamper END) as IsTamper,
			0 as Decimals,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				ELSE ISNULL(CustomerReadingsMain.AverageReading,'0.00') END) as AverageReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.TotalReadings
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN CustomerReadings.TotalReadings
				ELSE CustomerReadings.TotalReadings END) as TotalReadings,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else NULL end)
								 End)
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else NULL end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
									else NULL end)
								 End) END) as PresentReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
								else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end)
				ELSE 0 END) as PrvExist,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				ELSE (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadingsMain.Usage,0) end) END) as Usage,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				WHEN CustomerReadings.ApproveStatusId = 1
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading							
											else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
									else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End) END) as PreviousReading
			,CustomerList.BookNo
			,CustomerList.InitialReading
			,CustomerReadings.IsRollOver AS Rollover
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerLatestReadingsMain	  As CustomerReadingsMain
			ON
			CustomerList.GlobalAccountNumber=CustomerReadingsMain.GlobalAccountNumber	
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table #CustomerLatestReadingsMain
DROP Table #CusotmersWithMaxReadIDMain
DROP Table  #CustomersListBook  
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 08 May 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@OldAccountNo VARCHAR(50)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount 
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@OldAccountNo=OldAccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY CustomerPaymentID DESC
    
    SELECT 
    (
		SELECT CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
			,@LastPaidAmount AS LastPaidAmount
			,@GlobalAccountNo AS AccountNo
			,@Name AS Name
			,@ServiceAddress AS ServiceAddress
			,@OutstandingAmount AS OutStandingAmount
			,@BookGroup AS BookGroup 
			,@BookName AS BookName
			,@Tariff AS Tariff
			,@OldAccountNo AS OldAccountNo
		FOR XML PATH('Customerdetails'),TYPE 
    )
    ,
    (
		SELECT TOP(1)
			 CB.BillNo
			,CustomerBillId AS CustomerBillID
			,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
			,CB.BillYear        
			,CB.BillMonth        
			,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
			,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
			,ISNULL(CB.PreviousReading,'0') AS PreviousReading
			,ISNULL(CB.PresentReading,'0') AS PresentReading
			,Usage AS Consumption
			,Dials AS MeterDials
			,NetEnergyCharges
			,NetFixedCharges
			,TotalBillAmount
			,VAT
			,TotalBillAmountWithTax
			,NetArrears
			,TotalBillAmountWithArrears 
			,CB.AccountNo AS AccountNo
			,CB.PaidAmount AS LastPaidAmount
		FROM Tbl_CustomerBills CB 
		WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
		ORDER BY CB.CustomerBillId DESC
		FOR XML PATH('CustomerBillDetails'),TYPE 
    )
    FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')            
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@CustomerTypeId INT
			,@OldAccountNo VARCHAR(50)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount 
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@CustomerTypeId=CustomerTypeId
			,@OldAccountNo=OldAccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC, CustomerPaymentID  DESC
	
	
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
		,RC.DisplayCode AS ReadType
		,CB.BillYear        
		,CB.BillMonth        
		,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
		,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		
		,Usage AS Consumption
		,Dials AS MeterDials
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT 
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 		
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,CB.AccountNo AS AccountNo
		,@Name AS Name
		,@ServiceAddress AS ServiceAddress
		,@OutstandingAmount AS OutStandingAmount
		,@BookGroup AS BookGroup 
		,@BookName AS BookName
		,@Tariff AS Tariff
		,@OldAccountNo AS OldAccountNo
		,COUNT(0) OVER() AS TotalRecords
		,VAT AS Vat
		,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS ClassName
		,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=@CustomerTypeId) AS CustomerType
		
	FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
	WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
	ORDER BY CB.CustomerBillId DESC
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     


GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]    Script Date: 05/30/2015 18:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================

ALTER PROCEDURE [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @GlobalAccountNo VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	DECLARE @IsDisabledBook BIT 
	
	SELECT @CustomerBusinessUnitID = U.BU_ID,@CustomerActiveStatusID = ISNULL(U.ActiveStatusId,0)  
		,@GlobalAccountNo = U.GlobalAccountNumber
		,@IsDisabledBook = (CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN IsPartialBill IS NULL THEN 1 ELSE 0 END)
								ELSE 0 END)
	FROM  UDV_IsCustomerExists U
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = U.BookNo
	WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo OR MeterNumber=@AccountNo)
	  	
	IF @GlobalAccountNo IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
		IF @CustomerBusinessUnitID = @BUID    
			BEGIN
				IF @CustomerActiveStatusID = 1 --OR @CustomerActiveStatusID=2 
					BEGIN
						--PRINT 'Sucess and Active  Customer'
						SELECT 1 AS IsSuccess,
							@GlobalAccountNo AS AccountNo,
							@IsDisabledBook AS CustIsNotActive
						FOR XML PATH('RptCustomerLedgerBe') 
					END
				ELSE
					BEGIN							 
						--PRINT 'Failure In Active Status'
						SELECT 0 AS IsSuccess
						FOR XML PATH('RptCustomerLedgerBe') 
					END
			END
		ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU ,
						@IsDisabledBook AS CustIsNotActive
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO
