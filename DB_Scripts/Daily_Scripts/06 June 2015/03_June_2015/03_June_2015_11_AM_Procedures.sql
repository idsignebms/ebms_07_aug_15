
GO

/****** Object:  StoredProcedure [dbo].[UPS_IsBillInSchedule]    Script Date: 06/03/2015 10:59:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <NEERAJ KANOJIYA>      
-- Create date: <07-MAY-2015>      
-- Description: <CHECK FOR NEW BILL SCHEDULE>        
-- =============================================      
      
CREATE PROCEDURE  [dbo].[UPS_IsBillInSchedule]        
AS        
BEGIN       
  DECLARE @IsExists BIT =0     
 IF((SELECT COUNT(BillGenarationStatusId) FROM Tbl_BillingQueueSchedule WHERE BillGenarationStatusId=5)>0)
 BEGIN
	SET @IsExists=1	 
 END 
    SELECT @IsExists AS IsExists
	FOR XML PATH('BillGenerationList'),ROOT('BillGenerationInfoByXml')         
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteArea]    Script Date: 06/03/2015 10:59:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz - ID103>      
-- Create date: <02-Jun-2015>      
-- Description: <Delete Area details Row from MASTERS.Tbl_MAreaDetails> 
-- =============================================  
CREATE PROCEDURE [dbo].[USP_DeleteArea]
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
	DECLARE @AreaCode INT  
			,@IsActive BIT  
			,@ModifiedBy VARCHAR(50)  

	SELECT @AreaCode = C.value('(AreaCode)[1]','INT')  
			,@IsActive = C.value('(IsActive)[1]','BIT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('AreaBe') AS T(C)  

	IF NOT EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE AreaCode=@AreaCode)
		BEGIN
			UPDATE MASTERS.Tbl_MAreaDetails   
				SET IsActive = @IsActive,  
					ModifedBy = @ModifiedBy,  
					ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE AreaCode = @AreaCode  

			SELECT @@ROWCOUNT As RowsEffected      
			FOR XML PATH('AreaBe'),TYPE     
		END
	ELSE
		BEGIN
			SELECT 
				0 AS RowsEffected
				,1 AS IsExists			
			FOR XML PATH('AreaBe'),TYPE
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateArea]    Script Date: 06/03/2015 10:59:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz - ID103>        
-- Create date: <02-Jun-2015>        
-- Description: <Update Area in MASTERS.Tbl_MAreaDetails>        
-- =============================================        
CREATE PROCEDURE [dbo].[USP_UpdateArea]       
(        
@XmlDoc XML=null        
)        
AS        
BEGIN         
 
 DECLARE   @AreaCode INT
		   ,@Address1 VARCHAR(100)    
		   ,@Address2 VARCHAR(100) 
		   ,@City VARCHAR(50)   
		   ,@ZipCode VARCHAR(10)
		   ,@AreaDetails VARCHAR(100)
		   ,@ModifiedBy VARCHAR(50)
		           
            
		   
 SELECT    @AreaCode=C.value('(AreaCode)[1]','INT')         
		   ,@Address1=C.value('(Address1)[1]','VARCHAR(100)')    
		   ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')    
		   ,@City=C.value('(City)[1]','VARCHAR(50)')    
		   ,@ZipCode=C.value('(ZipCode)[1]','VARCHAR(10)')    
		   ,@AreaDetails=C.value('(AreaDetails)[1]','VARCHAR(100)') 
		   ,@ModifiedBy = C.value('(ModifedBy)[1]','VARCHAR(50)')        
 FROM @XmlDoc.nodes('AreaBe') as T(C)               
 --IF NOT EXISTS(SELECT 0 FROM MASTERS.Tbl_MAreaDetails WHERE Name=@Name AND AreaCode!=@AreaCode )        
 --BEGIN         
   UPDATE MASTERS.Tbl_MAreaDetails SET 
						Address1 = @Address1     
						,Address2=@Address2
						,City=@City
						,ZipCode=@ZipCode
						,AreaDetails = @AreaDetails         
						,ModifedBy = @ModifiedBy        
						,ModifiedDate= dbo.fn_GetCurrentDateTime()          
   WHERE AreaCode = @AreaCode       
           
 --END        
			SELECT 1 As RowsEffected        
			FOR XML PATH('AreaBe'),TYPE        
          
END        
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertArea]    Script Date: 06/03/2015 10:59:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jun-2015>    
-- Description: <For Insert Area Data Into MASTERS.Tbl_MAreaDetails>    
-- =============================================    
CREATE PROCEDURE [dbo].[USP_InsertArea]  
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @Address1 VARCHAR(100)    
		   ,@Address2 VARCHAR(100) 
		   ,@City VARCHAR(50)   
		   ,@ZipCode VARCHAR(10)
		   ,@AreaDetails VARCHAR(100)
		   ,@CreatedBy VARCHAR(50)    
       
 SELECT   @Address1=C.value('(Address1)[1]','VARCHAR(100)')    
		   ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')    
		   ,@City=C.value('(City)[1]','VARCHAR(50)')    
		   ,@ZipCode=C.value('(ZipCode)[1]','VARCHAR(10)')    
		   ,@AreaDetails=C.value('(AreaDetails)[1]','VARCHAR(100)')    
		   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')    
       
      
   FROM @XmlDoc.nodes('AreaBe') AS T(C)     
     
 --IF NOT EXISTs(SELECT 0 FROM MASTERS.Tbl_MAreaDetails WHERE Name=@Name AND AreaCode = @AreaCode)    
 --BEGIN    
  INSERT INTO MASTERS.Tbl_MAreaDetails(    
					  Address1  
					  ,Address2 
					  ,City
					  ,ZipCode
					  ,AreaDetails   
					  ,CreatedBy    
					  ,CreatedDate    
          )    
        VALUES(    
					  @Address1
					  ,@Address2
					  ,@City
					  ,@ZipCode
					  ,@AreaDetails  
					  ,@CreatedBy    
					  ,dbo.fn_GetCurrentDateTime()    
          )    
              
     
  --END    
		SELECT @@ROWCOUNT AS RowsEffected    
		FOR XML PATH ('AreaBe'),TYPE    
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetArea]    Script Date: 06/03/2015 10:59:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jun-2015>    
-- Description: <Retriving Area records form MASTERS.Tbl_MAreaDetails>  
-- =============================================    
CREATE PROCEDURE [dbo].[USP_GetArea]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
  DECLARE	@PageNo INT    
			,@PageSize INT    
        
  SELECT       
		   @PageNo = C.value('(PageNo)[1]','INT')    
		   ,@PageSize = C.value('(PageSize)[1]','INT')     
  FROM @XmlDoc.nodes('AreaBe') AS T(C) 
     
  ;WITH PagedResults AS    
  (    
   SELECT    
		 ROW_NUMBER() OVER(ORDER BY IsActive ASC , CreatedDate DESC ) AS RowNumber  
		 ,AreaCode    
		 ,Address1   
		 ,Address2  
		 ,City
		 ,ZipCode  
		 ,IsActive 
   FROM MASTERS.Tbl_MAreaDetails
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('AreaBe'),TYPE    
  )    
  FOR XML PATH(''),ROOT('AreaInfoByXml')     
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCountriesList]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Bhimaraju Vanka>
-- Create date: <12-FEB-2014>
-- Description:	<Insert Coutries From AddCountries to Tbl_Countries>
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCountriesList] 	
AS
BEGIN
	
	SELECT(
	    SELECT CountryCode,CountryName 
		FROM Tbl_Countries 
		
    WHERE IsActive=1
    
    FOR XML PATH('MastersBE'),TYPE
	)
	FOR XML PATH(''),ROOT('MastersBEInfoByXml')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogs]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CNL.CreatedDate DESC) AS RowNumber
			   ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
			   --,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName  
			   --,CNL.NewFirstName  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   ,CNL.OldKnownAs  AS OldSurName 
			   ,CNL.NewKnownAs  AS NewSurName 
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CNL.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
			   ,CustomerView.ClassName
			   ,CNL.CreatedDate
			   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId   
		 --ORDER BY CNL.CreatedDate DESC
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END 
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersLog]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersLog]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')
	   ,@PageSize=C.value('(PageSize)[1]','INT')
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY AM.CreatedDate DESC ) AS RowNumber				
				,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,(CAST(AM.InitialReading AS INT )) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks
			   ,CONVERT(VARCHAR(50),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS LastTransactionDate  
			   ,AM.CreatedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo
			   ,AM.CreatedDate
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
			   ,CustomerView.BookSortOrder  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			   ,COUNT(0) OVER() AS TotalChanges
		 FROM Tbl_AssignedMeterLogs  AM
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 
		 )
		 
		 SELECT * FROM PageResult
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountwiseMeterReadings]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 21 May 2015
-- Description: <Get BillReading details from customerreading table>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAccountwiseMeterReadings] 
(
	@XmlDoc xml
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists bit = 0
		Declare	  @PrvExists bit = 0
		Declare	  @IsInProcess bit = 0
		Declare	  @IsNoReadings bit = 0
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0
		Declare @IsMeterApproval bit=0

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAcountNumber AND ApproveStatusId = 1)
			BEGIN
				SET @IsMeterApproval = 1
			END
		
		DECLARE @MaxCustomerReadinglogId INT,@LogIsLocked BIT,@LogApprovalStatusId INT,@LogReadDate DATE
		
		SELECT TOP 1 @MaxCustomerReadinglogId = CustomerReadingLogId
			,@LogIsLocked = IsLocked
			,@LogApprovalStatusId = ApproveStatusId
			,@LogReadDate = ReadDate
		FROM Tbl_CustomerReadingApprovalLogs 
		WHERE GlobalAccountNumber = @GlobalAcountNumber
		ORDER BY CustomerReadingLogId DESC
		
		IF(@LogIsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate))
			BEGIN
				SET @IsExists = 1 
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingLogs
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingLogs where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingLogs  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingLogs
					END
			END
		ELSE IF(@LogApprovalStatusId = 1)
			BEGIN
				SET @IsInProcess = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingApprove
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND ApproveStatusId = 1
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingApprove where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingApprove  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingApprove
					END
			END
		ELSE 
			BEGIN
				SET @IsNoReadings = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,IsBilled
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingId
				INTO #CustomerTopTwoReadingMain
				from Tbl_CustomerReadings
				where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
				Order By CustomerReadingId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingMain where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingMain  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
							 @PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=IsBilled
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingMain
					END
			END
			
		--select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		--SELECT TOP(1) @Month = BillMonth,
		--@Year = BillYear ,@LastBillReadType= 
		--(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		--=CB.ReadCodeId)
		--,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		--WHERE AccountNo = @GlobalAcountNumber  
		--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		--SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		--CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		--DECLARE @IsLatestBill BIT = 0 	

		--IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		--BEGIN
		--	SET @IsLatestBill = 1
		--END 
		--ELSE
		--  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
			 @AcountNumber AS AccountNo,
			 @OldAcountNumber AS OldAccountNo,
			 @Name   AS Name,
			 @usage as Usage
			 ,@RouteName   as RouteNum
			 --,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
			 --,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS IsActiveMonth
			 ,@IsTamper as IsTamper
			 ,@IsExists	as IsExists 
			 ,@IsInProcess as IsApprovalProces
			 ,@IsNoReadings AS IsActive
			 ,@IsMeterApproval AS IsMeterChangeApproval
			 ,convert(varchar(11),@LatestDate,105) as LatestDate
			 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
			 ,@MeterNumber as MeterNo
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
			 ,@PrvExists as IsPrvExists  
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
			 --,@LastBillReadType as LastBillReadType
			 ,ISNULL(@InititalReading,0) as	 InititalReading
			 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
	)
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  


END  


GO

/****** Object:  StoredProcedure [dbo].[UPS_IsBillInSchedule]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <NEERAJ KANOJIYA>      
-- Create date: <07-MAY-2015>      
-- Description: <CHECK FOR NEW BILL SCHEDULE>        
-- =============================================      
      
ALTER PROCEDURE  [dbo].[UPS_IsBillInSchedule]        
AS        
BEGIN       
  DECLARE @IsExists BIT =0     
 IF((SELECT COUNT(BillGenarationStatusId) FROM Tbl_BillingQueueSchedule WHERE BillGenarationStatusId=5)>0)
 BEGIN
	SET @IsExists=1	 
 END 
    SELECT @IsExists AS IsExists
	FOR XML PATH('BillGenerationList'),ROOT('BillGenerationInfoByXml')         
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteArea]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz - ID103>      
-- Create date: <02-Jun-2015>      
-- Description: <Delete Area details Row from MASTERS.Tbl_MAreaDetails> 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_DeleteArea]
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
	DECLARE @AreaCode INT  
			,@IsActive BIT  
			,@ModifiedBy VARCHAR(50)  

	SELECT @AreaCode = C.value('(AreaCode)[1]','INT')  
			,@IsActive = C.value('(IsActive)[1]','BIT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('AreaBe') AS T(C)  

	IF NOT EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE AreaCode=@AreaCode)
		BEGIN
			UPDATE MASTERS.Tbl_MAreaDetails   
				SET IsActive = @IsActive,  
					ModifedBy = @ModifiedBy,  
					ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE AreaCode = @AreaCode  

			SELECT @@ROWCOUNT As RowsEffected      
			FOR XML PATH('AreaBe'),TYPE     
		END
	ELSE
		BEGIN
			SELECT 
				0 AS RowsEffected
				,1 AS IsExists			
			FOR XML PATH('AreaBe'),TYPE
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateArea]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz - ID103>        
-- Create date: <02-Jun-2015>        
-- Description: <Update Area in MASTERS.Tbl_MAreaDetails>        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_UpdateArea]       
(        
@XmlDoc XML=null        
)        
AS        
BEGIN         
 
 DECLARE   @AreaCode INT
		   ,@Address1 VARCHAR(100)    
		   ,@Address2 VARCHAR(100) 
		   ,@City VARCHAR(50)   
		   ,@ZipCode VARCHAR(10)
		   ,@AreaDetails VARCHAR(100)
		   ,@ModifiedBy VARCHAR(50)
		           
            
		   
 SELECT    @AreaCode=C.value('(AreaCode)[1]','INT')         
		   ,@Address1=C.value('(Address1)[1]','VARCHAR(100)')    
		   ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')    
		   ,@City=C.value('(City)[1]','VARCHAR(50)')    
		   ,@ZipCode=C.value('(ZipCode)[1]','VARCHAR(10)')    
		   ,@AreaDetails=C.value('(AreaDetails)[1]','VARCHAR(100)') 
		   ,@ModifiedBy = C.value('(ModifedBy)[1]','VARCHAR(50)')        
 FROM @XmlDoc.nodes('AreaBe') as T(C)               
 --IF NOT EXISTS(SELECT 0 FROM MASTERS.Tbl_MAreaDetails WHERE Name=@Name AND AreaCode!=@AreaCode )        
 --BEGIN         
   UPDATE MASTERS.Tbl_MAreaDetails SET 
						Address1 = @Address1     
						,Address2=@Address2
						,City=@City
						,ZipCode=@ZipCode
						,AreaDetails = @AreaDetails         
						,ModifedBy = @ModifiedBy        
						,ModifiedDate= dbo.fn_GetCurrentDateTime()          
   WHERE AreaCode = @AreaCode       
           
 --END        
			SELECT 1 As RowsEffected        
			FOR XML PATH('AreaBe'),TYPE        
          
END        
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertArea]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jun-2015>    
-- Description: <For Insert Area Data Into MASTERS.Tbl_MAreaDetails>    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_InsertArea]  
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @Address1 VARCHAR(100)    
		   ,@Address2 VARCHAR(100) 
		   ,@City VARCHAR(50)   
		   ,@ZipCode VARCHAR(10)
		   ,@AreaDetails VARCHAR(100)
		   ,@CreatedBy VARCHAR(50)    
       
 SELECT   @Address1=C.value('(Address1)[1]','VARCHAR(100)')    
		   ,@Address2=C.value('(Address2)[1]','VARCHAR(100)')    
		   ,@City=C.value('(City)[1]','VARCHAR(50)')    
		   ,@ZipCode=C.value('(ZipCode)[1]','VARCHAR(10)')    
		   ,@AreaDetails=C.value('(AreaDetails)[1]','VARCHAR(100)')    
		   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')    
       
      
   FROM @XmlDoc.nodes('AreaBe') AS T(C)     
     
 --IF NOT EXISTs(SELECT 0 FROM MASTERS.Tbl_MAreaDetails WHERE Name=@Name AND AreaCode = @AreaCode)    
 --BEGIN    
  INSERT INTO MASTERS.Tbl_MAreaDetails(    
					  Address1  
					  ,Address2 
					  ,City
					  ,ZipCode
					  ,AreaDetails   
					  ,CreatedBy    
					  ,CreatedDate    
          )    
        VALUES(    
					  @Address1
					  ,@Address2
					  ,@City
					  ,@ZipCode
					  ,@AreaDetails  
					  ,@CreatedBy    
					  ,dbo.fn_GetCurrentDateTime()    
          )    
              
     
  --END    
		SELECT @@ROWCOUNT AS RowsEffected    
		FOR XML PATH ('AreaBe'),TYPE    
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetArea]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jun-2015>    
-- Description: <Retriving Area records form MASTERS.Tbl_MAreaDetails>  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetArea]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
  DECLARE	@PageNo INT    
			,@PageSize INT    
        
  SELECT       
		   @PageNo = C.value('(PageNo)[1]','INT')    
		   ,@PageSize = C.value('(PageSize)[1]','INT')     
  FROM @XmlDoc.nodes('AreaBe') AS T(C) 
     
  ;WITH PagedResults AS    
  (    
   SELECT    
		 ROW_NUMBER() OVER(ORDER BY IsActive ASC , CreatedDate DESC ) AS RowNumber  
		 ,AreaCode    
		 ,Address1   
		 ,Address2  
		 ,City
		 ,ZipCode  
		 ,IsActive 
   FROM MASTERS.Tbl_MAreaDetails
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('AreaBe'),TYPE    
  )    
  FOR XML PATH(''),ROOT('AreaInfoByXml')     
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading BIGINT
		,@NewMeterReading BIGINT
		,@PreviousReading BIGINT
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading BIGINT
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = CAST(OldMeterReading AS NUMERIC)
		,@NewMeterReading = CAST(NewMeterReading AS NUMERIC)
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = CAST(NewMeterInitialReading AS NUMERIC)
		,@NewMeterReadingDate = NewMeterReadingDate
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_New(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId ANd IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
					--						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

				DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage > 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,@PreviousReading
										,@OldMeterReading
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,@NewMeterInitialReading
										,@NewMeterReading
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked = 1
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage > 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,@PreviousReading
								,@OldMeterReading
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,@NewMeterInitialReading
								,@NewMeterReading
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
							--									WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentEditListReport]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT 
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, (CD.GlobalAccountNumber+' - '+CD.AccountNo) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, TotalAmountEffected
		, (CASE WHEN TotalAmountEffected < 0 THEN ' DR' ELSE ' Cr' END) AS Format
		, MR.Name AS AdjustmentName
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@AdjustmentTypes,',')) TU ON TU.TypeId = CR.BillAdjustmentType 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			--,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,(REPLACE(CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1),'-','')+Format) AS Amount
			--,(SELECT SUM(TotalAmountEffected) FROM #CustomerReadingsList) AS TotalAmount
			,AdjustmentName
			,TotalRecords
			,BusinessUnitName 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustInfoForAdjustmentNoBill]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 8-APRIL-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 ALTER PROCEDURE [dbo].[USP_GetCustInfoForAdjustmentNoBill]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)             
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name                   
      ,A.GlobalAccountNumber AS AccountNo
      ,(A.GlobalAccountNumber +' - '+ A.AccountNo) AS GlobalAccNoAndAccNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,A.OutStandingAmount
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                      
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- Modified By : Bhimaraju V
-- Modified Date:06-05-2015    
-- Description: Updating the Direct Cust Avg reading in Main Table
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
	
	-- To get the Payments data to validate
	SELECT ISNULL(Temp.AverageReading,0) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,ISNULL(CD.GlobalAccountNumber,'') AS AccNum
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
		--							AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--	when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
		--	else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CPD.ReadCodeID
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions K
				WHERE AccountNo = (ISNULL(CD.GlobalAccountNumber,0))
				AND K.AvgUploadFileId = Temp.AvgUploadFileId GROUP BY AccountNo) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions(NOLOCK) AS Temp
	INNER JOIN Tbl_AverageUploadFiles(NOLOCK) PUF ON PUF.AvgUploadFileId = Temp.AvgUploadFileId  
	INNER JOIN Tbl_AverageUploadBatches(NOLOCK) PUB ON PUB.AvgUploadBatchId = PUF.AvgUploadBatchId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (Temp.AccountNo = CD.OldAccountNo) --CD.GlobalAccountNumber = Temp.AccountNo OR 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions(NOLOCK)) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the paid amount is valid or not
			IF((SELECT COUNT(0) FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 0) > 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE ISNUMERIC(AverageReading) = 0
					
					INSERT INTO Tbl_AverageFailureTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,Comments
					)
					SELECT 
						 AvgUploadFileId
						,SNO
						,TempAccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,STUFF(Comments,1,2,'')
					FROM #AverageReadings
					WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
										
					SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
					
					-- TO delete the Failure Transactions from Reading Transaction table	
					DELETE FROM Tbl_AverageTransactions	
					WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
												WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0)
				 
					-- TO delete the Failure Transactions from temp table
					DELETE FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
						
				END				
			-- TO check whether the given AverageReading is valid or not
			
			IF((SELECT COUNT(0) FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 1) > 0)
				BEGIN								
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #AverageReadings
			WHERE IsValid = 0
								
			SELECT @FailureTransactions = ISNULL(@FailureTransactions,0) + COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
				,ReadCodeID
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber
			
			--WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
	    
		    UPDATE CAD   
			SET CAD.AvgReading = Tc.AverageReading
			FROM CUSTOMERS.Tbl_CustomerActiveDetails CAD
			INNER JOIN #ValidReadings TC ON TC.AccNum = CAD.GlobalAccountNumber
			AND TC.ReadCodeID=1-- For Direct CustomersOnly updating the avg reading
		     
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithDebitBal]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 09-05-2015
-- Description:	Get The Details of Debit Balence Of The Customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithDebitBal]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)			
			,@BookNo VARCHAR(MAX)
			,@MinAmt VARCHAR(50)
			,@MaxAmt VARCHAR(50)
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@MinAmt=C.value('(MinAmt)[1]','VARCHAR(50)')
			,@MaxAmt=C.value('(MaxAmt)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('RptDebitBe') AS T(C)
	
	
	IF(@MinAmt = '')
		SET @MinAmt = (SELECT MIN(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK))
	IF(@MaxAmt = '')
		SET @MaxAmt = (SELECT MAX(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK))
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits(NOLOCK) 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits(NOLOCK) 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
			
	;WITH PagedResults AS
	(
	SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
				--,A.AccountNo AS AccountNo
				,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
				,OutStandingAmount AS TotalDueAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)),'--') AS LastPaidDate
				,CD.OldAccountNo
				,C.CycleName
				,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
				,PD.SortOrder AS CustomerSortOrder
				,BN.SortOrder AS BookSortOrder
				,BU.BusinessUnitName AS BusinessUnitName
				,SU.ServiceUnitName AS ServiceUnitName
				,SC.ServiceCenterName AS ServiceCenterName
				,COUNT(0) OVER() AS TotalRecords
		 FROM CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD 
		 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber AND CAD.OutStandingAmount >= 0
				AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		 INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo  
		 INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId     
		 INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
		 INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID
		 INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID 
		 INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		--SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
		--		--,A.AccountNo AS AccountNo
		--		,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
		--		,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
		--		,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
		--											,CD.Service_Landmark
		--											,CD.Service_City,'',
		--											CD.Service_ZipCode) AS ServiceAddress
		--		,OutStandingAmount AS TotalDueAmount
		--		,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount
		--		,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(A.AccountNo)),'--') AS LastPaidDate
		--		,CD.OldAccountNo
		--		,C.CycleName
		--		,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
		--		,PD.SortOrder AS CustomerSortOrder
		--		,BN.SortOrder AS BookSortOrder
		--		,BU.BusinessUnitName AS BusinessUnitName
		--		,SU.ServiceUnitName AS ServiceUnitName
		--		,SC.ServiceCenterName AS ServiceCenterName
		--		,COUNT(0) OVER() AS TotalRecords
		-- FROM Tbl_CustomerBills(NOLOCK) A
		-- INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber = A.AccountNo
		-- INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		-- INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber AND CAD.OutStandingAmount >= 0
		--		AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		-- INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo  
		-- INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId     
		-- INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
		-- INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID
		-- INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID 
		-- INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID
		-- INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
		-- INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		-- INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		-- INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		-- INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		 
	)
	

		SELECT	 
			 RowNumber
			--,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,CONVERT(VARCHAR,CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
			,CONVERT(VARCHAR,CAST(LastPaidAmount AS MONEY),-1) AS LastPaidAmount
			,LastPaidDate
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(TotalDueAmount,0)) FROM PagedResults) AS MONEY),-1) AS DueAmount
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerForCustTypeChange]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Faiz-ID103 
-- Create date: 09-Mar-2015     
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For Customer Type Change 
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetCustomerForCustTypeChange]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(50)
    
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)      
  


 IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ActiveStatusId IN(1,2))    
  BEGIN    
   SELECT  
       CD.GlobalAccountNumber AS GlobalAccountNumber
      ,(CD.GlobalAccountNumber + ' - '+CD.AccountNo)AS GlobalAccNoAndAccNo
      ,CD.AccountNo As AccountNo  
      ,CD.FirstName  
      ,CD.MiddleName  
      ,CD.LastName  
      ,CD.Title        
      ,CD.KnownAs  
      ,CD.CustomerTypeId
      ,CT.CustomerType
      ,ISNULL(MeterNumber,'--') AS MeterNo  
      ,CD.ClassName AS Tariff  
      ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
      ,CD.OutStandingAmount  --Faiz-ID103
      FROM UDV_CustomerDescription  CD  
      join Tbl_MCustomerTypes CT
      on CD.CustomerTypeId=CT.CustomerTypeId
      WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)  
      AND CD.ActiveStatusId IN (1,2)    
      FOR XML PATH('ChangeCustomerTypeBE')      
  END    
 ELSE     
  BEGIN    
   SELECT 1 AS IsAccountNoNotExists   
   FOR XML PATH('ChangeCustomerTypeBE')    
  END  
END  

-----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- Modified By:  V.Bhimaraju  
-- Modified date: 07-May-2015   
-- Description: Added outstanding amount in CustomerStatus Change (Flag=2)
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 ,CAD.OutStandingAmount
				  FROM UDV_CustomerMeterInformation CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,CAD.OutStandingAmount --Faiz-ID103
					 ,dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber) AS LastTransactionDate --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate		
				 ,CAD.OutStandingAmount --Faiz-ID103		 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
     ,CD.OutStandingAmount --Faiz-ID103
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsList]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 11-08-2014   
-- Modified date: 28-10-2014  
-- Modified By: T.Karthik  
-- Description: The purpose of this procedure is to get customer  Details By AccountNo,MeterNo  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsList]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
  ,@OldAccountNo VARCHAR(50)  
  ,@MeterNo VARCHAR(50)  
  ,@BUID VARCHAR(50)=''  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')    
  ,@OldAccountNo=C.value('(OldAccountNo)[1]','VARCHAR(50)')    
  ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')    
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)    
     
  IF EXISTS(SELECT 0 FROM UDV_IsCustomerExists WHERE (BU_ID=@BUID OR @BUID='')  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
 BEGIN  
   SELECT    
      CD.GlobalAccountNumber AS AccountNo
     ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
     ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name   
     ,(SELECT dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber)) AS ServiceAddress  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber)),'--') AS LastBillDate  
     ,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)),'--') As LastPaidDate  
     ,CAD.OutStandingAmount AS DueAmount  
     ,CPD.TariffClassID AS ClassID  
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=CPD.TariffClassID) AS Tariff  
     ,(SELECT CategoryName + ' - ' + CategoryDescription FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CPD.ClusterCategoryId) AS ClusterType
     ,(SELECT ClusterCategoryId FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = CPD.ClusterCategoryId) AS ClusterTypeId
     ,1 AS IsSuccess  
     ,CD.OldAccountNo --Faiz-ID103
   FROM CUSTOMERS.Tbl_CustomersDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
  INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber
  INNER JOIN UDV_BookNumberDetails BD ON CPD.BookNo=BD.BookNo
   WHERE (CD.GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='')    
   AND (BU_ID=@BUID OR @BUID='') 
   FOR XML PATH('TariffManagementBE')    
 END  
 ELSE IF EXISTS(SELECT 0 FROM UDV_IsCustomerExists WHERE BU_ID!=@BUID  
  AND (GlobalAccountNumber=@AccountNo OR @AccountNo='')    
   AND (OldAccountNo=@OldAccountNo OR @OldAccountNo='')    
   AND (MeterNumber=@MeterNo OR @MeterNo='') AND ActiveStatusId=1)  
  BEGIN  
   SELECT 1 AS IsCustExistsInOtherBU,1 AS IsSuccess FOR XML PATH('TariffManagementBE')   
  END  
 ELSE  
  SELECT 0 AS IsSuccess FOR XML PATH('TariffManagementBE')   
END   

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddress]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author:  Padmini        
-- Create date: 05/02/2015        
-- Description: This Procedure is used to Fetching details for Customer Address Change Log      
-- MODIFIED BY: NEERAJ    
-- DESCRIPTION: ADDED FILTERED ADDRESS BY ISACTIVE    
-- DATE    : 7-FEB-15     
-- MODIFIED BY: Bhimaraju    
-- DESCRIPTION: ADDED BookNo details    
-- DATE    : 7-FEB-15      
-- Modified By: Padmini    
-- Modified Date:17-Feb-2015    
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order    
-- Modified By: Faiz-ID103    
-- Modified Date: 13-Mar-2015    
-- Description: Searching with old account no also added   
-- Modified By: Faiz-ID103    
-- Modified Date: 13-May-2015    
-- Description: Outstanding amount added 
-- Modified By: Faiz-ID103    
-- Modified Date: 28-May-2015    
-- Description: Existing Book field added for book no page
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerAddress]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN        
        
 DECLARE @GlobalAccountNo VARCHAR(50)        
               
    SELECT @GlobalAccountNo=C.value('GolbalAccountNumber[1]','VARCHAR(50)')             
    FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)        
                
   --         --1.Log Writing        
   --INSERT INTO Tbl_CustomerAddressChangeLogs( AccountNo                    
   --              ,OldHouseNo                     
   --             ,OldStreetName        
   --             ,OldCity        
   --             ,OldAreaCode        
   --             ,OldZipCode        
   --             ,OldPostalAddressID        
   --             ,IsServiceAddress         
   --            )        
   -- --- Fetching Postal Address Details For Insertion                    
   --SELECT        
   -- GlobalAccountNumber        
   -- ,CASE HouseNo WHEN '' THEN NULL ELSE HouseNo END         
   -- ,CASE StreetName WHEN '' THEN NULL ELSE StreetName END          
   -- ,CASE City WHEN '' THEN NULL ELSE City END         
   -- ,CASE AreaCode WHEN '' THEN NULL ELSE AreaCode END         
   -- ,CASE ZipCode WHEN '' THEN NULL ELSE ZipCode END         
   -- ,CASE AddressID WHEN '' THEN NULL ELSE AddressID END        
   -- ,IsServiceAddress        
   --FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]        
   --WHERE GlobalAccountNumber=@GlobalAccountNo         
           
 SELECT(     -- 2.Fetching Customer Basic List To Display --        
   SELECT         
    CD.GlobalAccountNumber  AS GolbalAccountNumber      
    ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)AS ContactName        
    ,CD.HomeContactNo AS HomeContactNumber
    ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
    ,CD.BusinessContactNo AS BusinessContactNumber    
    ,CD.AccountNo AS AccountNo    
    ,CD.ClassName AS Tariff    
    ,CD.MeterNumber    
    ,CD.OldAccountNo    
    --,B.BookNo    
    --,BU.BU_ID    
    --,SU.SU_ID    
    --,SC.ServiceCenterId AS SC_ID    
    --,B.CycleId    
    --,BU.StateCode    
    ,CD.BookNo    
    ,CD.BU_ID    
    ,CD.SU_ID    
    ,CD.ServiceCenterId AS SC_ID    
    ,CD.CycleId
    ,CD.OutStandingAmount    
    ,CD.BookId + ' ('+BookCode+')' AS ExistingBook --Faiz-ID103
    --,CD.StateCode    
    ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode    
   FROM UDV_CustomerDescription CD    
 --  JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber    
 --  JOIN Tbl_BookNumbers B ON B.BookNo=CPD.BookNo    
 --  JOIN Tbl_Cycles C ON C.CycleId=B.CycleId    
 --JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId    
 --JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID    
 --JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID    
   WHERE CD.GlobalAccountNumber=@GlobalAccountNo    
   or OldAccountNo=@GlobalAccountNo -- Faiz-ID103
   FOR XML PATH('CustomerRegistrationList_1BE'),TYPE        
 ),        
 (         
    --3.Fetching Customer Address Details        
   SELECT        
    CPAD.GlobalAccountNumber  AS GolbalAccountNumber      
    , HouseNo   AS HouseNoPostal
    , StreetName     AS StreetPostal      
    , City   AS CityPostaL      
    , AreaCode   AS AreaPostal      
    , ZipCode   AS ZipCodePostal      
    , AddressID   AS PostalAddressID      
    ,IsServiceAddress  AS IsServiceAddress      
    ,IsCommunication      
   FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] as CPAD
   Join [CUSTOMERS].[Tbl_CustomerSDetail] as CSD on CSD.GlobalAccountNumber = CPAD.GlobalAccountNumber
   WHERE (CPAD.GlobalAccountNumber=@GlobalAccountNo or CSD.OldAccountNo=@GlobalAccountNo)AND IsActive=1   -- Faiz-ID103    
   ORDER BY IsServiceAddress ASC        
   FOR XML PATH('CustomerRegistrationList_2BE'),TYPE        
  )        
    FOR XML PATH(''),ROOT('CustomerRegistrationInfoByXml')          
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
      
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount
	END
END  
				 

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 

 
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd   .Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						IF	 isnull(@GetPaidMeterBalanceAfterBill,0) <>0
						BEGIN
						
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
 
						update Tbl_PaidMeterDetails
						SET OutStandingAmount =@PaidMeterDeductedAmount -isnull(OutStandingAmount,0)  
						WHERE AccountNo = @GlobalAccountNumber	 -- and ActiveStatusId=1
					 	
						END
						 
				
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1,
						BillNo=@CusotmerNewBillID
						where GlobalAccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForContactInfoByAccno]    Script Date: 06/03/2015 10:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author: Karteek
-- Create date: 09-04-2015 
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For Contact info 
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForContactInfoByAccno]    
(    
	@XmlDoc xml    
)    
AS    
BEGIN
    
	DECLARE @AccountNo VARCHAR(50)

	SELECT  @AccountNo = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')		
	FROM @XmlDoc.nodes('ChangeContactBE') as T(C)    

	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE (GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo) AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
				 CD.GlobalAccountNumber AS GlobalAccountNumber
				,CD.AccountNo As AccountNo
				,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccountNo
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
						,CD.Service_Landmark    
						,CD.Service_City,'',    
						CD.Service_ZipCode) AS ServiceAddress
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				,ISNULL(CD.EmailId,'--') AS OldEmailId
				,ISNULL(CD.HomeContactNo,'--') AS OldHomeContactNumber
				,ISNULL(CD.BusinessContactNo,'--') AS OldBusinessContactNumber
				,ISNULL(CD.OtherContactNo,'--') AS OldOtherContactNumber
				,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				,CD.OutStandingAmount
			FROM UDV_CustomerDescription CD
			WHERE (CD.GlobalAccountNumber = @AccountNo  OR OldAccountNo = @AccountNo)
			AND ActiveStatusId IN (1,2)  
			FOR XML PATH('ChangeContactBE')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeContactBE')  
		END
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForAssignMeter]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 5-MARCHAR-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)             
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
      ,(A.GlobalAccountNumber +' - '+A.AccountNo ) AS GlobalAccountNumber
      ,A.GlobalAccountNumber AS AccountNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,A.OutStandingAmount AS OutStandingAmount --Faiz-ID103
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                      
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    AND A.ActiveStatusId=1                              
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END           

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateRole]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz Mohammed>      
-- Create date: <19-01-2015>      
-- Description: <For Updateing Role Data in Tbl_NewPagePermissions>      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_UpdateRole]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @RoleId INT  
   ,@Permissions VARCHAR(MAX)    
   ,@ModifiedBy VARCHAR(50)  
   ,@ModifiedDate DATETIME    
   ,@ExistedRoleId INT  
   ,@ExistedCount INT  
   ,@IsValid BIT    
   ,@StatusText VARCHAR(MAX)  
   ,@AccessLevelID INT
         
 SELECT  @RoleId=C.value('(RoleId)[1]','INT')     
  ,@Permissions=C.value('(Permissions)[1]','VARCHAR(MAX)')       
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')            
  ,@AccessLevelID=C.value('(AccessLevelID)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('AdminBE') AS T(C)     
  
--Start Validating if Role wtih the same permisssions already exists in the Tbl_NewPagePermissions  
--If Role Name is doesn't Exist  
    --Start Validating if Role wtih the same permisssions already exists in the Tbl_NewPagePermissions  
   DECLARE @TempPagePermissions TABLE(RoleId INT, PagePermissions VARCHAR(MAX))    
     
   DELETE @TempPagePermissions  
     
   INSERT INTO @TempPagePermissions(RoleId,PagePermissions)  
   SELECT DISTINCT P.Role_Id,  
   STUFF((SELECT  ',' + CONVERT(VARCHAR(MAX), P1.MenuId)  
     FROM  Tbl_NewPagePermissions P1  
     WHERE P.Role_Id = P1.Role_Id  ORDER BY P1.MenuId  
     FOR XML PATH(''), TYPE  
     ).value('.', 'NVARCHAR(MAX)')  
    ,1,1,'') MenuId   
   FROM Tbl_NewPagePermissions P;  
     
   --SELECT * FROM @TempPagePermissions  
     
   --Arranging the permission id in increasing order to verify  
   DECLARE @PermissionsSortder Table (PermissionId INT)  
   DELETE @PermissionsSortder  
   INSERT INTO @PermissionsSortder SELECT * FROM dbo.fn_Split (@Permissions,',')  
     
   SELECT @Permissions = STUFF((SELECT  ',' + CONVERT(VARCHAR(MAX), PermissionId)  
     FROM  @PermissionsSortder ORDER BY PermissionId FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')    
   --End of sorting permission id's  
  
   PRINT @Permissions  
  
   --Validating the permission id's exist for other roles  
   IF EXISTS( SELECT RoleId FROM @TempPagePermissions WHERE PagePermissions=@Permissions and RoleId!=@RoleId )  
   BEGIN  
    --If Same Role Permissones Already Exist  
    SELECT -1 AS RowsEffected  
    ,(SELECT RoleName FROM Tbl_MRoles   
     WHERE RoleId=(SELECT RoleId FROM @TempPagePermissions WHERE PagePermissions=@Permissions)) AS ExistingRoleName    
    FOR XML PATH ('AdminBE'),TYPE   
   END  
   ELSE  
   BEGIN  
        DELETE FROM Tbl_NewPagePermissions WHERE Role_Id = @RoleId --Faiz-ID103 02-Apr-2015  
      --GET DATE FROM UDF    
      SET @ModifiedDate = dbo.fn_GetCurrentDateTime();    
      
      
  --------------------------Updating the role table for AccessLevel----Faiz-ID103----------------
      
		  BEGIN TRY
		  Declare @ExistingAccessLevelID INT
		  Select @ExistingAccessLevelID = AccessLevelID FROM Tbl_MRoles where RoleId=@RoleId
		  IF(@ExistingAccessLevelID = @AccessLevelID)
		  BEGIN
			Update Tbl_MRoles 
			SET AccessLevelID=@AccessLevelID 
			where RoleId=@RoleId
		  END   
		  END TRY    
		  BEGIN CATCH    
			SET @IsValid=0;    
			SET @StatusText='Cannot update Roles AccessLevel list.';    
		  END CATCH    
		
  -------------------------End Updating the role table for AccessLevel----Faiz-ID103-----------
		
      BEGIN TRY    
       CREATE TABLE #PermissionList(    
        RoleId INT    
        ,MenuId INT    
        ,CreatedDate DATETIME    
        ,CreatedBy VARCHAR(50)    
        )    
       INSERT INTO #PermissionList(    
        RoleId    
        ,MenuId    
        ,CreatedDate    
        ,CreatedBy    
          )    
       SELECT @RoleId    
       ,MenuId    
       ,@ModifiedDate  AS CreatedDate    
       ,@ModifiedBy AS CreatedBy    
       from  [dbo].[fn_splitMenuId](@Permissions,',')    
      END TRY    
      BEGIN CATCH    
        SET @IsValid=0;    
        SET @StatusText='Cannot prepare MenuId list.';    
      END CATCH    
            
        IF EXISTS(SELECT 0 FROM #PermissionList)    
      BEGIN    
       INSERT INTO Tbl_NewPagePermissions(    
          Role_Id    
          ,MenuId    
          ,Created_Date    
          ,Created_By    
          )    
       SELECT * FROM #PermissionList    
          END   
           
        SELECT @@ROWCOUNT AS RowsEffected, 0 AS IsRolePermissionsExists      
        FOR XML PATH ('AdminBE'),TYPE      
            
        DROP TABLE #PermissionList    
   END  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertRole]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz Mohammed>        
-- Create date: <10-01-2015>        
-- Description: <For Insert Role Data Into Tbl_NewPagePermissions>        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_InsertRole]        
(        
 @XmlDoc xml        
)        
AS        
BEGIN        
 DECLARE @RoleId INT      
   ,@RoleName VARCHAR(50)        
   ,@CreatedBy VARCHAR(50)       
   ,@CreatedDate DATETIME      
   ,@Permissions VARCHAR(MAX)      
   ,@IsValid BIT      
   ,@StatusText VARCHAR(MAX)      
   ,@ExistedRoleId INT    
   ,@ExistedCount INT    
   ,@IsExists bit   
   ,@AccessLevelID INT 
           
 SELECT  @RoleName=C.value('(RoleName)[1]','VARCHAR(50)')        
  ,@Permissions=C.value('(Permissions)[1]','VARCHAR(MAX)')        
  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')            
  ,@AccessLevelID=C.value('(AccessLevelID)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('AdminBE') AS T(C)         
    
IF EXISTS (SELECT 0 FROM Tbl_MRoles WHERE RoleName = @RoleName)     
 BEGIN    
  --If Same Role Name Already Exist    
  SELECT 0 AS RowsEffected    
       ,(SELECT RoleName FROM Tbl_MRoles WHERE RoleName=@RoleName) AS ExistingRoleName      
     FOR XML PATH ('AdminBE'),TYPE     
 END    
 ELSE    
 BEGIN    
  --If Role Name is doesn't Exist    
    --Start Validating if Role wtih the same permisssions already exists in the Tbl_NewPagePermissions    
   DECLARE @TempPagePermissions TABLE(RoleId INT, PagePermissions VARCHAR(MAX))      
       
   DELETE @TempPagePermissions    
       
   INSERT INTO @TempPagePermissions(RoleId,PagePermissions)    
   SELECT DISTINCT P.Role_Id,    
   STUFF((SELECT  ',' + CONVERT(VARCHAR(MAX), P1.MenuId)    
     FROM  Tbl_NewPagePermissions P1    
     WHERE P.Role_Id = P1.Role_Id  ORDER BY P1.MenuId    
     FOR XML PATH(''), TYPE    
     ).value('.', 'NVARCHAR(MAX)')    
    ,1,1,'') MenuId     
   FROM Tbl_NewPagePermissions P;    
       
   --SELECT * FROM @TempPagePermissions    
       
   --Arranging the permission id in increasing order to verify    
   DECLARE @PermissionsSortder Table (PermissionId INT)    
   DELETE @PermissionsSortder    
   INSERT INTO @PermissionsSortder SELECT * FROM dbo.fn_Split (@Permissions,',')    
       
   SELECT @Permissions = STUFF((SELECT  ',' + CONVERT(VARCHAR(MAX), PermissionId)    
     FROM  @PermissionsSortder ORDER BY PermissionId FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')      
   --End of sorting permission id's    
    
   PRINT @Permissions    
    
   --Validating the permission id's exist for other roles    
   IF EXISTS( SELECT RoleId FROM @TempPagePermissions WHERE PagePermissions=@Permissions )    
   BEGIN    
    --If Same Role Permissones Already Exist    
    SELECT -1 AS RowsEffected    
    ,(SELECT RoleName FROM Tbl_MRoles     
     WHERE RoleId=(SELECT RoleId FROM @TempPagePermissions WHERE PagePermissions=@Permissions)) AS ExistingRoleName      
    FOR XML PATH ('AdminBE'),TYPE     
   END    
   ELSE    
   BEGIN    
        INSERT INTO Tbl_MRoles (RoleName,AccessLevelID)       
        VALUES (@RoleName,@AccessLevelID)      
              
      SET @RoleId = SCOPE_IDENTITY()      
      --GET DATE FROM UDF      
      SET @CreatedDate=dbo.fn_GetCurrentDateTime();      
    
      BEGIN TRY      
       CREATE TABLE #PermissionList(      
        RoleId INT      
        ,MenuId INT      
        ,CreatedDate DATETIME      
        ,CreatedBy VARCHAR(50)      
        )      
       INSERT INTO #PermissionList(      
        RoleId      
        ,MenuId      
        ,CreatedDate      
        ,CreatedBy      
          )      
       SELECT @RoleId      
       ,MenuId      
       ,@CreatedDate  AS CreatedDate      
       ,@CreatedBy AS CreatedBy      
       from  [dbo].[fn_splitMenuId](@Permissions,',')      
      END TRY      
      BEGIN CATCH      
        SET @IsValid=0;      
        SET @StatusText='Cannot prepare MenuId list.';      
      END CATCH      
              
        IF EXISTS(SELECT 0 FROM #PermissionList)      
      BEGIN      
       INSERT INTO Tbl_NewPagePermissions(      
          Role_Id      
         ,MenuId      
          ,Created_Date      
          ,Created_By      
          )      
       SELECT * FROM #PermissionList      
          END     
             
        SELECT @@ROWCOUNT AS RowsEffected, 0 AS IsRolePermissionsExists        
        FOR XML PATH ('AdminBE'),TYPE        
              
        DROP TABLE #PermissionList      
   END    
 END    
END        
GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate
					,ApprovedBy 
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					,@ModifiedBy
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill
							UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							WHERE CustomerBillId=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
   SET @NewConsumption=@Consumption-@AdjustedUnits;  
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * 5)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @NewConsumption END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @NewConsumption END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
									RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

					IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@ModifedBy
								 ,@ModifiedBy
								 ,GETDATE()  
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected 
							 ,CreatedBy
							 ,ApprovedBy
							 ,CreatedDate  
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected 
							 ,@ModifiedBy
							 ,@ModifiedBy
							 ,GETDATE() 
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
													RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = @AdjustmentLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_BillAdjustment]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <22-JULY-2014>
-- Description:	<INSERT BILL ADJUSTMENT DETAILS>
-- Modified By : Padmini
-- Modified Date : 26-12-2014
-- =============================================
ALTER PROCEDURE [dbo].[USP_Insert_BillAdjustment]
(
	@XmlDoc xml=null
	,@MultiXmlDoc XML
)
AS
BEGIN	
		DECLARE
			@BillAdjustmentId INT
			,@CustomerBillId INT
			,@AccountNo VARCHAR(50)
			,@CustomerId VARCHAR(50)
			,@AmountEffected DECIMAL(18,2)
			,@TaxEffected DECIMAL(18,2)
			,@TotalAmountEffected DECIMAL(18,2)
			,@AdjustedUnits INT
			,@BillAdjustmentType INT
			,@BatchNo INT
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@ApprovalStatusId INT 
			,@Details VARCHAR(MAX)  
			,@EnergyCharges DECIMAL(18,2)
			,@AdditionalCharges DECIMAL(18,2)
			,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
		SELECT
			@CustomerBillId = C.value('(CustomerBillId)[1]','INT')
			,@AccountNo	= C.value('(AccountNo)[1]','VARCHAR(50)')
			,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')
			,@TaxEffected = C.value('(TaxEffected)[1]','DECIMAL(18,2)')
			,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)')
			,@BillAdjustmentType	= C.value('(BillAdjustmentType)[1]','INT')
			,@BatchNo = C.value('(BatchNo)[1]','INT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
			,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
			,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)')
				,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C) 

		
 		IF((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				INSERT INTO Tbl_BillAdjustments(
					CustomerBillId
					,AccountNo
					,CustomerId
					,AmountEffected
					,BillAdjustmentType
					,TaxEffected
					,TotalAmountEffected
					,BatchNo
					,ApprovedBy
					,CreatedBy
					,CreatedDate
					) 						
				VALUES(					
					@CustomerBillId
					,@AccountNo
					,@CustomerId
					,@AmountEffected
					,@BillAdjustmentType
					,@TaxEffected
					,@TotalAmountEffected
					,@BatchNo
					,@ModifiedBy
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					)		
				SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
				INSERT INTO Tbl_BillAdjustmentDetails
										(
										BillAdjustmentId
										,AdditionalChargesID
										,AdditionalCharges
										,EnergryCharges
										,CreatedBy
										,CreatedDate
										)
				SELECT       
					@BillAdjustmentId 
					,C.value('(ChargeID)[1]','INT')       
					,C.value('(AmountEffected)[1]','DECIMAL(18,2)')      
					,@EnergyCharges
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
				 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(C)    	
				
				--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
				,ModifedBy=@ModifiedBy
				,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
				 
				--Updating the adjustment amount in Bill
				UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
				WHERE CustomerBillId=@CustomerBillId
				 
			END
			SELECT 
					(CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess
				FOR XML PATH('BillAdjustmentsBe'),TYPE
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
 	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_AdjustmentForNoBill]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 31-MARCH-2015    
--Description : Update Bill adjustment
--Modified By  : Bhimaraju 
--Created Date : 02-May-2015    
--Description : Logs And Approvals insertion
--===================================         
ALTER PROCEDURE [dbo].[USP_AdjustmentForNoBill]         
(        
@XmlDoc xml        
)        
AS        
BEGIN     
  DECLARE    
    @BillAdjustmentId INT   
   ,@AccountNo VARCHAR(50)    
   ,@CustomerId VARCHAR(50)    
   ,@AmountEffected DECIMAL(18,2)    
   ,@TotalAmountEffected DECIMAL(18,2)    
   ,@BillAdjustmentType INT   
   ,@ApprovalStatusId INT
   ,@FunctionId INT
   ,@CreatedBy VARCHAR(50)
   ,@IsFinalApproval BIT
   ,@BU_ID VARCHAR(50)
  ,@CurrentApprovalLevel INT =0 
       
  SELECT     
	@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
   ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
   ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')   
   ,@FunctionId = C.value('(FunctionId)[1]','INT')   
   ,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')    
   ,@TotalAmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')       
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       

	IF((SELECT COUNT(0)FROM Tbl_CustomerTypeChangeLogs 
				WHERE GlobalAccountNumber = @AccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillAdjustmentsBe')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AdjustmentRequestId INT
					,@Forward INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
						SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
				INSERT INTO Tbl_AdjustmentsLogs
							(		 GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId
									,CreatedBy 
									,CreatedDate
									,CurrentApprovalLevel
									,IsLocked
							)
				SELECT   @AccountNo
						,AccountNo
						,NULL--BillNumber
						,@BillAdjustmentType
						,MeterNumber
						,InitialReading
						,NULL--CurrentReadingAfterAdjustment
						,NULL--CurrentReadingBeforeAdjustment
						,NULL--AdjustedUnits
						,NULL--TaxEffected
						,@TotalAmountEffected
						,NULL-- EnergryCharges 
						,NULL--	AdditionalCharges
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
						,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM UDV_CustomerDescription
				WHERE GlobalAccountNumber=@AccountNo
				
				SET @AdjustmentRequestId = SCOPE_IDENTITY()
					
				IF(@IsFinalApproval = 1)
				BEGIN
			  						
				   INSERT INTO Tbl_BillAdjustments(
					 AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected
					 ,ApprovedBy
					 ,CreatedBy
					 ,CreatedDate
					 )           
					VALUES(         
					  @AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@TotalAmountEffected
					 ,@CreatedBy
					 ,@CreatedBy
					 ,GetDate()
					 )      
				   SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
   
				   INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
					  SELECT @BillAdjustmentId,@AmountEffected
					
					--OutStanding Amount is Updating Start
			
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=ISNULL(OutStandingAmount,0) - @TotalAmountEffected
					WHERE GlobalAccountNumber=@AccountNo
					--OutStanding Amount is Updating End
										  
				   INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
				   SELECT		     AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
				   FROM Tbl_AdjustmentsLogs
				   WHERE AdjustmentLogId=@AdjustmentRequestId
					
				END
			   --SELECT (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
			     SELECT 1 As IsSuccess    
			  FOR XML PATH('BillAdjustmentsBe'),TYPE    
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAddressChangeApproval]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAddressChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@AddressChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @AddressChangeLogId = C.value('(AddressChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@TempAddressId INT,
		@TempServiceId INT
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNumber 
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		FROM Tbl_CustomerAddressChangeLog_New
		WHERE AddressChangeLogId=@AddressChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId)
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
					--						WHERE AddressChangeLogId = @AddressChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					DECLARE @FinalApproval BIT

	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
			SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
					
					
					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel= CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerAddressChangeLog_New
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel = 0
						,IsLocked = 1
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					AND  AddressChangeLogId= @AddressChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber AND  AddressChangeLogId= @AddressChangeLogId)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedDate
									,CreatedBy)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
							--									WHERE AddressChangeLogId = @AddressChangeLogId))
									SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
						WHERE AddressChangeLogId = @AddressChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerAddressChangeLog_New
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked = 1
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			AND AddressChangeLogId = @AddressChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomersAddress_New]    Script Date: 06/03/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 1/APRIL/2015      
-- Description: This Procedure is used to change customer address    
-- Modified BY: Faiz-ID103
-- Modified Date: 07-Apr-2015
-- Description: changed the street field value from 200 to 255 characters
-- Modified BY: Bhimaraju V
-- Modified Date: 17-Apr-2015
-- Description: Implemented Logs for Audit Tray and approvals
-- =============================================    
ALTER PROCEDURE [dbo].[USP_ChangeCustomersAddress_New]      
(      
 @XmlDoc xml         
)      
AS      
BEGIN    
 DECLARE @TotalAddress INT
     ,@IsSameAsServie BIT  
     ,@GlobalAccountNo   VARCHAR(50)
     ,@NewPostalLandMark  VARCHAR(200)
     ,@NewPostalStreet   VARCHAR(255)
     ,@NewPostalCity   VARCHAR(200)      
     ,@NewPostalHouseNo   VARCHAR(200)      
     ,@NewPostalZipCode   VARCHAR(50)      
     ,@NewServiceLandMark  VARCHAR(200)      
     ,@NewServiceStreet   VARCHAR(255)      
     ,@NewServiceCity   VARCHAR(200)      
     ,@NewServiceHouseNo  VARCHAR(200)      
     ,@NewServiceZipCode  VARCHAR(50)      
     ,@NewPostalAreaCode  VARCHAR(50)      
     ,@NewServiceAreaCode  VARCHAR(50)      
     ,@NewPostalAddressID  INT      
     ,@NewServiceAddressID  INT     
     ,@ModifiedBy    VARCHAR(50)      
     ,@Details     VARCHAR(MAX)      
     ,@RowsEffected    INT      
     ,@AddressID INT     
     ,@StatusText VARCHAR(50)  
     ,@IsCommunicationPostal BIT  
     ,@IsCommunicationService BIT  
     ,@ApprovalStatusId INT=2    
     ,@PostalAddressID INT  
     ,@ServiceAddressID INT
     ,@IsFinalApproval BIT
	 ,@FunctionId INT
	,@BU_ID VARCHAR(50)  
	,@CurrentApprovalLevel INT
		
 SELECT @GlobalAccountNo=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    --,@NewPostalLandMark=C.value('()[1]','VARCHAR(200)')          
     ,@NewPostalStreet=C.value('(StreetPostal)[1]','VARCHAR(255)')          
     ,@NewPostalCity=C.value('(CityPostaL)[1]','VARCHAR(200)')          
     ,@NewPostalHouseNo=C.value('(HouseNoPostal)[1]','VARCHAR(200)')         
     ,@NewPostalZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')        
     --,@NewServiceLandMark=C.value('(ServiceLandMark)[1]','VARCHAR(200)')          
     ,@NewServiceStreet=C.value('(StreetService)[1]','VARCHAR(255)')          
     ,@NewServiceCity=C.value('(CityService)[1]','VARCHAR(200)')          
     ,@NewServiceHouseNo=C.value('(HouseNoService)[1]','VARCHAR(200)')         
     ,@NewServiceZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')      
     ,@NewPostalAreaCode=C.value('(AreaPostal)[1]','VARCHAR(50)')      
     ,@NewServiceAreaCode=C.value('(AreaService)[1]','VARCHAR(50)')      
     ,@NewPostalAddressID=C.value('PostalAddressID[1]','INT')      
     ,@NewServiceAddressID=C.value('ServiceAddressID[1]','INT')      
     ,@ModifiedBy=C.value('(ModifedBy)[1]','VARCHAR(50)')      
     ,@IsSameAsServie=C.value('(IsSameAsService)[1]','BIT')      
     ,@Details=C.value('(Reason)[1]','VARCHAR(MAX)')  
     ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')      
     ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
     ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
     ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	 ,@FunctionId = C.value('(FunctionId)[1]','INT')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)


IF((SELECT COUNT(0)FROM Tbl_CustomerAddressChangeLog_New 
				WHERE GlobalAccountNumber = @GlobalAccountNo AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
ELSE
		BEGIN
			DECLARE @RoleId INT
							,@CurrentLevel INT
							,@PresentRoleId INT
							,@NextRoleId INT
							,@TypeChangeRequestId INT
					
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			--IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId)
			--	BEGIN
			--		SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
			--	END
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
							
				
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			IF (@IsSameAsServie =0)
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New(  GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,CurrentApprovalLevel
																,IsLocked
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewServiceHouseNo
																,@NewServiceStreet
																,@NewServiceCity
																,NULL
																,@NewServiceAreaCode
																,@NewServiceZipCode
																,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			ELSE
				BEGIN
					INSERT INTO Tbl_CustomerAddressChangeLog_New( GlobalAccountNumber
																,OldPostal_HouseNo
																,OldPostal_StreetName
																,OldPostal_City
																,OldPostal_Landmark
																,OldPostal_AreaCode
																,OldPostal_ZipCode
																,OldService_HouseNo
																,OldService_StreetName
																,OldService_City
																,OldService_Landmark
																,OldService_AreaCode
																,OldService_ZipCode
																,NewPostal_HouseNo
																,NewPostal_StreetName
																,NewPostal_City
																,NewPostal_Landmark
																,NewPostal_AreaCode
																,NewPostal_ZipCode
																,NewService_HouseNo
																,NewService_StreetName
																,NewService_City
																,NewService_Landmark
																,NewService_AreaCode
																,NewService_ZipCode
																,ApprovalStatusId
																,Remarks
																,CreatedBy
																,CreatedDate
																,PresentApprovalRole
																,NextApprovalRole
																,OldPostalAddressID
																,NewPostalAddressID
																,OldServiceAddressID
																,NewServiceAddressID
																,OldIsSameAsService
																,NewIsSameAsService
																,IsPostalCommunication
																,IsServiceComunication
																,CurrentApprovalLevel
																,IsLocked
															)
														SELECT   @GlobalAccountNo
																,Postal_HouseNo
																,Postal_StreetName
																,Postal_City
																,Postal_Landmark
																,Postal_AreaCode
																,Postal_ZipCode
																,Service_HouseNo
																,Service_StreetName
																,Service_City
																,Service_Landmark
																,Service_AreaCode
																,Service_ZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,@NewPostalHouseNo
																,@NewPostalStreet
																,@NewPostalCity
																,NULL
																,@NewPostalAreaCode
																,@NewPostalZipCode
																,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
																,@Details
																,@ModifiedBy
																,dbo.fn_GetCurrentDateTime()
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
																,PostalAddressID
																,@NewPostalAddressID
																,ServiceAddressID
																,@NewServiceAddressID
																,IsSameAsService
																,@IsSameAsServie
																,@IsCommunicationPostal
																,@IsCommunicationService
																,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
																,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
														FROM CUSTOMERS.Tbl_CustomerSDetail
														WHERE GlobalAccountNumber=@GlobalAccountNo
					SET @TypeChangeRequestId = SCOPE_IDENTITY()							
				END
			
			IF(@IsFinalApproval = 1)
				BEGIN
					BEGIN TRY  
						  BEGIN TRAN   
							--UPDATE POSTAL ADDRESS  
						 SET @StatusText='Total address count.'     
						 SET @TotalAddress=(SELECT COUNT(0)   
							  FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails   
							  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsActive=1  
								)
						 --=====================================================================================          
						 --Customer has only one addres and wants to update the address. //CONDITION 1//  
						 --=====================================================================================   
						 IF(@TotalAddress =1 AND @IsSameAsServie = 1)  
							BEGIN  
						  SET @StatusText='CONDITION 1'
						 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END    
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END      
						 --=====================================================================================  
						 --Customer has one addres and wants to add new address. //CONDITION 2//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =1 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 2'
																		 
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails(  
										HouseNo  
										,LandMark  
										,StreetName  
										,City  
										,AreaCode  
										,ZipCode  
										,ModifedBy  
										,ModifiedDate  
										,IsCommunication  
										,IsServiceAddress  
										,IsActive  
										,GlobalAccountNumber  
										)  
									   VALUES (@NewServiceHouseNo         
										,@NewServiceLandMark         
										,@NewServiceStreet          
										,@NewServiceCity        
										,@NewServiceAreaCode        
										,@NewServiceZipCode          
										,@ModifiedBy        
										,dbo.fn_GetCurrentDateTime()    
										,@IsCommunicationService           
										,1  
										,1  
										,@GlobalAccountNo  
										)   
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						 
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo                 
						 END     
						   
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to in-active service. //CONDITION 3//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 1)   
							BEGIN  
						  SET @StatusText='CONDITION 3'  
						     
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							IsActive=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewPostalLandMark  
							,Service_StreetName=@NewPostalStreet  
							,Service_City=@NewPostalCity  
							,Service_HouseNo=@NewPostalHouseNo  
							,Service_ZipCode=@NewPostalZipCode  
							,Service_AreaCode=@NewPostalAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@PostalAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=1  
						  WHERE GlobalAccountNumber=@GlobalAccountNo  
						 END    
						 --=====================================================================================  
						 --Customer alrady has tow address and wants to update both address. //CONDITION 4//  
						 --=====================================================================================  
						 ELSE IF(@TotalAddress =2 AND @IsSameAsServie = 0)  
							BEGIN  
						  SET @StatusText='CONDITION 4'
						       
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewPostalLandMark WHEN '' THEN NULL ELSE @NewPostalLandMark END        
							  ,StreetName=CASE @NewPostalStreet WHEN '' THEN NULL ELSE @NewPostalStreet END        
							  ,City=CASE @NewPostalCity WHEN '' THEN NULL ELSE @NewPostalCity END        
							  ,HouseNo=CASE @NewPostalHouseNo WHEN '' THEN NULL ELSE @NewPostalHouseNo END        
							  ,ZipCode=CASE @NewPostalZipCode WHEN '' THEN NULL ELSE @NewPostalZipCode END          
							  ,AreaCode=CASE @NewPostalAreaCode WHEN '' THEN NULL ELSE @NewPostalAreaCode END        
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationPostal       
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1   
						    
						  UPDATE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SET        
							LandMark=CASE @NewServiceLandMark WHEN '' THEN NULL ELSE @NewServiceLandMark END        
							  ,StreetName=CASE @NewServiceStreet WHEN '' THEN NULL ELSE @NewServiceStreet END        
							  ,City=CASE @NewServiceCity WHEN '' THEN NULL ELSE @NewServiceCity END        
							  ,HouseNo=CASE @NewServiceHouseNo WHEN '' THEN NULL ELSE @NewServiceHouseNo END        
							  ,ZipCode=CASE @NewServiceZipCode WHEN '' THEN NULL ELSE @NewServiceZipCode END          
							  ,AreaCode=CASE @NewServiceAreaCode WHEN '' THEN NULL ELSE @NewServiceAreaCode END           
							  ,ModifedBy=@ModifiedBy        
							  ,ModifiedDate=dbo.fn_GetCurrentDateTime()     
							  ,IsCommunication=@IsCommunicationService  
						  WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1  
						
						  SET @PostalAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=0 AND IsActive=1)  
						  SET @ServiceAddressID=(SELECT AddressID FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] WHERE GlobalAccountNumber=@GlobalAccountNo AND IsServiceAddress=1 AND IsActive=1)  
						  
						  UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
							Service_Landmark=@NewServiceLandMark  
							,Service_StreetName=@NewServiceStreet  
							,Service_City=@NewServiceCity  
							,Service_HouseNo=@NewServiceHouseNo  
							,Service_ZipCode=@NewServiceZipCode  
							,Service_AreaCode=@NewServiceAreaCode  
							,Postal_Landmark=@NewPostalLandMark  
							,Postal_StreetName=@NewPostalStreet  
							,Postal_City=@NewPostalCity  
							,Postal_HouseNo=@NewPostalHouseNo  
							,Postal_ZipCode=@NewPostalZipCode  
							,Postal_AreaCode=@NewPostalAreaCode  
							,ServiceAddressID=@ServiceAddressID  
							,PostalAddressID=@PostalAddressID  
							,IsSameAsService=0  
						  WHERE GlobalAccountNumber=@GlobalAccountNo     
						 END    
						 COMMIT TRAN  
						END TRY  
						BEGIN CATCH  
						 ROLLBACK TRAN  
						 SET @RowsEffected=0  
						END CATCH
					
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
							AddressChangeLogId
							,GlobalAccountNumber
							,OldPostal_HouseNo
							,OldPostal_StreetName
							,OldPostal_City
							,OldPostal_Landmark
							,OldPostal_AreaCode
							,OldPostal_ZipCode
							,NewPostal_HouseNo
							,NewPostal_StreetName
							,NewPostal_City
							,NewPostal_Landmark
							,NewPostal_AreaCode
							,NewPostal_ZipCode
							,OldService_HouseNo
							,OldService_StreetName
							,OldService_City
							,OldService_Landmark
							,OldService_AreaCode
							,OldService_ZipCode
							,NewService_HouseNo
							,NewService_StreetName
							,NewService_City
							,NewService_Landmark
							,NewService_AreaCode
							,NewService_ZipCode
							,ApprovalStatusId
							,Remarks
							,CreatedBy
							,CreatedDate
							,ModifiedBy
							,ModifiedDate
							,PresentApprovalRole
							,NextApprovalRole
							,IsPostalCommunication
							,IsServiceComunication
							) 
					SELECT  
							AddressChangeLogId
							,GlobalAccountNumber
							,OldPostal_HouseNo
							,OldPostal_StreetName
							,OldPostal_City
							,OldPostal_Landmark
							,OldPostal_AreaCode
							,OldPostal_ZipCode
							,NewPostal_HouseNo
							,NewPostal_StreetName
							,NewPostal_City
							,NewPostal_Landmark
							,NewPostal_AreaCode
							,NewPostal_ZipCode
							,OldService_HouseNo
							,OldService_StreetName
							,OldService_City
							,OldService_Landmark
							,OldService_AreaCode
							,OldService_ZipCode
							,NewService_HouseNo
							,NewService_StreetName
							,NewService_City
							,NewService_Landmark
							,NewService_AreaCode
							,NewService_ZipCode
							,ApprovalStatusId
							,Remarks
							,CreatedBy
							,CreatedDate
							,ModifiedBy
							,ModifiedDate
							,PresentApprovalRole
							,NextApprovalRole
							,IsPostalCommunication
							,IsServiceComunication
					FROM Tbl_CustomerAddressChangeLog_New WHERE AddressChangeLogId = @TypeChangeRequestId
					
					END
			
			SELECT 1 AS IsSuccess,@StatusText AS StatusText FOR XML PATH('CustomerRegistrationBE')
				END
		END
  


GO


