GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId,AccessLevelID)
VALUES('Add Area','../Masters/AddArea.aspx',122,60,1,1,204,3)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,204,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,204,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO