
GO
/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_Billing_INDV]    Script Date: 06/04/2015 16:12:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- Modified By: Neeraj Kanojiya
-- Description: Created new copy. Will search Activ and In-Active customer with Temporary Book Disabled.
-- Date		  : 3-June-15
-- =============================================
CREATE PROCEDURE [dbo].[USP_IsCustomerExistsInBU_Billing_INDV] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 


	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	Declare @NoPower int
	
 

	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo AND ActiveStatusId IN (1,2)

	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	 
	  
	Select @NoPower=case when DisableTypeId=2 then 1 else 0 end from 
	Tbl_BillingDisabledBooks    where BookNo =@BookNo	 and IsActive=1   
	PRINT   @CustomerActiveStatusID
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
		SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
						IF	 @CustomerActiveStatusID  = 1   OR @CustomerActiveStatusID  = 2
							BEGIN
								IF  isnull(@NoPower,0)!=1 
									SELECT 1 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
								ELSE
									 SELECT 0 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess     Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 06/04/2015 16:13:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- Modified By : Bhimaraju V
-- Modified Date:06-05-2015    
-- Description: Updating the Direct Cust Avg reading in Main Table
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
	
	-- To get the Payments data to validate
	SELECT ISNULL(Temp.AverageReading,0) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,ISNULL(CD.GlobalAccountNumber,'') AS AccNum
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
		--							AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--	when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
		--	else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CPD.ReadCodeID
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions K
				WHERE AccountNo = (ISNULL(CD.GlobalAccountNumber,0))
				AND K.AvgUploadFileId = Temp.AvgUploadFileId GROUP BY AccountNo) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions(NOLOCK) AS Temp
	INNER JOIN Tbl_AverageUploadFiles(NOLOCK) PUF ON PUF.AvgUploadFileId = Temp.AvgUploadFileId  
	INNER JOIN Tbl_AverageUploadBatches(NOLOCK) PUB ON PUB.AvgUploadBatchId = PUF.AvgUploadBatchId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (CD.GlobalAccountNumber = Temp.AccountNo OR Temp.AccountNo = CD.OldAccountNo) 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions(NOLOCK)) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the paid amount is valid or not
			IF((SELECT COUNT(0) FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 0) > 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE ISNUMERIC(AverageReading) = 0
					
					INSERT INTO Tbl_AverageFailureTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,Comments
					)
					SELECT 
						 AvgUploadFileId
						,SNO
						,TempAccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,STUFF(Comments,1,2,'')
					FROM #AverageReadings
					WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
										
					SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
					
					-- TO delete the Failure Transactions from Reading Transaction table	
					DELETE FROM Tbl_AverageTransactions	
					WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
												WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0)
				 
					-- TO delete the Failure Transactions from temp table
					DELETE FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
						
				END				
			-- TO check whether the given AverageReading is valid or not
			
			IF((SELECT COUNT(0) FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 1) > 0)
				BEGIN								
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #AverageReadings
			WHERE IsValid = 0
								
			SELECT @FailureTransactions = ISNULL(@FailureTransactions,0) + COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
				,ReadCodeID
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber
			
			--WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
	    
		    UPDATE CAD   
			SET CAD.AvgReading = Tc.AverageReading
			FROM CUSTOMERS.Tbl_CustomerActiveDetails CAD
			INNER JOIN #ValidReadings TC ON TC.AccNum = CAD.GlobalAccountNumber
			AND TC.ReadCodeID=1-- For Direct CustomersOnly updating the avg reading
		     
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount    
		,TempDetails.ReceiptNO   
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(VARCHAR(20),PaidAmount) = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNo) then 1 else 0 end)
				ELSE 0 END) as IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
		--									AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,BD.BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	INNER JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId  
	INNER JOIN Tbl_BatchDetails(NOLOCK) BD ON BD.BatchNo = PUB.BatchNo AND BD.BatchDate = PUB.BatchDate
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (CD.OldAccountNo = TempDetails.AccountNo OR CD.GlobalAccountNumber = TempDetails.AccountNo)
	--((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
	--					OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the paid amount is valid or not
			IF((SELECT 0 FROM #PaymentValidation WHERE ISNUMERIC(PaidAmount) = 0) > 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Amount'
					WHERE ISNUMERIC(PaidAmount) = 0
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				--UPDATE B
				--SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				--FROM Tbl_BatchDetails B
				--INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetUsersToAdjustmentReport]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek.P  
-- Create date: 26 Mar 2015  
-- Description: To get the Users from Tbl_BillAdjustments for Adjustment report  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetUsersToAdjustmentReport]  
   
AS  
BEGIN  
   
 SELECT  
 (  
  SELECT UserId, Name   
  FROM Tbl_UserDetails   
  WHERE UserId IN (SELECT DISTINCT CreatedBy FROM Tbl_BillAdjustments)  
  ORDER BY Name  
  FOR XML PATH('UserManagementBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('UserManagementBEInfoByXml')  
   
END  
---------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterReadingsReport]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX)   
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
   
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, UD.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, (CD.GlobalAccountNumber+' - '+ CD.AccountNo) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) U ON U.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@TrxTypes,',')) TU ON TU.TypeId = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails UD ON UD.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
					AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
					
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(AverageReading AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress AS ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAverageUploadReport]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAverageUploadReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')		 		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, (CD.GlobalAccountNumber+' - '+CD.AccountNo) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SU.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	SELECT
	(
		SELECT RowNumber
			,CAST(AverageReading AS INT) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentEditListReport]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@TrxFrom = '')  
		BEGIN  
			SELECT @TrxFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50))   
					FROM Tbl_MPaymentType 
					WHERE ActiveStatusID = 1  
					FOR XML PATH(''), TYPE)  
					.value('.','NVARCHAR(MAX)'),1,1,'')  
		END  
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, PT.PaymentType AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, (CD.GlobalAccountNumber+' - '+CD.AccountNo)AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS PaymentType FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.PaymentType = CR.PaymentType   
	INNER JOIN Tbl_MPaymentType PT ON PT.PaymentTypeID = CR.PaymentType 
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
			Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0))  
				else ISNULL(CustomerReadings.PresentReading,ISNULL(CB.InitialReading,0)) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(Case when CustomerReadings.IsBilled IS NULL then 0
			else CustomerReadings.IsBilled end) as IsBilled
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) ELSE 0 END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) AS IsDisabledBook
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1 OR PrvExist = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Billed'
					WHERE IsBilled = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE IsRollOver = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					--IF(@IsExists = 1)
					--	BEGIN					
					--		DELETE FROM	Tbl_CustomerReadings 
					--		WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
					--								FROM Tbl_CustomerReadings
					--								WHERE GlobalAccountNumber = @AccountNo
					--								ORDER BY CustomerReadingId DESC)						
					--	END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookwiseCUstomerReadings]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================  
-- ModifiedBy : Karteek
-- Modified date : 21-May-2015
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookwiseCUstomerReadings] 
(
	@XmlDoc xml 
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
			,@UserId VARCHAR(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  
	
	DECLARE @RoleId INT
		,@UserIds VARCHAR(500)
		,@FunctionId INT = 13 -- Meter Readings
		,@ApprovalRoleId INT
	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
		
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	--SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  

	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
		dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
		,MeterNumber as CustomerExistingMeterNumber
		,MeterDials As CustomerExistingMeterDials
		,Decimals as Decimals
		,InitialReading as	InitialReading
		,AccountNo
		,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeID = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId IN(1,4)  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadingApprovalLogs   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND ApproveStatusId IN(1,2,4)
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,ISNULL(CustomerReadingInner.IsLocked,0) AS IsLocked
	,CustomerReadingInner.ApproveStatusId
	,(CASE WHEN NextApprovalRole IS NULL 
				THEN (CASE ApproveStatusId WHEN 1 THEN 'Action Pending' ELSE ApprovalStatus END)
				ELSE (CASE ApproveStatusId WHEN 1 THEN 'Action Pending From ' + RoleName ELSE ApprovalStatus + ' By ' + RoleName END)
				END) AS [Status]
	,(CASE WHEN (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')
				THEN 1
				ELSE 0
				END) AS IsPerformAction
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadingApprovalLogs CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
		ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	INNER JOIN Tbl_MApprovalStatus AS APS ON CustomerReadingInner.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS CR ON CustomerReadingInner.PresentApprovalRole = CR.RoleId
	
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadIDMain
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND IsBilled = 0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadingsMain
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDMain	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId 
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

----------------------------------------------------------------------------------------------------------------

	--Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	--INTO #CusotmersWithMaxBillID    
	--from Tbl_CustomerBills CustomerBillInnner 
	--INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	--Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 --select CustomerBillMain.AccountNo as GlobalAccountNumber
	 --,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 --INTO #CustomerLatestBills  
	 --from  Tbl_CustomerBills As CustomerBillMain
	 --INNER JOIN 
	 --#CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 --on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) 
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end)
				ELSE (Case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) END) as IsExists,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				ELSE ISNULL(CONVERT(VARCHAR(20),CustomerReadingsMain.ReadDate,106),'--') END) as LatestDate,
			CustomerList.Name,
			--(CASE WHEN CustomerReadings.IsLocked = 0 AND CustomerReadings.ApproveStatusId = 1 THEN 1
			--	WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
			--		THEN 2
			--	ELSE 3 END) as ApproveStatusId,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) THEN 1
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN 2
				ELSE 3 END) as ApproveStatusId,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.ReadingMultiplier
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.ReadingMultiplier
				ELSE CustomerReadingsMain.ReadingMultiplier END) as Multiplier,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.IsTamper
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.IsTamper
				ELSE CustomerReadingsMain.IsTamper END) as IsTamper,
			0 as Decimals,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				ELSE ISNULL(CustomerReadingsMain.AverageReading,'0.00') END) as AverageReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.TotalReadings
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.TotalReadings
				ELSE CustomerReadings.TotalReadings END) as TotalReadings,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else NULL end)
								 End)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else NULL end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading
											else null end) 
								else (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
									else NULL end)
								 End) END) as PresentReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
								else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end)
				ELSE 0 END) as PrvExist,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				ELSE (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadingsMain.Usage,0) end) END) as Usage,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
									else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading							
											else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
									else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End) END) as PreviousReading
			,CustomerList.BookNo
			,CustomerList.InitialReading
			,(CASE WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4) THEN CustomerReadings.IsRollOver ELSE 0 END) AS Rollover
			,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
			,CustomerReadings.[Status] AS [Description]
			,(CASE WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4) THEN ISNULL(CustomerReadings.IsPerformAction,1) ELSE 1 END) AS IsPerformAction
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerLatestReadingsMain	  As CustomerReadingsMain
			ON
			CustomerList.GlobalAccountNumber=CustomerReadingsMain.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerMeterApprovaStatus	  As CustomerMeterApprovals
			ON
			CustomerList.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table #CustomerLatestReadingsMain
DROP Table #CusotmersWithMaxReadIDMain
DROP Table #CusotmersWithMeterChangeLogs
DROP Table #CustomerMeterApprovaStatus
DROP Table  #CustomersListBook  
 
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountwiseMeterReadings]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 21 May 2015
-- Description: <Get BillReading details from customerreading table>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAccountwiseMeterReadings] 
(
	@XmlDoc xml
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@UserId VARCHAR(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists bit = 0
		Declare	  @PrvExists bit = 0
		Declare	  @IsInProcess bit = 0
		Declare	  @IsNoReadings bit = 0
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0
		Declare @IsMeterApproval bit=0
		
		DECLARE @RoleId INT
			,@UserIds VARCHAR(500)
			,@FunctionId INT = 13 -- Meter Readings
			,@ApprovalRoleId INT

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
		
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) =1)
			BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
			END
		ELSE
			BEGIN
				DECLARE @UserDetailedId INT
				SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
				SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			END
			
		SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
		--SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAcountNumber AND ApproveStatusId IN (1,4))
			BEGIN
				SET @IsMeterApproval = 1
			END
		
		DECLARE @MaxCustomerReadinglogId INT,@LogIsLocked BIT,@LogApprovalStatusId INT,@LogReadDate DATE
				,@Status VARCHAR(200), @IsPerformAction BIT
		
		SELECT TOP 1 @MaxCustomerReadinglogId = CustomerReadingLogId
			,@LogIsLocked = IsLocked
			,@LogApprovalStatusId = ApproveStatusId
			,@LogReadDate = ReadDate
			,@Status = (CASE WHEN NextApprovalRole IS NULL 
				THEN (CASE ApproveStatusId WHEN 1 THEN 'Action Pending' ELSE ApprovalStatus END)
				ELSE (CASE ApproveStatusId WHEN 1 THEN 'Action Pending From ' + RoleName ELSE ApprovalStatus + ' By ' + RoleName END)
				END) 
			,@IsPerformAction = (CASE WHEN (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')
				THEN 1
				ELSE 0
				END)
		FROM Tbl_CustomerReadingApprovalLogs T
		INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
			AND GlobalAccountNumber = @GlobalAcountNumber
		LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
		ORDER BY CustomerReadingLogId DESC
		
		IF(@LogIsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate))
			BEGIN
				SET @IsExists = 1 
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingLogs
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingLogs where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingLogs  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingLogs
					END
			END
		ELSE IF(@LogApprovalStatusId = 1 OR @LogApprovalStatusId = 4)
			BEGIN
				SET @IsInProcess = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingApprove
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND ApproveStatusId IN (1,4)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingApprove where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingApprove  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingApprove
					END
			END
		ELSE 
			BEGIN
				SET @IsNoReadings = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,IsBilled
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingId
				INTO #CustomerTopTwoReadingMain
				from Tbl_CustomerReadings
				where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
				Order By CustomerReadingId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingMain where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingMain  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
							 @PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=IsBilled
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingMain
					END
			END
			
		--select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		--SELECT TOP(1) @Month = BillMonth,
		--@Year = BillYear ,@LastBillReadType= 
		--(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		--=CB.ReadCodeId)
		--,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		--WHERE AccountNo = @GlobalAcountNumber  
		--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		--SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		--CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		--DECLARE @IsLatestBill BIT = 0 	

		--IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		--BEGIN
		--	SET @IsLatestBill = 1
		--END 
		--ELSE
		--  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
			 @AcountNumber AS AccountNo,
			 @OldAcountNumber AS OldAccountNo,
			 @Name   AS Name,
			 @usage as Usage
			 ,@RouteName   as RouteNum
			 --,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
			 --,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS IsActiveMonth
			 ,@IsTamper as IsTamper
			 ,@IsExists	as IsExists 
			 ,@IsInProcess as IsApprovalProces
			 ,@IsNoReadings AS IsActive
			 ,@IsMeterApproval AS IsMeterChangeApproval
			 ,convert(varchar(11),@LatestDate,105) as LatestDate
			 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
			 ,@MeterNumber as MeterNo
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
			 ,@PrvExists as IsPrvExists  
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
			 --,@LastBillReadType as LastBillReadType
			 ,ISNULL(@InititalReading,0) as	 InititalReading
			 ,(CASE WHEN (@LogApprovalStatusId = 1 OR @LogApprovalStatusId = 4) THEN ISNULL(@IsPerformAction,1) ELSE 1 END) AS IsPerformAction
			 ,@Status AS [Description]
			 ,@IsRollOver as Rollover
		FOR XML PATH('BillingBE'),TYPE
	)
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  


END  


GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentEditListReport]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT 
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, (CD.GlobalAccountNumber+' - '+CD.AccountNo) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, TotalAmountEffected
		, (CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
		, MR.Name AS AdjustmentName
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@AdjustmentTypes,',')) TU ON TU.TypeId = CR.BillAdjustmentType 
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			--,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,(REPLACE(CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1),'-','')+Format) AS Amount
			--,(SELECT SUM(TotalAmountEffected) FROM #CustomerReadingsList) AS TotalAmount
			,AdjustmentName
			,TotalRecords
			,BusinessUnitName 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
 

 
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd   .Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						 SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 


							IF @PrevCustomerBillId IS NULL 
							BEGIN

							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						IF	 isnull(@GetPaidMeterBalanceAfterBill,0) <>0
						BEGIN
						
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
 
						update Tbl_PaidMeterDetails
						SET OutStandingAmount =@PaidMeterDeductedAmount -isnull(OutStandingAmount,0)  
						WHERE AccountNo = @GlobalAccountNumber	 -- and ActiveStatusId=1
					 	
						END
						 
				
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1,
						BillNo=@CusotmerNewBillID
						where GlobalAccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteArea]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz - ID103>      
-- Create date: <02-Jun-2015>      
-- Description: <Delete Area details Row from MASTERS.Tbl_MAreaDetails> 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_DeleteArea]
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
	DECLARE @AreaCode INT  
			,@IsActive BIT  
			,@ModifiedBy VARCHAR(50)  

	SELECT @AreaCode = C.value('(AreaCode)[1]','INT')  
			,@IsActive = C.value('(IsActive)[1]','BIT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('AreaBe') AS T(C)  

	IF NOT EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE AreaCode=@AreaCode)
		BEGIN
			UPDATE MASTERS.Tbl_MAreaDetails   
				SET IsActive = @IsActive,  
					ModifedBy = @ModifiedBy,  
					ModifiedDate = dbo.fn_GetCurrentDateTime()  
			WHERE AreaCode = @AreaCode  

			SELECT @@ROWCOUNT As RowsEffected      
			FOR XML PATH('AreaBe'),TYPE     
		END
	ELSE
		BEGIN
			SELECT 
				0 AS RowsEffected
				,1 AS IsRecordsAssociated			
			FOR XML PATH('AreaBe'),TYPE
		END
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- Modified By:  V.Bhimaraju  
-- Modified date: 07-May-2015   
-- Description: Added outstanding amount in CustomerStatus Change (Flag=2)
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 ,CAD.OutStandingAmount
				  FROM UDV_CustomerMeterInformation CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,CAD.OutStandingAmount --Faiz-ID103
					 --,dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber) AS LastTransactionDate --Faiz-ID103
					 ,CONVERT(VARCHAR(20),dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber),103) AS LastTransactionDate1 --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate		
				 ,CAD.OutStandingAmount --Faiz-ID103		 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,(CD.GlobalAccountNumber +' - '+ CD.AccountNo) AS GlobalAccNoAndAccNo
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
     ,CD.OutStandingAmount --Faiz-ID103
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   


GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=3 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
      
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount
	END
END  
				 

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBookwiseMeterReadings_WithDT]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 04-05-2015
-- This is to insert the meter readings bookwise bulk into logs 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertBookwiseMeterReadings_WithDT]
(
	 @MeterReadingFrom INT
	,@CreateBy varchar(50)
	,@BU_ID VARCHAR(50)
	,@IsFinalApproval BIT
	,@FunctionId INT
	,@ActiveStatusId INT
	,@TempReadings TblMeterReadingsUpload READONLY
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess BIT = 0

	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT 
		,@CurrentApprovalLevel INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempReadings
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
					DECLARE @UserDetailedId INT
					SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
				
					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
							BEGIN
									SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																				CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																						ELSE NULL END
																			END
																			 FROM TBL_FunctionApprovalRole 
																			WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
							END
						ELSE
							BEGIN
								
								SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
							END
							
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
												
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = ISNULL(Usage,0)
				,@AccountNo = AccNum
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
				,@Multiple = Multiplier
			 FROM @TempReadings WHERE SNo = @SNo
			 
			IF(@IsExists = 1)
				BEGIN
					DECLARE @ReadId INT
					Declare @IsExistingRollOver bit
					
					SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
					FROM Tbl_CustomerReadingApprovalLogs
					WHERE GlobalAccountNumber = @AccountNo
					ORDER BY CustomerReadingLogId DESC
						 
					IF @IsExistingRollOver = 1
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
					END
					ELSE
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
					END	
				END
			 
			SELECT @MeterNumber = MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @AccountNo 
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)		
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				,(CASE WHEN @IsFinalApproval = 1 THEN 2 ELSE @ActiveStatusId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @PresentRoleId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @NextRoleId END)
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				
			IF(@IsFinalApproval = 1)
				BEGIN
				
					INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver
						,ApproveStatusId
						,PresentApprovalRole
						,NextApprovalRole)
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						,@ActiveStatusId
						,(CASE WHEN @PresentRoleId = 0 THEN @RoleId ELSE @PresentRoleId END)
						,(CASE WHEN @NextRoleId = 0 THEN @RoleId ELSE @NextRoleId END))
				
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
				END
				
			SET @IsSuccess = 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			SET @Multiple = NULL
			
		END
	
	SELECT @IsSuccess AS IsSuccess 
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingLogs]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04 may 2015
-- Description: TO insert the customer meter readings into the logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingLogs]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
		
		,@IsFinalApproval BIT
		,@FunctionId INT
		,@ActiveStatusId INT
		,@IsExists BIT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
		,@IsFinalApproval=C.value('(IsFinalApproval)[1]','bit')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')
		,@IsExists = C.value('(IsExists)[1]','BIT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	DECLARE @RoleId INT, @CurrentLevel INT
			,@PresentRoleId INT, @NextRoleId INT
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level			
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
		
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
					BEGIN
							SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																		CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																				ELSE NULL END
																	END
																	 FROM TBL_FunctionApprovalRole 
																	WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
					END
				ELSE
					BEGIN
						
						SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					END
					
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				ELSE
				BEGIN
					SET @CurrentApprovalLevel = @CurrentLevel+1
				END 
										
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
	
		END
	ELSE
		BEGIN
			SET @IsFinalApproval=1
			SET @PresentRoleId=0
			SET @NextRoleId=0
			SET @CurrentLevel=0	
			SET @CurrentApprovalLevel =0
		END
			
	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum
	
	IF(@IsExists = 1)
		BEGIN
			DECLARE @ReadId INT
			Declare @IsExistingRollOver bit
			
			SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
			FROM Tbl_CustomerReadingApprovalLogs
			WHERE GlobalAccountNumber = @AccNum
			ORDER BY CustomerReadingLogId DESC
				 
			IF @IsExistingRollOver = 1
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
			END
			ELSE
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
			END	
		END
	
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	 
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);		  
		END
		
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
	
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)	
			VALUES(
				 @AccNum
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,@IsRollover
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				

			IF(@IsRollover=1)
				BEGIN
		 
					IF @SecountReadingUsage != 0
						BEGIN
						--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
						 IF	 @SecountReadingUsage > 0
										 INSERT INTO Tbl_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole
											,CurrentApprovalLevel
											,IsLocked)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
											
						END
				END
				
		IF(@IsFinalApproval = 1)
			BEGIN		
			
				INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver
					,ApproveStatusId
					,PresentApprovalRole
					,NextApprovalRole)	
				VALUES(
					@AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover
					,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)	
				
				INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver)
				VALUES(
					 @AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover)
					

				IF(@IsRollover=1)
					BEGIN
			 
						IF @SecountReadingUsage != 0
							BEGIN
							--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
							 IF	 @SecountReadingUsage > 0
								 BEGIN
									INSERT INTO Tbl_CustomerReadings(
												 GlobalAccountNumber
												,[ReadDate]
												,[ReadBy]
												,[PreviousReading]
												,[PresentReading]
												,[AverageReading]
												,[Usage]
												,[TotalReadingEnergies]
												,[TotalReadings]
												,[Multiplier]
												,[ReadType]
												,[CreatedBy]
												,[CreatedDate]
												,[IsTamper]
												,MeterNumber
												,[MeterReadingFrom]
												,IsRollOver)
											VALUES(
												 @AccNum
												,@ReadDate
												,@ReadBy
												,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
												,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
												,@AverageReading
												,@SecountReadingUsage
												,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
												,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
												,@Multiple
												,2
												,@CreateBy
												,GETDATE()
												,@IsTamper
												,@MeterNumber
												,@MeterReadingFrom
												,@IsRollover)
												
										INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)
								END												
							END
					END
					
			 
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
				SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
					,AvgReading = CONVERT(NUMERIC,@AverageReading) 
				WHERE GlobalAccountNumber = @AccNum
			END
		
	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading BIGINT
		,@NewMeterReading BIGINT
		,@PreviousReading BIGINT
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading BIGINT
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = CAST(OldMeterReading AS NUMERIC)
		,@NewMeterReading = CAST(NewMeterReading AS NUMERIC)
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = CAST(NewMeterInitialReading AS NUMERIC)
		,@NewMeterReadingDate = NewMeterReadingDate
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_New(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId ANd IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
					--						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

				DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage > 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked = 1
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage > 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
							--									WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetArea]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jun-2015>    
-- Description: <Retriving Area records form MASTERS.Tbl_MAreaDetails>  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetArea]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
  DECLARE	@PageNo INT    
			,@PageSize INT    
        
  SELECT       
		   @PageNo = C.value('(PageNo)[1]','INT')    
		   ,@PageSize = C.value('(PageSize)[1]','INT')     
  FROM @XmlDoc.nodes('AreaBe') AS T(C) 
     
  ;WITH PagedResults AS    
  (    
   SELECT    
		 ROW_NUMBER() OVER(ORDER BY IsActive ASC , CreatedDate DESC ) AS RowNumber  
		 ,AreaCode    
		 ,Address1   
		 ,Address2  
		 ,City
		 ,ZipCode  
		 ,AreaDetails
		 ,IsActive 
   FROM MASTERS.Tbl_MAreaDetails
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('AreaBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('AreaInfoByXml')     
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogs]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT   
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	--SELECT(  
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CC.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate   
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,CC.Remarks  
		   ,CC.CreatedBy
		   ,CustomerView.ClassName
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder      
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo
		   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 --ORDER BY CC.CreatedBy DESC
		 -- changed by karteek end 
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
END
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogs]    Script Date: 06/04/2015 16:13:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT   
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CCL.CreatedDate DESC) AS RowNumber
			   ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			   ,CCL.Remarks
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS LastTransactionDate
			   ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate
			   ,CONVERT(VARCHAR(30),CCL.ChangeDate,107) AS StatusChangedDate
			   ,ApproveSttus.ApprovalStatus  
			   ,CCL.CreatedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder
			   ,CustomerView.CycleName
		       ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder      
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
			   ,COUNT(0) OVER() AS TotalChanges
		 FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		 --ORDER BY CCL.CreatedDate DESC
		 -- changed by karteek end   
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_Billing_INDV]    Script Date: 06/04/2015 16:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- Modified By: Neeraj Kanojiya
-- Description: Created new copy. Will search Activ and In-Active customer with Temporary Book Disabled.
-- Date		  : 3-June-15
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_Billing_INDV] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 


	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	Declare @BookNo varchar(50)
	Declare @NoPower int
	
 

	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo AND ActiveStatusId IN (1,2)

	PRINT	 @CustomerBusinessUnitID 
	PRINT	 @CustomerActiveStatusID
	 
	  
	Select @NoPower=case when DisableTypeId=2 then 1 else 0 end from 
	Tbl_BillingDisabledBooks    where BookNo =@BookNo	 and IsActive=1   
	PRINT   @CustomerActiveStatusID
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
		SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
						IF	 @CustomerActiveStatusID  = 1   OR @CustomerActiveStatusID  = 2
							BEGIN
								IF  isnull(@NoPower,0)!=1 
									SELECT 1 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
								ELSE
									 SELECT 0 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess     Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
				END
				ELSE
					BEGIN
					SELECT 0 AS IsSuccess,@CustomerActiveStatusID AS ActiveStatusId FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectLogs]    Script Date: 06/04/2015 16:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY RD.CreatedDate DESC) AS RowNumber
			   ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder
			   ,COUNT(0) OVER() AS TotalChanges 
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 --ORDER BY RD.CreatedDate DESC
		 -- changed by karteek end 
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogs]    Script Date: 06/04/2015 16:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
	--SELECT(  
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CMI.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' )') AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' )') AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType
			   ,('Old Reading: '+CMI.OldMeterReading +' <br/> New Reading: '+ CONVERT(VARCHAR(10),CAST(ISNULL(NewMeterInitialReading,0) AS NUMERIC))) AS Readings
			   ,CMI.Remarks
			   ,CustomerView.ClassName  
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CMI.CreatedBy  
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name 
			   ,CustomerView.OldAccountNo
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 --ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end    
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
-----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 06/04/2015 16:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
     
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY TCR.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS LastTransactionDate
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 --ORDER BY TCR.CreatedDate DESC
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 06/04/2015 16:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY BookChange.CreatedDate DESC) AS RowNumber
			 ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/> BG : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br/> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br/> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br/> BG : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br/> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CONVERT(VARCHAR(50),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS LastTransactionDate
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,BD.BusinessUnitName
			,BD.ServiceUnitName
			,BD.ServiceCenterName
			,BD.CycleName
			,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
			,BD.BookSortOrder
			,'' AS CustomerSortOrder
			,COUNT(0) OVER() AS TotalChanges 
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		--ORDER BY  BookChange.CreatedDate DESC--,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		
		)
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		--SELECT 
		--	 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
		--	--,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/>'+
		--	--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/>'+
		--	--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/>'+
		--	--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/>'+
		--	--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
		--	--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br/>'+
		--	--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br/>'+
		--	--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br/>'+
		--	--  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br/>'+
		--	--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
		--	,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		--   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

		--	,BookChange.Remarks
		--	,BookChange.CreatedBy
		--	,MTC.ClassName
		--	,CD.OldAccountNo
		--	,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		--	,ApproveSttus.ApprovalStatus  
		--	,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name
		--	,BD.BusinessUnitName
		--	,BD.ServiceUnitName
		--	,BD.ServiceCenterName
		--	,BD.CycleName
		--	,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
		--	,BD.BookSortOrder
		--	,BD.CustomerSortOrder
		--FROM Tbl_BookNoChangeLogs  BookChange  
		--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
		--AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		--INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		--INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		--INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		--INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		--INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		--INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogs]    Script Date: 06/04/2015 16:13:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By : Bhimaraju V
-- Modified Date: 22-Apr-2015
--Desc : Getting Single field instead of all
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
		,@PageNo INT
		,@PageSize INT
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
	   ,@PageNo=C.value('(PageNo)[1]','VARCHAR(20)')  
	   ,@PageSize=C.value('(PageSize)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END    
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
	;WITH PageResult AS(
		SELECT ROW_NUMBER() OVER(ORDER BY CA.CreatedDate DESC) AS RowNumber    
			 ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
			 ,dbo.fn_GetCustomerServiceAddress_New(OldPostal_HouseNo,OldPostal_StreetName,
			 OldPostal_Landmark,OldPostal_City,OldPostal_AreaCode,OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewPostal_HouseNo,NewPostal_StreetName,
			 NewPostal_Landmark,NewPostal_City,NewPostal_AreaCode,NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(OldService_HouseNo,OldService_StreetName,
			 OldService_Landmark,OldService_City,OldService_AreaCode,OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewService_HouseNo,NewService_StreetName,
			 NewService_Landmark,NewService_City,NewService_AreaCode,NewService_ZipCode) AS NewServiceAddress			  
			 ,CA.Remarks  
			 ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS LastTransactionDate   
			 ,ApproveSttus.ApprovalStatus  
			 ,CA.CreatedBy
			 ,CustomerView.ClassName
			 ,CustomerView.BusinessUnitName
			 ,CustomerView.ServiceUnitName
			 ,CustomerView.ServiceCenterName
			 ,CustomerView.SortOrder AS CustomerSortOrder  
			 ,CustomerView.CycleName
		     ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
		     ,CustomerView.BookSortOrder     
			 ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		     ,CustomerView.OldAccountNo
		     ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_CustomerAddressChangeLog_New CA   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId     
		 
	)  
    
    SELECT(  
    SELECT   *
			--,(SELECT COUNT(0) FROM PageResult) AS TotalChanges
    FROM PageResult
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	FOR XML PATH('AuditTrayAddressList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')
END 
---------------------------------------------------------------------------------------------------

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 06/04/2015 16:47:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
	
	SET @MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
	
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId NOT IN(3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((@OldMeterReading < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage > 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END

