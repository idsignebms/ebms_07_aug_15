
GO

/****** Object:  UserDefinedFunction [dbo].[fn_CustomerExistenceCheck]    Script Date: 06/09/2015 19:14:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA
-- Create date: 09-JUNE-2015  
-- Description: To get the customer by BU is exists   
-- =============================================  
CREATE FUNCTION [dbo].[fn_CustomerExistenceCheck]
(  
@AccountNo varchar(50),
@BUID varchar(50)
) 
RETURNS @CustomerExistenceCheck TABLE(IsSuccess BIT,IsCustExistsInOtherBU BIT, GlobalAccountNumber VARCHAR(50),ActiveStatusId INT) 
AS  
BEGIN  
 DECLARE @CustomerBusinessUnitID VARCHAR(50)
		 ,@CustomerActiveStatusID INT   
		 ,@GlobalAccountNumber VARCHAR(50)
		 ,@ActiveStatusId INT

 SELECT @CustomerBusinessUnitID=BU_ID
		,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
		,@GlobalAccountNumber=GlobalAccountNumber 
		,@ActiveStatusId=ActiveStatusId 
 FROM	UDV_IsCustomerExists 
 WHERE	GlobalAccountNumber=@AccountNo 
		OR OldAccountNo=@AccountNo   
		OR MeterNumber=@AccountNo  
      
 IF @CustomerBusinessUnitID IS NULL  
	  BEGIN  
		INSERT INTO @CustomerExistenceCheck
		SELECT  0 AS IsSuccess
				,0 AS IsCustExistsInOtherBU
				,@GlobalAccountNumber AS GlobalAccountNumber
				,@ActiveStatusId AS ActiveStatusId 
	  END  
 ELSE
	 BEGIN
		IF @CustomerBusinessUnitID =  @BUID OR @BUID =''      
			BEGIN 
				 IF  @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3   
				  BEGIN  
				   INSERT INTO @CustomerExistenceCheck 
				  SELECT 1 AS IsSuccess 
						,0 AS IsCustExistsInOtherBU
						,@GlobalAccountNumber AS GlobalAccountNumber
						,@ActiveStatusId AS ActiveStatusId
				  END  
				 ELSE  
				  BEGIN          
				   INSERT INTO @CustomerExistenceCheck 
				   SELECT	0 AS IsSuccess
							,0 AS IsCustExistsInOtherBU
							,@GlobalAccountNumber AS GlobalAccountNumber
							,@ActiveStatusId AS ActiveStatusId
				  END  
			END 
		   ELSE  
			   BEGIN  
				INSERT INTO @CustomerExistenceCheck
				SELECT	1 AS IsSuccess
						,1 AS IsCustExistsInOtherBU 
						,@GlobalAccountNumber AS GlobalAccountNumber
						,@ActiveStatusId AS ActiveStatusId
			   END  
	END  
  RETURN
END  
GO


