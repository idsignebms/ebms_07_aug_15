
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno_ByCheck]    Script Date: 06/09/2015 19:14:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  09/JUNE/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
CREATE PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
		  ,@Flag=C.value('(Flag)[1]','INT') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
			
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1) --//If customer existence check is successfull then get data
	 BEGIN
		IF(@Flag =1)--For CustomerName Change
			BEGIN
				IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))  
					BEGIN  
						SELECT TOP 1
								  CD.GlobalAccountNumber AS GlobalAccountNumber
								 ,CD.AccountNo As AccountNo
								 ,CD.FirstName
								 ,CD.MiddleName
								 ,CD.LastName
								 ,CD.Title					 
								 ,CD.KnownAs
								 ,ISNULL(MeterNumber,'--') AS MeterNo
								 ,CD.ClassName AS Tariff
								 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
							  FROM UDV_CustomerMeterInformation CD
							  WHERE (CD.GlobalAccountNumber=@GlobalAccountNumber)
							  AND ActiveStatusId IN (1,2)  
							  
							  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,OutStandingAmount --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber AND ReadCodeID=1)
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END 
	END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmount - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  Bhimaraju V 
-- Create date: 12-03-2014 5 
-- Description: Get Details For PaymentEntry-AccountWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)
	
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('PaymentsBe') as T(C)	

	SELECT CD.GlobalAccountNumber
			,CD.OldAccountNo
			,(CD.AccountNo+' - '+CD.GlobalAccountNumber)AS AccNoAndGlobalAccNo
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			,MeterNumber
			,ClassName
			,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
			 ,CD.Service_Landmark    
			 ,CD.Service_City,''    
			 ,CD.Service_ZipCode) AS ServiceAddress 
			,OutStandingAmount
			,ISNULL(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber),'--') AS LastPaidDate
			,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS LastPaidAmount
			,ISNULL(dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber),'--') AS LastBillGeneratedDate		
			,dbo.fn_GetCustomerLastBillAmountWithArrears_New(CD.GlobalAccountNumber) AS LastBillAmount
			,1 AS IsExists
	FROM UDV_CustomerDescription CD
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail C ON C.GlobalAccountNumber=CD.GlobalAccountNumber
	WHERE (CD.GlobalAccountNumber = @AccountNo OR CD.OldAccountNo = @AccountNo) AND C.ActiveStatusId in(1,2,3)
	FOR XML PATH('PaymentsBe')
	 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
        
  DECLARE @GlobalAccountNumber VARCHAR(50)  
 
	SET @GlobalAccountNumber =(SELECT GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail 
			WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo)

	IF( @GlobalAccountNumber IS NOT NULL)
		BEGIN
			IF ((SELECT [dbo].[fn_IsAccountNoExists_BU_Id_Payments](@GlobalAccountNumber,@BU_ID)) = 1)
				BEGIN
						SELECT  
						 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
						,CD.OldAccountNo--Faiz-ID103
						,CD.ClassName
						,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS AccNoAndGlobalAccNo
						,CD.OutStandingAmount AS DueAmount  --Commented By Raja-ID065
						,CD.GlobalAccountNumber AS AccountNo
						,'success' AS StatusText
						,(SELECT CustomerType FROM Tbl_MCustomerTypes CT WHERE CT.CustomerTypeId=CD.CustomerTypeId) AS CustomerType
						,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,NULL,CD.Service_ZipCode) AS Address1
					FROM [UDV_CustomerDescription] CD
					WHERE GlobalAccountNumber = @GlobalAccountNumber AND (BU_ID = @BU_ID OR @BU_ID = '')
					FOR XML PATH('MastersBE')  
				END
			ELSE
				BEGIN
					SELECT 'Customer doesn''t exist in selected Bussiness Unit' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
		END
	ELSE
		BEGIN
			SELECT 'Customer doesn''t exist' AS StatusText FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForBillGen]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 25-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- Create date  : 8-APRIL-2015                    
 -- Description  : REMOVE ACTIVE STATUS ID FILTER
 
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForBillGen]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID  
				  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                   
				  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo
				  ,(A.AccountNo+ ' - '+ A.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,A.OutStandingAmount AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful  
				  ,A.ServiceUnitName 
				  ,A.ServiceCenterName
				  ,A.CycleName
				  --,A.BookNo      
				  ,A.OldAccountNo  
				  ,A.BookId AS BookNo
		  FROM [UDV_CustomerDescription] AS A    
		  LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				--AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@CustomerTypeId INT
			,@OldAccountNo VARCHAR(50)
			,@AccNoAndGlobalAccNo VARCHAR(100)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount
			,@AccNoAndGlobalAccNo=(AccountNo+' - '+GlobalAccountNumber)
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@CustomerTypeId=CustomerTypeId
			,@OldAccountNo=OldAccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC, CustomerPaymentID  DESC
	
	
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
		,RC.DisplayCode AS ReadType
		,CB.BillYear        
		,CB.BillMonth        
		,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
		,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		
		,Usage AS Consumption
		,Dials AS MeterDials
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT 
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 		
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,CB.AccountNo AS AccountNo
		,@Name AS Name
		,@AccNoAndGlobalAccNo AS AccNoAndGlobalAccNo
		,@ServiceAddress AS ServiceAddress
		,@OutstandingAmount AS OutStandingAmount
		,@BookGroup AS BookGroup 
		,@BookName AS BookName
		,@Tariff AS Tariff
		,@OldAccountNo AS OldAccountNo
		,COUNT(0) OVER() AS TotalRecords
		,VAT AS Vat
		,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS ClassName
		,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=@CustomerTypeId) AS CustomerType
		
	FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
	WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
	ORDER BY CB.CustomerBillId DESC
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     


GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 08-JAN-2014      
-- Description: The purpose of this procedure is to get list of MeterInformation      
-- Modified By:  Faiz-ID103  
-- Modified date: 08-JAN-2014      
-- Description: Added Business Unit name and Meter Dials field for new customer registration page  
  
-- =============================================      
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @MeterNo VARCHAR(100)      
   ,@MeterSerialNo VARCHAR(100)      
   ,@BUID VARCHAR(50)      
   ,@CustomerTypeId INT      
         
  SELECT         
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')      
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')      
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')      
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')      
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  IF (@CustomerTypeId=3)      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType     
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo      
       ,BU.BusinessUnitName    
       ,MeterDials  
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID WHERE M.ActiveStatusId IN(1) AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
				WHERE MeterNumber !='' )      
      AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))       
      AND M.MeterType=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
  ELSE      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType      
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo       
       ,MeterDials  
       ,BU.BusinessUnitName    
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID    
      WHERE M.ActiveStatusId IN(1)      
      AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT DISTINCT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
							WHERE ISNULL(MeterNumber,'') !='' )      
        AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))   
      AND M.MeterType !=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
         
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- Modified By:  V.Bhimaraju  
-- Modified date: 07-May-2015   
-- Description: Added outstanding amount in CustomerStatus Change (Flag=2)
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 ,CAD.OutStandingAmount
				  FROM UDV_CustomerMeterInformation CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,CAD.OutStandingAmount --Faiz-ID103
					 --,dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber) AS LastTransactionDate --Faiz-ID103
					 ,CONVERT(VARCHAR(20),dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber),103) AS LastTransactionDate1 --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate		
				 ,dbo.fn_GetCustomer_ADC(Usage) AS AverageDailyUsage
				 ,CAD.OutStandingAmount --Faiz-ID103		 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
     ,CD.OutStandingAmount --Faiz-ID103
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchListEditListReport]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the List of Batches for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchListEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			--,@PageSize INT  
			--,@PageNo INT  
			,@BU_ID VARCHAR(50)
			,@TransactionFrom VARCHAR(MAX) 
		
	SELECT
		 @Users = C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(15)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')		
		,@TransactionFrom = C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TransactionFrom = '')
		BEGIN
			SELECT @TransactionFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50)) 
					 FROM Tbl_MPaymentType
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	SELECT
	(
		SELECT 
			 ISNULL(CONVERT(VARCHAR(20),BD.CreatedDate,106),'--') AS TransactionDate
			,BD.BatchNo 
			,BD.BatchID
			,ISNULL(CONVERT(VARCHAR(20),BD.BatchDate,106),'--') AS PaidDate
			,(CASE WHEN CP.PaymentType = 2 
				THEN CONVERT(VARCHAR(25), CAST(SUM(ISNULL(CP.PaidAmount,0)) AS MONEY), 1)
				ELSE CONVERT(VARCHAR(25), CAST(BD.BatchTotal AS MONEY), 1) END) AS Amount
			,U.Name AS UserName
			,CONVERT(VARCHAR(25), CAST(SUM(ISNULL(CP.PaidAmount,0)) AS MONEY), 1) AS PaidAmount
			,(CASE WHEN ISNULL(CP.BatchNo,0) = 0 THEN 0 ELSE COUNT(0) END) AS TotalRecords -- Total Customers
		FROM Tbl_BatchDetails BD
		INNER JOIN Tbl_UserDetails U ON U.UserId = BD.CreatedBy 			 
				AND BD.CreatedBy IN(SELECT [com] AS UserId FROM dbo.fn_Split(@Users,','))
				AND CONVERT (DATE,BD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
				AND BD.BU_ID = @BU_ID 
		LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchID 
		WHERE BD.BatchStatus = 2 AND CP.PaymentType IN (SELECT [com] AS UserId FROM dbo.fn_Split(@TransactionFrom,','))
		GROUP BY BD.CreatedDate,BD.BatchNo,BD.BatchDate,BD.BatchTotal
				,U.Name,CP.BatchNo,BD.BatchID,CP.PaymentType
		--HAVING SUM(CP.PaidAmount) >= BD.BatchTotal
		
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
END
------------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno_ByCheck]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  09/JUNE/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
		  ,@Flag=C.value('(Flag)[1]','INT') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
			
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1) --//If customer existence check is successfull then get data
	 BEGIN
		IF(@Flag =1)--For CustomerName Change
			BEGIN
				IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))  
					BEGIN  
						SELECT TOP 1
								  CD.GlobalAccountNumber AS GlobalAccountNumber
								 ,CD.AccountNo As AccountNo
								 ,CD.FirstName
								 ,CD.MiddleName
								 ,CD.LastName
								 ,CD.Title					 
								 ,CD.KnownAs
								 ,ISNULL(MeterNumber,'--') AS MeterNo
								 ,CD.ClassName AS Tariff
								 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
							  FROM UDV_CustomerMeterInformation CD
							  WHERE (CD.GlobalAccountNumber=@GlobalAccountNumber)
							  AND ActiveStatusId IN (1,2)  
							  
							  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,OutStandingAmount --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber AND ReadCodeID=1)
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END 
	END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_ClosePaymentUploadBatch]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to Close Payment Upload Batch record
-- =============================================
ALTER PROCEDURE [dbo].[USP_ClosePaymentUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
			,@BatchID INT
		
	SELECT       
		 @PUploadBatchId = C.value('(PUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
	
	SELECT @BatchID = BD.BatchID 
	FROM Tbl_BatchDetails BD 
	INNER JOIN Tbl_PaymentUploadBatches PUB ON PUB.BatchNo = BD.BatchNo 
			AND PUB.BatchDate = BD.BatchDate AND PUB.BU_ID = BD.BU_ID
			AND PUB.PUploadBatchId = @PUploadBatchId
	
	UPDATE Tbl_PaymentUploadBatches 
	SET IsClosedBatch = 1
	WHERE PUploadBatchId = @PUploadBatchId
	
	UPDATE Tbl_BatchDetails 
	SET BatchStatus = 2
	WHERE BatchID = @BatchID
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('PaymentsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookwiseCUstomerReadings]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 

-- =============================================  
-- ModifiedBy : Karteek
-- Modified date : 21-May-2015
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookwiseCUstomerReadings] 
(
	@XmlDoc xml 
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
			,@UserId VARCHAR(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  
	
	DECLARE @RoleId INT
		,@UserIds VARCHAR(500)
		,@FunctionId INT = 13 -- Meter Readings
		,@ApprovalRoleId INT
		,@UserDetailedId INT
		,@ApprovalLevel INT
	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
		
	SET @UserDetailedId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) > 0)
		BEGIN
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) =1)
				BEGIN
					SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
					SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
					SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
					SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
				END
			ELSE
				BEGIN
					--
					--SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
					SET @UserIds=(SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
				END
		END

	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
		dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
		,MeterNumber as CustomerExistingMeterNumber
		,MeterDials As CustomerExistingMeterDials
		,Decimals as Decimals
		,InitialReading as	InitialReading
		,AccountNo
		,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeID = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId IN(1,4)  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadingApprovalLogs   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND ApproveStatusId IN(1,2,4)
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,ISNULL(CustomerReadingInner.IsLocked,0) AS IsLocked
	,CustomerReadingInner.ApproveStatusId
	,(CASE WHEN NextApprovalRole IS NULL 
				THEN (CASE ApproveStatusId WHEN 1 THEN 'Action Pending' ELSE ApprovalStatus END)
				ELSE (CASE ApproveStatusId WHEN 1 THEN 'Action Pending From ' + RoleName ELSE ApprovalStatus + ' By ' + RoleName END)
				END) AS [Status]
	,(CASE WHEN @UserIds IS NULL 
			THEN (CASE WHEN @UserId = CustomerReadingInner.CreatedBy THEN 1 ELSE 0 END)
			ELSE (CASE WHEN (@UserDetailedId IN (SELECT com FROM dbo.fn_Split(@UserIds,','))) AND (@ApprovalLevel + 1 = CurrentApprovalLevel)
				THEN 1
				ELSE 0
				END)
			END) AS IsPerformAction
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadingApprovalLogs CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
		ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	INNER JOIN Tbl_MApprovalStatus AS APS ON CustomerReadingInner.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS CR ON CustomerReadingInner.PresentApprovalRole = CR.RoleId
	
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadIDMain
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND IsBilled = 0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadingsMain
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDMain	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId 
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

----------------------------------------------------------------------------------------------------------------

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 

----------------------------------------------------------------------------------------------------------------

	--Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	--INTO #CusotmersWithMaxBillID    
	--from Tbl_CustomerBills CustomerBillInnner 
	--INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	--Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 --select CustomerBillMain.AccountNo as GlobalAccountNumber
	 --,CustomerBillMain.BillGeneratedDate,CustomerBillMain.ReadType 
	 --INTO #CustomerLatestBills  
	 --from  Tbl_CustomerBills As CustomerBillMain
	 --INNER JOIN 
	 --#CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 --on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) 
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end)
				ELSE (Case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) END) as IsExists,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				ELSE ISNULL(CONVERT(VARCHAR(20),CustomerReadingsMain.ReadDate,106),'--') END) as LatestDate,
			CustomerList.Name,
			--(CASE WHEN CustomerReadings.IsLocked = 0 AND CustomerReadings.ApproveStatusId = 1 THEN 1
			--	WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
			--		THEN 2
			--	ELSE 3 END) as ApproveStatusId,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) THEN 1
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN 2
				ELSE 3 END) as ApproveStatusId,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.ReadingMultiplier
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.ReadingMultiplier
				ELSE CustomerReadingsMain.ReadingMultiplier END) as Multiplier,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.IsTamper
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.IsTamper
				ELSE CustomerReadingsMain.IsTamper END) as IsTamper,
			0 as Decimals,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				ELSE ISNULL(CustomerReadingsMain.AverageReading,'0.00') END) as AverageReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.TotalReadings
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.TotalReadings
				ELSE CustomerReadings.TotalReadings END) as TotalReadings,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else NULL
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else NULL end)
								 End)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else NULL
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else NULL end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading
											else null end) 
								else NULL
								--(case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
								--	else NULL end)
								 End) END) as PresentReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
								else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end)
				ELSE 0 END) as PrvExist,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				ELSE (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadingsMain.Usage,0) end) END) as Usage,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else isnull(CustomerList.InitialReading,0)
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else isnull(CustomerList.InitialReading,0)
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading							
											else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else isnull(CustomerList.InitialReading,0)
								--(case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
								--	else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End) END) as PreviousReading
			,CustomerList.BookNo
			,CustomerList.InitialReading
			,(CASE WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4) THEN CustomerReadings.IsRollOver ELSE 0 END) AS Rollover
			,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
			,(CASE WHEN (MeterDisconnectApprovals.ApprovalStatusId = 1 OR MeterDisconnectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
			,CustomerReadings.[Status] AS [Description]
			,(CASE WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4) THEN ISNULL(CustomerReadings.IsPerformAction,1) ELSE 1 END) AS IsPerformAction
			,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,@ReadDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
			,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,@ReadDate) 
					THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerLatestReadingsMain	  As CustomerReadingsMain
			ON
			CustomerList.GlobalAccountNumber=CustomerReadingsMain.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerMeterApprovaStatus	  As CustomerMeterApprovals
			ON
			CustomerList.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerReadToDirectApprovaStatus	  As MeterDisconnectApprovals
			ON
			CustomerList.GlobalAccountNumber=MeterDisconnectApprovals.GlobalAccountNumber
			LEFT JOIN  
			#CustomerAssignMeterApprovaStatus	  As AssignMeterApprovals
			ON
			CustomerList.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber	
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table #CustomerLatestReadingsMain
DROP Table #CusotmersWithMaxReadIDMain
DROP Table #CusotmersWithMeterChangeLogs
DROP Table #CusotmersReadToDirectChangeLogs
DROP Table #CustomerReadToDirectApprovaStatus
DROP Table #CustomerMeterApprovaStatus
DROP Table #CustomerAssignMeterApprovaStatus
DROP Table #CusotmersAssignMeterLogs
DROP Table  #CustomersListBook  
 
END  



GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountwiseMeterReadings]    Script Date: 06/09/2015 19:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 21 May 2015
-- Description: <Get BillReading details from customerreading table>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAccountwiseMeterReadings] 
(
	@XmlDoc xml
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@UserId VARCHAR(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists bit = 0
		Declare	  @PrvExists bit = 0
		Declare	  @IsInProcess bit = 0
		Declare	  @IsNoReadings bit = 0
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0
		Declare @IsMeterApproval bit=0
		Declare @IsReadToDirectApproval bit=0		
		DECLARE @MeterChangedDate DATETIME
		DECLARE @MeterAssignedDate DATETIME
		
		DECLARE @RoleId INT
			,@UserIds VARCHAR(500)
			,@FunctionId INT = 13 -- Meter Readings
			,@ApprovalRoleId INT
			,@UserDetailedId INT
			,@ApprovalLevel INT

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
		
		SET @UserDetailedId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
		
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) > 0)
			BEGIN
				IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) =1)
					BEGIN
						SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
						SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
						SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
						SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
					END
				ELSE
					BEGIN
						--
						--SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
						SET @UserIds=(SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
					END
			END
  
		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAcountNumber AND ApproveStatusId IN (1,4))
			BEGIN
				SET @IsMeterApproval = 1
			END
		IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @GlobalAcountNumber AND ApprovalStatusId IN (1,4))
			BEGIN
				SET @IsReadToDirectApproval = 1
			END
		IF EXISTS(SELECT 0 FROM Tbl_Audit_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAcountNumber)
			BEGIN
				SELECT TOP 1 @MeterChangedDate = MeterChangedDate FROM Tbl_Audit_CustomerMeterInfoChangeLogs 
					WHERE AccountNo = @GlobalAcountNumber
					ORDER BY MeterInfoChangeLogId DESC
			END
		IF EXISTS(SELECT 0 FROM Tbl_Audit_AssignedMeterLogs WHERE GlobalAccountNo = @GlobalAcountNumber)
			BEGIN
				SELECT TOP 1 @MeterAssignedDate = AssignedMeterDate FROM Tbl_Audit_AssignedMeterLogs 
					WHERE GlobalAccountNo = @GlobalAcountNumber
					ORDER BY AssignedMeterId DESC
			END
		
		DECLARE @MaxCustomerReadinglogId INT,@LogIsLocked BIT,@LogApprovalStatusId INT,@LogReadDate DATE
				,@Status VARCHAR(200), @IsPerformAction BIT
		
		SELECT TOP 1 @MaxCustomerReadinglogId = CustomerReadingLogId
			,@LogIsLocked = IsLocked
			,@LogApprovalStatusId = ApproveStatusId
			,@LogReadDate = ReadDate
			,@Status = (CASE WHEN NextApprovalRole IS NULL 
				THEN (CASE ApproveStatusId WHEN 1 THEN 'Action Pending' ELSE ApprovalStatus END)
				ELSE (CASE ApproveStatusId WHEN 1 THEN 'Action Pending From ' + RoleName ELSE ApprovalStatus + ' By ' + RoleName END)
				END) 
			,@IsPerformAction = (CASE WHEN @UserIds IS NULL 
									THEN (CASE WHEN @UserId = T.CreatedBy THEN 1 ELSE 0 END)
									ELSE (CASE WHEN (@UserDetailedId IN (SELECT com FROM dbo.fn_Split(@UserIds,','))) AND (@ApprovalLevel + 1 = CurrentApprovalLevel)
										THEN 1
										ELSE 0
										END)
									END)
		FROM Tbl_CustomerReadingApprovalLogs T
		INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
			AND GlobalAccountNumber = @GlobalAcountNumber
		LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
		ORDER BY CustomerReadingLogId DESC
		
		IF(@LogIsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate))
			BEGIN
				SET @IsExists = 1 
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingLogs
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingLogs where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingLogs  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingLogs
					END
			END
		ELSE IF(@LogApprovalStatusId = 1 OR @LogApprovalStatusId = 4)
			BEGIN
				SET @IsInProcess = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingApprove
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND ApproveStatusId IN (1,4)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingApprove where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingApprove  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingApprove
					END
			END
		ELSE 
			BEGIN
				SET @IsNoReadings = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,IsBilled
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingId
				INTO #CustomerTopTwoReadingMain
				from Tbl_CustomerReadings
				where GlobalAccountNumber=@GlobalAcountNumber and IsBilled=0 
				Order By CustomerReadingId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingMain where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingMain  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
							 @PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=IsBilled
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingMain
					END
			END
			
		--select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		--SELECT TOP(1) @Month = BillMonth,
		--@Year = BillYear ,@LastBillReadType= 
		--(Select top 1 ReadCode from Tbl_MReadCodes where ReadCodeId 
		--=CB.ReadCodeId)
		--,@EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		--WHERE AccountNo = @GlobalAcountNumber  
		--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		--SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+
		--CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   

		--DECLARE @IsLatestBill BIT = 0 	

		--IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		--BEGIN
		--	SET @IsLatestBill = 1
		--END 
		--ELSE
		--  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
			 @AcountNumber AS AccountNo,
			 @OldAcountNumber AS OldAccountNo,
			 @Name   AS Name,
			 @usage as Usage
			 ,@RouteName   as RouteNum
			 --,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
			 --,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS IsActiveMonth
			 ,@IsTamper as IsTamper
			 ,@IsExists	as IsExists 
			 ,@IsInProcess as IsApprovalProces
			 ,@IsNoReadings AS IsActive
			 ,@IsMeterApproval AS IsMeterChangeApproval
			 ,@IsReadToDirectApproval AS IsReadToDirectApproval
			 ,convert(varchar(11),@LatestDate,106) as LatestDate
			 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
			 ,@MeterNumber as MeterNo
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
			 ,@PrvExists as IsPrvExists  
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as	 IsBilled
			 --,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
			 --,@LastBillReadType as LastBillReadType
			 ,ISNULL(@InititalReading,0) as	 InititalReading
			 ,(CASE WHEN (@LogApprovalStatusId = 1 OR @LogApprovalStatusId = 4) THEN ISNULL(@IsPerformAction,1) ELSE 1 END) AS IsPerformAction
			 ,@Status AS [Description]
			 ,@IsRollOver as Rollover
			 ,(CASE WHEN ISNULL(@MeterChangedDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,@MeterChangedDate) >= CONVERT(DATE,@ReadDate) THEN 1 ELSE 0 END) END) AS IsMeterChangedDateExists
			 ,(CASE WHEN ISNULL(@MeterAssignedDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,@MeterAssignedDate) >= CONVERT(DATE,@ReadDate) THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
		FOR XML PATH('BillingBE'),TYPE
	)
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  


END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 06/09/2015 19:15:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml    
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,BookCode  
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,CAD.InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  ,CD.AccountNo --Faiz-ID103
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  
GO


