
GO

/****** Object:  View [dbo].[UDV_SearchCustomer]    Script Date: 06/12/2015 19:37:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[UDV_SearchCustomer]     
AS    
   
 SELECT    DISTINCT
  CPD.GlobalAccountNumber,
  (CD.AccountNo+' - '+CPD.GlobalAccountNumber) AS AccNoAndGlobalAccNo,
  CD.AccountNo,  
  CD.OldAccountNo,  
  dbo.fn_GetCustomerFullName_New(   
    CD.Title,    
    CD.FirstName,    
    CD.MiddleName,    
    CD.LastName) AS Name,   
  CD.HomeContactNumber,  
  BusinessContactNumber,  
  CD.OtherContactNumber,   
  BookDetails.BusinessUnitName,  
  Dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,Service_City,'',Service_ZipCode) As ServiceAddress,  
  BookDetails.ServiceUnitName,  
  BookDetails.ServiceCenterName,  
  TariffClasses.ClassName,     
  CStatus.StatusName,  
  CPD.IsBookNoChanged,  
  CPD.MeterNumber,  
  CD.ActiveStatusId  
  ,BookDetails.BU_ID  
  ,BookDetails.SU_ID  
  ,BookDetails.ServiceCenterId  
  ,BookDetails.CycleId  
  ,CPD.TariffClassID as TariffId  
  ,ReadCodeId  
  ,CAD.CreatedDate  
  ,BookDetails.BookCode  
  ,BookDetails.ID  
  ,BookDetails.BookSortOrder  
  ,CPD.SortOrder  
  ,CAD.InitialReading
  ,BookDetails.CycleName  
 FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD    
 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN  dbo.UDV_BookNumberDetails(NOLOCK) BookDetails ON BookDetails.BookNo=CPD.BookNo  
 INNER JOIN  Tbl_MCustomerStatus(NOLOCK) CStatus ON CStatus.StatusId=CD.ActiveStatusId  
 INNER JOIN  Tbl_MTariffClasses(NOLOCK) TariffClasses ON TariffClasses.ClassID=CPD.TariffClassID 
 INNER JOIN TBL_BussinessUnits AS BU ON BookDetails.BU_ID =BU.BU_ID AND BU.ActiveStatusId=1-- Neeraj ID-077 18-May-15


GO


