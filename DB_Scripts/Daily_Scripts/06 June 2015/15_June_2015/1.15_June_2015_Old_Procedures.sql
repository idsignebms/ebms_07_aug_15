
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 06/15/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
 

 
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd   .Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
							GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET @Usage =0
											SET @ActualUsage=0
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						 SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 


							IF @PrevCustomerBillId IS NULL 
							BEGIN

							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						IF	 isnull(@GetPaidMeterBalanceAfterBill,0) <>0
						BEGIN
						
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
 
						update Tbl_PaidMeterDetails
						SET OutStandingAmount =@PaidMeterDeductedAmount -isnull(OutStandingAmount,0)  
						WHERE AccountNo = @GlobalAccountNumber	 -- and ActiveStatusId=1
					 	
						END
						 
				
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1,
						BillNo=@CusotmerNewBillID
						where GlobalAccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------






GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 06/15/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	SELECT
	(	
		SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.AmountEffected) AS AmountEffected
			,BA.BatchNo
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidAverageConsumptionUploads]    Script Date: 06/15/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 01 May 2015
-- Description:	To validate the AVerage Consumption data and Inserting the Consumption into AverageConsumption realted tables 
-- Modified By : Bhimaraju V
-- Modified Date:06-05-2015    
-- Description: Updating the Direct Cust Avg reading in Main Table
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidAverageConsumptionUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
	
	-- To get the Payments data to validate
	SELECT DISTINCT ISNULL(Temp.AverageReading,0) AS AverageReading
		,CD.ActiveStatusId AS ActiveStatusId
		,CD.OldAccountNo
		,ISNULL(CD.GlobalAccountNumber,'') AS AccNum
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy
		--							AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--	when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = Temp.CreatedBy) then 1
		--	else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,Temp.CreatedBy
		,Temp.CreatedDate
		,Temp.AvgUploadFileId
		,Temp.AvgTransactionId
		,Temp.SNO
		,Temp.AccountNo AS TempAccountNo
		,1 AS IsValid
		,CPD.ReadCodeID
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AverageTransactions K
				WHERE (AccountNo = (ISNULL(CD.GlobalAccountNumber,0)) OR AccountNo = CD.OldAccountNo)) > 1) THEN 1 ELSE 0 END) AS IsDuplicate
	INTO #AverageReadings
	FROM Tbl_AverageTransactions(NOLOCK) AS Temp
	INNER JOIN Tbl_AverageUploadFiles(NOLOCK) PUF ON PUF.AvgUploadFileId = Temp.AvgUploadFileId  
	INNER JOIN Tbl_AverageUploadBatches(NOLOCK) PUB ON PUB.AvgUploadBatchId = PUF.AvgUploadBatchId  
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (CD.GlobalAccountNumber = Temp.AccountNo OR Temp.AccountNo = CD.OldAccountNo) 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	WHERE Temp.AvgUploadFileId IN (SELECT MAX(AvgUploadFileId) FROM Tbl_AverageTransactions(NOLOCK)) 
	
	SELECT TOP(1) @FileUploadId = AvgUploadFileId FROM #AverageReadings
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE AccNum = '')
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the customer is in active or not
			--IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 3)
			--	BEGIN
					
			--		UPDATE #AverageReadings
			--		SET IsValid = 0,
			--			Comments = Comments + ', Hold Customer'
			--		WHERE ActiveStatusId = 3
					
			--	END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #AverageReadings WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the paid amount is valid or not
			IF((SELECT COUNT(0) FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 0) > 0)
				BEGIN
					
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE ISNUMERIC(AverageReading) = 0
					
					INSERT INTO Tbl_AverageFailureTransactions
					(
						 AvgUploadFileId
						,SNO
						,AccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,Comments
					)
					SELECT 
						 AvgUploadFileId
						,SNO
						,TempAccountNo
						,AverageReading
						,CreatedDate
						,CreatedBy
						,STUFF(Comments,1,2,'')
					FROM #AverageReadings
					WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
										
					SELECT @FailureTransactions = COUNT(0) FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
					
					-- TO delete the Failure Transactions from Reading Transaction table	
					DELETE FROM Tbl_AverageTransactions	
					WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
												WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0)
				 
					-- TO delete the Failure Transactions from temp table
					DELETE FROM #AverageReadings WHERE IsValid = 0 AND ISNUMERIC(AverageReading) = 0
						
				END				
			-- TO check whether the given AverageReading is valid or not
			
			IF((SELECT COUNT(0) FROM #AverageReadings WHERE ISNUMERIC(AverageReading) = 1) > 0)
				BEGIN								
					UPDATE #AverageReadings
					SET IsValid = 0,
						Comments = Comments + ', Invalid Average Reading'
					WHERE AverageReading <= 0
				END
			
			INSERT INTO Tbl_AverageFailureTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #AverageReadings
			WHERE IsValid = 0
								
			SELECT @FailureTransactions = ISNULL(@FailureTransactions,0) + COUNT(0) FROM #AverageReadings WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_AverageTransactions	
			WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #AverageReadings WHERE IsValid = 0
				
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,AverageReading
				,CreatedBy
				,ReadCodeID
			INTO #ValidReadings
			FROM #AverageReadings
				
			SELECT @SuccessTransactions = COUNT(0) FROM #ValidReadings

			UPDATE DC   
			SET DC.AverageReading = Tc.AverageReading  
				,DC.ModifiedBy = TC.CreatedBy  
				,DC.ModifiedDate = dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC  
			INNER JOIN #ValidReadings TC ON TC.AccNum = DC.GlobalAccountNumber
			
			--WHERE DC.GlobalAccountNumber IN (SELECT AccNum FROM #ValidReadings)  
	    
		    UPDATE CAD   
			SET CAD.AvgReading = Tc.AverageReading
			FROM CUSTOMERS.Tbl_CustomerActiveDetails CAD
			INNER JOIN #ValidReadings TC ON TC.AccNum = CAD.GlobalAccountNumber
			AND TC.ReadCodeID=1-- For Direct CustomersOnly updating the avg reading
		     
		        
			INSERT INTO Tbl_DirectCustomersAvgReadings  
			(  
				GlobalAccountNumber  
				,AverageReading  
				,CreatedBy  
				,CreatedDate  
			)  
			SELECT TC.AccNum  
				,TC.AverageReading  
				,TC.CreatedBy  
				,dbo.fn_GetCurrentDateTime()  
			FROM Tbl_DirectCustomersAvgReadings DC 
			RIGHT JOIN #ValidReadings TC on TC.AccNum = DC.GlobalAccountNumber 
			WHERE GlobalAccountNumber IS NULL
					
			INSERT INTO Tbl_AverageSucessTransactions
			(
				 AvgUploadFileId
				,SNO
				,AccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 AvgUploadFileId
				,SNO
				,TempAccountNo
				,AverageReading
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #AverageReadings
			WHERE IsValid = 1
			
			DELETE FROM Tbl_AverageTransactions 
				WHERE AvgTransactionId IN(SELECT AvgTransactionId FROM #AverageReadings)
			
			UPDATE Tbl_AverageUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE AvgUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustInfoForAdjustmentNoBill]    Script Date: 06/15/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 8-APRIL-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 ALTER PROCEDURE [dbo].[USP_GetCustInfoForAdjustmentNoBill]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)             
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name                   
      ,A.GlobalAccountNumber AS AccountNo
      ,(A.AccountNo+' - '+A.GlobalAccountNumber) AS GlobalAccNoAndAccNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,A.OutStandingAmount
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                      
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetDirectCustomerAverageUpload]    Script Date: 06/15/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 14-Apr-2015
-- Description:	To Get the details of customer for DirectCustomerAverageUpload 
-- Modified By: Faiz-ID103
-- Modified date: 07-May-2015
-- Description:	Cluster Type & Custoemr status Fields Added 
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetDirectCustomerAverageUpload]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)=''
			,@BUID VARCHAR(50)=''
			,@ReadCodeID INT
			,@GlobalAccountNumber VARCHAR(50)

	--SET	@AccountNo= '0000057173'
	--SET @BUID='BEDC_BU_0004'   --BUnit_AP
	
		SELECT  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
				,@BUID = C.value('(BUID)[1]','VARCHAR(50)') 
		FROM @XmlDoc.nodes('DirectCustomerAverageUploadBE') as T(C)      
	
	Declare @CustomerBusinessUnitID varchar(50)
	Declare @CustomerActiveStatusID int
	
		
	Select @CustomerBusinessUnitID=BU_ID
			,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
			,@ReadCodeID=ReadCodeID
			,@GlobalAccountNumber=GlobalAccountNumber
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	
	IF @CustomerBusinessUnitID IS NULL-- Is Customer Exists 
		BEGIN
		
			SELECT 1 AS IsAccountNoNotExists
			FOR XML PATH('DirectCustomerAverageUploadBE')
		END
	ELSE -- If Custoemr exists
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID ='' -- Is custoemr belongs to same BU
				BEGIN
					IF @ReadCodeID=1 -- Check Is Customer Read Or Direct
					BEGIN
						IF	 @CustomerActiveStatusID  = 1 or @CustomerActiveStatusID=2 --IF customer Active/Inactive
							BEGIN
								 SELECT 1 AS IsSuccess 
										,@GlobalAccountNumber AS GlobalAccountNumber
										,(CD.AccountNo+' - '+@GlobalAccountNumber) AS AccNoAndGlobalAccNo
										,CD.AccountNo As AccountNo  
										,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name  
										--,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress  
										,(dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)) AS ServiceAddress       
										--,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo  
										,(CASE WHEN (ISNULL(CD.OldAccountNo,'')='') then '--' else  CD.OldAccountNo end) AS OldAccountNo
										,TC.ClassName as Tariff
										,ISNULL(DCAR.AverageReading,'0') AS AverageReading
										,CC.CategoryName AS ClusterType -- Faiz-ID103
										,CS.StatusName AS CustomerStatus-- Faiz-ID103
										,CAD.OutStandingAmount
								FROM CUSTOMERS.Tbl_CustomersDetail CD  
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
								INNER JOIN Tbl_MTariffClasses TC ON CPD.TariffClassID = TC.ClassID
								LEFT JOIN Tbl_DirectCustomersAvgReadings DCAR ON DCAR.GlobalAccountNumber= CD.GlobalAccountNumber
								LEFT JOIN MASTERS.Tbl_MClusterCategories CC ON CC.ClusterCategoryId = CPD.ClusterCategoryId -- Faiz-ID103
								JOIN Tbl_MCustomerStatus CS ON CS.StatusId = CD.ActiveStatusId -- Faiz-ID103
								JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
								WHERE (CD.GlobalAccountNumber = @GlobalAccountNumber)  
								AND CD.ActiveStatusId IN (1,2)    
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
						ELSE --IF customer Hold/Closed
							BEGIN
								--print 'Failure In Active Status Hold/Closed Customer'
								SELECT 1 AS IsHoldOrClosed
								FOR XML PATH('DirectCustomerAverageUploadBE')
							END
					END
					ELSE -- If Read Customer
					BEGIN
						--Print 'Read Customer'
						SELECT 1 AS IsReadCustomer
						FOR XML PATH('DirectCustomerAverageUploadBE')
					END		
				END
				ELSE-- IF Customer belongs to other BU
				BEGIN
					--Print 'Not Belogns to the Same BU' 
					SELECT 1 AS IsCustExistsInOtherBU
					FOR XML PATH('DirectCustomerAverageUploadBE')
				END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersExportByBookWise]    Script Date: 06/15/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 31-10-2014
-- Modified date: 02-12-2014
-- Description:	The purpose of this procedure is to get Customers export by Book wise
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomersExportByBookWise]
(
@XmlDoc xml
)
AS
BEGIN

	DECLARE @BUID VARCHAR(50)=''
		,@SUID VARCHAR(MAX)=''
		,@SCID VARCHAR(MAX)=''
		,@CycleId VARCHAR(MAX)=''
		,@BookNos VARCHAR(MAX)=''
		
		SELECT
			@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
		
		IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	select 
		 CustomerList.AccountNo
		 , CustomerList.GlobalAccountNumber
		 ,Name
		 ,OldAccountNo
		 ,MeterNumber
		 ,BusinessUnitName
		 ,ServiceUnitName
		 ,ServiceCenterName
		 ,ID AS BookCode
		 ,' ' CurrentReading
		 ,' ' AS Remarks
		 ,' ' AS PreviousReading
		 ,ServiceAddress as [Address]
		 ,BookSortOrder
		 ,SortOrder as CustomerSortOrder
		 ,InitialReading
		 ,CycleName
		 ,ClassName
	 INTO  #TotalCustomersList
	 FROM UDV_SearchCustomer   CustomerList
	 WHERE ReadCodeId=2 AND CustomerList.ActiveStatusId=1
	 AND (CustomerList.BU_ID=@BUID OR @BUID='') 
	 AND CustomerList.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,','))
	 AND CustomerList.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) 


SELECT   max(CustomerReadingId) as CurrentReadingID , CL.GlobalAccountNumber 
INTO #ReadingsList from Tbl_CustomerReadings	 CR
INNER JOIN #TotalCustomersList CL
ON CL.GlobalAccountNumber=CR.GlobalAccountNumber
GROUP BY CL.GlobalAccountNumber	  

SELECT RL.GlobalAccountNumber,CR.PresentReading as PreviousReading
INTO #ReadingsListWIthPreviousReading
FROM #ReadingsList  RL
INNER JOIN	 Tbl_CustomerReadings CR
ON		CR.CustomerReadingId=RL.CurrentReadingID

SELECT (TCL.AccountNo+' - '+TCL.GlobalAccountNumber) AS GlobalAccountNumber
	 ,Name
	 ,OldAccountNo
	 ,MeterNumber AS MeterNo
	 ,BusinessUnitName
	 ,ServiceUnitName
	 ,ServiceCenterName
	 ,BookCode AS BookNumber
	 ,' ' CurrentReading
	 ,' ' AS Remarks
	 ,TCL.[Address]
	 ,BookSortOrder
	 ,CustomerSortOrder
	 ,ClassName
	 ,CycleName
	 ,ISNULL(RL.PreviousReading,TCL.InitialReading) as PreviousReading 
	 From   #TotalCustomersList	TCL
	LEFT JOIN  #ReadingsListWIthPreviousReading	RL
	ON	 TCL.GlobalAccountNumber=RL.GlobalAccountNumber


	DROP TABLE	 #TotalCustomersList
	DROP TABLE	 #ReadingsListWIthPreviousReading
	DROP TABLE	 #ReadingsList
		
		--SELECT CD.AccountNo
		--	  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		--	  ,ISNULL(CD.OldAccountNo,'') AS OldAccountNo
		--	  ,cd.MeterNumber AS MeterNo
		--	  ,CD.BusinessUnitName
		--	  ,CD.ServiceUnitName
		--	  ,CD.ServiceCenterName
		--	  ,CD.CycleName
		--	  ,BN.BookCode
		--	  ,'' AS CurrentReading
		--	  ,'' AS Remarks
		--	  ,(SELECT TOP(1) PreviousReading FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=CD.GlobalAccountNumber
		--			ORDER BY CustomerReadingId DESC) AS PreviousReading
		--	  ,dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber) AS [Address]
		--	  ,BN.SortOrder AS BookSortOrder
		--	  ,CD.SortOrder AS CustomerSortOrder
		-- FROM [UDV_CustomerDescription] CD
		-- --JOIN Tbl_Cycles AS C ON CD.CycleId=C.CycleId
		-- JOIN Tbl_BookNumbers BN ON CD.BookNo = BN.BookNo
		-- --JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID
		-- --JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID
		-- --JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CD.ServiceCenterId
		-- --JOIN Tbl_Cycles BC ON BC.CycleId = C.CycleId
		-- WHERE ReadCodeId=2 AND CD.ActiveStatusId=1
		-- AND (CD.BU_ID=@BUID OR @BUID='')
		-- AND (CD.SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		-- AND (CD.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		-- AND (CD.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		-- --AND (CD.BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')
		-- AND (CD.BookNo IN
		--	(SELECT BookNo FROM Tbl_BookNumbers WHERE ActiveStatusId=1
		--	 AND (BU_ID=@BUID OR @BUID='')
		--	 AND (SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		--	 AND (ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		--	 AND (CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		--	 AND (BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')) OR @BookNos='')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
					--						WHERE AdjustmentLogId = @AdjustmentLogId))

					--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END			
					
					
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@ModifedBy
								 ,@ModifiedBy
								 ,GETDATE()  
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
							,CurrentApprovalLevel= 0
							,IsLocked=1
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected 
							 ,CreatedBy
							 ,ApprovedBy
							 ,CreatedDate  
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected 
							 ,@ModifiedBy
							 ,@ModifiedBy
							 ,GETDATE() 
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAddressChangeApproval]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAddressChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@AddressChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @AddressChangeLogId = C.value('(AddressChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@TempAddressId INT,
		@TempServiceId INT
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNumber 
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		FROM Tbl_CustomerAddressChangeLog_New
		WHERE AddressChangeLogId=@AddressChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId)
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
					--						WHERE AddressChangeLogId = @AddressChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					DECLARE @FinalApproval BIT

	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
			SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
					
					
					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_CustomerAddressChangeLog(  
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber AND  AddressChangeLogId= @AddressChangeLogId)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_CustomerAddressChangeLog_New 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel= CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNumber 
							AND AddressChangeLogId = @AddressChangeLogId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerAddressChangeLog_New
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel = 0
						,IsLocked = 1
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					AND  AddressChangeLogId= @AddressChangeLogId

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_CustomerAddressChangeLog(
										AddressChangeLogId,
										GlobalAccountNumber,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication
										)  
								VALUES(  
										@AddressChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_CustomerAddressChangeLog_New
								WHERE GlobalAccountNumber=@GlobalAccountNumber AND  AddressChangeLogId= @AddressChangeLogId)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedDate
									,CreatedBy)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,1
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedDate
												,CreatedBy)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
							--									WHERE AddressChangeLogId = @AddressChangeLogId))
									SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerAddressChangeLog_New 
											WHERE AddressChangeLogId = @AddressChangeLogId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerAddressChangeLog_New 
						WHERE AddressChangeLogId = @AddressChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerAddressChangeLog_New
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked = 1
			WHERE GlobalAccountNumber = @GlobalAccountNumber  
			AND AddressChangeLogId = @AddressChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForNameChangeByAccno_ByCheck]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA  (Faiz-ID103)      
-- Create date	:  09/JUNE/2015     (11-Jun-2015)  
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER NAME DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustDetForNameChangeByAccno_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
		  ,@Flag=C.value('(Flag)[1]','INT') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
			
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1 or @ActiveStatusId = 2) --//If customer existence check is successfull then get data
	 BEGIN
		IF(@Flag =1)--For CustomerName Change
			BEGIN
				IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))  
					BEGIN  
						SELECT TOP 1
								  CD.GlobalAccountNumber AS GlobalAccountNumber
								  ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
								 ,CD.AccountNo As AccountNo
								 ,CD.FirstName
								 ,CD.MiddleName
								 ,CD.LastName
								 ,CD.Title					 
								 ,CD.KnownAs
								 ,ISNULL(MeterNumber,'--') AS MeterNo
								 ,CD.ClassName AS Tariff
								 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
								,CAD.OutStandingAmount
							  FROM UDV_CustomerMeterInformation CD
							  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
							  WHERE (CD.GlobalAccountNumber=@GlobalAccountNumber)
							  AND ActiveStatusId IN (1,2)  
							  
							  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
--ELSE IF(@Flag =2)--For CustomerStatus Change 
--BEGIN
--	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber)  
--		BEGIN  
--			SELECT
--					  CD.GlobalAccountNumber AS GlobalAccountNumber
--					 ,CD.AccountNo As AccountNo
--					 ,CD.ActiveStatusId
--					 ,CD.ActiveStatus
--					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
--					 ,ISNULL(MeterNumber,'--') AS MeterNo
--					 ,CD.ClassName AS Tariff
--					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
--					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
--					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
--					 ,OutStandingAmount --Faiz-ID103
--				  FROM UDV_CustomerMeterInformation  CD
--				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
--				  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
--				  FOR XML PATH('ChangeBookNoBe')    
--		END  
--	ELSE   
--		BEGIN  
--			SELECT 1 AS IsAccountNoNotExists 
--			FOR XML PATH('ChangeBookNoBe')  
--		END
--END
--ELSE IF(@Flag =3)--For Customer MeterChange
--BEGIN
--	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
--		BEGIN  
--			SELECT TOP (1)
--				  CD.GlobalAccountNumber AS GlobalAccountNumber
--				 ,CD.AccountNo As AccountNo
--				 ,CD.ActiveStatusId
--				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
--				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
--				 ,CD.ClassName AS Tariff
--				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
--				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
--				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
--				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
--				 ,ISNULL(AverageReading,'--') As AverageUsage
--				 ,ISNULL(BT.BillingType,'--') AS LastReadType
--				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
--			  FROM UDV_CustomerMeterInformation  CD
--			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
--			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
--			  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
--			  AND ReadCodeID=2--Only Read Customers
--			  ORDER BY CustomerReadingId DESC
--				  FOR XML PATH('ChangeBookNoBe')    
--		END  
--	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber AND ReadCodeID=1)
--		BEGIN
--			SELECT 1 AS IsDirectCustomer
--			FOR XML PATH('ChangeBookNoBe')
--		END
--	ELSE  
--		BEGIN  
--			SELECT 1 AS IsAccountNoNotExists 
--			FOR XML PATH('ChangeBookNoBe')  
--		END
--END
--ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))
-- BEGIN  
--  SELECT
--	CD.GlobalAccountNumber AS AccountNo  
--     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
--     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
--     ,KnownAs
--     ,ISNULL(MeterNumber,'--') AS MeterNo
--     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
--     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
--     ,BU_ID  
--     ,SU_ID  
--     ,ServiceCenterId  
--     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
--     ,BookNo  
--     ,ActiveStatusId
--     ,MeterTypeId
--     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
--  FROM UDV_CustomerDescription  CD
--  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
--  AND ActiveStatusId IN (1,2)  
--  FOR XML PATH('ChangeBookNoBe')    
-- END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END 
	END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBook_ByCheck]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author		:  NEERAJ KANOJIYA(Faiz-ID103)        
-- Create date	:  09/MAY/2015    (11-Jun-2015)    
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER BOOK ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerBook_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND (@ActiveStatusId=1)) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT(-- 2.Fetching Customer Basic List To Display --        
		   SELECT         
			CD.GlobalAccountNumber  AS GolbalAccountNumber      
			,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)AS ContactName        
			,CD.HomeContactNo AS HomeContactNumber     
			,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo   
			,CD.BusinessContactNo AS BusinessContactNumber    
			,CD.AccountNo AS AccountNo    
			,CD.ClassName AS Tariff    
			,CD.MeterNumber    
			,CD.OldAccountNo 
			,CD.BookNo    
			,CD.BU_ID    
			,CD.SU_ID    
			,CD.ServiceCenterId AS SC_ID    
			,CD.CycleId        
			,CD.BookId + ' ('+BookCode+')' AS ExistingBook --Faiz-ID103
			--,CD.StateCode    
			,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode    
		   FROM UDV_CustomerDescription CD     
		   WHERE CD.GlobalAccountNumber=@GlobalAccountNumber    
		   FOR XML PATH('CustomerRegistrationList_1BE'),TYPE        
		 ),        
		 (         
			--3.Fetching Customer Address Details        
		   SELECT        
			CPAD.GlobalAccountNumber  AS GolbalAccountNumber      
			, HouseNo   AS HouseNoPostal      
			, StreetName     AS StreetPostal      
			, City   AS CityPostaL      
			, AreaCode   AS AreaPostal      
			, ZipCode   AS ZipCodePostal      
			, AddressID   AS PostalAddressID      
			,IsServiceAddress  AS IsServiceAddress      
			,IsCommunication      
		   FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] as CPAD
		   Join [CUSTOMERS].[Tbl_CustomerSDetail] as CSD on CSD.GlobalAccountNumber = CPAD.GlobalAccountNumber
		   WHERE CPAD.GlobalAccountNumber=@GlobalAccountNumber AND IsActive=1   -- Faiz-ID103    
		   ORDER BY IsServiceAddress ASC        
		   FOR XML PATH('CustomerRegistrationList_2BE'),TYPE        
		  )        
		FOR XML PATH(''),ROOT('CustomerRegistrationInfoByXml')
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddress_ByCheck]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author		:  NEERAJ KANOJIYA(Faiz-ID103)        
-- Create date	:  09/MAY/2015    (11-Jun-2015)    
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerAddress_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND (@ActiveStatusId=1 OR @ActiveStatusId=2)) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT(-- 2.Fetching Customer Basic List To Display --        
		   SELECT         
			CD.GlobalAccountNumber  AS GolbalAccountNumber      
			,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber)AS ContactName        
			,CD.HomeContactNo AS HomeContactNumber        
			,CD.BusinessContactNo AS BusinessContactNumber    
			,CD.AccountNo AS AccountNo    
			,CD.ClassName AS Tariff    
			,CD.MeterNumber    
			,CD.OldAccountNo 
			,CD.BookNo    
			,CD.BU_ID    
			,CD.SU_ID    
			,CD.ServiceCenterId AS SC_ID    
			,CD.CycleId    
			--,CD.StateCode    
			,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode    
		   FROM UDV_CustomerDescription CD     
		   WHERE CD.GlobalAccountNumber=@GlobalAccountNumber    
		   FOR XML PATH('CustomerRegistrationList_1BE'),TYPE        
		 ),        
		 (         
			--3.Fetching Customer Address Details        
		   SELECT        
			CPAD.GlobalAccountNumber  AS GolbalAccountNumber      
			, HouseNo   AS HouseNoPostal      
			, StreetName     AS StreetPostal      
			, City   AS CityPostaL      
			, AreaCode   AS AreaPostal      
			, ZipCode   AS ZipCodePostal      
			, AddressID   AS PostalAddressID      
			,IsServiceAddress  AS IsServiceAddress      
			,IsCommunication      
		   FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] as CPAD
		   Join [CUSTOMERS].[Tbl_CustomerSDetail] as CSD on CSD.GlobalAccountNumber = CPAD.GlobalAccountNumber
		   WHERE CPAD.GlobalAccountNumber=@GlobalAccountNumber AND IsActive=1   -- Faiz-ID103    
		   ORDER BY IsServiceAddress ASC        
		   FOR XML PATH('CustomerRegistrationList_2BE'),TYPE        
		  )        
		FOR XML PATH(''),ROOT('CustomerRegistrationInfoByXml')
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalFunctionsWithCount]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 10-Jun-2015
-- Description:	The purpose of this procedure is to get approval functions with no of approvals pending
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalFunctionsWithCount]
(
@XmlDoc XML
)
AS
BEGIN

DECLARE @BU_ID VARCHAR(20) --= 'BEDC_BU_0005'
		,@RoleId INT
		,@UserId VARCHAR(50)--='admin'

	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') AS T(C)   

SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
SELECT
	(
		SELECT
			([Function] 
			+ 			
			CASE 
			WHEN FunctionId = 1 -- Bill Adjustment approval function
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_AdjustmentsLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
			
			WHEN FunctionId = 2 -- Payment Entry
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_PaymentsLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
			
			WHEN FunctionId = 3 -- Change Customer Tariff
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_LCustomerTariffChangeRequest T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApprovalStatusId IN (1,4)
						)
						
			WHEN FunctionId = 4 -- Change Customer Book No
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_BookNoChangeLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.GlobalAccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApprovalStatusId IN (1,4)
						)
						
			--WHEN FunctionId = 5 -- Book No Disabled
			--THEN (
			--	SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
			--			FROM Tbl_LCustomerTariffChangeRequest T
			--			INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
			--			WHERE T.ApprovalStatusId IN (1,4)
			--			)
						
			WHEN FunctionId = 6 -- Change Customer Address
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_CustomerAddressChangeLog_New T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApprovalStatusId IN (1,4)
						)
						
			WHEN FunctionId = 7 -- Change Customer Meter No
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_CustomerMeterInfoChangeLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
						
			WHEN FunctionId = 8 -- Change Customer Name
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_CustomerNameChangeLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
						
			WHEN FunctionId = 9 -- Change Customer Status
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_CustomerActiveStatusChangeLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
						
			WHEN FunctionId = 10 -- Customer Type Change
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_CustomerTypeChangeLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApprovalStatusId IN (1,4)
						)
						
			WHEN FunctionId = 11 -- Assign Meter
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_AssignedMeterLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.GlobalAccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApprovalStatusId IN (1,4)
						)
						
			WHEN FunctionId = 12 -- Change Read to Direct
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_ReadToDirectCustomerActivityLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.GlobalAccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApprovalStatusId IN (1,4)
						)
						
			WHEN FunctionId = 13 -- Meter Readings
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_CustomerReadingApprovalLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
						
			WHEN FunctionId = 14 -- Direct Customer avg Upload
			THEN (
				SELECT ' ('+ Convert(VARCHAR(MAX),COUNT(0))+ ')' 
						FROM Tbl_DirectCustomersAverageChangeLogs T
						INNER JOIN [UDV_IsCustomerExists] CD ON CD.GlobalAccountNumber = T.AccountNo AND (CD.BU_ID=@BU_ID OR @BU_ID='')
						WHERE T.ApproveStatusId IN (1,4)
						)
						
			ELSE
				 ''
			END) AS [Function]
			,FunctionId
		FROM TBL_FunctionalAccessPermission FAP
		WHERE IsActive=1
		AND @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole FAR WHERE FAR.FunctionId = FAP.FunctionId  AND (BU_ID=@BU_ID OR @BU_ID=''))
		ORDER BY [Function] ASC
		FOR XML PATH('ApprovalsList'),TYPE
	)
	FOR XML PATH(''),ROOT('ApprovalsInfoByXml')
END
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateLGA]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz - ID103>      
-- Create date: <03-Jan-2015>      
-- Description: <Update LGA From AddLGA to Tbl_MLGA>      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_UpdateLGA]      
 (      
 @XmlDoc XML=null      
)      
AS      
BEGIN       
 SET NOCOUNT ON;      
 DECLARE      
       @LGAId INT       
      ,@LGAName VARCHAR(50)  
	  ,@ModifiedBy VARCHAR(50) 
	  ,@BU_ID VARCHAR(50)
          
 SELECT       
    @LGAId=C.value('(LGAId)[1]','INT')      
   ,@LGAName=C.value('(LGAName)[1]','VARCHAR(50)')    
   ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')        
 FROM @XmlDoc.nodes('LGABE') as T(C)       
       --SELECT lganame FROM Tbl_MLGA WHERE LGAId=@LGAId
 IF NOT EXISTS(SELECT 0 FROM Tbl_MLGA WHERE LGAName=@LGAName AND LGAId!=@LGAId AND BU_ID =@BU_ID)      
 BEGIN       
   UPDATE Tbl_MLGA SET LGAName = @LGAName
			,BU_ID=@BU_ID
          ,ModifiedBy = @ModifiedBy      
          ,ModifiedDate= dbo.fn_GetCurrentDateTime()        
        WHERE LGAId = @LGAId      
         
 END      
 SELECT @@ROWCOUNT As RowsEffected      
    FOR XML PATH('LGABE'),TYPE      
        
END      
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgUploadedCustoemrs]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CONVERT(INT,DAVG.AverageReading,0) AS Usage
	FROM
	UDV_PrebillingRpt CD(NOLOCK)
	INNER JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG 
	ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	and  CD.ReadCodeID = 1 
	AND CD.ActiveStatusId=1  and isnull(CD.IsPartialBook,0)=0 
	-- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertLGA]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Faiz Mohammed>  
-- Create date: <22-Dec-2014>  
-- Description: <For Insert LGA Data Into Tbl_LGA>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_InsertLGA]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @LGAName VARCHAR(50)  
   ,@CreatedBy VARCHAR(50)  
   ,@BU_ID VARCHAR(50)  
     
 SELECT   @LGAName=C.value('(LGAName)[1]','VARCHAR(50)')  
   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
     
    
   FROM @XmlDoc.nodes('LGABE') AS T(C)   
   
 IF NOT EXISTs(SELECT 0 FROM Tbl_MLGA WHERE LGAName=@LGAName AND BU_ID=@BU_ID)  
 BEGIN  
  INSERT INTO Tbl_MLGA (  
          LGAName
          ,BU_ID
          ,CreatedBy  
          ,CreatedDate  
          )  
        VALUES(  
          @LGAName
          ,@BU_ID
          ,@CreatedBy  
          ,dbo.fn_GetCurrentDateTime()  
          ) 
  END  
      
   SELECT @@ROWCOUNT AS RowsEffected  
   FOR XML PATH ('LGABE'),TYPE  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForAssignMeter]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                        
 -- Author  : NEERAJ KANOJIYA                      
 -- Create date  : 5-MARCHAR-2015                        
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.           
 -- =============================================                        
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter]                        
 (                        
 @XmlDoc xml                        
 )                        
 AS                        
 BEGIN                        
  DECLARE @GlobalAccountNumber VARCHAR(50)                      
  SELECT             
 @GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                    
 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
 
 DECLARE @LastReadingDate VARCHAR(20), @LastReading VARCHAR(50), @MeterNumber VARCHAR(50)
 
 SELECT TOP 1 @LastReadingDate = CONVERT(VARCHAR(20),ReadDate,106)
	,@LastReading = PresentReading
	,@MeterNumber = MeterNumber FROM Tbl_CustomerReadings 
 WHERE GlobalAccountNumber = @GlobalAccountNumber
 ORDER BY CustomerReadingId DESC
     
         
    SELECT     
       A.GlobalAccountNumber AS CustomerID      
      ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                       
      --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
      ,(A.AccountNo+' - '+A.GlobalAccountNumber) AS GlobalAccountNumber
      ,A.GlobalAccountNumber AS AccountNo           
      ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber        
      ,A.OldAccountNo                     
      ,RT.RouteName AS RouteName     
      ,A.RouteSequenceNo AS RouteSequenceNumber                                
      ,A.ReadCodeID AS ReadCodeID                      
      ,A.TariffId AS ClassID                           
      --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount    
      ,A.OutStandingAmount AS OutStandingAmount --Faiz-ID103
      ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials     
      ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress     
      --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)    
      ,A.CustomerTypeId AS CustomerTypeId    
      ,1 AS IsSuccessful                   
      ,@LastReadingDate AS LastReadingDate
      ,(CASE WHEN A.MeterNumber = @MeterNumber THEN ISNULL(@LastReading,A.InitialReading) ELSE A.InitialReading END) AS LastMeterReading
    FROM [UDV_CustomerDescription] AS A        
    LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID      
    WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)    
    AND A.ActiveStatusId=1                              
    FOR XML PATH('CustomerRegistrationBE'),TYPE                     
 END           

GO

/****** Object:  StoredProcedure [dbo].[USP_GetLGAList]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Faiz-ID103>  
-- Create date: <03-Jan-2015>  
-- Description: <Retriving LGA records form Tbl_MLGA>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetLGAList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @PageNo INT  
    ,@PageSize INT  
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('LGABE') AS T(C)  

  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY L.ActiveStatusId ASC , L.CreatedDate DESC ) AS RowNumber
     ,LGAId  
     ,LGAName
     ,BU.BU_ID
     ,BU.BusinessUnitName
     ,L.ActiveStatusId  
   FROM Tbl_MLGA L
   JOIN Tbl_BussinessUnits BU ON BU.BU_ID=L.BU_ID
   WHERE L.ActiveStatusId IN(1,2)  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('LGABE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('LGABeInfoByXml')   
END  
  
  
  --select * from Tbl_MLGA

GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT	
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		 ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE IF((SELECT COUNT(0)FROM Tbl_CustomerMeterInfoChangeLogs 
				WHERE NewMeterNo = @MeterNumber AND ApproveStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterChangeApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
						DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_IsMeterAvailable_New]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Modified by : Satya  
-- Modified Date: 6-APRIL-2014    
-- DESC   : MODIFIED CONDITION WITH IS NULL  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_IsMeterAvailable_New]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterNo VARCHAR(100)    
   ,@BUID VARCHAR(50)    
   ,@CustomerTypeId INT    
       
  SELECT       
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')    
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
    
  Declare @MeterAvaiable bit = 1  
		  ,@MeterDials INT
  
  IF NOT EXISTS (SELECT M.MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2  
      AND M.ActiveStatusId IN(1)   
      AND (BU_ID=@BUID OR @BUID='')        
      AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE IF EXISTS (SELECT M.NewMeterNo from Tbl_CustomerMeterInfoChangeLogs M where  M.NewMeterNo =@MeterNo   
      AND M.ApproveStatusId IN(1,4))    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE
   BEGIN
		SELECT @MeterDials=M.MeterDials FROM Tbl_MeterInformation M WHERE MeterNo=@MeterNo
   END
  SELECT @MeterAvaiable AS IsSuccess, @MeterDials AS MeterDials
  FOR XML PATH('MastersBE'),TYPE      
       
END    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBusinessUnitsList]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  <Bhimaraju Vanka>      
-- Create date: <20-FEB-2014>      
-- Description: <Purpose is To get List Of BusinessUnits>    
  
-- Updated By  : Faiz - ID103  
-- Updated Date: 08-01-20185    
-- Description: Added feilds for getting address,city, zip and Region  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBusinessUnitsList]      
(      
 @XmlDoc Xml      
)      
AS      
BEGIN      
      
 DECLARE  @PageNo INT      
   ,@PageSize INT        
          
  SELECT   @PageNo = C.value('(PageNo)[1]','INT')      
    ,@PageSize = C.value('(PageSize)[1]','INT')       
           
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
        
  ;WITH PagedResults AS      
  (      
  SELECT      
      ROW_NUMBER() OVER(ORDER BY BU.ActiveStatusId ASC , BU.CREATEDDATE DESC ) AS RowNumber      
     ,BU.BU_ID      
     ,BU.BusinessUnitName      
     ,BU.Notes      
     ,BU.CreatedDate    
     ,S.StateCode      
     --,BU.RegionId  
     ,BU.ActiveStatusId    
     ,BUCode    
     ,BU.Address1  
     ,BU.Address2  
     ,BU.City  
     ,BU.ZIP  
   FROM Tbl_BussinessUnits BU      
   JOIN Tbl_States S ON S.StateCode=BU.StateCode 
   --Join Tbl_MRegion R on R.RegionId=BU.RegionId and R.ActiveStatusId=1 --FaizID103
   WHERE S.IsActive=1    
  )      
      
    
        
   SELECT       
    (      
   SELECT  BU_ID      
     ,BusinessUnitName      
     ,CreatedDate      
     ,(CASE ISNULL(Notes,'') WHEN ''      
        THEN '---'            
        ELSE Notes      
        END) as Notes      
     ,(Select COUNT(0) from PagedResults) as TotalRecords      
     ,ActiveStatusId     
     ,StateCode      
     --,RegionId  
     --,(SELECT RegionName FROM Tbl_MRegion WHERE RegionId=p.RegionId) as RegionName  
     ,(SELECT StateName FROM Tbl_States WHERE StateCode=p.StateCode) AS StateName    
    ,ISNULL(BUCode,'--') AS BUCode    
     ,RowNumber     
     ,Address1  
     ,Address2  
     ,City  
     ,ZIP   
    FROM PagedResults p      
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
    FOR XML PATH('MastersBE'),TYPE      
  )      
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')       
 END     
 ------------------------------------------------------------------------------------------------  

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBussinessUnits]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Bhimaraju Vanka>    
-- Create date: <21-FEB-2014>    
-- Modified date: 11-JULY-2014  
-- Description: <Update Business Units From AddBusinessUnits to Tbl_BussinessUnits>    

--Modified By:  Faiz - ID103
--Modified Date: 08-Jan-2015
--Description: updated for address city, zip and Region
-- =============================================    
ALTER PROCEDURE [dbo].[USP_UpdateBussinessUnits]    
 (    
 @XmlDoc XML  
)    
AS    
BEGIN     
 SET NOCOUNT ON;    
 DECLARE    
      
      @BU_ID VARCHAR(20)     
     ,@BusinessUnitName VARCHAR(300)  
     ,@Address1 varchar(100)  
     ,@Address2 varchar(100)  
     ,@City varchar(100)  
     ,@ZIP varchar(100)
     ,@StateCode VARCHAR(20)  
     --,@RegionId INT  
  ,@Notes varchar(MAX)    
  ,@ModifiedBy VARCHAR(50)    
  ,@BUCode VARCHAR(10)    
        
 SELECT     
  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')    
    ,@BusinessUnitName=C.value('(BusinessUnitName)[1]','VARCHAR(300)')  
    ,@StateCode=C.value('(StateCode)[1]','VARCHAR(20)')  
    --,@RegionId=C.value('(RegionId)[1]','INT')  
	,@Address1=C.value('(Address1)[1]','VARCHAR(100)') 
	,@Address2=C.value('(Address2)[1]','VARCHAR(100)')
	,@City =C.value('(City)[1]','VARCHAR(100)')
	,@ZIP =C.value('(ZIP)[1]','VARCHAR(100)')    
    ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')    
       ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')    
       ,@BUCode = C.value('(BUCode)[1]','VARCHAR(10)')    
 FROM @XmlDoc.nodes('MastersBE') as T(C)    
           
 IF NOT EXISTS(SELECT 0 FROM Tbl_BussinessUnits WHERE BusinessUnitName=@BusinessUnitName AND BU_ID!=@BU_ID)    
 BEGIN  
   --IF NOT EXISTS(SELECT 0 FROM Tbl_BussinessUnits WHERE BUCode=@BUCode AND BU_ID!=@BU_ID)    
   --BEGIN  
       UPDATE Tbl_BussinessUnits SET    
       StateCode=@StateCode  
       --,RegionId=@RegionId
         ,BusinessUnitName=@BusinessUnitName    
         ,Address1=@Address1
		 ,Address2=@Address2
		 ,City =@City
		 ,ZIP =@ZIP
         ,ModifiedBy = @ModifiedBy    
         ,ModifiedDate= DATEADD(SECOND,19800,GETUTCDATE())    
         ,Notes=(CASE @Notes WHEN '' THEN NULL ELSE @Notes END)  
         --,BUCode=@BUCode    
       WHERE BU_ID = @BU_ID    
     SELECT 1 As IsSuccess    
     FOR XML PATH('MastersBE'),TYPE    
   --END  
  --ELSE  
  --    BEGIN  
  --      SELECT 1 As IsBUCodeExists  
  --    FOR XML PATH('MastersBE'),TYPE   
  --    END  
 END  
 ELSE    
   BEGIN    
  SELECT 0 As IsSuccess    
  FOR XML PATH('MastersBE'),TYPE    
   END  
END    
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBusinessUnits]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  <Bhimaraju Vanka>      
-- Create date: <20-FEB-2014>      
-- Description: <Insert Business units From AddCountries to Tbl_BussinessUnits>     
-- Modified By: Bhimaraju .V      
-- Modified date: 11-JULY-2014      
-- Description: Get BU_ID With Success Message  added BUCode Field    
-- Modified By: NEERAJ KANOJIYA     
-- Modified date: 22-DEC-2014      
-- Description: CALL AUTO GEN FUNC FOR BU CODE AS AN INPUT    
-- =============================================      
ALTER PROCEDURE [dbo].[USP_InsertBusinessUnits]      
 (      
 @XmlDoc xml      
 )      
AS      
BEGIN      
 DECLARE  @BusinessUnitName VARCHAR(300)      
   ,@Notes VARCHAR(MAX)      
   ,@CreatedBy VARCHAR(50)      
   ,@DistrictCode VARCHAR(20)      
   ,@UniqueId VARCHAR(20)    
   ,@StateCode VARCHAR(20)    
   ,@BUCode VARCHAR(10)    
   ,@Address1 VARCHAR(250)    
   ,@Address2 VARCHAR(250)    
   ,@City VARCHAR(50)    
   ,@ZIP VARCHAR(10)  
   --,@RegionId INT        
   
 SELECT   @BusinessUnitName=C.value('(BusinessUnitName)[1]','VARCHAR(300)')      
     ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')      
     ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')      
     ,@DistrictCode=C.value('(DistrictCode)[1]','VARCHAR(20)')      
     ,@StateCode=C.value('(StateCode)[1]','VARCHAR(20)')      
     ,@Address1=C.value('(Address1)[1]','VARCHAR(250)')         
     ,@Address2=C.value('(Address2)[1]','VARCHAR(250)')         
     ,@City=C.value('(City)[1]','VARCHAR(50)')         
     ,@ZIP=C.value('(ZIP)[1]','VARCHAR(10)')    
     --,@RegionId=C.value('(RegionId)[1]','INT')      
   --,@BUCode=C.value('(BUCode)[1]','VARCHAR(10)')    
   FROM @XmlDoc.nodes('MastersBE') AS T(C)     
     
 SET @BUCode=MASTERS.fn_BusinessUnitCodeGenerate(); --Temp comntd  
 SET @UniqueId=(SELECT dbo.fn_GenerateUniqueId(1));    
  
IF(@BUCode IS NOT NULL)        
BEGIN
 IF NOT EXISTs(SELECT 0 FROM Tbl_BussinessUnits WHERE BusinessUnitName=@BusinessUnitName )      
 BEGIN      
  IF(@BUCode IS NOT NULL)  
  BEGIN  
   IF NOT EXISTs(SELECT 0 FROM Tbl_BussinessUnits WHERE BUCode=@BUCode)    
   BEGIN     
     INSERT INTO Tbl_BussinessUnits (      
        BU_ID      
       ,BusinessUnitName      
       ,CreatedBy      
       ,CreatedDate      
       ,Notes      
       --,DistrictCode     
       ,StateCode    
       ,BUCode     
       ,Address1     
       ,Address2     
       ,City     
       ,ZIP  
	   --,RegionId 
       )    
  
     VALUES(      
        @UniqueId      
       ,@BusinessUnitName      
       ,@CreatedBy      
       ,DATEADD(SECOND,19800,GETUTCDATE())      
       ,CASE @Notes WHEN '' THEN NULL ELSE @Notes END      
       --,@DistrictCode     
       , @StateCode    
       --,@BUCode    
       ,@BUCode    
       ,@Address1             
       ,@Address2             
       ,@City             
       ,@ZIP 
	   --,@RegionId  
       )  
  
        SELECT 1 AS IsSuccess,@UniqueId AS BU_ID,@BUCode AS BUCode       
        FOR XML PATH ('MastersBE'),TYPE    
   END    
  ELSE    
   BEGIN    
    SELECT 1 AS IsBUCodeExists,@BUCode AS BUCode        
    FOR XML PATH ('MastersBE'),TYPE    
   END    
  END    
 END      
 ELSE      
   BEGIN      
     SELECT 0 As IsSuccess,@BUCode AS BUCode       
   FOR XML PATH('MastersBE'),TYPE      
   END    
END
END 


GO

/****** Object:  StoredProcedure [dbo].[USP_GetActiveStatesList]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Bhimaraju Vanka>  
-- Create date: <13-FEB-2014>  
-- Modified Date:14-04-2014  
-- Description: <Get States List For Binding Grid>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetActiveStatesList]  
(  
@XmlDoc Xml  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
 DECLARE  @PageNo INT  
   ,@PageSize INT    
     
 SELECT   
  @PageNo = C.value('(PageNo)[1]','INT')  
  ,@PageSize = C.value('(PageSize)[1]','INT')   
      
 FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  ( 
  SELECT  
   ROW_NUMBER() OVER(ORDER BY IsActive DESC , CreatedDate DESC) AS RowNumber  
   ,StateCode  
   ,StateName  
   ,CountryCode 
   ,RegionId 
   ,CreatedDate  
   ,DisplayCode  
   ,IsActive  
   ,Notes  
  FROM Tbl_States S  
  WHERE CountryCode=(SELECT CountryCode FROM Tbl_Countries WHERE CountryCode=S.CountryCode AND IsActive=1)  
  --AND IsActive=1  
  )   
     
  SELECT   
   (  
  SELECT  
    StateCode     
   ,CountryCode 
   ,RegionId 
   ,CreatedDate  
   ,StateName  
   ,DisplayCode  
   ,(CASE ISNULL(Notes,'') WHEN ''  
      THEN '---'        
      ELSE Notes  
      END) as Notes  
   ,(Select COUNT(0) from PagedResults) as TotalRecords  
   ,IsActive  
   ,(SELECT CountryName FROM Tbl_Countries WHERE CountryCode=p.CountryCode) AS CountryName  
   --,(SELECT LGAName FROM Tbl_MLGA WHERE LGAId=p.LGAId) AS Region  
   ,(SELECT RegionName FROM Tbl_MRegion WHERE RegionId=p.RegionId) AS RegionName
   ,RowNumber  
  FROM PagedResults p  
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
  FOR XML PATH('MastersBE'),TYPE  
 )  
 FOR XML PATH(''),ROOT('MastersBEInfoByXml')  
END  


  
  -----------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateState]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Bhimaraju Vanka>  
-- Create date: <12-FEB-2014>  
-- Description: <Update State From AddStatess to Tbl_States>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateState]  
 (  
 @XmlDoc XML=null  
)  
AS  
BEGIN   
 SET NOCOUNT ON;  
 DECLARE  
    
      @StateCode VARCHAR(20)   
     ,@StateName VARCHAR(50)       
     ,@CountryCode VARCHAR(20)
     ,@RegionId INT
     ,@DisplayCode VARCHAR(20)   
  ,@Notes varchar(100)  
  ,@ModifiedBy VARCHAR(50)  
      
 SELECT   
   @StateCode=C.value('(StateCode)[1]','VARCHAR(20)')  
   ,@StateName=C.value('(StateName)[1]','VARCHAR(50)')  
   ,@CountryCode=C.value('(CountryCode)[1]','VARCHAR(50)')  
   ,@RegionId=C.value('(RegionId)[1]','INT') 
   ,@DisplayCode=C.value('(DisplayCode)[1]','VARCHAR(50)')  
   ,@Notes=C.value('(Notes)[1]','VARCHAR(100)')  
      ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('MastersBE') as T(C)         
   
 IF NOT EXISTS(SELECT 0 FROM Tbl_States WHERE StateName=@StateName AND StateCode!=@StateCode)  
  BEGIN  
     IF NOT EXISTS(SELECT 0 FROM Tbl_States WHERE DisplayCode=@DisplayCode AND StateCode!=@StateCode)  
     BEGIN  
      UPDATE Tbl_States SET  CountryCode = @CountryCode  
			 ,RegionId=@RegionId
             ,StateCode=@StateCode  
             ,StateName=@StateName  
             ,DisplayCode=@DisplayCode  
             ,ModifiedBy = @ModifiedBy  
             ,ModifiedDate= DATEADD(SECOND,19800,GETUTCDATE())  
             ,Notes=(CASE @Notes WHEN '' THEN NULL ELSE @Notes END)  
           WHERE StateCode = @StateCode  
       SELECT 1 As IsSuccess  
       FOR XML PATH('MastersBE'),TYPE  
     END  
     ELSE  
     BEGIN  
      SELECT 1 As IsDisplayCodeExists  
      For XML PATH('MastersBE'),TYPE  
     END  
  END  
 ELSE  
  BEGIN  
    SELECT 0 As IsSuccess  
    For XML PATH('MastersBE'),TYPE  
  END   
    
END  
-------------------------------IsDisplayCodeExists----------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertStates]    Script Date: 06/15/2015 18:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Bhimaraju Vanka>
-- Create date: <13-FEB-2014>
-- Description:	<Insert States From AddStates to Tbl_States>
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertStates]
	(
	@XmlDoc XML=null
	)
AS
BEGIN
	DECLARE
	     @StateCode VARCHAR(20) 
	    ,@StateName VARCHAR(50)
	    ,@CountryCode VARCHAR(20) 
	    ,@DisplayCode VARCHAR(20)
	    ,@RegionId INT
		,@Notes varchar(100)
		,@CreatedBy VARCHAR(50)
				
	SELECT 
			--@StateCode=C.value('(StateCode)[1]','VARCHAR(20)')
			@StateName=C.value('(StateName)[1]','VARCHAR(50)')
			,@CountryCode=C.value('(CountryCode)[1]','VARCHAR(50)')
			,@DisplayCode=C.value('(DisplayCode)[1]','VARCHAR(50)')
			,@RegionId=C.value('(RegionId)[1]','INT')
			,@Notes=C.value('(Notes)[1]','VARCHAR(100)')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)	      
	
	SET @StateCode=MASTERS.fn_StateCodeGenerate()
	IF(@StateCode IS NOT NULL)
		BEGIN
			IF NOT EXISTs(SELECT 0 FROM Tbl_States WHERE DisplayCode=@DisplayCode)
				BEGIN
					IF NOT EXISTs(SELECT 0 FROM Tbl_States WHERE StateCode=@StateCode )
					BEGIN
					IF NOT EXISTs(SELECT 0 FROM Tbl_States WHERE StateName=@StateName)
						BEGIN	
							
									INSERT INTO Tbl_States(
													 StateCode
													,StateName
													,CountryCode
													,DisplayCode
													,RegionId
													,CreatedBy
													,CreatedDate
													,Notes
												) 
										values 
												(
														--1
													 @StateCode
													,@StateName
													,@CountryCode
													,@DisplayCode
													,@RegionId
													--,case @CreatedBy when NULL then 'Admin' else @CreatedBy end
													,@CreatedBy										
													,DATEADD(SECOND,19800,GETUTCDATE())
													,CASE @Notes WHEN '' THEN NULL ELSE @Notes END
													
												)
							SELECT 1 As IsSuccess
							FOR XML PATH('MastersBE'),TYPE	
					END
				ELSE
					BEGIN
						SELECT 1 As IsStateNameExists
							FOR XML PATH('MastersBE'),TYPE
					END
				END
				
				ELSE
					BEGIN
							SELECT 1 As IsStateCodeExists
							FOR XML PATH('MastersBE'),TYPE
					END
				END
				ELSE
				BEGIN
							SELECT 1 As IsDisplayCodeExists
							FOR XML PATH('MastersBE'),TYPE
			END	
		END
END


--====================================================================================================

GO

/****** Object:  StoredProcedure [dbo].[USP_GetRegionsList]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 03-01-2015
-- Description: The purpose of this procedure is to get Regions List By StateCode 
-- Modified Date: 10-June-2015
-- Description: The purpose of this procedure is to get Regions List By Country Code --functionality changed 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetRegionsList]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE @CountryCode VARCHAR(MAX)   
 SELECT @CountryCode=C.value('(CountryCode)[1]','VARCHAR(MAX)')
 FROM @XmlDoc.nodes('RegionsBE') AS T(C)

		   SELECT  
		   (  
			SELECT RegionId,RegionName
			FROM Tbl_MRegion
			WHERE CountryCode = @CountryCode
			--WHERE StateCode = @StateCode
			AND ActiveStatusId=1  
			ORDER BY RegionName ASC  
			FOR XML PATH('RegionsBE'),TYPE  
		   )  
		   FOR XML PATH(''),ROOT('RegionsBEInfoByXml')  
  END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateRegion]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz - ID103>        
-- Create date: <02-Feb-2015>        
-- Description: <Update Regions From AddRegions to Tbl_MRegions>
-- Modified By : Bhimaraju Vanka
-- Modified Date: 10-06-2015
-- Desc: Added Country Code Column and updated values design changed
-- =============================================        
ALTER PROCEDURE [dbo].[USP_UpdateRegion]        
 (        
 @XmlDoc XML=null        
)        
AS        
BEGIN         
 SET NOCOUNT ON;        
 DECLARE        
      @RegionId INT         
     ,@RegionName VARCHAR(50)    
     ,@StateCode VARCHAR(20)    
  ,@ModifiedBy VARCHAR(50)        
            
 SELECT         
    @RegionId=C.value('(RegionId)[1]','INT')        
   ,@RegionName=C.value('(RegionName)[1]','VARCHAR(50)')         
   ,@StateCode=C.value('(StateCode)[1]','VARCHAR(20)')     
   ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')        
 FROM @XmlDoc.nodes('RegionsBE') as T(C)               
 IF NOT EXISTS(SELECT 0 FROM Tbl_MRegion WHERE RegionName=@RegionName AND RegionId!=@RegionId AND CountryCode = @StateCode)        
 BEGIN         
   UPDATE Tbl_MRegion SET RegionName = @RegionName     
		  ,CountryCode=@StateCode         
          ,ModifiedBy = @ModifiedBy        
          ,ModifiedDate= dbo.fn_GetCurrentDateTime()          
        WHERE RegionId = @RegionId        
           
 END        
 SELECT @@ROWCOUNT As RowsEffected        
    FOR XML PATH('RegionsBE'),TYPE        
          
END        
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetRegions]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jan-2015>    
-- Description: <Retriving Regions records form Tbl_MRegion>    
-- MODIFIED BY:  <NEERAJ KANOJIYA-ID103>    
-- Create date: <03-APRIL-2015>    
-- Description: <LEFT JOIN ADDED>    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetRegions]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
  DECLARE  @PageNo INT    
    ,@PageSize INT    
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')     
  FROM @XmlDoc.nodes('RegionsBE') AS T(C)    
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY R.ActiveStatusId ASC , R.CreatedDate DESC ) AS RowNumber  
     ,R.RegionId    
     --,R.StateCode   
     ,C.CountryName
     ,C.CountryCode
     ,RegionName  
     ,ActiveStatusId    
   FROM Tbl_MRegion R  
   LEFT JOIN Tbl_Countries C  On C.CountryCode=R.CountryCode
   --LEFT JOIN Tbl_States S  On R.StateCode=S.StateCode 
   WHERE ActiveStatusId IN(1,2) and C.IsActive=1 
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('RegionsBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('RegionsBEInfoByXml')     
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertRegion]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz Mohammed>    
-- Create date: <22-Dec-2014>    
-- Description: <For Insert Region Data Into Tbl_MRegion> 
-- Modified By : Bhimaraju Vanka
-- Modified Date: 10-06-2015
-- Desc: Added Country Code Column and inserted values design changed
-- =============================================    
ALTER PROCEDURE [dbo].[USP_InsertRegion]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @RegionName VARCHAR(50)    
   ,@StateCode VARCHAR(20)    
   ,@CreatedBy VARCHAR(50)    
       
 SELECT     @RegionName=C.value('(RegionName)[1]','VARCHAR(50)')    
		   ,@StateCode=C.value('(StateCode)[1]','VARCHAR(20)')    
		   ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')    
       
      
   FROM @XmlDoc.nodes('RegionsBE') AS T(C)     
     
 IF NOT EXISTs(SELECT 0 FROM Tbl_MRegion WHERE RegionName=@RegionName AND CountryCode = @StateCode)    
 BEGIN    
  INSERT INTO Tbl_MRegion (    
          RegionName  
          --,StateCode    
          ,CreatedBy    
          ,CreatedDate
          ,CountryCode    
          )    
        VALUES(    
          @RegionName  
          --,@StateCode    
          ,@CreatedBy    
          ,dbo.fn_GetCurrentDateTime() 
          ,@StateCode   
          )    
              
     
  END    
      SELECT @@ROWCOUNT AS RowsEffected    
   FOR XML PATH ('RegionsBE'),TYPE    
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) --= 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) --= 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) --= 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) --= '2,3,4,5,7,8,9'
		,@PageNo INT --= 1
		,@PageSize INT --= 100  
		
	SELECT  
		 @BU_IDs = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(Months)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(Years)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerBillsBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,( CD.AccountNo  + ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress 
		,CD.MeterNumber AS MeterNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CB.NetEnergyCharges
		,CB.NetFixedCharges
		,CB.TotalBillAmount
		,CB.VAT
		,CB.VATPercentage
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CB.NetArrears
		,CB.TotalBillAmountWithArrears
		,(CASE CD.ReadCodeID WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadCode
		,CB.Usage
		,CB.PaidAmount
		,CONVERT(VARCHAR(20),CB.PaymentLastDate,106) AS PaymentLastDate
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  (NOLOCK)
	INNER JOIN Tbl_MBillingTypes BT (NOLOCK) ON CB.ReadType = BT.BillingTypeId AND
		(CB.ReadType = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN UDV_CustomerPDFReport CD (NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo 
	INNER JOIN Tbl_MTariffClasses T (NOLOCK) ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY CB.AccountNo ASC


	SELECT   
		 RowNumber 
		,GlobalAccountNumber 
		,OldAccountNo 	
		,Name      
		,ServiceAddress
		,MeterNo
		,CONVERT(VARCHAR(20),CAST(NetEnergyCharges AS MONEY),-1) AS NetEnergyCharges
		,CONVERT(VARCHAR(20),CAST(NetFixedCharges AS MONEY),-1) AS NetFixedCharges
		,CONVERT(VARCHAR(20),CAST(TotalBillAmount AS MONEY),-1) AS TotalBillAmount
		,CONVERT(VARCHAR(20),CAST(VAT AS MONEY),-1) AS VAT
		,CONVERT(VARCHAR(20),CAST(VATPercentage AS MONEY),-1) AS VATPercentage
		,BillNo    
		,TariffId  
		,TariffName  
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithTax AS MONEY),-1) AS TotalBillAmountWithTax  
		,CONVERT(VARCHAR(20),CAST(NetArrears AS MONEY),-1) AS NetArrears
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithArrears AS MONEY),-1) AS TotalBillAmountWithArrears
		,Usage
		,ReadCode
		,ReadDate  
		,PaymentLastDate
		,BillMonth  
		,BillYear   
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,BillProcessType
		,TotalRecords 
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
 

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersNoNameOrAddList]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 16-Sep-2014  
-- Description: Get The Details of The Customers With out Having No Name Or Address  
-- Modified By : Karthik  
-- Modified Date : 27-12-2014  
-- Modified By : Padmini  
-- Modified Date : 01/12/2015  
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015
-- Modified Date: 11-05-2015     
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageSize INT  
   ,@PageNo INT  
   ,@BU_ID VARCHAR(MAX)  
   ,@SU_ID VARCHAR(MAX)  
   ,@SC_ID VARCHAR(MAX)    
   ,@CycleId VARCHAR(MAX)  
   ,@BookNo VARCHAR(MAX)     
     
 SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('RptNoNameOrAddressBe') AS T(C)  
   
   
 IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SU_ID = '')  
  BEGIN  
   SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50))   
      FROM Tbl_ServiceUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SC_ID = '')  
  BEGIN  
   SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50))   
      FROM Tbl_ServiceCenter  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@CycleId = '')  
  BEGIN  
   SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50))   
      FROM Tbl_Cycles  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@BookNo = '')  
  BEGIN  
   SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50))   
      FROM Tbl_BookNumbers  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
   
 ;WITH PagedResults AS  
 (  
  SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber  
    ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
    ,(CD.AccountNo  + ' - ' + CD.GlobalAccountNumber) AS GlobalAccountNumber  
    ,CD.AccountNo AS AccountNo  
    ,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode)) As ServiceAddress  
    ,ISNULL(OldAccountNo,'--') AS OldAccountNo  
    ,CD.CycleName  
    ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber  
    ,CD.SortOrder AS CustomerSortOrder  
    ,CD.BookSortOrder  
    ,CD.BusinessUnitName  
    ,CD.ServiceUnitName  
    ,CD.ServiceCenterName  
    FROM UDV_CustomerDescription(NOLOCK) CD  
    INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
      AND CD.ActiveStatusId=1 AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName='')  
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD(NOLOCK) ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber   
      AND PAD.IsServiceAddress = 0 AND (PAD.StreetName IS NULL OR PAD.StreetName = ''   
               OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
               OR PAD.City IS NULL OR PAD.City = '')  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD(NOLOCK) ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber   
      AND SAD.IsServiceAddress = 1 AND (SAD.StreetName IS NULL OR SAD.StreetName = ''   
               OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
               OR SAD.City IS NULL OR SAD.City = '')   
 )  
   
  SELECT  
		RowNumber  
	   ,Name  
	   ,GlobalAccountNumber  
	   ,AccountNo  
	   ,ServiceAddress  
	   ,BusinessUnitName  
	   ,OldAccountNo  
	   ,CycleName  
	   ,BookNumber  
	   ,CustomerSortOrder  
	   ,BookSortOrder  
	   ,BusinessUnitName  
	   ,ServiceUnitName  
	   ,ServiceCenterName  
	   ,(Select COUNT(0) from PagedResults) as TotalRecords  
  FROM PagedResults p  
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   
END  
-------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithMeter]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptReadCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,CD.MeterNumber
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=2
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  ,MeterNumber
				  ,CONVERT(VARCHAR(20),ConnectionDate,106) AS ConnectionDate
				  --,@Count As TotalRecords 
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithoutMeter]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithoutMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptDirectCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=1
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,CONVERT(VARCHAR(20),ConnectionDate,106)AS ConnectionDate
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  --,@Count As TotalRecords
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetPaymets]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetPaymets]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPaymentsBe') AS T(C)
		
	IF ISNULL(@FromDate,'') =''    
	BEGIN    
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
	END 
	IF ISNULL(@Todate,'') =''
	BEGIN    
		SET @Todate = dbo.fn_GetCurrentDateTime()    
	END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,(CD.AccountNo + ' - ' + CD.GlobalAccountNumber) AS GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,CD.OldAccountNo
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,107),'--') AS ReceivedDate
			--,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
			,CD.OutStandingAmount AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD(NOLOCK)	
		INNER JOIN Tbl_CustomerPayments CP(NOLOCK) ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
		INNER JOIN Tbl_MBillingTypes BT(NOLOCK) ON CP.ReceivedDevice = BT.BillingTypeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		
	)
	
	SELECT	 
		 RowNumber
		,GlobalAccountNumber AS AccountNo
		,Name
		,ServiceAddress
		,BusinessUnitName
		,ServiceCenterName
		,ServiceUnitName
		,CycleName
		,BookNumber
		,CustomerSortOrder
		,BookSortOrder
		,ReceivedDevice
		,OldAccountNo
		--,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,PaidAmount
		,ReceivedDate
		--,CONVERT(VARCHAR(20),CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
		,TotalDueAmount
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(TotalDueAmount) FROM PagedResults) AS MONEY),-1) AS TotalDue
		,(Select COUNT(0) from PagedResults) as TotalRecords
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(PaidAmount) FROM PagedResults) AS MONEY),-1) AS TotalReceivedAmount
	FROM PagedResults p
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithCreditBal]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 23-Aug-2014
-- Description:	Get The Details of Credit Balance Of The Customers
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithCreditBal]
(
	@XmlDoc xml
)
AS
BEGIN

		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCreditBe') AS T(C)
	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(		
		SELECT ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber				
			,(CD.AccountNo+ ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
			--,CD.AccountNo AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
			,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount		
			,ISNULL(CONVERT(VARCHAR(50),(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)) ,106),'--') AS LastPaidDate
			,ISNULL(CAD.OutStandingAmount,0) As OverPayAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalBillsAmount(CD.GlobalAccountNumber)),0) As TotalBillsAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalPaidAmount(CD.GlobalAccountNumber)),0) As TotalPaidAmount
			,BN.BookCode AS BookCode
			,CD.OldAccountNo
			,C.CycleName
			,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
			,PD.SortOrder AS CustomerSortOrder
			,BN.SortOrder AS BookSortOrder
			,BU.BusinessUnitName
			,SU.ServiceUnitName
			,SC.ServiceCenterName
			,COUNT(0) OVER() AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			AND CAD.OutStandingAmount < 0
		INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo 
		INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId 
		INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId
		INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID 
		INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID
		INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID 		
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
	)
	
		SELECT
			 RowNumber
			,GlobalAccountNumber
			--,AccountNo
			,Name
			,ServiceAddress
			,CONVERT(VARCHAR,CAST(LastPaidAmount AS MONEY),-1) AS LastPaidAmount	
			,LastPaidDate
			,CONVERT(VARCHAR,CAST(OverPayAmount AS MONEY),-1) AS OverPayAmount
			,CONVERT(VARCHAR,CAST(TotalBillsAmount AS MONEY),-1) AS TotalBillsAmount
			,CONVERT(VARCHAR,CAST(TotalPaidAmount AS MONEY),-1) AS TotalPaidAmount
			,BookCode
			,OldAccountNo
			,CycleName
			,BookNumber 
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,CONVERT(VARCHAR,CAST((SELECT SUM(OverPayAmount) FROM PagedResults) AS MONEY),-1) AS TotalCreditBalance				
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	
END
------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomerLedgers]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Karteek
-- Create date: 09 May 2015
-- Description: The purpose of this procedure is to Get the Customers Ledger Report by account no
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomerLedgers]
(  
	@XmlDoc XML  
)  
AS  
BEGIN  
	
	Declare @AcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@ReportMonths INT
		,@CurrentDate DATETIME 
		,@BUID VARCHAR(50)=''
		,@GlobalAcountNo VARCHAR(50)

	SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()	 

	SELECT  
		  @AcountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)')   
		  ,@ReportMonths = C.value('(ReportMonths)[1]','INT') 
		  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('LedgerBe') as T(C)  
	
	SELECT @GlobalAcountNo = GlobalAccountNumber FROM [UDV_CustomerDescription](NOLOCK) CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND (BU_ID=@BUID OR @BUID='') --AND ActiveStatusId=1--Allow all customers

   IF @GlobalAcountNo IS NOT NULL
   BEGIN
			DECLARE @ResultTable TABLE(Id INT IDENTITY(1,1),CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))
			DECLARE @FinalResult TABLE(Id INT,CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))

			DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
		    
			INSERT INTO @ResultedMonths
			SELECT [YEAR],[MONTH] FROM DBO.fn_GetCurrentYearMonths_ByCurrentDate(@CurrentDate)-- ORDER BY Id DESC
									
			INSERT INTO @ResultTable (CreateDate,Particulars,Debit,Credit,Balance,Remarks)
			SELECT OpeningDate,'Opening Balance',TotalPendingAmount,0,0,''
			FROM dbo.fn_GetCustomerYear_OpeningBalance(@GlobalAcountNo)	
			

			SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
			---- Collection all the transaction Start
			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
			  BEGIN      
				SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
				
				SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
				SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

				INSERT INTO @ResultTable (CreateDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks)
				SELECT CreatedDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks FROM 
				(SELECT CONVERT(DATE,CreatedDate) AS CreatedDate
						,BillNo AS ReferenceNo
						,'Sale of energy' As Particulars
						,0 AS Debit
						,TotalBillAmountWithTax As Credit
						,0 AS Balance
						,'' As Remarks
					FROM Tbl_CustomerBills(NOLOCK)
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
					--AND BillMonth = @PresentMonth AND BillYear = @PresentYear
				UNION
					SELECT 
						CONVERT(DATE,CreatedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),BillAdjustmentId) AS ReferenceNo
						,'Adjustment' As Particulars
						,0 AS Debit
						,AmountEffected As Credit,0 AS Balance,Remarks
					FROM Tbl_BillAdjustments(NOLOCK) 
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				UNION
					SELECT CONVERT(DATE,RecievedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),CustomerPaymentID) AS ReferenceNo
						,(SELECT BillingType FROM Tbl_MBillingTypes WHERE BillingTypeId = CP.ReceivedDevice)+' Payments' As Particulars
						,PaidAmount AS Debit
						,0 As Credit
						,0 AS Balance
						,Remarks
					FROM Tbl_CustomerPayments(NOLOCK) CP
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,RecievedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				) AS A ORDER By CONVERT(DATE,A.CreatedDate)	
				  
				  IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
							IF(@MonthId IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Collection all the transaction END

			---- Finalise the transaction Records Start

			DECLARE @PresentTrans INT 
			SELECT TOP(1) @PresentTrans = Id FROM @ResultTable ORDER BY Id ASC    

			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultTable WHERE Id >= @PresentTrans ORDER BY Id ASC))    
			  BEGIN 
					
					IF NOT EXISTS(SELECT 0 FROM @FinalResult WHERE Id < @PresentTrans)
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 THEN ((0 - Debit)) 
								   WHEN Credit <> 0 THEN (0 + Credit)
								   ELSE 0 END)AS Balance
							,CreateDate
							,Remarks	   
						FROM @ResultTable WHERE Id = @PresentTrans
					END
				ELSE
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) - Debit))
								 WHEN Credit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) + Credit))
									END)AS Balance
							,CreateDate
							,Remarks		
						FROM @ResultTable WHERE Id = @PresentTrans
					END
					
				  IF(@PresentTrans = (SELECT TOP(1) Id FROM @ResultTable ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentTrans = (SELECT TOP(1) Id FROM @ResultTable WHERE  Id > @PresentTrans ORDER BY Id ASC)    
							IF(@PresentTrans IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Finalise the transaction Records End
		    
		   
				 SELECT 
					  (AccountNo + ' - ' + GlobalAccountNumber) AS GlobalAccountNumber
					  --,KnownAs AS Name
					  ,GlobalAccountNumber AS AccountNo
					  ,(SELECT dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)) AS Name
					  ,ISNULL(ISNULL(HomeContactNo,ISNULL(BusinessContactNo,OtherContactNo)),'--') AS MobileNo
					  ,ISNULL(OldAccountNo,'--') AS OldAccountNo
					  ,CD.ClassName 
					  ,ISNULL(EmailId,'--') AS EmailId
					  ,CD.BusinessUnitName
					  ,CD.ServiceUnitName
					  ,CD.ServiceCenterName
					  ,CD.BookNo
					  ,CD.CycleName
					  ,ISNULL(CD.MeterNumber,'--') AS MeterNumber
					  ,dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,
										Service_City,'',Service_ZipCode) AS ServiceAddress
					  ,1 AS IsSuccess
					  ,CONVERT(VARCHAR,CAST(ISNULL(AvgReading,0) AS DECIMAL(18,2)),-1) AS AvgReading
					  ,CONVERT(VARCHAR,CAST(OutStandingAmount AS MONEY),-1) AS OutStandingAmount
					  ,CD.ReadCodeID
					  ,ReadCode
					  ,0 AS IsCustExistsInOtherBU
			   FROM [UDV_CustomerDescription](NOLOCK) CD
			   INNER JOIN Tbl_MReadCodes(NOLOCK) RC ON RC.ReadCodeId = CD.ReadCodeID
					AND GlobalAccountNumber = @GlobalAcountNo
			
				SELECT 
					 ROW_NUMBER() OVER(ORDER BY Particulars) AS RowNumber
					,Particulars
					,CONVERT(VARCHAR,CAST(ISNULL(Debit,0) AS MONEY),-1) AS Debit
					,CONVERT(VARCHAR,CAST(ISNULL(Credit,0) AS MONEY),-1) AS Credit
					,CONVERT(VARCHAR,CAST(ISNULL(Balance,0) AS MONEY),-1) AS Balance
					,CONVERT(VARCHAR,CAST((-1*ISNULL(Balance,0)) AS MONEY),-1) As Due		
					,CONVERT(VARCHAR,CONVERT(VARCHAR(50),CreateDate,106)) AS TransactionDate
					,ReferenceNo
					,Remarks
				FROM @FinalResult 
			
	END
    ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND BU_ID!=@BUID AND ActiveStatusId=1)
    BEGIN
		
			SELECT 1 AS IsSuccess,1 AS IsCustExistsInOtherBU
	
    END
    ELSE
    BEGIN
		
			SELECT 0 AS IsSuccess
	
    END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentEditListReport]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of customers for Adjustment Report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAdjustmentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@AdjustmentTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT 
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@AdjustmentTypes=C.value('(AdjustmentName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptAdjustmentEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_BillAdjustments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@AdjustmentTypes = '')
		BEGIN
			SELECT @AdjustmentTypes = STUFF((SELECT ',' + CAST(BATID AS VARCHAR(50)) 
					 FROM Tbl_BillAdjustmentType 
					 WHERE [Status] = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT DISTINCT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, U.UserId, U.Name
		, (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, TotalAmountEffected
		, (CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
		, ISNULL(MR.Name,'Adjustment(NoBill)') AS AdjustmentName
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_BillAdjustments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.ApprovedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@AdjustmentTypes,',')) TU ON (TU.TypeId = CR.BillAdjustmentType OR CR.BillAdjustmentType IS NULL)
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.ApprovedBy
--	INNER JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		LEFT JOIN Tbl_BillAdjustmentType MR ON MR.BATID = CR.BillAdjustmentType
	  
	SELECT
	(
		SELECT RowNumber
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,UserId
			,Name AS UserName
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			--,CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1) AS Amount
			,(REPLACE(CONVERT(VARCHAR(25), CAST(TotalAmountEffected AS MONEY), 1),'-','')+Format) AS Amount
			--,(SELECT SUM(TotalAmountEffected) FROM #CustomerReadingsList) AS TotalAmount
			,AdjustmentName
			,TotalRecords
			,BusinessUnitName 
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('AdjustmentEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptAdjustmentEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPaymentEditListReport]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetPaymentEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxFrom VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxFrom=C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	IF(@BU_ID = '')
	BEGIN
		SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
				 FROM Tbl_BussinessUnits 
				 WHERE ActiveStatusId = 1
				 FOR XML PATH(''), TYPE)
				.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	IF(@TrxFrom = '')  
		BEGIN  
			SELECT @TrxFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50))   
					FROM Tbl_MPaymentType 
					WHERE ActiveStatusID = 1  
					FOR XML PATH(''), TYPE)  
					.value('.','NVARCHAR(MAX)'),1,1,'')  
		END  
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CR.CreatedDate, PT.PaymentType AS TransactionFrom
		, RecievedDate, PaidAmount
		, U.UserId, U.Name, (CD.AccountNo+' - '+CD.GlobalAccountNumber)AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerPayments CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.AccountNo
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS PaymentType FROM dbo.fn_Split(@TrxFrom,',')) SU ON SU.PaymentType = CR.PaymentType   
	INNER JOIN Tbl_MPaymentType PT ON PT.PaymentTypeID = CR.PaymentType 
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SUs ON SUs.SU_ID = SC.SU_ID
					AND SUs.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SUs.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	SELECT
	(
		SELECT RowNumber
			,CONVERT(VARCHAR(25), CAST(PaidAmount AS MONEY), 1) AS Amount
			,UserId
			,Name AS UserName
			,TransactionFrom
			,ISNULL(CONVERT(VARCHAR(20),RecievedDate,106),'--') AS PaidDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAverageUploadReport]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 26 Mar 2015
-- Description:	To get the List of Customers for Average Upload report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAverageUploadReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX) 
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX) 
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')	 
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')		 		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_DirectCustomersAvgReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		
	SELECT IDENTITY (INT, 1,1) AS RowNumber, AverageReading, CR.CreatedDate
		, U.UserId, U.Name, (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_DirectCustomersAvgReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN Tbl_UserDetails U ON U.UserId = CR.CreatedBy
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) BU ON BU.UserId = CR.CreatedBy
	  
	  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BUs ON BUs.BU_ID = SU.BU_ID 
					AND BUs.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
	SELECT
	(
		SELECT RowNumber
			,CAST(AverageReading AS INT) AS Usage
			,UserId
			,Name AS UserName
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterReadingsReport]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 25 Mar 2015
-- Description:	To get the List of customers for Meter Reading report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetMeterReadingsReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@TrxTypes VARCHAR(MAX)
			,@FromDate DATE
			,@ToDate DATE
			,@PageSize INT  
			,@PageNo INT  
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)  
			,@SC_ID VARCHAR(MAX)    
			,@CycleId VARCHAR(MAX)  
			,@BookNo VARCHAR(MAX)   
		
	SELECT
		 @Users=C.value('(UserName)[1]','VARCHAR(MAX)')
		,@TrxTypes=C.value('(MeterReadingFrom)[1]','VARCHAR(MAX)')
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(15)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  	
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')		
	FROM @XmlDoc.nodes('RptMeterReadingBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerReadings 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
   
	IF(@TrxTypes = '')
		BEGIN
			SELECT @TrxTypes = STUFF((SELECT ',' + CAST(MeterReadingFromId AS VARCHAR(50)) 
					 FROM MASTERS.Tbl_MMeterReadingsFrom 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT IDENTITY (INT, 1,1) AS RowNumber, CustomerReadingId, ReadDate, PreviousReading, CR.CreatedDate
		, CR.PresentReading, Usage, AverageReading, U.UserId, UD.Name, MR.MeterReadingFromId, MR.MeterReadingFrom
		, (CD.AccountNo+' - '+ CD.GlobalAccountNumber) AS GlobalAccountNumber, CD.OldAccountNo, CD.CycleName
		, CustomerFullName AS CustomerName
		, ServiceAddress AS ServiceAdress
		, CD.BusinessUnitName
		, COUNT(0) over () AS TotalRecords
	INTO #CustomerReadingsList
	FROM Tbl_CustomerReadings CR
	INNER JOIN UDV_EditListReport CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
		AND CONVERT (DATE,CR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		AND CD.BU_ID = @BU_ID
	INNER JOIN (SELECT [com] AS UserId FROM dbo.fn_Split(@Users,',')) U ON U.UserId = CR.CreatedBy
    INNER JOIN (SELECT [com] AS TypeId FROM dbo.fn_Split(@TrxTypes,',')) TU ON TU.TypeId = CR.MeterReadingFrom 
	INNER JOIN Tbl_UserDetails UD ON UD.UserId = CR.CreatedBy
	INNER JOIN MASTERS.Tbl_MMeterReadingsFrom MR ON MR.MeterReadingFromId = CR.MeterReadingFrom
	
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		
		INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo  
					AND BN.BookNo IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@BookNo,','))   
		INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
					AND C.CycleId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@CycleId,','))
		INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
					AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
		INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID
					AND SU.SU_ID IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
		INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
					AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
					
	SELECT
	(
		SELECT RowNumber
			,CustomerReadingId
			,PreviousReading
			,PresentReading
			,CAST(Usage AS INT) AS Usage
			,CAST(AverageReading AS VARCHAR(50)) AS AverageReading
			,UserId
			,Name AS UserName
			,MeterReadingFromId
			,MeterReadingFrom
			,ISNULL(CONVERT(VARCHAR(20),ReadDate,106),'--') AS ReadDate
			,ISNULL(CONVERT(VARCHAR(20),CreatedDate,106),'--') AS TransactionDate
			,TotalRecords 
			,GlobalAccountNumber
			,OldAccountNo
			,CycleName AS BookGroup
			,CustomerName
			,ServiceAdress AS ServiceAdress
			,BusinessUnitName
		FROM #CustomerReadingsList
		WHERE  RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
		FOR XML PATH('RptMeterReadingsList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptMeterReadingsInfoByXml')
	
	DROP TABLE	#CustomerReadingsList
	
END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNoBillsCustomers]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- PrepaidCustomers
	SET	@BookDisableType = '1'

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
	SELECT   
		(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress AS ServiceAddress
		,
		CASE
		when   CustomerStatus.CustomerStatusID=3 then convert(varchar(max),'Hold Customer') else
		case when CustomerStatus.CustomerStatusID=4 then convert(varchar(max),'Closed Customer') 
		else convert(varchar(max),'Pre-Paid Customer')
		end
		END
		AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	INTO  #NoBillCustomersList
	FROM  UDV_PrebillingRpt(NOLOCK) CD	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
 	INNER JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus 
 	ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
	LEFT JOIN Tbl_MeterInformation(NOLOCK) MI ON CD.MeterNumber = MI.MeterNo and MI.MeterType = 1
	WHERE
	  (CustomerStatus.CustomerStatusID IS NUll OR  MI.MeterType IS NULL)
	
	insert into #NoBillCustomersList 
	SELECT  
		(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,CD.BusinessUnitName,
		CD.ServiceUnitName,CD.ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress  AS ServiceAddress
		,'BookDisable No Power' AS Comments
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
 
	FROM UDV_PrebillingRpt(NOLOCK) CD
	INNER JOIN Tbl_BillingDisabledBooks(NOLOCK) BD ON ISNULL(CD.BookNo,0) = BD.BookNo
	AND BD.DisableTypeId IN(SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,','))
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID	--INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId     
  SELECT *		 FROm #NoBillCustomersList
END
--------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetPartialBillCustomers]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(max)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(max)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(max)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(max)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
 	Create table #PartialBillCustomers 
 	(
 	 GlobalAccountNumber varchar(50)
 	,OldAccountNo varchar(50)
 	,MeterNo    varchar(50)
 	,ClassName  varchar(50)
 	,Name    varchar(MAX)
 	,ServiceAddress	 varchar(MAX)
 	,BusinessUnitName  varchar(200)
 	,ServiceUnitName	varchar(200)   
 	,ServiceCenterName varchar(200)
 	,CustomerSortOrder int
 	,CycleName	  varchar(100)
 	,BookNumber	 varchar(100)    
 	,BookSortOrder int 
 	,Comments varchar(MAX)   
 	)
 	
 insert into #PartialBillCustomers
	SELECT	    
		(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		 ,CustomerFullName AS Name 
	 	,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
	 	,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt(NOLOCK) CD
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
 	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	
	
	 
	insert into #PartialBillCustomers
	SELECT 
		 (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
	 
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 
	AND BD.IsPartialBill = 1
 
	
	SELECT * from #PartialBillCustomers
	
	DROP table #PartialBillCustomers
END
---------------------------------------------------------------------------------------------




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT Top 1 CBILLS.AccountNo,CBILLS.AverageReading as  LastBillAverage,CBILLS.Usage, 
	CD.GlobalAccountNumber,CD.AccountNo AS AccNo,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
		 INTO #CustomerBillswithAverage
		 from   Tbl_CustomerBills(NOLOCK)	CBILLS
	INNER JOIN  UDV_PreBillingRpt(NOLOCK) CD ON CBILLS.AccountNo = CD.GlobalAccountNumber 
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
		
	
	SELECT (CD.AccNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.Name 
		,CD.ServiceAddress AS ServiceAddress
		,MIN(CR.PreviousReading) AS PreviousReading 
		,MAX (CR.ReadDate) AS PreviousReadDate
		,MAX(CR.PresentReading) AS	 PresentReading
		,CAST(SUM(CR.usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(CR.CreatedDate) AS LatestTransactionDate
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CustomerSortOrder 
		,CD.CycleName
		,BookNumber
		,CD.BookSortOrder 
	FROM Tbl_CustomerReadings CR
	INNER JOIN #CustomerBillswithAverage CD ON	   CD.GlobalAccountNumber=CR.GlobalAccountNumber
	GROUP BY CD.GlobalAccountNumber,CD.OldAccountNo,
		CD.MeterNumber,CD.ClassName,
		CD.Name,
		CD.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CD.BookSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
		, CD.Usage
		,CustomerSortOrder
		,CD.AccNo 
    HAVING 
	CAST(SUM(CR.usage) AS DECIMAL(18,2)) 
	between   
	 CD.Usage- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	 CD.Usage +((30*CONVERT(Decimal(18,2),AverageReading)/100))
    
    DELETE FROM		 #CustomerBillswithAverage
 	
END
----------------------------------------------------------------------------------------------------
 




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
	FROM
	UDV_PrebillingRpt(NOLOCK)  CD
	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID   and  CD.ReadCodeID = 1	AND CD.ActiveStatusId=1  
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	 AND  isnull(CD.BoolDisableTypeId,0)=0

	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	WHERE isnull(DAVG.AvgReadingId,0)=0 
       -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT  (CD.AccountNo+' - '+ CD.GlobalAccountNumber) AS GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			CD.BusinessUnitName,
			CD.ServiceUnitName,
			CD.ServiceCenterName,
			CD.CycleName,
			(CD.BookId + ' - ' + CD.BookCode) AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD(NOLOCK)
	Inner join
	Tbl_CustomerReadings(NOLOCK)  CR
	ON CR.GlobalAccountNumber=CD.GlobalAccountNumber
	and IsBilled=0 and Usage=0
 	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
 	
END
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNonReadCustomers]    Script Date: 06/15/2015 18:22:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''
			 

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

SELECT	(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,CD.InitialBillingKWh AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON CD.ServiceCenterId =Service_Centers.SC_ID
	AND ActiveStatusId=1  and isnull(IsPartialBook,1)= 1
	and CustomerTypeId <>3
	AND CD.ReadCodeID = 2 
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON
	CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE   CR.CustomerReadingId IS NULL 
	
END
--------------------------------------------------------------------------------------------






GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetReadCustomers]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

Modified Date : 09-04-2015

Description : 
In Active,Closed,Hold,Tempary CLose  Customers ,DIRECT Customer , NoPower Also- No Need to Come

 Hold -- 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

 
	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
			,@ActiveStatusIds varchar(max)='2,3,4'
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.AccountNo,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS BIGINT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder
		,CR.CustomerReadingId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings(NOLOCK) CR
	INNER JOIN UDV_PreBillingRpt(NOLOCK) CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	AND ActiveStatusId=1  and isnull(IsPartialBook,1)= 1-- Disable books and	   Active Customers
	and CD.CustomerTypeId <>3
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID 
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy
    and ISNULL(CD.IsPartialBook,1)=1

	SELECT (Tem.AccountNo+' - '+Tem.GlobalAccountNumber) AS GlobalAccountNumber
		,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,(select top 1 PreviousReading from #ReadCustomersList RL where RL.GlobalAccountNumber=Tem.GlobalAccountNumber order by CustomerReadingId ASC) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,(select top 1 PresentReading from #ReadCustomersList RL where RL.GlobalAccountNumber=Tem.GlobalAccountNumber order by CustomerReadingId Desc) AS PresentReading 
 		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
		,Tem.AccountNo
	DROP TABLE #ReadCustomersList		 

END
----------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_GetPagePermisions]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz-ID103>      
-- Create date: <09-JAN-2015>      
-- Description: <Retriving Master and Child page list for Role Permisions.>      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetPagePermisions] 
(
@XmlDoc XML=null
)
AS      
 BEGIN     
 DECLARE	@AccessLevelID INT
			,@RoleId INT 

 SELECT     
	@AccessLevelID = C.value('(AccessLevelID)[1]','INT') 
	,@RoleId = C.value('(RoleId)[1]','INT') 
	FROM @XmlDoc.nodes('AdminBE') AS T(C)    
 
 Select (    
 --List for Master Menu Item    
     
     
   SELECT [MenuId]    
      ,[Name]    
      --,[Path]    
      ,[Menu_Order]    
      ,[ReferenceMenuId]    
      --,[Icon1]    
      --,[Icon2]    
      ,[Page_Order]    
      ,[IsActive]    
      --,[ActiveHeader]    
      --,[Menu_Image]    
   FROM Tbl_Menus WHERE Menu_Order IS NOT NULL AND IsActive= 1 ORDER BY Menu_Order    
   FOR XML PATH('AdminListBE'),TYPE    
    ),    
    (    
 --List for Child Menu Item    
    SELECT [MenuId]    
      ,[Name]    
      --,[Path]    
      ,[Menu_Order]    
      ,(CASE [ReferenceMenuId] WHEN 122 THEN 7 ELSE [ReferenceMenuId] END) AS [ReferenceMenuId]  
      --,[Icon1]    
      --,[Icon2]    
      ,[Page_Order]    
      ,[IsActive]    
      --,[ActiveHeader]    
      --,[Menu_Image]    
      ,ISNULL(Tbl_Menus.AccessLevelID,0) AS AccessId  
   FROM Tbl_Menus   
  LEFT JOIN Tbl_MMenuAccessLevels ON Tbl_Menus.AccessLevelID = Tbl_MMenuAccessLevels.AccessLevelID  
   WHERE Menu_Order IS NULL  
   AND IsActive= 1 AND Tbl_Menus.AccessLevelID >= @AccessLevelID
	ORDER BY ReferenceMenuId    
   FOR XML PATH('AdminListBE_1'),TYPE    
   )    
   FOR XML PATH(''),ROOT('AdminBEInfoByXml')          
 END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForPaidMeters_ByAccno]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For Paid Meter Adjustments
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForPaidMeters_ByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)  
 SELECT    
   @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('PaidMeterBe') as T(C)    
     
 IF EXISTS (SELECT 0 FROM [UDV_CustomerDescription] C JOIN Tbl_PaidMeterDetails P ON P.AccountNo=C.GlobalAccountNumber WHERE C.GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo )
 BEGIN  
  SELECT    P.AccountNo
		   ,P.MeterNo
		   ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS AccNoAndGlobalAccNo
		   ,P.MeterCost
		   ,P.OutStandingAmount
		   ,(SELECT dbo.fn_GetCustomerFullName(P.AccountNo)) AS Name
		   ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
		   ,CD.ClassName AS Tariff
		   ,ISNULL((SELECT SUM(AdjustedAmount) FROM Tbl_PaidMeterAdjustmentDetails WHERE AccountNo=P.AccountNo),0) AS PaidAmount 
		  ,ISNULL(( SELECT SUM(ISNULL(Amount,0)) FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=P.AccountNo) ,0) AS BilladjustmentAmount 
	FROM Tbl_PaidMeterDetails P
	JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber=P.AccountNo
	WHERE (CD.GlobalAccountNumber=@AccountNo  OR CD.OldAccountNo=@AccountNo)
  FOR XML PATH('PaidMeterBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsNotPaidCustomer FOR XML PATH('PaidMeterBe')  
 END  
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmount - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  Bhimaraju V 
-- Create date: 12-03-2014 5 
-- Description: Get Details For PaymentEntry-AccountWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustDetailsForPaymentEntry_AccountWise]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)
	
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('PaymentsBe') as T(C)	

	SELECT CD.GlobalAccountNumber
			,CD.OldAccountNo
			,(CD.AccountNo+' - '+CD.GlobalAccountNumber)AS AccNoAndGlobalAccNo
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			,MeterNumber
			,ClassName
			,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName    
			 ,CD.Service_Landmark    
			 ,CD.Service_City,''    
			 ,CD.Service_ZipCode) AS ServiceAddress 
			,OutStandingAmount
			,ISNULL(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber),'--') AS LastPaidDate
			,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS LastPaidAmount
			,ISNULL(dbo.fn_GetCustomerLastBillGeneratedDate(CD.GlobalAccountNumber),'--') AS LastBillGeneratedDate		
			,dbo.fn_GetCustomerLastBillAmountWithArrears_New(CD.GlobalAccountNumber) AS LastBillAmount
			,1 AS IsExists
	FROM UDV_CustomerDescription CD
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail C ON C.GlobalAccountNumber=CD.GlobalAccountNumber
	WHERE (CD.GlobalAccountNumber = @AccountNo OR CD.OldAccountNo = @AccountNo) AND C.ActiveStatusId in(1,2,3)
	FOR XML PATH('PaymentsBe')
	 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
        
  DECLARE @GlobalAccountNumber VARCHAR(50)  
 
	SET @GlobalAccountNumber =(SELECT GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail 
			WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo)

	IF( @GlobalAccountNumber IS NOT NULL)
		BEGIN
			IF ((SELECT [dbo].[fn_IsAccountNoExists_BU_Id_Payments](@GlobalAccountNumber,@BU_ID)) = 1)
				BEGIN
						SELECT  
						 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
						,CD.OldAccountNo--Faiz-ID103
						,CD.ClassName
						,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS AccNoAndGlobalAccNo
						,CD.OutStandingAmount AS DueAmount  --Commented By Raja-ID065
						,CD.GlobalAccountNumber AS AccountNo
						,'success' AS StatusText
						,(SELECT CustomerType FROM Tbl_MCustomerTypes CT WHERE CT.CustomerTypeId=CD.CustomerTypeId) AS CustomerType
						,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,NULL,CD.Service_ZipCode) AS Address1
					FROM [UDV_CustomerDescription] CD
					WHERE GlobalAccountNumber = @GlobalAccountNumber AND (BU_ID = @BU_ID OR @BU_ID = '')
					FOR XML PATH('MastersBE')  
				END
			ELSE
				BEGIN
					SELECT 'Customer doesn''t exist in selected Bussiness Unit' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
		END
	ELSE
		BEGIN
			SELECT 'Customer doesn''t exist' AS StatusText FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForBillGen]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 25-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- Create date  : 8-APRIL-2015                    
 -- Description  : REMOVE ACTIVE STATUS ID FILTER
 
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForBillGen]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID  
				  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                   
				  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo
				  ,(A.AccountNo+ ' - '+ A.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,A.OutStandingAmount AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful  
				  ,A.ServiceUnitName 
				  ,A.ServiceCenterName
				  ,A.CycleName
				  --,A.BookNo      
				  ,A.OldAccountNo  
				  ,A.BookId AS BookNo
		  FROM [UDV_CustomerDescription] AS A    
		  LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				--AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@CustomerTypeId INT
			,@OldAccountNo VARCHAR(50)
			,@AccNoAndGlobalAccNo VARCHAR(100)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount
			,@AccNoAndGlobalAccNo=(AccountNo+' - '+GlobalAccountNumber)
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@CustomerTypeId=CustomerTypeId
			,@OldAccountNo=OldAccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC, CustomerPaymentID  DESC
	
	
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
		,RC.DisplayCode AS ReadType
		,CB.BillYear        
		,CB.BillMonth        
		,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
		,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		
		,Usage AS Consumption
		,Dials AS MeterDials
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT 
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 		
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,CB.AccountNo AS AccountNo
		,@Name AS Name
		,@AccNoAndGlobalAccNo AS AccNoAndGlobalAccNo
		,@ServiceAddress AS ServiceAddress
		,@OutstandingAmount AS OutStandingAmount
		,@BookGroup AS BookGroup 
		,@BookName AS BookName
		,@Tariff AS Tariff
		,@OldAccountNo AS OldAccountNo
		,COUNT(0) OVER() AS TotalRecords
		,VAT AS Vat
		,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS ClassName
		,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=@CustomerTypeId) AS CustomerType
		
	FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
	WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
	ORDER BY CB.CustomerBillId DESC
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     


GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 08-JAN-2014      
-- Description: The purpose of this procedure is to get list of MeterInformation      
-- Modified By:  Faiz-ID103  
-- Modified date: 08-JAN-2014      
-- Description: Added Business Unit name and Meter Dials field for new customer registration page  
  
-- =============================================      
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @MeterNo VARCHAR(100)      
   ,@MeterSerialNo VARCHAR(100)      
   ,@BUID VARCHAR(50)      
   ,@CustomerTypeId INT      
         
  SELECT         
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')      
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')      
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')      
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')      
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  IF (@CustomerTypeId=3)      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType     
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo      
       ,BU.BusinessUnitName    
       ,MeterDials  
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID WHERE M.ActiveStatusId IN(1) AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
				WHERE MeterNumber !='' )      
      AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))       
      AND M.MeterType=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
  ELSE      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType      
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo       
       ,MeterDials  
       ,BU.BusinessUnitName    
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID    
      WHERE M.ActiveStatusId IN(1)      
      AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT DISTINCT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
							WHERE ISNULL(MeterNumber,'') !='' )      
        AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))   
      AND M.MeterType !=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
         
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- Modified By:  V.Bhimaraju  
-- Modified date: 07-May-2015   
-- Description: Added outstanding amount in CustomerStatus Change (Flag=2)
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 ,CAD.OutStandingAmount
				  FROM UDV_CustomerMeterInformation CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,CAD.OutStandingAmount --Faiz-ID103
					 --,dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber) AS LastTransactionDate --Faiz-ID103
					 ,CONVERT(VARCHAR(20),dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber),103) AS LastTransactionDate1 --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate		
				 ,dbo.fn_GetCustomer_ADC(Usage) AS AverageDailyUsage
				 ,CAD.OutStandingAmount --Faiz-ID103		 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
     ,CD.OutStandingAmount --Faiz-ID103
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchListEditListReport]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the List of Batches for Payment report
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchListEditListReport]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @Users VARCHAR(MAX) 
			,@FromDate DATE
			,@ToDate DATE
			--,@PageSize INT  
			--,@PageNo INT  
			,@BU_ID VARCHAR(50)
			,@TransactionFrom VARCHAR(MAX) 
		
	SELECT
		 @Users = C.value('(UserName)[1]','VARCHAR(MAX)')
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(15)')
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(15)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')		
		,@TransactionFrom = C.value('(TransactionFrom)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
	
	IF(@Users = '')
		BEGIN
			SELECT @Users = STUFF((SELECT ',' + CreatedBy
					 FROM Tbl_CustomerPayments 
					 GROUP BY CreatedBy 
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TransactionFrom = '')
		BEGIN
			SELECT @TransactionFrom = STUFF((SELECT ',' + CAST(PaymentTypeID AS VARCHAR(50)) 
					 FROM Tbl_MPaymentType
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	SELECT
	(
		SELECT 
			 ISNULL(CONVERT(VARCHAR(20),BD.CreatedDate,106),'--') AS TransactionDate
			,BD.BatchNo 
			,BD.BatchID
			,ISNULL(CONVERT(VARCHAR(20),BD.BatchDate,106),'--') AS PaidDate
			,(CASE WHEN CP.PaymentType = 2 
				THEN CONVERT(VARCHAR(25), CAST(SUM(ISNULL(CP.PaidAmount,0)) AS MONEY), 1)
				ELSE CONVERT(VARCHAR(25), CAST(BD.BatchTotal AS MONEY), 1) END) AS Amount
			,U.Name AS UserName
			,CONVERT(VARCHAR(25), CAST(SUM(ISNULL(CP.PaidAmount,0)) AS MONEY), 1) AS PaidAmount
			,(CASE WHEN ISNULL(CP.BatchNo,0) = 0 THEN 0 ELSE COUNT(0) END) AS TotalRecords -- Total Customers
		FROM Tbl_BatchDetails BD
		INNER JOIN Tbl_UserDetails U ON U.UserId = BD.CreatedBy 			 
				AND BD.CreatedBy IN(SELECT [com] AS UserId FROM dbo.fn_Split(@Users,','))
				AND CONVERT (DATE,BD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
				AND BD.BU_ID = @BU_ID 
		LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchID 
		WHERE BD.BatchStatus = 2 AND CP.PaymentType IN (SELECT [com] AS UserId FROM dbo.fn_Split(@TransactionFrom,','))
		GROUP BY BD.CreatedDate,BD.BatchNo,BD.BatchDate,BD.BatchTotal
				,U.Name,CP.BatchNo,BD.BatchID,CP.PaymentType
		--HAVING SUM(CP.PaidAmount) >= BD.BatchTotal
		
		FOR XML PATH('RptPaymentsEditList'),TYPE
	)
	FOR XML PATH(''),ROOT('RptPaymentsEditListInfoByXml')
	
END
------------------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno_ByCheck]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  09/JUNE/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
		  ,@Flag=C.value('(Flag)[1]','INT') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
			
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1) --//If customer existence check is successfull then get data
	 BEGIN
		IF(@Flag =1)--For CustomerName Change
			BEGIN
				IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))  
					BEGIN  
						SELECT TOP 1
								  CD.GlobalAccountNumber AS GlobalAccountNumber
								 ,CD.AccountNo As AccountNo
								 ,CD.FirstName
								 ,CD.MiddleName
								 ,CD.LastName
								 ,CD.Title					 
								 ,CD.KnownAs
								 ,ISNULL(MeterNumber,'--') AS MeterNo
								 ,CD.ClassName AS Tariff
								 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
							  FROM UDV_CustomerMeterInformation CD
							  WHERE (CD.GlobalAccountNumber=@GlobalAccountNumber)
							  AND ActiveStatusId IN (1,2)  
							  
							  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,OutStandingAmount --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate				 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber AND ReadCodeID=1)
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId IN(1,2))
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
  FROM UDV_CustomerDescription  CD
  WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END 
	END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_ClosePaymentUploadBatch]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 27-Apr-2015
-- Description:	This pupose of this procedure is to Close Payment Upload Batch record
-- =============================================
ALTER PROCEDURE [dbo].[USP_ClosePaymentUploadBatch] 
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @PUploadBatchId INT
			,@LastModifyBy VARCHAR(50)   
			,@BatchID INT
		
	SELECT       
		 @PUploadBatchId = C.value('(PUploadBatchId)[1]','INT')  
		,@LastModifyBy = C.value('(LastModifyBy)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('PaymentsBatchBE') AS T(C)    
	
	SELECT @BatchID = BD.BatchID 
	FROM Tbl_BatchDetails BD 
	INNER JOIN Tbl_PaymentUploadBatches PUB ON PUB.BatchNo = BD.BatchNo 
			AND PUB.BatchDate = BD.BatchDate AND PUB.BU_ID = BD.BU_ID
			AND PUB.PUploadBatchId = @PUploadBatchId
	
	UPDATE Tbl_PaymentUploadBatches 
	SET IsClosedBatch = 1
	WHERE PUploadBatchId = @PUploadBatchId
	
	UPDATE Tbl_BatchDetails 
	SET BatchStatus = 2
	WHERE BatchID = @BatchID
	
	SELECT 1 AS RowsEffected
	FOR XML PATH('PaymentsBatchBE'),TYPE      
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml    
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,BookCode  
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,CAD.InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  ,CD.AccountNo --Faiz-ID103
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchDetails_ByBatchNo]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 05 Apr 2015
-- Description:	To get the Batch details
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchDetails_ByBatchNo]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @BatchNo INT
		
	SELECT
		 @BatchNo=C.value('(BatchNo)[1]','INT')		
	FROM @XmlDoc.nodes('RptPaymentsEditListBE') as T(C)
		
	SELECT 
		 BD.BatchNo
		,ISNULL(CONVERT(VARCHAR(20),BatchDate,106),'--') AS PaidDate
		,CONVERT(VARCHAR(25), CAST(BatchTotal AS MONEY), 1) AS Amount
		,COUNT(0) AS TotalRecords
		,CONVERT(VARCHAR(25), CAST(SUM(CP.PaidAmount) AS MONEY), 1) AS PaidAmount
	FROM Tbl_BatchDetails BD
	LEFT JOIN Tbl_CustomerPayments CP ON CP.BatchNo = BD.BatchID --AND CP.PaymentType = 1
	WHERE BD.BatchID = @BatchNo
	GROUP BY BD.CreatedDate,BD.BatchNo,BD.BatchDate,BD.BatchTotal
	FOR XML PATH('RptPaymentsEditListBE')
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingLogs]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04 may 2015
-- Description: TO insert the customer meter readings into the logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingLogs]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
		
		,@IsFinalApproval BIT
		,@FunctionId INT
		,@ActiveStatusId INT
		,@IsExists BIT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
		,@IsFinalApproval=C.value('(IsFinalApproval)[1]','bit')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')
		,@IsExists = C.value('(IsExists)[1]','BIT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	DECLARE @RoleId INT, @CurrentLevel INT
			,@PresentRoleId INT, @NextRoleId INT
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level			
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
		
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
					BEGIN
							SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																		CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																				ELSE NULL END
																	END
																	 FROM TBL_FunctionApprovalRole 
																	WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
					END
				ELSE
					BEGIN
						
						SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					END
					
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				ELSE
				BEGIN
					SET @CurrentApprovalLevel = @CurrentLevel+1
				END 
										
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
	
		END
	ELSE
		BEGIN
			SET @IsFinalApproval=1
			SET @PresentRoleId=0
			SET @NextRoleId=0
			SET @CurrentLevel=0	
			SET @CurrentApprovalLevel =0
		END
			
	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum
	
	IF(@IsExists = 1)
		BEGIN
			DECLARE @ReadId INT
			Declare @IsExistingRollOver bit
			
			SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
			FROM Tbl_CustomerReadingApprovalLogs
			WHERE GlobalAccountNumber = @AccNum
			ORDER BY CustomerReadingLogId DESC
				 
			IF @IsExistingRollOver = 1
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
			END
			ELSE
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
			END	
		END
	
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	 
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  --SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @SecountReadingUsage=convert(bigint,@Current+1) 
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);		  
		END
		
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
	
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)	
			VALUES(
				 @AccNum
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
				,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,@IsRollover
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				

			IF(@IsRollover=1)
				BEGIN
		 
					--IF @SecountReadingUsage != 0
					--	BEGIN
						--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
						 --IF	 @SecountReadingUsage > 0
										 INSERT INTO Tbl_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole
											,CurrentApprovalLevel
											,IsLocked)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
											
						--END
				END
				
		IF(@IsFinalApproval = 1)
			BEGIN		
			
				INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver
					,ApproveStatusId
					,PresentApprovalRole
					,NextApprovalRole)	
				VALUES(
					@AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover
					,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)	
				
				INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver)
				VALUES(
					 @AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover)
					

				IF(@IsRollover=1)
					BEGIN
			 
						--IF @SecountReadingUsage != 0
						--	BEGIN
							--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
							 --IF	 @SecountReadingUsage > 0
								-- BEGIN
									INSERT INTO Tbl_CustomerReadings(
												 GlobalAccountNumber
												,[ReadDate]
												,[ReadBy]
												,[PreviousReading]
												,[PresentReading]
												,[AverageReading]
												,[Usage]
												,[TotalReadingEnergies]
												,[TotalReadings]
												,[Multiplier]
												,[ReadType]
												,[CreatedBy]
												,[CreatedDate]
												,[IsTamper]
												,MeterNumber
												,[MeterReadingFrom]
												,IsRollOver)
											VALUES(
												 @AccNum
												,@ReadDate
												,@ReadBy
												,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
												,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
												,@AverageReading
												,@SecountReadingUsage
												,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
												,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
												,@Multiple
												,2
												,@CreateBy
												,GETDATE()
												,@IsTamper
												,@MeterNumber
												,@MeterReadingFrom
												,@IsRollover)
												
										INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)
								END												
							--END
					--END
					
			 
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
				SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
					,AvgReading = CONVERT(NUMERIC,@AverageReading) 
				WHERE GlobalAccountNumber = @AccNum
			END
		
	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_IsAreaExists]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 08-JAN-2014
-- Description:	IS AREA CODE EXISTS
-- =============================================
ALTER PROCEDURE [MASTERS].[USP_IsAreaExists]
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
DECLARE
		@AreaCode INT
		,@IsExists bit=0
		
SELECT 
		@AreaCode =C.value('(AreaCode)[1]','INT')
FROM @XmlDoc.nodes('AreaBe') as T(C)  
	if EXISTS(SELECT 0 FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaCode AND IsActive = 1)--Faiz-ID103 --added IsActive = 1 
		SET @IsExists=1

	
	SELECT @IsExists AS IsExists
	FOR XML PATH('AreaBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetArea]    Script Date: 06/15/2015 18:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Faiz-ID103>    
-- Create date: <02-Jun-2015>    
-- Description: <Retriving Area records form MASTERS.Tbl_MAreaDetails>  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetArea]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
  DECLARE	@PageNo INT    
			,@PageSize INT    
        
  SELECT       
		   @PageNo = C.value('(PageNo)[1]','INT')    
		   ,@PageSize = C.value('(PageSize)[1]','INT')     
  FROM @XmlDoc.nodes('AreaBe') AS T(C) 
     
  ;WITH PagedResults AS    
  (    
   SELECT    
		 ROW_NUMBER() OVER(ORDER BY IsActive DESC , CreatedDate DESC ) AS RowNumber  
		 ,AreaCode    
		 ,Address1   
		 ,Address2  
		 ,City
		 ,ZipCode  
		 ,AreaDetails
		 ,IsActive 
   FROM MASTERS.Tbl_MAreaDetails
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   FOR XML PATH('AreaBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('AreaInfoByXml')     
END

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 06/15/2015 19:16:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	,CustomerReadingInner.IsRollOver
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	
	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 
	
	
	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
			

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) ELSE ISNULL(CustomerReadings.IsRollOver,0) END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,(CASE WHEN (ReadToDirectApprovals.ApprovalStatusId = 1 OR ReadToDirectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) AS IsDisabledBook
		,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
		,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
		,(CASE WHEN CustomerReadings.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (Case when CONVERT(DATE,CB.ReadingDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)
			ELSE 0 END) as IsLatestBill
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerReadToDirectApprovaStatus As ReadToDirectApprovals ON CB.GlobalAccountNumber=ReadToDirectApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerAssignMeterApprovaStatus As AssignMeterApprovals	ON CB.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber
	LEFT JOIN #CustomerLatestBills As LatestBills ON CB.GlobalAccountNumber=LatestBills.GlobalAccountNumber
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)-- OR PrvExist = 1
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 --OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1 AND IsLatestBill = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Bill Generated for Future Date'
					WHERE IsBilled = 1 AND IsLatestBill = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId IN (1,4))
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE ApproveStatusId IN (1,4)
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsReadToDirectApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Disconnect Change Approval Process is pending for this Customer'
					WHERE IsReadToDirectApproval = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Changed Date'
					WHERE IsMeterChangedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterAssignedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Assigned Date'
					WHERE IsMeterAssignedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

