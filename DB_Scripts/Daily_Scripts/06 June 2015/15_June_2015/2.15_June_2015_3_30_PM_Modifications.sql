
GO

/****** Object:  UserDefinedFunction [dbo].[fn_CustomerExistenceCheck]    Script Date: 06/15/2015 15:24:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA
-- Create date: 09-JUNE-2015  
-- Description: To get the customer by BU is exists   
-- =============================================  
CREATE FUNCTION [dbo].[fn_CustomerExistenceCheck]
(  
@AccountNo varchar(50),
@BUID varchar(50)
) 
RETURNS @CustomerExistenceCheck TABLE(IsSuccess BIT,IsCustExistsInOtherBU BIT, GlobalAccountNumber VARCHAR(50),ActiveStatusId INT) 
AS  
BEGIN  
 DECLARE @CustomerBusinessUnitID VARCHAR(50)
		 ,@CustomerActiveStatusID INT   
		 ,@GlobalAccountNumber VARCHAR(50)
		 ,@ActiveStatusId INT

 SELECT @CustomerBusinessUnitID=BU_ID
		,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
		,@GlobalAccountNumber=GlobalAccountNumber 
		,@ActiveStatusId=ActiveStatusId 
 FROM	UDV_IsCustomerExists 
 WHERE	GlobalAccountNumber=@AccountNo 
		OR OldAccountNo=@AccountNo   
		OR MeterNumber=@AccountNo  
      
 IF @CustomerBusinessUnitID IS NULL  
	  BEGIN  
		INSERT INTO @CustomerExistenceCheck
		SELECT  0 AS IsSuccess
				,0 AS IsCustExistsInOtherBU
				,@GlobalAccountNumber AS GlobalAccountNumber
				,@ActiveStatusId AS ActiveStatusId 
	  END  
 ELSE
	 BEGIN
		IF @CustomerBusinessUnitID =  @BUID OR @BUID =''      
			BEGIN 
				 IF  @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3   
				  BEGIN  
				   INSERT INTO @CustomerExistenceCheck 
				  SELECT 1 AS IsSuccess 
						,0 AS IsCustExistsInOtherBU
						,@GlobalAccountNumber AS GlobalAccountNumber
						,@ActiveStatusId AS ActiveStatusId
				  END  
				 ELSE  
				  BEGIN          
				   INSERT INTO @CustomerExistenceCheck 
				   SELECT	0 AS IsSuccess
							,0 AS IsCustExistsInOtherBU
							,@GlobalAccountNumber AS GlobalAccountNumber
							,@ActiveStatusId AS ActiveStatusId
				  END  
			END 
		   ELSE  
			   BEGIN  
				INSERT INTO @CustomerExistenceCheck
				SELECT	1 AS IsSuccess
						,1 AS IsCustExistsInOtherBU 
						,@GlobalAccountNumber AS GlobalAccountNumber
						,@ActiveStatusId AS ActiveStatusId
			   END  
	END  
  RETURN
END  
GO



GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNormalCustomerConsumption]    Script Date: 06/15/2015 15:21:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,>
-- Description:	<Description,, Generate Normal customer consumption>
-- =============================================

--SELECT *	FROM fn_GetNormalCustomerConsumption('0001329260')

--select* from [fn_GetNormalCustomerConsumption]('0000026917',0)

   
			
			
ALTER FUNCTION [dbo].[fn_GetNormalCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50),
	@OpeningBalance decimal(18,2)	
)
RETURNS @CustomerConsumption TABLE 
(
	-- Add the column definitions for the TABLE variable here
	 GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage DECIMAL(18,2)
	,LastBillGenerated DATETIME
	,BalanceUsage BIT
	,IsFromReading INT
	,CustomerBillId INT
	,PreviousBalance Decimal(18,2)
	,Multiplier INt
	,ReadDate datetime
)
AS
BEGIN
	DECLARE @ResultConsumption VARCHAR(1000),@LastBillGenerated DATETIME,@MeterMultiplier INT, @BalanceUsage INT,@CustomerBillId Int,@PreviousBalance decimal(18,2),@ReadDate Datetime,@LastBillCurrentReading varchar(50)
	
		declare @CustomerReadings TABLE (
			-- Add the column definitions for the TABLE variable here
			 GlobalAccountNumber VARCHAR(50)
			,PreviousReading VARCHAR(50)
			,PresentReading VARCHAR(50)
			,Usage DECIMAL(18,2)
			,LastBillGenerated DATETIME
			,BalanceUsage BIT
			,IsFromReading INT
			,CustomerBillId INT
			,PreviousBalance Decimal(18,2)
			,Multiplier INt
			,ReadDate datetime
			,CustomerReadingId INT 
			,IsRollOver Bit
		)
	-- Add the T-SQL statements to compute the return value here
	-- If reading available
	IF EXISTS(SELECT GlobalAccountNumber FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0)
	BEGIN
		
		 
		SELECT @MeterMultiplier=(SELECT top (1)  MeterMultiplier FROM Tbl_MeterInformation(NOLOCK)    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber))    
		
		SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@BalanceUsage=BalanceUsage
		,@CustomerBillId=CustomerBillId,@PreviousBalance=PreviousBalance
		,@LastBillCurrentReading=PresentReading
		FROM Tbl_CustomerBills (NOLOCK)    
		WHERE AccountNo = @GlobalAccountNumber     
		ORDER BY CustomerBillId DESC   	
			
			IF @PreviousBalance IS NULL
				BEGIN
					select @PreviousBalance=@OpeningBalance
				END
			if @BalanceUsage IS NULL
				SET @BalanceUsage=0
			if @CustomerBillId IS NULL
				SET @CustomerBillId=0
		-- Fill the table variable with the rows for your result set
		INSERT INTO @CustomerReadings(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate
		,CustomerReadingId,IsRollOver)
		SELECT GlobalAccountNumber
			,PreviousReading AS PreviousReading
			,PresentReading AS PresentReading
			,Usage*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,ReadDate
			,CustomerReadingId
			,IsRollOver
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		
		INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate)
		SELECT GlobalAccountNumber
			,case when @LastBillCurrentReading IS NULL then (select top 1 PreviousReading from @CustomerReadings order by CustomerReadingId asc)  else @LastBillCurrentReading end AS PreviousReading
			,(select top 1 PresentReading from @CustomerReadings order by CustomerReadingId desc) AS PresentReading	   
			,sum(isnull(Usage,0))*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,Max(ReadDate)
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		group by GlobalAccountNumber
				
	
	END
	ELSE -- If reading not available
	BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 
		SELECT @IsFirstTimeCustomer=dbo.fn_IsFirstTimeCustomerForBilling(@GlobalAccountNumber)
		
		IF @IsFirstTimeCustomer=0
		BEGIN
		
		 
 		
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT TOP(1) AccountNo AS GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,AverageReading AS Usage
				,BillGeneratedDate AS LastBillGenerated
				,isnull(BalanceUsage,0)
				,0 AS IsFromReading
				,CustomerBillId
				,PreviousBalance
				 
			FROM Tbl_CustomerBills (NOLOCK) 
			WHERE AccountNo = @GlobalAccountNumber 
			ORDER BY CustomerBillId DESC
		END
		ELSE
		BEGIN
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,
			PresentReading,Usage,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,case when AvgReading IS NULL THen  InitialBillingKWh else AvgReading end AS Usage
				,0 AS  BalanceUsage 
				,0 AS IsFromReading
				,0
				,@OpeningBalance
				
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
		END		
	
	END	
	
	RETURN  
	
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 06/15/2015 15:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
   ,@AdjustmentAmountEffected DECIMAL(18,2)
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	 SELECT @AdjustmentAmountEffected=BA.AmountEffected
	 FROM Tbl_CustomerBills AS CB
	 JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
	 JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
	 WHERE CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
      
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
	END
END  
				 

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerActiveStatus]    Script Date: 06/15/2015 15:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju      
-- Create date: 07-10-2014      
-- Description: The purpose of this procedure is to Update Customer ActiveStausLog   
   --ModifiedBy : Padmini  
   --Modified Date :05-02-2015  
-- Modified By: Karteek  
-- Modified Date: 03-04-2015   
-- Modified Date: 15-06-2015   
-- =============================================  
ALTER PROCEDURE [dbo].[USP_ChangeCustomerActiveStatus]  
(  
 @XmlDoc xml      
)  
AS  
BEGIN  
 DECLARE     
   @GlobalAccountNumber VARCHAR(50)   
  ,@ModifiedBy VARCHAR(50)  
  ,@ActiveStatusId INT  
  ,@Details VARCHAR(MAX)  
  ,@ApprovalStatusId INT  
  ,@IsFinalApproval BIT = 0  
  ,@FunctionId INT  
  --,@ChangeDate VARCHAR(10)
  ,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
          
 SELECT       
   @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
  ,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')  
  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
  ,@Details = C.value('(Details)[1]','VARCHAR(MAX)')  
  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')    
  ,@FunctionId = C.value('(FunctionId)[1]','INT')    
  --,@ChangeDate = C.value('(ChangeDate)[1]','VARCHAR(10)')    
  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)  
   
 IF((SELECT COUNT(0)FROM Tbl_CustomerActiveStatusChangeLogs   
   WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN(1,4)) > 0)  
  BEGIN  
   SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
  END     
 ELSE  
  BEGIN  
   DECLARE @RoleId INT, @CurrentLevel INT  
     ,@PresentRoleId INT, @NextRoleId INT  
     ,@StatusChangeRequestId INT  
     
   SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId  
   SET @CurrentLevel = 0 -- For Stating Level     
     
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
					
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
       
   INSERT INTO Tbl_CustomerActiveStatusChangeLogs(   
     AccountNo  
    ,OldStatus  
    ,NewStatus  
    ,CreatedBy  
    ,CreatedDate  
    ,ChangeDate
    ,ApproveStatusId  
    ,Remarks  
    ,PresentApprovalRole  
    ,NextApprovalRole
    ,ModifiedBy
    ,ModifiedDate
	,CurrentApprovalLevel
	,IsLocked
    )  
   SELECT GlobalAccountNumber  
    ,ActiveStatusId  
    ,@ActiveStatusId  
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
    ,dbo.fn_GetCurrentDateTime()  --@ChangeDate 
    ,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
    ,@Details  
    ,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
	,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
    ,@ModifiedBy  
    ,dbo.fn_GetCurrentDateTime() 
    ,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
	,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
   FROM [CUSTOMERS].[Tbl_CustomerSDetail]     
   WHERE GlobalAccountNumber = @GlobalAccountNumber    
  
   SET @StatusChangeRequestId = SCOPE_IDENTITY()  
     
   IF(@IsFinalApproval = 1)  
    BEGIN  
       
     UPDATE CUSTOMERS.Tbl_CustomersDetail  
     SET         
       ActiveStatusId = @ActiveStatusId  
      ,ModifedBy = @ModifiedBy    
      ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
     WHERE GlobalAccountNumber = @GlobalAccountNumber   
       
     INSERT INTO Tbl_Audit_CustomerActiveStatusChangeLogs(    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole
      ,ModifiedBy
      ,ModifiedDate)    
     SELECT    
       ActiveStatusChangeLogId   
      ,AccountNo   
      ,OldStatus   
      ,NewStatus   
      ,dbo.fn_GetCurrentDateTime() --@ChangeDate
      ,ApproveStatusId   
      ,Remarks   
      ,CreatedBy   
      ,CreatedDate  
      ,PresentApprovalRole   
      ,NextApprovalRole
      ,ModifiedBy
      ,ModifiedDate
     FROM Tbl_CustomerActiveStatusChangeLogs WHERE ActiveStatusChangeLogId = @StatusChangeRequestId  
       
    END  
      
   SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')  
  
  END   
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 06/15/2015 15:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	,CustomerReadingInner.IsRollOver
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	
	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 
	
	
	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
			

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) ELSE ISNULL(CustomerReadings.IsRollOver,0) END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,(CASE WHEN (ReadToDirectApprovals.ApprovalStatusId = 1 OR ReadToDirectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) AS IsDisabledBook
		,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
		,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
		,(CASE WHEN CustomerReadings.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (Case when CONVERT(DATE,CB.ReadingDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)
			ELSE 0 END) as IsLatestBill
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerReadToDirectApprovaStatus As ReadToDirectApprovals ON CB.GlobalAccountNumber=ReadToDirectApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerAssignMeterApprovaStatus As AssignMeterApprovals	ON CB.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber
	LEFT JOIN #CustomerLatestBills As LatestBills ON CB.GlobalAccountNumber=LatestBills.GlobalAccountNumber
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)-- OR PrvExist = 1
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 --OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1 AND IsLatestBill = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Bill Generated for Future Date'
					WHERE IsBilled = 1 AND IsLatestBill = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId IN (1,4))
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE ApproveStatusId IN (1,4)
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsReadToDirectApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Disconnect Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Changed Date'
					WHERE IsMeterChangedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterAssignedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Assigned Date'
					WHERE IsMeterAssignedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookwiseCUstomerReadings]    Script Date: 06/15/2015 15:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 

-- =============================================  
-- ModifiedBy : Karteek
-- Modified date : 21-May-2015
-- Modified Desc: only Active customers retrieved for Bookwise and RouteWise
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookwiseCUstomerReadings] 
(
	@XmlDoc xml 
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@BookNo varchar(50)
			,@UserId VARCHAR(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')
		,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  
	
	DECLARE @RoleId INT
		,@UserIds VARCHAR(500)
		,@FunctionId INT = 13 -- Meter Readings
		,@ApprovalRoleId INT
		,@UserDetailedId INT
		,@ApprovalLevel INT
	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
		
	SET @UserDetailedId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) > 0)
		BEGIN
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) =1)
				BEGIN
					SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
					SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
					SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
					SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
				END
			ELSE
				BEGIN
					--
					--SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
					SET @UserIds=(SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
				END
		END

	SELECT IDENTITY(INT ,1,1) AS Sno,GlobalAccountNumber,OldAccountNo,BookNo,
		dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) as Name
		,MeterNumber as CustomerExistingMeterNumber
		,MeterDials As CustomerExistingMeterDials
		,Decimals as Decimals
		,InitialReading as	InitialReading
		,AccountNo
		,COUNT(0) OVER() AS TotalRecords 
	INTO #CustomersListBook
	FROM 
	UDV_CustomerMeterInformation where BookNo = @BookNo  AND ReadCodeID = 2 
	AND (BU_ID=@BUID OR @BUID='') 
	AND ActiveStatusId IN(1,4)  
	ORDER BY CreatedDate ASC

	DELETE FROM #CustomersListBook
	WHERE Sno < (@PageNo - 1) * @PageSize + 1 or Sno > @PageNo * @PageSize

	Select   MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadingApprovalLogs   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	AND ApproveStatusId IN(1,2,4)
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,ISNULL(CustomerReadingInner.IsLocked,0) AS IsLocked
	,CustomerReadingInner.ApproveStatusId
	,(CASE WHEN NextApprovalRole IS NULL 
				THEN (CASE ApproveStatusId WHEN 1 THEN 'Action Pending' ELSE ApprovalStatus END)
				ELSE (CASE ApproveStatusId WHEN 1 THEN 'Action Pending From ' + RoleName ELSE ApprovalStatus + ' By ' + RoleName END)
				END) AS [Status]
	,(CASE WHEN @UserIds IS NULL 
			THEN (CASE WHEN @UserId = CustomerReadingInner.CreatedBy THEN 1 ELSE 0 END)
			ELSE (CASE WHEN (@UserDetailedId IN (SELECT com FROM dbo.fn_Split(@UserIds,','))) AND (@ApprovalLevel + 1 = CurrentApprovalLevel)
				THEN 1
				ELSE 0
				END)
			END) AS IsPerformAction
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadingApprovalLogs CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
		ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	INNER JOIN Tbl_MApprovalStatus AS APS ON CustomerReadingInner.ApproveStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS CR ON CustomerReadingInner.PresentApprovalRole = CR.RoleId
	
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadIDMain
	From Tbl_CustomerReadings   CustomerReadingInner
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	--AND IsBilled = 0
	Group By CustomerReadingInner.GlobalAccountNumber


---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.IsTamper,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate,CustomerReadingInner.ReadType
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	INTO #CustomerLatestReadingsMain
	From Tbl_CustomerReadings CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDMain	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId 
----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

----------------------------------------------------------------------------------------------------------------

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

----------------------------------------------------------------------------------------------------------------

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
---------------------------------------------------------------------------------------------------------------------------------------------------

	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 

----------------------------------------------------------------------------------------------------------------

	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

----------------------------------------------------------------------------------------------------------------


	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
				   
---------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------



	SELECT(  
		  Select 
			CustomerList.Sno AS RowNumber,
			CustomerList.TotalRecords,
			CustomerList.OldAccountNo,
			CustomerList.GlobalAccountNumber as AccNum,
			CustomerList.AccountNo,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) 
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,@ReadDate) then 1 else 0 end)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (Case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end)
				ELSE (Case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) END) as IsExists,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN ISNULL(CONVERT(VARCHAR(20),CustomerReadings.ReadDate,106),'--')
				ELSE ISNULL(CONVERT(VARCHAR(20),CustomerReadingsMain.ReadDate,106),'--') END) as LatestDate,
			CustomerList.Name,
			(CASE WHEN CustomerReadingsMain.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled,
			(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)   as  IsLatestBill,
			--(CASE WHEN CustomerReadings.IsLocked = 0 AND CustomerReadings.ApproveStatusId = 1 THEN 1
			--	WHEN CustomerReadings.IsLocked = 1 AND CustomerReadings.ApproveStatusId = 1
			--		THEN 2
			--	ELSE 3 END) as ApproveStatusId,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate) THEN 1
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN 2
				ELSE 3 END) as ApproveStatusId,
			CustomerList.CustomerExistingMeterNumber as MeterNo,
			CustomerList.CustomerExistingMeterDials as MeterDials,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.ReadingMultiplier
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.ReadingMultiplier
				ELSE CustomerReadingsMain.ReadingMultiplier END) as Multiplier,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.IsTamper
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.IsTamper
				ELSE CustomerReadingsMain.IsTamper END) as IsTamper,
			0 as Decimals,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN ISNULL(CustomerReadings.AverageReading,'0.00')
				ELSE ISNULL(CustomerReadingsMain.AverageReading,'0.00') END) as AverageReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN CustomerReadings.TotalReadings
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN CustomerReadings.TotalReadings
				ELSE CustomerReadings.TotalReadings END) as TotalReadings,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else NULL
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else NULL end)
								 End)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading
											else null end) 
								else NULL
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else NULL end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading
											else null end) 
								else NULL
								--(case when CONVERT(DATE,CustomerReadingsMain.ReadDate) >= CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
								--	else NULL end)
								 End) END) as PresentReading,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0  
								else (case when isnull(CustomerReadings.PresentReading,0) = 0 then 0 else 1 end) end)
				ELSE 0 END) as PrvExist,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadings.Usage,0) end)
				ELSE (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then 0 else ISNULL(CustomerReadingsMain.Usage,0) end) END) as Usage,
			(CASE WHEN CustomerReadings.IsLocked = 0 AND CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,@ReadDate)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else isnull(CustomerList.InitialReading,0)
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4)
					THEN (case when CustomerList.CustomerExistingMeterNumber=CustomerReadings.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading							
											else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else isnull(CustomerList.InitialReading,0)
								--(case when CONVERT(DATE,CustomerReadings.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadings.PresentReading 
								--	else ISNULL(CustomerReadings.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End)
				ELSE (case when CustomerList.CustomerExistingMeterNumber=CustomerReadingsMain.ReadingMeterNumber 
								then (case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading							
											else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end) 
								else isnull(CustomerList.InitialReading,0)
								--(case when CONVERT(DATE,CustomerReadingsMain.ReadDate) < CONVERT(DATE,@ReadDate) then CustomerReadingsMain.PresentReading 
								--	else ISNULL(CustomerReadingsMain.PreviousReading,isnull(CustomerList.InitialReading,0)) end)
								 End) END) as PreviousReading
			,CustomerList.BookNo
			,CustomerList.InitialReading
			,(CASE WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4) THEN CustomerReadings.IsRollOver ELSE 0 END) AS Rollover
			,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
			,(CASE WHEN (MeterDisconnectApprovals.ApprovalStatusId = 1 OR MeterDisconnectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
			,CustomerReadings.[Status] AS [Description]
			,(CASE WHEN (CustomerReadings.ApproveStatusId = 1 OR CustomerReadings.ApproveStatusId = 4) THEN ISNULL(CustomerReadings.IsPerformAction,1) ELSE 1 END) AS IsPerformAction
			,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,@ReadDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
			,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,@ReadDate) 
					THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
			from #CustomersListBook CustomerList 
			LEFT JOIN  
			#CustomerLatestReadings	  As CustomerReadings
			ON
			CustomerList.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerLatestReadingsMain	  As CustomerReadingsMain
			ON
			CustomerList.GlobalAccountNumber=CustomerReadingsMain.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerMeterApprovaStatus	  As CustomerMeterApprovals
			ON
			CustomerList.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerReadToDirectApprovaStatus	  As MeterDisconnectApprovals
			ON
			CustomerList.GlobalAccountNumber=MeterDisconnectApprovals.GlobalAccountNumber
			LEFT JOIN  
			#CustomerAssignMeterApprovaStatus	  As AssignMeterApprovals
			ON
			CustomerList.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber	
			LEFT JOIN  
			#CustomerLatestBills	  As LatestBills
			ON
			CustomerList.GlobalAccountNumber=LatestBills.GlobalAccountNumber	
			ORDER BY OldAccountNo 
		  FOR XML PATH('BillingBE'),TYPE
		)
	 FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  		  
	
DROP Table #CustomerLatestReadings 
DROP Table #CusotmersWithMaxReadID
DROP Table #CustomerLatestReadingsMain
DROP Table #CusotmersWithMaxReadIDMain
DROP Table #CusotmersWithMeterChangeLogs
DROP Table #CusotmersReadToDirectChangeLogs
DROP Table #CustomerReadToDirectApprovaStatus
DROP Table #CustomerMeterApprovaStatus
DROP Table #CustomerAssignMeterApprovaStatus
DROP Table #CusotmersAssignMeterLogs
DROP Table  #CustomersListBook  
DROP TABLE #CustomerLatestBills 
DROP TABLE #CusotmersWithMaxBillID  

END  



GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountwiseMeterReadings]    Script Date: 06/15/2015 15:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 21 May 2015
-- Description: <Get BillReading details from customerreading table>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAccountwiseMeterReadings] 
(
	@XmlDoc xml
)  
AS  
BEGIN  
   
 DECLARE @Type VARCHAR(50);
    DECLARE @AccNum VARCHAR(100);  
     DECLARE @ReadDate VARCHAR(20)  
		   ,@PageNo INT  
		   ,@PageSize INT  
		   ,@Year INT
		   ,@Month INT
		   	,@OldAccountNo VARCHAR(50)
			,@MeterNo VARCHAR(50)
			,@BUID VARCHAR(50)=''
			,@UserId VARCHAR(50)
   
	SELECT @Type = C.value('(ReadMethod)[1]','VARCHAR(50)')  
	    ,@AccNum = C.value('(AccNum)[1]','VARCHAR(100)')  
	    ,@OldAccountNo = C.value('(OldAccountNo)[1]','VARCHAR(100)')
		,@MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')
		,@ReadDate = C.value('(ReadDate)[1]','VARCHAR(20)')  
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT') 
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)  

SELECT @Year = DATEPART(YY,@ReadDate),@Month = DATEPART(MM,@ReadDate)

	

		Declare @GlobalAcountNumber varchar(100)
		Declare  @OldAcountNumber   varchar(100)
		Declare @Name varchar(500)
		Declare @RouteNo varchar(50) 
		Declare @RouteName varchar(50) 
		Declare @MeterNumber varchar(100)
		Declare @InititalReading varchar(50)
		Declare @IsActiveMonth int
		Declare @MonthStartDate datetime
		Declare @SelectedDate datetime
		Declare @LastBillReadType varchar(50)
		Declare @EstimatdBillDate datetime

		Declare @usage decimal(18,2) 
		Declare	 @IsTamper int
		Declare	  @IsExists bit = 0
		Declare	  @PrvExists bit = 0
		Declare	  @IsInProcess bit = 0
		Declare	  @IsNoReadings bit = 0
		Declare @LatestDate datetime
		Declare @TotalReadings int
		Declare @PresentReading  varchar(50)
		Declare	 @PreviousReading varchar(50)
		Declare @IsBilled int
		Declare @AverageReading DECIMAL(18,2)
		Declare @AcountNumber varchar(50)
		Declare @ReadingsMeterNumber varchar(50)
		Declare @IsRollOver bit=0
		Declare @IsMeterApproval bit=0
		Declare @IsReadToDirectApproval bit=0		
		DECLARE @MeterChangedDate DATETIME
		DECLARE @MeterAssignedDate DATETIME
		
		DECLARE @RoleId INT
			,@UserIds VARCHAR(500)
			,@FunctionId INT = 13 -- Meter Readings
			,@ApprovalRoleId INT
			,@UserDetailedId INT
			,@ApprovalLevel INT

		SELECT	@RouteName =(select case when AvgMaxLimit is null then 0 else AvgMaxLimit end  from  Tbl_MRoutes where RouteId='')

		SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
		
		SET @UserDetailedId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
		
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) > 0)
			BEGIN
				IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID) =1)
					BEGIN
						SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
						SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
						SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BUID)
						SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
					END
				ELSE
					BEGIN
						--
						--SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
						SET @UserIds=(SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						SET @ApprovalLevel =(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BUID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserDetailedId ELSE @UserIds END)
					END
			END
  
		select  @OldAcountNumber=OldAccountNo,@GlobalAcountNumber=CD.GlobalAccountNumber,@MeterNumber=MeterNumber,
		@RouteNo=CPD.RouteSequenceNumber
		,@AcountNumber=CD.AccountNo
		,@Name=dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,Cd.LastName)
		,@InititalReading=InitialReading 
		from CUSTOMERS.Tbl_CustomersDetail	CD
		Inner Join	  CUSTOMERS.Tbl_CustomerProceduralDetails CPD
		ON CPD.GlobalAccountNumber=CD.GlobalAccountNumber
		and (CPD.GlobalAccountNumber= isnull(@AccNum,'') 
		OR isnull(CD.OldAccountNo,'N') = isnull(@AccNum,'|') 
		OR isnull(CPD.MeterNumber,'N') = isnull(@AccNum,'|'))
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails  CAD
		ON	CAD.GlobalAccountNumber=CD.GlobalAccountNumber
		Left Join Tbl_MRoutes [Routes]	 On [Routes].RouteId=isnull(CPD.RouteSequenceNumber,0)
		
		IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAcountNumber AND ApproveStatusId IN (1,4))
			BEGIN
				SET @IsMeterApproval = 1
			END
		IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @GlobalAcountNumber AND ApprovalStatusId IN (1,4))
			BEGIN
				SET @IsReadToDirectApproval = 1
			END
		IF EXISTS(SELECT 0 FROM Tbl_Audit_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAcountNumber)
			BEGIN
				SELECT TOP 1 @MeterChangedDate = MeterChangedDate FROM Tbl_Audit_CustomerMeterInfoChangeLogs 
					WHERE AccountNo = @GlobalAcountNumber
					ORDER BY MeterInfoChangeLogId DESC
			END
		IF EXISTS(SELECT 0 FROM Tbl_Audit_AssignedMeterLogs WHERE GlobalAccountNo = @GlobalAcountNumber)
			BEGIN
				SELECT TOP 1 @MeterAssignedDate = AssignedMeterDate FROM Tbl_Audit_AssignedMeterLogs 
					WHERE GlobalAccountNo = @GlobalAcountNumber
					ORDER BY AssignedMeterId DESC
			END
		
		DECLARE @MaxCustomerReadinglogId INT,@LogIsLocked BIT,@LogApprovalStatusId INT,@LogReadDate DATE
				,@Status VARCHAR(200), @IsPerformAction BIT
		
		SELECT TOP 1 @MaxCustomerReadinglogId = CustomerReadingLogId
			,@LogIsLocked = IsLocked
			,@LogApprovalStatusId = ApproveStatusId
			,@LogReadDate = ReadDate
			,@Status = (CASE WHEN NextApprovalRole IS NULL 
				THEN (CASE ApproveStatusId WHEN 1 THEN 'Action Pending' ELSE ApprovalStatus END)
				ELSE (CASE ApproveStatusId WHEN 1 THEN 'Action Pending From ' + RoleName ELSE ApprovalStatus + ' By ' + RoleName END)
				END) 
			,@IsPerformAction = (CASE WHEN @UserIds IS NULL 
									THEN (CASE WHEN @UserId = T.CreatedBy THEN 1 ELSE 0 END)
									ELSE (CASE WHEN (@UserDetailedId IN (SELECT com FROM dbo.fn_Split(@UserIds,','))) AND (@ApprovalLevel + 1 = CurrentApprovalLevel)
										THEN 1
										ELSE 0
										END)
									END)
		FROM Tbl_CustomerReadingApprovalLogs T
		INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
			AND GlobalAccountNumber = @GlobalAcountNumber
		LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
		ORDER BY CustomerReadingLogId DESC
		
		IF(@LogIsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate))
			BEGIN
				SET @IsExists = 1 
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingLogs
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND IsLocked = 0 AND CONVERT(DATE,@LogReadDate) = CONVERT(DATE,@ReadDate)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingLogs where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingLogs  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingLogs
					END
			END
		ELSE IF(@LogApprovalStatusId = 1 OR @LogApprovalStatusId = 4)
			BEGIN
				SET @IsInProcess = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingLogId
				INTO #CustomerTopTwoReadingApprove
				from Tbl_CustomerReadingApprovalLogs
				where GlobalAccountNumber=@GlobalAcountNumber 
				 AND ApproveStatusId IN (1,4)
				Order By CustomerReadingLogId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingApprove where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadingApprovalLogs where CustomerReadingLogId = Max(TopTwoReadings.CustomerReadingLogId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=0
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingApprove  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
								@PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=0
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingApprove
					END
			END
		ELSE 
			BEGIN
				SET @IsNoReadings = 1
				
				Select Top 2  Usage, isnull(IsTamper ,0) as IsTamper,
				 (Case when CONVERT(DATE,ReadDate) >= CONVERT(DATE,@ReadDate) then 1 else 0 end) as PrvExist 
				,ReadDate   as  LatestDate 
				,AverageReading 
				,TotalReadings  
				,PreviousReading
				,PresentReading 
				,IsBilled
				,MeterNumber   as  ReadingsMeterNumber
				,IsRollOver
				,GlobalAccountNumber
				,CustomerReadingId
				INTO #CustomerTopTwoReadingMain
				from Tbl_CustomerReadings
				where GlobalAccountNumber=@GlobalAcountNumber --and IsBilled=0 
				Order By CustomerReadingId DESC
				
				IF ((select COUNT(0) from #CustomerTopTwoReadingMain where IsRollOver=1) > 1)
					BEGIN

						SELECT  
							@usage=Sum(Usage),
							@IsTamper = max(case when isnull(IsTamper ,0)=0 then 0 else 1 end),
							@PrvExists = max(case when PrvExist=0 then 0 else 1 end)
							,@LatestDate=max(LatestDate) 
							,@AverageReading=(select AverageReading from Tbl_CustomerReadings where CustomerReadingId = Max(TopTwoReadings.CustomerReadingId))
							,@TotalReadings=max(TotalReadings)  
							,@PreviousReading = max(PreviousReading)
							,@PresentReading=min(PresentReading) 
							,@IsBilled=max(case when IsBilled=0 then 0 else 1 end )
							,@ReadingsMeterNumber=max(ReadingsMeterNumber) 
							,@IsRollOver =1 
						FROM #CustomerTopTwoReadingMain  AS TopTwoReadings
						Group By GlobalAccountNumber

					END
				ELSE
					BEGIN
							 Select Top 1 @usage=Usage,@IsTamper = isnull(IsTamper ,0),
							 @PrvExists = PrvExist
							,@LatestDate=LatestDate 
							,@AverageReading=AverageReading 
							,@TotalReadings=TotalReadings  
							,@PreviousReading = PreviousReading
							,@PresentReading=PresentReading 
							,@IsBilled=IsBilled
							,@ReadingsMeterNumber=ReadingsMeterNumber 
							,@IsRollOver =0
							from #CustomerTopTwoReadingMain
					END
			END
			
		--select @IsActiveMonth=dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month, @GlobalAcountNumber)  

		SELECT TOP(1) @EstimatdBillDate=BillGeneratedDate FROM Tbl_CustomerBills CB 
		WHERE AccountNo = @GlobalAcountNumber  
		ORDER BY CustomerBillId DESC
		
		--DECLARE @IsLatestBill BIT = 0 	

		--IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
		--BEGIN
		--	SET @IsLatestBill = 1
		--END 
		--ELSE
		--  SET @EstimatdBillDate=NULL		

	SELECT
	(
		select   @GlobalAcountNumber as AccNum,
			 @AcountNumber AS AccountNo,
			 @OldAcountNumber AS OldAccountNo,
			 @Name   AS Name,
			 @usage as Usage
			 ,@RouteName   as RouteNum
			 ,convert(varchar(11),@EstimatdBillDate,106) as EstimatedBillDate
			 --,(SELECT dbo.fn_IsCustomerBilledMonthLocked(@Year,@Month,@GlobalAcountNumber)) AS IsActiveMonth
			 ,@IsTamper as IsTamper
			 ,@IsExists	as IsExists 
			 ,@IsInProcess as IsApprovalProces
			 ,@IsNoReadings AS IsActive
			 ,@IsMeterApproval AS IsMeterChangeApproval
			 ,@IsReadToDirectApproval AS IsReadToDirectApproval
			 ,convert(varchar(11),@LatestDate,106) as LatestDate
			 ,case when isnull(@AverageReading,0)=0 then 0 else @AverageReading end as AverageReading
			 ,@MeterNumber as MeterNo
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PreviousReading	else @InititalReading End as PreviousReading
			 , case when @MeterNumber=@ReadingsMeterNumber then  @PresentReading	else NULL End    as	 PresentReading
			 ,@PrvExists as IsPrvExists  
			 ,@IsBilled AS IsBilled
			 ,(Case when CONVERT(DATE,@ReadDate) < CONVERT(DATE,@EstimatdBillDate) then 1 else 0 end)   as  IsLatestBill
			 --,@LastBillReadType as LastBillReadType
			 ,ISNULL(@InititalReading,0) as	 InititalReading
			 ,(CASE WHEN (@LogApprovalStatusId = 1 OR @LogApprovalStatusId = 4) THEN ISNULL(@IsPerformAction,1) ELSE 1 END) AS IsPerformAction
			 ,@Status AS [Description]
			 ,@IsRollOver as Rollover
			 ,(CASE WHEN ISNULL(@MeterChangedDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,@MeterChangedDate) >= CONVERT(DATE,@ReadDate) THEN 1 ELSE 0 END) END) AS IsMeterChangedDateExists
			 ,(CASE WHEN ISNULL(@MeterAssignedDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,@MeterAssignedDate) >= CONVERT(DATE,@ReadDate) THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
		FOR XML PATH('BillingBE'),TYPE
	)
	FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')  


END  


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 06/15/2015 15:23:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
 

 
ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)


	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
				
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd   .Service_Landmark  
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   
		
		 
		  
		  
		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN
			 
			BEGIN TRY		    
					 	 
					 
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0)+(Select top 1 isnull(Amount,0) from Tbl_PaidMeterPaymentDetails   WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId)
							WHERE AccountNo=@GlobalAccountNumber
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
							GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET @Usage =0
											SET @ActualUsage=0
											SET	 @EnergyCharges=0
										END			
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges(@TariffId,@MonthStartDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @FixedCharges=0
								SET @PaidMeterDeductedAmount=@FixedCharges
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						 SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 


							IF @PrevCustomerBillId IS NULL 
							BEGIN

							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = C.GlobalAccountNumber ORDER BY RecievedDate DESC) --@LastDateOfBill        
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,@PreviousBalance
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						IF	 isnull(@GetPaidMeterBalanceAfterBill,0) <>0
						BEGIN
						
						INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)
						SELECT @GlobalAccountNumber,@MeterNumber,@GetPaidMeterBalanceAfterBill,@BillNo,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY      
 
						update Tbl_PaidMeterDetails
						SET OutStandingAmount =@PaidMeterDeductedAmount -isnull(OutStandingAmount,0)  
						WHERE AccountNo = @GlobalAccountNumber	 -- and ActiveStatusId=1
					 	
						END
						 
				
						 
						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------
						
						update Tbl_CustomerReadings set IsBilled =1,
						BillNo=@CusotmerNewBillID
						where GlobalAccountNumber=@GlobalAccountNumber
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						 
						------------------------------------------------------------------------------------
						
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-

						SET @TotalAmount = NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						 
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL


  			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue        
						END
			END TRY
			BEGIN CATCH

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------






GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 06/15/2015 15:23:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	SELECT
	(	
		SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.AmountEffected) AS AmountEffected
			,BA.BatchNo
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 06/15/2015 19:16:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	,CustomerReadingInner.IsRollOver
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	
	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 
	
	
	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
			

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN ISNUMERIC(IsValidReading) = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) ELSE ISNULL(CustomerReadings.IsRollOver,0) END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,(CASE WHEN (ReadToDirectApprovals.ApprovalStatusId = 1 OR ReadToDirectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,(CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) AS IsDisabledBook
		,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
		,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') = '' THEN 0 
					ELSE (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) END) AS IsMeterAssignedDateExists
		,(CASE WHEN CustomerReadings.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (Case when CONVERT(DATE,CB.ReadingDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)
			ELSE 0 END) as IsLatestBill
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerReadToDirectApprovaStatus As ReadToDirectApprovals ON CB.GlobalAccountNumber=ReadToDirectApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerAssignMeterApprovaStatus As AssignMeterApprovals	ON CB.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber
	LEFT JOIN #CustomerLatestBills As LatestBills ON CB.GlobalAccountNumber=LatestBills.GlobalAccountNumber
	LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)-- OR PrvExist = 1
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 --OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1 AND IsLatestBill = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Bill Generated for Future Date'
					WHERE IsBilled = 1 AND IsLatestBill = 1
					
				END
				
			-- TO check whether the reading is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Readings'
					WHERE ISNULL(PresentReading,'') = ''
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId IN (1,4))
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE ApproveStatusId IN (1,4)
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsReadToDirectApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Disconnect Change Approval Process is pending for this Customer'
					WHERE IsReadToDirectApproval = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Changed Date'
					WHERE IsMeterChangedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterAssignedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Assigned Date'
					WHERE IsMeterAssignedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

