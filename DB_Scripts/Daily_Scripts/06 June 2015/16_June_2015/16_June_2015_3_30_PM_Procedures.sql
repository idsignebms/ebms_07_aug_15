
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 06/16/2015 15:52:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT DISTINCT
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount  
		,TempDetails.ReceiptNo  
		,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) 
									WHERE ReceiptNo = TempDetails.ReceiptNo) 
				THEN 1 
				ELSE (CASE WHEN ((SELECT COUNT(0) FROM Tbl_PaymentTransactions 
									WHERE ReceiptNo = TempDetails.ReceiptNo AND PUploadFileId = TempDetails.PUploadFileId) > 1)
						THEN 1 
						ELSE 0
					END)
			END) AS IsDuplicateReceiptNo 
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(VARCHAR(20),PaidAmount) = TempDetails.AmountPaid AND ReceiptNo = TempDetails.ReceiptNo) then 1 else 0 end)
				ELSE (case when ((SELECT 0 FROM Tbl_PaymentTransactions PPPP WHERE AccountNo = CD.GlobalAccountNumber 
					AND CONVERT(DATE,PPPP.ReceivedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(VARCHAR(20),PPPP.AmountPaid) = TempDetails.AmountPaid 
					AND ReceiptNo = TempDetails.ReceiptNo AND PUploadFileId = TempDetails.PUploadFileId) > 1) then 1 else 0 end)
				END) as IsDuplicate
		--,(CASE WHEN ISDATE(ReceivedDate) = 1 AND TempDetails.ReceiptNo IS NULL
		--		THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) WHERE AccountNo = CD.GlobalAccountNumber 
		--			AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
		--			AND CONVERT(VARCHAR(20),PaidAmount) = TempDetails.AmountPaid) then 1 else 0 end)
		--		ELSE 0 END) as IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy 
		--									AND ActiveStatusId = 1 AND BU_ID = BookDetails.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = TempDetails.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,BD.BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	INNER JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId  
	INNER JOIN Tbl_BatchDetails(NOLOCK) BD ON BD.BatchNo = PUB.BatchNo AND BD.BatchDate = PUB.BatchDate
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (CD.OldAccountNo = TempDetails.AccountNo OR CD.GlobalAccountNumber = TempDetails.AccountNo)
	--((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
	--					OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicateReceiptNo = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate ReceiptNo'
					WHERE IsDuplicateReceiptNo = 1
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the paid amount is valid or not
			IF((SELECT 0 FROM #PaymentValidation WHERE ISNUMERIC(PaidAmount) = 0) > 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Amount'
					WHERE ISNUMERIC(PaidAmount) = 0
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				--UPDATE B
				--SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				--FROM Tbl_BatchDetails B
				--INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetForBookNoChangeByAccno]    Script Date: 06/16/2015 15:52:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 29-09-2014   
-- Description: The purpose of this procedure is to get customer  Details By AccountNo For BookNo Change  
-- Modified By:  V.Bhimaraju  
-- Modified date: 07-May-2015   
-- Description: Added outstanding amount in CustomerStatus Change (Flag=2)
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustDetForBookNoChangeByAccno]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE @AccountNo VARCHAR(50)
		,@Flag INT
		
 SELECT  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@Flag=C.value('(Flag)[1]','INT')
 FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)    

IF(@Flag =1)--For CustomerName Change
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
					 ,CD.FirstName
					 ,CD.MiddleName
					 ,CD.LastName
					 ,CD.Title					 
					 ,CD.KnownAs
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 ,CAD.OutStandingAmount
				  FROM UDV_CustomerMeterInformation CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  AND ActiveStatusId IN (1,2)  
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END 
ELSE IF(@Flag =2)--For CustomerStatus Change 
BEGIN
	IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomersDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)  
		BEGIN  
			SELECT
					  CD.GlobalAccountNumber AS GlobalAccountNumber
					 ,CD.AccountNo As AccountNo
					 ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
					 ,CD.ActiveStatusId
					 ,CD.ActiveStatus
					 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
					 ,ISNULL(MeterNumber,'--') AS MeterNo
					 ,CD.ClassName AS Tariff
					 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
					 --,ISNULL(CD.MeterTypeId,0) AS MeterTypeId
					 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
					 ,CAD.OutStandingAmount --Faiz-ID103
					 --,dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber) AS LastTransactionDate --Faiz-ID103
					 ,CONVERT(VARCHAR(20),dbo.fn_GetLastTransactionDateByGlobalAccountNumber(CD.GlobalAccountNumber),103) AS LastTransactionDate1 --Faiz-ID103
				  FROM UDV_CustomerMeterInformation  CD
				  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber --Faiz-ID103
				  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE   
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF(@Flag =3)--For Customer MeterChange
BEGIN
	IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=2 AND ActiveStatusId = 1)  
		BEGIN  
			SELECT TOP (1)
				  CD.GlobalAccountNumber AS GlobalAccountNumber
				 ,CD.AccountNo As AccountNo
				 ,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				 ,CD.ActiveStatusId
				 ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
				 ,ISNULL(CD.MeterNumber,'--') AS MeterNo
				 ,CD.ClassName AS Tariff
				 ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo
				 ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
				,CASE WHEN  CR.PresentReading IS NULL THEN ISNULL(CAST(CD.InitialReading AS VARCHAR(20)),'--') ELSE CR.PresentReading END AS PreviousReading
				 --,ISNULL(CD.InitialReading,'--') AS PreviousReading				 
				 ,ISNULL(AverageReading,'--') As AverageUsage
				 ,ISNULL(BT.BillingType,'--') AS LastReadType
				 ,ISNULL(CONVERT(VARCHAR(20),ReadDate,103),'--') AS PreviousReadingDate		
				 ,dbo.fn_GetCustomer_ADC(Usage) AS AverageDailyUsage
				 ,CAD.OutStandingAmount --Faiz-ID103		 
			  FROM UDV_CustomerMeterInformation  CD
			  LEFT JOIN Tbl_CustomerReadings CR ON CD.GlobalAccountNumber=CR.GlobalAccountNumber AND CR.MeterNumber=CD.MeterNumber
			  LEFT JOIN Tbl_MBillingTypes BT ON BT.BillingTypeId=CR.ReadType
			  JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
			  AND ReadCodeID=2--Only Read Customers
			  ORDER BY CustomerReadingId DESC
				  FOR XML PATH('ChangeBookNoBe')    
		END  
	ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo) AND ReadCodeID=1)  
		BEGIN
			SELECT 1 AS IsDirectCustomer
			FOR XML PATH('ChangeBookNoBe')
		END
	ELSE  
		BEGIN  
			SELECT 1 AS IsAccountNoNotExists 
			FOR XML PATH('ChangeBookNoBe')  
		END
END
ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo AND ActiveStatusId IN(1,2))  
 BEGIN  
  SELECT
	CD.GlobalAccountNumber AS AccountNo  
     ,(SELECT StateCode FROM Tbl_BussinessUnits WHERE BU_ID=CD.BU_ID) AS StateCode
     ,dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
     ,KnownAs
     ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
     ,ISNULL(MeterNumber,'--') AS MeterNo
     ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS Tariff
     ,ISNULL(OldAccountNo,'--') AS OldAccountNo
     ,BU_ID  
     ,SU_ID  
     ,ServiceCenterId  
     ,dbo.fn_GetCycleIdByBook(BookNo) AS CycleId  
     ,BookNo  
     ,ActiveStatusId
     ,MeterTypeId
     ,ISNULL((SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=CD.MeterNumber),0) AS MeterDials
     ,CD.OutStandingAmount --Faiz-ID103
  FROM UDV_CustomerDescription  CD
  WHERE (CD.GlobalAccountNumber=@AccountNo  OR OldAccountNo=@AccountNo)
  AND ActiveStatusId IN (1,2)  
  FOR XML PATH('ChangeBookNoBe')    
 END  
ELSE   
 BEGIN  
  SELECT 1 AS IsAccountNoNotExists FOR XML PATH('ChangeBookNoBe')  
 END  
END   


GO


