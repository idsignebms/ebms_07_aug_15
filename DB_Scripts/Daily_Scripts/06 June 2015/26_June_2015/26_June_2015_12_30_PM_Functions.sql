
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 06/26/2015 12:54:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  ISNULL(CB.VAT,0)  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]           
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,  convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(BIGINT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(BIGINT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  ) 
		AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
   
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	  ,7 as [LastDueDays]
	  ,	CONVERT(VARCHAR(11),BillGeneratedDate+7,106)	 as LastDueDate
	  ,(SELECT dbo.fn_GetCustomer_ADC(CONVERT(BIGINT,CB.Usage))) AS ADC
	  ,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID 
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetNormalCustomerConsumption]    Script Date: 06/26/2015 12:54:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,>
-- Description:	<Description,, Generate Normal customer consumption>
-- =============================================

--SELECT *	FROM fn_GetNormalCustomerConsumption('0001329260')

--select * from [fn_GetNormalCustomerConsumption]('0000057417',0)

   
			
			
ALTER FUNCTION [dbo].[fn_GetNormalCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50),
	@OpeningBalance decimal(18,2)	
)
RETURNS @CustomerConsumption TABLE 
(
	-- Add the column definitions for the TABLE variable here
	 GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage DECIMAL(18,2)
	,LastBillGenerated DATETIME
	,BalanceUsage BIT
	,IsFromReading INT
	,CustomerBillId INT
	,PreviousBalance Decimal(18,2)
	,Multiplier INt
	,ReadDate datetime
)
AS
BEGIN
	DECLARE @ResultConsumption VARCHAR(1000),@LastBillGenerated DATETIME,@MeterMultiplier INT, @BalanceUsage INT,@CustomerBillId Int,@PreviousBalance decimal(18,2),@ReadDate Datetime,@LastBillCurrentReading varchar(50)
	
		declare @CustomerReadings TABLE (
			-- Add the column definitions for the TABLE variable here
			 GlobalAccountNumber VARCHAR(50)
			,PreviousReading VARCHAR(50)
			,PresentReading VARCHAR(50)
			,Usage DECIMAL(18,2)
			,LastBillGenerated DATETIME
			,BalanceUsage BIT
			,IsFromReading INT
			,CustomerBillId INT
			,PreviousBalance Decimal(18,2)
			,Multiplier INt
			,ReadDate datetime
			,CustomerReadingId INT 
			,IsRollOver Bit
		)
	-- Add the T-SQL statements to compute the return value here
	-- If reading available
	IF EXISTS(SELECT GlobalAccountNumber FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0)
	BEGIN
		
		 
		SELECT @MeterMultiplier=(SELECT top (1)  MeterMultiplier FROM Tbl_MeterInformation(NOLOCK)    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber))    
		
		SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@BalanceUsage=BalanceUsage
		,@CustomerBillId=CustomerBillId,@PreviousBalance=PreviousBalance
		,@LastBillCurrentReading=PresentReading
		FROM Tbl_CustomerBills (NOLOCK)    
		WHERE AccountNo = @GlobalAccountNumber     
		ORDER BY CustomerBillId DESC   	
			
			IF @PreviousBalance IS NULL
				BEGIN
					select @PreviousBalance=@OpeningBalance
				END
			if @BalanceUsage IS NULL
				SET @BalanceUsage=0
			if @CustomerBillId IS NULL
				SET @CustomerBillId=0
		-- Fill the table variable with the rows for your result set
		INSERT INTO @CustomerReadings(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate
		,CustomerReadingId,IsRollOver)
		SELECT GlobalAccountNumber
			,convert(int,PreviousReading) AS PreviousReading
			,convert(int,PresentReading) AS PresentReading
			,Usage*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,ReadDate
			,CustomerReadingId
			,IsRollOver
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		
		INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate)
		SELECT GlobalAccountNumber
			,(select top 1 convert(int,PreviousReading) from @CustomerReadings order by CustomerReadingId asc)  AS PreviousReading
			,(select top 1 convert(int,PresentReading) from @CustomerReadings order by CustomerReadingId desc) AS PresentReading	   
			,sum(isnull(Usage,0))*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,Max(ReadDate)
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		group by GlobalAccountNumber
				
	
	END
	ELSE -- If reading not available
	BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 
		SELECT @IsFirstTimeCustomer=dbo.fn_IsFirstTimeCustomerForBilling(@GlobalAccountNumber)
		
		IF @IsFirstTimeCustomer=0
		BEGIN
		
		 
 		
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT TOP(1) AccountNo AS GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,AverageReading AS Usage
				,BillGeneratedDate AS LastBillGenerated
				,isnull(BalanceUsage,0)
				,0 AS IsFromReading
				,CustomerBillId
				,PreviousBalance
				 
			FROM Tbl_CustomerBills (NOLOCK) 
			WHERE AccountNo = @GlobalAccountNumber 
			ORDER BY CustomerBillId DESC
		END
		ELSE
		BEGIN
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,
			PresentReading,Usage,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,case when isnull(AvgReading,0)=0 THen  InitialBillingKWh else AvgReading end AS Usage
				,0 AS  BalanceUsage 
				,0 AS IsFromReading
				,0
				,@OpeningBalance
				
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
		END		
	
	END	
	
	RETURN  
	
END


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint_Customerwise]    Script Date: 06/26/2015 12:54:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint_Customerwise]
(  
 @GlobalAcountNumber VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (
  
   
  SELECT         
	CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  ISNULL(CB.VAT,0)  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]             
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
	 ,convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  )  AS Usage              
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate    
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	,7 as [LastDueDays]
	,CONVERT(VARCHAR(11),BillGeneratedDate+7,106) as LastDueDate
	,(SELECT dbo.fn_GetCustomer_ADC(CB.Usage)) AS ADC
	,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID  
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
     where AccountNo=@GlobalAcountNumber and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO



GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 06/26/2015 12:54:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Satya
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
	Declare @NewAverage decimal(18,2) = 0,@TotalRecords INT = 0
		, @TotalUsage DECIMAL(18,2) = 0
		, @NoOfMonths INT = 3

   SELECT Top 1
         @TotalRecords = TotalReadings
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
   
    IF(ISNULL(@TotalRecords,0) >= @NoOfMonths)
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
			AND TotalReadings > (@TotalRecords - @NoOfMonths + 1)
			
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / @NoOfMonths
			ELSE
				SET @NewAverage = @usage
			
		END
    ELSE
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
						
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / (ISNULL(@TotalRecords,0) + 1)
			ELSE
				SET @NewAverage = @usage
			
		END
 
	RETURN @NewAverage

END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomer_ADC]    Script Date: 06/26/2015 12:54:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 29-MAY-2015
-- Description:	The purpose of this function is to calculate average daily consumption of a customer.
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomer_ADC]
(
@Usage BIGINT
)
RETURNS DECIMAL(18,2)
AS 
BEGIN
	RETURN CONVERT(DECIMAL(18,2),@Usage)/31
END

GO



GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 06/26/2015 14:30:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Satya
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
	Declare @NewAverage decimal(18,2) = 0,@TotalRecords INT = 0
		, @TotalUsage DECIMAL(18,2) = 0
		, @NoOfMonths INT = 12

   SELECT Top 1
         @TotalRecords = TotalReadings
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
   
    IF(ISNULL(@TotalRecords,0) >= @NoOfMonths)
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
			AND TotalReadings > (@TotalRecords - @NoOfMonths + 1)
			
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / @NoOfMonths
			ELSE
				SET @NewAverage = @usage
			
		END
    ELSE
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
						
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / (ISNULL(@TotalRecords,0) + 1)
			ELSE
				SET @NewAverage = @usage
			
		END
 
	RETURN @NewAverage

END
