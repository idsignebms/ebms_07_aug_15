
GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId,AccessLevelID)
VALUES('Assign Functional Permissions','../Admin/AssignFunctionalPermissions.aspx',122,61,1,1,205,3)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,205,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,205,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
