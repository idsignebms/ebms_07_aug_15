
GO
ALTER TABLE Tbl_MeterInformation
ADD IsCAPMIMeter BIT DEFAULT 0
GO
ALTER TABLE Tbl_MeterInformation
ADD CAPMIAmount DECIMAL(18,2)
GO

GO
UPDATE Tbl_MeterInformation
SET IsCAPMIMeter = 0
	,CAPMIAmount = 0
WHERE IsCAPMIMeter IS NULL
GO
--SELECT M.MeterNo, CD.IsCAPMI, CD.MeterAmount FROM Tbl_MeterInformation M
--INNER JOIN UDV_CustomerDescription CD ON CD.MeterNumber = M.MeterNo AND IsCAPMI = 1
GO
UPDATE M
SET M.IsCAPMIMeter = CD.IsCAPMI,
	M.CAPMIAmount = CD.MeterAmount 
FROM Tbl_MeterInformation M
INNER JOIN UDV_CustomerDescription CD ON CD.MeterNumber = M.MeterNo AND IsCAPMI = 1
GO

GO
ALTER TABLE Tbl_AssignedMeterLogs
ADD IsCAPMIMeter BIT DEFAULT 0
GO
ALTER TABLE Tbl_AssignedMeterLogs
ADD CAPMIAmount DECIMAL(18,2)
GO
GO
ALTER TABLE Tbl_Audit_AssignedMeterLogs
ADD IsCAPMIMeter BIT DEFAULT 0
GO
ALTER TABLE Tbl_Audit_AssignedMeterLogs
ADD CAPMIAmount DECIMAL(18,2)
GO


GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs
ADD IsCAPMIMeter BIT DEFAULT 0
GO
ALTER TABLE Tbl_CustomerMeterInfoChangeLogs
ADD CAPMIAmount DECIMAL(18,2)
GO
ALTER TABLE Tbl_Audit_CustomerMeterInfoChangeLogs
ADD IsCAPMIMeter BIT DEFAULT 0
GO
ALTER TABLE Tbl_Audit_CustomerMeterInfoChangeLogs
ADD CAPMIAmount DECIMAL(18,2)
GO