
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 06/27/2015 13:14:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Satya
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
	Declare @NewAverage decimal(18,2) = 0,@TotalRecords INT = 0
		, @TotalUsage DECIMAL(18,2) = 0
		, @NoOfMonths INT = 12

   SELECT Top 1
         @TotalRecords = TotalReadings
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
   
    IF(ISNULL(@TotalRecords,0) >= @NoOfMonths)
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
			AND TotalReadings > (@TotalRecords - @NoOfMonths + 1)
			
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / @NoOfMonths
			ELSE
				SET @NewAverage = @usage
			
		END
    ELSE
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
						
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / (ISNULL(@TotalRecords,0) + 1)
			ELSE
				SET @NewAverage = @usage
			
		END
 
	RETURN @NewAverage

END
