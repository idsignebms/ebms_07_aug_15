
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateActiveStatusForFunctionalPermission]    Script Date: 06/27/2015 13:14:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	Modified the function for deactivation the function
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateActiveStatusForFunctionalPermission]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @FunctionId INT
			,@IsActive BIT
			,@ModifiedBy VARCHAR(50)
			,@ModifiedDate DATETIME
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@IsActive=C.value('(IsActive)[1]','BIT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	SET @ModifiedDate=dbo.fn_GetCurrentDateTime();
	
	UPDATE TBL_FunctionalAccessPermission SET 
		IsActive=@IsActive 
		,ModifiedBy=@ModifiedBy
		,ModifiedDate=@ModifiedDate
	WHERE FunctionId=@FunctionId 
	
	SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 06/27/2015 13:14:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
CREATE PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetFunctionalAccessSettings]    Script Date: 06/27/2015 13:14:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to get Functional Access settings
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetFunctionalAccessSettings]
(
	@XmlDoc Xml=null
)
AS
BEGIN
	SELECT 
		 FAP.FunctionId
		,FAP.[Function]
		,FAP.IsActive
	 FROM TBL_FunctionalAccessPermission AS FAP
 	 ORDER BY FAP.[Function] ASC
END



GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- Modified By : Bhimaraju Vanka
-- Desc: Here in Adjustment (No Bill) These details are not having in master table we need do left join 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	
	;WITH PageResult AS(
	SELECT TOP 5 ISNULL(BT.Name,'Adjustment(No Bill)') AS AdjustmentName
		--SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.TotalAmountEffected) AS AmountEffected
			,BA.BatchNo
			--,(CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		--JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		)
		
		
	SELECT
	(	
		SELECT AdjustmentName
				--,(REPLACE(CONVERT(VARCHAR(25), CAST(AmountEffected AS MONEY), 1),'-','')+Format) AS NewAmountEffected
				,AmountEffected
				,BatchNo
				,CreatedDate
		FROM PageResult
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate
					,ApprovedBy 
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					,@ModifiedBy
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill--commented by neeraj as per SATYA 
							
							--UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							--WHERE CustomerBillId=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							--,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,CD.OutStandingAmount AS TotalDueAmount
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmount - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CBILLS.AccountNo,CBILLS.AverageReading as  LastBillAverage,CBILLS.Usage, 
	CD.GlobalAccountNumber,CD.AccountNo AS AccNo,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.MeterNumber AS MeterNo
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
		 INTO #CustomerBillswithAverage
		 from   Tbl_CustomerBills(NOLOCK)	CBILLS
	INNER JOIN  UDV_PreBillingRpt(NOLOCK) CD ON CBILLS.AccountNo = CD.GlobalAccountNumber 
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
		
	
	SELECT (CD.AccNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.Name 
		,CD.ServiceAddress AS ServiceAddress
		,MIN(CR.PreviousReading) AS PreviousReading 
		,MAX (CR.ReadDate) AS PreviousReadDate
		,MAX(CR.PresentReading) AS	 PresentReading
		,CAST(SUM(CR.usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(CR.CreatedDate) AS LatestTransactionDate
		,CreatedBy AS TransactionLog
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CustomerSortOrder 
		,CD.CycleName
		,BookNumber
		,CD.MeterNo
		,CD.BookSortOrder 
	FROM Tbl_CustomerReadings CR
	INNER JOIN #CustomerBillswithAverage CD ON	   CD.GlobalAccountNumber=CR.GlobalAccountNumber
	GROUP BY CD.GlobalAccountNumber,CD.OldAccountNo,
		CD.MeterNumber,CD.ClassName,
		CD.Name,
		CD.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CD.BookSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
		, CD.Usage
		,CustomerSortOrder
		,CD.AccNo 
		,CD.MeterNo
		,CreatedBy
    HAVING 
	CAST(SUM(CR.usage) AS DECIMAL(18,2)) 
	between   
	 CD.Usage- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	 CD.Usage +((30*CONVERT(Decimal(18,2),AverageReading)/100))
    
    DELETE FROM		 #CustomerBillswithAverage
 	
END
----------------------------------------------------------------------------------------------------
 




GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- Modified By: NEERAJ KANOJIYA
-- Modified Date: 26-JUNE-2015
-- Description: @AccountNo was incorrect in update statement for outstanding, modified with @GlobalAccountNo.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
					--						WHERE AdjustmentLogId = @AdjustmentLogId))

					--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END			
					
					
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@ModifedBy
								 ,@CreatedBy
								 ,@CreatedDate 
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
							,CurrentApprovalLevel= 0
							,IsLocked=1
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected 
							 ,CreatedBy
							 ,ApprovedBy
							 ,CreatedDate  
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected 
							 ,@ModifedBy
							 ,@CreatedBy
							 ,@CreatedDate 
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END
GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
		,CD.InitialBillingKWh
	FROM
	UDV_PrebillingRpt(NOLOCK)  CD
	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID   and  CD.ReadCodeID = 1	AND CD.ActiveStatusId=1  
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	 AND  isnull(CD.BoolDisableTypeId,0)=0

	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	WHERE isnull(DAVG.AvgReadingId,0)=0 
       -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateActiveStatusForFunctionalPermission]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	Modified the function for deactivation the function
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateActiveStatusForFunctionalPermission]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @FunctionId INT
			,@IsActive BIT
			,@ModifiedBy VARCHAR(50)
			,@ModifiedDate DATETIME
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@IsActive=C.value('(IsActive)[1]','BIT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	SET @ModifiedDate=dbo.fn_GetCurrentDateTime();
	
	UPDATE TBL_FunctionalAccessPermission SET 
		IsActive=@IsActive 
		,ModifiedBy=@ModifiedBy
		,ModifiedDate=@ModifiedDate
	WHERE FunctionId=@FunctionId 
	
	SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetFunctionalAccessSettings]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to get Functional Access settings
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetFunctionalAccessSettings]
(
	@XmlDoc Xml=null
)
AS
BEGIN
	SELECT 
		 FAP.FunctionId
		,FAP.[Function]
		,FAP.IsActive
	 FROM TBL_FunctionalAccessPermission AS FAP
 	 ORDER BY FAP.[Function] ASC
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) --= 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) --= 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) --= 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) --= '2,3,4,5,7,8,9'
		,@PageNo INT --= 1
		,@PageSize INT --= 100  
		
	SELECT  
		 @BU_IDs = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(Months)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(Years)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerBillsBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,( CD.AccountNo  + ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress 
		,CD.MeterNumber AS MeterNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CB.NetEnergyCharges
		,CB.NetFixedCharges
		,CB.TotalBillAmount
		,CB.VAT
		,CB.VATPercentage
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CB.NetArrears
		,CB.TotalBillAmountWithArrears
		,(CASE CD.ReadCodeID WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadCode
		,CB.Usage
		,CB.PaidAmount
		,CONVERT(VARCHAR(20),CB.PaymentLastDate,106) AS PaymentLastDate
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CycleName
		,CD.BookDetails AS BookNumber
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  (NOLOCK)
	INNER JOIN Tbl_MBillingTypes BT (NOLOCK) ON CB.ReadType = BT.BillingTypeId AND
		(CB.ReadType = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN UDV_CustomerPDFReport CD (NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo 
	INNER JOIN Tbl_MTariffClasses T (NOLOCK) ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY CB.AccountNo ASC


	SELECT   
		 RowNumber 
		,GlobalAccountNumber 
		,OldAccountNo 	
		,Name      
		,ServiceAddress
		,MeterNo
		,CONVERT(VARCHAR(20),CAST(NetEnergyCharges AS MONEY),-1) AS NetEnergyCharges
		,CONVERT(VARCHAR(20),CAST(NetFixedCharges AS MONEY),-1) AS NetFixedCharges
		,CONVERT(VARCHAR(20),CAST(TotalBillAmount AS MONEY),-1) AS TotalBillAmount
		,CONVERT(VARCHAR(20),CAST(VAT AS MONEY),-1) AS VAT
		,CONVERT(VARCHAR(20),CAST(VATPercentage AS MONEY),-1) AS VATPercentage
		,BillNo    
		,TariffId  
		,TariffName  
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithTax AS MONEY),-1) AS TotalBillAmountWithTax  
		,CONVERT(VARCHAR(20),CAST(NetArrears AS MONEY),-1) AS NetArrears
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithArrears AS MONEY),-1) AS TotalBillAmountWithArrears
		,CONVERT(INT,Usage) AS Usage
		,ReadCode
		,ReadDate  
		,PaymentLastDate
		,BillMonth  
		,BillYear   
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,BillProcessType
		,TotalRecords
		,0 AS BookSortOrder
		,0 AS CustomerSortOrder
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CycleName
		,BookNumber
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
 

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerBookNoChangeApproval]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerBookNoChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @BookNoChangeLogId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ApprovalStatusId INT
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @BookNoChangeLogId = C.value('(BookNoChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@OldBU_ID VARCHAR(20),
		@NewBU_ID VARCHAR(20),
		@OldSU_ID VARCHAR(20),
		@NewSU_ID VARCHAR(20),
		@OldSC_ID VARCHAR(20),	
		@NewSC_ID VARCHAR(20),
		@OldCycleId VARCHAR(20),
		@NewCycleId VARCHAR(20),	
		@OldBookNo VARCHAR(20),
		@NewBookNo VARCHAR(20),
		@TempAddressId INT,
		@TempServiceId INT,
		@AccountNo VARCHAR(50)	
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNo
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		,@OldBU_ID					 = OldBU_ID 
		,@NewBU_ID					 = NewBU_ID 
		,@OldSU_ID					 = OldSU_ID 
		,@NewSU_ID					 = NewSU_ID 
		,@OldSC_ID					 = OldSC_ID 
		,@NewSC_ID					 = NewSC_ID 
		,@OldCycleId				 = OldCycleId
		,@NewCycleId				 = NewCycleId
		,@OldBookNo					 = OldBookNo
		,@NewBookNo					 = NewBookNo
		FROM Tbl_BookNoChangeLogs
		WHERE BookNoChangeLogId=@BookNoChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_BookNoChangeLogs 
					--						WHERE BookNoChangeLogId = @BookNoChangeLogId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_BookNoChangeLogs 
											WHERE BookNoChangeLogId = @BookNoChangeLogId)
											
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END

					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
							
							
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewCycleId,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNumber
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET BookNo=@NewBookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1
					
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
									,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_BookNoChangeLogs
								WHERE GlobalAccountNo =@GlobalAccountNumber AND BookNoChangeLogId=@BookNoChangeLogId)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,0
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN							
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_BookNoChangeLogs
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
					WHERE GlobalAccountNo = @GlobalAccountNumber
					AND  BookNoChangeLogId= @BookNoChangeLogId
					
					SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewCycleId,@NewBookNo);

					UPDATE CUSTOMERS.Tbl_CustomersDetail 
						SET AccountNo=@AccountNo 
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET BookNo=@NewBookNo,IsBookNoChanged=1       
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_BookNoChangeLogs(
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
					/*--Postal Address Details Insertion Based on IsSameAsService
					if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
					old Data will be deleted from the address table for that account no*/
					IF ((SELECT NewIsSameAsService FROM Tbl_BookNoChangeLogs
						WHERE GlobalAccountNo =@GlobalAccountNumber AND BookNoChangeLogId=@BookNoChangeLogId)=1)
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
							(GlobalAccountNumber
							,HouseNo
							,StreetName
							,City
							,Landmark
							,Details
							,AreaCode
							,IsServiceAddress
							,ZipCode
							,IsCommunication
							,IsActive
							,CreatedDate
							,CreatedBy)
							VALUES
							(@GlobalAccountNumber
							,@NewPostal_HouseNo
							,@NewPostal_StreetName
							,@NewPostal_City
							,@NewPostal_Landmark
							,NULL
							,@NewPostal_AreaCode
							,0
							,@NewPostal_ZipCode
							,1
							,1
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime())
				
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,ServiceAddressID=@TempAddressId
									,IsSameAsService=1
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
					ELSE
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewPostal_HouseNo
										,@NewPostal_StreetName
										,@NewPostal_City
										,@NewPostal_Landmark
										,NULL
										,@NewPostal_AreaCode
										,0
										,@NewPostal_ZipCode
										,@IsPostalCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
							
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,IsSameAsService=0
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewService_HouseNo
										,@NewService_StreetName
										,@NewService_City
										,@NewService_Landmark
										,NULL
										,@NewService_AreaCode
										,1
										,@NewService_ZipCode
										,@IsServiceCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
										
							SET	@TempServiceId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET ServiceAddressID=@TempServiceId
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
			END
			
				SELECT 1 As IsSuccess  
				FOR XML PATH('ChangeCustomerTypeBE'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_BookNoChangeLogs 
							--									WHERE BookNoChangeLogId = @BookNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_BookNoChangeLogs 
																WHERE  BookNoChangeLogId = @BookNoChangeLogId )
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_BookNoChangeLogs 
						WHERE BookNoChangeLogId = @BookNoChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_BookNoChangeLogs
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNo = @GlobalAccountNumber  
			AND BookNoChangeLogId = @BookNoChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 06/27/2015 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,(CD.BookId+' ( '+BookCode+' )') AS BookCode
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,CAD.InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  ,CD.AccountNo --Faiz-ID103
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  and PAD1.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 06/27/2015 13:15:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	IF(@BillingRule = 1) -- As per settings
		BEGIN
			SELECT
			(			
				SELECT 
					 ClassID
					,TC.ClassName
					,CC.CategoryName
					,CC.ClusterCategoryId 
					,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
					,Cycles.CycleId 
					,Cycles.CycleName
					,MAX(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					) as TotalReadCustomersCount	  
					,SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					) as TotalDirectCustomers
					,SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end
					) as TotalReadCustomersHavingReadings
					,SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=2 then  ISNULL(CR.Usage,CAD.AvgReading)  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=2 then  ISNULL(CR.Usage,0)  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=1 then  ISNULL(DCAVG.AverageReading,isnull(CAD.AvgReading,CAD.InitialBillingKWh))  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=1 then  ISNULL(DCAVG.AverageReading,0)  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					)
					-
					SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end
					) as TotalNonUploadedCustomersCount
					,SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					)  
					-
					SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=2 then  case when  CR.Usage IS NULL  then isnull(CAD.AvgReading,0) else 0 end
						 Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=1 then  case when DCAVG.AverageReading IS NULL  then isnull(CAD.AvgReading,CAD.InitialBillingKWh) else 0  end  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
				INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
				INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
				INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
				INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
				INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
				LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
				LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
				LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
				LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
						AND ES.ClusterCategoryId = CC.ClusterCategoryId 
						AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
						And ES.BillingMonth= @Month	   
						AND ES.BillingYear=	@year					 
				GROUP BY TC.ClassID,TC.ClassName,CC.CategoryName
					,CC.ClusterCategoryId,Cycles.CycleCode,Cycles.CycleName
					,Cycles.CycleId 
				ORDER BY Cycles.CycleId,CC.ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					 ClassID
					,TC.ClassName
					,CC.CategoryName
					,CC.ClusterCategoryId 
					,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
					,Cycles.CycleId 
					,Cycles.CycleName
					,MAX(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					) as TotalReadCustomersCount	  
					,SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end

						else 0
						end
					) as TotalDirectCustomers
					,SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end

						else 0
						end
					) as TotalReadCustomersHavingReadings
					,SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=2 then  ISNULL(CR.Usage,CAD.AvgReading)  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=2 then  ISNULL(CR.Usage,0)  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=1 then  ISNULL(DCAVG.AverageReading,isnull(CAD.AvgReading,CAD.InitialBillingKWh))  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=1 then  ISNULL(DCAVG.AverageReading,0)  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					)
					-
					SUM(
						case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end
					) as TotalNonUploadedCustomersCount
					,SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then 1 
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end
					)  
					-
					SUM(
						case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsPartialBill,1)  =1 
							then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=2 then  case when  CR.Usage IS NULL  then isnull(CAD.AvgReading,0) else 0 end
						 Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  then 
						(case when isnull(BDB.IsPartialBill,1)=1 then
						case when  isnull(ReadCodeID,0)=1 then  case when DCAVG.AverageReading IS NULL  then isnull(CAD.AvgReading,CAD.InitialBillingKWh) else 0  end  Else 0 end 
						else  0 end ) else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
				INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
				INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
				INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
				INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
				INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
				LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
				LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
				LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
				LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,',')) 
						AND ES.ClusterCategoryId = CC.ClusterCategoryId
						AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
						And ES.BillingMonth= @Month	   
						AND ES.BillingYear=	@year					 
				GROUP BY TC.ClassID,TC.ClassName,CC.CategoryName
					,CC.ClusterCategoryId,Cycles.CycleCode,Cycles.CycleName
					,Cycles.CycleId 
				ORDER BY Cycles.CycleId,CC.ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		END
	
	DROP TABLE #ReadCustomersList
	
END



--SELECT ClassID
--	,
--	SUM(
--			case 
--			when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
--			then

--			case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 else
--																case 
--																when  
--																isnull(BDB.DisableTypeId,2) =1 
--																then 0  
--																else 1 
--																end
--			end
--			else 
--			0
--			end
--			)
	
--	as TotalReadCustomers
--	,
--	SUM ( case 
--	when 
--	(CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and
--	(isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
--	then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
--	then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )  
--	 as EstimatedCustomers
--	,TC.ClassName
--	,CC.CategoryName
--	,CC.ClusterCategoryId 
--	,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
--	,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
--	,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
--	,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
--	,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
--	,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
--	,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
--	,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
--	,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
--	,Cycles.CycleId 
--	,Cycles.CycleName
--FROM Tbl_MTariffClasses(NOLOCK)	TC
--INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
--INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
--INNER JOIN   MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
--INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
--INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
--LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
--LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
--LEFT JOIN Tbl_BillingDisabledBooks BDB
--ON  BDB.BookNo=BN.BookNo and IsActive=1
--LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
--		and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
--		And ES.BillingMonth = @Month -- Need To Modify	 
--		AND ES.BillingYear = @Year	 -- Need To Modify
--Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
--ORDER BY Cycles.CycleId,CC.ClusterCategoryId

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForBillGen]    Script Date: 06/27/2015 13:15:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 25-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- Create date  : 8-APRIL-2015                    
 -- Description  : REMOVE ACTIVE STATUS ID FILTER
 
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForBillGen]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID  
				  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                   
				  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo
				  ,(A.AccountNo+ ' - '+ A.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,A.OutStandingAmount AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful  
				  ,A.ServiceUnitName 
				  ,A.ServiceCenterName
				  ,A.CycleName
				  --,A.BookNo      
				  ,A.OldAccountNo  
				  ,A.BookId AS BookNo
				  ,A.CycleId
		  FROM [UDV_CustomerDescription] AS A    
		  LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				--AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END 
GO


