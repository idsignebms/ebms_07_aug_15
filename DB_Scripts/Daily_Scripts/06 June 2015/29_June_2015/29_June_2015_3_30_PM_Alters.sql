
GO
CREATE TABLE Tbl_Cashier(
	CashierId int IDENTITY(1,1) NOT NULL,
	FirstName varchar(50),
	MiddleName varchar(50),
	LastName varchar(50),
	ContactNumber varchar(20),
	AlternateContactNumber varchar(20),
	Address1 varchar(max),
	Address2 varchar(max),
	City varchar(50),
	Zipcode varchar(50),
	EmailId varchar(max),
	Notes varchar(max),
	CashOfficeId INT,
	ActiveStatusId int DEFAULT 1,
	CreatedBy varchar(50),
	CreatedDate datetime,
	ModifiedBy varchar(50),
	ModifiedDate datetime
) 


GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId,AccessLevelID)
VALUES('Add Cashier','../Masters/AddCashier.aspx',6,24,1,1,206,3)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,206,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,206,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO