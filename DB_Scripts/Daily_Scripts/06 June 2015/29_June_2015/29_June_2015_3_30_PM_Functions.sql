
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCashiersFullName]    Script Date: 06/29/2015 15:43:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 29-Jun-2015
-- Description:	To Get FullName Of the Cashier FirstName+MiddleName+LastName
-- =============================================
CREATE FUNCTION [dbo].[fn_GetCashiersFullName]
(
	@CashierId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Name VARCHAR(MAX)

		SELECT @Name =	LTRIM(RTRIM(
		CASE WHEN FirstName IS NULL OR FirstName = '' THEN '' ELSE  LTRIM(RTRIM(FirstName)) END + 
		CASE WHEN MiddleName IS NULL OR MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(MiddleName)) END +
		CASE WHEN LastName IS NULL OR LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(LastName)) END))
	FROM [Tbl_Cashier]				
	WHERE CashierId = @CashierId
	RETURN @Name
END

GO


