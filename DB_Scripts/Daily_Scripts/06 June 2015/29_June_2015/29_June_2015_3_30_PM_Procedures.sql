
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCashOffices_ByBU_ID]    Script Date: 06/29/2015 15:44:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <29-Jun-2015>
-- Description:	<Get CashOffices>
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCashOffices_ByBU_ID]
	(@XmlDoc xml=null)
AS
BEGIN
	DECLARE @BU_ID VARCHAR(MAX)

	SELECT @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
	
	FROM @XmlDoc.nodes('BillingBE') AS T(C)	

	IF(@BU_ID != '')
	BEGIN
		SELECT (
			SELECT CashOfficeId as OfficeId 
					,CashOffice as OfficeName 
			FROM Tbl_CashOffices 
			WHERE ActiveStatusId=1
			AND	(BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,',')) OR @BU_ID='')
		FOR XML PATH('BillingBE'),TYPE
		)
		FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
	END
	ELSE
	BEGIN
		SELECT (
				SELECT CashOfficeId as OfficeId 
						,CashOffice as OfficeName 
				FROM Tbl_CashOffices WHERE ActiveStatusId=1
			FOR XML PATH('BillingBE'),TYPE
			)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
	 END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCashiersForDdl]    Script Date: 06/29/2015 15:44:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Faiz-ID103  
-- Create date: 29-Jun-2015
-- Description: Purpose is To get List Of Cashiers for dropdownlist
-- =============================================    
CREATE PROCEDURE [dbo].[USP_GetCashiersForDdl]    
(    
 @xmlDoc Xml=null    
)    
AS    
BEGIN    
     
 DECLARE @CashOfficeId VARCHAR(50)    
 SELECT @CashOfficeId=C.value('(CashOfficeId)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('CashierBE') as T(C)    
 SELECT    
 (    
  SELECT   CashierId    
    ,dbo.fn_GetCashiersFullName(CashierId) AS Cashier    
  FROM Tbl_Cashier    
  --WHERE ActiveStatusId IN(1,2)  
  WHERE ActiveStatusId =1   
  AND CashOfficeId=@CashOfficeId
  FOR XML PATH('CashierBE'),TYPE    
 )    
 FOR XML PATH(''),ROOT('CashierBeInfoByXml')    
END  

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetExtraChargeNameAndCharges]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------PROCEDURE 1-------------------------------------------------      
-- =============================================      
-- Author  : Naresh      
-- Create date : 24-Apr-2014      
-- Modified By : NEERAJ KANOJIYA      
-- Modified date: 3-MAY-2014      
-- Modified By : Suresh Kumar  
-- Modified date: 25-JULY-2014      
-- Description : To Get Extra Charges Name And Charges      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetExtraChargeNameAndCharges]      
 (      
 @XmlDoc xml      
 )      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(20)        
          ,@CustomerBillId INT        
           ,@Traffid Varchar(20)           
         
  SELECT        
       @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')        
      ,@CustomerBillId = C.value('(CustomerBillID)[1]','INT')        
      FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)       
  IF(@AccountNo = '' OR @AccountNo IS NULL)     --NEERAJ   
  BEGIN    
   SELECT @AccountNo=AccountNo FROM Tbl_Customerbills WHERE CustomerBillId=@CustomerBillId  
  END    
  
  DECLARE @TotalAdditionCharges DECIMAL(18,2) = 0
  
  SELECT @TotalAdditionCharges = SUM(AmountEffected) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 2
      
	SELECT (
		 SELECT CM.ChargeId AS ChargeID
				,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,@TotalAdditionCharges AS GrandTotal
		 FROM Tbl_MChargeIds CM
		 LEFT JOIN TBL_Customer_Additionalcharges CA ON CM.ChargeId = CA.ChargeId AND CA.AccountNO = @AccountNo AND CA.CustomerBillId = @CustomerBillId    
		 LEFT JOIN Tbl_CustomerBills CB ON CA.CustomerBillId = CB.CustomerBillId
		 WHERE CM.IsAcitve = 1
		FOR XML PATH('CustomerBillDetails'),TYPE        
	 )       
	 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
	 
			 
		----------------Old One Modified  by Suresh---	 
		 --SELECT   CC.ChargeId AS ChargeID,      
			--	(Select ChargeName from Tbl_MChargeIds WHERE ChargeId=CC.ChargeId) AS ChargeName,      
			--	CustomerAdditioalCharges,      
			--	 Amount,      
			--	(SELECT BillNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId ) AS BillNo,    
			--	(SELECT TotalBillAmount FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId) AS TotalBillAmount    
		 --FROM  TBL_Customer_Additionalcharges CC WHERE  CC.AccountNO=@AccountNo       
		 --AND CC.CustomerBillId=@CustomerBillId   
 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillReadings_ByAccountNo]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================              
-- Author  : Naresh Kumar                
-- Create date  : 17 Apr 2014              
-- Description  : The purpose of this procedure is to get Reading details based on Accno and Billno           
-- ModifiedBy : NEERAJ        
-- Modified On : 28-Aug      
-- ModifiedBy : NEERAJ        
-- Modified On : 28-FEB
-- DESC			: ADDED MeterMultiplier FIELD  
-- ModifiedBy : NEERAJ        
-- Modified On : 1-March-2015
-- DESC			: change join to left join for meter information for direct customers.
-- =============================================              
ALTER PROCEDURE [dbo].[USP_GetCustomerBillReadings_ByAccountNo]  
(              
@XmlDoc xml              
)              
AS              
BEGIN              
 DECLARE @AccountNo VARCHAR(20)              
          ,@CustomerBillID INT            
           ,@TariffId Varchar(20)             
           ,@ReadCode INT             
            ,@Month INT = 4      
   ,@Year INT = 2014      
   ,@Date VARCHAR(50)        
               
 SELECT              
  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')              
  ,@CustomerBillID = C.value('(CustomerBillID)[1]','INT')              
 FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
          SET @Month=(SELECT BillMonth FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)         
   SET @Year= (SELECT BillYear FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)      
    SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
    IF(@AccountNo='')  
        SELECT @AccountNo=AccountNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillID  
 SET @TariffId=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)             
  SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WHERE GlobalAccountNumber=@AccountNo)  
  
  DECLARE @TotalConsumption DECIMAL(18,2) = 0
  
  SELECT @TotalConsumption = SUM(AdjustedUnits) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 3
             
 SELECT (               
   SELECT       -- Get bill details for meter reading adjustment         
       B.AccountNo,              
       B.TotalBillAmount,                               
       B.BillNo,              
       B.PreviousReading ,              
       B.PresentReading AS PresentReading,              
       B.Usage AS Consumption,        
       B.TotalBillAmountWithTax,    
       (SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials,      
       CONVERT(DECIMAL(18,2),B.VATPercentage) AS TaxPercent,         
       (SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffId) AS Tariff,        
       (Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo) AS TariffId,        
       CONVERT(DECIMAL(18,2),B.NetFixedCharges) AS FixedCharges,               
       @ReadCode AS ReadCodeId,            
       NetEnergyCharges,  
      (SELECT dbo.fn_GetPreviousBalanceUsage(B.AccountNo)) AS BalanceUsage  
	  ,ISNULL(MI.MeterMultiplier,1) AS MeterMultiplier     
	  ,B.VAT AS Vat
	  ,@TotalConsumption AS GrandTotal 
    FROM Tbl_CustomerBills B    
    left JOIN Tbl_MeterInformation AS MI ON B.MeterNo=MI.MeterNo              
    WHERE B.CustomerBillID=@CustomerBillID         
    FOR XML PATH('CustomerBillDetails'),Type             
    ) ,        
    (  
     -- Get all tariff details for meter reading adjustment        
       --SELECT ClassID AS TariffId,ClassName AS Tariff FROM Tbl_MTariffClasses WHERE IsActiveClass=1       
    SELECT   
     ClassID,FromKW ,(CASE ISNULL(ToKW,'') WHEN '' THEN '&#8734;' ELSE CONVERT(varchar(10),ToKW) END)AS ToKW,Amount,TaxId   
    FROM Tbl_LEnergyClassCharges       
    WHERE ClassID  = (SELECT A.TariffClassID FROM CUSTOMERS.Tbl_CustomerProceduralDetails A WHERE GlobalAccountNumber = @AccountNo)        
    AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)       
    ORDER BY FromKW ASC     
    FOR XML PATH('CustomerBillDetails'),Type        
   )        
     FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                    
             
              
END  
-------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentLogsToApprove]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- MODIFIED BY:  NEERAJ KANOJIYA
-- Create date: 29-JUNE-2015
-- Description: CHANGE BILL ID TO BILL NO BY JOINING CUSTOMERBILL TBL, ADDED REQUEST DATE FIELS AND AND ORDER BY REQUEST DATE
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAdjustmentLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
      ,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			--ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			ROW_NUMBER() OVER (ORDER BY T.AdjustmentLogId desc) AS RowNumber
			,T.AdjustmentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			--,T.BillNumber
			,CSTB.BillNo AS BillNumber
			,ISNULL(BA.Name,'Adjustment(NoBill)') AS BillAdjustmentType
			,T.MeterNumber 
			,T.PreviousReading
			,T.CurrentReadingAfterAdjustment
			,T.CurrentReadingBeforeAdjustment 
			,T.AdjustedUnits
			,T.TaxEffected
			,T.TotalAmountEffected
			,T.EnergryCharges
			,T.AdditionalCharges
			,T.Remarks AS Details
			,CONVERT(VARCHAR(12),T.CreatedDate,106) AS RequestDate 
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_AdjustmentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	--INNER JOIN Tbl_BillAdjustmentType BAT ON BAT.Name= T.BillAdjustmentTypeId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	LEFT JOIN Tbl_BillAdjustmentType BA ON BA.BATID = T.BillAdjustmentTypeId
	LEFT JOIN Tbl_CustomerBills AS CSTB ON T.BillNumber = CSTB.CustomerBillId
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
	ORDER BY T.CreatedDate desc
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAssignMeterChangeApproval]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: Update AssignMeter change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAssignMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@AssignMeterChangeId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(MAX) 
		,@BU_ID VARCHAR(50) 		

	SELECT  
		 @AssignMeterChangeId = C.value('(AssignMeterChangeId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(MAX)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
DECLARE
		 @GlobalAccountNo VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@AssignedMeterDate DATETIME
		,@InitialReading DECIMAL(18,2)
		,@Remarks VARCHAR(MAX)
		--,@PresentApprovalRole INT
		--,@NextApprovalRole INT
		,@CreatedBy VARCHAR(50)
		,@CreatedDate DATETIME
		,@IsCAPMIMeter BIT
		,@CAPMIAmount DECIMAL(18,2)
	
	SELECT 
		@GlobalAccountNo	 =GlobalAccountNo 
       ,@MeterNo             =MeterNo 
       ,@AssignedMeterDate   =AssignedMeterDate 
       ,@InitialReading      =InitialReading
       ,@Remarks             =Remarks
       ,@CreatedBy           =CreatedBy 
       ,@CreatedDate         =CreatedDate
       ,@IsCAPMIMeter		 =ISNULL(IsCAPMIMeter,0)
       ,@CAPMIAmount		 =ISNULL(CAPMIAmount,0)
	FROM Tbl_AssignedMeterLogs
	WHERE AssignedMeterId = @AssignMeterChangeId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AssignedMeterLogs 
					--						WHERE AssignedMeterId = @AssignMeterChangeId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AssignedMeterLogs 
											WHERE AssignedMeterId = @AssignMeterChangeId)
					

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
									SET MeterNumber=@MeterNo
										,ReadCodeID=2
							WHERE GlobalAccountNumber = @GlobalAccountNo
			
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							SET InitialReading=@InitialReading
								,PresentReading=@InitialReading
								,IsCAPMI=@IsCAPMIMeter
								,MeterAmount=@CAPMIAmount
							WHERE GlobalAccountNumber=@GlobalAccountNo

							
							INSERT INTO Tbl_Audit_AssignedMeterLogs(  
										 AssignedMeterId
										,GlobalAccountNo
										,MeterNo
										,AssignedMeterDate
										,InitialReading
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole
										,IsCAPMIMeter
										,CAPMIAmount)  
									VALUES(  
										 @AssignMeterChangeId
										,@GlobalAccountNo
										,@MeterNo
										,@AssignedMeterDate
										,@InitialReading
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId
										,@IsCAPMIMeter
										,@CAPMIAmount
										)
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_AssignedMeterLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
					WHERE GlobalAccountNo = @GlobalAccountNo  
					AND AssignedMeterId = @AssignMeterChangeId 

					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNo
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNo
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
						,IsCAPMI=@IsCAPMIMeter
						,MeterAmount=@CAPMIAmount
					WHERE GlobalAccountNumber=@GlobalAccountNo
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,IsCAPMIMeter
						,CAPMIAmount)   
					VALUES(  
						 @AssignMeterChangeId
						,@GlobalAccountNo
						,@MeterNo
						,@AssignedMeterDate
						,@InitialReading
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy  
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId
						,@IsCAPMIMeter
						,@CAPMIAmount
						)
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AssignedMeterLogs 
							--									WHERE AssignedMeterId = @AssignMeterChangeId))
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AssignedMeterLogs 
																WHERE AssignedMeterId = @AssignMeterChangeId )

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AssignedMeterLogs 
						WHERE AssignedMeterId = @AssignMeterChangeId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AssignedMeterLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND AssignedMeterId = @AssignMeterChangeId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT	
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		 ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE IF((SELECT COUNT(0)FROM Tbl_CustomerMeterInfoChangeLogs 
				WHERE NewMeterNo = @MeterNumber AND ApproveStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterChangeApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END  
	IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
						DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1		
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
						,IsCAPMI = @IsCAPMIMeter
						,MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END


GO

/****** Object:  StoredProcedure [MASTERS].[USP_Re-AssignMeter]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 5-MARCH-2015  
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_Re-AssignMeter]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
		  @MeterNumber VARCHAR(50)    
		  ,@ReadCodeID VARCHAR(50)   
		  ,@GlobalAccountNumber VARCHAR(50)   
		  ,@IsSuccessful BIT =1  
		  ,@ApprovalStatusId INT
		  ,@IsFinalApproval BIT = 0
		  ,@FunctionId INT
		  ,@Reason VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50)
		  ,@InitialReading INT
     ,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
  SELECT    
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
    ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
	IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE MeterNumber=@MeterNumber)
		BEGIN
			SELECT 1 AS IsMeterAlreadyAssigned FOR XML PATH('CustomerRegistrationBE')
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo=@MeterNumber AND ApprovalStatusId IN (1,4))
		BEGIN
			SELECT 1 AS IsMeterReqByAnotherCust FOR XML PATH('CustomerRegistrationBE')
		END
 	ELSE IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
	IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				BEGIN
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										DECLARE @UserDetailedId INT
										SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
									SET @CurrentApprovalLevel = @CurrentLevel+1
					--SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
			
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1
				
			
			SET @InitialReading=(SELECT ISNULL(ISNULL(PresentReading,0),ISNULL(InitialReading,0)) 
			FROM CUSTOMERS.Tbl_CustomerActiveDetails
			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,dbo.fn_GetCurrentDateTime()
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN					
				IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
					BEGIN
					
						UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
							 SET MeterNumber=@MeterNumber  
							  ,ReadCodeID=@ReadCodeID  
						WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						SET IsCAPMI = @IsCAPMIMeter
							,MeterAmount = @CAPMIAmount
						WHERE GlobalAccountNumber=@GlobalAccountNumber 
						
						INSERT INTO Tbl_Audit_AssignedMeterLogs(  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks
								,ApprovalStatusId
								,CreatedBy
								,CreatedDate
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
								,IsCAPMIMeter
								,CAPMIAmount)  
							SELECT  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks  
								,ApprovalStatusId  
								,CreatedBy  
								,CreatedDate  
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
								,IsCAPMIMeter
								,CAPMIAmount
							FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
						
						SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')	
					END
				ELSE
					BEGIN
						SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
					END
				END
			ELSE 
				BEGIN
					SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
				END
		END
END
		
--DECLARE   
--		  @MeterNumber VARCHAR(50)    
--		  ,@ReadCodeID VARCHAR(50)   
--		  ,@GlobalAccountNumber VARCHAR(50)   
--		  ,@IsSuccessful BIT =1  
--		  ,@StatusText VARCHAR(200)
     
--BEGIN  
--BEGIN TRY  
-- BEGIN TRAN  
     
-- SET @StatusText='Deserialization'  
--  SELECT    
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
--		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    
--  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
-- IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
--	 BEGIN 
--		 SET @StatusText='Update Statement'  
		   
--		 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
--		 SET MeterNumber=@MeterNumber  
--		  ,ReadCodeID=@ReadCodeID  
--		 WHERE GlobalAccountNumber = @GlobalAccountNumber  
		   	   
--		 SET @StatusText='Success'
--	 END
-- ELSE 
--	BEGIN
--		SET @StatusText='Meter is in DeActivate state.'
--		SET @IsSuccessful =0  
--	END 
-- COMMIT TRAN   
--END TRY      
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @IsSuccessful =0  
--END CATCH  
--END  
-- SELECT   
--   @IsSuccessful AS IsSuccessful  
--   ,@StatusText AS StatusText  
-- FOR XML PATH('CustomerRegistrationBE'),TYPE  
  
--END  
--------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
		
	
	DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
	SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), 
		@CAPMIAmount = ISNULL(CAPMIAmount,0),
		@MeterTypeId = MeterType
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNo AND ActiveStatusId = 1		
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID AND ActiveStatusId = 1)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(2,3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @AccountNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsReadToDirectApproval FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((CONVERT(DECIMAL(18,2),@OldMeterReading) < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,MI.MeterType
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			--INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage >= 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END,
				IsCAPMI = @IsCAPMIMeter,
				MeterAmount = @CAPMIAmount
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END



GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading BIGINT
		,@NewMeterReading BIGINT
		,@PreviousReading BIGINT
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading BIGINT
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
		,@IsCAPMIMeter BIT = 0
		,@CAPMIAmount DECIMAL(18,2) = 0
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = CAST(OldMeterReading AS NUMERIC)
		,@NewMeterReading = CAST(NewMeterReading AS NUMERIC)
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = CAST(NewMeterInitialReading AS NUMERIC)
		,@NewMeterReadingDate = NewMeterReadingDate
		,@IsCAPMIMeter = ISNULL(IsCAPMIMeter,0)
		,@CAPMIAmount = ISNULL(CAPMIAmount,0)
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_New(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId ANd IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
					--						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

				DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
								,IsCAPMIMeter
								,CAPMIAmount)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
								,IsCAPMIMeter
								,CAPMIAmount
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage >= 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading,
								IsCAPMI = @IsCAPMIMeter,
								MeterAmount = @CAPMIAmount
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked = 1
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage >= 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading,
						IsCAPMI = @IsCAPMIMeter,
						MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
							--									WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCashOffices_ByBU_ID]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <29-Jun-2015>
-- Description:	<Get CashOffices>
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCashOffices_ByBU_ID]
	(@XmlDoc xml=null)
AS
BEGIN
	DECLARE @BU_ID VARCHAR(MAX)

	SELECT @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
	
	FROM @XmlDoc.nodes('BillingBE') AS T(C)	

	IF(@BU_ID != '')
	BEGIN
		SELECT (
			SELECT CashOfficeId as OfficeId 
					,CashOffice as OfficeName 
			FROM Tbl_CashOffices 
			WHERE ActiveStatusId=1
			AND	(BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,',')) OR @BU_ID='')
		FOR XML PATH('BillingBE'),TYPE
		)
		FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
	END
	ELSE
	BEGIN
		SELECT (
				SELECT CashOfficeId as OfficeId 
						,CashOffice as OfficeName 
				FROM Tbl_CashOffices WHERE ActiveStatusId=1
			FOR XML PATH('BillingBE'),TYPE
			)
			FOR XML PATH(''),ROOT('GlobalMessageBEInfoByXml')
	 END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterDialsByMeterNo]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 26-02-2015
-- Description: The purpose of this Procedure is to MeterDials By Meter No
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterDialsByMeterNo]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
 DECLARE @MeterNo VARCHAR(100),@BUID VARCHAR(50)
 SELECT  
  @MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)
 
	--DECLARE @MeterDials INT
		
	--SET @MeterDials=(SELECT ISNULL(MeterDials,0) 
	--FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	--SELECT @MeterDials AS MeterDials,1 As IsSuccess 
	
	Declare @MeterAvaiable bit = 1  
		  ,@MeterDials INT
		  ,@IsCAMPIMeter BIT = 0
		  ,@CAPMIAmount DECIMAL(18,2) = 0
  
	  IF NOT EXISTS (SELECT M.MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2  
		  AND M.ActiveStatusId IN(1)   
		  AND (BU_ID=@BUID OR @BUID='')        
		  AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )    
	   BEGIN  
		 SET @MeterAvaiable=0    
	   END 
	   ELSE IF EXISTS (SELECT M.NewMeterNo from Tbl_CustomerMeterInfoChangeLogs M where  M.NewMeterNo =@MeterNo   
		  AND M.ApproveStatusId IN(1,4))    
	   BEGIN  
		 SET @MeterAvaiable=0    
	   END 
		ELSE IF EXISTS (SELECT M.MeterNo from Tbl_AssignedMeterLogs M where  M.MeterNo =@MeterNo   
		  AND M.ApprovalStatusId IN(1,4))    
	   BEGIN  
		 SET @MeterAvaiable=0    
	   END 
	   ELSE
	   BEGIN
			SELECT @MeterDials=M.MeterDials,@IsCAMPIMeter = IsCAPMIMeter, @CAPMIAmount = CAPMIAmount
			FROM Tbl_MeterInformation M WHERE MeterNo=@MeterNo AND ActiveStatusId = 1
	   END
	  SELECT @MeterAvaiable AS IsSuccess, @MeterDials AS MeterDials
			,@IsCAMPIMeter AS IsCAPMIMeter, @CAPMIAmount AS MeterAmt
	
 FOR XML PATH('ConsumerBe')  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCashiersList]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz-ID103>      
-- Create date: <27-Jun-2015>      
-- Description: <Retriving Marketers records form Tbl_Cashier>      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetCashiersList]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
  DECLARE  @PageNo INT      
    ,@PageSize INT    
    ,@BU_ID VARCHAR(50)  
  SELECT         
   @PageNo = C.value('(PageNo)[1]','INT')      
   ,@PageSize = C.value('(PageSize)[1]','INT')       
   ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')       
  FROM @XmlDoc.nodes('CashierBE') AS T(C)      
    
  ;WITH PagedResults AS      
  (      
   SELECT      
     ROW_NUMBER() OVER(ORDER BY C.ActiveStatusId ASC , C.CreatedDate DESC ) AS RowNumber   
     ,CashierId   
     ,FirstName  
     ,MiddleName  
     ,LastName  
     ,ContactNumber AS ContactNo1  
     ,AlternateContactNumber AS ContactNo2  
     ,Address1  
     ,Address2  
     ,City  
     ,ZipCode  
     ,EmailId  
     ,Notes
     ,C.CashOfficeId  
     ,CO.CashOffice
     ,C.ActiveStatusId      
   FROM Tbl_Cashier C   
   JOIN Tbl_CashOffices CO ON CO.CashOfficeId = C.CashOfficeId
   WHERE C.ActiveStatusId IN(1,2) 
   AND (BU_ID = @BU_ID OR @BU_ID='')     
  )      
        
  SELECT       
    (      
   SELECT *      
     ,(Select COUNT(0) from PagedResults) as TotalRecords           
   FROM PagedResults      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
   FOR XML PATH('CashierBE'),TYPE      
  )      
  FOR XML PATH(''),ROOT('CashierBeInfoByXml')       
END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetDisconnectedMeterDetials]    Script Date: 06/29/2015 15:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 23-MARCH-2014  
-- Description: The purpose of this procedure is to get list of MeterInformation  
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_GetDisconnectedMeterDetials]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
		DECLARE
			@GlobalAccountNo varchar(50)
			,@BUID VARCHAR(50)  
			,@MeterNo VARCHAR(50)
			SELECT     
			@GlobalAccountNo = C.value('(AccountNo)[1]','VARCHAR(50)') 
			,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
			FROM @XmlDoc.nodes('MastersBE') AS T(C) 			
  
		SET @MeterNo=(SELECT  TOP 1
							MeterNumber
							FROM TBL_ReadToDirectCustomerActivity 
							WHERE GlobalAccountNo=@GlobalAccountNo AND IsLatest=1
							ORDER BY CreatedDate DESC)
			
		IF NOT EXISTS (SELECT MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2
																  AND M.ActiveStatusId IN(1,2) 
																  AND (BU_ID=@BUID OR @BUID='')
																  AND M.MeterNo IN(SELECT CD.MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD)      
																  --AND M.MeterNo NOT IN (SELECT MeterNumber FROM TBL_ReadToDirectCustomerActivity 
																		--					WHERE GlobalAccountNo=@GlobalAccountNo AND IsLatest=1)
																							)  
		  BEGIN
			
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), 
				@CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNo AND ActiveStatusId = 1		
			
		   	SELECT  TOP 1
				 GlobalAccountNo AS GlobalAccountNumber
				,MeterNumber
				,CreatedDate 
				,@IsCAPMIMeter AS IsCAPMI
				,@CAPMIAmount AS MeterAmount
			FROM TBL_ReadToDirectCustomerActivity 
			WHERE GlobalAccountNo=@GlobalAccountNo AND IsLatest=1
			ORDER BY CreatedDate DESC
			FOR XML PATH('CustomerRegistrationBE'),TYPE	
		  END	  	
		 ELSE
			SELECT 0
			FOR XML PATH('CustomerRegistrationBE'),TYPE	
END
----------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCashiersForDdl]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Faiz-ID103  
-- Create date: 29-Jun-2015
-- Description: Purpose is To get List Of Cashiers for dropdownlist
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCashiersForDdl]    
(    
 @xmlDoc Xml=null    
)    
AS    
BEGIN    
     
 DECLARE @CashOfficeId VARCHAR(50)    
 SELECT @CashOfficeId=C.value('(CashOfficeId)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('CashierBE') as T(C)    
 SELECT    
 (    
  SELECT   CashierId    
    ,dbo.fn_GetCashiersFullName(CashierId) AS Cashier    
  FROM Tbl_Cashier    
  --WHERE ActiveStatusId IN(1,2)  
  WHERE ActiveStatusId =1   
  AND CashOfficeId=@CashOfficeId
  FOR XML PATH('CashierBE'),TYPE    
 )    
 FOR XML PATH(''),ROOT('CashierBeInfoByXml')    
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCashiers]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz - ID103>        
-- Create date: <26-Jun-2015>        
-- Description: <Update Cashier From AddCashier to Tbl_Cashier>        
-- =============================================        
ALTER PROCEDURE [dbo].[USP_UpdateCashiers]        
 (        
 @XmlDoc XML     
)        
AS        
BEGIN         
   
 DECLARE        
 
  @FirstName		VARCHAR(50)
 ,@LastName			VARCHAR(50)     
 ,@MiddleName		VARCHAR(50)   
 ,@ContactNo1		VARCHAR(20)   
 ,@ContactNo2		VARCHAR(20)   
 ,@Address1			VARCHAR(MAX)     
 ,@Address2			VARCHAR(MAX)     
 ,@City				VARCHAR(50)         
 ,@ZipCode			VARCHAR(50)      
 ,@Notes 			VARCHAR(MAX)       
 ,@EmailId			VARCHAR(MAX)   
 ,@CashierId		INT     
 ,@ModifiedBy		VARCHAR(50) 
 ,@CashOfficeId		INT
            
 SELECT         
		  @FirstName =C.value('(FirstName)[1]','VARCHAR(50)')
         ,@LastName =C.value('(LastName)[1]','VARCHAR(50)')
         ,@MiddleName =C.value('(MiddleName)[1]','VARCHAR(50)')
         ,@ContactNo1 =C.value('(ContactNo1)[1]','VARCHAR(20)')
         ,@ContactNo2 =C.value('(ContactNo2)[1]','VARCHAR(20)')
         ,@Address1 =C.value('(Address1)[1]','VARCHAR(MAX)')
         ,@Address2 =C.value('(Address2)[1]','VARCHAR(MAX)')
         ,@City =C.value('(City)[1]','VARCHAR(50)')
         ,@ZipCode =C.value('(ZipCode)[1]','VARCHAR(50)')
         ,@Notes =C.value('(Notes)[1]','VARCHAR(MAX)')
         ,@EmailId =C.value('(EmailId)[1]','VARCHAR(MAX)')
         ,@CashOfficeId =C.value('(CashOfficeId)[1]','INT')
         ,@CashierId =C.value('(CashierId)[1]','INT')
         ,@ModifiedBy =C.value('(ModifiedBy)[1]','VARCHAR(50)')      
 FROM @XmlDoc.nodes('CashierBE') as T(C)         
       
 IF NOT EXISTS(SELECT 0 FROM Tbl_Cashier WHERE EmailId=@EmailId AND CashierId!=@CashierId)        
 BEGIN         
   UPDATE Tbl_Cashier 
		SET FirstName = @FirstName		
			,LastName = @LastName			
			,MiddleName = @MiddleName		
			,ContactNumber = @ContactNo1		
			,AlternateContactNumber = @ContactNo2		
			,Address1= @Address1			
			,Address2= @Address2			
			,City= @City				
			,ZipCode= @ZipCode			
			,Notes= @Notes			
			,EmailId= @EmailId
			,ModifiedBy= @ModifiedBy
			,ModifiedDate= dbo.fn_GetCurrentDateTime()
			,CashOfficeId = @CashOfficeId   
        WHERE CashierId = @CashierId      
                  
 SELECT @@ROWCOUNT As RowsEffected        
    FOR XML PATH('CashierBE'),TYPE  
 END       
 ELSE
 BEGIN
	SELECT 1 AS IsEmailIdExists        
    FOR XML PATH('CashierBE'),TYPE  
 END
END        
-----------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteCashiers]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Faiz - ID103>      
-- Create date: <27-Jun-2015>      
-- Description: <Delete Cashier Row By CashierId in Tbl_Cashier>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_DeleteCashiers]   
(  
 @XmlDoc XML  
)  
AS  
BEGIN  
 DECLARE @CashierId INT  
   ,@ActiveStatusId INT  
   ,@ModifiedBy VARCHAR(50)  

   
 SELECT @CashierId = C.value('(CashierId)[1]','VARCHAR(50)')  
    ,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')  
    ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CashierBE') AS T(C)  

   UPDATE Tbl_Cashier   
   SET ActiveStatusId = @ActiveStatusId,  
    ModifiedBy = @ModifiedBy,  
    ModifiedDate = dbo.fn_GetCurrentDateTime()  
   WHERE CashierId = @CashierId  
 
 SELECT @@ROWCOUNT As RowsEffected    
    FOR XML PATH('CashierBE'),TYPE     
    
END

GO

/****** Object:  StoredProcedure [dbo].[USP_MasterSearch]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Bhimaraju Vanka      
-- Create date: 15-Nov-2014      
-- Description: This is the proc about search masters using flag      
-- MODIFIED BY : NEERAJ KANOJIYA      
-- MODIFIED DATE: 17-NOV-2014      
-- Modified By: Padmini      
-- Modified Date:17-Feb-2015      
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order      
-- Modified By: Faiz      
-- Modified Date:27-Mar-2015    
-- Description: Modified BookNo and Id for flag-5 for book no search    
-- Modified By: Faiz      
-- Modified Date:03-Apr-2015  
-- Description: Added Meter Type to search with meter type in Meter Information Search      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_MasterSearch]      
 (@XmlDoc Xml=null)      
AS      
BEGIN      
 DECLARE  @BusinessUnit VARCHAR(50)      
   ,@BusinessUnitCode VARCHAR(50)      
   ,@ServiceUnit VARCHAR(50)      
   ,@ServiceUnitCode VARCHAR(50)      
   ,@ServiceCenter VARCHAR(50)      
   ,@ServiceCenterCode VARCHAR(50)      
   ,@CycleName VARCHAR(50)      
   ,@CycleCode VARCHAR(50)      
   ,@BookNo VARCHAR(50)      
   ,@BookCode VARCHAR(50)      
   ,@RouteName VARCHAR(50)      
   ,@RouteCode VARCHAR(50)      
   ,@MeterNo VARCHAR(50)      
   ,@MeterSerialNo VARCHAR(50)      
   ,@Brand VARCHAR(50)    
   ,@MeterType VARCHAR(50)  --Faiz-ID103  
   ,@Name VARCHAR(50)      
   ,@ContactNo VARCHAR(50)      
   ,@RoleId INT      
   ,@DesignationId INT      
   ,@PageNo INT      
   ,@PageSize INT      
   ,@Flag INT      
   ,@BU_ID  VARCHAR(50)      
         
 SELECT   @BusinessUnit=C.value('(BusinessUnit)[1]','VARCHAR(50)')      
   ,@BusinessUnitCode=C.value('(BusinessUnitCode)[1]','VARCHAR(50)')      
   ,@ServiceUnit=C.value('(ServiceUnit)[1]','VARCHAR(50)')      
   ,@ServiceUnitCode=C.value('(ServiceUnitCode)[1]','VARCHAR(50)')      
   ,@ServiceCenter=C.value('(ServiceCenter)[1]','VARCHAR(50)')      
   ,@ServiceCenterCode=C.value('(ServiceCenterCode)[1]','VARCHAR(50)')      
   ,@CycleName=C.value('(CycleName)[1]','VARCHAR(50)')      
   ,@CycleCode=C.value('(CycleCode)[1]','VARCHAR(50)')      
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')      
   ,@BookCode=C.value('(BookCode)[1]','VARCHAR(50)')      
   ,@RouteName=C.value('(RouteName)[1]','VARCHAR(50)')      
   ,@RouteCode=C.value('(RouteCode)[1]','VARCHAR(50)')      
   ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')      
   ,@MeterSerialNo=C.value('(MeterSerialNo)[1]','VARCHAR(50)')      
   ,@Brand=C.value('(Brand)[1]','VARCHAR(50)')       
   ,@MeterType=C.value('(MeterType)[1]','VARCHAR(50)')    --Faiz-ID103  
   ,@Name=C.value('(Name)[1]','VARCHAR(50)')      
   ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(50)')      
   ,@RoleId=C.value('(RoleId)[1]','INT')      
   ,@DesignationId=C.value('(DesignationId)[1]','INT')      
   ,@PageNo=C.value('(PageNo)[1]','INT')      
   ,@PageSize=C.value('(PageSize)[1]','INT')      
   ,@Flag=C.value('(Flag)[1]','INT')         
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')         
  FROM @XmlDoc.nodes('MasterSearchBE') as T(C)      
        
  --============================== ** Note ** ==========================      
  --@Flag = 1 --Bussiness Unit Search      
  --@Flag = 2 --Service Unit Search      
  --@Flag = 3 --Service Center Search      
  --@Flag = 4 --Cycle Search      
  --@Flag = 5 --Book Search      
  --@Flag = 6 --Route Management Search      
  --@Flag = 7 --Meter Information Search      
  --@Flag = 8 --User Search      
  --@Flag = 9 --Change Customer BU      
  --============================== ** Note ** ==========================      
        
  IF (@Flag = 1)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
    SELECT B.BU_ID      
      ,B.BusinessUnitName      
      ,ROW_NUMBER() OVER(ORDER BY B.BU_ID ASC) AS RowNumber      
      ,ISNULL(B.BUCode,'-') AS BusinessUnitCode      
      ,B.Notes      
      ,(S.StateName +' ( '+ S.StateCode +' )')  AS StateName      
      ,S.StateCode      
    FROM Tbl_BussinessUnits B      
    LEFT JOIN Tbl_States S ON S.StateCode=B.StateCode      
    WHERE       
    (ISNULL(BusinessUnitName,'') like '%'+@BusinessUnit+'%' OR @BusinessUnit='')      
    AND (ISNULL(BUCode,'') like '%'+@BusinessUnitCode+'%' OR @BusinessUnitCode='')      
    --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
    AND (B.BU_ID=@BU_ID OR @BU_ID='')      
  )        
   SELECT        
    *          
    ,(Select COUNT(0) from PagedResults) as TotalRecords      
    FROM PagedResults p      
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 2)      
  BEGIN      
  ;WITH PagedResults AS      
  (      
   SELECT  --S.SU_ID      
     S.ServiceUnitName      
     ,ROW_NUMBER() OVER(ORDER BY S.SU_ID ASC) AS RowNumber      
     ,ISNULL(S.SUCode,'--') AS ServiceUnitCode      
     ,S.Notes      
     ,B.BusinessUnitName      
     ,B.StateCode      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_ServiceUnits S      
   LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode      
   WHERE (ISNULL(ServiceUnitName,'') like '%'+@ServiceUnit+'%' OR @ServiceUnit='')      
   AND (ISNULL(SUCode,'') like '%'+@ServiceUnitCode+'%' OR @ServiceUnitCode='')      
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (B.BU_ID=@BU_ID OR @BU_ID='')      
  )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
  END      
  ELSE IF(@Flag = 3)      
  BEGIN      
  ;WITH PagedResults AS      
  (      
   SELECT  --C.ServiceCenterId      
     C.ServiceCenterName      
     ,ROW_NUMBER() OVER(ORDER BY C.ServiceCenterId ASC) AS RowNumber      
     ,C.SCCode      
     ,C.Notes      
     ,S.ServiceUnitName      
     ,B.StateCode      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_ServiceCenter C      
   LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=C.SU_ID      
   LEFT JOIN Tbl_BussinessUnits B ON S.BU_ID=B.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode      
   WHERE (ISNULL(ServiceCenterName,'') like '%'+@ServiceCenter+'%' OR @ServiceCenter='')      
   AND (ISNULL(SCCode,'') like '%'+@ServiceCenterCode+'%' OR @ServiceCenterCode='')      
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (B.BU_ID=@BU_ID OR @BU_ID='')      
  )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
  END      
  ELSE IF(@Flag = 4)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  --C.CycleId      
     ROW_NUMBER() OVER(ORDER BY C.CycleId ASC) AS RowNumber      
     ,C.CycleCode      
     ,C.CycleName      
     ,C.ContactName      
     ,C.ContactNo      
     ,C.DetailsOfCycle      
     ,B.BusinessUnitName AS BusinessUnit      
     ,S.ServiceUnitName AS ServiceUnit      
     ,SC.ServiceCenterName AS ServiceCenter      
     ,B.StateCode      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_Cycles C      
   LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId      
   LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID      
   LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode      
   WHERE (ISNULL(CycleCode,'') like '%'+@CycleCode+'%' OR @CycleCode='')      
   AND (ISNULL(CycleName,'') like '%'+@CycleName+'%' OR @CycleName='')      
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (B.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
         
  END      
  ELSE IF(@Flag = 5)      
  BEGIN      
  ;WITH PagedResults AS      
  (      
   SELECT  --B.BookNo --Faiz-ID103    
   B.ID AS BookNo     
     ,B.BookCode      
     ,B.Details      
     ,B.NoOfAccounts      
     ,ROW_NUMBER() OVER(ORDER BY B.BookNo ASC) AS RowNumber      
     ,C.CycleName      
     ,BU.BusinessUnitName AS BusinessUnit      
     ,SU.ServiceUnitName AS ServiceUnitName      
     ,SC.ServiceCenterName AS ServiceCenterName      
     ,BU.StateCode      
     ,BU.BusinessUnitName      
     ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName      
   FROM Tbl_BookNumbers B      
   LEFT JOIN Tbl_Cycles C ON C.CycleId=B.CycleId      
   LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId      
   LEFT JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID      
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID      
   LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode      
   WHERE (ISNULL(BookCode,'') like '%'+@BookCode+'%' OR @BookCode='')      
   AND (ISNULL(B.ID,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103    
   --AND (ISNULL(BookNo,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103    
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 6)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  R.RouteName      
     ,R.RouteCode      
     ,R.Details      
     ,ROW_NUMBER() OVER(ORDER BY R.RouteName ASC) AS RowNumber      
     ,BU.BusinessUnitName AS BusinessUnit      
     ,BU.StateCode      
   FROM Tbl_MRoutes R      
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=R.BU_ID      
   WHERE (ISNULL(RouteName,'') like '%'+@RouteName+'%' OR @RouteName='')      
   AND (ISNULL(RouteCode,'') like '%'+@RouteCode+'%' OR @RouteCode='')      
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)      
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 7)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  M.MeterNo      
     ,M.MeterSerialNo      
     ,MT.MeterType      
     ,M.MeterSize      
     ,M.MeterBrand      
     ,M.MeterRating      
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber      
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate      
     ,M.MeterDials      
     ,M.MeterDetails      
     ,M.MeterMultiplier      
     ,M.MeterId      
     ,M.BU_ID     
     ,M.IsCAPMIMeter
     ,M.CAPMIAmount
    ,B.BusinessUnitName      
   FROM Tbl_MeterInformation M      
   JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType AND MT.ActiveStatus=1      
   LEFT JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID       
   WHERE (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')      
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')      
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')      
   AND (ISNULL(MT.MeterType,'')=@MeterType OR @MeterType='')    --Faiz-ID103  
   AND (M.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 8)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  U.UserId AS EmployeeId    
     ,U.Name      
     ,ROW_NUMBER() OVER(ORDER BY U.UserId ASC) AS RowNumber      
     ,U.SurName      
     ,U.PrimaryContact AS ContactNo      
     ,U.PrimaryEmailId      
     ,D.DesignationName AS Designation      
     ,R.RoleName      
     ,A.Status       
   FROM Tbl_UserDetails U      
   JOIN Tbl_MDesignations D ON D.DesignationId=U.DesignationId      
   JOIN Tbl_MRoles R ON R.RoleId=U.RoleId      
   JOIN Tbl_UserLoginDetails L ON L.UserId=U.UserId      
   JOIN Tbl_MActiveStatusDetails A ON L.ActiveStatusId=A.ActiveStatusId      
   AND L.ActiveStatusId IN (1,2)      
   AND ((ISNULL(Name,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(SurName,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(PrimaryEmailId,'') like '%'+@Name+'%' OR @Name=''))      
   AND (ISNULL(PrimaryContact,'') like '%'+@ContactNo+'%' OR @ContactNo='')      
   AND (ISNULL(R.RoleId,0)=@RoleId OR @RoleId=0)      
   AND (ISNULL(D.DesignationId,0)=@DesignationId OR @DesignationId=0)         
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
  ELSE IF(@Flag = 9)      
  BEGIN      
   ;WITH PagedResults AS      
  (      
   SELECT  M.MeterNo      
     ,M.MeterSerialNo      
     ,MT.MeterType      
     ,M.MeterSize      
     ,M.MeterBrand      
     ,M.MeterRating      
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber      
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate      
     ,M.MeterDials      
     ,M.MeterDetails      
     ,M.MeterMultiplier      
     ,M.MeterId      
     ,M.BU_ID      
    ,B.BusinessUnitName      
   FROM Tbl_MeterInformation M      
   LEFT JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType      
   JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID      
   AND M.MeterNo NOT IN (SELECT CPD.MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD WHERE CPD.MeterNumber IS Not NULL)      
   AND M.ActiveStatusId IN(1,2)      
   AND (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')      
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')      
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')    
   AND (M.BU_ID=@BU_ID OR @BU_ID='')      
   )      
        
  SELECT        
   *          
   ,(Select COUNT(0) from PagedResults) as TotalRecords      
   FROM PagedResults p      
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize      
  END      
 END      

GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterInformation]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 08-04-2014  
-- Description: The purpose of this procedure is to get list of MeterInformation  
-- Author:  V.Bhimaraju  
-- Modified date: 04-02-2015  
-- Description: Added GlobalAccount No in this proc  
-- Author:  Faiz-ID103
-- Modified date: 20-APR-2015
-- Description: Modified the SP for active meter Type filter
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterInformation]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT  
   ,@BU_ID VARCHAR(20)  
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID= C.value('(BU_ID)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY M.ActiveStatusId ASC , M.CreatedDate DESC ) AS RowNumber  
    ,MeterId  
    ,MeterNo  
    ,M.MeterType AS MeterTypeId  
    --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType  
    ,MT.MeterType AS MeterType --Faiz-ID103
    ,MeterSize  
    ,ISNULL(MeterDetails,'---') AS Details  
    ,M.ActiveStatusId  
    ,M.CreatedBy  
    ,MeterMultiplier  
    ,MeterBrand  
    ,MeterSerialNo  
    ,MeterRating  
    ,CONVERT(VARCHAR(50),NextCalibrationDate,103) AS BillDate  
    ,ISNULL((CONVERT(VARCHAR(50),NextCalibrationDate,106)),'--') AS NextCalibrationDate  
    ,ISNULL(ServiceHoursBeforeCalibration,'--') AS ServiceHoursBeforeCalibration  
    ,MeterDials  
    ,ISNULL(Decimals,0) AS Decimals  
    ,M.BU_ID  
    ,B.BusinessUnitName  
    ,M.IsCAPMIMeter
    ,M.CAPMIAmount
    ,ISNULL(CPD.GlobalAccountNumber,'--') AS AccountNo  
    FROM Tbl_MeterInformation AS M  
    JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID   
    LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.MeterNumber=M.MeterNo  
    JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 --Faiz-ID103
    WHERE M.ActiveStatusId IN(1,2)  
    AND M.BU_ID=@BU_ID OR @BU_ID=''  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCashiers]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz-ID103
-- Create date: 27-06-2015  
-- Description: The purpose of this procedure is to insert Cashier  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_InsertCashiers]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @FirstName varchar(50)  
    ,@MiddleName varchar(50)  
    ,@LastName varchar(50)  
    ,@ContactNumber varchar(20)  
    ,@AlternateContactNumber varchar(20)  
    ,@Address1 varchar(max)  
    ,@Address2 varchar(max)  
    ,@City varchar(50)  
    ,@Zipcode Varchar(50)  
    ,@EmailId Varchar(max)  
    ,@Notes Varchar(max)  
    ,@CreatedBy varchar(50)  
    ,@CashOfficeId INT  
 SELECT  
   @FirstName=C.value('(FirstName)[1]','VARCHAR(50)')  
  ,@MiddleName=C.value('(MiddleName)[1]','VARCHAR(50)')  
  ,@LastName=C.value('(LastName)[1]','VARCHAR(50)')  
  ,@ContactNumber=C.value('(ContactNo1)[1]','VARCHAR(20)')  
  ,@AlternateContactNumber=C.value('(ContactNo2)[1]','VARCHAR(20)')  
  ,@Address1=C.value('(Address1)[1]','VARCHAR(MAX)')  
  ,@Address2=C.value('(Address2)[1]','VARCHAR(MAX)')  
  ,@City=C.value('(City)[1]','VARCHAR(50)')  
  ,@Zipcode=C.value('(ZipCode)[1]','VARCHAR(50)')  
  ,@EmailId=C.value('(EmailId)[1]','VARCHAR(MAX)')  
  ,@Notes=C.value('(Notes)[1]','VARCHAR(MAX)')  
  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')  
  ,@CashOfficeId=C.value('(CashOfficeId)[1]','INT')  
 FROM @XmlDoc.nodes('CashierBE') as T(C)  
   
 IF EXISTS(SELECT 0 FROM Tbl_Cashier WHERE EmailId=@EmailId)  
 BEGIN  
  SELECT 1 AS IsEmailIdExists FOR XML PATH('CashierBE')  
 END  
 ELSE  
 BEGIN  
  INSERT INTO Tbl_Cashier( FirstName   
          ,MiddleName  
          ,LastName  
          ,ContactNumber  
          ,AlternateContactNumber  
          ,Address1  
          ,Address2  
          ,City  
          ,Zipcode  
          ,EmailId  
          ,Notes  
          ,CreatedDate  
          ,CreatedBy  
          ,CashOfficeId)  
     VALUES(@FirstName  
       ,CASE @MiddleName WHEN '' THEN NULL ELSE @MiddleName END  
       ,@LastName  
       ,@ContactNumber  
       ,CASE @AlternateContactNumber WHEN '' THEN NULL ELSE @AlternateContactNumber END   
       ,@Address1  
       ,CASE @Address2 WHEN '' THEN NULL ELSE @Address2 END   
       ,@City  
       ,@Zipcode  
       ,@EmailId  
       ,CASE @Notes WHEN '' THEN NULL ELSE @Notes END 
       ,dbo.fn_GetCurrentDateTime()
       ,@CreatedBy  
       ,@CashOfficeId)    
  SELECT 1 AS IsSuccess FOR XML PATH('CashierBE')  
 END  
END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_IsMeterAvailable_New]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Modified by : Satya  
-- Modified Date: 6-APRIL-2014    
-- DESC   : MODIFIED CONDITION WITH IS NULL  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_IsMeterAvailable_New]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterNo VARCHAR(100)    
   ,@BUID VARCHAR(50)    
   ,@CustomerTypeId INT    
       
  SELECT       
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')    
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
    
  Declare @MeterAvaiable bit = 1  
		  ,@MeterDials INT
		  ,@IsCAMPIMeter BIT = 0
		  ,@CAPMIAmount DECIMAL(18,2) = 0
  
  IF NOT EXISTS (SELECT M.MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2  
      AND M.ActiveStatusId IN(1)   
      AND (BU_ID=@BUID OR @BUID='')        
      AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE IF EXISTS (SELECT M.NewMeterNo from Tbl_CustomerMeterInfoChangeLogs M where  M.NewMeterNo =@MeterNo   
      AND M.ApproveStatusId IN(1,4))    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
    ELSE IF EXISTS (SELECT M.MeterNo from Tbl_AssignedMeterLogs M where  M.MeterNo =@MeterNo   
      AND M.ApprovalStatusId IN(1,4))    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE
   BEGIN
		SELECT @MeterDials=M.MeterDials,@IsCAMPIMeter = IsCAPMIMeter, @CAPMIAmount = CAPMIAmount
		FROM Tbl_MeterInformation M WHERE MeterNo=@MeterNo AND ActiveStatusId = 1
   END
  SELECT @MeterAvaiable AS IsSuccess, @MeterDials AS MeterDials
		,@IsCAMPIMeter AS IsCAPMIMeter, @CAPMIAmount AS CAPMIAmount
  FOR XML PATH('MastersBE'),TYPE      
       
END    


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- Modified By : Bhimaraju Vanka
-- Desc: Here in Adjustment (No Bill) These details are not having in master table we need do left join 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	
	;WITH PageResult AS(
	SELECT TOP 5 ISNULL(BT.Name,'Adjustment(No Bill)') AS AdjustmentName
		--SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.TotalAmountEffected) AS AmountEffected
			,BA.BatchNo
			--,(CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		--JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		)
		
		
	SELECT
	(	
		SELECT AdjustmentName
				--,(REPLACE(CONVERT(VARCHAR(25), CAST(AmountEffected AS MONEY), 1),'-','')+Format) AS NewAmountEffected
				,AmountEffected
				,BatchNo
				,CreatedDate
		FROM PageResult
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate
					,ApprovedBy 
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					,@ModifiedBy
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill--commented by neeraj as per SATYA 
							
							--UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							--WHERE CustomerBillId=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 06/29/2015 15:44:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							--,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,CD.OutStandingAmount AS TotalDueAmount
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmount - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertMeterInformation]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 05-04-2014
-- Description:	Insert Meter Information details
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertMeterInformation]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @MeterNo VARCHAR(50)
			,@MeterType INT
			,@MeterSize VARCHAR(50)
			,@MeterDetails VARCHAR(50)
			,@MeterMultiplier VARCHAR(50)
			,@MeterBrand VARCHAR(50)
			,@MeterStatus INT
			,@MeterSerialNo VARCHAR(50)
			,@MeterRating VARCHAR(50)
			,@NextCalibrationDate VARCHAR(20)
			,@ServiceHoursBeforeCalibration VARCHAR(50)
			,@CreatedBy VARCHAR(50)
			,@MeterDials INT
			,@Decimals INT
			,@BU_ID VARCHAR(20)
			,@IsCAPMIMeter BIT = NULL
			,@CAPMIAmount DECIMAL(18,2) = NULL
			
		SELECT
			 @MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')
			,@MeterType=C.value('(MeterType)[1]','INT')
			,@MeterSize=C.value('(MeterSize)[1]','VARCHAR(50)')
			,@MeterDetails=C.value('(Details)[1]','VARCHAR(50)')
			,@MeterMultiplier=C.value('(MeterMultiplier)[1]','VARCHAR(50)')
			,@MeterBrand=C.value('(MeterBrand)[1]','VARCHAR(50)')
			,@MeterSerialNo=C.value('(MeterSerialNo)[1]','VARCHAR(50)')
			,@MeterRating=C.value('(MeterRating)[1]','VARCHAR(50)')
			,@NextCalibrationDate=C.value('(NextCalibrationDate)[1]','VARCHAR(20)')
			,@ServiceHoursBeforeCalibration=C.value('(ServiceHoursBeforeCalibration)[1]','VARCHAR(50)')			
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@MeterStatus=C.value('(MeterStatus)[1]','INT')
			,@Decimals=C.value('(Decimals)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')
			,@IsCAPMIMeter=C.value('(IsCAPMIMeter)[1]','BIT')
			,@CAPMIAmount=C.value('(CAPMIAmount)[1]','DECIMAL(18,2)')
		FROM @XmlDoc.nodes('MastersBE') as T(C)
		
		IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
		BEGIN
			SELECT 1 AS IsMeterNoExists FOR XML PATH('MastersBE')
		END
		ELSE IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterSerialNo=@MeterSerialNo)
		BEGIN
			SELECT 1 AS IsMeterSerialNoExists FOR XML PATH('MastersBE')
		END
		ELSE
		BEGIN 
		INSERT INTO Tbl_MeterInformation(
									     MeterNo
										,MeterType
										,MeterSize
										,MeterDetails
										,MeterMultiplier
										,MeterBrand
										,MeterStatus
										,MeterSerialNo
										,MeterRating
										,NextCalibrationDate
										,ServiceHoursBeforeCalibration
										,CreatedBy
										,MeterDials
										,Decimals
										,CreatedDate
										,BU_ID
										,IsCAPMIMeter
										,CAPMIAmount
										)
								VALUES(
										 @MeterNo
										,@MeterType
										,@MeterSize
										,(CASE @MeterDetails WHEN '' THEN NULL ELSE @MeterDetails END)
										,@MeterMultiplier
										,@MeterBrand
										,1
										,@MeterSerialNo
										,@MeterRating
										,CASE @NextCalibrationDate WHEN '' THEN NULL ELSE @NextCalibrationDate END
										,CASE @ServiceHoursBeforeCalibration WHEN '' THEN NULL ELSE @ServiceHoursBeforeCalibration END
										,@CreatedBy
										,@MeterDials
										,(CASE @Decimals WHEN '' THEN 0 ELSE @Decimals END)
										,dbo.fn_GetCurrentDateTime()
										,@BU_ID
										,@IsCAPMIMeter
										,@CAPMIAmount
										)
					SELECT 1 AS IsSuccess
					FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CBILLS.AccountNo,CBILLS.AverageReading as  LastBillAverage,CBILLS.Usage, 
	CD.GlobalAccountNumber,CD.AccountNo AS AccNo,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.MeterNumber AS MeterNo
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.BookSortOrder 
		 INTO #CustomerBillswithAverage
		 from   Tbl_CustomerBills(NOLOCK)	CBILLS
	INNER JOIN  UDV_PreBillingRpt(NOLOCK) CD ON CBILLS.AccountNo = CD.GlobalAccountNumber 
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
		
	
	SELECT (CD.AccNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.Name 
		,CD.ServiceAddress AS ServiceAddress
		,MIN(CR.PreviousReading) AS PreviousReading 
		,MAX (CR.ReadDate) AS PreviousReadDate
		,MAX(CR.PresentReading) AS	 PresentReading
		,CAST(SUM(CR.usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(CR.CreatedDate) AS LatestTransactionDate
		,CreatedBy AS TransactionLog
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CustomerSortOrder 
		,CD.CycleName
		,BookNumber
		,CD.MeterNo
		,CD.BookSortOrder 
	FROM Tbl_CustomerReadings CR
	INNER JOIN #CustomerBillswithAverage CD ON	   CD.GlobalAccountNumber=CR.GlobalAccountNumber
	GROUP BY CD.GlobalAccountNumber,CD.OldAccountNo,
		CD.MeterNumber,CD.ClassName,
		CD.Name,
		CD.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CD.BookSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
		, CD.Usage
		,CustomerSortOrder
		,CD.AccNo 
		,CD.MeterNo
		,CreatedBy
    HAVING 
	CAST(SUM(CR.usage) AS DECIMAL(18,2)) 
	between   
	 CD.Usage- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	 CD.Usage +((30*CONVERT(Decimal(18,2),AverageReading)/100))
    
    DELETE FROM		 #CustomerBillswithAverage
 	
END
----------------------------------------------------------------------------------------------------
 




GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- Modified By: NEERAJ KANOJIYA
-- Modified Date: 26-JUNE-2015
-- Description: @AccountNo was incorrect in update statement for outstanding, modified with @GlobalAccountNo.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
					--						WHERE AdjustmentLogId = @AdjustmentLogId))

					--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END			
					
					
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@ModifedBy
								 ,@CreatedBy
								 ,@CreatedDate 
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
							,CurrentApprovalLevel= 0
							,IsLocked=1
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected 
							 ,CreatedBy
							 ,ApprovedBy
							 ,CreatedDate  
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected 
							 ,@ModifedBy
							 ,@CreatedBy
							 ,@CreatedDate 
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END
GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CD.CycleName
		,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
		,CD.InitialBillingKWh
	FROM
	UDV_PrebillingRpt(NOLOCK)  CD
	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID   and  CD.ReadCodeID = 1	AND CD.ActiveStatusId=1  
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	 AND  isnull(CD.BoolDisableTypeId,0)=0

	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	WHERE isnull(DAVG.AvgReadingId,0)=0 
       -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateActiveStatusForFunctionalPermission]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	Modified the function for deactivation the function
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateActiveStatusForFunctionalPermission]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @FunctionId INT
			,@IsActive BIT
			,@ModifiedBy VARCHAR(50)
			,@ModifiedDate DATETIME
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@IsActive=C.value('(IsActive)[1]','BIT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	SET @ModifiedDate=dbo.fn_GetCurrentDateTime();
	
	UPDATE TBL_FunctionalAccessPermission SET 
		IsActive=@IsActive 
		,ModifiedBy=@ModifiedBy
		,ModifiedDate=@ModifiedDate
	WHERE FunctionId=@FunctionId 
	
	SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetFunctionalAccessSettings]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to get Functional Access settings
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetFunctionalAccessSettings]
(
	@XmlDoc Xml=null
)
AS
BEGIN
	SELECT 
		 FAP.FunctionId
		,FAP.[Function]
		,FAP.IsActive
	 FROM TBL_FunctionalAccessPermission AS FAP
 	 ORDER BY FAP.[Function] ASC
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) --= 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) --= 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) --= 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) --= '2,3,4,5,7,8,9'
		,@PageNo INT --= 1
		,@PageSize INT --= 100  
		
	SELECT  
		 @BU_IDs = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(Months)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(Years)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerBillsBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,( CD.AccountNo  + ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress 
		,CD.MeterNumber AS MeterNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CB.NetEnergyCharges
		,CB.NetFixedCharges
		,CB.TotalBillAmount
		,CB.VAT
		,CB.VATPercentage
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CB.NetArrears
		,CB.TotalBillAmountWithArrears
		,(CASE CD.ReadCodeID WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadCode
		,CB.Usage
		,CB.PaidAmount
		,CONVERT(VARCHAR(20),CB.PaymentLastDate,106) AS PaymentLastDate
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CycleName
		,CD.BookDetails AS BookNumber
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  (NOLOCK)
	INNER JOIN Tbl_MBillingTypes BT (NOLOCK) ON CB.ReadType = BT.BillingTypeId AND
		(CB.ReadType = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN UDV_CustomerPDFReport CD (NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo 
	INNER JOIN Tbl_MTariffClasses T (NOLOCK) ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY CB.AccountNo ASC


	SELECT   
		 RowNumber 
		,GlobalAccountNumber 
		,OldAccountNo 	
		,Name      
		,ServiceAddress
		,MeterNo
		,CONVERT(VARCHAR(20),CAST(NetEnergyCharges AS MONEY),-1) AS NetEnergyCharges
		,CONVERT(VARCHAR(20),CAST(NetFixedCharges AS MONEY),-1) AS NetFixedCharges
		,CONVERT(VARCHAR(20),CAST(TotalBillAmount AS MONEY),-1) AS TotalBillAmount
		,CONVERT(VARCHAR(20),CAST(VAT AS MONEY),-1) AS VAT
		,CONVERT(VARCHAR(20),CAST(VATPercentage AS MONEY),-1) AS VATPercentage
		,BillNo    
		,TariffId  
		,TariffName  
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithTax AS MONEY),-1) AS TotalBillAmountWithTax  
		,CONVERT(VARCHAR(20),CAST(NetArrears AS MONEY),-1) AS NetArrears
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithArrears AS MONEY),-1) AS TotalBillAmountWithArrears
		,CONVERT(INT,Usage) AS Usage
		,ReadCode
		,ReadDate  
		,PaymentLastDate
		,BillMonth  
		,BillYear   
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,BillProcessType
		,TotalRecords
		,0 AS BookSortOrder
		,0 AS CustomerSortOrder
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CycleName
		,BookNumber
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
 

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerBookNoChangeApproval]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  V. Bhimaraju
-- Create date: 21-04-2015
-- Description: Update Address change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerBookNoChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;

	DECLARE  
		 @BookNoChangeLogId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@ApprovalStatusId INT
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @BookNoChangeLogId = C.value('(BookNoChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	DECLARE 
		@GlobalAccountNumber VARCHAR(50),
		@OldPostal_HouseNo VARCHAR(100),
		@OldPostal_StreetName VARCHAR(255),
		@OldPostal_City VARCHAR(100),
		@OldPostal_Landmark VARCHAR(100),
		@OldPostal_AreaCode VARCHAR(100),
		@OldPostal_ZipCode VARCHAR(100),
		@NewPostal_HouseNo VARCHAR(100),
		@NewPostal_StreetName VARCHAR(255),
		@NewPostal_City VARCHAR(100),
		@NewPostal_Landmark VARCHAR(100),
		@NewPostal_AreaCode VARCHAR(100),
		@NewPostal_ZipCode VARCHAR(100),
		@OldService_HouseNo VARCHAR(100),
		@OldService_StreetName VARCHAR(255),
		@OldService_City VARCHAR(100),
		@OldService_Landmark VARCHAR(100),
		@OldService_AreaCode VARCHAR(100),
		@OldService_ZipCode VARCHAR(100),
		@NewService_HouseNo VARCHAR(100),
		@NewService_StreetName VARCHAR(255),
		@NewService_City VARCHAR(100),
		@NewService_Landmark VARCHAR(100),
		@NewService_AreaCode VARCHAR(100),
		@NewService_ZipCode VARCHAR(100),
		@Remarks VARCHAR(MAX),
		@PresentApprovalRole INT,
		@NextApprovalRole INT,
		@OldServiceAddressID INT,
		@NewServiceAddressID INT,
		@OldPostalAddressID INT,
		@NewPostalAddressID INT,
		@OldIsSameAsService INT,
		@NewIsSameAsService INT,
		@IsPostalCommunication BIT,
		@IsServiceCommunication BIT,
		@OldBU_ID VARCHAR(20),
		@NewBU_ID VARCHAR(20),
		@OldSU_ID VARCHAR(20),
		@NewSU_ID VARCHAR(20),
		@OldSC_ID VARCHAR(20),	
		@NewSC_ID VARCHAR(20),
		@OldCycleId VARCHAR(20),
		@NewCycleId VARCHAR(20),	
		@OldBookNo VARCHAR(20),
		@NewBookNo VARCHAR(20),
		@TempAddressId INT,
		@TempServiceId INT,
		@AccountNo VARCHAR(50)	
	
	SELECT 
		 @GlobalAccountNumber        =	GlobalAccountNo
		,@OldPostal_HouseNo          =	OldPostal_HouseNo
		,@OldPostal_StreetName       =	OldPostal_StreetName
		,@OldPostal_City             =	OldPostal_City
		,@OldPostal_Landmark         =	OldPostal_Landmark
		,@OldPostal_AreaCode         =	OldPostal_AreaCode
		,@OldPostal_ZipCode          =	OldPostal_ZipCode
		,@NewPostal_HouseNo          =	NewPostal_HouseNo
		,@NewPostal_StreetName       =	NewPostal_StreetName
		,@NewPostal_City             =	NewPostal_City
		,@NewPostal_Landmark         =	NewPostal_Landmark
		,@NewPostal_AreaCode         =	NewPostal_AreaCode
		,@NewPostal_ZipCode          =	NewPostal_ZipCode
		,@OldService_HouseNo         =	OldService_HouseNo
		,@OldService_StreetName      =	OldService_StreetName
		,@OldService_City            =	OldService_City
		,@OldService_Landmark        =	OldService_Landmark
		,@OldService_AreaCode        =	OldService_AreaCode
		,@OldService_ZipCode         =	OldService_ZipCode
		,@NewService_HouseNo         =	NewService_HouseNo
		,@NewService_StreetName      =	NewService_StreetName
		,@NewService_City            =	NewService_City
		,@NewService_Landmark        =	NewService_Landmark
		,@NewService_AreaCode        =	NewService_AreaCode
		,@NewService_ZipCode         =	NewService_ZipCode
		,@Remarks                    =	Remarks
		,@OldServiceAddressID		 = OldServiceAddressID
		,@NewServiceAddressID		 = NewServiceAddressID
		,@OldPostalAddressID		 = OldPostalAddressID
		,@NewPostalAddressID		 = NewPostalAddressID
		,@OldIsSameAsService		 = OldIsSameAsService
		,@NewIsSameAsService		 = NewIsSameAsService	
		,@IsPostalCommunication		 = IsPostalCommunication
		,@IsServiceCommunication	 = IsServiceComunication
		,@OldBU_ID					 = OldBU_ID 
		,@NewBU_ID					 = NewBU_ID 
		,@OldSU_ID					 = OldSU_ID 
		,@NewSU_ID					 = NewSU_ID 
		,@OldSC_ID					 = OldSC_ID 
		,@NewSC_ID					 = NewSC_ID 
		,@OldCycleId				 = OldCycleId
		,@NewCycleId				 = NewCycleId
		,@OldBookNo					 = OldBookNo
		,@NewBookNo					 = NewBookNo
		FROM Tbl_BookNoChangeLogs
		WHERE BookNoChangeLogId=@BookNoChangeLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN
		
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_BookNoChangeLogs 
					--						WHERE BookNoChangeLogId = @BookNoChangeLogId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_BookNoChangeLogs 
											WHERE BookNoChangeLogId = @BookNoChangeLogId)
											
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END

					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
							
							
							SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewCycleId,@NewBookNo);

							UPDATE CUSTOMERS.Tbl_CustomersDetail 
							SET AccountNo=@AccountNo 
							WHERE GlobalAccountNumber = @GlobalAccountNumber
								
							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
								SET BookNo=@NewBookNo,IsBookNoChanged=1       
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1
					
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
									,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								
							INSERT INTO Tbl_Audit_BookNoChangeLogs(  
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
								
							UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
							SET IsActive=0
							WHERE GlobalAccountNumber=@GlobalAccountNumber
													
							
							/*--Postal Address Details Insertion Based on IsSameAsService
							if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
							old Data will be deleted from the address table for that account no*/
							IF ((SELECT NewIsSameAsService FROM Tbl_BookNoChangeLogs
								WHERE GlobalAccountNo =@GlobalAccountNumber AND BookNoChangeLogId=@BookNoChangeLogId)=1)
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
									(GlobalAccountNumber
									,HouseNo
									,StreetName
									,City
									,Landmark
									,Details
									,AreaCode
									,IsServiceAddress
									,ZipCode
									,IsCommunication
									,IsActive
									,CreatedBy
									,CreatedDate)
									VALUES
									(@GlobalAccountNumber
									,@NewPostal_HouseNo
									,@NewPostal_StreetName
									,@NewPostal_City
									,@NewPostal_Landmark
									,NULL
									,@NewPostal_AreaCode
									,0
									,@NewPostal_ZipCode
									,1
									,1
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime())
						
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,ServiceAddressID=@TempAddressId
											,IsSameAsService=1
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
							ELSE
								BEGIN
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewPostal_HouseNo
												,@NewPostal_StreetName
												,@NewPostal_City
												,@NewPostal_Landmark
												,NULL
												,@NewPostal_AreaCode
												,0
												,@NewPostal_ZipCode
												,@IsPostalCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
									
									SET	@TempAddressId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET PostalAddressID=@TempAddressId
											,IsSameAsService=0
											,ModifedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE GlobalAccountNumber=@GlobalAccountNumber
									
									INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
												(GlobalAccountNumber
												,HouseNo
												,StreetName
												,City
												,Landmark
												,Details
												,AreaCode
												,IsServiceAddress
												,ZipCode
												,IsCommunication
												,IsActive
												,CreatedBy
												,CreatedDate)
												VALUES
												(@GlobalAccountNumber
												,@NewService_HouseNo
												,@NewService_StreetName
												,@NewService_City
												,@NewService_Landmark
												,NULL
												,@NewService_AreaCode
												,1
												,@NewService_ZipCode
												,@IsServiceCommunication
												,1
												,@ModifiedBy
												,dbo.fn_GetCurrentDateTime())
												
									SET	@TempServiceId =SCOPE_IDENTITY()
									
									UPDATE CUSTOMERS.Tbl_CustomersDetail 
										SET ServiceAddressID=@TempServiceId
									WHERE GlobalAccountNumber=@GlobalAccountNumber
								END
						END
					ELSE -- Not a final approval
						BEGIN							
							UPDATE Tbl_BookNoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNumber 
							AND BookNoChangeLogId = @BookNoChangeLogId 							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_BookNoChangeLogs
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
					WHERE GlobalAccountNo = @GlobalAccountNumber
					AND  BookNoChangeLogId= @BookNoChangeLogId
					
					SET @AccountNo= MASTERS.fn_GenCustomerAccountNo(@NewBU_ID,@NewSU_ID,@NewSC_ID,@NewCycleId,@NewBookNo);

					UPDATE CUSTOMERS.Tbl_CustomersDetail 
						SET AccountNo=@AccountNo 
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET BookNo=@NewBookNo,IsBookNoChanged=1       
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND ActiveStatusId=1

					UPDATE CUSTOMERS.Tbl_CustomersDetail SET  
									Service_Landmark=@NewService_Landmark
									,Service_StreetName=@NewService_StreetName
									,Service_City=@NewService_City 
									,Service_HouseNo=@NewService_HouseNo  
									,Service_ZipCode=@NewService_ZipCode  
									,Service_AreaCode=@NewService_AreaCode  
									,Postal_Landmark=@NewPostal_LandMark  
									,Postal_StreetName=@NewPostal_StreetName  
									,Postal_City=@NewPostal_City  
									,Postal_HouseNo=@NewPostal_HouseNo  
									,Postal_ZipCode=@NewPostal_ZipCode  
									,Postal_AreaCode=@NewPostal_AreaCode  
									,ServiceAddressID=@NewServiceAddressID  
									,PostalAddressID=@NewPostalAddressID  
									,IsSameAsService=@NewIsSameAsService
								  WHERE GlobalAccountNumber=@GlobalAccountNumber
								
					INSERT INTO Tbl_Audit_BookNoChangeLogs(
										BookNoChangeLogId,
										GlobalAccountNo,
										OldPostal_HouseNo,
										OldPostal_StreetName,
										OldPostal_City,
										OldPostal_Landmark,
										OldPostal_AreaCode,
										OldPostal_ZipCode,
										NewPostal_HouseNo,
										NewPostal_StreetName,
										NewPostal_City,
										NewPostal_Landmark,
										NewPostal_AreaCode,
										NewPostal_ZipCode,
										OldService_HouseNo,
										OldService_StreetName,
										OldService_City,
										OldService_Landmark,
										OldService_AreaCode,
										OldService_ZipCode,
										NewService_HouseNo,
										NewService_StreetName,
										NewService_City,
										NewService_Landmark,
										NewService_AreaCode,
										NewService_ZipCode,
										ApprovalStatusId,
										Remarks,
										ModifiedBy,
										ModifiedDate,
										PresentApprovalRole,
										NextApprovalRole,
										OldServiceAddressID,
										NewServiceAddressID,
										OldPostalAddressID,
										NewPostalAddressID,
										OldIsSameAsService,
										NewIsSameAsService,
										IsPostalCommunication,
										IsServiceComunication,
										OldBU_ID,
										NewBU_ID,
										OldSU_ID,
										NewSU_ID,
										OldSC_ID,
										NewSC_ID,
										OldCycleId,
										NewCycleId,
										OldBookNo,
										NewBookNo
										)  
								VALUES(  
										@BookNoChangeLogId,
										@GlobalAccountNumber,
										@OldPostal_HouseNo,
										@OldPostal_StreetName,
										@OldPostal_City,
										@OldPostal_Landmark,
										@OldPostal_AreaCode,
										@OldPostal_ZipCode,
										@NewPostal_HouseNo,
										@NewPostal_StreetName,
										@NewPostal_City,
										@NewPostal_Landmark,
										@NewPostal_AreaCode,
										@NewPostal_ZipCode,
										@OldService_HouseNo,
										@OldService_StreetName,
										@OldService_City,
										@OldService_Landmark,
										@OldService_AreaCode,
										@OldService_ZipCode,
										@NewService_HouseNo,
										@NewService_StreetName,
										@NewService_City,
										@NewService_Landmark,
										@NewService_AreaCode,
										@NewService_ZipCode,
										@ApprovalStatusId,
										@Remarks,
										@ModifiedBy,
										dbo.fn_GetCurrentDateTime(),
										@PresentApprovalRole,
										@NextApprovalRole,
										@OldServiceAddressID,
										@NewServiceAddressID,
										@OldPostalAddressID,
										@NewPostalAddressID,
										@OldIsSameAsService,
										@NewIsSameAsService,
										@IsPostalCommunication,
										@IsServiceCommunication,
										@OldBU_ID,
										@NewBU_ID,
										@OldSU_ID,
										@NewSU_ID,
										@OldSC_ID,
										@NewSC_ID,
										@OldCycleId,
										@NewCycleId,
										@OldBookNo,
										@NewBookNo
										)
					
					UPDATE CUSTOMERS.Tbl_CustomerPostalAddressDetails
					SET IsActive=0
					WHERE GlobalAccountNumber=@GlobalAccountNumber
							
					/*--Postal Address Details Insertion Based on IsSameAsService
					if IsSameAsService =1 only postal address will be stored other wise 2 records will be inserted 
					old Data will be deleted from the address table for that account no*/
					IF ((SELECT NewIsSameAsService FROM Tbl_BookNoChangeLogs
						WHERE GlobalAccountNo =@GlobalAccountNumber AND BookNoChangeLogId=@BookNoChangeLogId)=1)
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
							(GlobalAccountNumber
							,HouseNo
							,StreetName
							,City
							,Landmark
							,Details
							,AreaCode
							,IsServiceAddress
							,ZipCode
							,IsCommunication
							,IsActive
							,CreatedDate
							,CreatedBy)
							VALUES
							(@GlobalAccountNumber
							,@NewPostal_HouseNo
							,@NewPostal_StreetName
							,@NewPostal_City
							,@NewPostal_Landmark
							,NULL
							,@NewPostal_AreaCode
							,0
							,@NewPostal_ZipCode
							,1
							,1
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime())
				
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,ServiceAddressID=@TempAddressId
									,IsSameAsService=1
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
					ELSE
						BEGIN
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewPostal_HouseNo
										,@NewPostal_StreetName
										,@NewPostal_City
										,@NewPostal_Landmark
										,NULL
										,@NewPostal_AreaCode
										,0
										,@NewPostal_ZipCode
										,@IsPostalCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
							
							SET	@TempAddressId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET PostalAddressID=@TempAddressId
									,IsSameAsService=0
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber=@GlobalAccountNumber
							
							INSERT INTO CUSTOMERS.Tbl_CustomerPostalAddressDetails
										(GlobalAccountNumber
										,HouseNo
										,StreetName
										,City
										,Landmark
										,Details
										,AreaCode
										,IsServiceAddress
										,ZipCode
										,IsCommunication
										,IsActive
										,CreatedDate
										,CreatedBy)
										VALUES
										(@GlobalAccountNumber
										,@NewService_HouseNo
										,@NewService_StreetName
										,@NewService_City
										,@NewService_Landmark
										,NULL
										,@NewService_AreaCode
										,1
										,@NewService_ZipCode
										,@IsServiceCommunication
										,1
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime())
										
							SET	@TempServiceId =SCOPE_IDENTITY()
							
							UPDATE CUSTOMERS.Tbl_CustomersDetail 
								SET ServiceAddressID=@TempServiceId
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
			END
			
				SELECT 1 As IsSuccess  
				FOR XML PATH('ChangeCustomerTypeBE'),TYPE
			
		END
	ELSE
		BEGIN
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_BookNoChangeLogs 
							--									WHERE BookNoChangeLogId = @BookNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_BookNoChangeLogs 
																WHERE  BookNoChangeLogId = @BookNoChangeLogId )
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_BookNoChangeLogs 
						WHERE BookNoChangeLogId = @BookNoChangeLogId
				END
				
			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_BookNoChangeLogs
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNo = @GlobalAccountNumber  
			AND BookNoChangeLogId = @BookNoChangeLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 06/29/2015 15:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,(CD.BookId+' ( '+BookCode+' )') AS BookCode
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,CAD.InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  ,CD.AccountNo --Faiz-ID103
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  and PAD1.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  
GO


