
GO
ALTER TABLE Tbl_MDesignations
ADD CreatedBy VARCHAR(50)
GO
ALTER TABLE Tbl_MDesignations
ADD CreatedDate DateTime
GO
ALTER TABLE Tbl_MDesignations
ADD ModifiedBy VARCHAR(50)
GO
ALTER TABLE Tbl_MDesignations
ADD ModifiedDate DateTime
GO


GO
ALTER TABLE Tbl_MIdentityTypes
ADD CreatedBy VARCHAR(50)
GO
ALTER TABLE Tbl_MIdentityTypes
ADD CreatedDate DateTime
GO
ALTER TABLE Tbl_MIdentityTypes
ADD ModifiedBy VARCHAR(50)
GO
ALTER TABLE Tbl_MIdentityTypes
ADD ModifiedDate DateTime
GO
--ALTER TABLE Tbl_MIdentityTypes
--ADD IsMaster BIT DEFAULT 0
--GO
GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId,AccessLevelID)
VALUES('Add Identity Type','../Masters/AddCustomerIdentityType.aspx',122,62,1,1,207,3)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,207,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,207,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO

GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId,AccessLevelID)
VALUES('Add Designation','../Masters/AddDesignation.aspx',122,63,1,1,208,3)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,208,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,208,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO