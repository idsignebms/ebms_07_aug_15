GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignationActiveStatus]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 29-JAN-2015  
-- Description: The purpose of this procedure is to update active status of Designation into Tbl_MDesignations
-- =============================================  
CREATE PROCEDURE [dbo].[USP_UpdateDesignationActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @DesignationId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @DesignationId=C.value('(DesignationId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	IF NOT EXISTS(SELECT 0 FROM Tbl_UserDetails WHERE DesignationId = @DesignationId)
		BEGIN
			UPDATE Tbl_MDesignations 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE DesignationId=@DesignationId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			FROM Tbl_UserDetails
			WHERE DesignationId = @DesignationId
			FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignation]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to Update Designation into Tbl_MDesignations  
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateDesignation]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(50)
			--,@Details VARCHAR(MAX)
			,@DesignationId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @DesignationName=C.value('(DesignationName)[1]','VARCHAR(500)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@DesignationId=C.value('(DesignationId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName AND DesignationId!=@DesignationId)
		BEGIN
				UPDATE Tbl_MDesignations SET    DesignationName=@DesignationName
											--,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE DesignationId=@DesignationId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsDesignationExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertDesignation]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to insert Designation into Tbl_MDesignations  
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertDesignation]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(500)
			--,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @DesignationName=C.value('(DesignationName)[1]','VARCHAR(50)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName)
		BEGIN
	INSERT INTO Tbl_MDesignations(
								 DesignationName
								--,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @DesignationName
								--,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsDesignationExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDesignationList]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of Designations from Tbl_MDesignations  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetDesignationList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,DesignationId   
    ,DesignationName
    ,ActiveStatusId 
    --,ISNULL(Details,'---') AS Details  
    FROM Tbl_MDesignations
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersIdentityTypesList]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of IdentityTypes from Tbl_MIdentityTypes  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomersIdentityTypesList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,IdentityId   
    ,[Type]
    ,ActiveStatusId  
    --,IsMaster
    ,ISNULL(Details,'---') AS Details  
    FROM Tbl_MIdentityTypes
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCustomerIdentityTypeActiveStatus]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 29-JAN-2015  
-- Description: The purpose of this procedure is to update active status of IdentityTypes into Tbl_MIdentityTypes
-- =============================================  
CREATE PROCEDURE [dbo].[USP_UpdateCustomerIdentityTypeActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @IdentityId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @IdentityId=C.value('(IdentityId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerIdentityDetails] WHERE IdentityTypeId = @IdentityId)
		BEGIN
			UPDATE Tbl_MIdentityTypes 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE IdentityId=@IdentityId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			FROM [CUSTOMERS].[Tbl_CustomerIdentityDetails]
			WHERE IdentityId = @IdentityId
			FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCustomerIdentityTypes]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to Update CustomerIdentityTypes into Tbl_MIdentityTypes  
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateCustomerIdentityTypes]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @Type VARCHAR(50)
			,@Details VARCHAR(MAX)
			,@IdentityId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @Type=C.value('(Type)[1]','VARCHAR(500)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@IdentityId=C.value('(IdentityId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MIdentityTypes WHERE [Type]=@Type AND IdentityId!=@IdentityId)
		BEGIN
				UPDATE Tbl_MIdentityTypes SET    [Type]=@Type
											,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE IdentityId=@IdentityId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsTypeExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerIdentityType]    Script Date: 06/30/2015 17:19:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to insert CustomerIdentityTypes into Tbl_MIdentityTypes  
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertCustomerIdentityType]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @Type VARCHAR(500)
			,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @Type=C.value('(Type)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MIdentityTypes WHERE [Type]=@Type)
		BEGIN
	INSERT INTO Tbl_MIdentityTypes(
								 [Type]
								,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @Type
								,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsTypeExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertDesignation]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to insert Designation into Tbl_MDesignations  
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertDesignation]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(500)
			--,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @DesignationName=C.value('(DesignationName)[1]','VARCHAR(50)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName)
		BEGIN
	INSERT INTO Tbl_MDesignations(
								 DesignationName
								--,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @DesignationName
								--,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsDesignationExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignation]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to Update Designation into Tbl_MDesignations  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateDesignation]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(50)
			--,@Details VARCHAR(MAX)
			,@DesignationId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @DesignationName=C.value('(DesignationName)[1]','VARCHAR(500)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@DesignationId=C.value('(DesignationId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName AND DesignationId!=@DesignationId)
		BEGIN
				UPDATE Tbl_MDesignations SET    DesignationName=@DesignationName
											--,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE DesignationId=@DesignationId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsDesignationExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignationActiveStatus]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 29-JAN-2015  
-- Description: The purpose of this procedure is to update active status of Designation into Tbl_MDesignations
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateDesignationActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @DesignationId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @DesignationId=C.value('(DesignationId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	IF NOT EXISTS(SELECT 0 FROM Tbl_UserDetails WHERE DesignationId = @DesignationId)
		BEGIN
			UPDATE Tbl_MDesignations 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE DesignationId=@DesignationId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			FROM Tbl_UserDetails
			WHERE DesignationId = @DesignationId
			FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDesignationList]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of Designations from Tbl_MDesignations  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDesignationList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,DesignationId   
    ,DesignationName
    ,ActiveStatusId 
    --,ISNULL(Details,'---') AS Details  
    FROM Tbl_MDesignations
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetExtraChargeNameAndCharges]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------PROCEDURE 1-------------------------------------------------      
-- =============================================      
-- Author  : Naresh      
-- Create date : 24-Apr-2014      
-- Modified By : NEERAJ KANOJIYA      
-- Modified date: 3-MAY-2014      
-- Modified By : Suresh Kumar  
-- Modified date: 25-JULY-2014      
-- Description : To Get Extra Charges Name And Charges      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetExtraChargeNameAndCharges]      
 (      
 @XmlDoc xml      
 )      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(20)        
          ,@CustomerBillId INT        
           ,@Traffid Varchar(20)           
         
  SELECT        
       @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')        
      ,@CustomerBillId = C.value('(CustomerBillID)[1]','INT')        
      FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)       
  IF(@AccountNo = '' OR @AccountNo IS NULL)     --NEERAJ   
  BEGIN    
   SELECT @AccountNo=AccountNo FROM Tbl_Customerbills WHERE CustomerBillId=@CustomerBillId  
  END    
  
  --DECLARE @TotalAdditionCharges DECIMAL(18,2) = 0
  
  --SELECT @TotalAdditionCharges = SUM(AmountEffected) FROM Tbl_BillAdjustments CAC 
  --WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 2
      
	SELECT (
		 SELECT CM.ChargeId AS ChargeID
				,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,CB.VATPercentage AS Vat
				,SUM(AdditionalCharges) AS AdditionalCharges
		 FROM Tbl_MChargeIds CM
		 LEFT JOIN TBL_Customer_Additionalcharges CA ON CM.ChargeId = CA.ChargeId AND CA.AccountNO = @AccountNo AND CA.CustomerBillId = @CustomerBillId    
		 LEFT JOIN Tbl_CustomerBills CB ON CA.CustomerBillId = CB.CustomerBillId
		 LEFT JOIN Tbl_BillAdjustments BA ON BA.CustomerBillId = CB.CustomerBillId 
		 LEFT JOIN Tbl_BillAdjustmentDetails BAD ON BAD.BillAdjustmentId = BA.BillAdjustmentId AND BAD.AdditionalChargesID = CM.ChargeId
		 WHERE CM.IsAcitve = 1
		GROUP BY CM.ChargeId,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,CB.VATPercentage
		FOR XML PATH('CustomerBillDetails'),TYPE        
	 )       
	 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
	 
			 
		----------------Old One Modified  by Suresh---	 
		 --SELECT   CC.ChargeId AS ChargeID,      
			--	(Select ChargeName from Tbl_MChargeIds WHERE ChargeId=CC.ChargeId) AS ChargeName,      
			--	CustomerAdditioalCharges,      
			--	 Amount,      
			--	(SELECT BillNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId ) AS BillNo,    
			--	(SELECT TotalBillAmount FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId) AS TotalBillAmount    
		 --FROM  TBL_Customer_Additionalcharges CC WHERE  CC.AccountNO=@AccountNo       
		 --AND CC.CustomerBillId=@CustomerBillId   
 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCustomerIdentityTypes]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to Update CustomerIdentityTypes into Tbl_MIdentityTypes  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateCustomerIdentityTypes]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @Type VARCHAR(50)
			,@Details VARCHAR(MAX)
			,@IdentityId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @Type=C.value('(Type)[1]','VARCHAR(500)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@IdentityId=C.value('(IdentityId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MIdentityTypes WHERE [Type]=@Type AND IdentityId!=@IdentityId)
		BEGIN
				UPDATE Tbl_MIdentityTypes SET    [Type]=@Type
											,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE IdentityId=@IdentityId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsTypeExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCustomerIdentityTypeActiveStatus]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 29-JAN-2015  
-- Description: The purpose of this procedure is to update active status of IdentityTypes into Tbl_MIdentityTypes
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateCustomerIdentityTypeActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @IdentityId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @IdentityId=C.value('(IdentityId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerIdentityDetails] WHERE IdentityTypeId = @IdentityId)
		BEGIN
			UPDATE Tbl_MIdentityTypes 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE IdentityId=@IdentityId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			FROM [CUSTOMERS].[Tbl_CustomerIdentityDetails]
			WHERE IdentityId = @IdentityId
			FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersIdentityTypesList]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of IdentityTypes from Tbl_MIdentityTypes  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomersIdentityTypesList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,IdentityId   
    ,[Type]
    ,ActiveStatusId  
    --,IsMaster
    ,ISNULL(Details,'---') AS Details  
    FROM Tbl_MIdentityTypes
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerIdentityType]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to insert CustomerIdentityTypes into Tbl_MIdentityTypes  
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerIdentityType]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @Type VARCHAR(500)
			,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @Type=C.value('(Type)[1]','VARCHAR(50)')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MIdentityTypes WHERE [Type]=@Type)
		BEGIN
	INSERT INTO Tbl_MIdentityTypes(
								 [Type]
								,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @Type
								,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsTypeExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=5)
	BEGIN
		SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')		
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetFunctionalAccessSettings]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to get Functional Access settings
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetFunctionalAccessSettings]
(
	@XmlDoc Xml=null
)
AS
BEGIN
	SELECT 
		 FAP.FunctionId
		,FAP.[Function]
		,FAP.IsActive
	 FROM TBL_FunctionalAccessPermission AS FAP
	 WHERE FAP.FunctionId !=5
 	 ORDER BY FAP.[Function] ASC
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) --= 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) --= 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) --= 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) --= '2,3,4,5,7,8,9'
		,@PageNo INT --= 1
		,@PageSize INT --= 100  
		
	SELECT  
		 @BU_IDs = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(Months)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(Years)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerBillsBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,( CD.AccountNo  + ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress 
		,CD.MeterNumber AS MeterNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CB.NetEnergyCharges
		,CB.NetFixedCharges
		,CB.TotalBillAmount
		,CB.VAT
		,CB.VATPercentage
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CB.NetArrears
		,CB.TotalBillAmountWithArrears
		,(CASE CD.ReadCodeID WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadCode
		,CB.Usage
		--,CB.PaidAmount
		--,CONVERT(VARCHAR(20),CB.PaymentLastDate,106) AS PaymentLastDate
		,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS PaidAmount
		,dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber) AS PaymentLastDate
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CycleName
		,CD.BookDetails AS BookNumber
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  (NOLOCK)
	INNER JOIN Tbl_MBillingTypes BT (NOLOCK) ON CB.BillingTypeId = BT.BillingTypeId AND
		(CB.BillingTypeId = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN UDV_CustomerPDFReport CD (NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo 
	INNER JOIN Tbl_MTariffClasses T (NOLOCK) ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY CB.AccountNo ASC


	SELECT   
		 RowNumber 
		,GlobalAccountNumber 
		,OldAccountNo 	
		,Name      
		,ServiceAddress
		,MeterNo
		,CONVERT(VARCHAR(20),CAST(NetEnergyCharges AS MONEY),-1) AS NetEnergyCharges
		,CONVERT(VARCHAR(20),CAST(NetFixedCharges AS MONEY),-1) AS NetFixedCharges
		,CONVERT(VARCHAR(20),CAST(TotalBillAmount AS MONEY),-1) AS TotalBillAmount
		,CONVERT(VARCHAR(20),CAST(VAT AS MONEY),-1) AS VAT
		,CONVERT(VARCHAR(20),CAST(VATPercentage AS MONEY),-1) AS VATPercentage
		,BillNo    
		,TariffId  
		,TariffName  
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithTax AS MONEY),-1) AS TotalBillAmountWithTax  
		,CONVERT(VARCHAR(20),CAST(NetArrears AS MONEY),-1) AS NetArrears
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithArrears AS MONEY),-1) AS TotalBillAmountWithArrears
		,CONVERT(INT,Usage) AS Usage
		,ReadCode
		,ReadDate  
		,PaymentLastDate
		,BillMonth  
		,BillYear   
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,BillProcessType
		,TotalRecords
		,0 AS BookSortOrder
		,0 AS CustomerSortOrder
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CycleName
		,BookNumber
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
 

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- Modified By: NEERAJ KANOJIYA
-- Modified Date: 26-JUNE-2015
-- Description: @AccountNo was incorrect in update statement for outstanding, modified with @GlobalAccountNo.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate 
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
					--						WHERE AdjustmentLogId = @AdjustmentLogId))

					--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
											WHERE AdjustmentLogId = @AdjustmentLogId)

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END			
					
					
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId 

							INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate   
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@TotalAmountEffected   
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@ModifedBy
								 ,@CreatedBy
								 ,@CreatedDate 
								 )      
							SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
							INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
							SELECT @BillAdjustmentId,@TotalAmountEffected
							
							----OutStanding Amount is Updating Start
							
							--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							
							--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							--SET OutStandingAmount=@OutStandingAmount
							--WHERE GlobalAccountNumber=@GlobalAccountNo
							----OutStanding Amount is Updating End
							
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
							
							INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())
								
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AdjustmentsLogs 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNumber = @GlobalAccountNo 
							AND AdjustmentLogId = @AdjustmentLogId
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					
						UPDATE Tbl_AdjustmentsLogs 
						SET   -- Updating Request with Level Roles & Approval status 
							 ModifedBy = @ModifiedBy
							,ModifiedDate = dbo.fn_GetCurrentDateTime()
							,PresentApprovalRole = @PresentRoleId
							,NextApprovalRole = @NextRoleId 
							,ApproveStatusId = @ApprovalStatusId
							,CurrentApprovalLevel= 0
							,IsLocked=1
						WHERE GlobalAccountNumber = @GlobalAccountNo 
						AND AdjustmentLogId = @AdjustmentLogId 

						INSERT INTO Tbl_BillAdjustments(
							 AccountNo    
							 --,CustomerId    
							 ,AmountEffected   
							 ,BillAdjustmentType    
							 ,TotalAmountEffected 
							 ,CreatedBy
							 ,ApprovedBy
							 ,CreatedDate  
							 )           
							VALUES(         
							  @GlobalAccountNo    
							 --,@CustomerId    
							 ,@TotalAmountEffected   
							 ,@BillAdjustmentTypeId   
							 ,@TotalAmountEffected 
							 ,@ModifedBy
							 ,@CreatedBy
							 ,@CreatedDate 
							 )      
						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
						SELECT @BillAdjustmentId,@TotalAmountEffected
						
						----OutStanding Amount is Updating Start
							
						--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
						--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
						--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						--	SET OutStandingAmount=@OutStandingAmount
						--	WHERE GlobalAccountNumber=@GlobalAccountNo
						--	--OutStanding Amount is Updating End
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
						
						INSERT INTO Tbl_Audit_AdjustmentsLogs 
								(	 AdjustmentLogId
									,GlobalAccountNumber 
									,AccountNo 
									,BillNumber 
									,BillAdjustmentTypeId  
									,MeterNumber 
									,PreviousReading 
									,CurrentReadingAfterAdjustment 
									,CurrentReadingBeforeAdjustment 
									,AdjustedUnits 
									,TaxEffected 
									,TotalAmountEffected 
									,EnergryCharges 
									,AdditionalCharges 
									,PresentApprovalRole
									,NextApprovalRole  
									,ApproveStatusId  
									,Remarks
									,CreatedBy 
									,CreatedDate  
									,ModifedBy 
									,ModifiedDate
								)
							VALUES (@AdjustmentLogId
									,@GlobalAccountNo
									,@AccountNo 
									,@BillNumber 
									,@BillAdjustmentTypeId  
									,@MeterNumber 
									,@PreviousReading 
									,@CurrentReadingAfterAdjustment 
									,@CurrentReadingBeforeAdjustment 
									,@AdjustedUnits 
									,@TaxEffected 
									,@TotalAmountEffected 
									,@EnergryCharges 
									,@AdditionalCharges 
									,@PresentRoleId
									,@NextRoleId
									,@ApprovalStatusId
									,@Remarks
									,@CreatedBy 
									,@CreatedDate  
									,@ModifedBy 
									,dbo.fn_GetCurrentDateTime())					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillReadings_ByAccountNo]    Script Date: 06/30/2015 17:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================              
-- Author  : Naresh Kumar                
-- Create date  : 17 Apr 2014              
-- Description  : The purpose of this procedure is to get Reading details based on Accno and Billno           
-- ModifiedBy : NEERAJ        
-- Modified On : 28-Aug      
-- ModifiedBy : NEERAJ        
-- Modified On : 28-FEB
-- DESC			: ADDED MeterMultiplier FIELD  
-- ModifiedBy : NEERAJ        
-- Modified On : 1-March-2015
-- DESC			: change join to left join for meter information for direct customers.
-- =============================================              
ALTER PROCEDURE [dbo].[USP_GetCustomerBillReadings_ByAccountNo]  
(              
@XmlDoc xml              
)              
AS              
BEGIN              
 DECLARE @AccountNo VARCHAR(20)              
          ,@CustomerBillID INT            
           ,@TariffId Varchar(20)             
           ,@ReadCode INT             
            ,@Month INT = 4      
   ,@Year INT = 2014      
   ,@Date VARCHAR(50)        
               
 SELECT              
  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')              
  ,@CustomerBillID = C.value('(CustomerBillID)[1]','INT')              
 FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
          SET @Month=(SELECT BillMonth FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)         
   SET @Year= (SELECT BillYear FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)      
    SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
    IF(@AccountNo='')  
        SELECT @AccountNo=AccountNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillID  
 SET @TariffId=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)             
  SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WHERE GlobalAccountNumber=@AccountNo)  
  
  DECLARE @TotalConsumption DECIMAL(18,2) = 0
  
  SELECT @TotalConsumption = SUM(AdjustedUnits) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 3
             
 SELECT (               
   SELECT       -- Get bill details for meter reading adjustment         
       B.AccountNo,              
       B.TotalBillAmount,                               
       B.BillNo,              
       B.PreviousReading ,              
       B.PresentReading AS PresentReading,              
       B.Usage AS Consumption,        
       B.TotalBillAmountWithTax,    
       (SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials,      
       CONVERT(DECIMAL(18,2),B.VATPercentage) AS TaxPercent,         
       (SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffId) AS Tariff,        
       (Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo) AS TariffId,        
       CONVERT(DECIMAL(18,2),B.NetFixedCharges) AS FixedCharges,               
       @ReadCode AS ReadCodeId,            
       NetEnergyCharges,  
      (SELECT dbo.fn_GetPreviousBalanceUsage(B.AccountNo)) AS BalanceUsage  
	  ,ISNULL(MI.MeterMultiplier,1) AS MeterMultiplier     
	  ,B.VAT AS Vat
	  ,ISNULL(@TotalConsumption,0) AS GrandTotal 
    FROM Tbl_CustomerBills B    
    left JOIN Tbl_MeterInformation AS MI ON B.MeterNo=MI.MeterNo              
    WHERE B.CustomerBillID=@CustomerBillID         
    FOR XML PATH('CustomerBillDetails'),Type             
    ) ,        
    (  
     -- Get all tariff details for meter reading adjustment        
       --SELECT ClassID AS TariffId,ClassName AS Tariff FROM Tbl_MTariffClasses WHERE IsActiveClass=1       
    SELECT   
     ClassID,FromKW ,(CASE ISNULL(ToKW,'') WHEN '' THEN '&#8734;' ELSE CONVERT(varchar(10),ToKW) END)AS ToKW,Amount,TaxId   
    FROM Tbl_LEnergyClassCharges       
    WHERE ClassID  = (SELECT A.TariffClassID FROM CUSTOMERS.Tbl_CustomerProceduralDetails A WHERE GlobalAccountNumber = @AccountNo)        
    AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)       
    ORDER BY FromKW ASC     
    FOR XML PATH('CustomerBillDetails'),Type        
   )        
     FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                    
             
              
END  
-------------------------------------------------------------------------------------------------

GO




