
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillReadings_ByAccountNo]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================              
-- Author  : Naresh Kumar                
-- Create date  : 17 Apr 2014              
-- Description  : The purpose of this procedure is to get Reading details based on Accno and Billno           
-- ModifiedBy : NEERAJ        
-- Modified On : 28-Aug      
-- ModifiedBy : NEERAJ        
-- Modified On : 28-FEB
-- DESC			: ADDED MeterMultiplier FIELD  
-- ModifiedBy : NEERAJ        
-- Modified On : 1-March-2015
-- DESC			: change join to left join for meter information for direct customers.
-- =============================================              
ALTER PROCEDURE [dbo].[USP_GetCustomerBillReadings_ByAccountNo]  
(              
@XmlDoc xml              
)              
AS              
BEGIN              
 DECLARE @AccountNo VARCHAR(20)              
          ,@CustomerBillID INT            
           ,@TariffId Varchar(20)             
           ,@ReadCode INT             
            ,@Month INT = 4      
   ,@Year INT = 2014      
   ,@Date VARCHAR(50)        
               
 SELECT              
  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')              
  ,@CustomerBillID = C.value('(CustomerBillID)[1]','INT')              
 FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
          SET @Month=(SELECT BillMonth FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)         
   SET @Year= (SELECT BillYear FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)      
    SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
    IF(@AccountNo='')  
        SELECT @AccountNo=AccountNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillID  
 SET @TariffId=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)             
  SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WHERE GlobalAccountNumber=@AccountNo)  
  
  DECLARE @TotalConsumption DECIMAL(18,2) = 0
  
  SELECT @TotalConsumption = SUM(AdjustedUnits) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 3
             
 SELECT (               
   SELECT       -- Get bill details for meter reading adjustment         
       B.AccountNo,              
       B.TotalBillAmount,                               
       B.BillNo,              
       B.PreviousReading ,              
       B.PresentReading AS PresentReading,              
       B.Usage AS Consumption,        
       B.TotalBillAmountWithTax,    
       (SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials,      
       CONVERT(DECIMAL(18,2),B.VATPercentage) AS TaxPercent,         
       (SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffId) AS Tariff,        
       (Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo) AS TariffId,        
       CONVERT(DECIMAL(18,2),B.NetFixedCharges) AS FixedCharges,               
       @ReadCode AS ReadCodeId,            
       NetEnergyCharges,  
      (SELECT dbo.fn_GetPreviousBalanceUsage(B.AccountNo)) AS BalanceUsage  
	  ,ISNULL(MI.MeterMultiplier,1) AS MeterMultiplier     
	  ,B.VAT AS Vat
	  ,ISNULL(@TotalConsumption,0) AS GrandTotal 
    FROM Tbl_CustomerBills B    
    left JOIN Tbl_MeterInformation AS MI ON B.MeterNo=MI.MeterNo              
    WHERE B.CustomerBillID=@CustomerBillID         
    FOR XML PATH('CustomerBillDetails'),Type             
    ) ,        
    (  
     -- Get all tariff details for meter reading adjustment        
       --SELECT ClassID AS TariffId,ClassName AS Tariff FROM Tbl_MTariffClasses WHERE IsActiveClass=1       
    SELECT   
     ClassID,FromKW ,(CASE ISNULL(ToKW,'') WHEN '' THEN '&#8734;' ELSE CONVERT(varchar(10),ToKW) END)AS ToKW,Amount,TaxId   
    FROM Tbl_LEnergyClassCharges       
    WHERE ClassID  = (SELECT A.TariffClassID FROM CUSTOMERS.Tbl_CustomerProceduralDetails A WHERE GlobalAccountNumber = @AccountNo)        
    AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)       
    ORDER BY FromKW ASC     
    FOR XML PATH('CustomerBillDetails'),Type        
   )        
     FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                    
             
              
END  
-------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetExtraChargeNameAndCharges]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------PROCEDURE 1-------------------------------------------------      
-- =============================================      
-- Author  : Naresh      
-- Create date : 24-Apr-2014      
-- Modified By : NEERAJ KANOJIYA      
-- Modified date: 3-MAY-2014      
-- Modified By : Suresh Kumar  
-- Modified date: 25-JULY-2014      
-- Description : To Get Extra Charges Name And Charges      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetExtraChargeNameAndCharges]      
 (      
 @XmlDoc xml      
 )      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(20)        
          ,@CustomerBillId INT        
           ,@Traffid Varchar(20)           
         
  SELECT        
       @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')        
      ,@CustomerBillId = C.value('(CustomerBillID)[1]','INT')        
      FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)       
  IF(@AccountNo = '' OR @AccountNo IS NULL)     --NEERAJ   
  BEGIN    
   SELECT @AccountNo=AccountNo FROM Tbl_Customerbills WHERE CustomerBillId=@CustomerBillId  
  END    
  
  --DECLARE @TotalAdditionCharges DECIMAL(18,2) = 0
  
  --SELECT @TotalAdditionCharges = SUM(AmountEffected) FROM Tbl_BillAdjustments CAC 
  --WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 2
      
	SELECT (
		 SELECT CM.ChargeId AS ChargeID
				,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,CB.VATPercentage AS Vat
				,SUM(AdditionalCharges) AS AdditionalCharges
		 FROM Tbl_MChargeIds CM
		 LEFT JOIN TBL_Customer_Additionalcharges CA ON CM.ChargeId = CA.ChargeId AND CA.AccountNO = @AccountNo AND CA.CustomerBillId = @CustomerBillId    
		 LEFT JOIN Tbl_CustomerBills CB ON CA.CustomerBillId = CB.CustomerBillId
		 LEFT JOIN Tbl_BillAdjustments BA ON BA.CustomerBillId = CB.CustomerBillId 
		 LEFT JOIN Tbl_BillAdjustmentDetails BAD ON BAD.BillAdjustmentId = BA.BillAdjustmentId AND BAD.AdditionalChargesID = CM.ChargeId
		 WHERE CM.IsAcitve = 1
		GROUP BY CM.ChargeId,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,CB.VATPercentage
		FOR XML PATH('CustomerBillDetails'),TYPE        
	 )       
	 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
	 
			 
		----------------Old One Modified  by Suresh---	 
		 --SELECT   CC.ChargeId AS ChargeID,      
			--	(Select ChargeName from Tbl_MChargeIds WHERE ChargeId=CC.ChargeId) AS ChargeName,      
			--	CustomerAdditioalCharges,      
			--	 Amount,      
			--	(SELECT BillNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId ) AS BillNo,    
			--	(SELECT TotalBillAmount FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId) AS TotalBillAmount    
		 --FROM  TBL_Customer_Additionalcharges CC WHERE  CC.AccountNO=@AccountNo       
		 --AND CC.CustomerBillId=@CustomerBillId   
 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- Modified By : Bhimaraju Vanka
-- Desc: Here in Adjustment (No Bill) These details are not having in master table we need do left join 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	
	;WITH PageResult AS(
	SELECT TOP 5 ISNULL(BT.Name,'Adjustment(No Bill)') AS AdjustmentName
		--SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.TotalAmountEffected) AS TotalAmountEffected
			,CONVERT(DECIMAL(18,2),BA.AmountEffected) AS AmountEffected
			,ISNULL(BA.AdjustedUnits,0) AS AdjustedUnits
			,BA.BatchNo
			--,(CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		--JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		)
		
		
	SELECT
	(	
		SELECT AdjustmentName
				--,(REPLACE(CONVERT(VARCHAR(25), CAST(AmountEffected AS MONEY), 1),'-','')+Format) AS NewAmountEffected
				,AmountEffected
				,TotalAmountEffected
				,AdjustedUnits
				,BatchNo
				,CreatedDate
		FROM PageResult
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchAdjustmentStatus]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-DEC-2014>
-- Description:	<Retriving Batch Adjustment Status (Open/Close)>
-- Author:		<Faiz-ID103>
-- Create date: <13-May-2015>
-- Description:	Added the BU_ID for filtering the records
-- Modified By : Bhimaraju Vanka
-- Desc : Added Batch Status Id Condition
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchAdjustmentStatus]
(
	@XmlDoc XML
)
AS
	BEGIN
		DECLARE @BU_ID VARCHAR(50)
				,@PageNo INT
				,@PageSize INT
		
		SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')     
				,@PageNo = C.value('(PageNo)[1]','INT')
				,@PageSize = C.value('(PageSize)[1]','INT')	      
		FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH PagedResults AS
		(
			SELECT   ROW_NUMBER() OVER(ORDER BY BA.BatchDate) AS RowNumber 
					,BA.BatchID
					,BA.BatchNo
					,ISNULL(CONVERT(VARCHAR(50),BA.BatchDate,106),'--') AS BatchDate
					,BA.BatchTotal
					,ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0) AS PaidAmount
					,BU_ID AS BUID
					,BillAdjustmentTypeId
					,Name AS BillAdjustmentType
			FROM	Tbl_BATCH_ADJUSTMENT AS BA
			LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentTypeId=BT.BATID
			WHERE	ISNULL(BatchStatusID,1) = 1
					--BatchTotal>(ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0))
					--AND BU_ID=@BU_ID
					AND BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		)
			SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults PR
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			ORDER BY  convert(datetime, PR.BatchDate, 103) 
	END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================  
--AUTHOR : NEERAJ KANOJIYA  
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS  
--CREATED ON: 4-OCT-14  
--=============================================================  
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]
(  
@XmlDoc XML  
)  
AS  
 BEGIN  
 DECLARE @BatchID INT
   
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')  
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)  
   
   
SELECT (
		SELECT	BA.BatchID,
				BA.BatchNo,
				BA.BatchDate,
				BA.BatchTotal,
				(SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,
				RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode
				(BA.BatchTotal-
				(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,
				convert(int, (BA.BatchTotal-
				(SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,
				(SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType
			FROM  Tbl_BATCH_ADJUSTMENT AS BA
			JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID
			WHERE BA.BatchID=@BatchID
			FOR XML PATH('BillAdjustments'),TYPE          
			),
			(
				SELECT	BA.BatchNO,
						BAT.Name AS AdjustmentName,
						CD.GlobalAccountNumber AS AccountNo,
						BLA.TotalAmountEffected,
						BLA.AmountEffected,
						ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,
						BLA.CreatedDate
				from Tbl_BATCH_ADJUSTMENT		AS BA
				JOIN Tbl_BillAdjustments		AS BLA	ON BA.BatchID = BLA.BatchNo
				JOIN Tbl_BillAdjustmentDetails	AS BAD	ON BLA.BillAdjustmentId=BAD.BillAdjustmentId
				JOIN Tbl_BillAdjustmentType		AS BAT	ON BLA.BillAdjustmentType=BAT.BATID
				JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber
				WHERE BA.BatchID=@BatchID
				FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE      
			)
			FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							--,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,CD.OutStandingAmount AS TotalDueAmount
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmountWithTax - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentLogsToApprove]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- MODIFIED BY:  NEERAJ KANOJIYA
-- Create date: 29-JUNE-2015
-- Description: CHANGE BILL ID TO BILL NO BY JOINING CUSTOMERBILL TBL, ADDED REQUEST DATE FIELS AND AND ORDER BY REQUEST DATE
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAdjustmentLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
      ,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			--ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			ROW_NUMBER() OVER (ORDER BY T.AdjustmentLogId desc) AS RowNumber
			,T.AdjustmentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			--,T.BillNumber
			,CSTB.BillNo AS BillNumber
			,ISNULL(BA.Name,'Adjustment(NoBill)') AS BillAdjustmentType
			,T.MeterNumber 
			,T.PreviousReading
			,T.CurrentReadingAfterAdjustment
			,T.CurrentReadingBeforeAdjustment 
			,(CONVERT(DECIMAL(18,2),T.CurrentReadingAfterAdjustment) - CONVERT(DECIMAL(18,2),T.PreviousReading)) AS Consumption
			,T.AdjustedUnits
			,T.TaxEffected
			,T.TotalAmountEffected
			,T.EnergryCharges
			,T.AdditionalCharges
			,T.Remarks AS Details
			,CONVERT(VARCHAR(12),T.CreatedDate,106) AS RequestDate 
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_AdjustmentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	--INNER JOIN Tbl_BillAdjustmentType BAT ON BAT.Name= T.BillAdjustmentTypeId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	LEFT JOIN Tbl_BillAdjustmentType BA ON BA.BATID = T.BillAdjustmentTypeId
	LEFT JOIN Tbl_CustomerBills AS CSTB ON T.BillNumber = CSTB.CustomerBillId
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
	ORDER BY T.CreatedDate desc
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationDetailsByCycle]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =======================================================================                      
-- Author  : NEERAJ KANOJIYA                    
-- Create date  : 25 July 2014                      
-- Description  : THIS PROCEDURE WILL GET ESTIMATION DETAILS OF READING  
-- Modified By : Padmini
-- Modified Date : 26-12-2014                 
-- =======================================================================                      
ALTER PROCEDURE [dbo].[USP_GetEstimationDetailsByCycle]                      
(                      
	@XmlDoc xml                      
)                      
AS                      
BEGIN 

	 DECLARE @Year INT      
		 ,@Month int                
		 ,@BU_ID Varchar(MAX)
		 ,@SU_ID Varchar(MAX)
		 ,@SC_ID Varchar(MAX)
		 ,@CycleId Varchar(MAX)
		 ,@BookNo Varchar(MAX)
		 ,@PageNo INT
		 ,@PageSize INT
		               
	  SELECT                      
		   @Year = C.value('(YearId)[1]','INT'),              
		   @Month = C.value('(MonthId)[1]','INT'),      
		   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)'),
		   @SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)'),
		   @SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)'),
		   @CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)'),
		   @BookNo = C.value('(BookNo)[1]','VARCHAR(MAX)'),
		   @PageNo = C.value('(PageNo)[1]','INT'),
		   @PageSize = C.value('(PageSize)[1]','INT')
	   FROM @XmlDoc.nodes('RptUsageConsumedBe') as T(C)         
	  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	  
	    ;WITH Result as      
	   (      
		   SELECT   
			    ClassID   
			   ,TS.ClassName     
			   ,COUNT(CustomerProcedureID)  AS EstimatedCustomers
			   ,(dbo.fn_GetMonthlyUsage_ByTariff(BG.CycleId,ClassID,@Year,@Month)) AS AvgReadingPerCust      
			   ,(select SUM(EnergytoCalculate) from Tbl_EstimationSettings ES WHERE CycleId=BG.CycleId AND BillingYear=@Year AND BillingMonth=@Month AND ES.TariffId= TS.ClassID AND ES.ActiveStatusId=1) AS AvgUsage         
		  FROM Tbl_MTariffClasses(NOLOCK) TS   
		  INNER JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails](NOLOCK) CD ON TS.ClassID=CD.TariffClassID  
		  INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON CD.BookNo=BN.BookNo  --AND  BN.CycleId=@Cycle
		  INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BN.CycleId
		  INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BNM ON BNM.BookNo = BN.BookNo
		  WHERE TS.RefClassID IS NOT NULL
		  GROUP BY ClassID ,TS.ClassName,BG.CycleId				   
	   )      
		         
		SELECT TS.ClassID      
			,TS.ClassName      
			,SUM(CONVERT(INT,ISNULL(B.EstimatedCustomers,0))) AS  EstimatedCustomers
			,SUM(CONVERT(INT,ISNULL(B.AvgReadingPerCust,0))) AS AvgReadingPerCust
			,SUM(CONVERT(INT,ISNULL(B.AvgUsage,0))) AS AvgUsage
			,SUM(CONVERT(INT,ISNULL(B.Total,0))) AS Total
			,ROW_NUMBER() OVER(ORDER BY TS.ClassID ASC ) AS RowNumber         
		INTO #tblResults 
		FROM Tbl_MTariffClasses(NOLOCK) TS   
		LEFT JOIN 
		( 
			SELECT ClassID      
				,ClassName      
				,EstimatedCustomers      
				,AvgReadingPerCust      
				,AvgUsage      
				,(ISNULL(AvgUsage,0) + ISNULL(AvgReadingPerCust,0)) AS Total
			FROM Result 
		)B ON B.ClassID=TS.ClassID 
		GROUP BY TS.ClassName,TS.ClassID 
		
	            
		SELECT ClassID      
			,ClassName      
			,EstimatedCustomers
			,AvgReadingPerCust
			,AvgUsage
			,Total
			,RowNumber         
			,(Select COUNT(0) from #tblResults) as TotalRecords
		FROM #tblResults
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize                      
		
	DROP TABLE #tblResults
END


GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]
(  
@XmlDoc xml  
)
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT		

	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
	  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
		END  
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsMeterChangeExist FOR XML PATH('MastersBE')  
		END 
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('MastersBE')  
		END
	ELSE IF ((SELECT ISNULL(OutStandingAmount,0) FROM Tbl_PaidMeterDetails WHERE AccountNo=@GlobalAccountNumber AND MeterNo=@MeterNumber AND ActiveStatusId = 1) > 0)
		BEGIN
			SELECT 1 AS IsHavingCapmiAmt FOR XML PATH('MastersBE')  
		END	
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ReadToDirectId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
											
			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
											 GlobalAccountNo
											,MeterNo
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											)
			
			SET @ReadToDirectId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE TBL_ReadToDirectCustomerActivity 
						SET IsLatest=0
					WHERE GlobalAccountNo = @GlobalAccountNumber
	
					INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNumber
														,@MeterNumber
														,@CreatedBy
														,DBO.fn_GetCurrentDateTime()
														)
												
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)  
					SELECT  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
		END
END
--AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@StatusText VARCHAR(200)='Initiation'
--		,@IsSuccessful BIT=1
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	SET @StatusText='Deserialization'
	
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	SET @StatusText='Update Statement'
	
--	UPDATE TBL_ReadToDirectCustomerActivity 
--	SET IsLatest=0
--	WHERE GlobalAccountNo = @GlobalAccountNumber
	
--	SET @StatusText='Insert Log Statement'
--	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
--												(
--												GlobalAccountNo
--												,MeterNumber
--												,Remarks
--												,CreatedBy 
--												,CreatedDate
--												,ApprovalStatusId
--												)
--										VALUES
--												(
--												@GlobalAccountNumber
--												,@MeterNumber
--												,@Reason
--												,@CreatedBy
--												,DBO.fn_GetCurrentDateTime()
--												,@ApprovalStatusId
--												)
--	SET @StatusText='Update Statement'
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--		SET ReadCodeID=1, MeterNumber = NULL
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('MastersBE'),TYPE

--END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
		
	
	DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
	SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), 
		@CAPMIAmount = ISNULL(CAPMIAmount,0),
		@MeterTypeId = MeterType
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNo AND ActiveStatusId = 1	
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID AND ActiveStatusId = 1)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(2,3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @AccountNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsReadToDirectApproval FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((CONVERT(DECIMAL(18,2),@OldMeterReading) < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF ((SELECT ISNULL(OutStandingAmount,0) FROM Tbl_PaidMeterDetails WHERE AccountNo=@AccountNo AND MeterNo=@OldMeterNo AND ActiveStatusId = 1) > 0)
		BEGIN
			SELECT 1 AS IsHavingCapmiAmt FOR XML PATH('ChangeBookNoBe')  
		END	
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,MI.MeterType
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			--INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage >= 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END,
				IsCAPMI = @IsCAPMIMeter,
				MeterAmount = @CAPMIAmount
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END



GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 08-JAN-2014      
-- Description: The purpose of this procedure is to get list of MeterInformation      
-- Modified By:  Faiz-ID103  
-- Modified date: 08-JAN-2014      
-- Description: Added Business Unit name and Meter Dials field for new customer registration page  
  
-- =============================================      
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @MeterNo VARCHAR(100)      
   ,@MeterSerialNo VARCHAR(100)      
   ,@BUID VARCHAR(50)      
   ,@CustomerTypeId INT      
         
  SELECT         
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')      
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')      
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')      
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')      
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  IF (@CustomerTypeId=3)      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType     
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo      
       ,BU.BusinessUnitName    
       ,MeterDials  
       ,(CASE WHEN M.IsCAPMIMeter=1 THEN 'Yes' ELSE 'No' END) AS IsCAPMIMeterString
       ,M.CAPMIAmount
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID WHERE M.ActiveStatusId IN(1) AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
				WHERE MeterNumber !='' )      
      AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))       
      AND M.MeterType=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
  ELSE      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType      
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo       
       ,MeterDials  
       ,BU.BusinessUnitName    
       ,(CASE WHEN M.IsCAPMIMeter=1 THEN 'Yes' ELSE 'No' END) AS IsCAPMIMeterString
       ,M.CAPMIAmount
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID    
      WHERE M.ActiveStatusId IN(1)      
      AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT DISTINCT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
							WHERE ISNULL(MeterNumber,'') !='' )      
        AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))   
      AND M.MeterType !=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
         
END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignationActiveStatus]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 29-JAN-2015  
-- Description: The purpose of this procedure is to update active status of Designation into Tbl_MDesignations
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateDesignationActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @DesignationId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @DesignationId=C.value('(DesignationId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	IF (	(NOT EXISTS(SELECT 0 FROM Tbl_UserDetails WHERE DesignationId = @DesignationId)) 
			AND
			(NOT EXISTS(SELECT 0 FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE DesignationId = @DesignationId)))
		BEGIN
			UPDATE Tbl_MDesignations 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE DesignationId=@DesignationId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			--FROM Tbl_UserDetails
			--WHERE DesignationId = @DesignationId
			FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBEDCEmployeeList]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Padmini.M
-- Create date: 09-02-2015
-- Description:	The purpose of this procedure is to get list of BEDC Employees
-- Modified By:		Faiz-ID103
-- Modified Date:	01-Jul-2015
-- Description:		Added Designation Id for designation from Tbl_MDesignations
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBEDCEmployeeList]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @PageNo INT
			,@PageSize INT		
				
		SELECT 	 
			@PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')	
		FROM @XmlDoc.nodes('EmployeeBE') AS T(C)	
		
		;WITH PagedResults AS
		(
			SELECT
				 ROW_NUMBER() OVER(ORDER BY B.ActiveStatusId ASC , B.CreatedDate DESC ) AS RowNumber
				 ,EmployeeName
				 --,EmployeeDesignation AS Designation
				 ,B.DesignationId
				 ,D.DesignationName AS Designation
				 ,BEDCEmpId AS BEDCEmpId
				 ,EmployeeLocation AS Location
				 ,B.ActiveStatusId
				 ,ISNULL([Address],'--')AS [Address]
				 ,ContactNo AS ContactNo
				 ,ISNULL(AnotherContactNo,'--')AS AnotherContactNo
			FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails B
			JOIN Tbl_MDesignations D ON D.DesignationId = B.DesignationId
			WHERE B.ActiveStatusId IN (1,2)
		)
		
		SELECT 
		  (
			SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			FOR XML PATH('EmployeeBE'),TYPE
		)
		FOR XML PATH(''),ROOT('EmployeeListBEInfoByXml')	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBEDCEmployee]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Padmini
-- Create date: 09/02/2015
-- Description:	This procedure is used to update the BEDC Employee
-- Modified By:		Faiz-ID103
-- Modified Date:	01-Jul-2015
-- Description:		Added Designation Id for designation from Tbl_MDesignations 
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBEDCEmployee] 
(
@XmlDoc xml
)
AS
BEGIN
 DECLARE   @BEDCEmpId INT
          ,@EmployeeName VARCHAR(50)  
		  ,@EmployeeDesignation VARCHAR(50)
		  ,@EmployeeLocation VARCHAR(50)
		  ,@ContactNo VARCHAR(20)
		  ,@AnotherContactNo VARCHAR(20)
		  ,@Address VARCHAR(MAX)
		  ,@ModifiedDate DATETIME
		  ,@ModifiedBy VARCHAR(20)
		  ,@DesignationId INT -- Faiz-ID103
  SELECT  
          @BEDCEmpId=C.value('(BEDCEmpId)[1]','INT')  
          ,@EmployeeName=C.value('(EmployeeName)[1]','VARCHAR(50)')  
         ,@EmployeeDesignation=C.value('(Designation)[1]','VARCHAR(50)') 
         ,@EmployeeLocation=C.value('(Location)[1]','VARCHAR(50)') 
         ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(20)') 
         ,@AnotherContactNo=C.value('(AnotherContactNo)[1]','VARCHAR(20)') 
         ,@Address=C.value('(Address)[1]','VARCHAR(MAX)')   
         ,@ModifiedDate=C.value('(ModifiedDate)[1]','VARCHAR(150)')  
         ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(150)')  
         ,@DesignationId=C.value('(DesignationId)[1]','INT')  -- Faiz-ID103
 FROM @XmlDoc.nodes('EmployeeBE') AS T(C)  	
 
 --IF NOT EXISTS(SELECT 0 FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails 
 --              WHERE EmployeeName=@EmployeeName AND [Address]=@Address AND ContactNo=@ContactNo AND BEDCEmpId!=@BEDCEmpId)
	--BEGIN
		  UPDATE EMPLOYEE.Tbl_MBEDCEmployeeDetails 
		  SET EmployeeName=@EmployeeName
		     ,EmployeeDesignation=@EmployeeDesignation
		     ,DesignationId = @DesignationId
		     ,EmployeeLocation=@EmployeeLocation
		     ,ContactNo=@ContactNo
		     ,AnotherContactNo=CASE @AnotherContactNo WHEN '' THEN NULL ELSE @AnotherContactNo END
		     ,[Address]=@Address
		     ,ModifedBy=@ModifiedBy
		     ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		  WHERE BEDCEmpId=@BEDCEmpId
		  SELECT 1 AS IsSuccess FOR XML PATH('EmployeeBE')
--	END
--ELSE
--	BEGIN		
--		SELECT 1 AS IsEmpNameExists FOR XML PATH('EmployeeBE')
--	END	
		              
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBEDCEmployee]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Padmini
-- Create date: 09/02/2015
-- Description:	This procedure is used to Insert BEDC Employee 
-- Modified By:		Faiz-ID103
-- Modified Date:	01-Jul-2015
-- Description:		Added Designation Id for designation from Tbl_MDesignations
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertBEDCEmployee]    
(
  @XmlDoc xml 
)
AS
BEGIN
  DECLARE  @EmployeeName VARCHAR(50)  
		  --,@EmployeeDesignation VARCHAR(50)
		  ,@EmployeeLocation VARCHAR(50)
		  ,@ContactNo VARCHAR(20)
		  ,@AnotherContactNo VARCHAR(20)
		  ,@Address VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50) 
		  ,@DesignationId INT -- Faiz-ID103
		    
  SELECT  @EmployeeName=C.value('(EmployeeName)[1]','VARCHAR(50)')  
         --,@EmployeeDesignation=C.value('(Designation)[1]','VARCHAR(50)') 
         ,@EmployeeLocation=C.value('(Location)[1]','VARCHAR(50)') 
         ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(20)') 
         ,@AnotherContactNo=C.value('(AnotherContactNo)[1]','VARCHAR(20)') 
         ,@Address=C.value('(Address)[1]','VARCHAR(MAX)')   
         ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(150)')  
         ,@DesignationId=C.value('(DesignationId)[1]','INT')  -- Faiz-ID103
 FROM @XmlDoc.nodes('EmployeeBE') AS T(C)  	
 
	 INSERT INTO EMPLOYEE.Tbl_MBEDCEmployeeDetails
		(
		  EmployeeName
		 --,EmployeeDesignation
		 ,EmployeeLocation
		 ,ContactNo
		 ,AnotherContactNo
		 ,[Address]
		 ,CreatedBy
		 ,CreatedDate
		 ,DesignationId-- Faiz-ID103
		 )
	VALUES(
	     @EmployeeName
	     --,@EmployeeDesignation
	     ,@EmployeeLocation
	     ,@ContactNo
	     ,CASE WHEN @AnotherContactNo='' THEN NULL ELSE @AnotherContactNo END
	     ,@Address
	     ,@CreatedBy
	     ,dbo.fn_GetCurrentDateTime()
	     ,@DesignationId-- Faiz-ID103
	     )
	     
   	SELECT 1 AS IsSuccess 
    FOR xml PATH('EmployeeBE')    	   

END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateRouteActiveStatus]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-04-2014
-- Description:	To update Active status of RouteManagement
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateRouteActiveStatus]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @RouteId INT,@ActiveStatusId INT,@ModifiedBy VARCHAR(50),@CurrentDate DATETIME
	SELECT
		@RouteId=C.value('(RouteId)[1]','INT')
		,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)
	
--	SELECT @CurrentDate=dbo.fn_GetCurrentDateTime()
	
--		UPDATE Tbl_MRoutes SET ActiveStatusId=@ActiveStatusId
--							  ,ModifiedBy=@ModifiedBy
--							  ,ModifiedDate=@CurrentDate
--						   WHERE RouteId=@RouteId
	
--	SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
--END

IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE RouteSequenceNumber = @RouteId)
		BEGIN
			UPDATE Tbl_MRoutes SET ActiveStatusId=@ActiveStatusId
							  ,ModifiedBy=@ModifiedBy
							  ,ModifiedDate=@CurrentDate
						   WHERE RouteId=@RouteId
	
			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
			WHERE RouteSequenceNumber = @RouteId
			FOR XML PATH('MastersBE')
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
  
   SET @NewConsumption= @Consumption-@AdjustedUnits; 
    
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * 5)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertDesignation]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to insert Designation into Tbl_MDesignations  
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertDesignation]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(500)
			--,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @DesignationName=C.value('(Type)[1]','VARCHAR(50)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName)
		BEGIN
	INSERT INTO Tbl_MDesignations(
								 DesignationName
								--,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @DesignationName
								--,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsDesignationExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersIdentityTypesList]    Script Date: 07/01/2015 19:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of IdentityTypes from Tbl_MIdentityTypes  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomersIdentityTypesList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,IdentityId   
    ,[Type]
    ,ActiveStatusId  
    --,IsMaster
    ,ISNULL(Details,'---') AS Details  
    FROM Tbl_MIdentityTypes
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignation]    Script Date: 07/01/2015 19:06:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to Update Designation into Tbl_MDesignations  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateDesignation]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(50)
			--,@Details VARCHAR(MAX)
			,@DesignationId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @DesignationName=C.value('(DesignationName)[1]','VARCHAR(500)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@DesignationId=C.value('(DesignationId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName AND DesignationId!=@DesignationId)
		BEGIN
				UPDATE Tbl_MDesignations SET    DesignationName=@DesignationName
											--,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE DesignationId=@DesignationId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsDesignationExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDesignationList]    Script Date: 07/01/2015 19:06:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of Designations from Tbl_MDesignations  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDesignationList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,DesignationId   
    ,DesignationName
    ,ActiveStatusId 
    --,ISNULL(Details,'---') AS Details  
    FROM Tbl_MDesignations
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO


