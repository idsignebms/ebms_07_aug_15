GO
CREATE TABLE Tbl_MNameTitle
(
 TitleId INT IDENTITY(1,1) PRIMARY KEY
 ,TitleName VARCHAR(10) 
 ,ActiveStatusId INT
 ,CreatedBy VARCHAR(50) 
 ,CreatedDate DATETIME
 ,ModifiedBy VARCHAR(50) 
 ,ModifiedDate DATETIME
)
GO