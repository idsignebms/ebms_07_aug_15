
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges_New]    Script Date: 07/07/2015 18:43:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE FUNCTION  [dbo].[fn_GetFixedCharges_New]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
	,@GlobalAccountNumber   varchar(200)
	,@IsFirstMonthBill Bit 
	,@SetupDate DATETIME
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	DECLARE @Multiplier DECIMAL=1
	 
	 
	IF @IsFirstMonthBill =1
	BEGIN
		IF @SetupDate IS NOT NULL 
			BEGIN
			  
			   
			   SET @Multiplier= DateDiff(mm,@SetupDate,DATEADD(day,-30,@MonthStartDate)  )
			    
			END
	 		 
	END


	 

	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount*ISNULL(@Multiplier,0) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN 
	CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges_New]    Script Date: 07/07/2015 18:43:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
ALTER FUNCTION  [dbo].[fn_GetFixedCharges_New]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
	,@GlobalAccountNumber   varchar(200)
	,@IsFirstMonthBill Bit 
	,@SetupDate DATETIME
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	DECLARE @Multiplier DECIMAL=1
	 
	 
	IF @IsFirstMonthBill =1
	BEGIN
		IF @SetupDate IS NOT NULL 
			BEGIN
			  
			   
			   SET @Multiplier= DateDiff(mm,@SetupDate,DATEADD(day,-30,@MonthStartDate)  )
			    
			END
	 		 
	END


	 

	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount*ISNULL(@Multiplier,0) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN 
	CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerYear_OpeningBalance]    Script Date: 07/07/2015 18:43:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------  
-- =============================================    
-- Author  : Suresh Kumar Dasi    
-- Create date : 27 Sept 2014    
-- Description : The purpose of this function is to get the total Pending Amonunt    
-- select * FROM dbo.fn_GetCustomerYear_OpeningBalance('BEDC_CAN_0042')  
-- =============================================    
ALTER FUNCTION [dbo].[fn_GetCustomerYear_OpeningBalance]  
(    
@AccountNo VARCHAR(50)    
)    
RETURNS @ResultTable TABLE(OpeningDate DATE,TotalPendingAmount DECIMAL(18,2))   
AS    
BEGIN     
 DECLARE @TotalPendingAmount DECIMAL(18,2)    
   ,@TotalArrears DECIMAL(18,2)    
   ,@TotalPaid DECIMAL(18,2)    
   ,@AdvancedPaid DECIMAL(18,2)    
   ,@DueStatus VARCHAR(MAX)  
        ,@PresentYear INT  
  ,@PresentMonth INT  
  ,@Date DATETIME       
  ,@CurrentDate DATETIME = (SELECT dbo.fn_GetCurrentDateTime())      
     
              
 SELECT @DueStatus = dbo.fn_BillDueStatus()  
    
 SELECT  @PresentMonth = 01,@PresentYear =  DATEPART(YEAR,@CurrentDate)  
   
 SET @Date = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'      
 SELECT @DueStatus = dbo.fn_BillDueStatus()  
  
IF(CONVERT(DATE,@Date) > CONVERT(DATE,dbo.fn_GetCurrentDateTime()))  
 BEGIN  
  SELECT @Date = dbo.fn_GetCurrentDateTime()  
 END  
  
 SELECT @TotalArrears = SUM(ISNULL(TotalBillAmountWithTax,0) - ISNULL(AdjustmentAmmount,0)) FROM Tbl_CustomerBills     
 Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))    
 AND AccountNo = @AccountNo   
 AND CONVERT(DATE,CreatedDate) <= CONVERT(DATE,@Date)  
       
  SELECT   
 @TotalPaid = SUM(BP.PaidAmount)   
 FROM Tbl_CustomerPayments BP  
 WHERE CustomerPaymentID IN (SELECT BP.CustomerPaymentId  
        FROM Tbl_CustomerBillPayments BP  
        JOIN Tbl_CustomerBills CB ON BP.BillNo = CB.BillNo AND AccountNo = @AccountNo  
        AND PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))   
       )  
       AND CONVERT(DATE,RecievedDate) <= CONVERT(DATE,@Date)  
    
  SELECT @AdvancedPaid  = SUM(payments .PaidAmount) FROM Tbl_CustomerPayments payments WHERE AccountNo = @AccountNo   
  AND CustomerPaymentID NOT IN (SELECT BP.CustomerPaymentId  
         FROM Tbl_CustomerBillPayments BP  
         JOIN Tbl_CustomerBills CB ON BP.BillNo = CB.BillNo AND AccountNo = @AccountNo  
         AND PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))   
        )  
       AND CONVERT(DATE,RecievedDate) <= CONVERT(DATE,@Date)  
     
         
 SET @TotalPendingAmount = (ISNULL(@TotalPaid,0)+ ISNULL(@AdvancedPaid,0)) - ISNULL(@TotalArrears,0)   
   
INSERT INTO @ResultTable(OpeningDate,TotalPendingAmount)  
VALUES(CONVERT(DATE,@Date),@TotalPendingAmount)   
        
       
 RETURN     
    
END    
-----------------------------------------------------------------------------  
GO



GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPaidMeterCustomer_Balance_New]    Script Date: 07/07/2015 18:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: <Satya>
-- Create date	: <01-July-2015>
-- Description	: This function is used to get the paid meter customer balance 
-- =============================================
CREATE FUNCTION [dbo].[fn_GetPaidMeterCustomer_Balance_New]
(
@GlobalAccountNo VARCHAR(50)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
		DECLARE @Result DECIMAL(18,2)
				,@TotalMeterPrice DECIMAL(18,2)
				,@TotalDiscountedAmount DECIMAL(18,2)
		
		select top 1 @Result=isnull(OutStandingAmount,0) from	Tbl_PaidMeterDetails
		where AccountNo=@GlobalAccountNo and   ActiveStatusId =1
		 
				 
	RETURN @Result

END		   

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyUsage_ByTariff]    Script Date: 07/07/2015 18:44:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                    
-- Author  : SURESH Kumar D               
-- Create date  : 29 July 2014                    
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID    
-- Modified By : Padmini    
-- Modified Date :25-12-2014    
-- =======================================================================       
ALTER FUNCTION [dbo].[fn_GetMonthlyUsage_ByTariff]    
(    
 @CycleID VARCHAR(20)    
 ,@TariffId  VARCHAR(20)    
 ,@Year INT    
    ,@Month int      
)    
RETURNS VARCHAR(50)    
AS    
BEGIN    
 DECLARE @TotalUsage INT    
   ,@Date DATETIME         
   ,@MonthStartDate VARCHAR(50)         
       
 SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'        
    SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))       
       
     
 --SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR    
 --JOIN Tbl_CustomerDetails CD ON CD.AccountNo = CR.AccountNumber    
 --LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo    
 --WHERE  BC.CycleId = @CycleID AND CD.TariffId=@TariffId    
 --AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)    
      
 SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR    
 JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber    
 LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo    
 WHERE  BC.CycleId = @CycleID AND CD.TariffId=@TariffId    
 AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)    
      
 RETURN @TotalUsage;    
END 