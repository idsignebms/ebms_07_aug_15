
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTitleActiveStatus]    Script Date: 07/07/2015 18:46:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 02-Jul-2015  
-- Description: The purpose of this procedure is to update active status of Title into Tbl_MNameTitle
-- =============================================  
CREATE PROCEDURE [dbo].[USP_UpdateTitleActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @TitleId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @TitleId=C.value('(TitleId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	--IF NOT EXISTS(SELECT 0 FROM Tbl_MNameTitle WHERE TitleId = @TitleId)
	--	BEGIN
			UPDATE Tbl_MNameTitle 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE TitleId=@TitleId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
	--	END
	--ELSE
	--	BEGIN
	--		SELECT 0 AS IsSuccess
	--			,1 AS [Count]
	--		FOR XML PATH('MastersBE')
	--	END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTitle]    Script Date: 07/07/2015 18:46:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 02-Jul-2015
-- Description:	The purpose of this procedure is to Update NameTitle into Tbl_MNameTitle  
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateTitle]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @TitleName VARCHAR(10)
			--,@Details VARCHAR(MAX)
			,@TitleId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @TitleName=C.value('(TitleName)[1]','VARCHAR(500)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@TitleId=C.value('(TitleId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MNameTitle WHERE TitleName=@TitleName AND TitleId!=@TitleId)
		BEGIN
				UPDATE Tbl_MNameTitle SET    TitleName=@TitleName
											--,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE TitleId=@TitleId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsTitleExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertTitle]    Script Date: 07/07/2015 18:46:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 01-Jul-2015
-- Description:	The purpose of this procedure is to insert Title into Tbl_MNameTitle
-- =============================================
CREATE PROCEDURE [dbo].[USP_InsertTitle]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @TitleName VARCHAR(10)
			--,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @TitleName=C.value('(TitleName)[1]','VARCHAR(10)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MNameTitle WHERE TitleName=@TitleName)
		BEGIN
	INSERT INTO Tbl_MNameTitle(
								 TitleName
								--,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @TitleName
								--,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsTitleExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTitleList]    Script Date: 07/07/2015 18:46:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 01-Jul-2015
-- Description: The purpose of this procedure is to get list of Titles from Tbl_MNameTitle  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetTitleList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,TitleId   
    ,TitleName
    ,ActiveStatusId 
    --,ISNULL(Details,'---') AS Details  
    FROM Tbl_MNameTitle
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
   ,@AdjustmentAmountEffected DECIMAL(18,2)
   ,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	 SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	 FROM Tbl_CustomerBills AS CB
	 JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
	 JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
	 WHERE CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      ,@SetupDate=SetupDate 
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END		
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)	
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@SetupDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   IF isnull(@PaidMeterDeductedAmount,0) >0
   BEGIN
   
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
   
   update Tbl_PaidMeterDetails
   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
   where AccountNo=@GlobalAccountNumber
   
   END
         
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
   SET @BillingComments=NULL
   SET @TotalBillAmount=NULL
   SET @PaidMeterDeductedAmount=NULL
   SET @IsEmbassyCustomer=NULL
   SET @TaxPercentage=NULL
   SET @PaidMeterDeductedAmount=NULL
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
	END
END  
				 


GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)
			,@IsFirstmonthBill Bit
			,@SetupDate DATETIME

	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
			,SetupDate DATETIME
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark
			,SetupDate  
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark 
			,CD.SetupDate 
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   

		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN

			BEGIN TRY		    
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@SetupDate=SetupDate 
						
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------


						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END
					
					  ;WITH CTE (PreviousReading,PresentReading,GlobalAccountNumber,DuplicateCount)
						AS
						(
						SELECT PreviousReading,PresentReading,GlobalAccountNumber,
						ROW_NUMBER() OVER(PARTITION BY PreviousReading,PresentReading,GlobalAccountNumber 
						ORDER BY GlobalAccountNumber) AS DuplicateCount
						FROM Tbl_CustomerReadings
						where GlobalAccountNumber=@GlobalAccountNumber
						and IsBilled=0
						)
						DELETE 
						FROM CTE
						WHERE DuplicateCount > 1	 
						
						
							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										
										SET @ReadType=5 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										 
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
											SET @Usage=0
											SET @ActualUSage=0
											
										END			
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@SetupDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxPercentage=5;
						 SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
						 			 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )

 
							IF @PrevCustomerBillId IS NULL 
							BEGIN

							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						
						SET  @BillGeneratedBY =( select  top 1 CreatedBy from Tbl_BillingQueueSchedule where BillingQueueScheduleId=@BillingQueueScheduleId)
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,
							(
								SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P 
								WHERE P.AccountNo = C.GlobalAccountNumber 
								ORDER BY RecievedDate DESC
							)         
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,case when (select COUNT(0) from Tbl_CustomerBills where AccountNo=@GlobalAccountNumber)>0  THEN @OutStandingAmount  else @OpeningBalance END
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						  IF isnull(@PaidMeterDeductedAmount,0) >0
						   BEGIN
						   
						   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
						   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
						   
						   update Tbl_PaidMeterDetails
						   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
						   where AccountNo=@GlobalAccountNumber
						   
						   END

						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------


						update Tbl_CustomerReadings set IsBilled =1,
						BillNo=@CusotmerNewBillID,ModifiedDate=dbo.fn_GetCurrentDateTime()
						where GlobalAccountNumber=@GlobalAccountNumber
						and IsBilled=0
						 
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						------------------------------------------------------------------------------------
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-
						SET @TotalAmount = NULL
						SET @CusotmerNewBillID=NULL
						SET @CusotmerNewBillID=NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL
						SET @IsEmbassyCustomer=NULL
						SET @TaxPercentage=NULL
						SET  @IsFirstmonthBill =NULL
						SET  @SetupDate =NULL
						SET @PaidMeterDeductedAmount=NULL
   			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue;        
						END
			END TRY
			BEGIN CATCH
			 
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue      

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------







GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerAssignMeterChangeApproval]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju V
-- Create date: 24-04-2015
-- Description: Update AssignMeter change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerAssignMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT  
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT
		,@AssignMeterChangeId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(MAX) 
		,@BU_ID VARCHAR(50) 		

	SELECT  
		 @AssignMeterChangeId = C.value('(AssignMeterChangeId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(MAX)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
DECLARE
		 @GlobalAccountNo VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@AssignedMeterDate DATETIME
		,@InitialReading DECIMAL(18,2)
		,@Remarks VARCHAR(MAX)
		--,@PresentApprovalRole INT
		--,@NextApprovalRole INT
		,@CreatedBy VARCHAR(50)
		,@CreatedDate DATETIME
		,@IsCAPMIMeter BIT
		,@CAPMIAmount DECIMAL(18,2)
	
	SELECT 
		@GlobalAccountNo	 =GlobalAccountNo 
       ,@MeterNo             =MeterNo 
       ,@AssignedMeterDate   =AssignedMeterDate 
       ,@InitialReading      =InitialReading
       ,@Remarks             =Remarks
       ,@CreatedBy           =CreatedBy 
       ,@CreatedDate         =CreatedDate
       ,@IsCAPMIMeter		 =ISNULL(IsCAPMIMeter,0)
       ,@CAPMIAmount		 =ISNULL(CAPMIAmount,0)
	FROM Tbl_AssignedMeterLogs
	WHERE AssignedMeterId = @AssignMeterChangeId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AssignedMeterLogs 
					--						WHERE AssignedMeterId = @AssignMeterChangeId))
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AssignedMeterLogs 
											WHERE AssignedMeterId = @AssignMeterChangeId)
					

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 

							UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
									SET MeterNumber=@MeterNo
										,ReadCodeID=2
							WHERE GlobalAccountNumber = @GlobalAccountNo
			
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
							SET InitialReading=@InitialReading
								,PresentReading=@InitialReading
								,IsCAPMI=@IsCAPMIMeter
								,MeterAmount=@CAPMIAmount
							WHERE GlobalAccountNumber=@GlobalAccountNo

							
							INSERT INTO Tbl_Audit_AssignedMeterLogs(  
										 AssignedMeterId
										,GlobalAccountNo
										,MeterNo
										,AssignedMeterDate
										,InitialReading
										,Remarks
										,ApprovalStatusId
										,CreatedBy
										,CreatedDate
										,PresentApprovalRole
										,NextApprovalRole
										,IsCAPMIMeter
										,CAPMIAmount)  
									VALUES(  
										 @AssignMeterChangeId
										,@GlobalAccountNo
										,@MeterNo
										,@AssignedMeterDate
										,@InitialReading
										,@Remarks  
										,@ApprovalStatusId  
										,@ModifiedBy  
										,dbo.fn_GetCurrentDateTime()  
										,@PresentRoleId
										,@NextRoleId
										,@IsCAPMIMeter
										,@CAPMIAmount
										)
							
							IF(@IsCAPMIMeter = 1)  
								BEGIN  
									INSERT INTO Tbl_PaidMeterDetails  
									(  
									 AccountNo  
									,MeterNo  
									,MeterTypeId  
									,MeterCost  
									,OutStandingAmount
									,ActiveStatusId  
									,MeterAssignedDate  
									,CreatedDate  
									,CreatedBy  
									)  
									values  
									(  
									 @GlobalAccountNo  
									,@MeterNo  
									,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNo)  
									,@CAPMIAmount  
									,@CAPMIAmount  
									,1  
									,@AssignedMeterDate
									,dbo.fn_GetCurrentDateTime()  
									,@ModifiedBy  
									)       
								END  	
						END
					ELSE -- Not a final approval
						BEGIN
							
							UPDATE Tbl_AssignedMeterLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApprovalStatusId = (CASE ApprovalStatusId WHEN 4 THEN 1 ELSE ApprovalStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE GlobalAccountNo = @GlobalAccountNo 
							AND AssignedMeterId = @AssignMeterChangeId 
							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_AssignedMeterLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApprovalStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
					WHERE GlobalAccountNo = @GlobalAccountNo  
					AND AssignedMeterId = @AssignMeterChangeId 

					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNo
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNo
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
						,IsCAPMI=@IsCAPMIMeter
						,MeterAmount=@CAPMIAmount
					WHERE GlobalAccountNumber=@GlobalAccountNo
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,IsCAPMIMeter
						,CAPMIAmount)   
					VALUES(  
						 @AssignMeterChangeId
						,@GlobalAccountNo
						,@MeterNo
						,@AssignedMeterDate
						,@InitialReading
						,@Remarks  
						,@ApprovalStatusId  
						,@ModifiedBy  
						,dbo.fn_GetCurrentDateTime()  
						,@PresentRoleId
						,@NextRoleId
						,@IsCAPMIMeter
						,@CAPMIAmount
						)
					
					IF(@IsCAPMIMeter = 1)  
						BEGIN  
							INSERT INTO Tbl_PaidMeterDetails  
							(  
							 AccountNo  
							,MeterNo  
							,MeterTypeId  
							,MeterCost  
							,OutStandingAmount
							,ActiveStatusId  
							,MeterAssignedDate  
							,CreatedDate  
							,CreatedBy  
							)  
							values  
							(  
							 @GlobalAccountNo  
							,@MeterNo  
							,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNo)  
							,@CAPMIAmount  
							,@CAPMIAmount  
							,1  
							,@AssignedMeterDate
							,dbo.fn_GetCurrentDateTime()  
							,@ModifiedBy  
							)       
						END  	
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AssignedMeterLogs 
							--									WHERE AssignedMeterId = @AssignMeterChangeId))
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AssignedMeterLogs 
																WHERE AssignedMeterId = @AssignMeterChangeId )

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AssignedMeterLogs 
						WHERE AssignedMeterId = @AssignMeterChangeId 
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AssignedMeterLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApprovalStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNo = @GlobalAccountNo  
			AND AssignedMeterId = @AssignMeterChangeId 
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading BIGINT
		,@NewMeterReading BIGINT
		,@PreviousReading BIGINT
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading BIGINT
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
		,@IsCAPMIMeter BIT = 0
		,@CAPMIAmount DECIMAL(18,2) = 0
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = CAST(OldMeterReading AS NUMERIC)
		,@NewMeterReading = CAST(NewMeterReading AS NUMERIC)
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = CAST(NewMeterInitialReading AS NUMERIC)
		,@NewMeterReadingDate = NewMeterReadingDate
		,@IsCAPMIMeter = ISNULL(IsCAPMIMeter,0)
		,@CAPMIAmount = ISNULL(CAPMIAmount,0)
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_New(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId ANd IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
					--						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

				DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
								,IsCAPMIMeter
								,CAPMIAmount)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
								,IsCAPMIMeter
								,CAPMIAmount
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage >= 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading,
								IsCAPMI = @IsCAPMIMeter,
								MeterAmount = @CAPMIAmount
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
							IF(@IsCAPMIMeter = 1)  
							BEGIN  
								INSERT INTO Tbl_PaidMeterDetails  
								(  
								 AccountNo  
								,MeterNo  
								,MeterTypeId  
								,MeterCost  
								,OutStandingAmount
								,ActiveStatusId  
								,MeterAssignedDate  
								,CreatedDate  
								,CreatedBy  
								)  
								values  
								(  
								 @AccountNo  
								,@NewMeterNo  
								,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@NewMeterNo)  
								,@CAPMIAmount  
								,@CAPMIAmount  
								,1  
								,@OldMeterReadingDate
								,dbo.fn_GetCurrentDateTime()  
								,@ModifiedBy  
								)       
							END  
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked = 1
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage >= 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading,
						IsCAPMI = @IsCAPMIMeter,
						MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
						
					IF(@IsCAPMIMeter = 1)  
						BEGIN  
							INSERT INTO Tbl_PaidMeterDetails  
							(  
							 AccountNo  
							,MeterNo  
							,MeterTypeId  
							,MeterCost  
							,OutStandingAmount
							,ActiveStatusId  
							,MeterAssignedDate  
							,CreatedDate  
							,CreatedBy  
							)  
							values  
							(  
							 @AccountNo  
							,@NewMeterNo  
							,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@NewMeterNo)  
							,@CAPMIAmount  
							,@CAPMIAmount  
							,1  
							,@OldMeterReadingDate
							,dbo.fn_GetCurrentDateTime()  
							,@ModifiedBy  
							)       
						END  
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
							--									WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_Re-AssignMeter]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 5-MARCH-2015  
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_Re-AssignMeter]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
		  @MeterNumber VARCHAR(50)    
		  ,@ReadCodeID VARCHAR(50)   
		  ,@GlobalAccountNumber VARCHAR(50)   
		  ,@IsSuccessful BIT =1  
		  ,@ApprovalStatusId INT
		  ,@IsFinalApproval BIT = 0
		  ,@FunctionId INT
		  ,@Reason VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50)
		  ,@InitialReading INT
     ,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
  SELECT    
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
    ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
	IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE MeterNumber=@MeterNumber)
		BEGIN
			SELECT 1 AS IsMeterAlreadyAssigned FOR XML PATH('CustomerRegistrationBE')
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo=@MeterNumber AND ApprovalStatusId IN (1,4))
		BEGIN
			SELECT 1 AS IsMeterReqByAnotherCust FOR XML PATH('CustomerRegistrationBE')
		END
 	ELSE IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
	IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				BEGIN
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										DECLARE @UserDetailedId INT
										SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
									SET @CurrentApprovalLevel = @CurrentLevel+1
					--SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
			
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1
				
			
			SET @InitialReading=(SELECT ISNULL(ISNULL(PresentReading,0),ISNULL(InitialReading,0)) 
			FROM CUSTOMERS.Tbl_CustomerActiveDetails
			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,dbo.fn_GetCurrentDateTime()
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN					
				IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
					BEGIN
					
						UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
							 SET MeterNumber=@MeterNumber  
							  ,ReadCodeID=@ReadCodeID  
						WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						SET IsCAPMI = @IsCAPMIMeter
							,MeterAmount = @CAPMIAmount
						WHERE GlobalAccountNumber=@GlobalAccountNumber 
						
						INSERT INTO Tbl_Audit_AssignedMeterLogs(  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks
								,ApprovalStatusId
								,CreatedBy
								,CreatedDate
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
								,IsCAPMIMeter
								,CAPMIAmount)  
							SELECT  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks  
								,ApprovalStatusId  
								,CreatedBy  
								,CreatedDate  
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
								,IsCAPMIMeter
								,CAPMIAmount
							FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
							
							IF(@IsCAPMIMeter = 1)  
								BEGIN  
									INSERT INTO Tbl_PaidMeterDetails  
									(  
									 AccountNo  
									,MeterNo  
									,MeterTypeId  
									,MeterCost  
									,OutStandingAmount
									,ActiveStatusId  
									,MeterAssignedDate  
									,CreatedDate  
									,CreatedBy  
									)  
									values  
									(  
									 @GlobalAccountNumber  
									,@MeterNumber  
									,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
									,@CAPMIAmount  
									,@CAPMIAmount  
									,1  
									,dbo.fn_GetCurrentDateTime()  
									,dbo.fn_GetCurrentDateTime()  
									,@CreatedBy  
									)       
								END  
						
						SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')	
					END
				ELSE
					BEGIN
						SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
					END
				END
			ELSE 
				BEGIN
					SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
				END
		END
END
		
--DECLARE   
--		  @MeterNumber VARCHAR(50)    
--		  ,@ReadCodeID VARCHAR(50)   
--		  ,@GlobalAccountNumber VARCHAR(50)   
--		  ,@IsSuccessful BIT =1  
--		  ,@StatusText VARCHAR(200)
     
--BEGIN  
--BEGIN TRY  
-- BEGIN TRAN  
     
-- SET @StatusText='Deserialization'  
--  SELECT    
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
--		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    
--  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
-- IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
--	 BEGIN 
--		 SET @StatusText='Update Statement'  
		   
--		 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
--		 SET MeterNumber=@MeterNumber  
--		  ,ReadCodeID=@ReadCodeID  
--		 WHERE GlobalAccountNumber = @GlobalAccountNumber  
		   	   
--		 SET @StatusText='Success'
--	 END
-- ELSE 
--	BEGIN
--		SET @StatusText='Meter is in DeActivate state.'
--		SET @IsSuccessful =0  
--	END 
-- COMMIT TRAN   
--END TRY      
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @IsSuccessful =0  
--END CATCH  
--END  
-- SELECT   
--   @IsSuccessful AS IsSuccessful  
--   ,@StatusText AS StatusText  
-- FOR XML PATH('CustomerRegistrationBE'),TYPE  
  
--END  
--------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT	
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		 ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END   
	ELSE IF((SELECT COUNT(0)FROM Tbl_CustomerMeterInfoChangeLogs 
				WHERE NewMeterNo = @MeterNumber AND ApproveStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterChangeApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END  
	IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
						DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1		
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
						,IsCAPMI = @IsCAPMIMeter
						,MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
					IF(@IsCAPMIMeter = 1)  
						BEGIN  
							INSERT INTO Tbl_PaidMeterDetails  
							(  
							 AccountNo  
							,MeterNo  
							,MeterTypeId  
							,MeterCost  
							,OutStandingAmount
							,ActiveStatusId  
							,MeterAssignedDate  
							,CreatedDate  
							,CreatedBy  
							)  
							values  
							(  
							 @GlobalAccountNumber  
							,@MeterNumber  
							,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
							,@CAPMIAmount  
							,@CAPMIAmount  
							,1  
							,@AssignedMeterDate  
							,dbo.fn_GetCurrentDateTime()  
							,@CreatedBy  
							)       
						END  
			
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END


GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
		
	
	DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
	SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), 
		@CAPMIAmount = ISNULL(CAPMIAmount,0),
		@MeterTypeId = MeterType
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNo AND ActiveStatusId = 1	
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID AND ActiveStatusId = 1)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(2,3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @AccountNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsReadToDirectApproval FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((CONVERT(DECIMAL(18,2),@OldMeterReading) < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF ((SELECT ISNULL(OutStandingAmount,0) FROM Tbl_PaidMeterDetails WHERE AccountNo=@AccountNo AND MeterNo=@OldMeterNo AND ActiveStatusId = 1) > 0)
		BEGIN
			SELECT 1 AS IsHavingCapmiAmt FOR XML PATH('ChangeBookNoBe')  
		END	
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,MI.MeterType
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			--INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage >= 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END,
				IsCAPMI = @IsCAPMIMeter,
				MeterAmount = @CAPMIAmount
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
			IF(@IsCAPMIMeter = 1)  
				BEGIN  
					INSERT INTO Tbl_PaidMeterDetails  
					(  
					 AccountNo  
					,MeterNo  
					,MeterTypeId  
					,MeterCost  
					,OutStandingAmount
					,ActiveStatusId  
					,MeterAssignedDate  
					,CreatedDate  
					,CreatedBy  
					)  
					values  
					(  
					 @AccountNo  
					,@MeterNo  
					,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNo)  
					,@CAPMIAmount  
					,@CAPMIAmount  
					,1  
					,@OldMeterReadingDate  
					,dbo.fn_GetCurrentDateTime()  
					,@ModifiedBy  
					)       
				END  
			
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetNoUsageCustomersReport]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		T.Karthik
-- Create date: 04-11-2014
-- Description:	The purpose of this procedure is to get No Usage Customers Report
-- Modified By	: Neeraj Kanojiya
-- Modified Date: 11-Nov-14
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:31-Mar-2015
-- Modified Date:11-05-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetNoUsageCustomersReport]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @BUID VARCHAR(MAX)
		,@SUID VARCHAR(MAX)
		,@SCID VARCHAR(MAX)
		,@Cycles VARCHAR(MAX)
		,@BookNos VARCHAR(MAX)
		--,@FromDate VARCHAR(50)
		--,@ToDate VARCHAR(50)
		,@PageSize INT  
		,@PageNo INT  
		,@YearId INT  
		,@MonthId INT  
		
	SELECT
		@BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		--,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		--,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
		,@YearId = C.value('(YearId)[1]','INT')  
		,@MonthId = C.value('(MonthId)[1]','INT')  
	FROM @XmlDoc.nodes('RptNoUsageConsumedBe') as T(C)

	--IF ISNULL(@FromDate,'') = ''
	--BEGIN  
	--	SET @FromDate = dbo.fn_GetCurrentDateTime() - 60 
	--END 
	--IF ISNULL(@Todate,'') = ''
	--BEGIN  
	--	SET @Todate = dbo.fn_GetCurrentDateTime()  
	--END 
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH LIST AS
	(	
		SELECT 
			 ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber) AS RowNumber 
			,(GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
			,CD.AccountNo
			,CD.OldAccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) AS ServiceAddress
			,MeterNumber AS MeterNo
			,CONVERT(VARCHAR(25),CB.BillGeneratedDate,107) AS BillGeneratedDate
			,CD.ClassName AS Tariff
			,ISNULL(CB.NetFixedCharges,0) AS AdditionalCharges
			,CB.TotalBillAmountWithArrears AS TotalBilledAmount
			,RC.ReadCode
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,CD.CycleName 
			,CD.BusinessUnitName
			,CD.ServiceUnitName
			,CD.ServiceCenterName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.BookCode
			,ISNULL(CD.OutStandingAmount,0) AS OutStandingAmount
			,COUNT(0) OVER() AS TotalRecords
		FROM [UDV_CustomerDescription](NOLOCK) AS CD
		INNER JOIN Tbl_CustomerBills AS CB(NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo AND ISNULL(CB.Usage,0) = 0 --AND CD.ActiveStatusId IN(1,2) 
				AND CD.ActiveStatusId = 1 AND CD.CylceActiveStatusId = 1 
				--AND (CD.CreatedDate BETWEEN @FromDate AND @ToDate)
				AND BillMonth = @MonthId AND BillYear = @YearId
		INNER JOIN Tbl_MReadCodes AS RC(NOLOCK) ON CB.ReadCodeId = RC.ReadCodeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
	)

	SELECT 
		 RowNumber
		,GlobalAccountNumber
		,AccountNo
		,OldAccountNo
		,Name
		,ServiceAddress 
		,MeterNo
		,BillGeneratedDate
		,Tariff
		,CONVERT(VARCHAR(20),CAST(AdditionalCharges AS MONEY),-1) AS AdditionalCharges
		,CONVERT(VARCHAR(20),CAST(TotalBilledAmount AS MONEY),-1) AS TotalBilledAmount
		,ReadCode
		,CustomerSortOrder
		,BookSortOrder
		,CycleName 
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,BookNumber
		,BookCode
		,CONVERT(VARCHAR(20),CAST(OutStandingAmount AS MONEY),-1) AS OutStandingAmount
		,TotalRecords
	FROM LIST
	WHERE RowNumber BETWEEN (@PageNo-1) * @PageSize + 1 AND @PageNo * @PageSize

END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersNoNameOrAddList]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 16-Sep-2014  
-- Description: Get The Details of The Customers With out Having No Name Or Address  
-- Modified By : Karthik  
-- Modified Date : 27-12-2014  
-- Modified By : Padmini  
-- Modified Date : 01/12/2015  
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015
-- Modified Date: 11-05-2015     
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageSize INT  
   ,@PageNo INT  
   ,@BU_ID VARCHAR(MAX)  
   ,@SU_ID VARCHAR(MAX)  
   ,@SC_ID VARCHAR(MAX)    
   ,@CycleId VARCHAR(MAX)  
   ,@BookNo VARCHAR(MAX)     
     
 SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('RptNoNameOrAddressBe') AS T(C)  
   
   
 IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SU_ID = '')  
  BEGIN  
   SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50))   
      FROM Tbl_ServiceUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SC_ID = '')  
  BEGIN  
   SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50))   
      FROM Tbl_ServiceCenter  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@CycleId = '')  
  BEGIN  
   SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50))   
      FROM Tbl_Cycles  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@BookNo = '')  
  BEGIN  
   SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50))   
      FROM Tbl_BookNumbers  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
   
 ;WITH PagedResults AS  
 (  
  SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber  
    ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
    ,(CD.AccountNo  + ' - ' + CD.GlobalAccountNumber) AS GlobalAccountNumber  
    ,CD.AccountNo AS AccountNo  
    ,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode)) As ServiceAddress  
    ,ISNULL(OldAccountNo,'--') AS OldAccountNo  
    ,CD.CycleName  
    ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber  
    ,CD.SortOrder AS CustomerSortOrder  
    ,CD.BookSortOrder  
    ,CD.BusinessUnitName  
    ,CD.ServiceUnitName  
    ,CD.ServiceCenterName  
    FROM UDV_CustomerDescription(NOLOCK) CD  
    INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
      AND CD.ActiveStatusId=1       
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD(NOLOCK) ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD(NOLOCK) ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber
      AND (PAD.IsServiceAddress = 0 OR SAD.IsServiceAddress = 1)
      AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName=''
      OR PAD.StreetName IS NULL OR PAD.StreetName = ''OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
      OR PAD.City IS NULL OR PAD.City = '' OR SAD.StreetName IS NULL OR SAD.StreetName = ''  OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
               OR SAD.City IS NULL OR SAD.City = '')   
    
    --INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
    --  AND CD.ActiveStatusId=1 AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName='')  
    --INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    --INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    --INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    --INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    --INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD(NOLOCK) ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber   
    --  AND PAD.IsServiceAddress = 0 AND (PAD.StreetName IS NULL OR PAD.StreetName = ''   
    --           OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
    --           OR PAD.City IS NULL OR PAD.City = '')  
    --INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD(NOLOCK) ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber   
    --  AND SAD.IsServiceAddress = 1 AND (SAD.StreetName IS NULL OR SAD.StreetName = ''   
    --           OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
    --           OR SAD.City IS NULL OR SAD.City = '')   
 )  
   
  SELECT  
		RowNumber  
	   ,Name  
	   ,GlobalAccountNumber  
	   ,AccountNo  
	   ,ServiceAddress  
	   ,BusinessUnitName  
	   ,OldAccountNo  
	   ,CycleName  
	   ,BookNumber  
	   ,CustomerSortOrder  
	   ,BookSortOrder  
	   ,BusinessUnitName  
	   ,ServiceUnitName  
	   ,ServiceCenterName  
	   ,(Select COUNT(0) from PagedResults) as TotalRecords  
  FROM PagedResults p  
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentLogsToApprove]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- MODIFIED BY:  NEERAJ KANOJIYA
-- Create date: 29-JUNE-2015
-- Description: CHANGE BILL ID TO BILL NO BY JOINING CUSTOMERBILL TBL, ADDED REQUEST DATE FIELS AND AND ORDER BY REQUEST DATE
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAdjustmentLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
      ,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			--ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			ROW_NUMBER() OVER (ORDER BY T.AdjustmentLogId desc) AS RowNumber
			,T.AdjustmentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			--,T.BillNumber
			,CSTB.BillNo AS BillNumber
			,ISNULL(BA.Name,'Adjustment(NoBill)') AS BillAdjustmentType
			,T.MeterNumber 
			,T.PreviousReading
			,T.CurrentReadingAfterAdjustment
			,T.CurrentReadingBeforeAdjustment 
			,(CONVERT(DECIMAL(18,2),T.CurrentReadingAfterAdjustment) - CONVERT(DECIMAL(18,2),T.PreviousReading)) AS Consumption
			,CONVERT(INT,T.AdjustedUnits) AS AdjustedUnits
			,T.TaxEffected
			,T.TotalAmountEffected
			,T.EnergryCharges
			,T.AdditionalCharges
			,T.Remarks AS Details
			,CONVERT(VARCHAR(12),T.CreatedDate,106) AS RequestDate 
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_AdjustmentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	--INNER JOIN Tbl_BillAdjustmentType BAT ON BAT.Name= T.BillAdjustmentTypeId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	LEFT JOIN Tbl_BillAdjustmentType BA ON BA.BATID = T.BillAdjustmentTypeId
	LEFT JOIN Tbl_CustomerBills AS CSTB ON T.BillNumber = CSTB.CustomerBillId
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
	ORDER BY T.CreatedDate desc
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTitleActiveStatus]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 02-Jul-2015  
-- Description: The purpose of this procedure is to update active status of Title into Tbl_MNameTitle
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateTitleActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @TitleId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @TitleId=C.value('(TitleId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	--IF NOT EXISTS(SELECT 0 FROM Tbl_MNameTitle WHERE TitleId = @TitleId)
	--	BEGIN
			UPDATE Tbl_MNameTitle 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE TitleId=@TitleId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
	--	END
	--ELSE
	--	BEGIN
	--		SELECT 0 AS IsSuccess
	--			,1 AS [Count]
	--		FOR XML PATH('MastersBE')
	--	END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateTitle]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 02-Jul-2015
-- Description:	The purpose of this procedure is to Update NameTitle into Tbl_MNameTitle  
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateTitle]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE  @TitleName VARCHAR(10)
			--,@Details VARCHAR(MAX)
			,@TitleId INT
			,@ModifiedBy VARCHAR(50)
	SELECT
			 @TitleName=C.value('(TitleName)[1]','VARCHAR(500)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@TitleId=C.value('(TitleId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MNameTitle WHERE TitleName=@TitleName AND TitleId!=@TitleId)
		BEGIN
				UPDATE Tbl_MNameTitle SET    TitleName=@TitleName
											--,Details=(CASE @Details WHEN '' THEN NULL ELSE @Details END)
											,ModifiedBy=@ModifiedBy
											,ModifiedDate=dbo.fn_GetCurrentDateTime()
									WHERE TitleId=@TitleId
						SELECT 1 AS IsSuccess
						FOR xml PATH('MastersBE')
		END
		ELSE
		BEGIN
					SELECT 1 AS IsTitleExists
					FOR xml PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertTitle]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 01-Jul-2015
-- Description:	The purpose of this procedure is to insert Title into Tbl_MNameTitle
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertTitle]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @TitleName VARCHAR(10)
			--,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @TitleName=C.value('(TitleName)[1]','VARCHAR(10)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MNameTitle WHERE TitleName=@TitleName)
		BEGIN
	INSERT INTO Tbl_MNameTitle(
								 TitleName
								--,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @TitleName
								--,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsTitleExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertDesignation]    Script Date: 07/07/2015 18:46:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description:	The purpose of this procedure is to insert Designation into Tbl_MDesignations  
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertDesignation]
	(
	@XmlDoc xml
	)
AS
BEGIN
	DECLARE  @DesignationName VARCHAR(500)
			--,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @DesignationName=C.value('(DesignationName)[1]','VARCHAR(50)')
			--,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('MastersBE') AS T(C)
		
		IF NOT EXISTS(SELECT 0 FROM Tbl_MDesignations WHERE DesignationName=@DesignationName)
		BEGIN
	INSERT INTO Tbl_MDesignations(
								 DesignationName
								--,Details 
								,CreatedBy
								,CreatedDate
								)
						VALUES(
								 @DesignationName
								--,CASE @Details WHEN '' THEN NULL ELSE @Details END
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								)
								
			SELECT 1 AS IsSuccess
			FOR xml PATH('MastersBE')
			END
		ELSE
		BEGIN
			SELECT 1 As IsDesignationExists
				FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetTitleList]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 01-Jul-2015
-- Description: The purpose of this procedure is to get list of Titles from Tbl_MNameTitle  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTitleList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,TitleId   
    ,TitleName
    ,ActiveStatusId 
    --,ISNULL(Details,'---') AS Details  
    FROM Tbl_MNameTitle
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDesignationList]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103
-- Create date: 30-Jun-2015
-- Description: The purpose of this procedure is to get list of Designations from Tbl_MDesignations  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDesignationList]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT    
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC,CreatedDate DESC) AS RowNumber  
    ,DesignationId   
    ,DesignationName
    ,ActiveStatusId 
    --,ISNULL(Details,'---') AS Details  
    FROM Tbl_MDesignations
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillReadings_ByAccountNo]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================              
-- Author  : Naresh Kumar                
-- Create date  : 17 Apr 2014              
-- Description  : The purpose of this procedure is to get Reading details based on Accno and Billno           
-- ModifiedBy : NEERAJ        
-- Modified On : 28-Aug      
-- ModifiedBy : NEERAJ        
-- Modified On : 28-FEB
-- DESC			: ADDED MeterMultiplier FIELD  
-- ModifiedBy : NEERAJ        
-- Modified On : 1-March-2015
-- DESC			: change join to left join for meter information for direct customers.
-- =============================================              
ALTER PROCEDURE [dbo].[USP_GetCustomerBillReadings_ByAccountNo]  
(              
@XmlDoc xml              
)              
AS              
BEGIN              
 DECLARE @AccountNo VARCHAR(20)              
          ,@CustomerBillID INT            
           ,@TariffId Varchar(20)             
           ,@ReadCode INT             
            ,@Month INT = 4      
   ,@Year INT = 2014      
   ,@Date VARCHAR(50)        
               
 SELECT              
  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')              
  ,@CustomerBillID = C.value('(CustomerBillID)[1]','INT')              
 FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
          SET @Month=(SELECT BillMonth FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)         
   SET @Year= (SELECT BillYear FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)      
    SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
    IF(@AccountNo='')  
        SELECT @AccountNo=AccountNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillID  
 SET @TariffId=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)             
  SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WHERE GlobalAccountNumber=@AccountNo)  
  
  DECLARE @TotalConsumption DECIMAL(18,2) = 0
  
  SELECT @TotalConsumption = SUM(AdjustedUnits) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 3
             
 SELECT (               
   SELECT       -- Get bill details for meter reading adjustment         
       B.AccountNo,              
       B.TotalBillAmount,                               
       B.BillNo,              
       B.PreviousReading ,              
       B.PresentReading AS PresentReading,              
       B.Usage AS Consumption,        
       B.TotalBillAmountWithTax,    
       (SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials,      
       CONVERT(DECIMAL(18,2),B.VATPercentage) AS TaxPercent,         
       (SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffId) AS Tariff,        
       (Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo) AS TariffId,        
       CONVERT(DECIMAL(18,2),B.NetFixedCharges) AS FixedCharges,               
       @ReadCode AS ReadCodeId,            
       NetEnergyCharges,  
      (SELECT dbo.fn_GetPreviousBalanceUsage(B.AccountNo)) AS BalanceUsage  
	  ,ISNULL(MI.MeterMultiplier,1) AS MeterMultiplier     
	  ,B.VAT AS Vat
	  ,ISNULL(@TotalConsumption,0) AS GrandTotal 
    FROM Tbl_CustomerBills B    
    left JOIN Tbl_MeterInformation AS MI ON B.MeterNo=MI.MeterNo              
    WHERE B.CustomerBillID=@CustomerBillID         
    FOR XML PATH('CustomerBillDetails'),Type             
    ) ,        
    (  
     -- Get all tariff details for meter reading adjustment        
       --SELECT ClassID AS TariffId,ClassName AS Tariff FROM Tbl_MTariffClasses WHERE IsActiveClass=1       
    SELECT   
     ClassID,FromKW ,(CASE ISNULL(ToKW,'') WHEN '' THEN '&#8734;' ELSE CONVERT(varchar(10),ToKW) END)AS ToKW,Amount,TaxId   
    FROM Tbl_LEnergyClassCharges       
    WHERE ClassID  = (SELECT A.TariffClassID FROM CUSTOMERS.Tbl_CustomerProceduralDetails A WHERE GlobalAccountNumber = @AccountNo)        
    AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)       
    ORDER BY FromKW ASC     
    FOR XML PATH('CustomerBillDetails'),Type        
   )        
     FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                    
             
              
END  
-------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetExtraChargeNameAndCharges]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------PROCEDURE 1-------------------------------------------------      
-- =============================================      
-- Author  : Naresh      
-- Create date : 24-Apr-2014      
-- Modified By : NEERAJ KANOJIYA      
-- Modified date: 3-MAY-2014      
-- Modified By : Suresh Kumar  
-- Modified date: 25-JULY-2014      
-- Description : To Get Extra Charges Name And Charges      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetExtraChargeNameAndCharges]      
 (      
 @XmlDoc xml      
 )      
AS      
BEGIN      
 DECLARE @AccountNo VARCHAR(20)        
          ,@CustomerBillId INT        
           ,@Traffid Varchar(20)           
         
  SELECT        
       @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')        
      ,@CustomerBillId = C.value('(CustomerBillID)[1]','INT')        
      FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)       
  IF(@AccountNo = '' OR @AccountNo IS NULL)     --NEERAJ   
  BEGIN    
   SELECT @AccountNo=AccountNo FROM Tbl_Customerbills WHERE CustomerBillId=@CustomerBillId  
  END    
  
  --DECLARE @TotalAdditionCharges DECIMAL(18,2) = 0
  
  --SELECT @TotalAdditionCharges = SUM(AmountEffected) FROM Tbl_BillAdjustments CAC 
  --WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 2
      
	SELECT (
		 SELECT CM.ChargeId AS ChargeID
				,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,CB.VATPercentage AS Vat
				,SUM(AdditionalCharges) AS AdditionalCharges
		 FROM Tbl_MChargeIds CM
		 LEFT JOIN TBL_Customer_Additionalcharges CA ON CM.ChargeId = CA.ChargeId AND CA.AccountNO = @AccountNo AND CA.CustomerBillId = @CustomerBillId    
		 LEFT JOIN Tbl_CustomerBills CB ON CA.CustomerBillId = CB.CustomerBillId
		 LEFT JOIN Tbl_BillAdjustments BA ON BA.CustomerBillId = CB.CustomerBillId 
		 LEFT JOIN Tbl_BillAdjustmentDetails BAD ON BAD.BillAdjustmentId = BA.BillAdjustmentId AND BAD.AdditionalChargesID = CM.ChargeId
		 WHERE CM.IsAcitve = 1
		GROUP BY CM.ChargeId,ChargeName
				,CA.CustomerAdditioalCharges
				,CA.Amount
				,CB.BillNo   
				,CB.TotalBillAmount
				,CB.VATPercentage
		FOR XML PATH('CustomerBillDetails'),TYPE        
	 )       
	 FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
	 
			 
		----------------Old One Modified  by Suresh---	 
		 --SELECT   CC.ChargeId AS ChargeID,      
			--	(Select ChargeName from Tbl_MChargeIds WHERE ChargeId=CC.ChargeId) AS ChargeName,      
			--	CustomerAdditioalCharges,      
			--	 Amount,      
			--	(SELECT BillNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId ) AS BillNo,    
			--	(SELECT TotalBillAmount FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillId) AS TotalBillAmount    
		 --FROM  TBL_Customer_Additionalcharges CC WHERE  CC.AccountNO=@AccountNo       
		 --AND CC.CustomerBillId=@CustomerBillId   
 
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- Modified By : Bhimaraju Vanka
-- Desc: Here in Adjustment (No Bill) These details are not having in master table we need do left join 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	
	;WITH PageResult AS(
	SELECT TOP 5 ISNULL(BT.Name,'Adjustment(No Bill)') AS AdjustmentName
		--SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.TotalAmountEffected) AS TotalAmountEffected
			,CONVERT(DECIMAL(18,2),BA.AmountEffected) AS AmountEffected
			,ISNULL(BA.AdjustedUnits,0) AS AdjustedUnits
			,BA.BatchNo
			--,(CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		--JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		)
		
		
	SELECT
	(	
		SELECT AdjustmentName
				--,(REPLACE(CONVERT(VARCHAR(25), CAST(AmountEffected AS MONEY), 1),'-','')+Format) AS NewAmountEffected
				,AmountEffected
				,TotalAmountEffected
				,AdjustedUnits
				,BatchNo
				,CreatedDate
		FROM PageResult
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchAdjustmentStatus]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Faiz-ID103>
-- Create date: <23-DEC-2014>
-- Description:	<Retriving Batch Adjustment Status (Open/Close)>
-- Author:		<Faiz-ID103>
-- Create date: <13-May-2015>
-- Description:	Added the BU_ID for filtering the records
-- Modified By : Bhimaraju Vanka
-- Desc : Added Batch Status Id Condition
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBatchAdjustmentStatus]
(
	@XmlDoc XML
)
AS
	BEGIN
		DECLARE @BU_ID VARCHAR(50)
				,@PageNo INT
				,@PageSize INT
		
		SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')     
				,@PageNo = C.value('(PageNo)[1]','INT')
				,@PageSize = C.value('(PageSize)[1]','INT')	      
		FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)   
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH PagedResults AS
		(
			SELECT   ROW_NUMBER() OVER(ORDER BY BA.BatchDate) AS RowNumber 
					,BA.BatchID
					,BA.BatchNo
					,ISNULL(CONVERT(VARCHAR(50),BA.BatchDate,106),'--') AS BatchDate
					,BA.BatchTotal
					,ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0) AS PaidAmount
					,BU_ID AS BUID
					,BillAdjustmentTypeId
					,Name AS BillAdjustmentType
			FROM	Tbl_BATCH_ADJUSTMENT AS BA
			LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentTypeId=BT.BATID
			WHERE	ISNULL(BatchStatusID,1) = 1
					--BatchTotal>(ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0))
					--AND BU_ID=@BU_ID
					AND BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
		)
			SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults PR
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			ORDER BY  convert(datetime, PR.BatchDate, 103) 
	END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================  
--AUTHOR : NEERAJ KANOJIYA  
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS  
--CREATED ON: 4-OCT-14  
--=============================================================  
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]
(  
@XmlDoc XML  
)  
AS  
 BEGIN  
 DECLARE @BatchID INT
   
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')  
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)  
   
   
SELECT (
		SELECT	BA.BatchID,
				BA.BatchNo,
				BA.BatchDate,
				BA.BatchTotal,
				(SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,
				RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode
				(BA.BatchTotal-
				(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,
				convert(int, (BA.BatchTotal-
				(SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,
				(SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType
			FROM  Tbl_BATCH_ADJUSTMENT AS BA
			JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID
			WHERE BA.BatchID=@BatchID
			FOR XML PATH('BillAdjustments'),TYPE          
			),
			(
				SELECT	BA.BatchNO,
						BAT.Name AS AdjustmentName,
						CD.GlobalAccountNumber AS AccountNo,
						BLA.TotalAmountEffected,
						BLA.AmountEffected,
						ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,
						BLA.CreatedDate
				from Tbl_BATCH_ADJUSTMENT		AS BA
				JOIN Tbl_BillAdjustments		AS BLA	ON BA.BatchID = BLA.BatchNo
				JOIN Tbl_BillAdjustmentDetails	AS BAD	ON BLA.BillAdjustmentId=BAD.BillAdjustmentId
				JOIN Tbl_BillAdjustmentType		AS BAT	ON BLA.BillAdjustmentType=BAT.BATID
				JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber
				WHERE BA.BatchID=@BatchID
				FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE      
			)
			FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      

		
		SELECT @AccountNo = GlobalAccountNumber FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							--,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,CD.OutStandingAmount AS TotalDueAmount
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmountWithTax - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationDetailsByCycle]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =======================================================================                      
-- Author  : NEERAJ KANOJIYA                    
-- Create date  : 25 July 2014                      
-- Description  : THIS PROCEDURE WILL GET ESTIMATION DETAILS OF READING  
-- Modified By : Padmini
-- Modified Date : 26-12-2014                 
-- =======================================================================                      
ALTER PROCEDURE [dbo].[USP_GetEstimationDetailsByCycle]                      
(                      
	@XmlDoc xml                      
)                      
AS                      
BEGIN 

	 DECLARE @Year INT      
		 ,@Month int                
		 ,@BU_ID Varchar(MAX)
		 ,@SU_ID Varchar(MAX)
		 ,@SC_ID Varchar(MAX)
		 ,@CycleId Varchar(MAX)
		 ,@BookNo Varchar(MAX)
		 ,@PageNo INT
		 ,@PageSize INT
		               
	  SELECT                      
		   @Year = C.value('(YearId)[1]','INT'),              
		   @Month = C.value('(MonthId)[1]','INT'),      
		   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)'),
		   @SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)'),
		   @SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)'),
		   @CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)'),
		   @BookNo = C.value('(BookNo)[1]','VARCHAR(MAX)'),
		   @PageNo = C.value('(PageNo)[1]','INT'),
		   @PageSize = C.value('(PageSize)[1]','INT')
	   FROM @XmlDoc.nodes('RptUsageConsumedBe') as T(C)         
	  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	  
	    ;WITH Result as      
	   (      
		   SELECT   
			    ClassID   
			   ,TS.ClassName     
			   ,COUNT(CustomerProcedureID)  AS EstimatedCustomers
			   ,(dbo.fn_GetMonthlyUsage_ByTariff(BG.CycleId,ClassID,@Year,@Month)) AS AvgReadingPerCust      
			   ,(select SUM(EnergytoCalculate) from Tbl_EstimationSettings ES WHERE CycleId=BG.CycleId AND BillingYear=@Year AND BillingMonth=@Month AND ES.TariffId= TS.ClassID AND ES.ActiveStatusId=1) AS AvgUsage         
		  FROM Tbl_MTariffClasses(NOLOCK) TS   
		  INNER JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails](NOLOCK) CD ON TS.ClassID=CD.TariffClassID  
		  INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON CD.BookNo=BN.BookNo  --AND  BN.CycleId=@Cycle
		  INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BN.CycleId
		  INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BNM ON BNM.BookNo = BN.BookNo
		  WHERE TS.RefClassID IS NOT NULL
		  GROUP BY ClassID ,TS.ClassName,BG.CycleId				   
	   )      
		         
		SELECT TS.ClassID      
			,TS.ClassName      
			,SUM(CONVERT(INT,ISNULL(B.EstimatedCustomers,0))) AS  EstimatedCustomers
			,SUM(CONVERT(INT,ISNULL(B.AvgReadingPerCust,0))) AS AvgReadingPerCust
			,SUM(CONVERT(INT,ISNULL(B.AvgUsage,0))) AS AvgUsage
			,SUM(CONVERT(INT,ISNULL(B.Total,0))) AS Total
			,ROW_NUMBER() OVER(ORDER BY TS.ClassID ASC ) AS RowNumber         
		INTO #tblResults 
		FROM Tbl_MTariffClasses(NOLOCK) TS   
		LEFT JOIN 
		( 
			SELECT ClassID      
				,ClassName      
				,EstimatedCustomers      
				,AvgReadingPerCust      
				,AvgUsage      
				,(ISNULL(AvgUsage,0) + ISNULL(AvgReadingPerCust,0)) AS Total
			FROM Result 
		)B ON B.ClassID=TS.ClassID 
		GROUP BY TS.ClassName,TS.ClassID 
		
	            
		SELECT ClassID      
			,ClassName      
			,EstimatedCustomers
			,AvgReadingPerCust
			,AvgUsage
			,Total
			,RowNumber         
			,(Select COUNT(0) from #tblResults) as TotalRecords
		FROM #tblResults
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize                      
		
	DROP TABLE #tblResults
END


GO

/****** Object:  StoredProcedure [MASTERS].[USP_ChangeReadCustToDirect]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 7-MARCH-2015
-- Description: The purpose of this procedure is to convert read cust to direct.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 16-Apr-2015
-- Log and Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_ChangeReadCustToDirect]
(  
@XmlDoc xml  
)
AS  
BEGIN  
 DECLARE 
		@MeterNumber VARCHAR(50)  
		,@GlobalAccountNumber VARCHAR(50) 
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT		

	 SELECT  
	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
	  ,@Reason=C.value('(Remarks)[1]','VARCHAR(MAX)')
	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
	  ,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')
	  ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
	IF((SELECT COUNT(0)FROM TBL_ReadToDirectCustomerActivityLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('MastersBE')
		END  
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @GlobalAccountNumber AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsMeterChangeExist FOR XML PATH('MastersBE')  
		END 
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('MastersBE')  
		END
	ELSE IF ((SELECT ISNULL(OutStandingAmount,0) FROM Tbl_PaidMeterDetails WHERE AccountNo=@GlobalAccountNumber AND MeterNo=@MeterNumber AND ActiveStatusId = 1) > 0)
		BEGIN
			SELECT 1 AS IsHavingCapmiAmt FOR XML PATH('MastersBE')  
		END	
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@ReadToDirectId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
											
			INSERT INTO Tbl_ReadToDirectCustomerActivityLogs (	
											 GlobalAccountNo
											,MeterNo
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											)
			
			SET @ReadToDirectId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE TBL_ReadToDirectCustomerActivity 
						SET IsLatest=0
					WHERE GlobalAccountNo = @GlobalAccountNumber
	
					INSERT INTO TBL_ReadToDirectCustomerActivity
														(
														GlobalAccountNo
														,MeterNumber 
														,CreatedBy 
														,CreatedDate 							
														)
												VALUES
														(
														@GlobalAccountNumber
														,@MeterNumber
														,@CreatedBy
														,DBO.fn_GetCurrentDateTime()
														)
												
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
						SET ReadCodeID=1, MeterNumber = NULL
					WHERE GlobalAccountNumber = @GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_ReadToDirectCustomerActivityLogs(  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate)  
					SELECT  
						 ReadToDirectId
						,GlobalAccountNo
						,MeterNo
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
					FROM Tbl_ReadToDirectCustomerActivityLogs WHERE ReadToDirectId = @ReadToDirectId
					
				END
			
			SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
		END
END
--AS  
--BEGIN  
-- DECLARE 
--		@MeterNumber VARCHAR(50)  
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@StatusText VARCHAR(200)='Initiation'
--		,@IsSuccessful BIT=1
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	SET @StatusText='Deserialization'
	
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNo)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('MastersBE') as T(C)  
  
--	SET @StatusText='Update Statement'
	
--	UPDATE TBL_ReadToDirectCustomerActivity 
--	SET IsLatest=0
--	WHERE GlobalAccountNo = @GlobalAccountNumber
	
--	SET @StatusText='Insert Log Statement'
--	INSERT INTO TBL_ReadToDirectCustomerActivityLogs
--												(
--												GlobalAccountNo
--												,MeterNumber
--												,Remarks
--												,CreatedBy 
--												,CreatedDate
--												,ApprovalStatusId
--												)
--										VALUES
--												(
--												@GlobalAccountNumber
--												,@MeterNumber
--												,@Reason
--												,@CreatedBy
--												,DBO.fn_GetCurrentDateTime()
--												,@ApprovalStatusId
--												)
--	SET @StatusText='Update Statement'
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--		SET ReadCodeID=1, MeterNumber = NULL
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
												
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('MastersBE'),TYPE

--END
---------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [MASTERS].[USP_GetAvailableMeterList]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  NEERAJ KANOJIYA      
-- Create date: 08-JAN-2014      
-- Description: The purpose of this procedure is to get list of MeterInformation      
-- Modified By:  Faiz-ID103  
-- Modified date: 08-JAN-2014      
-- Description: Added Business Unit name and Meter Dials field for new customer registration page  
  
-- =============================================      
ALTER PROCEDURE [MASTERS].[USP_GetAvailableMeterList]
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE  @MeterNo VARCHAR(100)      
   ,@MeterSerialNo VARCHAR(100)      
   ,@BUID VARCHAR(50)      
   ,@CustomerTypeId INT      
         
  SELECT         
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')      
   ,@MeterSerialNo = C.value('(MeterSerialNo)[1]','VARCHAR(100)')      
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')      
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')      
  FROM @XmlDoc.nodes('MastersBE') AS T(C)       
        
  IF (@CustomerTypeId=3)      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType     
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo      
       ,BU.BusinessUnitName    
       ,MeterDials  
       ,(CASE WHEN M.IsCAPMIMeter=1 THEN 'Yes' ELSE 'No' END) AS IsCAPMIMeterString
       ,M.CAPMIAmount
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID WHERE M.ActiveStatusId IN(1) AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
				WHERE MeterNumber !='' )      
      AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))       
      AND M.MeterType=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
  ELSE      
   BEGIN      
    SELECT      
    (      
     SELECT      
       MeterId      
       ,MeterNo      
       --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType      
       ,MT.MeterType AS MeterType      
       ,MeterSize      
       ,MeterMultiplier      
       ,MeterBrand      
       ,MeterSerialNo       
       ,MeterDials  
       ,BU.BusinessUnitName    
       ,(CASE WHEN M.IsCAPMIMeter=1 THEN 'Yes' ELSE 'No' END) AS IsCAPMIMeterString
       ,M.CAPMIAmount
      FROM Tbl_MeterInformation AS M      
      Join Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 -- Faiz-ID103
      Join Tbl_BussinessUnits BU on BU.BU_ID=M.BU_ID    
      WHERE M.ActiveStatusId IN(1)      
      AND (M.BU_ID=@BUID OR @BUID='')      
      AND M.MeterNo NOT IN (SELECT DISTINCT MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails       
							WHERE ISNULL(MeterNumber,'') !='' )      
        AND (M.MeterNo NOT IN (SELECT NewMeterNo FROM Tbl_CustomerMeterInfoChangeLogs WHERE ApproveStatusId IN (1,2,4))
				AND M.MeterNo NOT IN (SELECT MeterNo FROM Tbl_AssignedMeterLogs WHERE ApprovalStatusId IN (1,2,4)))   
      AND M.MeterType !=1      
      AND M.MeterNo LIKE '%'+@MeterNo+'%'       
      AND M.MeterSerialNo LIKE '%'+@MeterSerialNo+'%'      
     FOR XML PATH('MastersBE'),TYPE      
    )      
    FOR XML PATH(''),ROOT('MastersBEInfoByXml')      
   END      
         
END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateDesignationActiveStatus]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Faiz - ID103  
-- Create date: 29-JAN-2015  
-- Description: The purpose of this procedure is to update active status of Designation into Tbl_MDesignations
-- =============================================  
ALTER PROCEDURE [dbo].[USP_UpdateDesignationActiveStatus]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE  @DesignationId INT  
			,@ActiveStatusId INT  
			,@ModifiedBy VARCHAR(50)  
	
	SELECT @DesignationId=C.value('(DesignationId)[1]','INT')  
			,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')  
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('MastersBE') as T(C)

	IF (	(NOT EXISTS(SELECT 0 FROM Tbl_UserDetails WHERE DesignationId = @DesignationId)) 
			AND
			(NOT EXISTS(SELECT 0 FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE DesignationId = @DesignationId)))
		BEGIN
			UPDATE Tbl_MDesignations 
				SET ActiveStatusId=@ActiveStatusId  
					,ModifiedBy=@ModifiedBy  
					,ModifiedDate=dbo.fn_GetCurrentDateTime()  
			WHERE DesignationId=@DesignationId  

			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')  
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			--FROM Tbl_UserDetails
			--WHERE DesignationId = @DesignationId
			FOR XML PATH('MastersBE')
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBEDCEmployeeList]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Padmini.M
-- Create date: 09-02-2015
-- Description:	The purpose of this procedure is to get list of BEDC Employees
-- Modified By:		Faiz-ID103
-- Modified Date:	01-Jul-2015
-- Description:		Added Designation Id for designation from Tbl_MDesignations
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetBEDCEmployeeList]
(
@XmlDoc xml
)
AS
BEGIN
		DECLARE  @PageNo INT
			,@PageSize INT		
				
		SELECT 	 
			@PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')	
		FROM @XmlDoc.nodes('EmployeeBE') AS T(C)	
		
		;WITH PagedResults AS
		(
			SELECT
				 ROW_NUMBER() OVER(ORDER BY B.ActiveStatusId ASC , B.CreatedDate DESC ) AS RowNumber
				 ,EmployeeName
				 --,EmployeeDesignation AS Designation
				 ,B.DesignationId
				 ,D.DesignationName AS Designation
				 ,BEDCEmpId AS BEDCEmpId
				 ,EmployeeLocation AS Location
				 ,B.ActiveStatusId
				 ,ISNULL([Address],'--')AS [Address]
				 ,ContactNo AS ContactNo
				 ,ISNULL(AnotherContactNo,'--')AS AnotherContactNo
			FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails B
			JOIN Tbl_MDesignations D ON D.DesignationId = B.DesignationId
			WHERE B.ActiveStatusId IN (1,2)
		)
		
		SELECT 
		  (
			SELECT	*
					,(Select COUNT(0) from PagedResults) as TotalRecords					
			FROM PagedResults
			WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
			FOR XML PATH('EmployeeBE'),TYPE
		)
		FOR XML PATH(''),ROOT('EmployeeListBEInfoByXml')	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBEDCEmployee]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Padmini
-- Create date: 09/02/2015
-- Description:	This procedure is used to update the BEDC Employee
-- Modified By:		Faiz-ID103
-- Modified Date:	01-Jul-2015
-- Description:		Added Designation Id for designation from Tbl_MDesignations 
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateBEDCEmployee] 
(
@XmlDoc xml
)
AS
BEGIN
 DECLARE   @BEDCEmpId INT
          ,@EmployeeName VARCHAR(50)  
		  ,@EmployeeDesignation VARCHAR(50)
		  ,@EmployeeLocation VARCHAR(50)
		  ,@ContactNo VARCHAR(20)
		  ,@AnotherContactNo VARCHAR(20)
		  ,@Address VARCHAR(MAX)
		  ,@ModifiedDate DATETIME
		  ,@ModifiedBy VARCHAR(20)
		  ,@DesignationId INT -- Faiz-ID103
  SELECT  
          @BEDCEmpId=C.value('(BEDCEmpId)[1]','INT')  
          ,@EmployeeName=C.value('(EmployeeName)[1]','VARCHAR(50)')  
         ,@EmployeeDesignation=C.value('(Designation)[1]','VARCHAR(50)') 
         ,@EmployeeLocation=C.value('(Location)[1]','VARCHAR(50)') 
         ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(20)') 
         ,@AnotherContactNo=C.value('(AnotherContactNo)[1]','VARCHAR(20)') 
         ,@Address=C.value('(Address)[1]','VARCHAR(MAX)')   
         ,@ModifiedDate=C.value('(ModifiedDate)[1]','VARCHAR(150)')  
         ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(150)')  
         ,@DesignationId=C.value('(DesignationId)[1]','INT')  -- Faiz-ID103
 FROM @XmlDoc.nodes('EmployeeBE') AS T(C)  	
 
 --IF NOT EXISTS(SELECT 0 FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails 
 --              WHERE EmployeeName=@EmployeeName AND [Address]=@Address AND ContactNo=@ContactNo AND BEDCEmpId!=@BEDCEmpId)
	--BEGIN
		  UPDATE EMPLOYEE.Tbl_MBEDCEmployeeDetails 
		  SET EmployeeName=@EmployeeName
		     ,EmployeeDesignation=@EmployeeDesignation
		     ,DesignationId = @DesignationId
		     ,EmployeeLocation=@EmployeeLocation
		     ,ContactNo=@ContactNo
		     ,AnotherContactNo=CASE @AnotherContactNo WHEN '' THEN NULL ELSE @AnotherContactNo END
		     ,[Address]=@Address
		     ,ModifedBy=@ModifiedBy
		     ,ModifiedDate=dbo.fn_GetCurrentDateTime()
		  WHERE BEDCEmpId=@BEDCEmpId
		  SELECT 1 AS IsSuccess FOR XML PATH('EmployeeBE')
--	END
--ELSE
--	BEGIN		
--		SELECT 1 AS IsEmpNameExists FOR XML PATH('EmployeeBE')
--	END	
		              
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBEDCEmployee]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Padmini
-- Create date: 09/02/2015
-- Description:	This procedure is used to Insert BEDC Employee 
-- Modified By:		Faiz-ID103
-- Modified Date:	01-Jul-2015
-- Description:		Added Designation Id for designation from Tbl_MDesignations
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertBEDCEmployee]    
(
  @XmlDoc xml 
)
AS
BEGIN
  DECLARE  @EmployeeName VARCHAR(50)  
		  --,@EmployeeDesignation VARCHAR(50)
		  ,@EmployeeLocation VARCHAR(50)
		  ,@ContactNo VARCHAR(20)
		  ,@AnotherContactNo VARCHAR(20)
		  ,@Address VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50) 
		  ,@DesignationId INT -- Faiz-ID103
		    
  SELECT  @EmployeeName=C.value('(EmployeeName)[1]','VARCHAR(50)')  
         --,@EmployeeDesignation=C.value('(Designation)[1]','VARCHAR(50)') 
         ,@EmployeeLocation=C.value('(Location)[1]','VARCHAR(50)') 
         ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(20)') 
         ,@AnotherContactNo=C.value('(AnotherContactNo)[1]','VARCHAR(20)') 
         ,@Address=C.value('(Address)[1]','VARCHAR(MAX)')   
         ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(150)')  
         ,@DesignationId=C.value('(DesignationId)[1]','INT')  -- Faiz-ID103
 FROM @XmlDoc.nodes('EmployeeBE') AS T(C)  	
 
	 INSERT INTO EMPLOYEE.Tbl_MBEDCEmployeeDetails
		(
		  EmployeeName
		 --,EmployeeDesignation
		 ,EmployeeLocation
		 ,ContactNo
		 ,AnotherContactNo
		 ,[Address]
		 ,CreatedBy
		 ,CreatedDate
		 ,DesignationId-- Faiz-ID103
		 )
	VALUES(
	     @EmployeeName
	     --,@EmployeeDesignation
	     ,@EmployeeLocation
	     ,@ContactNo
	     ,CASE WHEN @AnotherContactNo='' THEN NULL ELSE @AnotherContactNo END
	     ,@Address
	     ,@CreatedBy
	     ,dbo.fn_GetCurrentDateTime()
	     ,@DesignationId-- Faiz-ID103
	     )
	     
   	SELECT 1 AS IsSuccess 
    FOR xml PATH('EmployeeBE')    	   

END

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateRouteActiveStatus]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-04-2014
-- Description:	To update Active status of RouteManagement
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateRouteActiveStatus]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @RouteId INT,@ActiveStatusId INT,@ModifiedBy VARCHAR(50),@CurrentDate DATETIME
	SELECT
		@RouteId=C.value('(RouteId)[1]','INT')
		,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)
	
--	SELECT @CurrentDate=dbo.fn_GetCurrentDateTime()
	
--		UPDATE Tbl_MRoutes SET ActiveStatusId=@ActiveStatusId
--							  ,ModifiedBy=@ModifiedBy
--							  ,ModifiedDate=@CurrentDate
--						   WHERE RouteId=@RouteId
	
--	SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
--END

IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE RouteSequenceNumber = @RouteId)
		BEGIN
			UPDATE Tbl_MRoutes SET ActiveStatusId=@ActiveStatusId
							  ,ModifiedBy=@ModifiedBy
							  ,ModifiedDate=@CurrentDate
						   WHERE RouteId=@RouteId
	
			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
			WHERE RouteSequenceNumber = @RouteId
			FOR XML PATH('MastersBE')
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 07/07/2015 18:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
  
   SET @NewConsumption= @Consumption-@AdjustedUnits; 
    
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * 5)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO


