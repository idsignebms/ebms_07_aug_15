
GO

/****** Object:  View [dbo].[UDV_CustDetailsForBillGen]    Script Date: 07/07/2015 18:42:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[UDV_CustDetailsForBillGen] AS

SELECT		
 
			CD.GlobalAccountNumber
			--, CD.DocumentNo
			--, CD.AccountNo
			, CD.OldAccountNo
			, CD.Title
			, CD.FirstName
			, CD.MiddleName
			, CD.LastName
			--, CD.KnownAs
			--, CD.EmployeeCode
			--, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo
			, ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo
			--, TD.PhoneNumber
			--, TD.AlternatePhoneNumber
			, CD.ActiveStatusId
			, PD.PoleID
			, PD.TariffClassID AS TariffId
			--, TC.ClassName
			--, PD.IsBookNoChanged
			 , PD.ReadCodeID
			, PD.SortOrder
			--, CAD.Highestconsumption
			, CAD.OutStandingAmount
			, BN.BookNo
			--, BN.BookCode
			, BU.BU_ID
			, BU.BusinessUnitName
			--, BU.BUCode
			, SU.SU_ID
			--, SU.ServiceUnitName
			--, SU.SUCode
			, SC.ServiceCenterId
			--, SC.ServiceCenterName
			--, SC.SCCode
			, C.CycleId
			--, C.CycleName
			--, C.CycleCode
			--, MS.[Status] AS ActiveStatus
			, CD.EmailId
			, PD.MeterNumber
			--, MI.MeterType AS MeterTypeId
			--, PD.PhaseId
			--, PD.ClusterCategoryId
			--, CAD.InitialReading
			, CAD.InitialBillingKWh 
			--, CAD.AvgReading
			--, PD.RouteSequenceNumber AS RouteSequenceNo
			--, CD.ConnectionDate
			--, CD.CreatedDate
			--, CD.CreatedBy
			, PD.CustomerTypeId
			,PD.ClusterCategoryId
			,PD.IsEmbassyCustomer
			,CAD.Highestconsumption
			,CD.ServiceAddressID
			,CD.PostalAddressID
			 
			,CAD.OpeningBalance
			,CD.Service_HouseNo   
			,CD.Service_StreetName    
			,CD.Service_City   
			,CD.Service_ZipCode           
			,TC.ClassName  
			,TC.ClassID
			,CD.Postal_ZipCode
			,CD.Postal_HouseNo   
			,CD.Postal_StreetName    
			,CD.Postal_City
			,CD.Postal_Landmark
			,Cd.Service_Landmark 
			,CD.SetupDate   
			  
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId 
                      INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
                      INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
                      INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
                      INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
                      INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
                      INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
                      LEFT OUTER JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId 
                      LEFT OUTER JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo




GO


