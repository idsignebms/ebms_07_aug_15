
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges_New]    Script Date: 07/10/2015 18:24:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER FUNCTION  [dbo].[fn_GetFixedCharges_New]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
	,@GlobalAccountNumber   varchar(200)
	,@IsFirstMonthBill Bit 
	,@SetupDate DATETIME
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	DECLARE @Multiplier DECIMAL=1
	 
	 
	IF @IsFirstMonthBill =1
	BEGIN
		IF @SetupDate IS NOT NULL 
			BEGIN
			  
			   
			   SET @Multiplier= DateDiff(mm,@SetupDate,DATEADD(day,-30,@MonthStartDate)  )
			    
			END
	 		 
	END


	 

	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount*ISNULL(@Multiplier,0) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN 
	CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END


GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 07/10/2015 18:24:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Satya
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
	Declare @NewAverage decimal(18,2) = 0,@TotalRecords INT = 0
		, @TotalUsage DECIMAL(18,2) = 0
		, @NoOfMonths INT = 12

   SELECT Top 1
         @TotalRecords = TotalReadings
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
   
    IF(ISNULL(@TotalRecords,0) >= @NoOfMonths)
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
			AND TotalReadings > (@TotalRecords - @NoOfMonths + 1)
			
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / @NoOfMonths
			ELSE
				SET @NewAverage = @usage
			
		END
    ELSE
		BEGIN
			SELECT 
				@TotalUsage = SUM(ISNULL(Usage,0))
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
						
			IF(ISNULL(@TotalUsage,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / (ISNULL(@TotalRecords,0) + 1)
			ELSE
				SET @NewAverage = @usage
			
		END
 
	RETURN @NewAverage

END
