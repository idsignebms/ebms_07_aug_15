
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadings]    Script Date: 07/13/2015 15:55:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 13-Jul-2015
-- Description:	The purpose of this procedure is to get the last 6 months Reading of the read customers
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCustomerReadings]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @GlobalAccountNumber VARCHAR(50)  
			,@IsSuccess BIT	
			,@IsCustExistsInOtherBU BIT
			,@BU_ID VARCHAR(50)
			,@Flag INT
			,@FlagDetails INT
			,@ActiveStatusId INT  
    SELECT              
    @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)'),   
    @BU_ID = C.value('(BUID)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)       
	
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1)
		BEGIN
		IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
				BEGIN
				SELECT
				(
					SELECT 
					 dbo.fn_GetCustomerFullName(CR.GlobalAccountNumber) AS Name
					 ,CD.ClassName As Tariff
					 ,CD.OutStandingAmount
					  from UDV_CustomerDescription CD
					  left Join Tbl_CustomerReadings CR ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
					  where CD.GlobalAccountNumber = @GlobalAccountNumber
					 FOR XML PATH('BillingBE'),TYPE    
				)
				,
				(
					SELECT (datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate)) as ReadingMonth
							,ReadDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
							,SUM(Usage) as Usage
							,MeterNumber
							,AverageReading
							,PreviousReading as prv
							,PresentReading as curr
							,R.ReadCode
							,ISNULL(BilledDate,'-') AS BilledDate
					FROM    Tbl_CustomerReadings CR
					JOIN Tbl_MReadCodes R ON R.ReadCodeId = CR.ReadType
					WHERE   GlobalAccountNumber =@GlobalAccountNumber--'0000046744'
					 --and ReadDate > dateadd(m, -6, CONVERT(DATETIME,'13/07/2015',103) - datepart(d, CONVERT(DATETIME,'13/05/2015',103)) + 1)
					 and ReadDate > dateadd(m, -6, GETDATE() - datepart(d, GETDATE()) + 1)
					GROUP BY DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
							, MeterNumber
							,(datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate))        
							,R.ReadCode
							,BilledDate
							,AverageReading
							,PreviousReading
							,PresentReading
							FOR XML PATH('BillingListBE'),TYPE    
				)	
				FOR XML PATH(''),ROOT('CustomerReadingListBeByXml')       		 	
				END
		ELSE		
				BEGIN
					SELECT 1 as IsDirectCustomer
					FOR XML PATH(''),ROOT('BillingListBE')       	
				END
		END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]    Script Date: 07/13/2015 15:55:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  13/JULY/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
CREATE PROCEDURE [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId = ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND (@ActiveStatusId=1 OR @ActiveStatusId=2)) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT    
       CD.GlobalAccountNumber AS GlobalAccountNumber    
      ,CD.AccountNo As AccountNo    
      ,CD.FirstName    
      ,CD.MiddleName    
      ,CD.LastName    
      ,CD.Title          
      ,CD.KnownAs    
      ,CD.CustomerTypeId  
      ,CT.CustomerType  
      ,ISNULL(MeterNumber,'--') AS MeterNo    
      ,CD.ClassName AS Tariff    
      ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo    
      FROM UDV_CustomerDescription  CD    
      join Tbl_MCustomerTypes CT  
      on CD.CustomerTypeId=CT.CustomerTypeId  
      WHERE CD.GlobalAccountNumber=@GlobalAccountNumber      
      FOR XML PATH('ChangeCustomerTypeBE')   
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadings]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 13-Jul-2015
-- Description:	The purpose of this procedure is to get the last 6 months Reading of the read customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerReadings]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @GlobalAccountNumber VARCHAR(50)  
			,@IsSuccess BIT	
			,@IsCustExistsInOtherBU BIT
			,@BU_ID VARCHAR(50)
			,@Flag INT
			,@FlagDetails INT
			,@ActiveStatusId INT  
    SELECT              
    @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)'),   
    @BU_ID = C.value('(BUID)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)       
	
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1)
		BEGIN
		IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
				BEGIN
				SELECT
				(
					SELECT 
					 dbo.fn_GetCustomerFullName(CR.GlobalAccountNumber) AS Name
					 ,CD.ClassName As Tariff
					 ,CD.OutStandingAmount
					  from UDV_CustomerDescription CD
					  left Join Tbl_CustomerReadings CR ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
					  where CD.GlobalAccountNumber = @GlobalAccountNumber
					 FOR XML PATH('BillingBE'),TYPE    
				)
				,
				(
					SELECT (datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate)) as ReadingMonth
							,ReadDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
							,SUM(Usage) as Usage
							,MeterNumber
							,AverageReading
							,PreviousReading as prv
							,PresentReading as curr
							,R.ReadCode
							,ISNULL(BilledDate,'-') AS BilledDate
					FROM    Tbl_CustomerReadings CR
					JOIN Tbl_MReadCodes R ON R.ReadCodeId = CR.ReadType
					WHERE   GlobalAccountNumber =@GlobalAccountNumber--'0000046744'
					 --and ReadDate > dateadd(m, -6, CONVERT(DATETIME,'13/07/2015',103) - datepart(d, CONVERT(DATETIME,'13/05/2015',103)) + 1)
					 and ReadDate > dateadd(m, -6, GETDATE() - datepart(d, GETDATE()) + 1)
					GROUP BY DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
							, MeterNumber
							,(datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate))        
							,R.ReadCode
							,BilledDate
							,AverageReading
							,PreviousReading
							,PresentReading
							FOR XML PATH('BillingListBE'),TYPE    
				)	
				FOR XML PATH(''),ROOT('CustomerReadingListBeByXml')       		 	
				END
		ELSE		
				BEGIN
					SELECT 1 as IsDirectCustomer
					FOR XML PATH(''),ROOT('BillingListBE')       	
				END
		END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1,2'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	IF(@BillingRule = 1) -- As per settings
		BEGIN
			SELECT
			(			
				SELECT 
					 ClassID
					,TC.ClassName
					,CC.CategoryName
					,CC.ClusterCategoryId 
					,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
					,Cycles.CycleId 
					,Cycles.CycleName
					,MAX(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=2 then
						(case 
						when isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.DisableTypeId,0) = 2 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 1 
							else 0
							end
						else 0
						end)
						else 0 end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,ISNULL(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,isnull(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,0)  
														Else 0 end)
											else  0 end ) 
								else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then 0
															Else (case when  CR.Usage IS NULL  
																		then isnull(CAD.AvgReading,0) 
																		else 0 end) end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when DCAVG.AverageReading IS NULL  
																		then isnull(CAD.AvgReading,CAD.InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
				INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
				INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
				INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
				INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
				INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
				LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
				LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
				LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
				LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
						AND ES.ClusterCategoryId = CC.ClusterCategoryId 
						AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
						And ES.BillingMonth= @Month	   
						AND ES.BillingYear=	@year					 
				GROUP BY TC.ClassID,TC.ClassName,CC.CategoryName
					,CC.ClusterCategoryId,Cycles.CycleCode,Cycles.CycleName
					,Cycles.CycleId 
				ORDER BY Cycles.CycleId,CC.ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					 ClassID
					,TC.ClassName
					,CC.CategoryName
					,CC.ClusterCategoryId 
					,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
					,Cycles.CycleId 
					,Cycles.CycleName
					,MAX(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=2 then
						(case 
						when isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.DisableTypeId,0) = 2 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 1 
							else 0
							end
						else 0
						end)
						else 0 end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,ISNULL(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,isnull(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,0)  
														Else 0 end)
											else  0 end ) 
								else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(CPD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then 0
															Else (case when  CR.Usage IS NULL  
																		then isnull(CAD.AvgReading,0) 
																		else 0 end) end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(CPD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,1) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when DCAVG.AverageReading IS NULL  
																		then isnull(CAD.AvgReading,CAD.InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
				INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID and CPD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
				INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
				INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
				INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
				INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
				LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
				LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
				LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
				LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
						AND ES.ClusterCategoryId = CC.ClusterCategoryId 
						AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
						And ES.BillingMonth= @Month	   
						AND ES.BillingYear=	@year					 
				GROUP BY TC.ClassID,TC.ClassName,CC.CategoryName
					,CC.ClusterCategoryId,Cycles.CycleCode,Cycles.CycleName
					,Cycles.CycleId 
				ORDER BY Cycles.CycleId,CC.ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		END
	
	DROP TABLE #ReadCustomersList
	
END



--SELECT ClassID
--	,
--	SUM(
--			case 
--			when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
--			then

--			case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 else
--																case 
--																when  
--																isnull(BDB.DisableTypeId,2) =1 
--																then 0  
--																else 1 
--																end
--			end
--			else 
--			0
--			end
--			)
	
--	as TotalReadCustomers
--	,
--	SUM ( case 
--	when 
--	(CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and
--	(isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
--	then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
--	then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )  
--	 as EstimatedCustomers
--	,TC.ClassName
--	,CC.CategoryName
--	,CC.ClusterCategoryId 
--	,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
--	,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
--	,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
--	,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
--	,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
--	,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
--	,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
--	,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
--	,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
--	,Cycles.CycleId 
--	,Cycles.CycleName
--FROM Tbl_MTariffClasses(NOLOCK)	TC
--INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
--INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
--INNER JOIN   MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
--INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
--INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
--LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
--LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
--LEFT JOIN Tbl_BillingDisabledBooks BDB
--ON  BDB.BookNo=BN.BookNo and IsActive=1
--LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
--		and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
--		And ES.BillingMonth = @Month -- Need To Modify	 
--		AND ES.BillingYear = @Year	 -- Need To Modify
--Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
--ORDER BY Cycles.CycleId,CC.ClusterCategoryId

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersExportByBookWise]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 31-10-2014
-- Modified date: 02-12-2014
-- Description:	The purpose of this procedure is to get Customers export by Book wise
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomersExportByBookWise]
(
@XmlDoc xml
)
AS
BEGIN

	DECLARE @BUID VARCHAR(50)=''
		,@SUID VARCHAR(MAX)=''
		,@SCID VARCHAR(MAX)=''
		,@CycleId VARCHAR(MAX)=''
		,@BookNos VARCHAR(MAX)=''
		
		SELECT
			@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
		
		IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	select 
		 CustomerList.AccountNo
		 , CustomerList.GlobalAccountNumber
		 ,Name
		 ,OldAccountNo
		 ,MeterNumber
		 ,BusinessUnitName
		 ,ServiceUnitName
		 ,ServiceCenterName
		 ,ID AS BookCode
		 ,' ' CurrentReading
		 ,' ' AS Remarks
		 ,' ' AS PreviousReading
		 ,ServiceAddress as [Address]
		 ,BookSortOrder
		 ,SortOrder as CustomerSortOrder
		 ,InitialReading
		 ,CycleName
		 ,ClassName
		 ,CustomerList.BookNo
	 INTO  #TotalCustomersList
	 FROM UDV_SearchCustomer   CustomerList
	 WHERE ReadCodeId=2 AND CustomerList.ActiveStatusId=1
	 AND (CustomerList.BU_ID=@BUID OR @BUID='') 
	 AND CustomerList.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,','))
	 AND CustomerList.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,','))
	 AND CustomerList.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
	 AND CustomerList.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,','))


SELECT   max(CustomerReadingId) as CurrentReadingID , CL.GlobalAccountNumber 
INTO #ReadingsList from Tbl_CustomerReadings	 CR
INNER JOIN #TotalCustomersList CL
ON CL.GlobalAccountNumber=CR.GlobalAccountNumber
GROUP BY CL.GlobalAccountNumber	  

SELECT RL.GlobalAccountNumber,CR.PresentReading as PreviousReading
INTO #ReadingsListWIthPreviousReading
FROM #ReadingsList  RL
INNER JOIN	 Tbl_CustomerReadings CR
ON		CR.CustomerReadingId=RL.CurrentReadingID

SELECT (TCL.AccountNo+' - '+TCL.GlobalAccountNumber) AS GlobalAccountNumber
	 ,Name
	 ,OldAccountNo
	 ,MeterNumber AS MeterNo
	 ,BusinessUnitName
	 ,ServiceUnitName
	 ,ServiceCenterName
	 ,BookCode AS BookNumber
	 ,' ' CurrentReading
	 ,' ' AS Remarks
	 ,TCL.[Address]
	 ,BookSortOrder
	 ,CustomerSortOrder
	 ,ClassName
	 ,CycleName
	 ,BookNo
	 ,ISNULL(RL.PreviousReading,TCL.InitialReading) as PreviousReading 
	 From   #TotalCustomersList	TCL
	LEFT JOIN  #ReadingsListWIthPreviousReading	RL
	ON	 TCL.GlobalAccountNumber=RL.GlobalAccountNumber
	WHERE BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE DisableTypeId=1 AND IsActive=1)


	DROP TABLE	 #TotalCustomersList
	DROP TABLE	 #ReadingsListWIthPreviousReading
	DROP TABLE	 #ReadingsList
		
		--SELECT CD.AccountNo
		--	  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		--	  ,ISNULL(CD.OldAccountNo,'') AS OldAccountNo
		--	  ,cd.MeterNumber AS MeterNo
		--	  ,CD.BusinessUnitName
		--	  ,CD.ServiceUnitName
		--	  ,CD.ServiceCenterName
		--	  ,CD.CycleName
		--	  ,BN.BookCode
		--	  ,'' AS CurrentReading
		--	  ,'' AS Remarks
		--	  ,(SELECT TOP(1) PreviousReading FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=CD.GlobalAccountNumber
		--			ORDER BY CustomerReadingId DESC) AS PreviousReading
		--	  ,dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber) AS [Address]
		--	  ,BN.SortOrder AS BookSortOrder
		--	  ,CD.SortOrder AS CustomerSortOrder
		-- FROM [UDV_CustomerDescription] CD
		-- --JOIN Tbl_Cycles AS C ON CD.CycleId=C.CycleId
		-- JOIN Tbl_BookNumbers BN ON CD.BookNo = BN.BookNo
		-- --JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID
		-- --JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID
		-- --JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CD.ServiceCenterId
		-- --JOIN Tbl_Cycles BC ON BC.CycleId = C.CycleId
		-- WHERE ReadCodeId=2 AND CD.ActiveStatusId=1
		-- AND (CD.BU_ID=@BUID OR @BUID='')
		-- AND (CD.SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		-- AND (CD.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		-- AND (CD.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		-- --AND (CD.BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')
		-- AND (CD.BookNo IN
		--	(SELECT BookNo FROM Tbl_BookNumbers WHERE ActiveStatusId=1
		--	 AND (BU_ID=@BUID OR @BUID='')
		--	 AND (SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		--	 AND (ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		--	 AND (CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		--	 AND (BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')) OR @BookNos='')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  13/JULY/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId = ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND (@ActiveStatusId=1 OR @ActiveStatusId=2)) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT    
       CD.GlobalAccountNumber AS GlobalAccountNumber    
      ,CD.AccountNo As AccountNo    
      ,CD.FirstName    
      ,CD.MiddleName    
      ,CD.LastName    
      ,CD.Title          
      ,CD.KnownAs    
      ,CD.CustomerTypeId  
      ,CT.CustomerType  
      ,ISNULL(MeterNumber,'--') AS MeterNo    
      ,CD.ClassName AS Tariff    
      ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo    
      FROM UDV_CustomerDescription  CD    
      join Tbl_MCustomerTypes CT  
      on CD.CustomerTypeId=CT.CustomerTypeId  
      WHERE CD.GlobalAccountNumber=@GlobalAccountNumber      
      FOR XML PATH('ChangeCustomerTypeBE')   
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillReadings_ByAccountNo]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================              
-- Author  : Naresh Kumar                
-- Create date  : 17 Apr 2014              
-- Description  : The purpose of this procedure is to get Reading details based on Accno and Billno           
-- ModifiedBy : NEERAJ        
-- Modified On : 28-Aug      
-- ModifiedBy : NEERAJ        
-- Modified On : 28-FEB
-- DESC			: ADDED MeterMultiplier FIELD  
-- ModifiedBy : NEERAJ        
-- Modified On : 1-March-2015
-- DESC			: change join to left join for meter information for direct customers.
-- =============================================              
ALTER PROCEDURE [dbo].[USP_GetCustomerBillReadings_ByAccountNo]  
(              
@XmlDoc xml              
)              
AS              
BEGIN              
 DECLARE @AccountNo VARCHAR(20)              
          ,@CustomerBillID INT            
           ,@TariffId Varchar(20)             
           ,@ReadCode INT             
            ,@Month INT = 4      
   ,@Year INT = 2014      
   ,@Date VARCHAR(50)        
               
 SELECT              
  @AccountNo = C.value('(AccountNo)[1]','VARCHAR(20)')              
  ,@CustomerBillID = C.value('(CustomerBillID)[1]','INT')              
 FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
          SET @Month=(SELECT BillMonth FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)         
   SET @Year= (SELECT BillYear FROM Tbl_CustomerBills WHERE CustomerBillID=@CustomerBillID)      
    SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
    IF(@AccountNo='')  
        SELECT @AccountNo=AccountNo FROM Tbl_CustomerBills WHERE CustomerBillId=@CustomerBillID  
 SET @TariffId=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)             
  SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WHERE GlobalAccountNumber=@AccountNo)  
  
  DECLARE @TotalConsumption DECIMAL(18,2) = 0
  DECLARE @AdjustmentedAmount DECIMAL(18,2) = 0
  
  SELECT @AdjustmentedAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId 
  
  SELECT @TotalConsumption = SUM(AdjustedUnits) FROM Tbl_BillAdjustments CAC 
  WHERE CAC.AccountNo = @AccountNo AND CAC.CustomerBillId = @CustomerBillId AND BillAdjustmentType = 3
             
 SELECT (               
   SELECT       -- Get bill details for meter reading adjustment         
       B.AccountNo,              
       B.TotalBillAmount,                               
       B.BillNo,              
       B.PreviousReading ,              
       B.PresentReading AS PresentReading,              
       B.Usage AS Consumption,        
       B.TotalBillAmountWithTax, 
       (B.TotalBillAmountWithTax - ISNULL(@AdjustmentedAmount,0)) AdditionalCharges,
       (SELECT DBO.fn_GetMeterDials_ByAccountNO(@AccountNo)) AS MeterDials,      
       CONVERT(DECIMAL(18,2),B.VATPercentage) AS TaxPercent,         
       (SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffId) AS Tariff,        
       (Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo) AS TariffId,        
       CONVERT(DECIMAL(18,2),B.NetFixedCharges) AS FixedCharges,               
       @ReadCode AS ReadCodeId,            
       NetEnergyCharges,  
      (SELECT dbo.fn_GetPreviousBalanceUsage(B.AccountNo)) AS BalanceUsage  
	  ,ISNULL(MI.MeterMultiplier,1) AS MeterMultiplier     
	  ,B.VAT AS Vat
	  ,ISNULL(@TotalConsumption,0) AS GrandTotal 
    FROM Tbl_CustomerBills B    
    left JOIN Tbl_MeterInformation AS MI ON B.MeterNo=MI.MeterNo              
    WHERE B.CustomerBillID=@CustomerBillID         
    FOR XML PATH('CustomerBillDetails'),Type             
    ) ,        
    (  
     -- Get all tariff details for meter reading adjustment        
       --SELECT ClassID AS TariffId,ClassName AS Tariff FROM Tbl_MTariffClasses WHERE IsActiveClass=1       
    SELECT   
     ClassID,FromKW ,(CASE ISNULL(ToKW,'') WHEN '' THEN '&#8734;' ELSE CONVERT(varchar(10),ToKW) END)AS ToKW,Amount,TaxId   
    FROM Tbl_LEnergyClassCharges       
    WHERE ClassID  = (SELECT A.TariffClassID FROM CUSTOMERS.Tbl_CustomerProceduralDetails A WHERE GlobalAccountNumber = @AccountNo)        
    AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)       
    ORDER BY FromKW ASC     
    FOR XML PATH('CustomerBillDetails'),Type        
   )        
     FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')                    
             
              
END  
-------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate
					,ApprovedBy 
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					,@ModifiedBy
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill--commented by neeraj as per SATYA 
							
							--UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							--WHERE CustomerBillId=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_MeterReadin]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  Satya 
-- Create date: 06-Apr-2015  
-- Description: The purpose of this procedure is to get Books list By Cycle for Book Wise Meter reading page     
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_MeterReadin]        
(        
 @XmlDoc xml        
)        
AS        
BEGIN        
        
 DECLARE @CycleId VARCHAR(MAX)         
     
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')        
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)        
     
 IF(@CycleId != '')        
  BEGIN        
   SELECT        
   (        
      
        
    --ORDER BY B.BookNo ASC        
      
      
    select  B.BookNo        
      ,(CASE     
       WHEN ISNULL(ID,'') = ''     
        THEN B.BookCode     
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')     
      END) AS BookNoWithDetails    
      ,COUNT(B.BookNo) over() as BookNo  
       from Tbl_BookNumbers   B  
    where ActiveStatusId=1   
    and B.BookNo NOT IN (    select BookNo from   
    Tbl_BillingDisabledBooks where   DisableTypeId =1  and IsActive=1  
     )    
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))    
    FOR XML PATH('Reports'),TYPE        
   )        
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')        
  END        
 ELSE        
  BEGIN        
   SELECT        
   (        
    SELECT (CASE     
       WHEN ISNULL(ID,'') = ''     
        THEN BookCode     
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')     
      END) AS BookNo          
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1        
    ORDER BY BookNo ASC        
    FOR XML PATH('Reports'),TYPE        
   )        
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')        
  END        
         
END    
    

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_MeterReading]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Jeevan  
-- Create date: 18-04-2015    
-- Description: The purpose of this procedure is to get Books list By Cycle    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_MeterReading]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
    
	DECLARE @CycleId VARCHAR(MAX)     
	
	SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')    
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)    
	
	IF(@CycleId != '')    
		BEGIN    
			SELECT    
			(    
				SELECT B.BookNo    
						,(CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN B.BookCode 
							ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')') 
						END) AS BookNoWithDetails
						,COUNT(CPD.BookNo)
				FROM Tbl_BookNumbers B , CUSTOMERS.Tbl_CustomerProceduralDetails CPD
				WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
				AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)
				AND B.ActiveStatusId=1    AND CPD.BookNo=B.BookNo
				GROUP BY 
				B.BookNo,ID,B.BookCode
				HAVING COUNT(CPD.BookNo)>0
				
				ORDER BY B.BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
	ELSE    
		BEGIN    
			SELECT    
			(    
				SELECT (CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN BookCode 
							ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')') 
						END) AS BookNo      
				FROM Tbl_BookNumbers WHERE ActiveStatusId=1    
				ORDER BY BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
     
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  V.Bhimaraju      
-- Create date: 28-08-2014      
-- Modified By: T.Karthik    
-- Modified date: 03-11-2014    
-- Description: The purpose of this procedure is to get Books list By Cycle      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
      
 DECLARE @CycleId VARCHAR(MAX)       
   
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)      
   
 IF(@CycleId != '')      
  BEGIN      
   SELECT      
   (      
    --SELECT B.BookNo      
    --  ,(CASE   
    --   WHEN ISNULL(ID,'') = ''   
    --    THEN B.BookCode   
    --   ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
    --  END) AS BookNoWithDetails  
    --  ,COUNT(CPD.BookNo)  
    --FROM Tbl_BookNumbers B , CUSTOMERS.Tbl_CustomerProceduralDetails CPD  
    --WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))    
    --AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    --AND B.ActiveStatusId=1    AND CPD.BookNo=B.BookNo  
    --GROUP BY   
    --B.BookNo,ID,B.BookCode  
    --HAVING COUNT(CPD.BookNo)>0  
      
    --ORDER BY B.BookNo ASC      
    
    
    select  B.BookNo      
      ,(CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN B.BookCode   
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
      END) AS BookNoWithDetails  
      ,COUNT(B.BookNo) over() as BookNo
       from Tbl_BookNumbers   B
    where ActiveStatusId=1 
    --and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND ISNULL(IsPartialBill,0) = 0)  
    and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND IsPartialBill=1)  
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
 ELSE      
  BEGIN      
   SELECT      
   (      
    SELECT (CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN BookCode   
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')   
      END) AS BookNo        
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1      
    ORDER BY BookNo ASC      
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
       
END  
  
GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 07/13/2015 15:55:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT	
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		 ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END  
	ELSE IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE MeterNo = @MeterNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterAlreadyAssigned FOR XML PATH('CustomerRegistrationBE')
		END 
	ELSE IF((SELECT COUNT(0)FROM Tbl_CustomerMeterInfoChangeLogs 
				WHERE NewMeterNo = @MeterNumber AND ApproveStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterChangeApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END  
	ELSE IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
						DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1		
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
						,IsCAPMI = @IsCAPMIMeter
						,MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
					IF(@IsCAPMIMeter = 1)  
						BEGIN  
							INSERT INTO Tbl_PaidMeterDetails  
							(  
							 AccountNo  
							,MeterNo  
							,MeterTypeId  
							,MeterCost  
							,OutStandingAmount
							,ActiveStatusId  
							,MeterAssignedDate  
							,CreatedDate  
							,CreatedBy  
							)  
							values  
							(  
							 @GlobalAccountNumber  
							,@MeterNumber  
							,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
							,@CAPMIAmount  
							,@CAPMIAmount  
							,1  
							,@AssignedMeterDate  
							,dbo.fn_GetCurrentDateTime()  
							,@CreatedBy  
							)       
						END  
			
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END


GO


