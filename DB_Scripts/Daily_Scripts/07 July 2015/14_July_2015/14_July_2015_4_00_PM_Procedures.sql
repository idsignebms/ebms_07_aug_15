
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]    Script Date: 07/14/2015 16:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 14-07-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1,2'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT DISTINCT
		 ClassID
		,TC.ClassName
		,CC.CategoryName
		,CC.ClusterCategoryId 
		,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
		,Cycles.CycleId 
		,Cycles.CycleName
		,CPD.CustomerTypeId
		,CSD.ActiveStatusId
		,BDB.IsActive DisabledActiveStatus
		,BDB.DisableTypeId
		,BDB.IsPartialBill
		,ReadCodeID	  
		,CPD.GlobalAccountNumber CustomerGlobalAccNo
		,CR.GlobalAccountNumber ReadingGlobalAccNo
		,DCAVG.GlobalAccountNumber AvgGlobalAccNo
		,CR.Usage
		,CAD.AvgReading CustomerAvgReading
		,CAD.InitialBillingKWh  
		,DCAVG.AverageReading UploadedAvgReading
		,ES.EnergytoCalculate
	INTO #ReaultData
	FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
	INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID 
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail (NOLOCK) CSD ON CSD.GlobalAccountNumber=CPD.GlobalAccountNumber and CSD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
	INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
	INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
	INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
	LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
	LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
	LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
			AND ES.ClusterCategoryId = CC.ClusterCategoryId 
			AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
			And ES.BillingMonth= @Month	   
			AND ES.BillingYear=	@year
	
	
	
			SELECT
			(			
				SELECT 
					 ClassID
					,ClassName
					,CategoryName
					,ClusterCategoryId 
					,Cycle
					,CycleId 
					,CycleName
					,MAX(isnull(EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case 
						when isnull(CustomerTypeId,0)<>3
						then
							case
							when ((ISNULL(ActiveStatusId,1)=2) OR (ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(DisableTypeId,0) = 2 AND ISNULL(IsPartialBill,0) = 0))
							then 1 
							else 0
							end
						else 0
						end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,ISNULL(CustomerAvgReading,InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(UploadedAvgReading,isnull(CustomerAvgReading,InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  ISNULL(UploadedAvgReading,0)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then (case when  Usage IS NULL  
																		then ISNULL(CustomerAvgReading,InitialBillingKWh)
																		else 0 end)
															Else 0
															 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when UploadedAvgReading IS NULL  
																		then isnull(CustomerAvgReading,InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM #ReaultData				 
				GROUP BY ClassID,ClassName,CategoryName
					,ClusterCategoryId,CycleName,Cycle
					,CycleId 
				ORDER BY CycleId,ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		
	DROP TABLE #ReadCustomersList
	DROP TABLE #ReaultData
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 07/14/2015 16:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
CREATE PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
		BEGIN
			SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
		BEGIN
			SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=1)
		BEGIN
			SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE
		BEGIN		
		
			SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@GlobalAccountNumber,@Usage)
			SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
			SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					--,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					--,IsRollOver
					)
				VALUES(
					 @GlobalAccountNumber
					,GETDATE()
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
					,@Multipiler
					,2
					,@CreatedBy
					,GETDATE()
					--,@IsTamper
					,@MeterNumber
					,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
					--,@IsRollover
					)
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@PreviousReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE 	
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_NotDisabled]    Script Date: 07/14/2015 16:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Karteek 
-- Create date: 14-07-2015
-- Description: The purpose of this procedure is to get Books list By Cycle      
-- =============================================      
CREATE PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_NotDisabled]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
      
 DECLARE @CycleId VARCHAR(MAX)       
   
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)      
   
 IF(@CycleId != '')      
  BEGIN      
   SELECT      
   (     
    select  B.BookNo      
      ,(CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN B.BookCode   
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
      END) AS BookNoWithDetails  
      ,COUNT(B.BookNo) over() as BookNo
       from Tbl_BookNumbers   B
    where ActiveStatusId=1 
    and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
 ELSE      
  BEGIN      
   SELECT      
   (      
    SELECT (CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN BookCode   
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')   
      END) AS BookNo        
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1      
    ORDER BY BookNo ASC      
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
       
END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_BookwiseExport]    Script Date: 07/14/2015 16:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Karteek  
-- Create date: 14-07-2015    
-- Description: The purpose of this procedure is to get Books list By Cycle    
-- =============================================    
CREATE PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_BookwiseExport]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
    
	DECLARE @CycleId VARCHAR(MAX)     
	
	SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')    
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)    
	
	IF(@CycleId != '')    
		BEGIN    
			SELECT    
			(    
				SELECT B.BookNo    
						,(CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN B.BookCode 
							ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')') 
						END) AS BookNoWithDetails
				FROM Tbl_BookNumbers B 
				WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
				AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND ISNULL(IsPartialBill,0) = 0)
				AND B.ActiveStatusId=1    
				GROUP BY 
				B.BookNo,ID,B.BookCode				
				ORDER BY B.BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
	ELSE    
		BEGIN    
			SELECT    
			(    
				SELECT (CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN BookCode 
							ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')') 
						END) AS BookNo      
				FROM Tbl_BookNumbers WHERE ActiveStatusId=1    
				ORDER BY BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
     
END


GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 14-07-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1,2'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT DISTINCT
		 ClassID
		,TC.ClassName
		,CC.CategoryName
		,CC.ClusterCategoryId 
		,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
		,Cycles.CycleId 
		,Cycles.CycleName
		,CPD.CustomerTypeId
		,CSD.ActiveStatusId
		,BDB.IsActive DisabledActiveStatus
		,BDB.DisableTypeId
		,BDB.IsPartialBill
		,ReadCodeID	  
		,CPD.GlobalAccountNumber CustomerGlobalAccNo
		,CR.GlobalAccountNumber ReadingGlobalAccNo
		,DCAVG.GlobalAccountNumber AvgGlobalAccNo
		,CR.Usage
		,CAD.AvgReading CustomerAvgReading
		,CAD.InitialBillingKWh  
		,DCAVG.AverageReading UploadedAvgReading
		,ES.EnergytoCalculate
	INTO #ReaultData
	FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
	INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID 
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail (NOLOCK) CSD ON CSD.GlobalAccountNumber=CPD.GlobalAccountNumber and CSD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
	INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
	INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
	INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
	LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
	LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
	LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
			AND ES.ClusterCategoryId = CC.ClusterCategoryId 
			AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
			And ES.BillingMonth= @Month	   
			AND ES.BillingYear=	@year
	
	
	
			SELECT
			(			
				SELECT 
					 ClassID
					,ClassName
					,CategoryName
					,ClusterCategoryId 
					,Cycle
					,CycleId 
					,CycleName
					,MAX(isnull(EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case 
						when isnull(CustomerTypeId,0)<>3
						then
							case
							when ((ISNULL(ActiveStatusId,1)=2) OR (ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(DisableTypeId,0) = 2 AND ISNULL(IsPartialBill,0) = 0))
							then 1 
							else 0
							end
						else 0
						end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,ISNULL(CustomerAvgReading,InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(UploadedAvgReading,isnull(CustomerAvgReading,InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  ISNULL(UploadedAvgReading,0)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then (case when  Usage IS NULL  
																		then ISNULL(CustomerAvgReading,InitialBillingKWh)
																		else 0 end)
															Else 0
															 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when UploadedAvgReading IS NULL  
																		then isnull(CustomerAvgReading,InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM #ReaultData				 
				GROUP BY ClassID,ClassName,CategoryName
					,ClusterCategoryId,CycleName,Cycle
					,CycleId 
				ORDER BY CycleId,ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		
	DROP TABLE #ReadCustomersList
	DROP TABLE #ReaultData
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadings]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 13-Jul-2015
-- Description:	The purpose of this procedure is to get the last 6 months Reading of the read customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerReadings]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @GlobalAccountNumber VARCHAR(50)  
			,@IsSuccess BIT	
			,@IsCustExistsInOtherBU BIT
			,@BU_ID VARCHAR(50)
			,@Flag INT
			,@FlagDetails INT
			,@ActiveStatusId INT  
    SELECT              
    @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)'),   
    @BU_ID = C.value('(BUID)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)       
	
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1)
		BEGIN
		IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
			BEGIN
					SELECT 1 as IsCustomerHaveReadings
					FOR XML PATH('BillingBE'),TYPE
			END		
		ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
			BEGIN
					SELECT
					(
						SELECT 
						 dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
						 ,CD.ClassName As Tariff
						 ,CD.OutStandingAmount
						 ,CD.GlobalAccountNumber
						 ,CD.MeterNumber AS MeterNo
						 ,CD.OldAccountNo
						 ,CD.InitialReading as PreviousReading
						 ,CD.PresentReading
						 ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
						  from UDV_CustomerDescription CD
						  --left Join Tbl_CustomerReadings CR ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
						  where CD.GlobalAccountNumber = @GlobalAccountNumber
						 FOR XML PATH('BillingBE'),TYPE    
					)
					,
					(
						SELECT (datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate)) as ReadingMonth
								,ReadDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0) 
								,SUM(Usage) as Usage
								,MeterNumber as MeterNo
								,AverageReading
								,PreviousReading 
								,PresentReading 
								,R.ReadCode
								,(CASE when BilledDate is null then '-' else  CONVERT(varchar(20),BilledDate,106) end) AS BilledDate
						FROM    Tbl_CustomerReadings CR
						JOIN Tbl_MReadCodes R ON R.ReadCodeId = CR.ReadType
						WHERE   GlobalAccountNumber =@GlobalAccountNumber
						 --and ReadDate > dateadd(m, -6, CONVERT(DATETIME,'13/07/2015',103) - datepart(d, CONVERT(DATETIME,'13/05/2015',103)) + 1)
						 and ReadDate > dateadd(m, -6, GETDATE() - datepart(d, GETDATE()) + 1)
						GROUP BY DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
								, MeterNumber
								,(datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate))        
								,R.ReadCode
								,BilledDate
								,AverageReading
								,PreviousReading
								,PresentReading
								FOR XML PATH('BillingListBE'),TYPE    
					)	
					FOR XML PATH(''),ROOT('CustomerReadingListBeByXml')       		 	
			END
			ELSE		
				BEGIN
						SELECT 1 as IsDirectCustomer
						--FOR XML PATH(''),ROOT('BillingListBE')       	
						FOR XML PATH('BillingBE'),TYPE
				END
		END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBooksOnlyNoPwr]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author  : Suresh Kumar Dasi  
-- Create date  : 17 OCT 2014  
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles  
-- Modified By : Padmini  
-- Modified Date : 25-12-2014  
-- Modified By : NEERAJ KANOJIYA  
-- Modified Date : 13-JULY-15
-- Description  : NEW COPY OF SAME PROC. BUT IT WILL CHECK ONLY FOR NO POWER CONDITION.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDisableBooksOnlyNoPwr]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @CycleId VARCHAR(MAX)  
   ,@BillMonth INT  
   ,@BillYear INT  
     
 SELECT       
  @CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@BillMonth = C.value('(BillingMonth)[1]','INT')  
  ,@BillYear = C.value('(BillingYear)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
 SELECT  
  (  
  SELECT   
    BN.BookNo+'('+ BookCode+')' AS BookNo -- Modified By Padmini
   ,COUNT(0) AS TotalRecords  
  FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD  
  JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID  
  JOIN Tbl_BookNumbers BN ON CD.BookNo=BN.BookNo
  --WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))  
  WHERE CycleId IN (SELECT [com] FROM dbo.fn_Split(@CycleId,',')) -- Modified By Padmini
  AND CD.BookNo IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive = 1 AND DisableTypeId=1)  
  AND CD.ActiveStatusId = 1  
  GROUP BY BN.BookNo,BookCode  
  FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')      
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
		BEGIN
			SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
		BEGIN
			SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=1)
		BEGIN
			SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE
		BEGIN		
		
			SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@GlobalAccountNumber,@Usage)
			SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
			SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					--,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					--,IsRollOver
					)
				VALUES(
					 @GlobalAccountNumber
					,GETDATE()
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
					,@Multipiler
					,2
					,@CreatedBy
					,GETDATE()
					--,@IsTamper
					,@MeterNumber
					,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
					--,@IsRollover
					)
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@PreviousReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE 	
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettingsData_BySC]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek/Satya
-- Create date: 24-04-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettingsData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1,2'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	IF(@BillingRule = 1) -- As per settings
		BEGIN
			SELECT
			(			
				SELECT 
					 ClassID
					,TC.ClassName
					,CC.CategoryName
					,CC.ClusterCategoryId 
					,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
					,Cycles.CycleId 
					,Cycles.CycleName
					,MAX(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case 
						when isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ((ISNULL(CSD.ActiveStatusId,1)=2) OR (ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.DisableTypeId,0) = 2 AND ISNULL(BDB.IsPartialBill,0) = 0))
							then 1 
							else 0
							end
						else 0
						end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,ISNULL(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,isnull(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,0)  
														Else 0 end)
											else  0 end ) 
								else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then 0
															Else (case when  CR.Usage IS NULL  
																		then isnull(CAD.AvgReading,0) 
																		else 0 end) end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when DCAVG.AverageReading IS NULL  
																		then isnull(CAD.AvgReading,CAD.InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
				INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID 
				INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
				INNER JOIN CUSTOMERS.Tbl_CustomersDetail (NOLOCK) CSD ON CSD.GlobalAccountNumber=CPD.GlobalAccountNumber and CSD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
				INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
				INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
				INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
				LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
				LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
				LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
				LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
						AND ES.ClusterCategoryId = CC.ClusterCategoryId 
						AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
						And ES.BillingMonth= @Month	   
						AND ES.BillingYear=	@year					 
				GROUP BY TC.ClassID,TC.ClassName,CC.CategoryName
					,CC.ClusterCategoryId,Cycles.CycleCode,Cycles.CycleName
					,Cycles.CycleId 
				ORDER BY Cycles.CycleId,CC.ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		END
	ELSE
		BEGIN
			SELECT
			(
				SELECT 
					 ClassID
					,TC.ClassName
					,CC.CategoryName
					,CC.ClusterCategoryId 
					,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
					,Cycles.CycleId 
					,Cycles.CycleName
					,MAX(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case 
						when isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ((ISNULL(CSD.ActiveStatusId,1)=2) OR (ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.DisableTypeId,0) = 2 AND ISNULL(BDB.IsPartialBill,0) = 0))
							then 1 
							else 0
							end
						else 0
						end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else 1
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then 1 
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0
							then 0 
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--case
							--when ISNULL(BDB.IsPartialBill,1)  =1 
							--then case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
							--else 0
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,ISNULL(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(CR.Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,isnull(CAD.AvgReading,CAD.InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
								then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(DCAVG.AverageReading,0)  
														Else 0 end)
											else  0 end ) 
								else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else case when isnull(DCAVG.GlobalAccountNumber,0)=0 then 0 else 1 end 
								--end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else 1 
								--end
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(CSD.ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
						then
							case
							when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--case 
								--when  isnull(BDB.DisableTypeId,2) =1 
								--then 0  
								--else  case when isnull(CR.GlobalAccountNumber,0)=0 then 0 else 1 end
								--end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then 0
															Else (case when  CR.Usage IS NULL  
																		then isnull(CAD.AvgReading,0) 
																		else 0 end) end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(CSD.ActiveStatusId,1)=1  
									then (case when ISNULL(BDB.IsActive,0) = 1 AND ISNULL(BDB.IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when DCAVG.AverageReading IS NULL  
																		then isnull(CAD.AvgReading,CAD.InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
				INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID 
				INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
				INNER JOIN CUSTOMERS.Tbl_CustomersDetail (NOLOCK) CSD ON CSD.GlobalAccountNumber=CPD.GlobalAccountNumber and CSD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
				INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
				INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
				INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
				LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
				LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
				LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
				LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
						AND ES.ClusterCategoryId = CC.ClusterCategoryId 
						AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
						And ES.BillingMonth= @Month	   
						AND ES.BillingYear=	@year					 
				GROUP BY TC.ClassID,TC.ClassName,CC.CategoryName
					,CC.ClusterCategoryId,Cycles.CycleCode,Cycles.CycleName
					,Cycles.CycleId 
				ORDER BY Cycles.CycleId,CC.ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		END
	
	DROP TABLE #ReadCustomersList
	
END



--SELECT ClassID
--	,
--	SUM(
--			case 
--			when isnull(ReadCodeID,0)=2 and isnull(CPD.CustomerTypeId,0)<>3
--			then

--			case when ISNULL(BDB.IsPartialBill,1)  =1 then 1 else
--																case 
--																when  
--																isnull(BDB.DisableTypeId,2) =1 
--																then 0  
--																else 1 
--																end
--			end
--			else 
--			0
--			end
--			)
	
--	as TotalReadCustomers
--	,
--	SUM ( case 
--	when 
--	(CPD.ActiveStatusId=1 or CPD.ActiveStatusId =2) and
--	(isnull(BDB.IsPartialBill,case when ISNULL(BDB.DisableTypeId,0) =1 
--	then -1 else 1 end) =1  OR isnull(BDB.DisableTypeId,2) =2)
--	then (case when isnull(ReadCodeID,0)=1 then 1 else 0 end) else 0 end )  
--	 as EstimatedCustomers
--	,TC.ClassName
--	,CC.CategoryName
--	,CC.ClusterCategoryId 
--	,COUNT(DISTINCT CR.GlobalAccountNumber) as TotalReadingsCustomers
--	,CAST(SUM(isnull(CR.Usage,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) end)) AS INT) as TotalMetersUsage
--	,CAST(SUM(isnull(DCAVG.AverageReading,case when @BillingRule=1 then 0 else isnull(CAD.AvgReading,0) END )) AS INT) as TotalUsageForDirectCustomers
--	,CAST(SUM(case when isnull(ReadCodeID,0) = 1 then 1 else 0 end) - COUNT(DISTINCT DCAVG.GlobalAccountNumber) AS INT) as NonUploadedCustomersTotal
--	,CAST(SUM(case when isnull(ReadCodeID,0) = 2 then 1 else 0 end) - COUNT(DISTINCT CR.GlobalAccountNumber) AS INT) As TotalNonReadCustomers 
--	,max(isnull(ES.EnergytoCalculate,0)) As EnergyToCalculate
--	,CAST(SUM(Case when CR.Usage IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonReadCustomersUsage 
--	,CAST(SUM(Case when DCAVG.AverageReading IS NULL THen CAD.AvgReading else 0 end) AS INT) as TotalNonUploadedCustomersUsage
--	,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
--	,Cycles.CycleId 
--	,Cycles.CycleName
--FROM Tbl_MTariffClasses(NOLOCK)	TC
--INNER JOIN Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CPD.TariffClassID = TC.ClassID AND CPD.ActiveStatusId <>4	 --Need To Check once all completed
--INNER JOIN	 CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
--INNER JOIN   MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
--INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo = CPD.BookNo	  
--INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId
--LEFT JOIN #ReadCustomersList(NOLOCK) CR ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber 
--LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber = DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId = 1
--LEFT JOIN Tbl_BillingDisabledBooks BDB
--ON  BDB.BookNo=BN.BookNo and IsActive=1
--LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES ON ES.CycleId = Cycles.CycleId and ES.ClusterCategoryId = CC.ClusterCategoryId 
--		and ES.TariffId = CPD.TariffClassID and ES.ActiveStatusId = 1 
--		And ES.BillingMonth = @Month -- Need To Modify	 
--		AND ES.BillingYear = @Year	 -- Need To Modify
--Group By TC.ClassID,TC.ClassName,CC.CategoryName,CC.ClusterCategoryId ,Cycles.CycleId,Cycles.CycleName,Cycles.CycleCode
--ORDER BY Cycles.CycleId,CC.ClusterCategoryId

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_NotDisabled]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Karteek 
-- Create date: 14-07-2015
-- Description: The purpose of this procedure is to get Books list By Cycle      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_NotDisabled]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
      
 DECLARE @CycleId VARCHAR(MAX)       
   
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)      
   
 IF(@CycleId != '')      
  BEGIN      
   SELECT      
   (     
    select  B.BookNo      
      ,(CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN B.BookCode   
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
      END) AS BookNoWithDetails  
      ,COUNT(B.BookNo) over() as BookNo
       from Tbl_BookNumbers   B
    where ActiveStatusId=1 
    and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
 ELSE      
  BEGIN      
   SELECT      
   (      
    SELECT (CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN BookCode   
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')   
      END) AS BookNo        
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1      
    ORDER BY BookNo ASC      
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
       
END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomersExportByBookWise]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 31-10-2014
-- Modified date: 02-12-2014
-- Description:	The purpose of this procedure is to get Customers export by Book wise
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomersExportByBookWise]
(
@XmlDoc xml
)
AS
BEGIN

	DECLARE @BUID VARCHAR(50)=''
		,@SUID VARCHAR(MAX)=''
		,@SCID VARCHAR(MAX)=''
		,@CycleId VARCHAR(MAX)=''
		,@BookNos VARCHAR(MAX)=''
		
		SELECT
			@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SCID=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
			,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCheckMetersBe') as T(C)
		
		IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	select 
		 CustomerList.AccountNo
		 , CustomerList.GlobalAccountNumber
		 ,Name
		 ,OldAccountNo
		 ,MeterNumber
		 ,BusinessUnitName
		 ,ServiceUnitName
		 ,ServiceCenterName
		 ,ID AS BookCode
		 ,' ' CurrentReading
		 ,' ' AS Remarks
		 ,' ' AS PreviousReading
		 ,ServiceAddress as [Address]
		 ,BookSortOrder
		 ,SortOrder as CustomerSortOrder
		 ,InitialReading
		 ,CycleName
		 ,ClassName
		 ,CustomerList.BookNo
	 INTO  #TotalCustomersList
	 FROM UDV_SearchCustomer   CustomerList
	 WHERE ReadCodeId=2 AND CustomerList.ActiveStatusId=1
	 AND (CustomerList.BU_ID=@BUID OR @BUID='') 
	 AND CustomerList.SU_ID IN(SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,','))
	 AND CustomerList.ServiceCenterId IN(SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,','))
	 AND CustomerList.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
	 AND CustomerList.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,','))


SELECT   max(CustomerReadingId) as CurrentReadingID , CL.GlobalAccountNumber 
INTO #ReadingsList from Tbl_CustomerReadings	 CR
INNER JOIN #TotalCustomersList CL
ON CL.GlobalAccountNumber=CR.GlobalAccountNumber
GROUP BY CL.GlobalAccountNumber	  

SELECT RL.GlobalAccountNumber,CR.PresentReading as PreviousReading
INTO #ReadingsListWIthPreviousReading
FROM #ReadingsList  RL
INNER JOIN	 Tbl_CustomerReadings CR
ON		CR.CustomerReadingId=RL.CurrentReadingID

SELECT (TCL.AccountNo+' - '+TCL.GlobalAccountNumber) AS GlobalAccountNumber
	 ,Name
	 ,OldAccountNo
	 ,MeterNumber AS MeterNo
	 ,BusinessUnitName
	 ,ServiceUnitName
	 ,ServiceCenterName
	 ,BookCode AS BookNumber
	 ,' ' CurrentReading
	 ,' ' AS Remarks
	 ,TCL.[Address]
	 ,BookSortOrder
	 ,CustomerSortOrder
	 ,ClassName
	 ,CycleName
	 ,BookNo
	 ,ISNULL(RL.PreviousReading,TCL.InitialReading) as PreviousReading 
	 From   #TotalCustomersList	TCL
	LEFT JOIN  #ReadingsListWIthPreviousReading	RL
	ON	 TCL.GlobalAccountNumber=RL.GlobalAccountNumber
	WHERE BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND ISNULL(IsPartialBill,0)=0)


	DROP TABLE	 #TotalCustomersList
	DROP TABLE	 #ReadingsListWIthPreviousReading
	DROP TABLE	 #ReadingsList
		
		--SELECT CD.AccountNo
		--	  ,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name
		--	  ,ISNULL(CD.OldAccountNo,'') AS OldAccountNo
		--	  ,cd.MeterNumber AS MeterNo
		--	  ,CD.BusinessUnitName
		--	  ,CD.ServiceUnitName
		--	  ,CD.ServiceCenterName
		--	  ,CD.CycleName
		--	  ,BN.BookCode
		--	  ,'' AS CurrentReading
		--	  ,'' AS Remarks
		--	  ,(SELECT TOP(1) PreviousReading FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=CD.GlobalAccountNumber
		--			ORDER BY CustomerReadingId DESC) AS PreviousReading
		--	  ,dbo.fn_GetCustomerAddress(CD.GlobalAccountNumber) AS [Address]
		--	  ,BN.SortOrder AS BookSortOrder
		--	  ,CD.SortOrder AS CustomerSortOrder
		-- FROM [UDV_CustomerDescription] CD
		-- --JOIN Tbl_Cycles AS C ON CD.CycleId=C.CycleId
		-- JOIN Tbl_BookNumbers BN ON CD.BookNo = BN.BookNo
		-- --JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID
		-- --JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID
		-- --JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CD.ServiceCenterId
		-- --JOIN Tbl_Cycles BC ON BC.CycleId = C.CycleId
		-- WHERE ReadCodeId=2 AND CD.ActiveStatusId=1
		-- AND (CD.BU_ID=@BUID OR @BUID='')
		-- AND (CD.SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		-- AND (CD.ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		-- AND (CD.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		-- --AND (CD.BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')
		-- AND (CD.BookNo IN
		--	(SELECT BookNo FROM Tbl_BookNumbers WHERE ActiveStatusId=1
		--	 AND (BU_ID=@BUID OR @BUID='')
		--	 AND (SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,',')) OR @SUID='')
		--	 AND (ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,',')) OR @SCID='')
		--	 AND (CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,',')) OR @CycleId='')
		--	 AND (BookNo IN(SELECT com FROM dbo.fn_Split(@BookNos,',')) OR @BookNos='')) OR @BookNos='')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_BookwiseExport]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Karteek  
-- Create date: 14-07-2015    
-- Description: The purpose of this procedure is to get Books list By Cycle    
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_BookwiseExport]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
    
	DECLARE @CycleId VARCHAR(MAX)     
	
	SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')    
	FROM @XmlDoc.nodes('ReportsBe') AS T(C)    
	
	IF(@CycleId != '')    
		BEGIN    
			SELECT    
			(    
				SELECT B.BookNo    
						,(CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN B.BookCode 
							ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')') 
						END) AS BookNoWithDetails
				FROM Tbl_BookNumbers B 
				WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
				AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND ISNULL(IsPartialBill,0) = 0)
				AND B.ActiveStatusId=1    
				GROUP BY 
				B.BookNo,ID,B.BookCode				
				ORDER BY B.BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
	ELSE    
		BEGIN    
			SELECT    
			(    
				SELECT (CASE 
							WHEN ISNULL(ID,'') = '' 
								THEN BookCode 
							ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')') 
						END) AS BookNo      
				FROM Tbl_BookNumbers WHERE ActiveStatusId=1    
				ORDER BY BookNo ASC    
				FOR XML PATH('Reports'),TYPE    
			)    
			FOR XML PATH(''),ROOT('ReportsBeInfoByXml')    
		END    
     
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForAssignMeter_ByCheck]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  09/MAY/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- MODIFIED BY	:  NEERAJ KANOJIYA        
-- Create date	:  14/JULY/2015        
-- Description	:  FETCH DETAILS ONLY FOR ACTIVE CUSTOMERS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustDetailsForAssignMeter_ByCheck]  
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId = ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT       
		   A.GlobalAccountNumber AS CustomerID        
		  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                         
		  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name                     
		  ,A.GlobalAccountNumber AS AccountNo  
		  ,(A.AccountNo+' - '+A.GlobalAccountNumber) AS GlobalAccountNumber
		  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber          
		  ,A.OldAccountNo                       
		  ,RT.RouteName AS RouteName       
		  ,A.RouteSequenceNo AS RouteSequenceNumber                                  
		  ,A.ReadCodeID AS ReadCodeID                        
		  ,A.TariffId AS ClassID                             
		  ,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount      
		  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials       
		  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress       
		  --,A.ServiceAddress  AS FullServiceAddress -- Modified By -Padmini (17th Mar 2015)      
		  ,A.CustomerTypeId AS CustomerTypeId      
		  ,1 AS IsSuccessful                        
			FROM [UDV_CustomerDescription] AS A          
			LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID        
			WHERE GlobalAccountNumber = @GlobalAccountNumber      
			--AND A.ActiveStatusId=1                                
		 FOR XML PATH('CustomerRegistrationBE'),TYPE 
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]    Script Date: 07/14/2015 16:58:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  13/JULY/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId = ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT    
       CD.GlobalAccountNumber AS GlobalAccountNumber    
      ,CD.AccountNo As AccountNo    
      ,CD.FirstName    
      ,CD.MiddleName    
      ,CD.LastName    
      ,CD.Title          
      ,CD.KnownAs    
      ,CD.CustomerTypeId  
      ,CT.CustomerType  
      ,ISNULL(MeterNumber,'--') AS MeterNo    
      ,CD.ClassName AS Tariff    
      ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo    
      FROM UDV_CustomerDescription  CD    
      join Tbl_MCustomerTypes CT  
      on CD.CustomerTypeId=CT.CustomerTypeId  
      WHERE CD.GlobalAccountNumber=@GlobalAccountNumber      
      FOR XML PATH('ChangeCustomerTypeBE')   
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 

GO


