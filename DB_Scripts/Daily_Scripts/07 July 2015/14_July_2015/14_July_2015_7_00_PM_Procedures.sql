
GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsPoleInfo]    Script Date: 07/14/2015 19:31:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- CREATED By : NEERAJ KANOJIYA  
-- Date		  : 14-JULY-15
-- Description  : CUSTOMER EXISTENCE IN POLE INFO
-- =============================================  
CREATE PROCEDURE [dbo].[USP_IsCustomerExistsPoleInfo]   
AS  
BEGIN  
 DECLARE @IsExists bit =0  
		,@Count BIGINT
SET @Count=(SELECT COUNT(0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD
					JOIN Tbl_PoleDescriptionTable AS PD ON CPD.PoleID=PD.PoleId)	  
IF(@Count > 0)
 SET @IsExists =1
 SELECT @IsExists AS IsExists
 FOR XML PATH('PoleBE'),TYPE
END  
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 07/14/2015 19:32:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
			,@Count INT
			,@TotalReadingEnergies DECIMAL(18,2)
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	IF((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)=0)
		BEGIN
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@ModifiedReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE
		END	
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
		BEGIN
			SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
		BEGIN
			SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
		BEGIN
			SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
		END	
	ELSE
		BEGIN		
		
			SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)			
			
			SET @Count=(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			IF (@Count=1)
				BEGIN
					SET @TotalReadingEnergies=@Usage
				END
			ELSE
				BEGIN
					
					DECLARE @Last1stTotalReadingEnergies DECIMAL(18,2)
							,@Last2ndTotalReadingEnergies DECIMAL(18,2)
					
					SELECT @Last2ndTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					WHERE TotalReadings =(SELECT MAX(TotalReadings)-1
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber=@GlobalAccountNumber
					
					SELECT @Last1stTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					WHERE TotalReadings =(SELECT MAX(TotalReadings)
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber=@GlobalAccountNumber

					SET @TotalReadingEnergies=@Last2ndTotalReadingEnergies-@Last1stTotalReadingEnergies+@Usage
				END
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@GlobalAccountNumber,@Usage)
			SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
			SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																			WHERE GlobalAccountNumber=@GlobalAccountNumber)

			
			
			INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					--,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					--,IsRollOver
					)
				VALUES(
					 @GlobalAccountNumber
					,GETDATE()
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,@AverageReading
					,@Usage
					--,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
					,@TotalReadingEnergies
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
					,@Multipiler
					,2
					,@CreatedBy
					,GETDATE()
					--,@IsTamper
					,@MeterNumber
					,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
					--,@IsRollover
					)
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@PreviousReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE 	
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]    Script Date: 07/14/2015 19:32:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 14-07-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1,2'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT DISTINCT
		 ClassID
		,TC.ClassName
		,CC.CategoryName
		,CC.ClusterCategoryId 
		,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
		,Cycles.CycleId 
		,Cycles.CycleName
		,CPD.CustomerTypeId
		,CSD.ActiveStatusId
		,BDB.IsActive DisabledActiveStatus
		,BDB.DisableTypeId
		,BDB.IsPartialBill
		,ReadCodeID	  
		,CPD.GlobalAccountNumber CustomerGlobalAccNo
		,CR.GlobalAccountNumber ReadingGlobalAccNo
		,DCAVG.GlobalAccountNumber AvgGlobalAccNo
		,CR.Usage
		,CAD.AvgReading CustomerAvgReading
		,CAD.InitialBillingKWh  
		,DCAVG.AverageReading UploadedAvgReading
		,ES.EnergytoCalculate
	INTO #ReaultData
	FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
	INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID 
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail (NOLOCK) CSD ON CSD.GlobalAccountNumber=CPD.GlobalAccountNumber and CSD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
	INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
	INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
	INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
	LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
	LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
	LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
			AND ES.ClusterCategoryId = CC.ClusterCategoryId 
			AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
			And ES.BillingMonth= @Month	   
			AND ES.BillingYear=	@year
	
	
	
			SELECT
			(			
				SELECT 
					 ClassID
					,ClassName
					,CategoryName
					,ClusterCategoryId 
					,Cycle
					,CycleId 
					,CycleName
					,MAX(isnull(EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case 
						when isnull(CustomerTypeId,0)<>3
						then
							case
							when ((ISNULL(ActiveStatusId,1)=2) OR (ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(DisableTypeId,0) = 2 AND ISNULL(IsPartialBill,0) = 0))
							then 1 
							else 0
							end
						else 0
						end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,ISNULL(CustomerAvgReading,InitialBillingKWh))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(UploadedAvgReading,isnull(CustomerAvgReading,InitialBillingKWh))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  ISNULL(UploadedAvgReading,0)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then (case when  Usage IS NULL  
																		then ISNULL(CustomerAvgReading,InitialBillingKWh)
																		else 0 end)
															Else 0
															 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when UploadedAvgReading IS NULL  
																		then isnull(CustomerAvgReading,InitialBillingKWh) 
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM #ReaultData				 
				GROUP BY ClassID,ClassName,CategoryName
					,ClusterCategoryId,CycleName,Cycle
					,CycleId 
				ORDER BY CycleId,ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		
	DROP TABLE #ReadCustomersList
	DROP TABLE #ReaultData
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadings]    Script Date: 07/14/2015 19:32:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 13-Jul-2015
-- Description:	The purpose of this procedure is to get the last 6 months Reading of the read customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerReadings]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @GlobalAccountNumber VARCHAR(50)  
			,@IsSuccess BIT	
			,@IsCustExistsInOtherBU BIT
			,@BU_ID VARCHAR(50)
			,@Flag INT
			,@FlagDetails INT
			,@ActiveStatusId INT  
    SELECT              
    @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)'),   
    @BU_ID = C.value('(BUID)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)       
	
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1)
		BEGIN
		IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
			BEGIN
					SELECT 1 as IsCustomerHaveReadings
					FOR XML PATH('BillingBE'),TYPE
			END		
		ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
			BEGIN			
					SELECT
					(
						SELECT 
						 dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
						 ,CD.ClassName As Tariff
						 ,CD.OutStandingAmount
						 ,CD.GlobalAccountNumber
						 ,CD.MeterNumber AS MeterNo
						 ,CD.OldAccountNo
						 ,CD.InitialReading as PreviousReading
						 ,CD.PresentReading
						 ,(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber) AS TotalReadings
						 ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
						  from UDV_CustomerDescription CD
						  --left Join Tbl_CustomerReadings CR ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
						  where CD.GlobalAccountNumber = @GlobalAccountNumber
						 FOR XML PATH('BillingBE'),TYPE    
					)
					,
					(
						SELECT (datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate)) as ReadingMonth
								,ReadDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0) 
								,SUM(Usage) as Usage
								,MeterNumber as MeterNo
								,AverageReading
								,PreviousReading 
								,PresentReading 
								,R.ReadCode
								,(CASE when BilledDate is null then '-' else  CONVERT(varchar(20),BilledDate,106) end) AS BilledDate
						FROM    Tbl_CustomerReadings CR
						JOIN Tbl_MReadCodes R ON R.ReadCodeId = CR.ReadType
						WHERE   GlobalAccountNumber =@GlobalAccountNumber
						 --and ReadDate > dateadd(m, -6, CONVERT(DATETIME,'13/07/2015',103) - datepart(d, CONVERT(DATETIME,'13/05/2015',103)) + 1)
						 and ReadDate > dateadd(m, -6, GETDATE() - datepart(d, GETDATE()) + 1)
						 --and ReadDate > dateadd(m, -6, ReadDate - datepart(d, ReadDate) + 1)
						GROUP BY DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
								, MeterNumber
								,(datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate))        
								,R.ReadCode
								,BilledDate
								,AverageReading
								,PreviousReading
								,PresentReading
								FOR XML PATH('BillingListBE'),TYPE    
					)	
					FOR XML PATH(''),ROOT('CustomerReadingListBeByXml')       		 	
			END
			ELSE		
				BEGIN
						SELECT 1 as IsDirectCustomer
						--FOR XML PATH(''),ROOT('BillingListBE')       	
						FOR XML PATH('BillingBE'),TYPE
				END
		END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsPoleInfo]    Script Date: 07/14/2015 19:32:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- CREATED By : NEERAJ KANOJIYA  
-- Date		  : 14-JULY-15
-- Description  : CUSTOMER EXISTENCE IN POLE INFO
-- =============================================  
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsPoleInfo]   
AS  
BEGIN  
 DECLARE @IsExists bit =0  
		,@Count BIGINT
SET @Count=(SELECT COUNT(0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD
					JOIN Tbl_PoleDescriptionTable AS PD ON CPD.PoleID=PD.PoleId)	  
IF(@Count > 0)
 SET @IsExists =1
 SELECT @IsExists AS IsExists
 FOR XML PATH('PoleBE'),TYPE
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBooksOnlyNoPwr]    Script Date: 07/14/2015 19:32:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author  : Suresh Kumar Dasi  
-- Create date  : 17 OCT 2014  
-- Description  : To Get the Bill Disabled Book having customers Under the Cycles  
-- Modified By : Padmini  
-- Modified Date : 25-12-2014  
-- Modified By : NEERAJ KANOJIYA  
-- Modified Date : 13-JULY-15
-- Description  : NEW COPY OF SAME PROC. BUT IT WILL CHECK ONLY FOR NO POWER CONDITION.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDisableBooksOnlyNoPwr]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @CycleId VARCHAR(MAX)  
   ,@BillMonth INT  
   ,@BillYear INT  
     
 SELECT       
  @CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
  ,@BillMonth = C.value('(BillingMonth)[1]','INT')  
  ,@BillYear = C.value('(BillingYear)[1]','INT')  
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   
 SELECT  
  (  
  SELECT   
    BN.BookNo+'('+ BookCode+')' AS BookNo -- Modified By Padmini
   ,COUNT(0) AS TotalRecords  
  FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD  
  JOIN Tbl_MTariffClasses T ON CD.TariffClassID = T.ClassID  
  JOIN Tbl_BookNumbers BN ON CD.BookNo=BN.BookNo
  --WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))  
  WHERE CycleId IN (SELECT [com] FROM dbo.fn_Split(@CycleId,',')) -- Modified By Padmini
  AND CD.BookNo IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive = 1 AND DisableTypeId=1)  
  AND CD.ActiveStatusId = 1  
  GROUP BY BN.BookNo,BookCode  
  FOR XML PATH('BillGenerationList'),TYPE  
  )  
 FOR XML PATH(''),ROOT('BillGenerationInfoByXml')      
END  
GO



GO
/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 07/14/2015 20:00:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
			,@Count INT
			,@TotalReadingEnergies DECIMAL(18,2)
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	IF((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)=0)
		BEGIN
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@ModifiedReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE
		END	
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
		BEGIN
			SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
		BEGIN
			SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
		BEGIN
			SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
		END	
	ELSE
		BEGIN		
		
			SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)			
			
			SET @Count=(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			IF (@Count=1)
				BEGIN
					SET @TotalReadingEnergies=@Usage
				END
			ELSE
				BEGIN
					
					DECLARE @Last2ndTotalReadingEnergies DECIMAL(18,2)
					
					SELECT @Last2ndTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					WHERE TotalReadings =(SELECT MAX(TotalReadings)-1
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber=@GlobalAccountNumber
					
					--SELECT @Last1stTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					--WHERE TotalReadings =(SELECT MAX(TotalReadings)
					--								FROM Tbl_CustomerReadings
					--								WHERE GlobalAccountNumber=@GlobalAccountNumber)
					--AND GlobalAccountNumber=@GlobalAccountNumber

					SET @TotalReadingEnergies= @Last2ndTotalReadingEnergies+@Usage
				END
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@GlobalAccountNumber,@Usage)
			SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
			SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																			WHERE GlobalAccountNumber=@GlobalAccountNumber)

			
			
			INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					--,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					--,IsRollOver
					)
				VALUES(
					 @GlobalAccountNumber
					,GETDATE()
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,@AverageReading
					,0
					--,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
					,@TotalReadingEnergies
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
					,@Multipiler
					,2
					,@CreatedBy
					,GETDATE()
					--,@IsTamper
					,@MeterNumber
					,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
					--,@IsRollover
					)
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@PreviousReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE 	
		END	
END


GO
/****** Object:  StoredProcedure [dbo].[USP_GetDisableBooksOnlyNoPwr]    Script Date: 07/14/2015 20:11:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================   
-- Modified By : NEERAJ KANOJIYA  
-- Modified Date : 13-JULY-15
-- Description  : CHECK ONLY FOR NO POWER CONDITION.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDisableBooksOnlyNoPwr]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @GlobalAccountNumber VARCHAR(MAX)  --='0000000125'
   ,@IsExists BIT  
     
 SELECT       
  @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(MAX)') 
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   

  IF EXISTS(SELECT CPD.BookNo FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD 
	JOIN Tbl_BillingDisabledBooks AS BDB ON CPD.BookNo=BDB.BookNo 
											AND BDB.IsActive=1 AND BDB.DisableTypeId=1 
											AND CPD.GlobalAccountNumber=@GlobalAccountNumber)
												
	SET @IsExists=1
  ELSE 
   SET @IsExists=0
   
   SELECT @IsExists AS IsExists
   FOR XML PATH('BillGenerationBe'),TYPE  
  
END  