
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint_Customerwise]    Script Date: 07/15/2015 19:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint_Customerwise]
(  
 @GlobalAcountNumber VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (
  
   
  SELECT         
	CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  ISNULL(CB.VAT,0)  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]             
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
	 ,convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + (case  ReadCodeId when 1 then ' D' when 2 then ' R' when 3 then ' A' else ' M' end  )  AS Usage              
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate    
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	,7 as [LastDueDays]
	,CONVERT(VARCHAR(11),BillGeneratedDate+7,106) as LastDueDate
	,(SELECT dbo.fn_GetCustomer_ADC(CB.Usage)) AS ADC
	,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID  
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
     where AccountNo=@GlobalAcountNumber and BillMonth=@BillMonth and BillYear=@BillYear
  );
  
  
  
  
  
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 07/15/2015 19:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Satya
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
	Declare @NewAverage decimal(18,2) = 0,@TotalRecords INT = 0
		, @TotalUsage DECIMAL(18,2) = 0
		, @NoOfMonths INT = 12
		, @TotalEnergy DECIMAL(18,2) = 0

   SELECT Top 1
          @TotalRecords = TotalReadings
         ,@TotalEnergy = TotalReadingEnergies
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
   
    IF(ISNULL(@TotalRecords,0) >= @NoOfMonths)
		BEGIN
			DECLARE @AdjustTotalReading DECIMAL(18,2) = 0
			
			SELECT 
				@AdjustTotalReading = TotalReadingEnergies
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
			AND TotalReadings = (@TotalRecords - @NoOfMonths + 1)
			--AND IsReadingWrong = 0
			
			SET @TotalEnergy = @TotalEnergy - @AdjustTotalReading
			
			IF(ISNULL(@TotalEnergy,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalEnergy,0) + @usage) / @NoOfMonths
			ELSE
				SET @NewAverage = @usage
			
			--SELECT 
			--	@TotalUsage = SUM(ISNULL(Usage,0))
			--FROM Tbl_CustomerReadings
			--WHERE GlobalAccountNumber = @AccountNo
			--AND TotalReadings > (@TotalRecords - @NoOfMonths + 1)
			--AND IsReadingWrong = 0
			
			--IF(ISNULL(@TotalUsage,0) > 0) 
			--	SET @NewAverage = (ISNULL(@TotalUsage,0) + @usage) / @NoOfMonths
			--ELSE
			--	SET @NewAverage = @usage
			
		END
    ELSE
		BEGIN
			--SELECT 
			--	@TotalUsage = SUM(ISNULL(Usage,0))
			--FROM Tbl_CustomerReadings
			--WHERE GlobalAccountNumber = @AccountNo
			--AND IsReadingWrong = 0
						
			IF(ISNULL(@TotalEnergy,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalEnergy,0) + @usage) / (ISNULL(@TotalRecords,0) + 1)
			ELSE
				SET @NewAverage = @usage
			
		END
 
	RETURN @NewAverage

END
