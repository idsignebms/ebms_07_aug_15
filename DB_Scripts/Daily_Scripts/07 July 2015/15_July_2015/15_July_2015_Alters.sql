GO
ALTER TABLE Tbl_CustomerReadings
ADD IsReadingWrong BIT DEFAULT 0
GO
UPDATE Tbl_CustomerReadings
SET IsReadingWrong = 0
GO
