CREATE TABLE Tbl_PresentReadingAdjustmentLog
(
	 GlobalAccountNumber VARCHAR(50)
	,Name VARCHAR(MAX)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,NewPresentReading VARCHAR(50)
	,PreviousReadDate VARCHAR(50)
	,PresentReadDate VARCHAR(50)
	,MeterNumber VARCHAR(50)
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)

