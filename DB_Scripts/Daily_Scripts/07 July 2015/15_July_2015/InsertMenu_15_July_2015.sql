--SELECT MAX(MenuId) FROM Tbl_Menus WHERE ReferenceMenuId=6
--ORDER BY Page_Order
GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Customer Present Reading Adjustment','../ConsumerManagement/ChangePresentReading.aspx',6,25,1,1,209)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,209,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,209,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO