
GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 07/16/2015 17:46:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
		
	
	DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
	SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), 
		@CAPMIAmount = ISNULL(CAPMIAmount,0),
		@MeterTypeId = MeterType
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNo AND ActiveStatusId = 1	
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID AND ActiveStatusId = 1)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@MeterTypeId != 2)
		BEGIN  
			SELECT 1 AS IsNotCreditMeter FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(2,3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @AccountNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsReadToDirectApproval FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((CONVERT(DECIMAL(18,2),@OldMeterReading) < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF ((SELECT ISNULL(OutStandingAmount,0) FROM Tbl_PaidMeterDetails WHERE AccountNo=@AccountNo AND MeterNo=@OldMeterNo AND ActiveStatusId = 1) > 0)
		BEGIN
			SELECT 1 AS IsHavingCapmiAmt FOR XML PATH('ChangeBookNoBe')  
		END	
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,MI.MeterType
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			--INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage >= 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END,
				IsCAPMI = @IsCAPMIMeter,
				MeterAmount = @CAPMIAmount
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
			IF(@IsCAPMIMeter = 1)  
				BEGIN  
					INSERT INTO Tbl_PaidMeterDetails  
					(  
					 AccountNo  
					,MeterNo  
					,MeterTypeId  
					,MeterCost  
					,OutStandingAmount
					,ActiveStatusId  
					,MeterAssignedDate  
					,CreatedDate  
					,CreatedBy  
					)  
					values  
					(  
					 @AccountNo  
					,@MeterNo  
					,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNo)  
					,@CAPMIAmount  
					,@CAPMIAmount  
					,1  
					,@OldMeterReadingDate  
					,dbo.fn_GetCurrentDateTime()  
					,@ModifiedBy  
					)       
				END  
			
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END



GO

/****** Object:  StoredProcedure [MASTERS].[USP_AssignMeter]    Script Date: 07/16/2015 17:46:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  NEERAJ KANOJIYA  
-- Create date: 5-MARCH-2015
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept
-- =============================================  
ALTER PROCEDURE [MASTERS].[USP_AssignMeter]
(  
@XmlDoc xml  
)  
AS  
BEGIN
	DECLARE 
		 @MeterNumber VARCHAR(50)
		,@GlobalAccountNumber VARCHAR(50)
		,@InitialReading INT
		,@AssignedMeterDate VARCHAR(20)
		,@CreatedBy VARCHAR(50)
		,@Reason VARCHAR(MAX)
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT	
	SELECT  
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		 ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
	
	DECLARE @MeterTypeId INT
		
	SELECT @MeterTypeId = ISNULL(MeterType,0)
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1	
	
	IF(@MeterTypeId != 2)
		BEGIN  
			SELECT 1 AS IsNotCreditMeter FOR XML PATH('CustomerRegistrationBE')  
		END
	ELSE IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END  
	ELSE IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE MeterNo = @MeterNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterAlreadyAssigned FOR XML PATH('CustomerRegistrationBE')
		END 
	ELSE IF((SELECT COUNT(0)FROM Tbl_CustomerMeterInfoChangeLogs 
				WHERE NewMeterNo = @MeterNumber AND ApproveStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsMeterChangeApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END  
	ELSE IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
						DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1		
				
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,@AssignedMeterDate
											,@InitialReading
											,@Reason
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					
					UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
							SET MeterNumber=@MeterNumber
								,ReadCodeID=2
					WHERE GlobalAccountNumber = @GlobalAccountNumber
	
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
					SET InitialReading=@InitialReading
						,PresentReading=@InitialReading
						,IsCAPMI = @IsCAPMIMeter
						,MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber=@GlobalAccountNumber
					
					INSERT INTO Tbl_Audit_AssignedMeterLogs(  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks
						,ApprovalStatusId
						,CreatedBy
						,CreatedDate
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  
						 AssignedMeterId
						,GlobalAccountNo
						,MeterNo
						,AssignedMeterDate
						,InitialReading
						,Remarks  
						,ApprovalStatusId  
						,CreatedBy  
						,CreatedDate  
						,PresentApprovalRole
						,NextApprovalRole
						,ModifiedBy
						,ModifiedDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
					
					IF(@IsCAPMIMeter = 1)  
						BEGIN  
							INSERT INTO Tbl_PaidMeterDetails  
							(  
							 AccountNo  
							,MeterNo  
							,MeterTypeId  
							,MeterCost  
							,OutStandingAmount
							,ActiveStatusId  
							,MeterAssignedDate  
							,CreatedDate  
							,CreatedBy  
							)  
							values  
							(  
							 @GlobalAccountNumber  
							,@MeterNumber  
							,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
							,@CAPMIAmount  
							,@CAPMIAmount  
							,1  
							,@AssignedMeterDate  
							,dbo.fn_GetCurrentDateTime()  
							,@CreatedBy  
							)       
						END  
			
				END
			
			SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
		END
END

--========Old Code

--DECLARE @MeterNumber VARCHAR(50)  
--		,@ReadCodeID VARCHAR(50) 
--		,@GlobalAccountNumber VARCHAR(50) 
--		,@IsSuccessful BIT =1
--		,@StatusText VARCHAR(200)
--		,@InitialReading INT
--		,@AssignedMeterDate VARCHAR(20)
--		,@CreatedBy VARCHAR(50)
--		,@Reason VARCHAR(MAX)
--		,@ApprovalStatusId INT

--BEGIN
--BEGIN TRY
--	BEGIN TRAN
			
--	--SET @StatusText='Deserialization'
--	 SELECT  
--	  @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')  
--	  ,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')  
--	  ,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
--	  ,@AssignedMeterDate=C.value('(MeterAssignedDate)[1]','VARCHAR(20)')
--	  ,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
--	  ,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
--	  ,@InitialReading=C.value('(InitialBillingKWh)[1]','INT')
--	  ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
--	 FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)  
  
--	SET @StatusText='Insert Log'
	
--		INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
--											,MeterNo
--											,AssignedMeterDate
--											,InitialReading
--											,Remarks
--											,ApprovalStatusId
--											,CreatedBy
--											,CreatedDate
--											)
--									VALUES( @GlobalAccountNumber
--											,@MeterNumber
--											,@AssignedMeterDate
--											,@InitialReading
--											,@Reason
--											,@ApprovalStatusId
--											,@CreatedBy
--											,dbo.fn_GetCurrentDateTime()
--											)
											
		
--	SET @StatusText='Update Statement'
	
--	UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails 
--	SET MeterNumber=@MeterNumber
--		,ReadCodeID=@ReadCodeID
--	WHERE GlobalAccountNumber = @GlobalAccountNumber
	
--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
--	SET InitialReading=@InitialReading
--		,PresentReading=@InitialReading
--	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
--	SET @StatusText='Success'
--	COMMIT TRAN	
--END TRY	   
--BEGIN CATCH
--	ROLLBACK TRAN
--	SET @IsSuccessful =0
--END CATCH
--END
--	SELECT 
--			@IsSuccessful AS IsSuccessful
--			,@StatusText AS StatusText
--	FOR XML PATH('CustomerRegistrationBE'),TYPE

--END


GO

/****** Object:  StoredProcedure [MASTERS].[USP_Re-AssignMeter]    Script Date: 07/16/2015 17:46:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  NEERAJ KANOJIYA    
-- Create date: 5-MARCH-2015  
-- Description: The purpose of this procedure is to assign meter to direct customer.
-- ModifiedBy: V. Bhimaraju
-- ModifiedDate: 24-Apr-2015
-- AssignedMeterDate,Reason Fields are added with approval concept  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_Re-AssignMeter]
(    
@XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE   
		  @MeterNumber VARCHAR(50)    
		  ,@ReadCodeID VARCHAR(50)   
		  ,@GlobalAccountNumber VARCHAR(50)   
		  ,@IsSuccessful BIT =1  
		  ,@ApprovalStatusId INT
		  ,@IsFinalApproval BIT = 0
		  ,@FunctionId INT
		  ,@Reason VARCHAR(MAX)
		  ,@CreatedBy VARCHAR(50)
		  ,@InitialReading INT
     ,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
  SELECT    
		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
		,@Reason=C.value('(Reason)[1]','VARCHAR(MAX)')
		,@CreatedBy=C.value('(CreatedBy)[1]','VARCHAR(50)')
		,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
    ,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
    
  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
	DECLARE @MeterTypeId INT
		
	SELECT @MeterTypeId = ISNULL(MeterType,0)
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1	
	
	IF(@MeterTypeId != 2)
		BEGIN  
			SELECT 1 AS IsNotCreditMeter FOR XML PATH('CustomerRegistrationBE')  
		END
	ELSE IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE MeterNumber=@MeterNumber)
		BEGIN
			SELECT 1 AS IsMeterAlreadyAssigned FOR XML PATH('CustomerRegistrationBE')
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo=@MeterNumber AND ApprovalStatusId IN (1,4))
		BEGIN
			SELECT 1 AS IsMeterReqByAnotherCust FOR XML PATH('CustomerRegistrationBE')
		END
 	ELSE IF((SELECT COUNT(0)FROM Tbl_AssignedMeterLogs 
				WHERE GlobalAccountNo = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('CustomerRegistrationBE')
		END
	IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
		BEGIN  
			SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')  
		END   
	ELSE
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@AssignedMeterRequestId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				BEGIN
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										DECLARE @UserDetailedId INT
										SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
									SET @CurrentApprovalLevel = @CurrentLevel+1
					--SELECT @CurrentLevel = [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId
				END
			
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				
			DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
			SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), @CAPMIAmount = ISNULL(CAPMIAmount,0) 
			FROM Tbl_MeterInformation 
			WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1
				
			
			SET @InitialReading=(SELECT ISNULL(ISNULL(PresentReading,0),ISNULL(InitialReading,0)) 
			FROM CUSTOMERS.Tbl_CustomerActiveDetails
			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			INSERT INTO Tbl_AssignedMeterLogs (	GlobalAccountNo
											,MeterNo
											,AssignedMeterDate
											,InitialReading
											,Remarks
											,ApprovalStatusId
											,CreatedBy
											,CreatedDate
											,PresentApprovalRole
											,NextApprovalRole
											,ModifiedBy
											,ModifiedDate
											,CurrentApprovalLevel
											,IsLocked
											,IsCAPMIMeter
											,CAPMIAmount
											)
									VALUES( @GlobalAccountNumber
											,@MeterNumber
											,dbo.fn_GetCurrentDateTime()
											,@InitialReading
											,@Reason
											,@ApprovalStatusId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,@PresentRoleId
											,@NextRoleId
											,@CreatedBy
											,dbo.fn_GetCurrentDateTime()
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
											,@IsCAPMIMeter
											,@CAPMIAmount
											)
			
			SET @AssignedMeterRequestId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN					
				IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
					BEGIN
					
						UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
							 SET MeterNumber=@MeterNumber  
							  ,ReadCodeID=@ReadCodeID  
						WHERE GlobalAccountNumber = @GlobalAccountNumber 
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						SET IsCAPMI = @IsCAPMIMeter
							,MeterAmount = @CAPMIAmount
						WHERE GlobalAccountNumber=@GlobalAccountNumber 
						
						INSERT INTO Tbl_Audit_AssignedMeterLogs(  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks
								,ApprovalStatusId
								,CreatedBy
								,CreatedDate
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
								,IsCAPMIMeter
								,CAPMIAmount)  
							SELECT  
								 AssignedMeterId
								,GlobalAccountNo
								,MeterNo
								,AssignedMeterDate
								,InitialReading
								,Remarks  
								,ApprovalStatusId  
								,CreatedBy  
								,CreatedDate  
								,PresentApprovalRole
								,NextApprovalRole
								,ModifiedBy
								,ModifiedDate
								,IsCAPMIMeter
								,CAPMIAmount
							FROM Tbl_AssignedMeterLogs WHERE AssignedMeterId = @AssignedMeterRequestId
							
							IF(@IsCAPMIMeter = 1)  
								BEGIN  
									INSERT INTO Tbl_PaidMeterDetails  
									(  
									 AccountNo  
									,MeterNo  
									,MeterTypeId  
									,MeterCost  
									,OutStandingAmount
									,ActiveStatusId  
									,MeterAssignedDate  
									,CreatedDate  
									,CreatedBy  
									)  
									values  
									(  
									 @GlobalAccountNumber  
									,@MeterNumber  
									,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
									,@CAPMIAmount  
									,@CAPMIAmount  
									,1  
									,dbo.fn_GetCurrentDateTime()  
									,dbo.fn_GetCurrentDateTime()  
									,@CreatedBy  
									)       
								END  
						
						SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')	
					END
				ELSE
					BEGIN
						SELECT 0 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
					END
				END
			ELSE 
				BEGIN
					SELECT 1 AS IsSuccessful FOR XML PATH('CustomerRegistrationBE')
				END
		END
END
		
--DECLARE   
--		  @MeterNumber VARCHAR(50)    
--		  ,@ReadCodeID VARCHAR(50)   
--		  ,@GlobalAccountNumber VARCHAR(50)   
--		  ,@IsSuccessful BIT =1  
--		  ,@StatusText VARCHAR(200)
     
--BEGIN  
--BEGIN TRY  
-- BEGIN TRAN  
     
-- SET @StatusText='Deserialization'  
--  SELECT    
--		 @MeterNumber=C.value('(MeterNumber)[1]','VARCHAR(50)')    
--		,@ReadCodeID=C.value('(ReadCodeID)[1]','VARCHAR(50)')
--		,@GlobalAccountNumber=C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')
    
--  FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)    
 
-- IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNumber AND ActiveStatusId = 1)
--	 BEGIN 
--		 SET @StatusText='Update Statement'  
		   
--		 UPDATE CUSTOMERS.Tbl_CustomerProceduralDetails   
--		 SET MeterNumber=@MeterNumber  
--		  ,ReadCodeID=@ReadCodeID  
--		 WHERE GlobalAccountNumber = @GlobalAccountNumber  
		   	   
--		 SET @StatusText='Success'
--	 END
-- ELSE 
--	BEGIN
--		SET @StatusText='Meter is in DeActivate state.'
--		SET @IsSuccessful =0  
--	END 
-- COMMIT TRAN   
--END TRY      
--BEGIN CATCH  
-- ROLLBACK TRAN  
-- SET @IsSuccessful =0  
--END CATCH  
--END  
-- SELECT   
--   @IsSuccessful AS IsSuccessful  
--   ,@StatusText AS StatusText  
-- FOR XML PATH('CustomerRegistrationBE'),TYPE  
  
--END  
--------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterInformation]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 08-04-2014  
-- Description: The purpose of this procedure is to get list of MeterInformation  
-- Author:  V.Bhimaraju  
-- Modified date: 04-02-2015  
-- Description: Added GlobalAccount No in this proc  
-- Author:  Faiz-ID103
-- Modified date: 20-APR-2015
-- Description: Modified the SP for active meter Type filter
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterInformation]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE  @PageNo INT  
   ,@PageSize INT  
   ,@BU_ID VARCHAR(20)  
      
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID= C.value('(BU_ID)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('MastersBE') AS T(C)   
    
  ;WITH PagedResults AS  
  (  
   SELECT  
     ROW_NUMBER() OVER(ORDER BY M.ActiveStatusId ASC , M.CreatedDate DESC ) AS RowNumber  
    ,MeterId  
    ,MeterNo  
    ,M.MeterType AS MeterTypeId  
    --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType  
    ,MT.MeterType AS MeterType --Faiz-ID103
    ,MeterSize  
    ,ISNULL(MeterDetails,'---') AS Details  
    ,M.ActiveStatusId  
    ,M.CreatedBy  
    ,MeterMultiplier  
    ,MeterBrand  
    ,MeterSerialNo  
    ,MeterRating  
    ,CONVERT(VARCHAR(50),NextCalibrationDate,103) AS BillDate  
    ,ISNULL((CONVERT(VARCHAR(50),NextCalibrationDate,106)),'--') AS NextCalibrationDate  
    ,ISNULL(ServiceHoursBeforeCalibration,'--') AS ServiceHoursBeforeCalibration  
    ,MeterDials  
    ,ISNULL(Decimals,0) AS Decimals  
    ,M.BU_ID  
    ,B.BusinessUnitName  
    ,M.IsCAPMIMeter
    ,M.CAPMIAmount
    ,ISNULL(CPD.GlobalAccountNumber,'--') AS AccountNo  
    ,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs AM WHERE AM.MeterNo = M.MeterNo AND ApprovalStatusId IN(1,4)) > 0) THEN 1
			WHEN ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs AM WHERE AM.NewMeterNo = M.MeterNo AND ApproveStatusId IN(1,4)) > 0) THEN 1
			ELSE 0 END) AS ActiveStatus
    FROM Tbl_MeterInformation AS M  
    JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID   
    LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.MeterNumber=M.MeterNo  
    JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 --Faiz-ID103
    WHERE M.ActiveStatusId IN(1,2)  
    AND M.BU_ID=@BU_ID OR @BU_ID=''  
  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('MastersBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')   
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateMeterInformationActiveStatus]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 08-04-2014
-- Description:	The purpose of this procedure is to Change ActiveStatusId of MeterInformation
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateMeterInformationActiveStatus]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @MeterId INT,@ActiveStatusId INT,@ModifiedBy VARCHAR(50)
	SELECT
		@MeterId=C.value('(MeterId)[1]','INT')
		,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)
	
	DECLARE @IsValid BIT = 1, @MeterNo VARCHAR(50), @IsSuccess INT = 0
	
	SELECT @MeterNo = MeterNo FROM Tbl_MeterInformation WHERE MeterId=@MeterId
	
	IF(@ActiveStatusId = 2)
		BEGIN
			IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD WHERE CPD.MeterNumber = @MeterNo) 
				BEGIN
					SET @IsValid = 0
				END
			ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4)) 
				BEGIN
					SET @IsValid = 0
				END
			ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId IN(1,4)) 
				BEGIN
					SET @IsValid = 0
				END
				
			IF(@IsValid = 1)
				BEGIN
					UPDATE Tbl_MeterInformation SET ActiveStatusId=@ActiveStatusId
						  ,ModifiedBy=@ModifiedBy
						  ,ModifiedDate=dbo.fn_GetCurrentDateTime()
					WHERE MeterId=@MeterId
					
					SET @IsSuccess = 1
				END
		END
	ELSE
		BEGIN
			UPDATE Tbl_MeterInformation SET ActiveStatusId=@ActiveStatusId
				  ,ModifiedBy=@ModifiedBy
				  ,ModifiedDate=dbo.fn_GetCurrentDateTime()
			WHERE MeterId=@MeterId
			
			SET @IsSuccess = 1
		END
	
	SELECT @IsSuccess AS IsSuccess FOR XML PATH('MastersBE')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
		,CMI.MeterMultiplier
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	,CustomerReadingInner.IsRollOver
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	
	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 
	
	
	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
			

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN IsValidReading = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN IsValidReading = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
				THEN (CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) AND ISDATE(CB.ReadingDate) = 1
							THEN (CASE WHEN CONVERT(DATE,CustomerReadingsLogs.ReadDate) >= CONVERT(DATE,CB.ReadingDate) 
										THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) 
										ELSE 0 END)
							ELSE (CASE WHEN CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CB.ReadingDate) 
										THEN ISNULL(CustomerReadings.IsRollOver,0) 
										ELSE 0 END)
					  END)
		  ELSE 0 END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,(CASE WHEN (ReadToDirectApprovals.ApprovalStatusId = 1 OR ReadToDirectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		--,(CASE WHEN BD.IsActive = 1 
		--		THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
		--		ELSE 0 END) AS IsDisabledBook
		,(SELECT TOP 1 (CASE WHEN BD.IsActive = 1 
				THEN (CASE WHEN ISNULL(BD.IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
				ELSE 0 END) 
			FROM Tbl_BillingDisabledBooks BD 
			WHERE BD.BookNo = CB.BookNo
			ORDER BY DisabledBookId DESC) AS IsDisabledBook
		,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 AND ISDATE(CB.ReadingDate) = 1
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
		,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') != '' AND ISDATE(CB.ReadingDate) = 1 
					THEN (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,CB.ReadingDate) THEN 1 ELSE 0 END)
					ELSE 0 
					END) AS IsMeterAssignedDateExists
		,(CASE WHEN CustomerReadings.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (Case when CONVERT(DATE,CB.ReadingDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)
			ELSE 0 END) as IsLatestBill
		,CB.MeterMultiplier
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerReadToDirectApprovaStatus As ReadToDirectApprovals ON CB.GlobalAccountNumber=ReadToDirectApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerAssignMeterApprovaStatus As AssignMeterApprovals	ON CB.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber
	LEFT JOIN #CustomerLatestBills As LatestBills ON CB.GlobalAccountNumber=LatestBills.GlobalAccountNumber
	--LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0 OR ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0 OR ISNULL(PresentReading,'') = ''
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)-- OR PrvExist = 1
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 --OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1 AND IsLatestBill = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Bill Generated for Future Date'
					WHERE IsBilled = 1 AND IsLatestBill = 1
					
				END
				
			---- TO check whether the reading is valid or not
			--IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
			--	BEGIN
					
			--		UPDATE #ReadingsValidation
			--		SET IsValid = 0,
			--			Comments = Comments + ', Invalid Readings'
			--		WHERE ISNULL(PresentReading,'') = ''
					
			--	END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId IN (1,4))
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE ApproveStatusId IN (1,4)
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsReadToDirectApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Disconnect Change Approval Process is pending for this Customer'
					WHERE IsReadToDirectApproval = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Changed Date'
					WHERE IsMeterChangedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterAssignedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Assigned Date'
					WHERE IsMeterAssignedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
				,@MeterMultiplier INT
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
				,MeterMultiplier
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy,
						@MeterMultiplier = MeterMultiplier
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@MeterMultiplier
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END


GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateMeterInformation]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 08-04-2014
-- Description:	Insert Meter Information details
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE  @MeterNo VARCHAR(50)
			,@MeterId INT
			,@MeterType INT
			,@MeterSize VARCHAR(50)
			,@MeterDetails VARCHAR(50)
			,@MeterMultiplier VARCHAR(50)
			,@MeterBrand VARCHAR(50)
			,@MeterStatus INT
			,@MeterSerialNo VARCHAR(50)
			,@MeterRating VARCHAR(50)
			,@NextCalibrationDate VARCHAR(20)
			,@ServiceHoursBeforeCalibration VARCHAR(50)
			,@ModifiedBy VARCHAR(50)
			,@BU_ID VARCHAR(20)
			,@MeterDials INT
			,@Decimals INT
			,@Flag INT
			
		SELECT
			 @MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')
			,@MeterId=C.value('(MeterId)[1]','INT')
			,@MeterType=C.value('(MeterType)[1]','INT')
			,@MeterSize=C.value('(MeterSize)[1]','VARCHAR(50)')
			,@MeterDetails=C.value('(Details)[1]','VARCHAR(50)')
			,@MeterMultiplier=C.value('(MeterMultiplier)[1]','VARCHAR(50)')
			,@MeterBrand=C.value('(MeterBrand)[1]','VARCHAR(50)')
			,@MeterSerialNo=C.value('(MeterSerialNo)[1]','VARCHAR(50)')
			,@MeterRating=C.value('(MeterRating)[1]','VARCHAR(50)')
			,@NextCalibrationDate=C.value('(NextCalibrationDate)[1]','VARCHAR(20)')
			,@ServiceHoursBeforeCalibration=C.value('(ServiceHoursBeforeCalibration)[1]','VARCHAR(50)')			
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(20)')
			,@MeterDials=C.value('(MeterDials)[1]','INT')
			,@MeterStatus=C.value('(MeterStatus)[1]','INT')
			,@Decimals=C.value('(Decimals)[1]','INT')
			,@Flag=C.value('(Flag)[1]','INT')
		FROM @XmlDoc.nodes('MastersBE') as T(C)
		
		IF(@Flag = 1)
			BEGIN
				UPDATE Tbl_MeterInformation SET  BU_ID = @BU_ID
				WHERE MeterId = @MeterId
				
				SELECT 1 AS IsSuccess
				FOR XML PATH('MastersBE')
			END		
		ELSE IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND MeterId != @MeterId)
			BEGIN
				SELECT 1 AS IsMeterNoExists FOR XML PATH('MastersBE')
			END
		ELSE IF EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterSerialNo=@MeterSerialNo AND MeterId != @MeterId)
			BEGIN
				SELECT 1 AS IsMeterSerialNoExists FOR XML PATH('MastersBE')
			END
		ELSE
			BEGIN 
				DECLARE @IsValid BIT = 1, @IsSuccess INT = 0
				
				IF EXISTS(SELECT 0 FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD WHERE CPD.MeterNumber = @MeterNo) 
					BEGIN
						SET @IsValid = 0
					END
				ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4)) 
					BEGIN
						SET @IsValid = 0
					END
				ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId IN(1,4)) 
					BEGIN
						SET @IsValid = 0
					END
					
				IF(@IsValid = 1)
				BEGIN
					UPDATE Tbl_MeterInformation 
					SET  MeterNo = @MeterNo
						,MeterType = @MeterType
						,MeterSize = @MeterSize
						,MeterDetails = (CASE @MeterDetails WHEN '' THEN NULL ELSE @MeterDetails END)
						,MeterMultiplier = @MeterMultiplier
						,MeterBrand = @MeterBrand
						,MeterStatus = 1
						,MeterSerialNo = @MeterSerialNo
						,MeterRating = @MeterRating
						,NextCalibrationDate = CASE @NextCalibrationDate WHEN '' THEN NULL ELSE @NextCalibrationDate END
						,ServiceHoursBeforeCalibration = CASE @ServiceHoursBeforeCalibration WHEN '' THEN NULL ELSE @ServiceHoursBeforeCalibration END
						,ModifiedBy = @ModifiedBy
						,MeterDials = @MeterDials
						,Decimals = (CASE @Decimals WHEN '' THEN 0 ELSE @Decimals END)
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE MeterId = @MeterId
					
					SET @IsSuccess = 1
				END
				
				SELECT @IsSuccess AS IsSuccess
				FOR XML PATH('MastersBE')
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================

ALTER PROCEDURE [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @GlobalAccountNo VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	DECLARE @IsDisabledBook BIT 
	
	SELECT @CustomerBusinessUnitID = U.BU_ID,@CustomerActiveStatusID = ISNULL(U.ActiveStatusId,0)  
		,@GlobalAccountNo = U.GlobalAccountNumber
		--,@IsDisabledBook = (CASE WHEN BD.IsActive = 1 
		--						THEN (CASE WHEN IsPartialBill IS NULL THEN 1 ELSE 0 END)
		--						ELSE 0 END)
		,@IsDisabledBook = (SELECT TOP 1 (CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(BD.IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) 
							FROM Tbl_BillingDisabledBooks BD 
							WHERE BD.BookNo = U.BookNo
							ORDER BY DisabledBookId DESC)
	FROM  UDV_IsCustomerExists U
	--LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = U.BookNo
	WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo OR MeterNumber=@AccountNo)
	  	
	IF @GlobalAccountNo IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
		IF @CustomerBusinessUnitID = @BUID    
			BEGIN
				IF @CustomerActiveStatusID = 1 --OR @CustomerActiveStatusID=2 
					BEGIN
						--PRINT 'Sucess and Active  Customer'
						SELECT 1 AS IsSuccess,
							@GlobalAccountNo AS AccountNo,
							@IsDisabledBook AS IsDisabledBook,
							0 AS CustIsNotActive
						FOR XML PATH('RptCustomerLedgerBe') 
					END
				ELSE
					BEGIN							 
						--PRINT 'Failure In Active Status'
						SELECT 1 AS IsSuccess,
								1 AS CustIsNotActive
						FOR XML PATH('RptCustomerLedgerBe') 
					END
			END
		ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU ,
						@IsDisabledBook AS IsDisabledBook,
						0 AS CustIsNotActive
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [MASTERS].[USP_IsMeterAvailable_New]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Modified by : Satya  
-- Modified Date: 6-APRIL-2014    
-- DESC   : MODIFIED CONDITION WITH IS NULL  
-- =============================================    
ALTER PROCEDURE [MASTERS].[USP_IsMeterAvailable_New]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @MeterNo VARCHAR(100)    
   ,@BUID VARCHAR(50)    
   ,@CustomerTypeId INT    
       
  SELECT       
   @MeterNo = C.value('(MeterNo)[1]','VARCHAR(100)')    
   ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
   ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
    
  Declare @MeterAvaiable bit = 1  
		  ,@MeterDials INT
		  ,@IsCAMPIMeter BIT = 0
		  ,@CAPMIAmount DECIMAL(18,2) = 0
  
  IF NOT EXISTS (SELECT M.MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2  
      AND M.ActiveStatusId IN(1)   
      AND (BU_ID=@BUID OR @BUID='')        
      AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE IF EXISTS (SELECT M.NewMeterNo from Tbl_CustomerMeterInfoChangeLogs M where  M.NewMeterNo =@MeterNo   
      AND M.ApproveStatusId IN(1,4))    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
    ELSE IF EXISTS (SELECT M.MeterNo from Tbl_AssignedMeterLogs M where  M.MeterNo =@MeterNo   
      AND M.ApprovalStatusId IN(1,4))    
   BEGIN  
     SET @MeterAvaiable=0    
   END 
   ELSE
   BEGIN
		SELECT @MeterDials=M.MeterDials,@IsCAMPIMeter = IsCAPMIMeter, @CAPMIAmount = CAPMIAmount
		FROM Tbl_MeterInformation M WHERE MeterNo=@MeterNo AND ActiveStatusId = 1
   END
  SELECT @MeterAvaiable AS IsSuccess, @MeterDials AS MeterDials
		,@IsCAMPIMeter AS IsCAPMIMeter, @CAPMIAmount AS CAPMIAmount
  FOR XML PATH('MastersBE'),TYPE      
       
END    


GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterDialsByMeterNo]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 26-02-2015
-- Description: The purpose of this Procedure is to MeterDials By Meter No
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetMeterDialsByMeterNo]  
 (  
 @XmlDoc xml  
 )  
AS  
BEGIN  
 DECLARE @MeterNo VARCHAR(100),@BUID VARCHAR(50)
 SELECT  
  @MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('ConsumerBe') as T(C)
 
	--DECLARE @MeterDials INT
		
	--SET @MeterDials=(SELECT ISNULL(MeterDials,0) 
	--FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	--SELECT @MeterDials AS MeterDials,1 As IsSuccess 
	
	Declare @MeterAvaiable bit = 1  
		  ,@MeterDials INT
		  ,@IsCAMPIMeter BIT = 0
		  ,@CAPMIAmount DECIMAL(18,2) = 0
  
	  IF NOT EXISTS (SELECT M.MeterNo from Tbl_MeterInformation M where  M.MeterNo =@MeterNo AND M.MeterType=2  
		  AND M.ActiveStatusId IN(1)   
		  AND (BU_ID=@BUID OR @BUID='')        
		  AND M.MeterNo NOT IN (SELECT ISNULL(MeterNumber,0) FROM CUSTOMERS.Tbl_CustomerProceduralDetails )   )    
	   BEGIN  
		 SET @MeterAvaiable=0    
	   END 
	   ELSE IF EXISTS (SELECT M.NewMeterNo from Tbl_CustomerMeterInfoChangeLogs M where  M.NewMeterNo =@MeterNo   
		  AND M.ApproveStatusId IN(1,4))    
	   BEGIN  
		 SET @MeterAvaiable=0    
	   END 
		ELSE IF EXISTS (SELECT M.MeterNo from Tbl_AssignedMeterLogs M where  M.MeterNo =@MeterNo   
		  AND M.ApprovalStatusId IN(1,4))    
	   BEGIN  
		 SET @MeterAvaiable=0    
	   END 
	   ELSE
	   BEGIN
			SELECT @MeterDials=M.MeterDials,@IsCAMPIMeter = IsCAPMIMeter, @CAPMIAmount = CAPMIAmount
			FROM Tbl_MeterInformation M WHERE MeterNo=@MeterNo AND ActiveStatusId = 1
	   END
	  SELECT @MeterAvaiable AS IsSuccess, @MeterDials AS MeterDials
			,@IsCAMPIMeter AS IsCAPMIMeter, @CAPMIAmount AS MeterAmt
	
 FOR XML PATH('ConsumerBe')  
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_Billing_INDV]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- Modified By: Neeraj Kanojiya
-- Description: Created new copy. Will search Activ and In-Active customer with Temporary Book Disabled.
-- Date		  : 3-June-15
-- Modified By: Neeraj Kanojiya
-- Description: WILL RETURN TRUE EVEN BOOK IS TEMPORARLY CLOSED
-- Date		  : 13-JULY-15
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_Billing_INDV] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 


	Declare @CustomerBusinessUnitID varchar(50)
			,@CustomerActiveStatusID int
			,@BookNo varchar(50)
			,@NoPower int
			,@CustomerType INT
			,@MeterType INT
			,@IsPrepaidCustomer BIT=0
			,@IsNotCreditMeter BIT=1
	
 
	SELECT @CustomerType=CPD.CustomerTypeId,@MeterType=MI.MeterType 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD
	JOIN Tbl_MeterInformation AS MI ON CPD.MeterNumber=MI.MeterNo
	WHERE CPD.GlobalAccountNumber=@AccountNo
	if(@CustomerType=3)
		SET @IsPrepaidCustomer=1
	if(@MeterType!=2)
		SET @IsNotCreditMeter = 0
	
	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo AND ActiveStatusId IN (1,2)

	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	 
	  
	Select @NoPower=case when DisableTypeId=2 then 1 else 0 end from 
	Tbl_BillingDisabledBooks    where BookNo =@BookNo	 and IsActive=1   
	PRINT   @CustomerActiveStatusID
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
		SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
						IF	 @CustomerActiveStatusID  = 1   OR @CustomerActiveStatusID  = 2
							BEGIN
								IF  isnull(@NoPower,0)!=1 
									SELECT	1 AS IsSuccess
											,@CustomerActiveStatusID AS ActiveStatusId 
											,@IsPrepaidCustomer AS IsPrepaidCustomer
											,@IsNotCreditMeter AS IsCreditMeter
									FOR XML PATH('RptCustomerLedgerBe')
								ELSE
									 SELECT 1 AS IsSuccess
											,@CustomerActiveStatusID AS ActiveStatusId 
											,@IsPrepaidCustomer AS IsPrepaidCustomer
											,@IsNotCreditMeter AS IsCreditMeter
									 FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess     Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess
										,@CustomerActiveStatusID AS ActiveStatusId 
										,@IsPrepaidCustomer AS IsPrepaidCustomer
										,@IsNotCreditMeter AS IsCreditMeter
								 FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
				END
				ELSE
					BEGIN
					SELECT	0 AS IsSuccess
							,@CustomerActiveStatusID AS ActiveStatusId 
							,@IsPrepaidCustomer AS IsPrepaidCustomer
							,@IsNotCreditMeter AS IsCreditMeter
					FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetPaymets]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetPaymets]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPaymentsBe') AS T(C)
		
	IF ISNULL(@FromDate,'') =''    
	BEGIN    
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
	END 
	IF ISNULL(@Todate,'') =''
	BEGIN    
		SET @Todate = dbo.fn_GetCurrentDateTime()    
	END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,(CD.AccountNo + ' - ' + CD.GlobalAccountNumber) AS GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,CD.BusinessUnitName AS BusinessUnitName
			,CD.ServiceCenterName AS ServiceCenterName
			,CD.ServiceUnitName AS ServiceUnitName
			,CD.CycleName
			,CD.OldAccountNo
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,ISNULL(CP.BatchNo,0) AS BatchNo
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,107),'--') AS ReceivedDate
			--,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
			,CD.OutStandingAmount AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD(NOLOCK)	
		INNER JOIN Tbl_CustomerPayments CP(NOLOCK) ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
		INNER JOIN Tbl_MBillingTypes BT(NOLOCK) ON CP.ReceivedDevice = BT.BillingTypeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		
	)
	
	SELECT	 
		 RowNumber
		,GlobalAccountNumber AS AccountNo
		,Name
		,ServiceAddress
		,BusinessUnitName
		,ServiceCenterName
		,ServiceUnitName
		,CycleName
		,BookNumber
		,CustomerSortOrder
		,BookSortOrder
		,ReceivedDevice
		,OldAccountNo
		--,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,PaidAmount
		,BatchNo
		,ReceivedDate
		--,CONVERT(VARCHAR(20),CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
		,TotalDueAmount
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(TotalDueAmount) FROM PagedResults) AS MONEY),-1) AS TotalDue
		,(Select COUNT(0) from PagedResults) as TotalRecords
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(PaidAmount) FROM PagedResults) AS MONEY),-1) AS TotalReceivedAmount
	FROM PagedResults p
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetYearAndMonthForTariffFixed]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 29-03-2014
-- Modified:  V.Bhimaraju
-- Modified date: 25-07-2014 
-- Description: The purpose of this procedure is to get Max month and year of Tariff Fixed by Tariff Class  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetYearAndMonthForTariffFixed]  
(  
@XmlDoc xml  
)   
AS  
BEGIN  
 DECLARE @TariffClassId INT
 --,@ChargeId INT  
 SELECT  
  @TariffClassId=C.value('(ClassID)[1]','INT')  
  --,@ChargeId=C.value('(ChargeId)[1]','INT')  
 FROM @XmlDoc.nodes('TariffManagementBE') as T(C)  
   
 SELECT   
  CASE WHEN MONTH(MAX(Todate)) IS NULL  
  THEN ''  
  WHEN MONTH(MAX(Todate))=12   
   THEN '01'  
  ELSE  
   ISNULL(CASE WHEN (MONTH(MAX(Todate))+1) <10 THEN '0'+CONVERT(VARCHAR(20),MONTH(MAX(Todate))+1) ELSE CONVERT(VARCHAR(20),MONTH(MAX(Todate))+1) END ,'')  
  END AS TariffMonth,  
  CASE WHEN MONTH(MAX(Todate)) IS NULL  
  THEN ''  
  WHEN MONTH(MAX(Todate))=12   
   THEN CONVERT(VARCHAR(20),YEAR(MAX(Todate))+1)  
  ELSE  
   ISNULL(CONVERT(VARCHAR(20),YEAR(MAX(Todate))),'')  
  END AS TariffYear  
  FROM Tbl_LAdditionalClassCharges   
 WHERE ClassID=@TariffClassId --AND IsActive=1 --AND ChargeId=@ChargeId  
 FOR XML PATH('TariffManagementBE')  
  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithMeter]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptReadCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,CD.MeterNumber
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  ,ISNULL(CD.PresentReading,ISNULL(CD.InitialReading,0)) AS PresentReading
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=2
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  ,MeterNumber
				  ,CONVERT(VARCHAR(20),ConnectionDate,106) AS ConnectionDate
				  --,@Count As TotalRecords 
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
				  ,PresentReading
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithoutMeter]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithoutMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptDirectCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,CD.BookCode AS BookNo
			  ,CD.BusinessUnitName
			  ,CD.ServiceUnitName
			  ,CD.ServiceCenterName
			  ,CD.ConnectionDate
			  ,CD.CycleName
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  ,ISNULL(CD.AvgReading,0) AS AvgReading
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  --LEFT JOIN Tbl_DirectCustomersAvgReadings DC ON DC.GlobalAccountNumber=CD.GlobalAccountNumber
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=1
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,CONVERT(VARCHAR(20),ConnectionDate,106)AS ConnectionDate
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  --,@Count As TotalRecords
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
				  ,AvgReading
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdditionalClassCharges]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju  
-- Create date: 16-MAR-2014  
-- ModifiedDate: 10-Apr-2014  
-- Description: The purpose of this procedure is to Get TariffAdditionalClassCharges  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetAdditionalClassCharges]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @PageNo INT  
    ,@PageSize INT  
    ,@Year INT   
    ,@Month INT   
    ,@Date DATE  
    ,@MonthStartDate VARCHAR(50)  
      
 SELECT @PageNo=C.value('PageNo[1]','INT')  
    ,@PageSize=C.value('PageSize[1]','INT')  
    ,@Year=C.value('(Year)[1]','INT')  
    ,@Month=C.value('(Month)[1]','INT')  
 FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)  
   
   
 IF(@Month <> 0 AND @Year <> 0)  
  BEGIN  
   SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'      
   SET @Date = CONVERT(DATE,(CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate)))))     
  END  
 ELSE  
  BEGIN  
   SET @Date = ''  
  END   
   
 ;WITH PagedResults AS  
  (  
  SELECT  
      ROW_NUMBER() OVER(ORDER BY C.ClassId ASC , FromDate ASC ) AS RowNumber  
      ,AdditionalChargeID  
      ,C.ClassId  
     -- ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassId=C.ClassId) AS ClassName  
      ,(MTC.ClassName) AS ClassName  
      --, (CASE (MTC.IsActiveClass) WHEN 0 THEN 'Inactive' ELSE 'Active' END)AS ClassStatus
      ,C.ChargeId  
      --,(SELECT ChargeName FROM Tbl_MChargeIds WHERE ChargeId = C.ChargeId AND IsAcitve = 1) AS ChargeName  
      ,MC.ChargeName  
      ,(CASE WHEN (CONVERT(Date,ToDate) < CONVERT(Date,GETDATE())) THEN 1 ELSE 0 END) AS IsPastDate
      ,Amount  
      --,CONVERT(VARCHAR(30),CONVERT(money,C.Amount),1) AS Amount  
      --,CONVERT(VARCHAR(50),FromDate,103) AS FromDate  
      --,CONVERT(VARCHAR(50), FromDate, 106) AS FromDate -- Corect  
      --, REPLACE(RIGHT(CONVERT(VARCHAR(30),FromDate,6),6),' ','-') AS FromDate  
      --,CONVERT(VARCHAR(50),ToDate,103) AS ToDate  
      --,CONVERT(VARCHAR(50), ToDate, 106) AS ToDate -- Corect  
      --, REPLACE(RIGHT(CONVERT(VARCHAR(30),ToDate,6),6),' ','-') AS ToDate  
      ,(CONVERT(VARCHAR(50), FromDate, 106) +' - '+ CONVERT(VARCHAR(50), ToDate, 106)) AS FromDate        
      ,IsActive 
	  ,(SELECT (case when (t.IsActiveClass = 0) then 'Inactive' 
	   when (tt.IsActiveClass = 0) then 'Inactive' when (C.IsActive = 0) then 'Inactive' else 'Active' end ) 
	   from Tbl_MTariffClasses t
	   LEFT join Tbl_MTariffClasses tt
	   on tt.RefClassID=t.ClassID 
	   where tt.ClassName is not null AND tt.ClassID=MTC.ClassID ) as ClassStatus
   FROM Tbl_LAdditionalClassCharges C  
   LEFT JOIN Tbl_MChargeIds MC ON MC.ChargeId=C.ChargeId 
   JOIN Tbl_MTariffClasses MTC ON MTC.ClassID=C.ClassID
   WHERE 
   --IsActive=1 AND 
   MC.IsAcitve=1  
   AND (@Date Between CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) OR @Date = '')  
   --WHERE   
   AND  
   (CASE @YEAR WHEN 0 THEN YEAR(GETDATE()) ELSE @YEAR END)  
    BETWEEN CASE @YEAR WHEN 0 THEN YEAR(GETDATE()) ELSE YEAR(FromDate) END  
    AND CASE @YEAR WHEN 0 THEN YEAR(getdate()) ELSE YEAR(ToDate) END  
   --AND IsActive=1  
   --AND (CASE @Month WHEN 0 THEN MONTH(GETDATE()) ELSE @Month END)  
   -- BETWEEN CASE @Month WHEN 0 THEN MONTH(GETDATE()) ELSE MONTH(FromDate) END  
   -- AND CASE @Month WHEN 0 THEN MONTH(getdate()) ELSE MONTH(ToDate) END  
  )  
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('TariffManagement'),TYPE  
  )  
  FOR XML PATH(''),ROOT('TariffManagementBEInfoByXml')  
END

GO

/****** Object:  StoredProcedure [dbo].[Usp_DeleteAdditionalClassCharges]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 16-MAR-2014
-- Description:	The purpose of this procedure is to Delete TariffAdditionalClassCharges
--SELECT * FROM Tbl_LAdditionalClassCharges
-- =============================================
ALTER PROCEDURE [dbo].[Usp_DeleteAdditionalClassCharges]
(
	@XmlDoc xml
)
AS

BEGIN
	DECLARE  @AdditionalChargeID INT
			,@ModifiedBy VARCHAR(50)
			,@ActiveStatusId BIT  
			,@TariffClassId INT
	SELECT @AdditionalChargeID=C.value('(AdditionalChargeID)[1]','INT')
		  ,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@ActiveStatusId = C.value('(ActiveStatusId)[1]','BIT')  
	FROM @XmlDoc.nodes('TariffManagementBE') AS T(C)
	
	IF(@ActiveStatusId = 0)--For Deactivation
	BEGIN
		SET @TariffClassId = (SELECT ClassID FROM Tbl_LAdditionalClassCharges WHERE AdditionalChargeID = @AdditionalChargeID)
		
		--IF No Customer associated
		IF NOT EXISTS (SELECT 0 FROM CUSTOMERS.Tbl_CustomerProceduralDetails where TariffClassID=@TariffClassId)
		BEGIN
			UPDATE Tbl_LAdditionalClassCharges SET  IsActive=0
												   ,ModifiedBy=@ModifiedBy
												   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
			WHERE AdditionalChargeID=@AdditionalChargeID
			
			SELECT 1 AS IsSuccess
			FOR XML PATH('TariffManagementBE')
		END
		ELSE--IF Customers Associated
		BEGIN
			SELECT 1 AS IsCustomersAssociated
			FOR XML PATH('TariffManagementBE')
		END
	END
	ELSE --- FOR Activation
	BEGIN
		UPDATE Tbl_LAdditionalClassCharges 
				SET  IsActive=1
				   ,ModifiedBy=@ModifiedBy
				   ,ModifiedDate=dbo.fn_GetCurrentDateTime()
			WHERE AdditionalChargeID=@AdditionalChargeID
			
			SELECT 1 AS IsSuccess
			FOR XML PATH('TariffManagementBE')
	END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerReadings]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 13-Jul-2015
-- Description:	The purpose of this procedure is to get the last 6 months Reading of the read customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerReadings]
(
@XmlDoc XML
)
AS
BEGIN

	DECLARE  @GlobalAccountNumber VARCHAR(50)  
			,@IsSuccess BIT	
			,@IsCustExistsInOtherBU BIT
			,@BU_ID VARCHAR(50)
			,@Flag INT
			,@FlagDetails INT
			,@ActiveStatusId INT  
    SELECT              
    @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)'),   
    @BU_ID = C.value('(BUID)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)       
	
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId=ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId=1)
		BEGIN
		IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
			BEGIN
					SELECT 1 as IsCustomerHaveReadings
					FOR XML PATH('BillingBE'),TYPE
			END		
		ELSE IF EXISTS(SELECT 0 FROM UDV_CustomerMeterInformation WHERE GlobalAccountNumber=@GlobalAccountNumber  AND ReadCodeID=2)
			BEGIN			
					SELECT
					(
						SELECT 
						 dbo.fn_GetCustomerFullName(CD.GlobalAccountNumber) AS Name
						 ,CD.ClassName As Tariff
						 ,CD.OutStandingAmount AS OutstandingAmount
						 ,CD.GlobalAccountNumber
						 ,CD.MeterNumber AS MeterNo
						 ,CD.OldAccountNo
						 ,CD.InitialReading as PreviousReading
						 ,CD.PresentReading
						 ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CD.CustomerTypeId) AS CustomerType
						 ,(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber) AS TotalReadings
						 ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS GlobalAccNoAndAccNo
						  from UDV_CustomerDescription CD
						  --left Join Tbl_CustomerReadings CR ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
						  where CD.GlobalAccountNumber = @GlobalAccountNumber
						 FOR XML PATH('BillingBE'),TYPE    
					)
					,
					(
					
						SELECT TOP 6 --(datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate)) as ReadingMonth				
									 (CONVERT(VARCHAR(20),ReadDate,106)) as ReadingMonth				
									,ReadDate
									,CR.Usage 
									,MeterNumber as MeterNo
									,CR.AverageReading
									,CR.PreviousReading
									,CR.PresentReading
									,ReadCode
									,(CASE when CB.BillGeneratedDate is null then '-' else  CONVERT(varchar(20),CB.BillGeneratedDate,106) end) AS BilledDate
						FROM Tbl_CustomerReadings CR
						LEFT JOIN Tbl_CustomerBills CB ON CB.CustomerBillId=CR.BillNo
						JOIN Tbl_MReadCodes R ON R.ReadCodeId=CR.ReadType
						WHERE GlobalAccountNumber=@GlobalAccountNumber
						ORDER BY CustomerReadingId DESC
						--SELECT (datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate)) as ReadingMonth
						--		,ReadDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0) 
						--		,SUM(Usage) as Usage
						--		,MeterNumber as MeterNo
						--		,AverageReading
						--		,PreviousReading 
						--		,PresentReading 
						--		,R.ReadCode
						--		,(CASE when BilledDate is null then '-' else  CONVERT(varchar(20),BilledDate,106) end) AS BilledDate
						--FROM    Tbl_CustomerReadings CR
						--JOIN Tbl_MReadCodes R ON R.ReadCodeId = CR.ReadType
						--WHERE   GlobalAccountNumber =@GlobalAccountNumber
						-- --and ReadDate > dateadd(m, -6, CONVERT(DATETIME,'13/07/2015',103) - datepart(d, CONVERT(DATETIME,'13/05/2015',103)) + 1)
						-- and ReadDate > dateadd(m, -6, GETDATE() - datepart(d, GETDATE()) + 1)
						-- --and ReadDate > dateadd(m, -6, ReadDate - datepart(d, ReadDate) + 1)
						--GROUP BY DATEADD(MONTH, DATEDIFF(MONTH, 0, ReadDate), 0)
						--		, MeterNumber
						--		,(datename(MONTH, ReadDate) +'-'+datename(YEAR, ReadDate))        
						--		,R.ReadCode
						--		,BilledDate
						--		,AverageReading
						--		,PreviousReading
						--		,PresentReading
								FOR XML PATH('BillingListBE'),TYPE    
					)	
					FOR XML PATH(''),ROOT('CustomerReadingListBeByXml')       		 	
			END
			ELSE		
				BEGIN
						SELECT 1 as IsDirectCustomer
						--FOR XML PATH(''),ROOT('BillingListBE')       	
						FOR XML PATH('BillingBE'),TYPE
				END
		END
	ELSE	--//If customer existence check is successfull then get data
		BEGIN
				SELECT  @IsSuccess AS IsSuccess
					,@GlobalAccountNumber AS GlobalAccountNumber 
					,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
					,@ActiveStatusId AS ActiveStatusId
				FOR XML PATH('RptCustomerLedgerBe')
			END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
			,@Count INT
			,@TotalReadingEnergies DECIMAL(18,2)
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	IF((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)=0)
		BEGIN
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@ModifiedReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE
		END	
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
		BEGIN
			SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
		BEGIN
			SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
		BEGIN
			SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
		END	
	ELSE
		BEGIN		
		
			SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)			
			
			SET @Count=(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			IF (@Count=1)
				BEGIN
					SET @TotalReadingEnergies=@Usage
				END
			ELSE
				BEGIN
					
					DECLARE @Last2ndTotalReadingEnergies DECIMAL(18,2)
					
					SELECT @Last2ndTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					WHERE TotalReadings =(SELECT MAX(TotalReadings)-1
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber=@GlobalAccountNumber
					
					--SELECT @Last1stTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					--WHERE TotalReadings =(SELECT MAX(TotalReadings)
					--								FROM Tbl_CustomerReadings
					--								WHERE GlobalAccountNumber=@GlobalAccountNumber)
					--AND GlobalAccountNumber=@GlobalAccountNumber
					
					UPDATE Tbl_CustomerReadings
					SET IsReadingWrong = 1
					WHERE TotalReadings =(SELECT MAX(TotalReadings)
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber = @GlobalAccountNumber

					SET @TotalReadingEnergies= @Last2ndTotalReadingEnergies+@Usage
				END
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@GlobalAccountNumber,@Usage)
			SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
			SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			DECLARE @PreviousReadDate DATETIME
			SELECT TOP 1 @PreviousReadDate=ReadDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber
			ORDER BY CustomerReadingId DESC
			
			
			INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					--,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					--,IsReadingWrong
					--,IsRollOver
					)
				VALUES(
					 @GlobalAccountNumber
					,GETDATE()
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,@AverageReading
					,0
					--,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
					,@TotalReadingEnergies
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
					,@Multipiler
					,2
					,@CreatedBy
					,GETDATE()
					--,@IsTamper
					,@MeterNumber
					,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
					--,@IsRollover
					--,1
					)
				
				
					
			INSERT INTO Tbl_PresentReadingAdjustmentLog
					(
						 GlobalAccountNumber
						,PreviousReading 
						,PresentReading 
						,NewPresentReading 
						,PreviousReadDate 
						,PresentReadDate 
						,MeterNumber 
						,CreatedBy 
						,CreatedDate
					)
			VALUES	(
						@GlobalAccountNumber
						,@PreviousReading
						,@PresentReading
						,@ModifiedReading
						,@PreviousReadDate
						,dbo.fn_GetCurrentDateTime()
						,@MeterNumber
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@ModifiedReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE 	
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetDisableBooksOnlyNoPwr]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================   
-- Modified By : NEERAJ KANOJIYA  
-- Modified Date : 13-JULY-15
-- Description  : CHECK ONLY FOR NO POWER CONDITION.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetDisableBooksOnlyNoPwr]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
 DECLARE @GlobalAccountNumber VARCHAR(MAX)  --='0000000125'
   ,@IsExists BIT  
     
 SELECT       
  @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(MAX)') 
 FROM @XmlDoc.nodes('BillGenerationBe') AS T(C)   
   

  IF EXISTS(SELECT CPD.BookNo FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD 
	JOIN Tbl_BillingDisabledBooks AS BDB ON CPD.BookNo=BDB.BookNo 
											AND BDB.IsActive=1 AND BDB.DisableTypeId=1 AND BDB.IsPartialBill =0 
											AND CPD.GlobalAccountNumber=@GlobalAccountNumber)
												
	SET @IsExists=1
  ELSE 
   SET @IsExists=0
   
   SELECT @IsExists AS IsExists
   FOR XML PATH('BillGenerationBe'),TYPE  
  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 07/16/2015 17:46:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
   ,@AdjustmentAmountEffected DECIMAL(18,2)
   ,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
          
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	 SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	 FROM Tbl_CustomerBills AS CB
	 JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
	 JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
	 WHERE CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      ,@SetupDate=SetupDate 
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END		
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)	
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@SetupDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   IF isnull(@PaidMeterDeductedAmount,0) >0
   BEGIN
   
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
   
   update Tbl_PaidMeterDetails
   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
   where AccountNo=@GlobalAccountNumber
   
   END
         
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
   SET @BillingComments=NULL
   SET @TotalBillAmount=NULL
   SET @PaidMeterDeductedAmount=NULL
   SET @IsEmbassyCustomer=NULL
   SET @TaxPercentage=NULL
   SET @PaidMeterDeductedAmount=NULL
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
	END
END  
				 


GO



GO
-- =============================================
-- Author:		Faiz-ID103
-- Create date: 16-Jul-2015
-- Description:	For getting highest pole Order id
-- =============================================
CREATE PROCEDURE USP_GetMaxPoleOrderId
	
AS
BEGIN
	DECLARE @MaxPoleOrderId INT
	SELECT @MaxPoleOrderId = (MAX(PoleMasterOrderID)+1) From Tbl_MPoleMasterDetails	

	SELECT @MaxPoleOrderId AS MaxPoleOrderId
	FOR XML PATH('PoleBE'),TYPE  
END
GO
