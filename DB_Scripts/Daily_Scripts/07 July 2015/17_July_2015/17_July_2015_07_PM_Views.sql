
GO

/****** Object:  View [dbo].[UDV_EditListReport]    Script Date: 07/17/2015 19:05:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
ALTER VIEW [dbo].[UDV_EditListReport]    
AS         
  
 SELECT   
	 CD.GlobalAccountNumber  
	 ,CD.AccountNo  
	 ,CD.OldAccountNo  
	 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
	 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
				 ,CD.Service_Landmark  
				 ,CD.Service_City,'',  
				CD.Service_ZipCode) AS [ServiceAddress]   
	 ,PD.SortOrder  
	 ,BN.SortOrder AS BookSortOrder  
	 ,PD.TariffClassID AS TariffId    
	 ,C.CycleId    
	 ,C.CycleName    
	 ,C.CycleCode  
	 ,BU.BU_ID
	 ,BU.BusinessUnitName
	 ,PD.MeterNumber
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID  
   
 ----------------------------------------------------------------------------------------------------  
  


GO



GO

/****** Object:  View [dbo].[UDV_IsCustomerExists]    Script Date: 07/17/2015 19:06:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
ALTER VIEW [dbo].[UDV_IsCustomerExists]  
AS  
 SELECT CD.GlobalAccountNumber,  
  CD.OldAccountNo,  
  PD.MeterNumber  
  ,CD.ActiveStatusId  
  ,SU.BU_ID  
  ,PD.ReadCodeID  
  ,PD.BookNo  
 FROM CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN  
         CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
         INNER JOIN Tbl_BookNumbers BN ON PD.BookNo=BN.BookNo  
         INNER JOIN Tbl_Cycles CY ON CY.CycleId=BN.CycleId  
         INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
         INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
  
  
GO


