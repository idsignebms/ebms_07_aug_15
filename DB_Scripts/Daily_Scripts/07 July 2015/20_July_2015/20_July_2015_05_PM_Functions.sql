
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_Adjustment]    Script Date: 07/20/2015 16:58:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	Karteek
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
CREATE FUNCTION  [dbo].[fn_GetAverageReading_Adjustment]
(
 @AccountNo VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
	Declare @NewAverage decimal(18,2) = 0,@TotalRecords INT = 0
		, @TotalUsage DECIMAL(18,2) = 0
		, @NoOfMonths INT = 12
		, @TotalEnergy DECIMAL(18,2) = 0

   SELECT Top 1
          @TotalRecords = TotalReadings
         ,@TotalEnergy = TotalReadingEnergies
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
   order by CustomerReadingId desc
   
    IF(ISNULL(@TotalRecords,0) > @NoOfMonths)
		BEGIN
			DECLARE @AdjustTotalReading DECIMAL(18,2) = 0
			
			SELECT 
				@AdjustTotalReading = TotalReadingEnergies
			FROM Tbl_CustomerReadings
			WHERE GlobalAccountNumber = @AccountNo
			AND TotalReadings = (@TotalRecords - @NoOfMonths)
			
			SET @TotalEnergy = @TotalEnergy - @AdjustTotalReading
			
			IF(ISNULL(@TotalEnergy,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalEnergy,0)) / @NoOfMonths
			ELSE
				SET @NewAverage = 0
		END
    ELSE
		BEGIN
			IF(ISNULL(@TotalEnergy,0) > 0) 
				SET @NewAverage = (ISNULL(@TotalEnergy,0)) / (ISNULL(@TotalRecords,1))
			ELSE
				SET @NewAverage = 0			
		END
 
	RETURN @NewAverage

END

GO


