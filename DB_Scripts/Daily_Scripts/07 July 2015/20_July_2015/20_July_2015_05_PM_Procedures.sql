
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 07/20/2015 16:59:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
			,@Count INT
			,@TotalReadingEnergies DECIMAL(18,2)
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	IF((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)=0)
		BEGIN
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@ModifiedReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE
		END	
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
		BEGIN
			SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
		BEGIN
			SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
		END
	ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
		BEGIN
			SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
		END	
	ELSE
		BEGIN		
		
			SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)			
			
			SET @Count=(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			IF (@Count=1)
				BEGIN
					SET @TotalReadingEnergies=@Usage
				END
			ELSE
				BEGIN
					
					DECLARE @Last2ndTotalReadingEnergies DECIMAL(18,2)
					
					SELECT @Last2ndTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					WHERE TotalReadings =(SELECT MAX(TotalReadings)-1
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber=@GlobalAccountNumber
					
					--SELECT @Last1stTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
					--WHERE TotalReadings =(SELECT MAX(TotalReadings)
					--								FROM Tbl_CustomerReadings
					--								WHERE GlobalAccountNumber=@GlobalAccountNumber)
					--AND GlobalAccountNumber=@GlobalAccountNumber
					
					UPDATE Tbl_CustomerReadings
					SET IsReadingWrong = 1
					WHERE TotalReadings =(SELECT MAX(TotalReadings)
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber=@GlobalAccountNumber)
					AND GlobalAccountNumber = @GlobalAccountNumber

					SET @TotalReadingEnergies= @Last2ndTotalReadingEnergies+@Usage
				END
			
			SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
			SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																			WHERE GlobalAccountNumber=@GlobalAccountNumber)
			
			DECLARE @PreviousReadDate DATETIME
			SELECT TOP 1 @PreviousReadDate=ReadDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber
			ORDER BY CustomerReadingId DESC
			
			
			INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					--,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					--,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					--,IsReadingWrong
					--,IsRollOver
					)
				VALUES(
					 @GlobalAccountNumber
					,GETDATE()
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
					--,@AverageReading
					,0
					--,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
					,@TotalReadingEnergies
					,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
					,@Multipiler
					,2
					,@CreatedBy
					,GETDATE()
					--,@IsTamper
					,@MeterNumber
					,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
					--,@IsRollover
					--,1
					)
				
			SELECT @AverageReading = dbo.fn_GetAverageReading_Adjustment(@GlobalAccountNumber)
			
			UPDATE Tbl_CustomerReadings
			SET AverageReading = @AverageReading
			WHERE TotalReadings =(SELECT MAX(TotalReadings)
											FROM Tbl_CustomerReadings
											WHERE GlobalAccountNumber=@GlobalAccountNumber)
			AND GlobalAccountNumber = @GlobalAccountNumber
						
			INSERT INTO Tbl_PresentReadingAdjustmentLog
					(
						 GlobalAccountNumber
						,PreviousReading 
						,PresentReading 
						,NewPresentReading 
						,PreviousReadDate 
						,PresentReadDate 
						,MeterNumber 
						,CreatedBy 
						,CreatedDate
					)
			VALUES	(
						@GlobalAccountNumber
						,@PreviousReading
						,@PresentReading
						,@ModifiedReading
						,@PreviousReadDate
						,dbo.fn_GetCurrentDateTime()
						,@MeterNumber
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)
			
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
			SET InitialReading=@ModifiedReading
				,PresentReading=@ModifiedReading
			WHERE GlobalAccountNumber=@GlobalAccountNumber
			
			SELECT 1 AS IsSuccess	
			FOR XML PATH('BillingBE'),TYPE 	
		END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 07/20/2015 16:59:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================  
--AUTHOR : NEERAJ KANOJIYA  
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS  
--CREATED ON: 4-OCT-14  
--=============================================================  
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]
(  
@XmlDoc XML  
)  
AS  
 BEGIN  
 DECLARE @BatchID INT, @BU_ID VARCHAR(50)
   
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')  
	, @BU_ID  = C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)  
   
   
SELECT (
		SELECT	BA.BatchID,
				BA.BatchNo,
				BA.BatchDate,
				BA.BatchTotal,
				(SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,
				RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode
				(BA.BatchTotal-
				(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,
				convert(int, (BA.BatchTotal-
				(SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,
				(SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType
			FROM  Tbl_BATCH_ADJUSTMENT AS BA
			JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID 
				AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID
			 
			FOR XML PATH('BillAdjustments'),TYPE          
			),
			(
				SELECT	BA.BatchNO,
						BAT.Name AS AdjustmentName,
						CD.GlobalAccountNumber AS AccountNo,
						BLA.TotalAmountEffected,
						BLA.AmountEffected,
						ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,
						BLA.CreatedDate
				from Tbl_BATCH_ADJUSTMENT		AS BA
				JOIN Tbl_BillAdjustments		AS BLA	ON BA.BatchID = BLA.BatchNo 
						AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID
				JOIN Tbl_BillAdjustmentDetails	AS BAD	ON BLA.BillAdjustmentId=BAD.BillAdjustmentId
				JOIN Tbl_BillAdjustmentType		AS BAT	ON BLA.BillAdjustmentType=BAT.BATID
				JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber
				 
				FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE      
			)
			FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo]    Script Date: 07/20/2015 16:59:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo]
(
@XmlDoc Xml=null
) 
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(MAX)
	
	SELECT   @Month = C.value('(Month)[1]','INT')
			,@Year = C.value('(Year)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')			
		FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)
	
	SELECT	CD.GlobalAccountNumber,
			CD.ClassName AS Tariff,
			ISNULL(CBills.NetFixedCharges,0) AS FixedCharges,
			ISNULL(CBills.TotalBillAmount,0) AS TotaAmountBilled,			
			ISNULL(CBills.PaidAmount,0) AS TotaAmountCollected,
			ISNULL(CBills.Usage,0) AS TotalUsage,
			ISNULL(CBills.NetEnergyCharges,0) AS TotalNetEnergyCharges,
			CD.ActiveStatus AS CustomerStatus
			INTO #tmpCustomerStatistics
	FROM tbl_customerbills CBills(NOLOCK)  
	INNER  JOIN UDV_CustomerPDFReport CD(NOLOCK) ON 
	CBills.BillMonth=@Month AND CBills.BillYear=@Year AND CD.BU_ID=@BU_ID
	AND CD.GlobalAccountNumber=CBills.AccountNo

	SELECT Tariff,
		'' AS KWHSold,
		'' AS KVASold,
		SUM(FixedCharges) AS FixedCharges,
		COUNT(TotaAmountBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		SUM(TotalUsage) AS TotalUsage,
		SUM(TotalNetEnergyCharges) AS TotalNetEnergyCharges,
		'' AS NoOfStubs,
		SUM(ISNULL(Active,0) +ISNULL(Hold,0)+ISNULL(InActive,0)) as TotalPopulation,
		'' AS WeightedAverage
		INTO #tmpCustomerStatisticsList
	FROM
	(
		SELECT	 Tariff,
			COUNT(CustomerStatus) AS TotalCustomerTariff,		
			SUM(FixedCharges) AS FixedCharges,
			COUNT(TotaAmountBilled) AS NoBilled,
			SUM(TotaAmountBilled) AS TotaAmountBilled,
			SUM(TotaAmountCollected) AS TotaAmountCollected,
			SUM(TotalUsage) AS TotalUsage,
			SUM(TotalNetEnergyCharges) AS TotalNetEnergyCharges,
			CustomerStatus
		FROM #tmpCustomerStatistics
		GROUP BY CustomerStatus,Tariff
	) ListOfCustomers
	PIVOT (SUM(TotalCustomerTariff)   FOR CustomerStatus IN (Active,InActive,Hold)
	) AS  ListOfCustomersT
	GROUP BY Tariff
	ORDER BY Tariff
	
	DECLARE @tblCustomerStatisticsList AS TABLE
	(
		ID INT  IDENTITY(1,1),
		Tariff VARCHAR(200 ),
		KWHSold VARCHAR(500),
		KVASold VARCHAR(500),
		FixedCharges DECIMAL(18,2),
		NoBilled INT,
		TotalUsage DECIMAL(18,2),
		TotaAmountBilled  DECIMAL(18,2),
		TotaAmountCollected  DECIMAL(18,2),
		TotalNetEnergyCharges  DECIMAL(18,2),
		NoOfStubs VARCHAR(500),
		TotalPopulation INT,
		WeightedAverage VARCHAR(500)
	)
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage,TotalUsage,TotalNetEnergyCharges)
	SELECT 	Tariff,
		KWHSold,
		KVASold,
		FixedCharges,
		NoBilled,
		TotaAmountBilled,
		TotaAmountCollected,
		NoOfStubs,
		TotalPopulation,
		WeightedAverage,
		TotalUsage,
		TotalNetEnergyCharges
	FROM #tmpCustomerStatisticsList
	
	INSERT INTO @tblCustomerStatisticsList(Tariff,KWHSold,	KVASold,FixedCharges,NoBilled,	TotaAmountBilled,
		TotaAmountCollected,	NoOfStubs,TotalPopulation,WeightedAverage,TotalUsage,TotalNetEnergyCharges)
	SELECT 'Total' AS Tariff,
		'' AS KWHSold,
		'' AS KVASold,
		SUM(FixedCharges) AS FixedCharges,
		SUM(NoBilled) AS NoBilled,
		SUM(TotaAmountBilled) AS TotaAmountBilled,
		SUM(TotaAmountCollected) AS TotaAmountCollected,
		'' AS NoOfStubs,
		SUM(TotalPopulation) AS TotalPopulation,
		'' AS WeightedAverage,
		SUM(TotalUsage) AS TotalUsage,
		SUM(TotalNetEnergyCharges) AS TotalNetEnergyCharges		
	FROM #tmpCustomerStatisticsList
		
	SELECT	 ID AS SNo,Tariff
			,KWHSold
			,KVASold
			,CONVERT(VARCHAR(20),CAST(FixedCharges AS MONEY),-1) AS FixedCharges
			,NoBilled
			,CONVERT(VARCHAR(20),CAST(TotaAmountBilled AS MONEY),-1) AS TotalAmountBilled
			,CONVERT(VARCHAR(20),CAST(TotaAmountCollected AS MONEY),-1) AS TotalAmountCollected
			,NoOfStubs
			,TotalPopulation
			,WeightedAverage 
			,REPLACE(CONVERT(VARCHAR(20),CAST(TotalUsage AS MONEY),-1), '.00', '') AS TotalUsage
			,CONVERT(VARCHAR(20),CAST(TotalNetEnergyCharges AS MONEY),-1) AS TotalNetEnergyCharges
	FROM @tblCustomerStatisticsList
	
	DROP TABLE #tmpCustomerStatistics
	DROP TABLE #tmpCustomerStatisticsList
	
END

GO


