
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
        
  DECLARE @GlobalAccountNumber VARCHAR(50), @ActiveStatusId INT 
 
	SELECT @GlobalAccountNumber = GlobalAccountNumber, @ActiveStatusId = ActiveStatusId FROM CUSTOMERS.Tbl_CustomersDetail 
			WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo

	IF( @GlobalAccountNumber IS NOT NULL)
		BEGIN
			IF ((SELECT [dbo].[fn_IsAccountNoExists_BU_Id_Payments](@GlobalAccountNumber,@BU_ID)) = 1)
				BEGIN
						SELECT  
						 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
						,CD.OldAccountNo--Faiz-ID103
						,CD.ClassName
						,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS AccNoAndGlobalAccNo
						,CD.OutStandingAmount AS DueAmount  --Commented By Raja-ID065
						,CD.GlobalAccountNumber AS AccountNo
						,'success' AS StatusText
						,(SELECT CustomerType FROM Tbl_MCustomerTypes CT WHERE CT.CustomerTypeId=CD.CustomerTypeId) AS CustomerType
						,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,NULL,CD.Service_ZipCode) AS Address1
					FROM [UDV_CustomerDescription] CD
					WHERE GlobalAccountNumber = @GlobalAccountNumber AND (BU_ID = @BU_ID OR @BU_ID = '')
					FOR XML PATH('MastersBE')  
				END
			ELSE IF(@ActiveStatusId = 4)
				BEGIN
					SELECT 'Customer status is Closed' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
			ELSE
				BEGIN
					SELECT 'Customer doesn''t exist in selected Bussiness Unit' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
		END
	ELSE
		BEGIN
			SELECT 'Customer doesn''t exist' AS StatusText FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBU_New]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustExistsByBU_New]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	--SET	@AccountNo= '12500450'
	--SET @BUID='BEDC_BU_0003'   
	--SET	 @Flag=	 ''
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	
	SELECT @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
	FROM  UDV_IsCustomerExists WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
			IF @CustomerBusinessUnitID =  @BUID OR @BUID =''    
			BEGIN
				IF @Flag=1
				BEGIN
					--PRINT 'Sucess and No Need to check the ActiveStatusID of the Customers'
					SELECT 1 AS IsSuccess,
						@CustomerActiveStatusID AS ActiveStatusId
					FOR XML PATH('RptCustomerLedgerBe')
				END
				ELSE
				BEGIN
					IF	 @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3 
						BEGIN
							--PRINT 'Sucess and Active  Customer'
							SELECT 1 AS IsSuccess
							FOR XML PATH('RptCustomerLedgerBe') 
						END
					ELSE
						BEGIN							 
							--PRINT 'Failure In Active Status'
							SELECT 0 AS IsSuccess,
								@CustomerActiveStatusID AS ActiveStatusId
							FOR XML PATH('RptCustomerLedgerBe') 
						END
				END
			END
			ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU 
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                    
 -- Author  : Faiz-ID103                  
 -- Create date  : 24 Apr 2015                 
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT For Bill Adjustments #   
 -- =============================================                    
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo_ForAdjustment]                    
(                    
 @XmlDoc xml                    
)                    
AS                    
 BEGIN                    
 DECLARE @AccountNo VARCHAR(50)
			,@Traffid Varchar(20)    
           ,@ReadCode INT  
          ,@BU_ID VARCHAR(50) 
   
 SELECT         
   @AccountNo = C.value('(SearchValue)[1]','VARCHAR(50)') --@AccountNo = '0000057408'      
  ,@BU_ID = C.value('(BusinessUnitName)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)      
	
		DECLARE @ActiveStatusId INT
		
		SELECT @AccountNo = GlobalAccountNumber,@ActiveStatusId = ActiveStatusId FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo  
		SET @Traffid=(Select TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   
		SET @ReadCode=(Select ReadCodeId from [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)   

		DECLARE @IsCustomerExist BIT
		SET @IsCustomerExist=(SELECT dbo.fn_IsAccountNoExists_BU_Id_Payments(@AccountNo,@BU_ID))
		
		IF(@IsCustomerExist = 1)
		BEGIN
				      
				SELECT(      
				  SELECT     
							dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
							,GlobalAccountNumber AS AccountNo
							,(AccountNo+' - '+GlobalAccountNumber) AS AccNoAndGlobalAccNo
							,OldAccountNo 
							,ISNULL(MeterNumber,'--') AS MeterNo
							,ISNULL(DocumentNo,'--') AS DocumentNo
							,ClassName    
							,BusinessUnitName    
							,ServiceUnitName    
							,ServiceCenterName    
							,dbo.fn_GetCustomerBillPendings(GlobalAccountNumber) AS TotalBillPendings    
							--,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS TotalDueAmount    
							,CD.OutStandingAmount AS TotalDueAmount
							,dbo.fn_GetCustomerLastPaymentDate(GlobalAccountNumber) AS LastPaymentDate    
							,dbo.fn_GetCustomerLastPaidAmount(GlobalAccountNumber) AS LastPaidAmount    
							,dbo.fn_GetCustomerLastBillGeneratedDate(GlobalAccountNumber) AS LastBillGeneratedDate   
							,@ReadCode AS ReadCodeId
							,1 AS RowsEffected   
							FROM [UDV_CustomerDescription] CD   
							--JOIN Tbl_BussinessUnits BU ON CD.BU_ID=BU.BU_ID  
							--JOIN Tbl_ServiceUnits SU ON CD.SU_ID=SU.SU_ID  
							--JOIN Tbl_ServiceCenter BC ON CD.ServiceCenterId=BC.ServiceCenterId  
							WHERE GlobalAccountNumber = @AccountNo    
							FOR XML PATH('Customerdetails'),TYPE    
							)    
							,    
							(
							SELECT TOP(1)  
									CB.BillNo  
									,CustomerBillId AS CustomerBillID  
									,ISNULL(CB.PreviousReading,'0') AS PreviousReading  
									,ISNULL(CB.PresentReading,'0') AS PresentReading  
									,ReadType  
									,Usage AS Consumption  
									,NetEnergyCharges  
									,NetFixedCharges  
									,TotalBillAmount  
									,VAT AS Vat 
									,TotalBillAmountWithTax  
									,NetArrears  
									,TotalBillAmountWithArrears   
									,CD.GlobalAccountNumber AS AccountNo  
									,CD.OldAccountNo AS OldAccountNo
									,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
									,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName      
										   ,CD.Service_Landmark      
										   ,CD.Service_City,''      
										   ,CD.Service_ZipCode) AS ServiceAddress  
									,CAD.OutStandingAmount AS TotalDueAmount  
									,Dials AS MeterDials  
									,(CONVERT(DECIMAL(18,2),VAT) + CONVERT(DECIMAL(20,2),TotalBillAmount)) AS GrandTotal    
									,CB.PaidAmount AS TotaPayments
									,(CB.TotalBillAmountWithTax - ISNULL(CB.PaidAmount,0) - ISNULL(CB.AdjustmentAmmount,0)) AS DueBill
									,COUNT(0) OVER() AS TotalRecords  
									,BillMonth
									,BillYear
									,@ReadCode AS ReadCodeId 
									,1 AS IsSuccess
									--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadTypeIndication
									,RC.DisplayCode AS ReadTypeIndication
							FROM Tbl_CustomerBills CB  
							INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CB.AccountNo = CD.GlobalAccountNumber  
							INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
							AND CD.GlobalAccountNumber = @AccountNo AND ISNULL(CB.PaymentStatusID,2) = 2  
							INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CB.AccountNo   
							ORDER BY CB.BillGeneratedDate DESC
							FOR XML PATH('CustomerBillDetails'),TYPE                    
						)                  
						FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      

		END
		ELSE
		BEGIN
		SELECT(
		SELECT 0 AS RowsEffected, @ActiveStatusId AS ActiveStatusId
		FOR XML PATH('Customerdetails'),TYPE)
		FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')      
		END
		
		
END       

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================  
--AUTHOR : NEERAJ KANOJIYA  
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS  
--CREATED ON: 4-OCT-14  
--=============================================================  
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]
(  
@XmlDoc XML  
)  
AS  
 BEGIN  
 DECLARE @BatchID INT, @BU_ID VARCHAR(50)
   
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')  
	, @BU_ID  = C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)  
   
   
SELECT (
		SELECT	BA.BatchID,
				BA.BatchNo,
				BA.BatchDate,
				BA.BatchTotal,
				(SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,
				RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode
				(BA.BatchTotal-
				(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,
				convert(int, (BA.BatchTotal-
				(SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,
				(SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType
			FROM  Tbl_BATCH_ADJUSTMENT AS BA
			JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID AND BA.BatchStatusID = 1
				AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID
			 
			FOR XML PATH('BillAdjustments'),TYPE          
			),
			(
				SELECT	BA.BatchNO,
						BAT.Name AS AdjustmentName,
						CD.GlobalAccountNumber AS AccountNo,
						BLA.TotalAmountEffected,
						BLA.AmountEffected,
						ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,
						BLA.CreatedDate
				from Tbl_BATCH_ADJUSTMENT		AS BA
				JOIN Tbl_BillAdjustments		AS BLA	ON BA.BatchID = BLA.BatchNo AND BA.BatchStatusID = 1
						AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID
				JOIN Tbl_BillAdjustmentDetails	AS BAD	ON BLA.BillAdjustmentId=BAD.BillAdjustmentId
				JOIN Tbl_BillAdjustmentType		AS BAT	ON BLA.BillAdjustmentType=BAT.BATID
				JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber
				 
				FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE      
			)
			FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerBillPayments]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 10 Apr 2015
-- Description:	To insert the customer bill payments
-- Modified By:	Bhimaraju V
-- Modified date: 01 May 2015
-- Description:	Approvals and Logs are implemented
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerBillPayments]
(
	@XmlDoc XML
)
AS
BEGIN
	
	DECLARE @AccountNo VARCHAR(50)  
		,@ReceiptNo VARCHAR(20)  
		,@PaymentMode INT  
		,@DocumentPath VARCHAR(MAX)  
		,@DocumentName VARCHAR(MAX) 
		,@Cashier VARCHAR(50)  
		,@CashOffice INT  
		,@CreatedBy VARCHAR(50)  
		,@BatchId INT  
		,@PaidAmount DECIMAL(20,4)  
		,@PaymentRecievedDate VARCHAR(20)  
		,@CustomerPaymentId INT  
		,@EffectedRows INT  
		,@PaymentType INT
		,@IsCustomerExists BIT = 0
		,@IsSuccess BIT = 0
		,@StatusText VARCHAR(200) = ''
		,@FunctionId INT
		,@ApprovalStatusId INT
		,@IsFinalApproval BIT
		,@PaymentFromId INT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT =1
		
	SELECT @AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@ReceiptNo = C.value('(ReceiptNo)[1]','VARCHAR(20)')  
		,@PaymentMode = C.value('(PaymentModeId)[1]','INT')  
		,@DocumentPath = C.value('(DocumentPath)[1]','VARCHAR(MAX)')  
		,@DocumentName = C.value('(Document)[1]','VARCHAR(MAX)') 
		,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')  
		,@BatchId = C.value('(BatchId)[1]','INT')  
		,@PaidAmount = C.value('(PaidAmount)[1]','DECIMAL(20,4)')  
		,@PaymentRecievedDate = C.value('(PaymentRecievedDate)[1]','VARCHAR(20)')  
		,@PaymentType = C.value('(PaymentType)[1]','INT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')
		,@PaymentFromId = C.value('(PaymentFromId)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') AS T(C) 
	
	IF(@AccountNo IS NOT NULL)
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					SET @StatusText = 'Customer Payments'
					DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@PaymentLogId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
		IF EXISTS( SELECT PaymentLogId FROM Tbl_PaymentsLogs WHERE GlobalAccountNumber=@AccountNo AND ReceiptNumber=@ReceiptNo )
			BEGIN
						SET @IsCustomerExists = 0
						SET @IsSuccess = 0
						SET @StatusText = 'Payment with same Receipt number already Exists'
			END
		ELSE IF EXISTS( SELECT 0 FROM Tbl_PaymentsLogs WHERE ReceiptNumber=@ReceiptNo )
			BEGIN
						SET @IsCustomerExists = 0
						SET @IsSuccess = 0
						SET @StatusText = 'Receipt number already Exists'
			END
		ELSE IF EXISTS( SELECT 0 FROM Tbl_CustomerPayments WHERE ReceiptNo=@ReceiptNo )
			BEGIN
						SET @IsCustomerExists = 0
						SET @IsSuccess = 0
						SET @StatusText = 'Receipt number already Exists'
			END
		ELSE
			BEGIN
				
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
					END
					
				INSERT INTO Tbl_PaymentsLogs(
							GlobalAccountNumber 
							,AccountNo 
							,ReceiptNumber 
							,PaymentMode  
							,PaidAmount 
							,PadiDate  
							,PresentApprovalRole  
							,NextApprovalRole  
							,ApproveStatusId
							,CreatedBy 
							,CreatedDate
							,PaymentFromId
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,PaymentType
							,BatchNo
							,CurrentApprovalLevel
							,IsLocked
							)
				SELECT   @AccountNo
						,AccountNo
						,@ReceiptNo
						,@PaymentMode
						,@PaidAmount
						,@PaymentRecievedDate
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
						,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
						,@PaymentFromId
						,@DocumentPath
						,@Cashier
						,@CashOffice
						,@DocumentName
						,@PaymentType
						,@BatchId
						,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
						,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM UDV_CustomerDescription
				WHERE GlobalAccountNumber=@AccountNo
				
				SET @PaymentLogId = SCOPE_IDENTITY()
						
				IF(@IsFinalApproval = 1)
					BEGIN
						
						INSERT INTO Tbl_CustomerPayments
						(
							 AccountNo
							,ReceiptNo
							,PaymentMode
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,RecievedDate
							,PaymentType
							,PaidAmount
							,BatchNo
							,ActivestatusId
							,CreatedBy
							,CreatedDate
						)
						VALUES
						(
							 @AccountNo
							,@ReceiptNo
							,@PaymentMode
							,@DocumentPath
							,@Cashier
							,@CashOffice
							,@DocumentName
							,@PaymentRecievedDate
							,@PaymentType
							,@PaidAmount
							,(CASE WHEN @BatchId = 0 THEN NULL ELSE @BatchId END)
							,1
							,@CreatedBy
							,dbo.fn_GetCurrentDateTime()
						)
						
						SET @CustomerPaymentId = SCOPE_IDENTITY()
						
						DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
						
						SET @StatusText = 'Customer Pending Bills'
						INSERT INTO @PaidBills
						(
							 CustomerBillId
							,BillNo
							,PaidAmount
							,BillStatus
						)
						SELECT 
							 CustomerBillId
							,BillNo
							,PaidAmount
							,BillStatus
						FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@AccountNo,@PaidAmount) 
						
						SET @StatusText = 'Customer Bills Paymnets'
						INSERT INTO Tbl_CustomerBillPayments
						(
							 CustomerPaymentId
							,PaidAmount
							,BillNo
							,CustomerBillId
							,CreatedBy
							,CreatedDate
						)
						SELECT 
							 @CustomerPaymentId
							,PaidAmount
							,BillNo
							,CustomerBillId
							,@CreatedBy
							,dbo.fn_GetCurrentDateTime()
						FROM @PaidBills
						
						SET @StatusText = 'Customer Bills'
						UPDATE CB 
						SET CB.ActiveStatusId = PB.BillStatus
							,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
						FROM Tbl_CustomerBills CB
						INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId
						
						SET @StatusText = 'Customer Outstanding'
						UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
						SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
						WHERE GlobalAccountNumber = @AccountNo
						
						SET @StatusText= 'Audit Table'
						
						INSERT INTO Tbl_Audit_PaymentsLogs(	
							PaymentLogId
							,GlobalAccountNumber 
							,AccountNo 
							,ReceiptNumber 
							,PaymentMode  
							,PaidAmount 
							,PadiDate  
							,PresentApprovalRole  
							,NextApprovalRole  
							,ApproveStatusId  
							,Remarks 
							,CreatedBy 
							,CreatedDate  
							,ModifedBy 
							,ModifiedDate
							,PaymentFromId
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,PaymentType
							,BatchNo)  
						SELECT  
							 PaymentLogId
							,GlobalAccountNumber 
							,AccountNo 
							,ReceiptNumber 
							,PaymentMode  
							,PaidAmount 
							,PadiDate  
							,PresentApprovalRole  
							,NextApprovalRole  
							,ApproveStatusId  
							,Remarks 
							,CreatedBy 
							,CreatedDate  
							,ModifedBy 
							,ModifiedDate
							,PaymentFromId
							,DocumentPath
							,Cashier
							,CashOffice
							,DocumentName
							,PaymentType
							,BatchNo
						FROM Tbl_PaymentsLogs WHERE PaymentLogId = @PaymentLogId
						
					END
						
						SET @IsCustomerExists = 1
						SET @IsSuccess = 1
						SET @StatusText = 'Success'
						END
						
						
						
					COMMIT TRAN	
				END TRY	   
				BEGIN CATCH
					ROLLBACK TRAN
					SET @IsCustomerExists = 0
					SET @IsSuccess =0
				END CATCH			
			END
	
	SELECT  
		 @IsCustomerExists AS IsCustomerExists  
		,@IsSuccess AS IsSuccess  
		,@StatusText AS StatusText
	FOR XML PATH ('MastersBE') 
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 08 May 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@OldAccountNo VARCHAR(50)
			,@AccountNo1 VARCHAR(50)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount 
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@OldAccountNo=OldAccountNo
			,@AccountNo1 = AccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY CustomerPaymentID DESC
    
    SELECT 
    (
		SELECT CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
			,@LastPaidAmount AS LastPaidAmount
			,@GlobalAccountNo AS AccountNo
			,(@AccountNo1+' - '+@GlobalAccountNo) AS AccNoAndGlobalAccNo
			,@Name AS Name
			,@ServiceAddress AS ServiceAddress
			,@OutstandingAmount AS OutStandingAmount
			,@BookGroup AS BookGroup 
			,@BookName AS BookName
			,@Tariff AS Tariff
			,@OldAccountNo AS OldAccountNo
		FOR XML PATH('Customerdetails'),TYPE 
    )
    ,
    (
		SELECT 
			 CB.BillNo
			,CustomerBillId AS CustomerBillID
			,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
			,CB.BillYear        
			,CB.BillMonth        
			,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
			,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
			,ISNULL(CB.PreviousReading,'0') AS PreviousReading
			,ISNULL(CB.PresentReading,'0') AS PresentReading
			,Usage AS Consumption
			,Dials AS MeterDials
			,NetEnergyCharges
			,NetFixedCharges
			,TotalBillAmount
			,VAT
			,TotalBillAmountWithTax
			,NetArrears
			,TotalBillAmountWithArrears 
			,CB.AccountNo AS AccountNo
			,CB.PaidAmount AS LastPaidAmount
		FROM Tbl_CustomerBills CB 
		WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
		ORDER BY CB.CustomerBillId DESC
		FOR XML PATH('CustomerBillDetails'),TYPE 
    )
    FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')            
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateRouteActiveStatus]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-04-2014
-- Description:	To update Active status of RouteManagement
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateRouteActiveStatus]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @RouteId INT,@ActiveStatusId INT,@ModifiedBy VARCHAR(50),@CurrentDate DATETIME
	SELECT
		@RouteId=C.value('(RouteId)[1]','INT')
		,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)
	
--	SELECT @CurrentDate=dbo.fn_GetCurrentDateTime()
	
--		UPDATE Tbl_MRoutes SET ActiveStatusId=@ActiveStatusId
--							  ,ModifiedBy=@ModifiedBy
--							  ,ModifiedDate=@CurrentDate
--						   WHERE RouteId=@RouteId
	
--	SELECT 1 AS IsSuccess FOR XML PATH('MastersBE')
--END

IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE RouteSequenceNumber = @RouteId)
		BEGIN
			UPDATE Tbl_MRoutes SET ActiveStatusId=@ActiveStatusId
							  ,ModifiedBy=@ModifiedBy
							  ,ModifiedDate=@CurrentDate
						   WHERE RouteId=@RouteId
	
			SELECT 1 AS IsSuccess 
			FOR XML PATH('MastersBE')
		END
	ELSE
		BEGIN
			SELECT 0 AS IsSuccess
				,1 AS [Count]
			--FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
			--WHERE RouteSequenceNumber = @RouteId
			FOR XML PATH('MastersBE')
		END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,(CD.BookId+' ( '+BookCode+' )') AS BookCode
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,CAD.InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  ,CD.AccountNo --Faiz-ID103
  ,PMD.OutStandingAmount AS CapmiOutstandingAmount--Faiz-ID103
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  and PAD1.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  LEFT JOIN Tbl_PaidMeterDetails PMD ON PMD.AccountNo = CD.GlobalAccountNumber AND PMD.MeterNo = CD.MeterNumber--Faiz-ID103
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateBookNumberActiveStatus]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  T.Karthik      
-- Create date: 26-02-2014      
-- Description: The purpose of this procedure is to update Active status of Book Number     
-- Modified By : Bhargav G    
-- Modified Date : 17th Feb, 2015    
-- Modified Description : Restricted user to deactive the Book Number which have customers associated to it.     
-- =============================================      
ALTER PROCEDURE [dbo].[USP_UpdateBookNumberActiveStatus]      
(      
@XmlDoc xml      
)      
AS      
BEGIN      
 DECLARE @Id VARCHAR(20)    
   ,@ActiveStatusId INT    
   ,@ModifiedBy VARCHAR(50)      
  
 SELECT @Id=C.value('(BookNo)[1]','VARCHAR(20)')      
  ,@ActiveStatusId=C.value('(ActiveStatusId)[1]','INT')      
  ,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')      
 FROM @XmlDoc.nodes('MastersBE') as T(C)      
  
 IF @ActiveStatusId = 1-- For Activating book
 BEGIN
	UPDATE Tbl_BookNumbers     
		SET ActiveStatusId = @ActiveStatusId    
		 ,ModifiedBy = @ModifiedBy    
		 ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
	   WHERE BookNo=@Id  
	  
	UPDATE Tbl_Audit_BookNumbers     
		SET ActiveStatusId=@ActiveStatusId    
		 ,ModifiedBy=@ModifiedBy    
		 ,ModifiedDate=dbo.fn_GetCurrentDateTime()    
	   WHERE BookNo=@Id  
  
   SELECT 1 AS IsSuccess     
   FOR XML PATH('MastersBE')  
 END
 ELSE IF NOT EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE BookNo = @Id)  -- For Inactivating book  
  BEGIN    
   UPDATE Tbl_BookNumbers     
    SET ActiveStatusId = @ActiveStatusId    
     ,ModifiedBy = @ModifiedBy    
     ,ModifiedDate = dbo.fn_GetCurrentDateTime()    
   WHERE BookNo=@Id  
  
   UPDATE Tbl_Audit_BookNumbers     
    SET ActiveStatusId=@ActiveStatusId    
     ,ModifiedBy=@ModifiedBy    
     ,ModifiedDate=dbo.fn_GetCurrentDateTime()    
   WHERE BookNo=@Id  
  
   SELECT 1 AS IsSuccess     
   FOR XML PATH('MastersBE')    
  END    
 ELSE    
  BEGIN    
   SELECT COUNT(0) AS [Count]    
    ,0 AS IsSuccess    
   FROM UDV_CustomerDescription    
   WHERE BookNo = @Id    
   FOR XML PATH('MastersBE')    
  END    
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithMeter]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptReadCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNo
			  ,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ' ) AS BusinessUnitName
			  ,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ' ) AS ServiceUnitName
			  ,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ' ) AS ServiceCenterName
			  ,CD.ConnectionDate
			  ,(CD.CycleName +' ( '+CD.CycleCode+' ) ' ) AS CycleName
			  ,CD.MeterNumber
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  ,(CASE WHEN ISNULL(CD.PresentReading,0)=0 THEN ISNULL(CD.InitialReading,0) ELSE CD.PresentReading END ) AS PresentReading
			  --,ISNULL(CD.PresentReading,ISNULL(CD.InitialReading,0)) AS PresentReading
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=2
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  ,MeterNumber
				  ,CONVERT(VARCHAR(20),ConnectionDate,106) AS ConnectionDate
				  --,@Count As TotalRecords 
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
				  ,PresentReading
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle_NotDisabled]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Karteek 
-- Create date: 14-07-2015
-- Description: The purpose of this procedure is to get Books list By Cycle      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle_NotDisabled]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
      
 DECLARE @CycleId VARCHAR(MAX)       
   
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)      
   
 IF(@CycleId != '')      
  BEGIN      
   SELECT      
   (     
    select  B.BookNo      
      ,(CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN B.BookCode   
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
      END) AS BookNoWithDetails  
      ,COUNT(B.BookNo) over() as BookNo
       from Tbl_BookNumbers   B
    where ActiveStatusId=1 
    and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
 ELSE      
  BEGIN      
   SELECT      
   (      
    SELECT (CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN BookCode   
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')   
      END) AS BookNo        
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1      
    ORDER BY BookNo ASC      
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
       
END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoList_By_Cycle]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  V.Bhimaraju      
-- Create date: 28-08-2014      
-- Modified By: T.Karthik    
-- Modified date: 03-11-2014    
-- Description: The purpose of this procedure is to get Books list By Cycle      
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetBookNoList_By_Cycle]      
(      
 @XmlDoc xml      
)      
AS      
BEGIN      
      
 DECLARE @CycleId VARCHAR(MAX)       
   
 SELECT @CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')      
 FROM @XmlDoc.nodes('ReportsBe') AS T(C)      
   
 IF(@CycleId != '')      
  BEGIN      
   SELECT      
   (      
    --SELECT B.BookNo      
    --  ,(CASE   
    --   WHEN ISNULL(ID,'') = ''   
    --    THEN B.BookCode   
    --   ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
    --  END) AS BookNoWithDetails  
    --  ,COUNT(CPD.BookNo)  
    --FROM Tbl_BookNumbers B , CUSTOMERS.Tbl_CustomerProceduralDetails CPD  
    --WHERE B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))    
    --AND B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1)  
    --AND B.ActiveStatusId=1    AND CPD.BookNo=B.BookNo  
    --GROUP BY   
    --B.BookNo,ID,B.BookCode  
    --HAVING COUNT(CPD.BookNo)>0  
      
    --ORDER BY B.BookNo ASC      
    
    
    select  B.BookNo      
      ,(CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN B.BookCode   
       ELSE ( ISNULL(ID,'') + ' ( '+B.BookCode + ')')   
      END) AS BookNoWithDetails  
      ,COUNT(B.BookNo) over() as BookNo
       from Tbl_BookNumbers   B
    where ActiveStatusId=1 
    --and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND ISNULL(IsPartialBill,0) = 0)  
    --and B.BookNo NOT IN (SELECT BookNo FROM Tbl_BillingDisabledBooks WHERE IsActive=1 AND IsPartialBill=1)  
    and B.CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
 ELSE      
  BEGIN      
   SELECT      
   (      
    SELECT (CASE   
       WHEN ISNULL(ID,'') = ''   
        THEN BookCode   
       ELSE ( ISNULL(Details,'') + ' ( '+BookCode+ ')')   
      END) AS BookNo        
    FROM Tbl_BookNumbers WHERE ActiveStatusId=1      
    ORDER BY BookNo ASC      
    FOR XML PATH('Reports'),TYPE      
   )      
   FOR XML PATH(''),ROOT('ReportsBeInfoByXml')      
  END      
       
END  
  
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetPaymets]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 01-Aug-2014
-- Description:	Get The Details of PaymentsReports list
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetPaymets]
(
	@XmlDoc xml
)
AS
BEGIN
		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)	
			,@CycleId VARCHAR(MAX)
			,@FromDate VARCHAR(20)
			,@ToDate VARCHAR(20)
			,@DeviceId VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')
			,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')
			,@DeviceId = C.value('(ReceivedDeviceId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPaymentsBe') AS T(C)
		
	IF ISNULL(@FromDate,'') =''    
	BEGIN    
		SET @FromDate = dbo.fn_GetCurrentDateTime() - 60    
	END 
	IF ISNULL(@Todate,'') =''
	BEGIN    
		SET @Todate = dbo.fn_GetCurrentDateTime()    
	END 
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@DeviceId = '')
		BEGIN
			SELECT @DeviceId = STUFF((SELECT ',' + CAST(BillingTypeId AS VARCHAR(50)) 
					 FROM Tbl_MBillingTypes(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY CP.RecievedDate DESC ,CD.GlobalAccountNumber) AS RowNumber
			,(CD.AccountNo + ' - ' + CD.GlobalAccountNumber) AS GlobalAccountNumber
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
			,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
			,(CD.ServiceCenterName +' ( '+CD.SUCode+' ) ') AS ServiceCenterName
			,(CD.ServiceUnitName +' ( '+CD.SCCode+' ) ') AS ServiceUnitName
			,(CD.CycleName +' ( '+CD.CycleCode+' ) ') AS CycleName
			,CD.OldAccountNo
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,ISNULL(BD.BatchNo,0) AS BatchNo
			,BT.BillingType AS ReceivedDevice
			,ISNULL(CP.PaidAmount,0) AS PaidAmount
			,ISNULL(CONVERT(VARCHAR(50),CP.RecievedDate,107),'--') AS ReceivedDate
			--,(SELECT dbo.fn_GetCustomerTotalDueAmount(CD.GlobalAccountNumber)) AS TotalDueAmount
			,CD.OutStandingAmount AS TotalDueAmount
		FROM [UDV_CustomerDescription] CD(NOLOCK)	
		INNER JOIN Tbl_CustomerPayments CP(NOLOCK) ON CP.AccountNo = CD.GlobalAccountNumber
				AND CP.RecievedDate BETWEEN @FromDate AND @ToDate
		INNER JOIN Tbl_MBillingTypes BT(NOLOCK) ON CP.ReceivedDevice = BT.BillingTypeId
		INNER JOIN Tbl_BatchDetails BD (NOLOCK) ON BD.BatchID = CP.BatchNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS DeviceId FROM dbo.fn_Split(@DeviceId,',')) DG ON DG.DeviceId = CP.ReceivedDevice
		
	)
	
	SELECT	 
		 RowNumber
		,GlobalAccountNumber AS AccountNo
		,Name
		,ServiceAddress
		,BusinessUnitName
		,ServiceCenterName
		,ServiceUnitName
		,CycleName
		,BookNumber
		,CustomerSortOrder
		,BookSortOrder
		,ReceivedDevice
		,OldAccountNo
		--,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,PaidAmount
		,BatchNo
		,ReceivedDate
		--,CONVERT(VARCHAR(20),CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
		,TotalDueAmount
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(TotalDueAmount) FROM PagedResults) AS MONEY),-1) AS TotalDue
		,(Select COUNT(0) from PagedResults) as TotalRecords
		,CONVERT(VARCHAR(20),CAST((SELECT SUM(PaidAmount) FROM PagedResults) AS MONEY),-1) AS TotalReceivedAmount
	FROM PagedResults p
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgUploadedCustoemrs]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,(CD.CycleName+' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CONVERT(INT,DAVG.AverageReading,0) AS Usage
	FROM
	UDV_PrebillingRpt CD(NOLOCK)
	INNER JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG 
	ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	and  CD.ReadCodeID = 1 
	AND CD.ActiveStatusId=1  and isnull(CD.IsPartialBook,0)=0 
	-- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetAvgNotUploadedCustoemrs]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.ClassName
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress AS ServiceAddress
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,(CD.CycleName +' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode +' ) ') AS BookNumber
		,CD.SortOrder AS BookSortOrder
		,CD.MeterNumber AS MeterNo
		,CD.InitialBillingKWh
	FROM
	UDV_PrebillingRpt(NOLOCK)  CD
	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID   and  CD.ReadCodeID = 1	AND CD.ActiveStatusId=1  
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
	 AND  isnull(CD.BoolDisableTypeId,0)=0

	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DAVG ON DAVG.GlobalAccountNumber = CD.GlobalAccountNumber
	
	WHERE isnull(DAVG.AvgReadingId,0)=0 
       -- Disable books and	   Active Customers


END
------------------------------------------------------------------------------------



GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetPartialBillCustomers]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Satya.K/Karteek
-- Create date: 30-03-2015
-- Description:	
/*				Partial bill Customers

--Embessy Customers - No VAT
--Book Disable -- > Temparary Close -- Only Entery Chages Consider
--		--> Is partial -- Only Enegy Charges
--Customer Status -->In Actve == Only Fixed  Charges

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetPartialBillCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(max)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(max)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(max)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(max)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
 	Create table #PartialBillCustomers 
 	(
 	 GlobalAccountNumber varchar(50)
 	,OldAccountNo varchar(50)
 	,MeterNo    varchar(50)
 	,ClassName  varchar(50)
 	,Name    varchar(MAX)
 	,ServiceAddress	 varchar(MAX)
 	,BusinessUnitName  varchar(200)
 	,ServiceUnitName	varchar(200)   
 	,ServiceCenterName varchar(200)
 	,CustomerSortOrder int
 	,CycleName	  varchar(100)
 	,BookNumber	 varchar(100)    
 	,BookSortOrder int 
 	,Comments varchar(MAX)   
 	)
 	
 insert into #PartialBillCustomers
	SELECT	    
		(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		 ,CustomerFullName AS Name 
	 	,ServiceAddress AS ServiceAddress
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName+' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,(CD.CycleName+' ( '+CD.CycleCode+' ) ') AS CycleName
	 	,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder
		,(CASE WHEN	ISNULL(CD.IsEmbassyCustomer,0) = 1  THEN 'Embassy Customers - Only Fixed Charges' ELSE 'In Active Customer - Only Fixed Charges' END) AS Comments
	FROM  UDV_PrebillingRpt(NOLOCK) CD
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
 	WHERE (ISNULL(CD.IsEmbassyCustomer,0) = 1 OR CD.CustomerStatusID = 2)
	
	
	 
	insert into #PartialBillCustomers
	SELECT 
		 (CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,CD.ClassName
		,CustomerFullName AS Name 
		,ServiceAddress AS ServiceAddress
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName+' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,(CD.CycleName+' ( '+CD.CycleCode+' ) ') AS CycleName
	 	,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder
		,'Book : "' + CD.BookNo + '" Disabled (Temp / Partial) - Only Enegy Charges'  As Comments
	FROM UDV_PrebillingRpt CD
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
	 
	INNER JOIN Tbl_BillingDisabledBooks BD ON  ISNULL(CD.BookNo,'') = BD.BookNo AND DisableTypeId = 2 
	AND BD.IsPartialBill = 1
 
	
	SELECT * from #PartialBillCustomers
	
	DROP table #PartialBillCustomers
END
---------------------------------------------------------------------------------------------




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Satya.K
-- Create date: 30-03-2015
-- Description:	
/*				1.High Low Estimated Customers

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetHighLowEstimatedCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)
	
	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CBILLS.AccountNo,CBILLS.AverageReading as  LastBillAverage,CBILLS.Usage, 
	CD.GlobalAccountNumber,CD.AccountNo AS AccNo,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.MeterNumber AS MeterNo
		,CD.SortOrder AS CustomerSortOrder
		,(CD.CycleName +' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder 
		 INTO #CustomerBillswithAverage
		 from   Tbl_CustomerBills(NOLOCK)	CBILLS
	INNER JOIN  UDV_PreBillingRpt(NOLOCK) CD ON CBILLS.AccountNo = CD.GlobalAccountNumber 
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
		
	
	SELECT (CD.AccNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo,CD.MeterNumber,CD.ClassName,
		CD.Name 
		,CD.ServiceAddress AS ServiceAddress
		,MIN(CR.PreviousReading) AS PreviousReading 
		,MAX (CR.ReadDate) AS PreviousReadDate
		,MAX(CR.PresentReading) AS	 PresentReading
		,CAST(SUM(CR.usage) AS INT) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(CR.CreatedDate) AS LatestTransactionDate
		,CreatedBy AS TransactionLog
		,CR.AverageReading 
		,CD.BusinessUnitName
		,CD.ServiceUnitName
		,CD.ServiceCenterName
		,CD.CustomerSortOrder 
		,CD.CycleName
		,BookNumber
		,CD.MeterNo
		,CD.BookSortOrder 
	FROM Tbl_CustomerReadings CR
	INNER JOIN #CustomerBillswithAverage CD ON	   CD.GlobalAccountNumber=CR.GlobalAccountNumber
	GROUP BY CD.GlobalAccountNumber,CD.OldAccountNo,
		CD.MeterNumber,CD.ClassName,
		CD.Name,
		CD.ServiceAddress
		,AverageReading
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CD.BookSortOrder
		,BookNumber
		,BookSortOrder
		,CycleName
		, CD.Usage
		,CustomerSortOrder
		,CD.AccNo 
		,CD.MeterNo
		,CreatedBy
    HAVING 
	CAST(SUM(CR.usage) AS DECIMAL(18,2)) 
	between   
	 CD.Usage- ((30*CONVERT(Decimal(18,2),AverageReading)/100))
	 and
	 CD.Usage +((30*CONVERT(Decimal(18,2),AverageReading)/100))
    
    DELETE FROM		 #CustomerBillswithAverage
 	
END
----------------------------------------------------------------------------------------------------
 




GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNoBillsCustomers]    Script Date: 07/21/2015 19:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				7.No Bills Customers

CustomerStatus - Hold , Closed
Customertyppe(meter Type) - Prepaid Customers
BookDisable - No Power 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNoBillsCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	DECLARE @CustomerStatus VARCHAR(50) 
	DECLARE @MeterTypes VARCHAR(50)
	DECLARE @BookDisableType VARCHAR(50) 
	SET	@CustomerStatus = '3,4' -- Hold, Closed Customers
	SET	@MeterTypes = '1' -- PrepaidCustomers
	SET	@BookDisableType = '1'

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	--Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address,
	SELECT   
		(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder,
		(CD.BusinessUnitName+' ( '+CD.BUCode+' ) ') AS BusinessUnitName,
		(CD.ServiceUnitName+' ( '+CD.SUCode+' ) ') AS ServiceUnitName,
		(CD.ServiceCenterName+' ( '+CD.SCCode+' ) ') AS ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress AS ServiceAddress
		,
		CASE
		when   CustomerStatus.CustomerStatusID=3 then convert(varchar(max),'Hold Customer') else
		case when CustomerStatus.CustomerStatusID=4 then convert(varchar(max),'Closed Customer') 
		else convert(varchar(max),'Pre-Paid Customer')
		end
		END
		AS Comments
		,(CD.CycleName+' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder
	INTO  #NoBillCustomersList
	FROM  UDV_PrebillingRpt(NOLOCK) CD	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID
 	INNER JOIN (SELECT [com] AS CustomerStatusID FROM dbo.fn_Split(@CustomerStatus,',')) CustomerStatus 
 	ON CustomerStatus.CustomerStatusID = CD.CustomerStatusID
	LEFT JOIN Tbl_MeterInformation(NOLOCK) MI ON CD.MeterNumber = MI.MeterNo and MI.MeterType = 1
	WHERE
	  (CustomerStatus.CustomerStatusID IS NUll OR  MI.MeterType IS NULL)
	
	insert into #NoBillCustomersList 
	SELECT  
		(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber,CD.OldAccountNo,CD.MeterNumber AS MeterNo,
		CD.ClassName,CD.SortOrder AS CustomerSortOrder
		,(CD.BusinessUnitName+' ( '+CD.BUCode+' ) ') AS BusinessUnitName,
		(CD.ServiceUnitName+' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName+' ( '+CD.SCCode+' ) ') AS ServiceCenterName,
		CustomerFullName AS Name ,
		ServiceAddress  AS ServiceAddress
		,'BookDisable No Power' AS Comments
		,(CD.CycleName+' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder
 
	FROM UDV_PrebillingRpt(NOLOCK) CD
	INNER JOIN Tbl_BillingDisabledBooks(NOLOCK) BD ON ISNULL(CD.BookNo,0) = BD.BookNo
	AND BD.DisableTypeId IN(SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,','))
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID	--INNER JOIN (SELECT [com] AS BookDisableTypeID FROM dbo.fn_Split(@BookDisableType,',')) BookDisableType ON BookDisableType.BookDisableTypeID = BD.DisableTypeId     
  SELECT *		 FROm #NoBillCustomersList
END
--------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*3.Zero Usage Customers [Minimum Billing Customers]-- Direct Customers

Global Account No,Old Account No,Tariff,Customer Name,Service Address 


*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetZeroUsageCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)


	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END


	SELECT  (CD.AccountNo+' - '+ CD.GlobalAccountNumber) AS GlobalAccountNumber,
			CD.OldAccountNo,
			CD.ClassName,
			CD.CustomerFullName AS Name,
			CD.ServiceAddress,
			(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName,
			(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName,
			(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName,
			(CD.CycleName +' ( '+CD.CycleCode+' ) ') AS CycleName,
			(CD.BookId + ' ( ' + CD.BookCode +' ) ') AS BookNumber,
			CD.SortOrder AS CustomerSortOrder,
			CD.BookSortOrder
	FROM  UDV_PrebillingRpt	CD(NOLOCK)
	Inner join
	Tbl_CustomerReadings(NOLOCK)  CR
	ON CR.GlobalAccountNumber=CD.GlobalAccountNumber
	and IsBilled=0 and Usage=0
 	 INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	 ON   CD.ServiceCenterId =Service_Centers.SC_ID
 	
END
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetNonReadCustomers]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*4.Non-Read Customer 

Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
Usage,Mulitiplier, Average Reading, 
Previous read Date, Present read Date, 
User (Created By), 
Transaction Date (Created Date), Comments 

*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetNonReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

		DECLARE  @Service_Units VARCHAR(MAX) = ''
				,@Business_Units VARCHAR(MAX) = ''
				,@Service_Centers VARCHAR(MAX) = ''
			 

		SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
				,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
				,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

 

SELECT	(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.MeterNumber AS MeterNo
		,CD.ClassName
		,CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,CR.PreviousReading AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading AS PresentReading
		,CD.InitialBillingKWh AS Usage
		, CR.CreatedDate
		,CR.CreatedBy
		,CR.ReadDate
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,(CD.CycleName +' ( '+CD.CycleCode+' ) ' ) AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder
	FROM  UDV_PrebillingRpt CD(NOLOCK)
	
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON CD.ServiceCenterId =Service_Centers.SC_ID
	AND ActiveStatusId=1  and isnull(IsPartialBook,1)= 1
	and CustomerTypeId <>3
	AND CD.ReadCodeID = 2 
	LEFT JOIN Tbl_CustomerReadings CR(NOLOCK) ON
	CD.GlobalAccountNumber = CR.GlobalAccountNumber AND CR.IsBilled = 0
	WHERE   CR.CustomerReadingId IS NULL 
	
END
--------------------------------------------------------------------------------------------






GO

/****** Object:  StoredProcedure [dbo].[USP_RptPreBilling_GetReadCustomers]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Satya.K
-- Create date: 27-03-2015
-- Description:	
/*				1.Read Customer Report

					Global Account No, Old Account No, Meter No, Tariff, Customer Name, Service Address, 
					Previous Reading, Present Reading, Usage, Average Reading, 
					Previous read Date, Present read Date, 
					User (Created By), 
					Transaction Date (Created Date)

Modified Date : 09-04-2015

Description : 
In Active,Closed,Hold,Tempary CLose  Customers ,DIRECT Customer , NoPower Also- No Need to Come

 Hold -- 
*/
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptPreBilling_GetReadCustomers]
(
	@XmlDoc Xml=null
)
AS
BEGIN	
SET NOCOUNT ON;

 
	DECLARE  @Service_Units VARCHAR(MAX) = ''
			,@Business_Units VARCHAR(MAX) = ''
			,@Service_Centers VARCHAR(MAX) = ''
			,@ActiveStatusIds varchar(max)='2,3,4'
		
	SELECT   @Business_Units=C.value('(BU_ID)[1]','VARCHAR(50)')
			,@Service_Units=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@Service_Centers=C.value('(ServiceCenterId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('RptPreBillingBe') AS T(C)

	IF(@Business_Units = '')
		BEGIN
			SELECT @Business_Units = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Units = '')
		BEGIN
			SELECT @Service_Units = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@Business_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Service_Centers = '')
		BEGIN
			SELECT @Service_Centers = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@Service_Units,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END

	SELECT CD.GlobalAccountNumber,CD.AccountNo,CD.OldAccountNo,CD.MeterNumber As MeterNumber,CD.ClassName,
		 CD.CustomerFullName AS Name 
		,CD.ServiceAddress AS ServiceAddress
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,CD.SortOrder AS CustomerSortOrder
		,CR.PreviousReading  AS PreviousReading 
		,CR.ReadDate AS PreviousReadDate
		,CR.PresentReading  AS	 PresentReading
		,CAST(ISNULL(CR.Usage,0) AS BIGINT)  AS Usage
		,CR.CreatedDate
		,UD.Name AS CreatedUSerName
		,CR.ReadDate
		,UD.CreatedBy AS  CreatedBy
		,(CD.CycleName+' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId + ' ( ' + CD.BookCode+' ) ') AS BookNumber
		,CD.BookSortOrder
		,CR.CustomerReadingId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings(NOLOCK) CR
	INNER JOIN UDV_PreBillingRpt(NOLOCK) CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber AND CR.IsBilled = 0
	AND ActiveStatusId=1  and isnull(IsPartialBook,1)= 1-- Disable books and	   Active Customers
	and CD.CustomerTypeId <>3
	INNER JOIN 
	(SELECT [com]    AS BU_ID FROM dbo.fn_Split(@Business_Units,','))    Business_Units
	ON Business_Units.BU_ID=CD.BU_ID
	INNER JOIN 
	(SELECT [com] AS SU_Id FROM dbo.fn_Split(@Service_Units,','))   Service_Units
	ON Service_Units.SU_ID=CD.SU_ID
	INNER JOIN 
	(SELECT [com] AS SC_ID FROM dbo.fn_Split(@Service_Centers,','))	  Service_Centers
	ON    CD.ServiceCenterId =Service_Centers.SC_ID 
	INNER JOIN Tbl_UserDetails(NOLOCK) UD ON UD.UserId = CR.CreatedBy
    and ISNULL(CD.IsPartialBook,1)=1

	SELECT (Tem.AccountNo+' - '+Tem.GlobalAccountNumber) AS GlobalAccountNumber
		,Tem.OldAccountNo
		,Tem.MeterNumber AS MeterNo
		,Tem.ClassName
		,Tem.Name
		,Tem.ServiceAddress
		,(select top 1 PreviousReading from #ReadCustomersList RL where RL.GlobalAccountNumber=Tem.GlobalAccountNumber order by CustomerReadingId ASC) AS PreviousReading 
		,MAX (Tem.ReadDate) AS PreviousReadDate
		,(select top 1 PresentReading from #ReadCustomersList RL where RL.GlobalAccountNumber=Tem.GlobalAccountNumber order by CustomerReadingId Desc) AS PresentReading 
 		,SUM(usage) AS Usage
		,COUNT(0) AS TotalReadings
		,MAX(Tem.CreatedDate) AS LatestTransactionDate
		,(SELECT STUFF((SELECT ISNULL(CAST(PreviousReading AS VARCHAR(50)),'--') + '-' + 
			ISNULL(CAST(PresentReading AS VARCHAR(50)),'--')+'='+
			ISNULL(CAST(Usage AS VARCHAR(50)),'--') +'[ '+CreatedBy+ ':' +CONVERT(VARCHAR(100),CreatedDate,100)+' ]'  + ' \n '
			FROM #ReadCustomersList  WHERE GlobalAccountNumber = Tem.GlobalAccountNumber
			FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,0,'')	)  as TransactionLog
		,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
	FROM #ReadCustomersList Tem
	GROUP BY Tem.GlobalAccountNumber,Tem.OldAccountNo,
		Tem.MeterNumber,Tem.ClassName,
		Tem.Name,
		Tem.ServiceAddress,Tem.BusinessUnitName
		,Tem.ServiceUnitName
		,Tem.ServiceCenterName
		,Tem.CustomerSortOrder
		,Tem.CycleName
		,Tem.BookNumber
		,Tem.BookSortOrder
		,Tem.AccountNo
	DROP TABLE #ReadCustomersList		 

END
----------------------------------------------------------------------------------------------





GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetNoUsageCustomersReport]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		T.Karthik
-- Create date: 04-11-2014
-- Description:	The purpose of this procedure is to get No Usage Customers Report
-- Modified By	: Neeraj Kanojiya
-- Modified Date: 11-Nov-14
-- Modified By: Padmini
-- Modified Date:17-Feb-2015
-- Description:	Getting it from Bookno,Cycle,SC,SU & BU order
-- Modified By: Karteek
-- Modified Date:31-Mar-2015
-- Modified Date:11-05-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetNoUsageCustomersReport]
(
	@XmlDoc xml
)
AS
BEGIN
	DECLARE @BUID VARCHAR(MAX)
		,@SUID VARCHAR(MAX)
		,@SCID VARCHAR(MAX)
		,@Cycles VARCHAR(MAX)
		,@BookNos VARCHAR(MAX)
		--,@FromDate VARCHAR(50)
		--,@ToDate VARCHAR(50)
		,@PageSize INT  
		,@PageNo INT  
		,@YearId INT  
		,@MonthId INT  
		
	SELECT
		@BUID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@SUID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@SCID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
		,@Cycles=C.value('(CycleId)[1]','VARCHAR(MAX)')
		,@BookNos=C.value('(BookNo)[1]','VARCHAR(MAX)')
		--,@FromDate=C.value('(FromDate)[1]','VARCHAR(50)')
		--,@ToDate=C.value('(ToDate)[1]','VARCHAR(50)')
		,@PageNo = C.value('(PageNo)[1]','INT')  
		,@PageSize = C.value('(PageSize)[1]','INT')  
		,@YearId = C.value('(YearId)[1]','INT')  
		,@MonthId = C.value('(MonthId)[1]','INT')  
	FROM @XmlDoc.nodes('RptNoUsageConsumedBe') as T(C)

	--IF ISNULL(@FromDate,'') = ''
	--BEGIN  
	--	SET @FromDate = dbo.fn_GetCurrentDateTime() - 60 
	--END 
	--IF ISNULL(@Todate,'') = ''
	--BEGIN  
	--	SET @Todate = dbo.fn_GetCurrentDateTime()  
	--END 
	
	IF(@BUID = '')
		BEGIN
			SELECT @BUID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SUID = '')
		BEGIN
			SELECT @SUID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits (NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SCID = '')
		BEGIN
			SELECT @SCID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SUID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@Cycles = '')
		BEGIN
			SELECT @Cycles = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SCID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNos = '')
		BEGIN
			SELECT @BookNos = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@Cycles,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
	;WITH LIST AS
	(	
		SELECT 
			 ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber) AS RowNumber 
			,(GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
			,CD.AccountNo
			,CD.OldAccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) AS ServiceAddress
			,MeterNumber AS MeterNo
			,CONVERT(VARCHAR(25),CB.BillGeneratedDate,107) AS BillGeneratedDate
			,CD.ClassName AS Tariff
			,ISNULL(CB.NetFixedCharges,0) AS AdditionalCharges
			,CB.TotalBillAmountWithArrears AS TotalBilledAmount
			,RC.ReadCode
			,CD.SortOrder AS CustomerSortOrder
			,CD.BookSortOrder
			,(CD.CycleName +' ( '+CD.CycleCode+' ) ')  AS CycleName
			,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ') AS BusinessUnitName
			,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
			,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ' ) AS ServiceCenterName
			,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			,(CD.BookId + ' - ' + CD.BookCode) AS BookCode
			,ISNULL(CD.OutStandingAmount,0) AS OutStandingAmount
			,COUNT(0) OVER() AS TotalRecords
		FROM [UDV_CustomerDescription](NOLOCK) AS CD
		INNER JOIN Tbl_CustomerBills AS CB(NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo AND ISNULL(CB.Usage,0) = 0 --AND CD.ActiveStatusId IN(1,2) 
				AND CD.ActiveStatusId = 1 AND CD.CylceActiveStatusId = 1 
				--AND (CD.CreatedDate BETWEEN @FromDate AND @ToDate)
				AND BillMonth = @MonthId AND BillYear = @YearId
		INNER JOIN Tbl_MReadCodes AS RC(NOLOCK) ON CB.ReadCodeId = RC.ReadCodeId
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BUID,',')) BU ON BU.BU_ID = CD.BU_ID 					
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SUID,',')) SU ON SU.SU_Id = CD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SCID,',')) SC ON SC.SC_ID = CD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) BG ON BG.CycleId = CD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNos,',')) BN ON BN.BookNo = CD.BookNo
	)

	SELECT 
		 RowNumber
		,GlobalAccountNumber
		,AccountNo
		,OldAccountNo
		,Name
		,ServiceAddress 
		,MeterNo
		,BillGeneratedDate
		,Tariff
		,CONVERT(VARCHAR(20),CAST(AdditionalCharges AS MONEY),-1) AS AdditionalCharges
		,CONVERT(VARCHAR(20),CAST(TotalBilledAmount AS MONEY),-1) AS TotalBilledAmount
		,ReadCode
		,CustomerSortOrder
		,BookSortOrder
		,CycleName 
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,BookNumber
		,BookCode
		,CONVERT(VARCHAR(20),CAST(OutStandingAmount AS MONEY),-1) AS OutStandingAmount
		,TotalRecords
	FROM LIST
	WHERE RowNumber BETWEEN (@PageNo-1) * @PageSize + 1 AND @PageNo * @PageSize

END
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillsDetails]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Suresh Kumar D  
-- Create date: 22-05-2014  
-- Modified By : T.Karthik
-- Modified Date : 15-10-2014
-- Description: The purpose of this procedure is to Calculate the Bill Generation 
-- Modified By : Karteek
-- Modified Date : 31-03-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillsDetails]  
(  
	@XmlDoc XML  
)  
AS  
BEGIN 
	
	DECLARE @Months VARCHAR(MAX)
		,@Years VARCHAR(MAX)
		,@ReadTypeId INT 
		,@Bu_Ids VARCHAR(MAX) --= 'BEDC_BU_0024' 
		,@Su_Ids VARCHAR(MAX) --= 'BEDC_SU_0140,BEDC_SU_0153'
		,@Cycles VARCHAR(MAX) --= 'BEDC_C_0844,BEDC_C_0841'
		,@TariffIds VARCHAR(MAX) --= '2,3,4,5,7,8,9'
		,@PageNo INT --= 1
		,@PageSize INT --= 100  
		
	SELECT  
		 @BU_IDs = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@Su_Ids = C.value('(SU_ID)[1]','VARCHAR(MAX)')
		,@Cycles = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffIds = C.value('(TariffIds)[1]','VARCHAR(MAX)')  
		,@Months = C.value('(Months)[1]','VARCHAR(MAX)')  
		,@Years = C.value('(Years)[1]','VARCHAR(MAX)')  
		,@ReadTypeId = C.value('(BillProcessTypeId)[1]','INT')  
		,@PageNo = C.value('(PageNo)[1]','INT')    
		,@PageSize = C.value('(PageSize)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerBillsBe') as T(C)  		    
   
	SELECT   
		 IDENTITY(INT, 1,1) AS RowNumber
		,CB.BillNo  
		,COUNT(0) OVER () AS TotalRecords
		,( CD.AccountNo  + ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
		,CD.OldAccountNo
		,CD.CustomerFullName AS Name
		,CD.ServiceAddress 
		,CD.MeterNumber AS MeterNo
		,CB.TariffId
		,T.ClassName AS TariffName
		,CB.NetEnergyCharges
		,CB.NetFixedCharges
		,CB.TotalBillAmount
		,CB.VAT
		,CB.VATPercentage
		,CONVERT(DECIMAL(18,2),CB.TotalBillAmountWithTax) AS TotalBillAmountWithTax
		,CB.NetArrears
		,CB.TotalBillAmountWithArrears
		,(CASE CD.ReadCodeID WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadCode
		,CB.Usage
		--,CB.PaidAmount
		--,CONVERT(VARCHAR(20),CB.PaymentLastDate,106) AS PaymentLastDate
		,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS PaidAmount
		,dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber) AS PaymentLastDate
		,CONVERT(VARCHAR(20),CB.BillGeneratedDate,106) AS ReadDate 
		,CB.BillYear
		,CB.BillMonth
		,BT.BillingType AS BillProcessType
		,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ' ) AS BusinessUnitName
		,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ') AS ServiceUnitName
		,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
		,(CD.CycleName +' ( '+CD.CycleCode+' ) ') AS CycleName
		,(CD.BookId +' ( '+CD.BookCode+' ) ' ) AS BookNumber
		,CD.ActiveStatus
	INTO #CustomerBills
	FROM Tbl_CustomerBills CB  (NOLOCK)
	INNER JOIN Tbl_MBillingTypes BT (NOLOCK) ON CB.BillingTypeId = BT.BillingTypeId AND
		(CB.BillingTypeId = @ReadTypeId OR @ReadTypeId = 0)
	INNER JOIN UDV_CustomerPDFReport CD (NOLOCK) ON CD.GlobalAccountNumber = CB.AccountNo 
	INNER JOIN Tbl_MTariffClasses T (NOLOCK) ON T.ClassID = CB.TariffId   
	INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@Bu_Ids,',')) BU ON BU.BU_ID = CB.BU_ID 
	INNER JOIN (SELECT [com] AS SU_ID FROM dbo.fn_Split(@Su_Ids,',')) SU ON SU.SU_ID = CB.SU_ID
	INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@Cycles,',')) Cycles ON Cycles.CycleId = CB.CycleId
	INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffIds,',')) TariffIds ON TariffIds.TariffId = CB.TariffId
	INNER JOIN (SELECT [com] AS BillYear FROM dbo.fn_Split(@Years,',')) Years ON Years.BillYear = CB.BillYear
	INNER JOIN (SELECT [com] AS BillMonth FROM dbo.fn_Split(@Months,',')) Months ON Months.BillMonth = CB.BillMonth		   
	ORDER BY CB.AccountNo ASC


	SELECT   
		 RowNumber 
		,GlobalAccountNumber 
		,OldAccountNo 	
		,Name      
		,ServiceAddress
		,MeterNo
		,CONVERT(VARCHAR(20),CAST(NetEnergyCharges AS MONEY),-1) AS NetEnergyCharges
		,CONVERT(VARCHAR(20),CAST(NetFixedCharges AS MONEY),-1) AS NetFixedCharges
		,CONVERT(VARCHAR(20),CAST(TotalBillAmount AS MONEY),-1) AS TotalBillAmount
		,CONVERT(VARCHAR(20),CAST(VAT AS MONEY),-1) AS VAT
		,CONVERT(VARCHAR(20),CAST(VATPercentage AS MONEY),-1) AS VATPercentage
		,BillNo    
		,TariffId  
		,TariffName  
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithTax AS MONEY),-1) AS TotalBillAmountWithTax  
		,CONVERT(VARCHAR(20),CAST(NetArrears AS MONEY),-1) AS NetArrears
		,CONVERT(VARCHAR(20),CAST(TotalBillAmountWithArrears AS MONEY),-1) AS TotalBillAmountWithArrears
		,CONVERT(INT,Usage) AS Usage
		,ReadCode
		,ReadDate  
		,PaymentLastDate
		,BillMonth  
		,BillYear   
		,CONVERT(VARCHAR(20),CAST(PaidAmount AS MONEY),-1) AS PaidAmount
		,BillProcessType
		,TotalRecords
		,0 AS BookSortOrder
		,0 AS CustomerSortOrder
		,BusinessUnitName
		,ServiceUnitName
		,ServiceCenterName
		,CycleName
		,BookNumber
		,ActiveStatus
	FROM #CustomerBills 
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize 
	 
	DROP TABLE #CustomerBills

END
 

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomerLedgers]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
-- =============================================  
-- Author:  Karteek
-- Create date: 09 May 2015
-- Description: The purpose of this procedure is to Get the Customers Ledger Report by account no
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomerLedgers]
(  
	@XmlDoc XML  
)  
AS  
BEGIN  
	
	Declare @AcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@ReportMonths INT
		,@CurrentDate DATETIME 
		,@BUID VARCHAR(50)=''
		,@GlobalAcountNo VARCHAR(50)

	SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()	 

	SELECT  
		  @AcountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)')   
		  ,@ReportMonths = C.value('(ReportMonths)[1]','INT') 
		  ,@BUID=C.value('(BU_ID)[1]','VARCHAR(50)')  
	FROM @XmlDoc.nodes('LedgerBe') as T(C)  
	
	SELECT @GlobalAcountNo = GlobalAccountNumber FROM [UDV_CustomerDescription](NOLOCK) CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND (BU_ID=@BUID OR @BUID='') --AND ActiveStatusId=1--Allow all customers

   IF @GlobalAcountNo IS NOT NULL
   BEGIN
			DECLARE @ResultTable TABLE(Id INT IDENTITY(1,1),CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))
			DECLARE @FinalResult TABLE(Id INT,CreateDate DATE,ReferenceNo VARCHAR(50),Particulars VARCHAR(50),Remarks VARCHAR(50),Debit DECIMAL(18,2),Credit DECIMAL(18,2),Balance DECIMAL(18,2))

			DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
		    
			INSERT INTO @ResultedMonths
			SELECT [YEAR],[MONTH] FROM DBO.fn_GetCurrentYearMonths_ByCurrentDate(@CurrentDate)-- ORDER BY Id DESC
									
			INSERT INTO @ResultTable (CreateDate,Particulars,Debit,Credit,Balance,Remarks)
			SELECT OpeningDate,'Opening Balance',TotalPendingAmount,0,0,''
			FROM dbo.fn_GetCustomerYear_OpeningBalance(@GlobalAcountNo)	
			

			SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
			---- Collection all the transaction Start
			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
			  BEGIN      
				SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
				
				SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
				SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

				INSERT INTO @ResultTable (CreateDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks)
				SELECT CreatedDate,ReferenceNo,Particulars,Debit,Credit,Balance,Remarks FROM 
				(SELECT CONVERT(DATE,CreatedDate) AS CreatedDate
						,BillNo AS ReferenceNo
						,'Sale of energy' As Particulars
						,0 AS Debit
						,TotalBillAmountWithTax As Credit
						,0 AS Balance
						,'' As Remarks
					FROM Tbl_CustomerBills(NOLOCK)
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
					--AND BillMonth = @PresentMonth AND BillYear = @PresentYear
				UNION
					SELECT 
						CONVERT(DATE,CreatedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),BillAdjustmentId) AS ReferenceNo
						,'Adjustment' As Particulars
						,0 AS Debit
						,AmountEffected As Credit,0 AS Balance,Remarks
					FROM Tbl_BillAdjustments(NOLOCK) 
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,CreatedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				UNION
					SELECT CONVERT(DATE,RecievedDate) AS CreatedDate
						,CONVERT(VARCHAR(50),CustomerPaymentID) AS ReferenceNo
						,(SELECT BillingType FROM Tbl_MBillingTypes WHERE BillingTypeId = CP.ReceivedDevice)+' Payments' As Particulars
						,PaidAmount AS Debit
						,0 As Credit
						,0 AS Balance
						,Remarks
					FROM Tbl_CustomerPayments(NOLOCK) CP
					WHERE AccountNo = @GlobalAcountNo
					AND CONVERT(DATE,RecievedDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date) 
				) AS A ORDER By CONVERT(DATE,A.CreatedDate)	
				  
				  IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
							IF(@MonthId IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Collection all the transaction END

			---- Finalise the transaction Records Start

			DECLARE @PresentTrans INT 
			SELECT TOP(1) @PresentTrans = Id FROM @ResultTable ORDER BY Id ASC    

			WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultTable WHERE Id >= @PresentTrans ORDER BY Id ASC))    
			  BEGIN 
					
					IF NOT EXISTS(SELECT 0 FROM @FinalResult WHERE Id < @PresentTrans)
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 THEN ((0 - Debit)) 
								   WHEN Credit <> 0 THEN (0 + Credit)
								   ELSE 0 END)AS Balance
							,CreateDate
							,Remarks	   
						FROM @ResultTable WHERE Id = @PresentTrans
					END
				ELSE
					BEGIN
						INSERT INTO @FinalResult(Id,ReferenceNo,Particulars,Debit,Credit,Balance,CreateDate,Remarks)
						SELECT 
							Id
							,ReferenceNo
							,Particulars
							,Debit
							,Credit 
							,(CASE WHEN Debit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) - Debit))
								 WHEN Credit <> 0 
									THEN (((SELECT TOP(1) ISNULL(Balance,0) FROM @FinalResult WHERE Id < @PresentTrans ORDER BY Id DESC) + Credit))
									END)AS Balance
							,CreateDate
							,Remarks		
						FROM @ResultTable WHERE Id = @PresentTrans
					END
					
				  IF(@PresentTrans = (SELECT TOP(1) Id FROM @ResultTable ORDER BY Id DESC))    
						   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentTrans = (SELECT TOP(1) Id FROM @ResultTable WHERE  Id > @PresentTrans ORDER BY Id ASC)    
							IF(@PresentTrans IS NULL) break;    
							  Continue    
					   END    
			  END      
			---- Finalise the transaction Records End
		    
		   
				 SELECT 
					  (AccountNo + ' - ' + GlobalAccountNumber) AS GlobalAccountNumber
					  --,KnownAs AS Name
					  ,GlobalAccountNumber AS AccountNo
					  ,(SELECT dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)) AS Name
					  ,ISNULL(ISNULL(HomeContactNo,ISNULL(BusinessContactNo,OtherContactNo)),'--') AS MobileNo
					  ,ISNULL(OldAccountNo,'--') AS OldAccountNo
					  ,CD.ClassName 
					  ,ISNULL(EmailId,'--') AS EmailId
					  ,CD.BusinessUnitName +' ( '+CD.BUCode+' ) '
					  ,CD.ServiceUnitName +' ( '+CD.SUCode+' ) '
					  ,CD.ServiceCenterName +' ( '+CD.SCCode+' ) '
					  --,CD.BookNo +' ( '+Cd.BUCode+' ) '
					  ,CD.BookId +' ( '+CD.BookCode+' ) '
					  ,CD.CycleName +' ( '+CD.CycleCode+' ) '
					  ,ISNULL(CD.MeterNumber,'--') AS MeterNumber
					  ,dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,
										Service_City,'',Service_ZipCode) AS ServiceAddress
					  ,1 AS IsSuccess
					  ,CONVERT(VARCHAR,CAST(ISNULL(AvgReading,0) AS DECIMAL(18,2)),-1) AS AvgReading
					  ,CONVERT(VARCHAR,CAST(OutStandingAmount AS MONEY),-1) AS OutStandingAmount
					  ,CD.ReadCodeID
					  ,ReadCode
					  ,0 AS IsCustExistsInOtherBU
			   FROM [UDV_CustomerDescription](NOLOCK) CD
			   INNER JOIN Tbl_MReadCodes(NOLOCK) RC ON RC.ReadCodeId = CD.ReadCodeID
					AND GlobalAccountNumber = @GlobalAcountNo
			
				SELECT 
					 ROW_NUMBER() OVER(ORDER BY Particulars) AS RowNumber
					,Particulars
					,CONVERT(VARCHAR,CAST(ISNULL(Debit,0) AS MONEY),-1) AS Debit
					,CONVERT(VARCHAR,CAST(ISNULL(Credit,0) AS MONEY),-1) AS Credit
					,CONVERT(VARCHAR,CAST(ISNULL(Balance,0) AS MONEY),-1) AS Balance
					,CONVERT(VARCHAR,CAST((-1*ISNULL(Balance,0)) AS MONEY),-1) As Due		
					,CONVERT(VARCHAR,CONVERT(VARCHAR(50),CreateDate,106)) AS TransactionDate
					,ReferenceNo
					,Remarks
				FROM @FinalResult 
			
	END
    ELSE IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] CD WHERE (OldAccountNo = @AcountNo OR GlobalAccountNumber= @AcountNo) AND BU_ID!=@BUID AND ActiveStatusId=1)
    BEGIN
		
			SELECT 1 AS IsSuccess,1 AS IsCustExistsInOtherBU
	
    END
    ELSE
    BEGIN
		
			SELECT 0 AS IsSuccess
	
    END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithDebitBal]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 09-05-2015
-- Description:	Get The Details of Debit Balence Of The Customers
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithDebitBal]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE  @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)			
			,@BookNo VARCHAR(MAX)
			,@MinAmt VARCHAR(50)
			,@MaxAmt VARCHAR(50)
			
	SELECT   @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')			
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
			,@MinAmt=C.value('(MinAmt)[1]','VARCHAR(50)')
			,@MaxAmt=C.value('(MaxAmt)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('RptDebitBe') AS T(C)
	
	
	IF(@MinAmt = '')
		SET @MinAmt = (SELECT MIN(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK))
	IF(@MaxAmt = '')
		SET @MaxAmt = (SELECT MAX(OutStandingAmount) FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK))
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits(NOLOCK) 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits(NOLOCK) 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
			
	;WITH PagedResults AS
	(
	SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
				--,A.AccountNo AS AccountNo
				,(CD.AccountNo+ ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
				,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
				,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode) AS ServiceAddress
				,OutStandingAmount AS TotalDueAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount
				,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)),'--') AS LastPaidDate
				,CD.OldAccountNo
				,(C.CycleName +' ( '+C.CycleCode+' ) ') AS CycleName				
				,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
				,PD.SortOrder AS CustomerSortOrder
				,BN.SortOrder AS BookSortOrder
				,(BU.BusinessUnitName +' ( '+BU.BUCode+' ) ') AS BusinessUnitName
				,(SU.ServiceUnitName +' ( '+SU.SUCode+' ) ') AS ServiceUnitName
				,(SC.ServiceCenterName +' ( '+SC.SCCode+' ) ') AS ServiceCenterName
				,COUNT(0) OVER() AS TotalRecords
		 FROM CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD 
		 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		 INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber AND CAD.OutStandingAmount >= 0
				AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		 INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo  
		 INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId     
		 INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
		 INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID
		 INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID 
		 INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		--SELECT	ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber
		--		--,A.AccountNo AS AccountNo
		--		,(CD.GlobalAccountNumber + ' - ' + CD.AccountNo) AS GlobalAccountNumber
		--		,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
		--		,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
		--											,CD.Service_Landmark
		--											,CD.Service_City,'',
		--											CD.Service_ZipCode) AS ServiceAddress
		--		,OutStandingAmount AS TotalDueAmount
		--		,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount
		--		,ISNULL((SELECT dbo.fn_GetCustomerLastPaymentDate(A.AccountNo)),'--') AS LastPaidDate
		--		,CD.OldAccountNo
		--		,C.CycleName
		--		,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
		--		,PD.SortOrder AS CustomerSortOrder
		--		,BN.SortOrder AS BookSortOrder
		--		,BU.BusinessUnitName AS BusinessUnitName
		--		,SU.ServiceUnitName AS ServiceUnitName
		--		,SC.ServiceCenterName AS ServiceCenterName
		--		,COUNT(0) OVER() AS TotalRecords
		-- FROM Tbl_CustomerBills(NOLOCK) A
		-- INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber = A.AccountNo
		-- INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		-- INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber AND CAD.OutStandingAmount >= 0
		--		AND CAD.OutStandingAmount BETWEEN @MinAmt AND @MaxAmt
		-- INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo  
		-- INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId     
		-- INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId  
		-- INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID
		-- INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID 
		-- INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID
		-- INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
		-- INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		-- INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		-- INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		-- INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		 
	)
	

		SELECT	 
			 RowNumber
			--,AccountNo
			,GlobalAccountNumber
			,Name
			,ServiceAddress
			,CONVERT(VARCHAR,CAST(TotalDueAmount AS MONEY),-1) AS TotalDueAmount
			,CONVERT(VARCHAR,CAST(LastPaidAmount AS MONEY),-1) AS LastPaidAmount
			,LastPaidDate
			,OldAccountNo
			,CycleName
			,BookNumber
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(TotalDueAmount,0)) FROM PagedResults) AS MONEY),-1) AS DueAmount
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize

END
--------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountWithoutMeter]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		  Satya
-- Modified By:   Karteek
-- Modified Date: 11th May 2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetAccountWithoutMeter]
(@xmlDoc xml)
AS
BEGIN
	Declare @BU varchar(MAX)	
			,@SU varchar(MAX)
			,@SC varchar(MAX)
			,@Tariff varchar(MAX)
			,@PageNo INT
			,@PageSize INT
				
		select @BU=C.value('(BU_ID)[1]','varchar(MAX)')
		,@SU=C.value('(SU_ID)[1]','varchar(MAX)')
		,@SC=C.value('(SC_ID)[1]','varchar(MAX)')
		,@Tariff=C.value('(Tariff)[1]','varchar(MAX)')
		,@PageNo = C.value('(PageNo)[1]','INT')
		,@PageSize = C.value('(PageSize)[1]','INT')	
		from @xmlDoc.nodes('RptDirectCustomersBe') AS T(C)

		Declare @Count INT
		
		IF(@BU = '')
			BEGIN
				SELECT @BU = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
						 FROM Tbl_BussinessUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SU = '')
			BEGIN
				SELECT @SU = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
						 FROM Tbl_ServiceUnits (NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@SC = '')
			BEGIN
				SELECT @SC = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
						 FROM Tbl_ServiceCenter(NOLOCK)
						 WHERE ActiveStatusId = 1
						 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU,','))
						 FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,1,'')
			END
		IF(@Tariff = '')
			BEGIN
				SELECT @Tariff = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
						 FROM Tbl_MTariffClasses C(NOLOCK)
						 JOIN Tbl_MTariffClasses SC(NOLOCK) ON C.ClassID=SC.RefClassID    
						  AND SC.IsActiveClass =1    
						  AND C.IsActiveClass=1
						  FOR XML PATH(''), TYPE)
						  .value('.','NVARCHAR(MAX)'),1,1,'')
			END 
				
			SELECT
			   IDENTITY (int, 1,1) As   RowNumber						   
			  ,CustomerFullName as Name
			  ,(CD.AccountNo +' - '+CD.GlobalAccountNumber) AS GlobalAccountNumber
			  ,CD.[ServiceAddress]
			  ,CD.ClassName
			  ,CD.OldAccountNo
			  ,CD.SortOrder AS CustomerSortOrder
			  --,CD.BookCode AS BookNo
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNo
			  ,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ' ) AS BusinessUnitName
			  ,(CD.ServiceUnitName +' ( '+CD.SUCode+' ) ' ) AS ServiceUnitName
			  ,(CD.ServiceCenterName +' ( '+CD.SCCode+' ) ' ) AS ServiceCenterName
			  ,CD.ConnectionDate
			  ,(CD.CycleName +' ( '+CD.CycleCode+' ) ' ) AS CycleName
			  ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber
			  ,CD.BookSortOrder
			  ,CD.OutstandingAmount
			  ,ISNULL(CD.AvgReading,0) AS AvgReading
			  INTO #CustomersList  
			  FROM [UDV_RptCustomerAndBookInfo](NOLOCK) CD
			  --LEFT JOIN Tbl_DirectCustomersAvgReadings DC ON DC.GlobalAccountNumber=CD.GlobalAccountNumber
			  INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU,',')) BU ON BU.BU_ID = CD.BU_ID AND CD.ReadCodeId=1
			  INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU,',')) SU ON SU.SU_Id = CD.SU_ID
			  INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC,',')) SC ON SC.SC_ID = CD.ServiceCenterId
			  INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@Tariff,',')) TC ON TC.TariffId = CD.TariffId
			   	  
			 SET @Count=(select count(0) from #CustomersList)				

			 SELECT RowNumber						   
				  ,Name
				  ,GlobalAccountNumber
				  ,[ServiceAddress]
				  ,ClassName
				  ,OldAccountNo
				  ,CustomerSortOrder
				  ,BookNo
				  ,CONVERT(VARCHAR(20),ConnectionDate,106)AS ConnectionDate
				  ,BusinessUnitName
				  ,ServiceUnitName
				  ,ServiceCenterName
				  ,CycleName
				  ,BookNumber
				  ,BookSortOrder
				  --,@Count As TotalRecords
				  ,REPLACE(CONVERT(VARCHAR, (CAST(@Count AS MONEY)), 1), '.00', '') AS TotalRecords
				  ,CONVERT(VARCHAR, (CAST(OutstandingAmount AS MONEY)), 1) AS OutstandingAmount
				  ,CONVERT(VARCHAR,CAST((SELECT SUM(ISNULL(OutstandingAmount,0)) FROM #CustomersList) AS MONEY),-1) AS TotalDueAmount
				  ,AvgReading
			 from #CustomersList			 
			 where RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
			 ORDER BY RowNumber
								
			 DROP TABLE #CustomersList
 
 END
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersNoNameOrAddList]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju.V  
-- Create date: 16-Sep-2014  
-- Description: Get The Details of The Customers With out Having No Name Or Address  
-- Modified By : Karthik  
-- Modified Date : 27-12-2014  
-- Modified By : Padmini  
-- Modified Date : 01/12/2015  
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015
-- Modified Date: 11-05-2015     
-- =============================================  
ALTER PROCEDURE [dbo].[USP_RptGetCustomersNoNameOrAddList]  
(  
 @XmlDoc xml  
)  
AS  
BEGIN  
  
 DECLARE  @PageSize INT  
   ,@PageNo INT  
   ,@BU_ID VARCHAR(MAX)  
   ,@SU_ID VARCHAR(MAX)  
   ,@SC_ID VARCHAR(MAX)    
   ,@CycleId VARCHAR(MAX)  
   ,@BookNo VARCHAR(MAX)     
     
 SELECT   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')  
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')     
  FROM @XmlDoc.nodes('RptNoNameOrAddressBe') AS T(C)  
   
   
 IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SU_ID = '')  
  BEGIN  
   SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50))   
      FROM Tbl_ServiceUnits   (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@SC_ID = '')  
  BEGIN  
   SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50))   
      FROM Tbl_ServiceCenter  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@CycleId = '')  
  BEGIN  
   SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50))   
      FROM Tbl_Cycles  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
 IF(@BookNo = '')  
  BEGIN  
   SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50))   
      FROM Tbl_BookNumbers  (NOLOCK)
      WHERE ActiveStatusId = 1  
      AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
   
 ;WITH PagedResults AS  
 (  
  SELECT   ROW_NUMBER() OVER(ORDER BY CD.GlobalAccountNumber ) AS RowNumber  
    ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name  
    ,(CD.AccountNo  + ' - ' + CD.GlobalAccountNumber) AS GlobalAccountNumber  
    ,CD.AccountNo AS AccountNo  
    ,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode)) As ServiceAddress  
    ,ISNULL(OldAccountNo,'--') AS OldAccountNo  
    ,(CD.CycleName  +' ( '+CD.CycleCode+' ) ')  AS CycleName
    ,(CD.BookId + ' - ' + CD.BookCode) AS BookNumber  
    ,CD.SortOrder AS CustomerSortOrder  
    ,CD.BookSortOrder  
    ,(CD.BusinessUnitName +' ( '+CD.BUCode+' ) ')  AS BusinessUnitName
    ,(CD.ServiceUnitName  +' ( '+CD.SUCode+' ) ')  AS ServiceUnitName
    ,(CD.ServiceCenterName  +' ( '+CD.SCCode+' ) ') AS ServiceCenterName
    FROM UDV_CustomerDescription(NOLOCK) CD  
    INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
      AND CD.ActiveStatusId=1       
    INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD(NOLOCK) ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber
    INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD(NOLOCK) ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber
      AND (PAD.IsServiceAddress = 0 OR SAD.IsServiceAddress = 1)
      AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName=''
      OR PAD.StreetName IS NULL OR PAD.StreetName = ''OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
      OR PAD.City IS NULL OR PAD.City = '' OR SAD.StreetName IS NULL OR SAD.StreetName = ''  OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
               OR SAD.City IS NULL OR SAD.City = '')   
    
    --INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CD.BU_ID   
    --  AND CD.ActiveStatusId=1 AND (CD.FirstName IS NULL OR CD.FirstName='' OR CD.LastName IS NULL OR CD.LastName='')  
    --INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CD.SU_ID  
    --INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CD.ServiceCenterId  
    --INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CD.CycleId  
    --INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CD.BookNo  
    --INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] PAD(NOLOCK) ON CD.GlobalAccountNumber = PAD.GlobalAccountNumber   
    --  AND PAD.IsServiceAddress = 0 AND (PAD.StreetName IS NULL OR PAD.StreetName = ''   
    --           OR PAD.HouseNo IS NULL OR PAD.HouseNo = ''   
    --           OR PAD.City IS NULL OR PAD.City = '')  
    --INNER JOIN [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] SAD(NOLOCK) ON CD.GlobalAccountNumber = SAD.GlobalAccountNumber   
    --  AND SAD.IsServiceAddress = 1 AND (SAD.StreetName IS NULL OR SAD.StreetName = ''   
    --           OR SAD.HouseNo IS NULL OR SAD.HouseNo = ''   
    --           OR SAD.City IS NULL OR SAD.City = '')   
 )  
   
  SELECT  
		RowNumber  
	   ,Name  
	   ,GlobalAccountNumber  
	   ,AccountNo  
	   ,ServiceAddress  
	   ,BusinessUnitName  
	   ,OldAccountNo  
	   ,CycleName  
	   ,BookNumber  
	   ,CustomerSortOrder  
	   ,BookSortOrder  
	   ,BusinessUnitName  
	   ,ServiceUnitName  
	   ,ServiceCenterName  
	   ,(Select COUNT(0) from PagedResults) as TotalRecords  
  FROM PagedResults p  
  WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetAccountsWithCreditBal]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 23-Aug-2014
-- Description:	Get The Details of Credit Balance Of The Customers
-- Modified By: Karteek
-- Modified Date:30-03-2015
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetAccountsWithCreditBal]
(
	@XmlDoc xml
)
AS
BEGIN

		DECLARE @PageSize INT
			,@PageNo INT
			,@BU_ID VARCHAR(MAX)
			,@SU_ID VARCHAR(MAX)
			,@SC_ID VARCHAR(MAX)		
			,@CycleId VARCHAR(MAX)
			,@BookNo VARCHAR(MAX)
			
		SELECT @PageNo = C.value('(PageNo)[1]','INT')
			,@PageSize = C.value('(PageSize)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')
			,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')
			,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')
			,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')
			,@BookNo=C.value('(BookNo)[1]','VARCHAR(MAX)')
		FROM @XmlDoc.nodes('RptCreditBe') AS T(C)
	
	
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits(NOLOCK)
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers(NOLOCK)
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	;WITH PagedResults AS
	(		
		SELECT ROW_NUMBER() OVER(ORDER BY PD.SortOrder ) AS RowNumber				
			,(CD.AccountNo+ ' - ' +CD.GlobalAccountNumber) AS GlobalAccountNumber
			--,CD.AccountNo AS AccountNo
			,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS Name
			,(dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName
													,CD.Service_Landmark
													,CD.Service_City,'',
													CD.Service_ZipCode)) As ServiceAddress
			,ISNULL((SELECT dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber)),0) AS LastPaidAmount		
			,ISNULL(CONVERT(VARCHAR(50),(dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber)) ,106),'--') AS LastPaidDate
			,ISNULL(CAD.OutStandingAmount,0) As OverPayAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalBillsAmount(CD.GlobalAccountNumber)),0) As TotalBillsAmount
			,ISNULL((SELECT dbo.fn_GetCustomerTotalPaidAmount(CD.GlobalAccountNumber)),0) As TotalPaidAmount
			,BN.BookCode AS BookCode
			,CD.OldAccountNo
			,(C.CycleName +' ( '+C.CycleCode+' ) ') AS CycleName
			,(BN.ID + ' - ' + BN.BookCode) AS BookNumber
			,PD.SortOrder AS CustomerSortOrder
			,BN.SortOrder AS BookSortOrder
			,(BU.BusinessUnitName +' ( '+BU.BUCode+' ) ') AS BusinessUnitName
			,(SU.ServiceUnitName +' ( '+SU.SUCode+' ) ') AS ServiceUnitName
			,(SC.ServiceCenterName +' ( '+SC.SCCode+' ) ') AS ServiceCenterName
			,COUNT(0) OVER() AS TotalRecords
		FROM CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
			AND CAD.OutStandingAmount < 0
		INNER JOIN dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo 
		INNER JOIN dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId 
		INNER JOIN dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId
		INNER JOIN dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID 
		INNER JOIN dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID
		INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID 		
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU1 ON BU1.BU_ID = BU.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU1 ON SU1.SU_Id = SU.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC1 ON SC1.SC_ID = SC.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = C.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN1 ON BN1.BookNo = BN.BookNo
	)
	
		SELECT
			 RowNumber
			,GlobalAccountNumber
			--,AccountNo
			,Name
			,ServiceAddress
			,CONVERT(VARCHAR,CAST(LastPaidAmount AS MONEY),-1) AS LastPaidAmount	
			,LastPaidDate
			,CONVERT(VARCHAR,CAST(OverPayAmount AS MONEY),-1) AS OverPayAmount
			,CONVERT(VARCHAR,CAST(TotalBillsAmount AS MONEY),-1) AS TotalBillsAmount
			,CONVERT(VARCHAR,CAST(TotalPaidAmount AS MONEY),-1) AS TotalPaidAmount
			,BookCode
			,OldAccountNo
			,CycleName
			,BookNumber 
			,CustomerSortOrder
			,BookSortOrder
			,BusinessUnitName
			,ServiceUnitName
			,ServiceCenterName
			,CONVERT(VARCHAR,CAST((SELECT SUM(OverPayAmount) FROM PagedResults) AS MONEY),-1) AS TotalCreditBalance				
			,TotalRecords
		FROM PagedResults p
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	
END
------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================

ALTER PROCEDURE [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @GlobalAccountNo VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	DECLARE @IsDisabledBook BIT 
	
	SELECT @CustomerBusinessUnitID = U.BU_ID,@CustomerActiveStatusID = ISNULL(U.ActiveStatusId,0)  
		,@GlobalAccountNo = U.GlobalAccountNumber
		--,@IsDisabledBook = (CASE WHEN BD.IsActive = 1 
		--						THEN (CASE WHEN IsPartialBill IS NULL THEN 1 ELSE 0 END)
		--						ELSE 0 END)
		,@IsDisabledBook = (SELECT TOP 1 (CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(BD.IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) 
							FROM Tbl_BillingDisabledBooks BD 
							WHERE BD.BookNo = U.BookNo
							ORDER BY DisabledBookId DESC)
	FROM  UDV_IsCustomerExists U
	--LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = U.BookNo
	WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo OR MeterNumber=@AccountNo)
	  	
	IF @GlobalAccountNo IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
		IF @CustomerBusinessUnitID = @BUID    
			BEGIN
				IF @CustomerActiveStatusID = 1 --OR @CustomerActiveStatusID=2 
					BEGIN
						--PRINT 'Sucess and Active  Customer'
						SELECT 1 AS IsSuccess,
							@GlobalAccountNo AS AccountNo,
							@IsDisabledBook AS IsDisabledBook,
							0 AS CustIsNotActive
						FOR XML PATH('RptCustomerLedgerBe') 
					END
				ELSE
					BEGIN							 
						--PRINT 'Failure In Active Status'
						SELECT 1 AS IsSuccess,
								1 AS CustIsNotActive,
								@CustomerActiveStatusID AS ActiveStatusId
						FOR XML PATH('RptCustomerLedgerBe') 
					END
			END
		ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU ,
						@IsDisabledBook AS IsDisabledBook,
						0 AS CustIsNotActive
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustomerExistsInBU_Billing_INDV]    Script Date: 07/21/2015 19:13:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		SATYA
-- Create date: 8-APRIL-2014
-- Description:	The purpose of this procedure is to check customer exists in given BU for billing purpose
-- Modified By: Neeraj Kanojiya
-- Description: Created new copy. Will search Activ and In-Active customer with Temporary Book Disabled.
-- Date		  : 3-June-15
-- Modified By: Neeraj Kanojiya
-- Description: WILL RETURN TRUE EVEN BOOK IS TEMPORARLY CLOSED
-- Date		  : 13-JULY-15
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustomerExistsInBU_Billing_INDV] 
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@BUID VARCHAR(50)=''
		,@Flag INT
		
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') as T(C) 


	Declare @CustomerBusinessUnitID varchar(50)
			,@CustomerActiveStatusID int
			,@BookNo varchar(50)
			,@NoPower int
			,@CustomerType INT
			,@MeterType INT
			,@IsPrepaidCustomer BIT=0
			,@IsNotCreditMeter BIT=1
	
 
	SELECT @CustomerType=CPD.CustomerTypeId,@MeterType=MI.MeterType 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD
	JOIN Tbl_MeterInformation AS MI ON CPD.MeterNumber=MI.MeterNo
	WHERE CPD.GlobalAccountNumber=@AccountNo
	
	if(@CustomerType=3)
		SET @IsPrepaidCustomer=1
	if(@MeterType!=2)
		SET @IsNotCreditMeter = 0
	
	Select @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)
	,@BookNo=BookNo  
	from  UDV_IsCustomerExists where GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo AND ActiveStatusId IN (1,2)

	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	 
	  
	Select @NoPower=case when DisableTypeId=2 then 1 else 0 end from 
	Tbl_BillingDisabledBooks    where BookNo =@BookNo	 and IsActive=1   
	PRINT   @CustomerActiveStatusID
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
		SELECT 0 AS IsSuccess FOR XML PATH('RptCustomerLedgerBe')
			--Print 'Customer Not Exist'
		END
	ELSE
		BEGIN
				IF	   @CustomerBusinessUnitID =  @BUID	  OR	@BUID =''    
				BEGIN
						IF	 @CustomerActiveStatusID  = 1   OR @CustomerActiveStatusID  = 2
							BEGIN
								IF  isnull(@NoPower,0)!=1 
									SELECT	1 AS IsSuccess
											,@CustomerActiveStatusID AS ActiveStatusId 
											,@IsPrepaidCustomer AS IsPrepaidCustomer
											,@IsNotCreditMeter AS IsNotCreditMeter 
									FOR XML PATH('RptCustomerLedgerBe')
								ELSE
									 SELECT 1 AS IsSuccess
											,@CustomerActiveStatusID AS ActiveStatusId 
											,@IsPrepaidCustomer AS IsPrepaidCustomer
											,@IsNotCreditMeter AS IsNotCreditMeter
									 FOR XML PATH('RptCustomerLedgerBe')
								 --print 'Sucess     Customer' 
							END
						ELSE
							BEGIN
								 SELECT 0 AS IsSuccess
										,@CustomerActiveStatusID AS ActiveStatusId 
										,@IsPrepaidCustomer AS IsPrepaidCustomer
										,@IsNotCreditMeter AS IsNotCreditMeter
								 FOR XML PATH('RptCustomerLedgerBe')
								--print 'Failure In Active Status'
							END
				END
				ELSE
					BEGIN
					SELECT	0 AS IsSuccess
							,@CustomerActiveStatusID AS ActiveStatusId 
							,@IsPrepaidCustomer AS IsPrepaidCustomer
							,@IsNotCreditMeter AS IsNotCreditMeter
					FOR XML PATH('RptCustomerLedgerBe')
					--Print 'Not Belogns to the Same BU'
					END
		END
	
END

GO


