
GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- Modified By: NEERAJ KANOJIYA
-- Modified Date: 26-JUNE-2015
-- Description: @AccountNo was incorrect in update statement for outstanding, modified with @GlobalAccountNo.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@AmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT,
				@BatchId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate
	,@BatchId							=BatchId
	,@AmountEffected					=(TotalAmountEffected - TaxEffected)
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			
			DECLARE @TotalAmountEffectedToCal DECIMAL(18,2) = 0, @TotalAdjustedUnits BIGINT = 0
			DECLARE @TotalAdditionalChanrges DECIMAL(18,2) = 0, @TotalConsumption BIGINT = 0
			
			SELECT 
				 @TotalAdditionalChanrges = ISNULL(NetFixedCharges,0)
				,@TotalConsumption = CONVERT(BIGINT,ISNULL(Usage,0))
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber
			
			SELECT 
				 @TotalAmountEffectedToCal = SUM(ISNULL(AmountEffected,0))
				,@TotalAdjustedUnits = SUM(ISNULL(AdjustedUnits,0))
			FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber
			
			IF(ISNULL(@TotalAdditionalChanrges,0) < (ISNULL(@AmountEffected,0) + ISNULL(@TotalAmountEffectedToCal,0)))
				BEGIN
					SELECT 0 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
			ELSE IF(ISNULL(@TotalConsumption,0) < (ISNULL(@AdjustedUnits,0) + ISNULL(@TotalAdjustedUnits,0)))
				BEGIN
					SELECT 0 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
			ELSE 
				BEGIN
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
						BEGIN
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
							--						WHERE AdjustmentLogId = @AdjustmentLogId))

							--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
							
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
													WHERE AdjustmentLogId = @AdjustmentLogId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
										FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
							DECLARE @FinalApproval BIT

							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
								BEGIN
										SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
								END
							ELSE
								BEGIN
									DECLARE @UserDetailedId INT
									SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
									SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
								END			
							
							
							IF(@FinalApproval =1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_AdjustmentsLogs 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApproveStatusId = @ApprovalStatusId
										,CurrentApprovalLevel=0
										,IsLocked = 1
									WHERE GlobalAccountNumber = @GlobalAccountNo 
									AND AdjustmentLogId = @AdjustmentLogId 

									INSERT INTO Tbl_BillAdjustments(
										 AccountNo    
										 --,CustomerId    
										 ,AmountEffected   
										 ,BillAdjustmentType    
										 ,TotalAmountEffected
										 ,TaxEffected
										 ,AdjustedUnits
										 ,CustomerBillId
										 ,ApprovedBy
										 ,CreatedBy
										 ,CreatedDate 
										 ,BatchNo  
										 )           
										VALUES(         
										  @GlobalAccountNo    
										 --,@CustomerId    
										 ,@AmountEffected
										 ,@BillAdjustmentTypeId   
										 ,@TotalAmountEffected
										 ,@TaxEffected
										 ,@AdjustedUnits
										 ,@BillNumber
										 ,@ModifedBy
										 ,@CreatedBy
										 ,@CreatedDate
										 ,@BatchId 
										 )      
									SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
									
									INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges,PreviousReading,CurrentReadingAfterAdjustment,AdditionalCharges,CurrentReadingBeforeAdjustment,AdditionalChargesID)
									SELECT @BillAdjustmentId,@EnergryCharges,@PreviousReading,@CurrentReadingAfterAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN @AmountEffected ELSE NULL END),@CurrentReadingBeforeAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN 1 ELSE 0 END)
									
									----OutStanding Amount is Updating Start
									
									--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
									--WHERE GlobalAccountNumber=@GlobalAccountNo
									
									--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
									
									--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
									--SET OutStandingAmount=@OutStandingAmount
									--WHERE GlobalAccountNumber=@GlobalAccountNo
									----OutStanding Amount is Updating End
									
									UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
										,ModifedBy=@ModifiedBy
										,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
									
									INSERT INTO Tbl_Audit_AdjustmentsLogs 
										(	 AdjustmentLogId
											,GlobalAccountNumber 
											,AccountNo 
											,BillNumber 
											,BillAdjustmentTypeId  
											,MeterNumber 
											,PreviousReading 
											,CurrentReadingAfterAdjustment 
											,CurrentReadingBeforeAdjustment 
											,AdjustedUnits 
											,TaxEffected 
											,TotalAmountEffected 
											,EnergryCharges 
											,AdditionalCharges 
											,PresentApprovalRole
											,NextApprovalRole  
											,ApproveStatusId  
											,Remarks
											,CreatedBy 
											,CreatedDate  
											,ModifedBy 
											,ModifiedDate
										)
									VALUES (@AdjustmentLogId
											,@GlobalAccountNo
											,@AccountNo 
											,@BillNumber 
											,@BillAdjustmentTypeId  
											,@MeterNumber 
											,@PreviousReading 
											,@CurrentReadingAfterAdjustment 
											,@CurrentReadingBeforeAdjustment 
											,@AdjustedUnits 
											,@TaxEffected 
											,@TotalAmountEffected 
											,@EnergryCharges 
											,@AdditionalCharges 
											,@PresentRoleId
											,@NextRoleId
											,@ApprovalStatusId
											,@Remarks
											,@CreatedBy 
											,@CreatedDate  
											,@ModifedBy 
											,dbo.fn_GetCurrentDateTime())
										
								END
							ELSE -- Not a final approval
								BEGIN
									
									UPDATE Tbl_AdjustmentsLogs 
									SET   -- Updating Request with Level Roles
										 ModifedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
										,CurrentApprovalLevel = CurrentApprovalLevel+1
										,IsLocked = 1
									WHERE GlobalAccountNumber = @GlobalAccountNo 
									AND AdjustmentLogId = @AdjustmentLogId
									
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							
								UPDATE Tbl_AdjustmentsLogs 
								SET   -- Updating Request with Level Roles & Approval status 
									 ModifedBy = @ModifiedBy
									,ModifiedDate = dbo.fn_GetCurrentDateTime()
									,PresentApprovalRole = @PresentRoleId
									,NextApprovalRole = @NextRoleId 
									,ApproveStatusId = @ApprovalStatusId
									,CurrentApprovalLevel= 0
									,IsLocked=1
								WHERE GlobalAccountNumber = @GlobalAccountNo 
								AND AdjustmentLogId = @AdjustmentLogId 

								INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,TaxEffected
								 ,AdjustedUnits
								 ,CustomerBillId
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate 
								 ,BatchNo  
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@AmountEffected
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@TaxEffected
								 ,@AdjustedUnits
								 ,@BillNumber
								 ,@ModifedBy
								 ,@CreatedBy
								 ,@CreatedDate
								 ,@BatchId 
								 )      
								SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
			   
								INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges,PreviousReading,CurrentReadingAfterAdjustment,AdditionalCharges,CurrentReadingBeforeAdjustment,AdditionalChargesID)
								SELECT @BillAdjustmentId,@EnergryCharges,@PreviousReading,@CurrentReadingAfterAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN @AmountEffected ELSE NULL END),@CurrentReadingBeforeAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN 1 ELSE 0 END)
														
								----OutStanding Amount is Updating Start
									
								--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
								--	WHERE GlobalAccountNumber=@GlobalAccountNo
									
								--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
									
								--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
								--	SET OutStandingAmount=@OutStandingAmount
								--	WHERE GlobalAccountNumber=@GlobalAccountNo
								--	--OutStanding Amount is Updating End
								
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
								
								INSERT INTO Tbl_Audit_AdjustmentsLogs 
										(	 AdjustmentLogId
											,GlobalAccountNumber 
											,AccountNo 
											,BillNumber 
											,BillAdjustmentTypeId  
											,MeterNumber 
											,PreviousReading 
											,CurrentReadingAfterAdjustment 
											,CurrentReadingBeforeAdjustment 
											,AdjustedUnits 
											,TaxEffected 
											,TotalAmountEffected 
											,EnergryCharges 
											,AdditionalCharges 
											,PresentApprovalRole
											,NextApprovalRole  
											,ApproveStatusId  
											,Remarks
											,CreatedBy 
											,CreatedDate  
											,ModifedBy 
											,ModifiedDate
										)
									VALUES (@AdjustmentLogId
											,@GlobalAccountNo
											,@AccountNo 
											,@BillNumber 
											,@BillAdjustmentTypeId  
											,@MeterNumber 
											,@PreviousReading 
											,@CurrentReadingAfterAdjustment 
											,@CurrentReadingBeforeAdjustment 
											,@AdjustedUnits 
											,@TaxEffected 
											,@TotalAmountEffected 
											,@EnergryCharges 
											,@AdditionalCharges 
											,@PresentRoleId
											,@NextRoleId
											,@ApprovalStatusId
											,@Remarks
											,@CreatedBy 
											,@CreatedDate  
											,@ModifedBy 
											,dbo.fn_GetCurrentDateTime())					
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentLogsToApprove]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Karteek
-- Create date: 03-04-2015
-- Description: The purpose of this procedure is to get list of Name Requested persons for approval
-- MODIFIED BY:  NEERAJ KANOJIYA
-- Create date: 29-JUNE-2015
-- Description: CHANGE BILL ID TO BILL NO BY JOINING CUSTOMERBILL TBL, ADDED REQUEST DATE FIELS AND AND ORDER BY REQUEST DATE
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAdjustmentLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50)
			,@FunctionId INT
      ,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT  
			--ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber
			ROW_NUMBER() OVER (ORDER BY T.AdjustmentLogId desc) AS RowNumber
			,T.AdjustmentLogId
			,(CD.AccountNo+' - '+  T.GlobalAccountNumber) AS GlobalAccountNumber
			,CD.OldAccountNo AS OldAccountNumber  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS Name
			--,T.BillNumber
			,CSTB.BillNo AS BillNumber
			,ISNULL(BA.Name,'Adjustment(NoBill)') AS BillAdjustmentType
			,T.MeterNumber 
			,T.PreviousReading
			,T.CurrentReadingAfterAdjustment
			,T.CurrentReadingBeforeAdjustment 
			--,(CONVERT(DECIMAL(18,2),T.CurrentReadingAfterAdjustment) - CONVERT(DECIMAL(18,2),T.PreviousReading)) AS Consumption
			,CSTB.Usage AS Consumption
			,CONVERT(INT,T.AdjustedUnits) AS AdjustedUnits
			,T.TaxEffected
			,T.TotalAmountEffected
			,T.EnergryCharges
			,T.AdditionalCharges
			,T.Remarks AS Details
			,CONVERT(VARCHAR(12),T.CreatedDate,106) AS RequestDate 
			,(CASE WHEN T.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (T.PresentApprovalRole IS NULL OR (T.PresentApprovalRole = @RoleId 
			AND T.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
			,NR.RoleName AS NextRole
			,CR.RoleName AS CurrentRole
			,T.PresentApprovalRole
			,T.NextApprovalRole
	FROM Tbl_AdjustmentsLogs T 
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = T.GlobalAccountNumber AND T.ApproveStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON T.ApproveStatusId = APS.ApprovalStatusId
	--INNER JOIN Tbl_BillAdjustmentType BAT ON BAT.Name= T.BillAdjustmentTypeId
	LEFT JOIN Tbl_MRoles AS NR ON T.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON T.PresentApprovalRole = CR.RoleId
	LEFT JOIN Tbl_BillAdjustmentType BA ON BA.BATID = T.BillAdjustmentTypeId
	LEFT JOIN Tbl_CustomerBills AS CSTB ON T.BillNumber = CSTB.CustomerBillId
	WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
	ORDER BY T.CreatedDate desc
END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
   ,@AdjustmentAmountEffected DECIMAL(18,2)
   ,@IsFirstmonthBill Bit
    ,@SetupDate DATETIME
   ,@ConnectionDate DATETIME
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	 SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	 FROM Tbl_CustomerBills AS CB
	 JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
	 JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
	 WHERE CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      ,@SetupDate=SetupDate
      ,@ConnectionDate=ConnectionDate  
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END		
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)	
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   IF isnull(@PaidMeterDeductedAmount,0) >0
   BEGIN
   
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
   
   update Tbl_PaidMeterDetails
   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
   where AccountNo=@GlobalAccountNumber
   
   END
         
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
   SET @BillingComments=NULL
   SET @TotalBillAmount=NULL
   SET @PaidMeterDeductedAmount=NULL
   SET @IsEmbassyCustomer=NULL
   SET @TaxPercentage=NULL
   SET @PaidMeterDeductedAmount=NULL
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
	END
END  
				 










GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)
			,@IsFirstmonthBill Bit
			,@SetupDate DATETIME
			,@ConnectionDate DATETIME

	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
			,SetupDate DATETIME
			,ConnectionDate DATETIME
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark
			,SetupDate  
			,ConnectionDate
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark 
			,CD.SetupDate 
			,CD.ConnectionDate
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   

		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN

			BEGIN TRY		    
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@SetupDate=SetupDate
						,@ConnectionDate=ConnectionDate 
						
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------


						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END
					
					  ;WITH CTE (PreviousReading,PresentReading,GlobalAccountNumber,DuplicateCount)
						AS
						(
						SELECT PreviousReading,PresentReading,GlobalAccountNumber,
						ROW_NUMBER() OVER(PARTITION BY PreviousReading,PresentReading,GlobalAccountNumber 
						ORDER BY GlobalAccountNumber) AS DuplicateCount
						FROM Tbl_CustomerReadings
						where GlobalAccountNumber=@GlobalAccountNumber
						and IsBilled=0
						)
						DELETE 
						FROM CTE
						WHERE DuplicateCount > 1	 
						
						
							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										
										SET @ReadType=5 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										 
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
											SET @Usage=0
											SET @ActualUSage=0
											
										END			
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						SET @TaxPercentage=5;
						 SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
						 			 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )

 
							IF @PrevCustomerBillId IS NULL 
							BEGIN

							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						
						SET  @BillGeneratedBY =( select  top 1 CreatedBy from Tbl_BillingQueueSchedule where BillingQueueScheduleId=@BillingQueueScheduleId)
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,
							(
								SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P 
								WHERE P.AccountNo = C.GlobalAccountNumber 
								ORDER BY RecievedDate DESC
							)         
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,case when (select COUNT(0) from Tbl_CustomerBills where AccountNo=@GlobalAccountNumber)>0  THEN @OutStandingAmount  else @OpeningBalance END
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						  IF isnull(@PaidMeterDeductedAmount,0) >0
						   BEGIN
						   
						   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
						   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
						   
						   update Tbl_PaidMeterDetails
						   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
						   where AccountNo=@GlobalAccountNumber
						   
						   END

						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------


						update Tbl_CustomerReadings set IsBilled =1,
						BillNo=@CusotmerNewBillID,ModifiedDate=dbo.fn_GetCurrentDateTime()
						where GlobalAccountNumber=@GlobalAccountNumber
						and IsBilled=0
						 
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)
						select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges
						
						delete from @tblFixedCharges
						------------------------------------------------------------------------------------
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-
						SET @TotalAmount = NULL
						SET @CusotmerNewBillID=NULL
						SET @CusotmerNewBillID=NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL
						SET @IsEmbassyCustomer=NULL
						SET @TaxPercentage=NULL
						SET  @IsFirstmonthBill =NULL
						SET  @SetupDate =NULL
						SET @PaidMeterDeductedAmount=NULL
   			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue;        
						END
			END TRY
			BEGIN CATCH
			 
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue      

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_BillAdjustment]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <22-JULY-2014>
-- Description:	<INSERT BILL ADJUSTMENT DETAILS>
-- Modified By : Padmini
-- Modified Date : 26-12-2014
-- =============================================
ALTER PROCEDURE [dbo].[USP_Insert_BillAdjustment]
(
	@XmlDoc xml=null
	,@MultiXmlDoc XML
)
AS
BEGIN	
		DECLARE
			@BillAdjustmentId INT
			,@CustomerBillId INT
			,@AccountNo VARCHAR(50)
			,@CustomerId VARCHAR(50)
			,@AmountEffected DECIMAL(18,2)
			,@TaxEffected DECIMAL(18,2)
			,@TotalAmountEffected DECIMAL(18,2)
			,@AdjustedUnits INT
			,@BillAdjustmentType INT
			,@BatchNo INT
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@ApprovalStatusId INT 
			,@Details VARCHAR(MAX)  
			,@EnergyCharges DECIMAL(18,2)
			,@AdditionalCharges DECIMAL(18,2)
			,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
		SELECT
			@CustomerBillId = C.value('(CustomerBillId)[1]','INT')
			,@AccountNo	= C.value('(AccountNo)[1]','VARCHAR(50)')
			,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')
			,@TaxEffected = C.value('(TaxEffected)[1]','DECIMAL(18,2)')
			,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)')
			,@BillAdjustmentType	= C.value('(BillAdjustmentType)[1]','INT')
			,@BatchNo = C.value('(BatchNo)[1]','INT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
			,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
			,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)')
				,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C) 

		
 		--IF((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs   
			--		WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		--BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				,MeterNumber
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
							,CPD.MeterNumber
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber
					AND CD.GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					INSERT INTO Tbl_BillAdjustments(
						CustomerBillId
						,AccountNo
						,CustomerId
						,AmountEffected
						,BillAdjustmentType
						,TaxEffected
						,TotalAmountEffected
						,BatchNo
						,ApprovedBy
						,CreatedBy
						,CreatedDate
						) 						
					VALUES(					
						@CustomerBillId
						,@AccountNo
						,@CustomerId
						,@AmountEffected
						,@BillAdjustmentType
						,@TaxEffected
						,@TotalAmountEffected
						,@BatchNo
						,@ModifiedBy
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						)		
					SELECT @BillAdjustmentId = SCOPE_IDENTITY()
				
					INSERT INTO Tbl_BillAdjustmentDetails
											(
											BillAdjustmentId
											,AdditionalChargesID
											,AdditionalCharges
											,EnergryCharges
											,CreatedBy
											,CreatedDate
											)
					SELECT       
						@BillAdjustmentId 
						,C.value('(ChargeID)[1]','INT')       
						,C.value('(AmountEffected)[1]','DECIMAL(18,2)')      
						,@EnergyCharges
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(C)    	
					
					--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
					,ModifedBy=@ModifiedBy
					,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
					 
					--Updating the adjustment amount in Bill
					UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
					WHERE CustomerBillId=@CustomerBillId
					 
				END
				
				SELECT 
					(CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess
				FOR XML PATH('BillAdjustmentsBe'),TYPE
		 		
 --		END 
	--ELSE  
	--	BEGIN  
	--		SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillAdjustmentsBe')  
	--	END  
 	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
			,@VATPer DECIMAL(18,2)
			,@MeterNo VARCHAR(50)
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
   ,@VATPer = ISNULL(VATPercentage,0)
	,@MeterNo = MeterNo
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
  
   SET @NewConsumption= @Consumption-@AdjustedUnits; 
    
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * @VATPer)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				,MeterNumber
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @MeterNo END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_BillReadingAdjustment]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <24-JULY-2014>
-- Description:	<INSERT READING ADJUSTMENT DETAILS>
-- =============================================
ALTER PROCEDURE [dbo].[USP_Insert_BillReadingAdjustment]
(
	@XmlDoc xml=null
)
AS
BEGIN	
		DECLARE
			@BillAdjustmentId INT
			,@CustomerBillId INT
			,@AccountNo VARCHAR(50)
			,@CustomerId VARCHAR(50)
			,@AmountEffected DECIMAL(18,2)
			,@TaxEffected DECIMAL(18,2)
			,@TotalAmountEffected DECIMAL(18,2)
			,@BillAdjustmentType INT
			,@PreviousReading VARCHAR(50)
			,@CurrentReadingBeforeAdjustment VARCHAR(50)
			,@CurrentReadingAfterAdjustment VARCHAR(50)
			,@ApprovalStatusId INT
			,@Year INT
			,@Month INT
			,@ActualAmount DECIMAL(18,2)= 0    
			,@ModifiedAmount DECIMAL(18,2)= 0    
			,@NewConsumption DECIMAL(18,2)
			,@OldConsumption DECIMAL(18,2)
			,@BatchNo INT
			,@VATPer DECIMAL(18,2)
			,@MeterNo VARCHAR(50)
			
		SELECT
			@CustomerBillId = C.value('(CustomerBillId)[1]','INT')
			,@AccountNo	= C.value('(AccountNo)[1]','VARCHAR(50)')
			,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')
			,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','VARCHAR(50)')
			,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','VARCHAR(50)')
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')
			,@BatchNo = C.value('(BatchNo)[1]','INT')  
		FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)  		
 		
 		--SELECT @CustomerId = CustomerUniqueNo FROM Tbl_CustomerDetails WHERE AccountNo = @AccountNo
		
 		SELECT @CustomerId = GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @AccountNo 
		--Replacing Tbl_CustomerDetails with view [UDV_CustomerDescription]

 		
		SELECT 
			@Month = BillMonth
			,@Year = BillYear
			,@ActualAmount = NetEnergyCharges
			,@PreviousReading = PreviousReading
			,@VATPer = ISNULL(VATPercentage,0)
			,@MeterNo = MeterNo
		FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId
 		
 		SET @NewConsumption = CONVERT(DECIMAL(18,2),ISNULL(@CurrentReadingAfterAdjustment,0)) - CONVERT(DECIMAL(18,2),ISNULL(@PreviousReading,0))
 		SET @OldConsumption  = CONVERT(DECIMAL(18,2),ISNULL(@CurrentReadingBeforeAdjustment,0)) - CONVERT(DECIMAL(18,2),ISNULL(@PreviousReading,0))
 		SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year) 
		
		SET @AmountEffected = @ActualAmount - @ModifiedAmount
		
		SET @TaxEffected = (@AmountEffected * @VATPer)/100
 		SET @TotalAmountEffected = @AmountEffected + @TaxEffected
 		
 		INSERT INTO Tbl_BillAdjustments(
					CustomerBillId
					,AccountNo
					,CustomerId
					,AmountEffected
					,ApprovalStatusId
					,BillAdjustmentType
					,TaxEffected
					,TotalAmountEffected
					,AdjustedUnits
					,BatchNo
					,CreatedDate
					) 						
				VALUES(					
					@CustomerBillId
					,@AccountNo
					,@CustomerId
					,@AmountEffected
					,@ApprovalStatusId
					,@BillAdjustmentType
					,@TaxEffected
					,@TotalAmountEffected
					,(@OldConsumption - @NewConsumption)
					,@BatchNo
					,dbo.fn_GetCurrentDateTime()
					)		
		SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
		INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,PreviousReading,CurrentReadingAfterAdjustment,EnergryCharges,CurrentReadingBeforeAdjustment)
		SELECT       
			@BillAdjustmentId 
			,@PreviousReading
			,@CurrentReadingAfterAdjustment
			,@AmountEffected
			,@CurrentReadingBeforeAdjustment
	
		SELECT 
			(CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess
		FOR XML PATH('BillAdjustmentsBe'),TYPE
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
        
  DECLARE @GlobalAccountNumber VARCHAR(50), @ActiveStatusId INT 
 
	SELECT @GlobalAccountNumber = GlobalAccountNumber, @ActiveStatusId = ActiveStatusId FROM CUSTOMERS.Tbl_CustomersDetail 
			WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo

	IF( @GlobalAccountNumber IS NOT NULL)
		BEGIN
			IF ((SELECT [dbo].[fn_IsAccountNoExists_BU_Id_Payments](@GlobalAccountNumber,@BU_ID)) = 1)
				BEGIN
						SELECT  
						 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
						,CD.OldAccountNo--Faiz-ID103
						,CD.ClassName
						,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS AccNoAndGlobalAccNo
						,CD.OutStandingAmount AS DueAmount  --Commented By Raja-ID065
						,CD.GlobalAccountNumber AS AccountNo
						,'success' AS StatusText
						,(SELECT CustomerType FROM Tbl_MCustomerTypes CT WHERE CT.CustomerTypeId=CD.CustomerTypeId) AS CustomerType
						,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,NULL,CD.Service_ZipCode) AS Address1
					FROM [UDV_CustomerDescription] CD
					WHERE GlobalAccountNumber = @GlobalAccountNumber AND (BU_ID = @BU_ID OR @BU_ID = '')
					FOR XML PATH('MastersBE')  
				END
			ELSE IF(@ActiveStatusId = 4)
				BEGIN
					SELECT 'Customer status is Closed' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
			ELSE
				BEGIN
					SELECT 'Customer doesn''t exist in selected Bussiness Unit' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
		END
	ELSE
		BEGIN
			SELECT 'Customer doesn''t exist' AS StatusText FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBU_New]    Script Date: 07/23/2015 12:08:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustExistsByBU_New]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	--SET	@AccountNo= '12500450'
	--SET @BUID='BEDC_BU_0003'   
	--SET	 @Flag=	 ''
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	
	SELECT @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
	FROM  UDV_IsCustomerExists WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
			IF @CustomerBusinessUnitID =  @BUID OR @BUID =''    
			BEGIN
				IF @Flag=1
				BEGIN
					--PRINT 'Sucess and No Need to check the ActiveStatusID of the Customers'
					SELECT 1 AS IsSuccess,
						@CustomerActiveStatusID AS ActiveStatusId
					FOR XML PATH('RptCustomerLedgerBe')
				END
				ELSE
				BEGIN
					IF	 @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3 
						BEGIN
							--PRINT 'Sucess and Active  Customer'
							SELECT 1 AS IsSuccess
							FOR XML PATH('RptCustomerLedgerBe') 
						END
					ELSE
						BEGIN							 
							--PRINT 'Failure In Active Status'
							SELECT 0 AS IsSuccess,
								@CustomerActiveStatusID AS ActiveStatusId
							FOR XML PATH('RptCustomerLedgerBe') 
						END
				END
			END
			ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU 
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO



GO
/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 07/23/2015 13:27:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
			,@VATPer DECIMAL(18,2)
			,@MeterNo VARCHAR(50)
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
   ,@VATPer = ISNULL(VATPercentage,0)
	,@MeterNo = MeterNo
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
  
   SET @NewConsumption= @Consumption-@AdjustedUnits; 
    
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * @VATPer)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				,MeterNumber
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @MeterNo END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  
