
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportData]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- =============================================  
CREATE PROCEDURE [dbo].[USP_Insert_PDFReportData]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	SELECT  
		 @Month = C.value('(Month)[1]','INT')  
		,@Year = C.value('(Year)[1]','INT')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
	
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
	IF(@Month = 1)
		BEGIN
			SET @LastMonth = 12
			SET @LastYear = @Year - 1
		END
	ELSE
		BEGIN
			SET @LastMonth = @Month - 1
			SET @LastYear = @Year
		END
		
	SELECT 
		 CB.BU_ID
		,BU.BusinessUnitName
		,CB.SU_ID
		,SU.ServiceUnitName
		,CB.ServiceCenterId
		,SC.ServiceCenterName
		,CB.TariffId
		,TC.ClassName
		,CPD.CustomerTypeId
		,CT.CustomerType
		,CB.BillYear
		,CB.BillMonth
		,CB.AccountNo
		,CPD.ReadCodeID
		,CB.Usage
		,CB.NetFixedCharges
		,CB.NetEnergyCharges
		,CB.VATPercentage
		,CB.NetArrears
		,CB.VAT
		,CB.TotalBillAmount
		,CB.TotalBillAmountWithTax
		,CB.TotalBillAmountWithArrears
		,CB.ReadType
		,CB.PaidAmount
		,CAD.OutStandingAmount
		,CB.TotalBillAmountWithTax AS ClosingBalance
		,CB.BillNo
		,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
	INTO #CustomersList
	FROM Tbl_CustomerBills CB 
	INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
	INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
	INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
	INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
	INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
	WHERE BillMonth = @Month AND BillYear = @Year

	SELECT 
		  AccountNo
		 ,TotalBillAmountWithTax
	INTO #CustomerOpeningBalance
	FROM Tbl_CustomerBills
	WHERE BillMonth = @LastMonth AND BillYear = @LastYear

	SELECT 
		  CPD.BookNo
		 ,BN.CycleId
		 ,BG.ServiceCenterId
		 ,SC.SU_ID
		 ,SU.BU_ID
		 ,CPD.TariffClassID
		 ,CPD.CustomerTypeId
		 ,GlobalAccountNumber
	INTO #CustomerPopulation
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
	INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
	INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

	INSERT INTO Tbl_ReportBillingStatisticsBySCId
	(
		 BU_ID
		,BusinessUnitName
		,SU_ID
		,ServiceUnitName
		,SC_ID
		,ServiceCenterName
		,CustomerTypeId
		,CustomerType
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,EnergyDelivered
		,FixedCharges
		,NoOfBilledCustomers
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
					AND CP.ServiceCenterId = CL.ServiceCenterId 
					AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		
	INSERT INTO Tbl_ReportBillingStatisticsByTariffId
	(
		 BU_ID
		,BusinessUnitName
		,TariffId
		,TariffName
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,NoOfBilledCustomers
		,EnergyDelivered
		,FixedCharges
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID  
					AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth

	DROP TABLE #CustomersList
	DROP TABLE #CustomerOpeningBalance
	DROP TABLE #CustomerPopulation
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerStatisticsByTariff_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author  : Bhimaraju Vanka/Karteek Panakani
-- Create date : 23-July-2015    
-- Description : The purpose of this procedure is to Get Customer Statistics  by Tariff
-- =============================================    
CREATE PROCEDURE [dbo].[USP_CustomerStatisticsByTariff_New]  
(
@XmlDoc XML    
)
AS    
BEGIN    
   
 DECLARE	@BU_ID VARCHAR(20)
			,@Month INT  
			,@Year INT  
   
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@Month = C.value('(Month)[1]','INT')
		,@Year = C.value('(Year)[1]','INT')
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    


		SELECT	 TariffName AS Tariff
				,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
				,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
				,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
				,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1) As EnergyBilled
				,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1) As RevenueBilled
		FROM Tbl_ReportBillingStatisticsByTariffId
		WHERE (BU_ID=@BU_ID OR @BU_ID='')
		AND (YearId=@Year OR @Year=0)
		AND (MonthId=@Month OR @Month=0)
		GROUP BY TariffName
       
END    
  
  
---------------------------------------------------------------------------------------------------------  

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Billing status for customer service center  
-- =============================================      
CREATE PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter_New]   
 (  
 @XmlDoc XML=null    
 )   
AS    
BEGIN    
 
 SET NOCOUNT ON;    
 DECLARE @Month INT,  
   @Year INT,  
   @BU_ID VARCHAR(100)    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
  ,@Year = C.value('(Year)[1]','INT')    
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    

		SELECT CustomerType
			  ,ServiceCenterName AS ServiceCenter
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
			  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
			  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(MINFCCustomersCount) AS MONEY),-1),'.00','') AS [MINFC]
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ReadCustomersCount) AS MONEY),-1),'.00','') AS [Read]
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(DirectCustomersCount) AS MONEY),-1),'.00','') AS Direct
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ESTCustomersCount) AS MONEY),-1),'.00','') AS EST
			  ,CONVERT(VARCHAR(20),CAST(SUM(Payments) AS MONEY),-1) AS Payment
			  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
			  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
		FROM Tbl_ReportBillingStatisticsBySCId
		WHERE (BU_ID=@BU_ID OR @BU_ID='')
		AND (YearId=@Year OR @Year=0)
		AND (MonthId=@Month OR @Month=0)
		GROUP BY CustomerType,ServiceCenterName
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType_New] 
(      
@XmlDoc Xml=null      
)  
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	
	SELECT @Month = C.value('(Month)[1]','INT')      
	   ,@Year = C.value('(Year)[1]','INT')      
	   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
 
	SELECT   CustomerType
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ActiveCustomersCount,0)) AS MONEY),-1),'.00','') AS Active
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(InActiveCustomersCount,0)) AS MONEY),-1),'.00','')AS InActive
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation				
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1),'.00','') AS EnergyDelivered
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1)  AS EnergyBilled
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(RevenueBilled,0)) AS MONEY),-1)  AS RevenueBilled
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1)AS AmountBilled
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS WAT
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS PIM
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(MINFCCustomersCount,0)) AS MONEY),-1),'.00','') AS [MINFC]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ReadCustomersCount,0)) AS MONEY),-1),'.00','') AS [Read]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(DirectCustomersCount,0)) AS MONEY),-1),'.00','') AS Direct
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ESTCustomersCount,0)) AS MONEY),-1),'.00','') AS EST
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfBilledCustomers,0)) AS MONEY),-1),'.00','') AS NoOfBilled
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS Response
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalCollection
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS TotalAdjustment
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(OpeningBalance,0)) AS MONEY),-1) AS OpeningBalance
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ClosingBalance,0)) AS MONEY),-1) AS ClossingBalance
	FROM Tbl_ReportBillingStatisticsBySCId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY CustomerType	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Get customer statistics and tariff Info
-- =============================================   
CREATE PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN    
 SET NOCOUNT ON;  
 DECLARE @Month INT,    
   @Year INT,    
   @BU_ID VARCHAR(100)      
 
SELECT   @Month = C.value('(Month)[1]','INT')      
  ,@Year = C.value('(Year)[1]','INT')      
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)      

   SELECT TariffName AS Tariff		 
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(KVASold) AS MONEY),-1),'.00','') AS KWHSold
		 ,CONVERT(VARCHAR(20),CAST(SUM(FixedCharges) AS MONEY),-1) AS FixedCharges
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS NoBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1) AS TotalAmountBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountCollected) AS MONEY),-1) AS TotalAmountCollected
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS NoOfStubs
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(WeightedAvg) AS MONEY),-1),'.00','') AS WeightedAverage
   FROM Tbl_ReportBillingStatisticsByTariffId
   WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
   GROUP BY TariffName
   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariff_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	Description,,Get customer statistics and tariff
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariff_New] 
	(
	@XmlDoc XML
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    
    -- Insert statements for procedure here
    
    SELECT TariffName AS TAriff
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
		  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueCollected) AS MONEY),-1) AS RevenueCollected		  
		  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
		  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
    FROM Tbl_ReportBillingStatisticsByTariffId
    WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
    GROUP BY TariffName
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	Description,,Get customer statistics and tariff with parent classes
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses_New] 
	(
	@XmlDoc XML
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
    -- Insert statements for procedure here
    
    SELECT TariffName AS TariffClass
		  ,'' AS TariffDescription
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
		  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueCollected) AS MONEY),-1) AS RevenueCollected
		  ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountBilled) AS MONEY),-1) AS AmountBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
		  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
    FROM Tbl_ReportBillingStatisticsByTariffId
    WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
    GROUP BY TariffName
END



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo_New]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
CREATE PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo_New]
(
@XmlDoc Xml=null
) 
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(MAX)
	
	SELECT   @Month = C.value('(Month)[1]','INT')
			,@Year = C.value('(Year)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)
	
	SELECT TariffName AS Tariff
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1) AS KWHSold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(KVASold,0)) AS MONEY),-1) AS KVASold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(FixedCharges,0)) AS MONEY),-1) AS FixedCharges
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS NoBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalAmountCollected
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfStubs,0)) AS MONEY),-1),'.00','') AS NoOfStubs
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountBilled,0)) AS MONEY),-1) AS TotalAmountBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(WeightedAvg,0)) AS MONEY),-1) AS WeightedAverage
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1), '.00', '') AS TotalUsage
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1) AS TotalNetEnergyCharges
	FROM Tbl_ReportBillingStatisticsByTariffId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY TariffName
END

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT
	SELECT   
     @ARID=C.value('(ARID)[1]','INT') 
     DECLARE @Xml XML 
	SET @Xml=(SELECT 
				Title_Tenant AS TitleTanent
				,FirstName_Tenant AS FirstNameTanent
				,MiddleName_Tenant AS MiddleNameTanent
				,LastName_Tenant AS LastNameTanent
				,PhoneNumber AS PhoneNumberTanent
				,AlternatePhoneNumber AS AlternatePhoneNumberTanent
				,EmailID_Tenant AS EmailIdTanent
				,Title AS TitleLandlord
				,FirstName AS FirstNameLandlord
				,MiddleName AS MiddleNameLandlord
				,LastName AS LastNameLandlord
				,KnownAs AS KnownAs
				,HomeContactNumber AS HomeContactNumberLandlord
				,BusinessContactNumber AS BusinessPhoneNumberLandlord
				,OtherContactNumber AS OtherPhoneNumberLandlord
				,EmailId AS EmailIdLandlord
				,IsSameAsService AS IsSameAsService
				,IsSameAsTenent AS IsSameAsTenent
				,DocumentNo AS DocumentNo
				,ApplicationDate AS ApplicationDate
				,ConnectionDate AS ConnectionDate
				,SetupDate AS SetupDate
				,IsBEDCEmployee AS IsBEDCEmployee
				,IsVIPCustomer AS IsVIPCustomer
				,OldAccountNo AS OldAccountNo
				,CustomerTypeId AS CustomerTypeId
				,BookNo AS BookNo
				,PoleID AS PoleID
				,MeterNumber AS MeterNumber
				,TariffClassID AS TariffClassID
				,IsEmbassyCustomer AS IsEmbassyCustomer
				,EmbassyCode AS EmbassyCode
				,PhaseId AS PhaseId
				,ReadCodeID AS ReadCodeID
				,IdentityTypeId AS IdentityTypeId
				,IdentityNumber AS IdentityNumber
				,CertifiedBy AS CertifiedBy
				,Seal1 AS Seal1
				,Seal2 AS Seal2
				,ApplicationProcessedBy AS ApplicationProcessedBy
				,InstalledBy AS InstalledBy
				,AgencyId AS AgencyId
				,IsCAPMI AS IsCAPMI
				,MeterAmount AS MeterAmount
				,InitialBillingKWh AS InitialBillingKWh
				,InitialReading AS InitialReading
				,PresentReading AS PresentReading
				,HouseNo_Postal AS HouseNoPostal
				,StreetName_Postal AS StreetPostal
				,City_Postal AS CityPostaL
				,AreaCode_Postal AS AreaPostal
				,HouseNo_Service AS HouseNoService
				,StreetName_Service AS StreetService
				,City_Service AS CityService
				,AreaCode_Service AS AreaService
				,DocumentName AS DocumentName
				,[Path] AS [Path]
				,EmployeeCode AS EmployeeCode
				,ZipCode_Postal AS PZipCode
				,ZipCode_Service AS SZipCode
				,AccountTypeId AS AccountTypeId
				,ClusterCategoryId AS ClusterCategoryId
				,RouteSequenceNumber AS RouteSequenceNumber
				,MGActTypeID AS MGActTypeID
				,IsCommunication_Postal AS IsCommunicationPostal
				,IsCommunication_Service AS IsCommunicationService
				,IdentityTypeIdList AS IdentityTypeIdList
				,IdentityNumberList AS IdentityNumberList
				,UDFTypeIdList AS UDFTypeIdList
				,UDFValueList AS UDFValueList
				,CreatedBy AS CreatedBy
				,CreatedDate AS CreatedDate
				 FROM CUSTOMERS.Tbl_ApprovalRegistration
				 WHERE ARID = @ARID
				FOR XML PATH('CustomerRegistrationBE'),TYPE) 
	EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
END
GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 07/23/2015 19:42:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN   
BEGIN TRY  
 BEGIN TRAN   
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
      
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	
	SET @StatusText='Approval Registration Starts'
	
	INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,	ApprovalStatusId
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			,1
		)
	
	SET @StatusText='Approval Registration Ends'	
	 COMMIT TRAN   
END TRY   
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @IsSuccessful =0  
END CATCH  

SELECT   
 @IsSuccessful AS IsSuccessful  
 ,@StatusText AS StatusText   
 FOR XML PATH('CustomerRegistrationBE'),TYPE  	
END
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Billing status for customer service center  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter_New]   
 (  
 @XmlDoc XML=null    
 )   
AS    
BEGIN    
 
 SET NOCOUNT ON;    
 DECLARE @Month INT,  
   @Year INT,  
   @BU_ID VARCHAR(100)    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
  ,@Year = C.value('(Year)[1]','INT')    
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    

		SELECT CustomerType
			  ,ServiceCenterName AS ServiceCenter
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
			  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
			  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(MINFCCustomersCount) AS MONEY),-1),'.00','') AS [MINFC]
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ReadCustomersCount) AS MONEY),-1),'.00','') AS [Read]
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(DirectCustomersCount) AS MONEY),-1),'.00','') AS Direct
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ESTCustomersCount) AS MONEY),-1),'.00','') AS EST
			  ,CONVERT(VARCHAR(20),CAST(SUM(Payments) AS MONEY),-1) AS Payment
			  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
			  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
		FROM Tbl_ReportBillingStatisticsBySCId
		WHERE (BU_ID=@BU_ID OR @BU_ID='')
		AND (YearId=@Year OR @Year=0)
		AND (MonthId=@Month OR @Month=0)
		GROUP BY CustomerType,ServiceCenterName
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Get customer statistics and tariff Info
-- =============================================   
ALTER PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN    
 SET NOCOUNT ON;  
 DECLARE @Month INT,    
   @Year INT,    
   @BU_ID VARCHAR(100)      
 
SELECT   @Month = C.value('(Month)[1]','INT')      
  ,@Year = C.value('(Year)[1]','INT')      
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)      

   SELECT TariffName AS Tariff		 
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(KVASold) AS MONEY),-1),'.00','') AS KWHSold
		 ,CONVERT(VARCHAR(20),CAST(SUM(FixedCharges) AS MONEY),-1) AS FixedCharges
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS NoBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1) AS TotalAmountBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountCollected) AS MONEY),-1) AS TotalAmountCollected
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS NoOfStubs
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(WeightedAvg) AS MONEY),-1),'.00','') AS WeightedAverage
   FROM Tbl_ReportBillingStatisticsByTariffId
   WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
   GROUP BY TariffName
   
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType_New] 
(      
@XmlDoc Xml=null      
)  
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	
	SELECT @Month = C.value('(Month)[1]','INT')      
	   ,@Year = C.value('(Year)[1]','INT')      
	   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
 
	SELECT   CustomerType
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ActiveCustomersCount,0)) AS MONEY),-1),'.00','') AS Active
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(InActiveCustomersCount,0)) AS MONEY),-1),'.00','')AS InActive
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation				
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1),'.00','') AS EnergyDelivered
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1)  AS EnergyBilled
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(RevenueBilled,0)) AS MONEY),-1)  AS RevenueBilled
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1)AS AmountBilled
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS WAT
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS PIM
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(MINFCCustomersCount,0)) AS MONEY),-1),'.00','') AS [MINFC]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ReadCustomersCount,0)) AS MONEY),-1),'.00','') AS [Read]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(DirectCustomersCount,0)) AS MONEY),-1),'.00','') AS Direct
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ESTCustomersCount,0)) AS MONEY),-1),'.00','') AS EST
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfBilledCustomers,0)) AS MONEY),-1),'.00','') AS NoOfBilled
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS Response
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalCollection
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS TotalAdjustment
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(OpeningBalance,0)) AS MONEY),-1) AS OpeningBalance
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ClosingBalance,0)) AS MONEY),-1) AS ClossingBalance
	FROM Tbl_ReportBillingStatisticsBySCId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY CustomerType	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerStatisticsByTariff_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author  : Bhimaraju Vanka/Karteek Panakani
-- Create date : 23-July-2015    
-- Description : The purpose of this procedure is to Get Customer Statistics  by Tariff
-- =============================================    
ALTER PROCEDURE [dbo].[USP_CustomerStatisticsByTariff_New]  
(
@XmlDoc XML    
)
AS    
BEGIN    
   
 DECLARE	@BU_ID VARCHAR(20)
			,@Month INT  
			,@Year INT  
   
SELECT	@BU_ID = C.value('(BU_ID)[1]','VARCHAR(20)')
		,@Month = C.value('(Month)[1]','INT')
		,@Year = C.value('(Year)[1]','INT')
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    


		SELECT	 TariffName AS Tariff
				,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
				,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
				,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
				,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1) As EnergyBilled
				,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1) As RevenueBilled
		FROM Tbl_ReportBillingStatisticsByTariffId
		WHERE (BU_ID=@BU_ID OR @BU_ID='')
		AND (YearId=@Year OR @Year=0)
		AND (MonthId=@Month OR @Month=0)
		GROUP BY TariffName
       
END    
  
  
---------------------------------------------------------------------------------------------------------  

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	Description,,Get customer statistics and tariff with parent classes
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariffClasses_New] 
	(
	@XmlDoc XML
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
    -- Insert statements for procedure here
    
    SELECT TariffName AS TariffClass
		  ,'' AS TariffDescription
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
		  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueCollected) AS MONEY),-1) AS RevenueCollected
		  ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountBilled) AS MONEY),-1) AS AmountBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
		  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
    FROM Tbl_ReportBillingStatisticsByTariffId
    WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
    GROUP BY TariffName
END



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo_New]
(
@XmlDoc Xml=null
) 
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(MAX)
	
	SELECT   @Month = C.value('(Month)[1]','INT')
			,@Year = C.value('(Year)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)
	
	SELECT TariffName AS Tariff
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1) AS KWHSold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(KVASold,0)) AS MONEY),-1) AS KVASold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(FixedCharges,0)) AS MONEY),-1) AS FixedCharges
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS NoBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalAmountCollected
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfStubs,0)) AS MONEY),-1),'.00','') AS NoOfStubs
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountBilled,0)) AS MONEY),-1) AS TotalAmountBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(WeightedAvg,0)) AS MONEY),-1) AS WeightedAverage
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1), '.00', '') AS TotalUsage
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1) AS TotalNetEnergyCharges
	FROM Tbl_ReportBillingStatisticsByTariffId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY TariffName
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsAndTariff_New]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	Description,,Get customer statistics and tariff
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsAndTariff_New] 
	(
	@XmlDoc XML
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,  
			@Year INT,  
			@BU_ID VARCHAR(100)    
 
SELECT   @Month = C.value('(Month)[1]','INT')    
		,@Year = C.value('(Year)[1]','INT')    
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    
    -- Insert statements for procedure here
    
    SELECT TariffName AS TAriff
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
		  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueCollected) AS MONEY),-1) AS RevenueCollected		  
		  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
		  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
    FROM Tbl_ReportBillingStatisticsByTariffId
    WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
    GROUP BY TariffName
END

GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportData]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportData]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	SELECT  
		 @Month = C.value('(Month)[1]','INT')  
		,@Year = C.value('(Year)[1]','INT')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
	
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
	IF(@Month = 1)
		BEGIN
			SET @LastMonth = 12
			SET @LastYear = @Year - 1
		END
	ELSE
		BEGIN
			SET @LastMonth = @Month - 1
			SET @LastYear = @Year
		END
		
	SELECT 
		 CB.BU_ID
		,BU.BusinessUnitName
		,CB.SU_ID
		,SU.ServiceUnitName
		,CB.ServiceCenterId
		,SC.ServiceCenterName
		,CB.TariffId
		,TC.ClassName
		,CPD.CustomerTypeId
		,CT.CustomerType
		,CB.BillYear
		,CB.BillMonth
		,CB.AccountNo
		,CPD.ReadCodeID
		,CB.Usage
		,CB.NetFixedCharges
		,CB.NetEnergyCharges
		,CB.VATPercentage
		,CB.NetArrears
		,CB.VAT
		,CB.TotalBillAmount
		,CB.TotalBillAmountWithTax
		,CB.TotalBillAmountWithArrears
		,CB.ReadType
		,CB.PaidAmount
		,CAD.OutStandingAmount
		,CB.TotalBillAmountWithTax AS ClosingBalance
		,CB.BillNo
		,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
	INTO #CustomersList
	FROM Tbl_CustomerBills CB 
	INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
	INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
	INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
	INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
	INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
	WHERE BillMonth = @Month AND BillYear = @Year

	SELECT 
		  AccountNo
		 ,TotalBillAmountWithTax
	INTO #CustomerOpeningBalance
	FROM Tbl_CustomerBills
	WHERE BillMonth = @LastMonth AND BillYear = @LastYear

	SELECT 
		  CPD.BookNo
		 ,BN.CycleId
		 ,BG.ServiceCenterId
		 ,SC.SU_ID
		 ,SU.BU_ID
		 ,CPD.TariffClassID
		 ,CPD.CustomerTypeId
		 ,GlobalAccountNumber
	INTO #CustomerPopulation
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
	INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
	INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

	INSERT INTO Tbl_ReportBillingStatisticsBySCId
	(
		 BU_ID
		,BusinessUnitName
		,SU_ID
		,ServiceUnitName
		,SC_ID
		,ServiceCenterName
		,CustomerTypeId
		,CustomerType
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,EnergyDelivered
		,FixedCharges
		,NoOfBilledCustomers
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
					AND CP.ServiceCenterId = CL.ServiceCenterId 
					AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		
	INSERT INTO Tbl_ReportBillingStatisticsByTariffId
	(
		 BU_ID
		,BusinessUnitName
		,TariffId
		,TariffName
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,NoOfBilledCustomers
		,EnergyDelivered
		,FixedCharges
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID  
					AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth

	DROP TABLE #CustomersList
	DROP TABLE #CustomerOpeningBalance
	DROP TABLE #CustomerPopulation
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=15)
	BEGIN
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN   
BEGIN TRY  
 BEGIN TRAN   
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
      
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	
	SET @StatusText='Approval Registration Starts'
	
	INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,	ApprovalStatusId
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			,1
		)
	
	SET @StatusText='Approval Registration Ends'	
	 COMMIT TRAN   
END TRY   
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @IsSuccessful =0  
END CATCH  

SELECT   
 @IsSuccessful AS IsSuccessful  
 ,@StatusText AS StatusText   
 FOR XML PATH('CustomerRegistrationBE'),TYPE  	
END
GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT
	SELECT   
     @ARID=C.value('(ARID)[1]','INT') 
     DECLARE @Xml XML 
	SET @Xml=(SELECT 
				Title_Tenant AS TitleTanent
				,FirstName_Tenant AS FirstNameTanent
				,MiddleName_Tenant AS MiddleNameTanent
				,LastName_Tenant AS LastNameTanent
				,PhoneNumber AS PhoneNumberTanent
				,AlternatePhoneNumber AS AlternatePhoneNumberTanent
				,EmailID_Tenant AS EmailIdTanent
				,Title AS TitleLandlord
				,FirstName AS FirstNameLandlord
				,MiddleName AS MiddleNameLandlord
				,LastName AS LastNameLandlord
				,KnownAs AS KnownAs
				,HomeContactNumber AS HomeContactNumberLandlord
				,BusinessContactNumber AS BusinessPhoneNumberLandlord
				,OtherContactNumber AS OtherPhoneNumberLandlord
				,EmailId AS EmailIdLandlord
				,IsSameAsService AS IsSameAsService
				,IsSameAsTenent AS IsSameAsTenent
				,DocumentNo AS DocumentNo
				,ApplicationDate AS ApplicationDate
				,ConnectionDate AS ConnectionDate
				,SetupDate AS SetupDate
				,IsBEDCEmployee AS IsBEDCEmployee
				,IsVIPCustomer AS IsVIPCustomer
				,OldAccountNo AS OldAccountNo
				,CustomerTypeId AS CustomerTypeId
				,BookNo AS BookNo
				,PoleID AS PoleID
				,MeterNumber AS MeterNumber
				,TariffClassID AS TariffClassID
				,IsEmbassyCustomer AS IsEmbassyCustomer
				,EmbassyCode AS EmbassyCode
				,PhaseId AS PhaseId
				,ReadCodeID AS ReadCodeID
				,IdentityTypeId AS IdentityTypeId
				,IdentityNumber AS IdentityNumber
				,CertifiedBy AS CertifiedBy
				,Seal1 AS Seal1
				,Seal2 AS Seal2
				,ApplicationProcessedBy AS ApplicationProcessedBy
				,InstalledBy AS InstalledBy
				,AgencyId AS AgencyId
				,IsCAPMI AS IsCAPMI
				,MeterAmount AS MeterAmount
				,InitialBillingKWh AS InitialBillingKWh
				,InitialReading AS InitialReading
				,PresentReading AS PresentReading
				,HouseNo_Postal AS HouseNoPostal
				,StreetName_Postal AS StreetPostal
				,City_Postal AS CityPostaL
				,AreaCode_Postal AS AreaPostal
				,HouseNo_Service AS HouseNoService
				,StreetName_Service AS StreetService
				,City_Service AS CityService
				,AreaCode_Service AS AreaService
				,DocumentName AS DocumentName
				,[Path] AS [Path]
				,EmployeeCode AS EmployeeCode
				,ZipCode_Postal AS PZipCode
				,ZipCode_Service AS SZipCode
				,AccountTypeId AS AccountTypeId
				,ClusterCategoryId AS ClusterCategoryId
				,RouteSequenceNumber AS RouteSequenceNumber
				,MGActTypeID AS MGActTypeID
				,IsCommunication_Postal AS IsCommunicationPostal
				,IsCommunication_Service AS IsCommunicationService
				,IdentityTypeIdList AS IdentityTypeIdList
				,IdentityNumberList AS IdentityNumberList
				,UDFTypeIdList AS UDFTypeIdList
				,UDFValueList AS UDFValueList
				,CreatedBy AS CreatedBy
				,CreatedDate AS CreatedDate
				 FROM CUSTOMERS.Tbl_ApprovalRegistration
				 WHERE ARID = @ARID
				FOR XML PATH('CustomerRegistrationBE'),TYPE) 
	EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
END
GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- Modified By: NEERAJ KANOJIYA
-- Modified Date: 26-JUNE-2015
-- Description: @AccountNo was incorrect in update statement for outstanding, modified with @GlobalAccountNo.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@AmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT,
				@BatchId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate
	,@BatchId							=BatchId
	,@AmountEffected					=(TotalAmountEffected - TaxEffected)
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			
			DECLARE @TotalAmountEffectedToCal DECIMAL(18,2) = 0, @TotalAdjustedUnits BIGINT = 0
			DECLARE @TotalAdditionalChanrges DECIMAL(18,2) = 0, @TotalConsumption BIGINT = 0
			
			SELECT 
				 @TotalAdditionalChanrges = ISNULL(NetFixedCharges,0)
				,@TotalConsumption = CONVERT(BIGINT,ISNULL(Usage,0))
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber
			
			SELECT 
				 @TotalAmountEffectedToCal = SUM(ISNULL(AmountEffected,0))
			FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber AND BillAdjustmentType = 2
			
			SELECT 
				 @TotalAdjustedUnits = SUM(ISNULL(AdjustedUnits,0))
			FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber AND BillAdjustmentType = 3
			
			IF(ISNULL(@TotalAdditionalChanrges,0) < (ISNULL(@AmountEffected,0) + ISNULL(@TotalAmountEffectedToCal,0)) AND @BillAdjustmentTypeId = 2)
				BEGIN
					SELECT 0 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
			ELSE IF(ISNULL(@TotalConsumption,0) < (ISNULL(@AdjustedUnits,0) + ISNULL(@TotalAdjustedUnits,0)) AND @BillAdjustmentTypeId = 3)
				BEGIN
					SELECT 0 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
			ELSE 
				BEGIN
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
						BEGIN
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
							--						WHERE AdjustmentLogId = @AdjustmentLogId))

							--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
							
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
													WHERE AdjustmentLogId = @AdjustmentLogId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
										FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
							DECLARE @FinalApproval BIT

							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
								BEGIN
										SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
								END
							ELSE
								BEGIN
									DECLARE @UserDetailedId INT
									SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
									SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
								END			
							
							
							IF(@FinalApproval =1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_AdjustmentsLogs 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApproveStatusId = @ApprovalStatusId
										,CurrentApprovalLevel=0
										,IsLocked = 1
									WHERE GlobalAccountNumber = @GlobalAccountNo 
									AND AdjustmentLogId = @AdjustmentLogId 

									INSERT INTO Tbl_BillAdjustments(
										 AccountNo    
										 --,CustomerId    
										 ,AmountEffected   
										 ,BillAdjustmentType    
										 ,TotalAmountEffected
										 ,TaxEffected
										 ,AdjustedUnits
										 ,CustomerBillId
										 ,ApprovedBy
										 ,CreatedBy
										 ,CreatedDate 
										 ,BatchNo  
										 )           
										VALUES(         
										  @GlobalAccountNo    
										 --,@CustomerId    
										 ,@AmountEffected
										 ,@BillAdjustmentTypeId   
										 ,@TotalAmountEffected
										 ,@TaxEffected
										 ,@AdjustedUnits
										 ,@BillNumber
										 ,@ModifedBy
										 ,@CreatedBy
										 ,@CreatedDate
										 ,@BatchId 
										 )      
									SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
									
									INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges,PreviousReading,CurrentReadingAfterAdjustment,AdditionalCharges,CurrentReadingBeforeAdjustment,AdditionalChargesID)
									SELECT @BillAdjustmentId,@EnergryCharges,@PreviousReading,@CurrentReadingAfterAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN @AmountEffected ELSE NULL END),@CurrentReadingBeforeAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN 1 ELSE 0 END)
									
									----OutStanding Amount is Updating Start
									
									--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
									--WHERE GlobalAccountNumber=@GlobalAccountNo
									
									--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
									
									--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
									--SET OutStandingAmount=@OutStandingAmount
									--WHERE GlobalAccountNumber=@GlobalAccountNo
									----OutStanding Amount is Updating End
									
									UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
										,ModifedBy=@ModifiedBy
										,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
									
									INSERT INTO Tbl_Audit_AdjustmentsLogs 
										(	 AdjustmentLogId
											,GlobalAccountNumber 
											,AccountNo 
											,BillNumber 
											,BillAdjustmentTypeId  
											,MeterNumber 
											,PreviousReading 
											,CurrentReadingAfterAdjustment 
											,CurrentReadingBeforeAdjustment 
											,AdjustedUnits 
											,TaxEffected 
											,TotalAmountEffected 
											,EnergryCharges 
											,AdditionalCharges 
											,PresentApprovalRole
											,NextApprovalRole  
											,ApproveStatusId  
											,Remarks
											,CreatedBy 
											,CreatedDate  
											,ModifedBy 
											,ModifiedDate
										)
									VALUES (@AdjustmentLogId
											,@GlobalAccountNo
											,@AccountNo 
											,@BillNumber 
											,@BillAdjustmentTypeId  
											,@MeterNumber 
											,@PreviousReading 
											,@CurrentReadingAfterAdjustment 
											,@CurrentReadingBeforeAdjustment 
											,@AdjustedUnits 
											,@TaxEffected 
											,@TotalAmountEffected 
											,@EnergryCharges 
											,@AdditionalCharges 
											,@PresentRoleId
											,@NextRoleId
											,@ApprovalStatusId
											,@Remarks
											,@CreatedBy 
											,@CreatedDate  
											,@ModifedBy 
											,dbo.fn_GetCurrentDateTime())
										
								END
							ELSE -- Not a final approval
								BEGIN
									
									UPDATE Tbl_AdjustmentsLogs 
									SET   -- Updating Request with Level Roles
										 ModifedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
										,CurrentApprovalLevel = CurrentApprovalLevel+1
										,IsLocked = 1
									WHERE GlobalAccountNumber = @GlobalAccountNo 
									AND AdjustmentLogId = @AdjustmentLogId
									
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							
								UPDATE Tbl_AdjustmentsLogs 
								SET   -- Updating Request with Level Roles & Approval status 
									 ModifedBy = @ModifiedBy
									,ModifiedDate = dbo.fn_GetCurrentDateTime()
									,PresentApprovalRole = @PresentRoleId
									,NextApprovalRole = @NextRoleId 
									,ApproveStatusId = @ApprovalStatusId
									,CurrentApprovalLevel= 0
									,IsLocked=1
								WHERE GlobalAccountNumber = @GlobalAccountNo 
								AND AdjustmentLogId = @AdjustmentLogId 

								INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,TaxEffected
								 ,AdjustedUnits
								 ,CustomerBillId
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate 
								 ,BatchNo  
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@AmountEffected
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@TaxEffected
								 ,@AdjustedUnits
								 ,@BillNumber
								 ,@ModifedBy
								 ,@CreatedBy
								 ,@CreatedDate
								 ,@BatchId 
								 )      
								SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
			   
								INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges,PreviousReading,CurrentReadingAfterAdjustment,AdditionalCharges,CurrentReadingBeforeAdjustment,AdditionalChargesID)
								SELECT @BillAdjustmentId,@EnergryCharges,@PreviousReading,@CurrentReadingAfterAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN @AmountEffected ELSE NULL END),@CurrentReadingBeforeAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN 1 ELSE 0 END)
														
								----OutStanding Amount is Updating Start
									
								--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
								--	WHERE GlobalAccountNumber=@GlobalAccountNo
									
								--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
									
								--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
								--	SET OutStandingAmount=@OutStandingAmount
								--	WHERE GlobalAccountNumber=@GlobalAccountNo
								--	--OutStanding Amount is Updating End
								
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
								
								INSERT INTO Tbl_Audit_AdjustmentsLogs 
										(	 AdjustmentLogId
											,GlobalAccountNumber 
											,AccountNo 
											,BillNumber 
											,BillAdjustmentTypeId  
											,MeterNumber 
											,PreviousReading 
											,CurrentReadingAfterAdjustment 
											,CurrentReadingBeforeAdjustment 
											,AdjustedUnits 
											,TaxEffected 
											,TotalAmountEffected 
											,EnergryCharges 
											,AdditionalCharges 
											,PresentApprovalRole
											,NextApprovalRole  
											,ApproveStatusId  
											,Remarks
											,CreatedBy 
											,CreatedDate  
											,ModifedBy 
											,ModifiedDate
										)
									VALUES (@AdjustmentLogId
											,@GlobalAccountNo
											,@AccountNo 
											,@BillNumber 
											,@BillAdjustmentTypeId  
											,@MeterNumber 
											,@PreviousReading 
											,@CurrentReadingAfterAdjustment 
											,@CurrentReadingBeforeAdjustment 
											,@AdjustedUnits 
											,@TaxEffected 
											,@TotalAmountEffected 
											,@EnergryCharges 
											,@AdditionalCharges 
											,@PresentRoleId
											,@NextRoleId
											,@ApprovalStatusId
											,@Remarks
											,@CreatedBy 
											,@CreatedDate  
											,@ModifedBy 
											,dbo.fn_GetCurrentDateTime())					
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END

GO

/****** Object:  StoredProcedure [dbo].[USP_MeterConsumptionAdjustment]    Script Date: 07/23/2015 19:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================  
--AUTHOR  : Neeraj Kanojiya  
--Created Date : 25-Aug-2014  
--Description : Update consumption adjustment  
--===================================  
ALTER PROCEDURE [dbo].[USP_MeterConsumptionAdjustment]  
(  
@XmlDoc XML  
)  
AS  
BEGIN   
  DECLARE  
		@BillAdjustmentId INT  
		,@CustomerBillId INT  
		,@AccountNo VARCHAR(50)  
		,@CustomerId VARCHAR(50)  
		,@AmountEffected DECIMAL(18,2)  
		,@TaxEffected DECIMAL(18,2)  
		,@EnergyCharges DECIMAL(18,2)  
		,@AdditionalCharges DECIMAL(18,2)  
		,@TotalAmountEffected DECIMAL(18,2)  
		,@BillAdjustmentType INT  
		,@PreviousReading VARCHAR(50)  
		,@CurrentReadingAfterAdjustment  VARCHAR(50)  
		,@CurrentReadingBeforeAdjustment   VARCHAR(50)  
		,@AdjustedUnits INT  
		,@ApprovalStatusId INT  
		,@Year INT  
		,@Month INT  
		,@ActualAmount DECIMAL(18,2)= 0      
		,@ModifiedAmount DECIMAL(18,2)= 0      
		,@Consumption INT  
		,@NewConsumption DECIMAL(18,2)= 0   
		,@BatchNo INT
		,@ModifiedBy VARCHAR(50) 
		,@IsFinalApproval BIT = 0
		,@FunctionId INT
		,@Details VARCHAR(MAX)   
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
			,@VATPer DECIMAL(18,2)
			,@MeterNo VARCHAR(50)
     
  SELECT  
   @CustomerBillId = C.value('(CustomerBillId)[1]','INT')  
   ,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')  
   ,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')  
   ,@AdjustedUnits = C.value('(AdjustedUnits)[1]','INT')  
   ,@Consumption = C.value('(Consumption)[1]','INT')  
   ,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
   ,@BatchNo = C.value('(BatchNo)[1]','INT')  
	,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')  
	,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
	,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
	,@PreviousReading = C.value('(PreviousReading)[1]','DECIMAL(18,2)')
	,@CurrentReadingAfterAdjustment = C.value('(CurrentReadingAfterAdjustment)[1]','DECIMAL(18,2)')
	,@CurrentReadingBeforeAdjustment = C.value('(CurrentReadingBeforeAdjustment)[1]','DECIMAL(18,2)')
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)      

  SELECT   
   @Month = BillMonth  
   ,@Year = BillYear  
   ,@ActualAmount = NetEnergyCharges  
   ,@PreviousReading = PreviousReading  
   ,@VATPer = ISNULL(VATPercentage,0)
	,@MeterNo = MeterNo
  FROM Tbl_CustomerBills WHERE CustomerBillId = @CustomerBillId  
  
   SET @NewConsumption= @Consumption-@AdjustedUnits; 
    
   SELECT @ModifiedAmount = [dbo].[fn_CaluculateBill_Consumption](@AccountNo,@NewConsumption,@Month,@Year)   
    
  SET @AmountEffected = @ActualAmount - @ModifiedAmount  
    
  SET @TaxEffected = (@AmountEffected * @VATPer)/100  
   SET @TotalAmountEffected = @AmountEffected + @TaxEffected  
     
----------------------------
----------------------------

IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
				IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,PreviousReading
				,CurrentReadingAfterAdjustment
				,CurrentReadingBeforeAdjustment
				,AdjustedUnits
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				,MeterNumber
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
							,@AdjustedUnits
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
							,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @MeterNo END
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
			WHERE CD.GlobalAccountNumber=@AccountNo
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
				   INSERT INTO Tbl_BillAdjustments(  
							 CustomerBillId  
							 ,AccountNo  
							 ,AmountEffected  
							 ,ApprovalStatusId  
							 ,BillAdjustmentType  
							 ,TaxEffected  
							 ,TotalAmountEffected  
							 ,AdjustedUnits  
							,Remarks 
							 ,BatchNo
							,CreatedBy 
							 ,CreatedDate
							 ,ApprovedBy
							 )         
							VALUES(       
							 @CustomerBillId  
							 ,@AccountNo  
							 ,@AmountEffected  
							 ,@ApprovalStatusId  
							 ,@BillAdjustmentType  
							 ,@TaxEffected  
							 ,@TotalAmountEffected  
							 ,@AdjustedUnits  
							,@Details 
							 ,@BatchNo
							,@ModifiedBy 
							 ,dbo.fn_GetCurrentDateTime()
							 ,@ModifiedBy
							 )    
						  SELECT @BillAdjustmentId = SCOPE_IDENTITY()  
						     
						  INSERT INTO Tbl_BillAdjustmentDetails
								(
								BillAdjustmentId
								,EnergryCharges
								,PreviousReading
								,CurrentReadingBeforeAdjustment
								,CurrentReadingAfterAdjustment
								,CreatedBy
								,CreatedDate
								)  
								SELECT         
								@BillAdjustmentId   
								,@AmountEffected 
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @PreviousReading END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingBeforeAdjustment END
								,CASE WHEN CPD.ReadCodeID=1 THEN NULL ELSE @CurrentReadingAfterAdjustment END
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime() 
								FROM CUSTOMERS.Tbl_CustomersDetail CD 
								INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
								WHERE CD.GlobalAccountNumber=@AccountNo
								
								--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
								,ModifedBy=@ModifiedBy
								,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
									
								--Updating the adjustment amount in Bill
								UPDATE Tbl_CustomerBills SET AdjustmentAmmount= AdjustmentAmmount+@TotalAmountEffected 
								WHERE CustomerBillId=@CustomerBillId
								
						
			END
		    SELECT   
			   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess  
				 FOR XML PATH('BillAdjustmentsBe'),TYPE   
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END  
  
END


  

GO


