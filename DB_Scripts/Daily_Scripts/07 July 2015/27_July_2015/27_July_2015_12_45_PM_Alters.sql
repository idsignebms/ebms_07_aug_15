
GO
CREATE TABLE Tbl_MCommunicationFeatures
(
	CommunicationFeaturesID INT PRIMARY KEY IDENTITY (1,1),
	FeatureName Varchar(200),
	IsEmailFeature BIT,
	IsSMSFeature BIT,
	CreatedBy Varchar(50),
	CreatedDate DateTime,
	ModifiedBy Varchar(50),
	ModifiedDate datetime,
	ActiveStatusId INT Default 1
)
GO
INSERT INTO Tbl_MCommunicationFeatures(FeatureName,IsEmailFeature,IsSMSFeature)
VALUES('BillGeneration',1,1)
GO
INSERT INTO Tbl_MCommunicationFeatures(FeatureName,IsEmailFeature,IsSMSFeature)
VALUES('Payments',1,1)
GO
INSERT INTO Tbl_MCommunicationFeatures(FeatureName,IsEmailFeature,IsSMSFeature)
VALUES('Adjustments',1,1)
GO
DROP TABLE Tbl_ReportBillingStatisticsBySCId
GO
DROP TABLE Tbl_ReportBillingStatisticsByTariffId
GO
CREATE TABLE Tbl_ReportBillingStatisticsBySCId
(
	 ReportBillingStatisticsBySCId BIGINT IDENTITY(1,1) PRIMARY KEY
	,BU_ID VARCHAR(50)
	,BusinessUnitName VARCHAR(100)
	,SU_ID VARCHAR(50)
	,ServiceUnitName VARCHAR(100)
	,SC_ID VARCHAR(50)
	,ServiceCenterName VARCHAR(100)
	,CustomerTypeId INT
	,CustomerType VARCHAR(100)
	,YearId INT
	,MonthId INT
	,ActiveCustomersCount BIGINT
	,InActiveCustomersCount BIGINT
	,HoldCustomersCount BIGINT
	,TotalPopulationCount BIGINT
	,EnergyDelivered DECIMAL(20,4)
	,FixedCharges DECIMAL(20,4)
	,NoOfBilledCustomers BIGINT
	,TotalAmountBilled DECIMAL(20,4)
	,MINFCCustomersCount BIGINT
	,ReadCustomersCount BIGINT
	,ESTCustomersCount BIGINT
	,DirectCustomersCount BIGINT
	,EnergyBilled DECIMAL(20,4)
	,RevenueBilled DECIMAL(20,4)
	,Payments DECIMAL(20,4)
	,OpeningBalance DECIMAL(20,4)
	,ClosingBalance DECIMAL(20,4)
	,RevenueCollected DECIMAL(20,4)
	,KVASold BIGINT
	,TotalAmountCollected DECIMAL(20,4)
	,NoOfStubs BIGINT
	,WeightedAvg DECIMAL(20,4)
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
)
GO
CREATE TABLE Tbl_ReportBillingStatisticsByTariffId
(
	 ReportBillingStatisticsByTariffId BIGINT IDENTITY(1,1) PRIMARY KEY
	,BU_ID VARCHAR(50)
	,BusinessUnitName VARCHAR(100)
	,TariffId VARCHAR(50)
	,TariffName VARCHAR(100)
	,YearId INT
	,MonthId INT
	,ActiveCustomersCount BIGINT
	,InActiveCustomersCount BIGINT
	,HoldCustomersCount BIGINT
	,TotalPopulationCount BIGINT
	,NoOfBilledCustomers BIGINT
	,EnergyDelivered DECIMAL(20,4)
	,FixedCharges DECIMAL(20,4)
	,TotalAmountBilled DECIMAL(20,4)
	,MINFCCustomersCount BIGINT
	,ReadCustomersCount BIGINT
	,ESTCustomersCount BIGINT
	,DirectCustomersCount BIGINT
	,EnergyBilled DECIMAL(20,4)
	,RevenueBilled DECIMAL(20,4)
	,Payments DECIMAL(20,4)
	,OpeningBalance DECIMAL(20,4)
	,ClosingBalance DECIMAL(20,4)
	,RevenueCollected DECIMAL(20,4)
	,KVASold BIGINT
	,TotalAmountCollected DECIMAL(20,4)
	,NoOfStubs BIGINT
	,WeightedAvg DECIMAL(20,4)
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
)
GO