
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCommunicationFeatures]    Script Date: 07/27/2015 13:24:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To Update the email & sms features details
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateCommunicationFeatures]
	@XmlDoc XML
AS
BEGIN

	DECLARE		@IsEmailEnabledBillGeneration BIT, 
				@IsSMSEnabledBillGeneration BIT, 
				@IsEmailEnabledPayments BIT, 
				@IsSMSEnabledPayments BIT, 
				@IsEmailEnabledAdjustments BIT, 
				@IsSMSEnabledAdjustments BIT
	
	SELECT         
		@IsEmailEnabledBillGeneration = C.value('(IsEmailEnabledBillGeneration)[1]','BIT'),
		@IsSMSEnabledBillGeneration = C.value('(IsSMSEnabledBillGeneration)[1]','BIT'),
		@IsEmailEnabledPayments = C.value('(IsEmailEnabledPayments)[1]','BIT'),
		@IsSMSEnabledPayments = C.value('(IsSMSEnabledPayments)[1]','BIT'),
		@IsEmailEnabledAdjustments = C.value('(IsEmailEnabledAdjustments)[1]','BIT'),
		@IsSMSEnabledAdjustments = C.value('(IsSMSEnabledAdjustments)[1]','BIT')
	FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)       
	
	UPDATE Tbl_MCommunicationFeatures 
	SET IsEmailFeature=@IsEmailEnabledBillGeneration,IsSMSFeature=@IsSMSEnabledBillGeneration
	WHERE CommunicationFeaturesID=1--For Bill Generation
	
	UPDATE Tbl_MCommunicationFeatures 
	SET IsEmailFeature=@IsEmailEnabledPayments,IsSMSFeature=@IsSMSEnabledPayments
	WHERE CommunicationFeaturesID=2--For Payments
	
	UPDATE Tbl_MCommunicationFeatures 
	SET IsEmailFeature=@IsEmailEnabledAdjustments,IsSMSFeature=@IsSMSEnabledAdjustments
	WHERE CommunicationFeaturesID=3--For Adjustments
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('CommunicationFeaturesBE'),TYPE     
      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCommunicationFeatures]    Script Date: 07/27/2015 13:24:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To get the email & sms features details
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCommunicationFeatures]
	
AS
BEGIN
	SELECT CommunicationFeaturesID,FeatureName,IsEmailFeature,IsSMSFeature,ActiveStatusId 
	From Tbl_MCommunicationFeatures
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsSMSFeatureEnabled]    Script Date: 07/27/2015 13:24:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To check for given Communication Feature for IsSMSEnabled
-- =============================================
CREATE PROCEDURE [dbo].[USP_IsSMSFeatureEnabled]
	@XmlDoc XML
AS
BEGIN
DECLARE		@CommunicationFeaturesID INT 
          
  SELECT         
		@CommunicationFeaturesID = C.value('(CommunicationFeaturesID)[1]','INT')  
  FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)       
  
  SELECT IsSMSFeature FROM Tbl_MCommunicationFeatures WHERE CommunicationFeaturesID=@CommunicationFeaturesID
  FOR XML PATH('CommunicationFeaturesBE'),TYPE     
  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsEmailFeatureEnabled]    Script Date: 07/27/2015 13:24:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To check for given Communication Feature for IsEmailEnabled
-- =============================================
CREATE PROCEDURE [dbo].[USP_IsEmailFeatureEnabled]
	@XmlDoc XML
AS
BEGIN
DECLARE		@CommunicationFeaturesID INT 
          
  SELECT         
		@CommunicationFeaturesID = C.value('(CommunicationFeaturesID)[1]','INT')  
  FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)       
  
  SELECT IsEmailFeature FROM Tbl_MCommunicationFeatures WHERE CommunicationFeaturesID=@CommunicationFeaturesID
  FOR XML PATH('CommunicationFeaturesBE'),TYPE     
  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrations]    Script Date: 07/27/2015 13:24:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetApprovalRegistrations]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT  
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)     
 
	SELECT    
	   --ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber  
	   CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId   
	  ,CD.ReadCodeID as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON CD.ApprovalStatusId = APS.ApprovalStatusId  
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 ORDER BY CD.CreatedDate desc  
END	
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrations]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrations]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT  
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)     
 
	SELECT    
	   --ROW_NUMBER() OVER (ORDER BY T.GlobalAccountNumber ASC) AS RowNumber  
	   CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId   
	  ,CD.ReadCodeID as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON CD.ApprovalStatusId = APS.ApprovalStatusId  
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 ORDER BY CD.CreatedDate desc  
END	
GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=15)
	BEGIN
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END
-------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT	  
			,@PresentRoleId INT = NULL
			,@NextRoleId INT = NULL
			,@CurrentLevel INT		
			,@IsFinalApproval BIT = 0
			,@AdjustmentLogId INT
			,@ApprovalStatusId INT
			,@FunctionId INT
			,@ModifiedBy VARCHAR(50)
			,@Details VARCHAR(200)
			,@BU_ID VARCHAR(50)
	SELECT   
     @ARID=C.value('(ARID)[1]','INT') 
     DECLARE @Xml XML 
     IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
											WHERE ARID = @ARID)
					 --(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
						--			RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerNameChangeLogs 
						--					WHERE NameChangeLogId = @NameChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
						IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE ARID = @ARID  
							SET @Xml=(SELECT 
										Title_Tenant AS TitleTanent
										,FirstName_Tenant AS FirstNameTanent
										,MiddleName_Tenant AS MiddleNameTanent
										,LastName_Tenant AS LastNameTanent
										,PhoneNumber AS PhoneNumberTanent
										,AlternatePhoneNumber AS AlternatePhoneNumberTanent
										,EmailID_Tenant AS EmailIdTanent
										,Title AS TitleLandlord
										,FirstName AS FirstNameLandlord
										,MiddleName AS MiddleNameLandlord
										,LastName AS LastNameLandlord
										,KnownAs AS KnownAs
										,HomeContactNumber AS HomeContactNumberLandlord
										,BusinessContactNumber AS BusinessPhoneNumberLandlord
										,OtherContactNumber AS OtherPhoneNumberLandlord
										,EmailId AS EmailIdLandlord
										,IsSameAsService AS IsSameAsService
										,IsSameAsTenent AS IsSameAsTenent
										,DocumentNo AS DocumentNo
										,ApplicationDate AS ApplicationDate
										,ConnectionDate AS ConnectionDate
										,SetupDate AS SetupDate
										,IsBEDCEmployee AS IsBEDCEmployee
										,IsVIPCustomer AS IsVIPCustomer
										,OldAccountNo AS OldAccountNo
										,CustomerTypeId AS CustomerTypeId
										,BookNo AS BookNo
										,PoleID AS PoleID
										,MeterNumber AS MeterNumber
										,TariffClassID AS TariffClassID
										,IsEmbassyCustomer AS IsEmbassyCustomer
										,EmbassyCode AS EmbassyCode
										,PhaseId AS PhaseId
										,ReadCodeID AS ReadCodeID
										,IdentityTypeId AS IdentityTypeId
										,IdentityNumber AS IdentityNumber
										,CertifiedBy AS CertifiedBy
										,Seal1 AS Seal1
										,Seal2 AS Seal2
										,ApplicationProcessedBy AS ApplicationProcessedBy
										,InstalledBy AS InstalledBy
										,AgencyId AS AgencyId
										,IsCAPMI AS IsCAPMI
										,MeterAmount AS MeterAmount
										,InitialBillingKWh AS InitialBillingKWh
										,InitialReading AS InitialReading
										,PresentReading AS PresentReading
										,HouseNo_Postal AS HouseNoPostal
										,StreetName_Postal AS StreetPostal
										,City_Postal AS CityPostaL
										,AreaCode_Postal AS AreaPostal
										,HouseNo_Service AS HouseNoService
										,StreetName_Service AS StreetService
										,City_Service AS CityService
										,AreaCode_Service AS AreaService
										,DocumentName AS DocumentName
										,[Path] AS [Path]
										,EmployeeCode AS EmployeeCode
										,ZipCode_Postal AS PZipCode
										,ZipCode_Service AS SZipCode
										,AccountTypeId AS AccountTypeId
										,ClusterCategoryId AS ClusterCategoryId
										,RouteSequenceNumber AS RouteSequenceNumber
										,MGActTypeID AS MGActTypeID
										,IsCommunication_Postal AS IsCommunicationPostal
										,IsCommunication_Service AS IsCommunicationService
										,IdentityTypeIdList AS IdentityTypeIdList
										,IdentityNumberList AS IdentityNumberList
										,UDFTypeIdList AS UDFTypeIdList
										,UDFValueList AS UDFValueList
										,CreatedBy AS CreatedBy
										,CreatedDate AS CreatedDate
										 FROM CUSTOMERS.Tbl_ApprovalRegistration
										 WHERE ARID = @ARID
										FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
					EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
						--Audit table
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE  ARID = @ARID
							
						END
				END
		ELSE
			BEGIN -- No Approval Levels are there
					UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
					WHERE  ARID = @ARID
						
					SET @Xml=(SELECT 
									Title_Tenant AS TitleTanent
									,FirstName_Tenant AS FirstNameTanent
									,MiddleName_Tenant AS MiddleNameTanent
									,LastName_Tenant AS LastNameTanent
									,PhoneNumber AS PhoneNumberTanent
									,AlternatePhoneNumber AS AlternatePhoneNumberTanent
									,EmailID_Tenant AS EmailIdTanent
									,Title AS TitleLandlord
									,FirstName AS FirstNameLandlord
									,MiddleName AS MiddleNameLandlord
									,LastName AS LastNameLandlord
									,KnownAs AS KnownAs
									,HomeContactNumber AS HomeContactNumberLandlord
									,BusinessContactNumber AS BusinessPhoneNumberLandlord
									,OtherContactNumber AS OtherPhoneNumberLandlord
									,EmailId AS EmailIdLandlord
									,IsSameAsService AS IsSameAsService
									,IsSameAsTenent AS IsSameAsTenent
									,DocumentNo AS DocumentNo
									,ApplicationDate AS ApplicationDate
									,ConnectionDate AS ConnectionDate
									,SetupDate AS SetupDate
									,IsBEDCEmployee AS IsBEDCEmployee
									,IsVIPCustomer AS IsVIPCustomer
									,OldAccountNo AS OldAccountNo
									,CustomerTypeId AS CustomerTypeId
									,BookNo AS BookNo
									,PoleID AS PoleID
									,MeterNumber AS MeterNumber
									,TariffClassID AS TariffClassID
									,IsEmbassyCustomer AS IsEmbassyCustomer
									,EmbassyCode AS EmbassyCode
									,PhaseId AS PhaseId
									,ReadCodeID AS ReadCodeID
									,IdentityTypeId AS IdentityTypeId
									,IdentityNumber AS IdentityNumber
									,CertifiedBy AS CertifiedBy
									,Seal1 AS Seal1
									,Seal2 AS Seal2
									,ApplicationProcessedBy AS ApplicationProcessedBy
									,InstalledBy AS InstalledBy
									,AgencyId AS AgencyId
									,IsCAPMI AS IsCAPMI
									,MeterAmount AS MeterAmount
									,InitialBillingKWh AS InitialBillingKWh
									,InitialReading AS InitialReading
									,PresentReading AS PresentReading
									,HouseNo_Postal AS HouseNoPostal
									,StreetName_Postal AS StreetPostal
									,City_Postal AS CityPostaL
									,AreaCode_Postal AS AreaPostal
									,HouseNo_Service AS HouseNoService
									,StreetName_Service AS StreetService
									,City_Service AS CityService
									,AreaCode_Service AS AreaService
									,DocumentName AS DocumentName
									,[Path] AS [Path]
									,EmployeeCode AS EmployeeCode
									,ZipCode_Postal AS PZipCode
									,ZipCode_Service AS SZipCode
									,AccountTypeId AS AccountTypeId
									,ClusterCategoryId AS ClusterCategoryId
									,RouteSequenceNumber AS RouteSequenceNumber
									,MGActTypeID AS MGActTypeID
									,IsCommunication_Postal AS IsCommunicationPostal
									,IsCommunication_Service AS IsCommunicationService
									,IdentityTypeIdList AS IdentityTypeIdList
									,IdentityNumberList AS IdentityNumberList
									,UDFTypeIdList AS UDFTypeIdList
									,UDFValueList AS UDFValueList
									,CreatedBy AS CreatedBy
									,CreatedDate AS CreatedDate
									 FROM CUSTOMERS.Tbl_ApprovalRegistration
									 WHERE ARID = @ARID
									FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
						EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
					--Audit Ray
				END
					SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
																WHERE ARID = @ARID)
							
							
							--(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
							--									WHERE NameChangeLogId = @NameChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM CUSTOMERS.Tbl_ApprovalRegistration 
						WHERE ARID = @ARID
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE  ARID = @ARID

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  	
END
--------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN   
BEGIN TRY  
 BEGIN TRAN   
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	,@IsFinalApproval BIT = 0
	,@FunctionId INT
	,@ApprovalStatusId INT 
	,@BU_ID VARCHAR(50) 
	,@CurrentApprovalLevel INT
	,@RoleId INT
	,@CurrentLevel INT
	,@PresentRoleId INT
	,@NextRoleId INT
	,@NameChangeRequestId INT
	,@ModifiedBy varchar(50)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
      
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	
	SET @StatusText='Approval Registration Starts'
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,PresentApprovalRole
		,NextApprovalRole
		,ApproveStatusId
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
		)
			
			IF(@IsFinalApproval = 1)
				BEGIN
					EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @XmlDoc					 
				END
	
	SET @StatusText='Approval Registration Ends'	
	 COMMIT TRAN   
END TRY   
BEGIN CATCH  
 ROLLBACK TRAN  
 SET @IsSuccessful =0  
END CATCH  

--SELECT   
-- @IsSuccessful AS IsSuccessful  
-- ,@StatusText AS StatusText   
-- FOR XML PATH('CustomerRegistrationBE'),TYPE  	
END
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_UpdateCommunicationFeatures]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To Update the email & sms features details
-- =============================================
ALTER PROCEDURE [dbo].[USP_UpdateCommunicationFeatures]
	@XmlDoc XML
AS
BEGIN

	DECLARE		@IsEmailEnabledBillGeneration BIT, 
				@IsSMSEnabledBillGeneration BIT, 
				@IsEmailEnabledPayments BIT, 
				@IsSMSEnabledPayments BIT, 
				@IsEmailEnabledAdjustments BIT, 
				@IsSMSEnabledAdjustments BIT
	
	SELECT         
		@IsEmailEnabledBillGeneration = C.value('(IsEmailEnabledBillGeneration)[1]','BIT'),
		@IsSMSEnabledBillGeneration = C.value('(IsSMSEnabledBillGeneration)[1]','BIT'),
		@IsEmailEnabledPayments = C.value('(IsEmailEnabledPayments)[1]','BIT'),
		@IsSMSEnabledPayments = C.value('(IsSMSEnabledPayments)[1]','BIT'),
		@IsEmailEnabledAdjustments = C.value('(IsEmailEnabledAdjustments)[1]','BIT'),
		@IsSMSEnabledAdjustments = C.value('(IsSMSEnabledAdjustments)[1]','BIT')
	FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)       
	
	UPDATE Tbl_MCommunicationFeatures 
	SET IsEmailFeature=@IsEmailEnabledBillGeneration,IsSMSFeature=@IsSMSEnabledBillGeneration
	WHERE CommunicationFeaturesID=1--For Bill Generation
	
	UPDATE Tbl_MCommunicationFeatures 
	SET IsEmailFeature=@IsEmailEnabledPayments,IsSMSFeature=@IsSMSEnabledPayments
	WHERE CommunicationFeaturesID=2--For Payments
	
	UPDATE Tbl_MCommunicationFeatures 
	SET IsEmailFeature=@IsEmailEnabledAdjustments,IsSMSFeature=@IsSMSEnabledAdjustments
	WHERE CommunicationFeaturesID=3--For Adjustments
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('CommunicationFeaturesBE'),TYPE     
      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCommunicationFeatures]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To get the email & sms features details
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCommunicationFeatures]
	
AS
BEGIN
	SELECT CommunicationFeaturesID,FeatureName,IsEmailFeature,IsSMSFeature,ActiveStatusId 
	From Tbl_MCommunicationFeatures
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsSMSFeatureEnabled]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To check for given Communication Feature for IsSMSEnabled
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsSMSFeatureEnabled]
	@XmlDoc XML
AS
BEGIN
DECLARE		@CommunicationFeaturesID INT 
          
  SELECT         
		@CommunicationFeaturesID = C.value('(CommunicationFeaturesID)[1]','INT')  
  FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)       
  
  SELECT IsSMSFeature FROM Tbl_MCommunicationFeatures WHERE CommunicationFeaturesID=@CommunicationFeaturesID
  FOR XML PATH('CommunicationFeaturesBE'),TYPE     
  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsEmailFeatureEnabled]    Script Date: 07/27/2015 13:24:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 24-Jul-2015
-- Description:	To check for given Communication Feature for IsEmailEnabled
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsEmailFeatureEnabled]
	@XmlDoc XML
AS
BEGIN
DECLARE		@CommunicationFeaturesID INT 
          
  SELECT         
		@CommunicationFeaturesID = C.value('(CommunicationFeaturesID)[1]','INT')  
  FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)       
  
  SELECT IsEmailFeature FROM Tbl_MCommunicationFeatures WHERE CommunicationFeaturesID=@CommunicationFeaturesID
  FOR XML PATH('CommunicationFeaturesBE'),TYPE     
  
END

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType_New]    Script Date: 07/27/2015 17:20:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType_New] 
(      
@XmlDoc Xml=null      
)  
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	
	SELECT @Month = C.value('(Month)[1]','INT')      
	   ,@Year = C.value('(Year)[1]','INT')      
	   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
 
	SELECT   CustomerType
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ActiveCustomersCount,0)) AS MONEY),-1),'.00','') AS Active
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(InActiveCustomersCount,0)) AS MONEY),-1),'.00','')AS InActive
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation				
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1),'.00','') AS EnergyDelivered
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1)  AS EnergyBilled
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(RevenueBilled,0)) AS MONEY),-1)  AS RevenueBilled
			,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountBilled) AS MONEY),-1)AS AmountBilled
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS WAT
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS PIM
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(MINFCCustomersCount,0)) AS MONEY),-1),'.00','') AS [MINFC]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ReadCustomersCount,0)) AS MONEY),-1),'.00','') AS [Read]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(DirectCustomersCount,0)) AS MONEY),-1),'.00','') AS Direct
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ESTCustomersCount,0)) AS MONEY),-1),'.00','') AS EST
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfBilledCustomers,0)) AS MONEY),-1),'.00','') AS NoOfBilled
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS Response
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalCollection
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS TotalAdjustment
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(OpeningBalance,0)) AS MONEY),-1) AS OpeningBalance
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ClosingBalance,0)) AS MONEY),-1) AS ClossingBalance
	FROM Tbl_ReportBillingStatisticsBySCId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY CustomerType	
END


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]    Script Date: 07/27/2015 17:20:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Get customer statistics and tariff Info
-- =============================================   
ALTER PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN    
 SET NOCOUNT ON;  
 DECLARE @Month INT,    
   @Year INT,    
   @BU_ID VARCHAR(100)      
 
SELECT   @Month = C.value('(Month)[1]','INT')      
  ,@Year = C.value('(Year)[1]','INT')      
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)      

   SELECT TariffName AS Tariff		 
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(EnergyDelivered) AS MONEY),-1),'.00','') AS KWHSold
		 ,CONVERT(VARCHAR(20),CAST(SUM(FixedCharges) AS MONEY),-1) AS FixedCharges
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfBilledCustomers) AS MONEY),-1),'.00','') AS NoBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1) AS TotalAmountBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountCollected) AS MONEY),-1) AS TotalAmountCollected
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfStubs) AS MONEY),-1),'.00','') AS NoOfStubs
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(WeightedAvg) AS MONEY),-1),'.00','') AS WeightedAverage
   FROM Tbl_ReportBillingStatisticsByTariffId
   WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
   GROUP BY TariffName
   
END  



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo_New]    Script Date: 07/27/2015 17:20:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo_New]
(
@XmlDoc Xml=null
) 
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(MAX)
	
	SELECT   @Month = C.value('(Month)[1]','INT')
			,@Year = C.value('(Year)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)
	
	SELECT TariffName AS Tariff
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1) AS KWHSold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(KVASold,0)) AS MONEY),-1) AS KVASold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(FixedCharges,0)) AS MONEY),-1) AS FixedCharges
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfBilledCustomers) AS MONEY),-1),'.00','') AS NoBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalAmountCollected
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfStubs,0)) AS MONEY),-1),'.00','') AS NoOfStubs
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountBilled,0)) AS MONEY),-1) AS TotalAmountBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(WeightedAvg,0)) AS MONEY),-1) AS WeightedAverage
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1), '.00', '') AS TotalUsage
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1) AS TotalNetEnergyCharges
	FROM Tbl_ReportBillingStatisticsByTariffId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY TariffName
END


GO


