
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerEmailId]    Script Date: 07/27/2015 19:32:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Email Id of a customer
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerEmailId]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	DECLARE  @LandLordEmailId VARCHAR(MAX)
			,@TenentEmailId VARCHAR(MAX)
			,@TenentId INT=0


	SELECT @LandLordEmailId=EmailId,@TenentId=ISNULL(TenentId,0)
	FROM CUSTOMERS.Tbl_CustomersDetail
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	IF(@TenentId != 0)
		BEGIN
			SELECT @TenentEmailId=EmailId
			FROM CUSTOMERS.Tbl_CustomerTenentDetails
			WHERE TenentId=@TenentId	
		END
	
	
	SELECT (CASE WHEN @LandLordEmailId IS NOT NULL  aND @TenentEmailId IS NOT NULL
				THEN @LandLordEmailId +'; '+@TenentEmailId
				WHEN @LandLordEmailId IS NOT NULL
				THEN @LandLordEmailId
				WHEN @TenentEmailId IS NOT NULL
				THEN @TenentEmailId
				ELSE '0' END ) AS EmailId
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMobileNo]    Script Date: 07/27/2015 19:32:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Mobile no of a customer
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetCustomerMobileNo]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','INT')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT   REPLACE(CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0)))))),'-','') AS MobileNo		 
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
	WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter_New]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Billing status for customer service center  
-- =============================================      
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerServiceCenter_New]   
 (  
 @XmlDoc XML=null    
 )   
AS    
BEGIN    
 
 SET NOCOUNT ON;    
 DECLARE @Month INT,  
   @Year INT,  
   @BU_ID VARCHAR(100)    
  
SELECT   @Month = C.value('(Month)[1]','INT')    
  ,@Year = C.value('(Year)[1]','INT')    
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')       
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)    

		SELECT CustomerType
			  ,ServiceCenterName AS ServiceCenter
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ActiveCustomersCount) AS MONEY),-1),'.00','') AS Active
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(InActiveCustomersCount) AS MONEY),-1),'.00','')AS InActive
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation				
			  ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1)  AS EnergyBilled
			  ,CONVERT(VARCHAR(20),CAST(SUM(RevenueBilled) AS MONEY),-1)  AS RevenueBilled
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(MINFCCustomersCount) AS MONEY),-1),'.00','') AS [MINFC]
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ReadCustomersCount) AS MONEY),-1),'.00','') AS [Read]
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(DirectCustomersCount) AS MONEY),-1),'.00','') AS Direct
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ESTCustomersCount) AS MONEY),-1),'.00','') AS EST
			  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfStubs) AS MONEY),-1),'.00','') AS NoOfStubs
			  ,CONVERT(VARCHAR(20),CAST(SUM(Payments) AS MONEY),-1) AS Payment
			  ,CONVERT(VARCHAR(20),CAST(SUM(OpeningBalance) AS MONEY),-1) AS OpeningBalance
			  ,CONVERT(VARCHAR(20),CAST(SUM(ClosingBalance) AS MONEY),-1) AS ClossingBalance
		FROM Tbl_ReportBillingStatisticsBySCId
		WHERE (BU_ID=@BU_ID OR @BU_ID='')
		AND (YearId=@Year OR @Year=0)
		AND (MonthId=@Month OR @Month=0)
		GROUP BY CustomerType,ServiceCenterName
END 

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerEmailId]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Email Id of a customer
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerEmailId]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	DECLARE  @LandLordEmailId VARCHAR(MAX)
			,@TenentEmailId VARCHAR(MAX)
			,@TenentId INT=0


	SELECT @LandLordEmailId=EmailId,@TenentId=ISNULL(TenentId,0)
	FROM CUSTOMERS.Tbl_CustomersDetail
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	IF(@TenentId != 0)
		BEGIN
			SELECT @TenentEmailId=EmailId
			FROM CUSTOMERS.Tbl_CustomerTenentDetails
			WHERE TenentId=@TenentId	
		END
	
	
	SELECT (CASE WHEN @LandLordEmailId IS NOT NULL  aND @TenentEmailId IS NOT NULL
				THEN @LandLordEmailId +'; '+@TenentEmailId
				WHEN @LandLordEmailId IS NOT NULL
				THEN @LandLordEmailId
				WHEN @TenentEmailId IS NOT NULL
				THEN @TenentEmailId
				ELSE '0' END ) AS EmailId
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetPoleMasters]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Faiz-ID103>  
-- Create date: <03-Feb-2015>  
-- Description: <Retriving Pole Master records form Tbl_MPoleMasterDetails>  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetPoleMasters]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE  @PageNo INT  
    ,@PageSize INT  
  SELECT     
   @PageNo = C.value('(PageNo)[1]','INT')  
   ,@PageSize = C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('PoleBE') AS T(C)  

DECLARE @PoleOrderId INT
 	
	SELECT @PoleOrderId = MIN(PoleMasterOrderID)
		 FROM Tbl_MPoleMasterDetails   
		  WHERE ActiveStatusId = 2
		  
  ;WITH PagedResults AS  
  (  
	SELECT 
			--ROW_NUMBER() OVER(ORDER BY ActiveStatusId ASC , CreatedDate DESC ) AS RowNumber
			ROW_NUMBER() OVER(ORDER BY PoleMasterOrderID DESC ) AS RowNumber
			,PoleMasterId
			,Name
			,PoleMasterOrderID
			,PoleMasterCodeLength
			,[Description]
			,ActiveStatusId
			,(CASE WHEN PM.PoleMasterOrderID > @PoleOrderId THEN 2 ELSE 1 END) AS IsParentActive
	FROM Tbl_MPoleMasterDetails PM

  )  
    
  SELECT   
    (  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   FOR XML PATH('PoleBE'),TYPE  
  )  
  FOR XML PATH(''),ROOT('PoleBEInfoByXml')   
END  
  
------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMobileNo]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Mobile no of a customer
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerMobileNo]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT   REPLACE(CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0)))))),'-','') AS MobileNo		 
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
	WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN     
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	,@IsFinalApproval BIT = 0
	,@FunctionId INT
	,@ApprovalStatusId INT 
	,@BU_ID VARCHAR(50) 
	,@CurrentApprovalLevel INT
	,@RoleId INT
	,@CurrentLevel INT
	,@PresentRoleId INT
	,@NextRoleId INT
	,@NameChangeRequestId INT
	,@ModifiedBy varchar(50)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT')  
      
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	
	SET @StatusText='Approval Registration Starts'
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,PresentApprovalRole
		,NextApprovalRole
		,ApproveStatusId
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
		)
			
			IF(@IsFinalApproval = 1)
				BEGIN
					EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @XmlDoc					 
				END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetBillingStatusForCustomerType_New]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description:	<Description,,Billing status for customer service center>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetBillingStatusForCustomerType_New] 
(      
@XmlDoc Xml=null      
)  
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(100)
	
	SELECT @Month = C.value('(Month)[1]','INT')      
	   ,@Year = C.value('(Year)[1]','INT')      
	   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)  
 
	SELECT   CustomerType
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ActiveCustomersCount,0)) AS MONEY),-1),'.00','') AS Active
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(InActiveCustomersCount,0)) AS MONEY),-1),'.00','')AS InActive
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation				
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1),'.00','') AS EnergyDelivered
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1)  AS EnergyBilled
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(RevenueBilled,0)) AS MONEY),-1)  AS RevenueBilled
			,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountBilled) AS MONEY),-1)AS AmountBilled
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS WAT
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS PIM
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(MINFCCustomersCount,0)) AS MONEY),-1),'.00','') AS [MINFC]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ReadCustomersCount,0)) AS MONEY),-1),'.00','') AS [Read]
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(DirectCustomersCount,0)) AS MONEY),-1),'.00','') AS Direct
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ESTCustomersCount,0)) AS MONEY),-1),'.00','') AS EST
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfBilledCustomers,0)) AS MONEY),-1),'.00','') AS NoOfBilled
			,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1),'.00','') AS Response
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalCollection
			,CONVERT(VARCHAR(20),CAST(SUM(0) AS MONEY),-1) AS TotalAdjustment
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(OpeningBalance,0)) AS MONEY),-1) AS OpeningBalance
			,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(ClosingBalance,0)) AS MONEY),-1) AS ClossingBalance
	FROM Tbl_ReportBillingStatisticsBySCId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY CustomerType	
END


GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Get customer statistics and tariff Info
-- =============================================   
ALTER PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN    
 SET NOCOUNT ON;  
 DECLARE @Month INT,    
   @Year INT,    
   @BU_ID VARCHAR(100)      
 
SELECT   @Month = C.value('(Month)[1]','INT')      
  ,@Year = C.value('(Year)[1]','INT')      
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)      

   SELECT TariffName AS Tariff		 
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(EnergyDelivered) AS MONEY),-1),'.00','') AS KWHSold
		 ,CONVERT(VARCHAR(20),CAST(SUM(FixedCharges) AS MONEY),-1) AS FixedCharges
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfBilledCustomers) AS MONEY),-1),'.00','') AS NoBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(EnergyBilled) AS MONEY),-1) AS TotalAmountBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountCollected) AS MONEY),-1) AS TotalAmountCollected
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfStubs) AS MONEY),-1),'.00','') AS NoOfStubs
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(WeightedAvg) AS MONEY),-1),'.00','') AS WeightedAverage
   FROM Tbl_ReportBillingStatisticsByTariffId
   WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
   GROUP BY TariffName
   
END  



GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersStatisticsInfo_New]    Script Date: 07/27/2015 19:33:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,03/14/2015>
-- Description:	<Description,,Get customer statistics and tariff Info>
-- =============================================
ALTER PROCEDURE [dbo].[USP_RptGetCustomersStatisticsInfo_New]
(
@XmlDoc Xml=null
) 
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @Month INT,@Year INT,@BU_ID VARCHAR(MAX)
	
	SELECT   @Month = C.value('(Month)[1]','INT')
			,@Year = C.value('(Year)[1]','INT')
			,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')			
	FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)
	
	SELECT TariffName AS Tariff
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1) AS KWHSold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(KVASold,0)) AS MONEY),-1) AS KVASold
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(FixedCharges,0)) AS MONEY),-1) AS FixedCharges
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfBilledCustomers) AS MONEY),-1),'.00','') AS NoBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountCollected,0)) AS MONEY),-1) AS TotalAmountCollected
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalPopulationCount,0)) AS MONEY),-1),'.00','') AS TotalPopulation
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(NoOfStubs,0)) AS MONEY),-1),'.00','') AS NoOfStubs
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(TotalAmountBilled,0)) AS MONEY),-1) AS TotalAmountBilled
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(WeightedAvg,0)) AS MONEY),-1) AS WeightedAverage
		  ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyDelivered,0)) AS MONEY),-1), '.00', '') AS TotalUsage
		  ,CONVERT(VARCHAR(20),CAST(SUM(ISNULL(EnergyBilled,0)) AS MONEY),-1) AS TotalNetEnergyCharges
	FROM Tbl_ReportBillingStatisticsByTariffId
	WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
	GROUP BY TariffName
END


GO



GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]    Script Date: 07/28/2015 11:08:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  13/JULY/2015        
-- Description	:  CHECKED CUSTOMER EXISTENCE AND GET CUSTOMER ADDRESS DETAILS 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetCustomerForCustTypeChange_ByCheck]        
(        
 @XmlDoc xml          
)        
AS        
BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @XmlOutput XML	
		,@IsSuccess BIT	
		,@GlobalAccountNumber VARCHAR(50)	
		,@IsCustExistsInOtherBU BIT
		,@BU_ID VARCHAR(50)
		,@Flag INT
		,@FlagDetails INT
		,@ActiveStatusId INT
	SELECT  
		  @GlobalAccountNumber=C.value('(AccountNo)[1]','VARCHAR(50)')  
		  ,@BU_ID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)  
		
	SELECT  @IsSuccess=IsSuccess
			,@GlobalAccountNumber=GlobalAccountNumber 
			,@IsCustExistsInOtherBU=IsCustExistsInOtherBU
			,@ActiveStatusId = ActiveStatusId
	from dbo.fn_CustomerExistenceCheck(@GlobalAccountNumber,@BU_ID)
	IF(@IsSuccess=1 AND @IsCustExistsInOtherBU =0 AND @ActiveStatusId IN(1,2)) --//If customer existence check is successfull then get data
	 BEGIN
		SELECT    
       CD.GlobalAccountNumber AS GlobalAccountNumber    
      ,CD.AccountNo As AccountNo    
      ,CD.FirstName    
      ,CD.MiddleName    
      ,CD.LastName    
      ,CD.Title          
      ,CD.KnownAs    
      ,CD.CustomerTypeId  
      ,CT.CustomerType  
      ,ISNULL(MeterNumber,'--') AS MeterNo    
      ,CD.ClassName AS Tariff    
      ,ISNULL(CD.OldAccountNo,'--') AS OldAccountNo    
      FROM UDV_CustomerDescription  CD    
      join Tbl_MCustomerTypes CT  
      on CD.CustomerTypeId=CT.CustomerTypeId  
      WHERE CD.GlobalAccountNumber=@GlobalAccountNumber      
      FOR XML PATH('ChangeCustomerTypeBE')   
	 END
	ELSE	--//If customer existence check is successfull then get data
	BEGIN
		SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
	END
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess
			,@GlobalAccountNumber AS GlobalAccountNumber 
			,@IsCustExistsInOtherBU AS IsCustExistsInOtherBU
			,@ActiveStatusId AS ActiveStatusId
		FOR XML PATH('RptCustomerLedgerBe')
END CATCH  
END 
