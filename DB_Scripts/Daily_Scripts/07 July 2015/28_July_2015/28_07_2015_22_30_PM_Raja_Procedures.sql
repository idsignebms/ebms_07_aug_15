GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Payment Details of a customer
-- =============================================  
CREATE PROCEDURE [USP_GetCustomerPaymnetDetForMail]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT   dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,NULL,CD.LastName) AS Name
		,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS PaidAmount
		,dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber) AS PaidDate
		,dbo.fn_GetCustomerLastPaymentTxnId(CD.GlobalAccountNumber) AS TransactionId
		,1 AS IsSuccess
		,dbo.fn_GetCustomerMobileNo(CD.GlobalAccountNumber) AS MobileNo
		,dbo.fn_GetCustomerEmailId(CD.GlobalAccountNumber) AS EmailId
		,ISNULL(CD.MeterNumber,'--') AS MeterNumber
		,CD.OutStandingAmount AS OutStandingAmount
	FROM UDV_CustomerDescription CD
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Adjustment Details of a customer
-- =============================================  
CREATE PROCEDURE [USP_GetCustomerAdjustmentDetForMail]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT TOP 1 B.AccountNo AS GlobalAccountNumber
			,B.TotalAmountEffected AS AdjustedAmount
			,CD.OutStandingAmount AS OutStandingAmount
			,BillAdjustmentId AS TransactionId
			,ISNULL(B.BillAdjustmentType,0) AS BillAdjustmentTypeId
			,1 AS IsSuccess
			,dbo.fn_GetCustomerMobileNo(CD.GlobalAccountNumber) AS MobileNo
			,dbo.fn_GetCustomerEmailId(CD.GlobalAccountNumber) AS EmailId
			,ISNULL(CD.MeterNumber,'--') AS MeterNumber
	FROM Tbl_BillAdjustments B
	JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber=B.AccountNo
	LEFT JOIN Tbl_BillAdjustmentType BT ON B.BillAdjustmentType=BT.BATID
	WHERE B.AccountNo=@GlobalAccountNumber
	AND B.BillAdjustmentType IN (2,4)
	ORDER BY BillAdjustmentId DESC
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
CREATE FUNCTION [fn_GetCustomerMobileNo]  
(  
@GlobalAccountNumber VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @MobileNo VARCHAR(MAX)  
  
SELECT  @MobileNo= REPLACE(CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0)))))),'-','')	 
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
	WHERE CD.GlobalAccountNumber=@GlobalAccountNumber 
    
 RETURN @MobileNo   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
CREATE FUNCTION [fn_GetCustomerLastPaymentTxnId]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @TxnId VARCHAR(MAX)  
  
 -- SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,103) FROM Tbl_CustomerPayments -- comented by Faiz-ID103
 SELECT TOP(1) 
 @TxnId=ISNULL(ReceiptNo,CustomerPaymentID) 
 FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY RecievedDate DESC  , CustomerPaymentID DESC
 --ORDER BY CreatedDate DESC-- Raja -ID065  
    
 RETURN @TxnId   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
CREATE FUNCTION [fn_GetCustomerEmailId]  
(  
@GlobalAccountNumber VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @EmailId VARCHAR(MAX)  
  
DECLARE  @LandLordEmailId VARCHAR(MAX)
			,@TenentEmailId VARCHAR(MAX)
			,@TenentId INT=0


	SELECT @LandLordEmailId=EmailId,@TenentId=ISNULL(TenentId,0)
	FROM CUSTOMERS.Tbl_CustomersDetail
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	IF(@TenentId != 0)
		BEGIN
			SELECT @TenentEmailId=EmailId
			FROM CUSTOMERS.Tbl_CustomerTenentDetails
			WHERE TenentId=@TenentId	
		END
	
	
	SELECT @EmailId=(CASE WHEN @LandLordEmailId IS NOT NULL  aND @TenentEmailId IS NOT NULL
				THEN @LandLordEmailId +'; '+@TenentEmailId
				WHEN @LandLordEmailId IS NOT NULL
				THEN @LandLordEmailId
				WHEN @TenentEmailId IS NOT NULL
				THEN @TenentEmailId
				ELSE '0' END )
    
 RETURN @EmailId   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------
GO