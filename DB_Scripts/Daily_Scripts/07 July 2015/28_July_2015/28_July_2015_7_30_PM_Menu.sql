--SELECT * FROM Tbl_Menus WHERE ReferenceMenuId=122
GO
INSERT INTO Tbl_Menus(Name,[Path],ReferenceMenuId,Page_Order,IsActive,ActiveHeader,MenuId)
VALUES('Communication Feature','../Admin/CommunicationFeature.aspx',122,64,1,1,210)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(1,210,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO
INSERT INTO Tbl_NewPagePermissions(Role_Id,MenuId,[View],[Create],Created_By,Created_Date,IsActive)
VALUES(12,210,1,1,'Admin',dbo.fn_GetCurrentDateTime(),1)
GO