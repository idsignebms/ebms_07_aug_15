
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerEmailId]    Script Date: 07/29/2015 11:04:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerEmailId]  
(  
@GlobalAccountNumber VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @EmailId VARCHAR(MAX)  
  
DECLARE  @LandLordEmailId VARCHAR(MAX)
			,@TenentEmailId VARCHAR(MAX)
			,@TenentId INT=0


	SELECT @LandLordEmailId=EmailId,@TenentId=ISNULL(TenentId,0)
	FROM CUSTOMERS.Tbl_CustomersDetail
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	IF(@TenentId != 0)
		BEGIN
			SELECT @TenentEmailId=EmailId
			FROM CUSTOMERS.Tbl_CustomerTenentDetails
			WHERE TenentId=@TenentId	
		END
	
	
	SELECT @EmailId=(CASE WHEN @LandLordEmailId IS NOT NULL  aND @TenentEmailId IS NOT NULL
				THEN @LandLordEmailId +'; '+@TenentEmailId
				WHEN @LandLordEmailId IS NOT NULL
				THEN @LandLordEmailId
				WHEN @TenentEmailId IS NOT NULL
				THEN @TenentEmailId
				ELSE '0' END )
    
 RETURN @EmailId   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerMobileNo]    Script Date: 07/29/2015 11:04:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerMobileNo]  
(  
@GlobalAccountNumber VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @MobileNo VARCHAR(MAX)  
  
SELECT  @MobileNo= REPLACE(CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0)))))),'-','')	 
	FROM CUSTOMERS.Tbl_CustomersDetail CD
	LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
	WHERE CD.GlobalAccountNumber=@GlobalAccountNumber 
    
 RETURN @MobileNo   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------

GO


