
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentDetForMail]    Script Date: 07/29/2015 11:08:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Adjustment Details of a customer
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAdjustmentDetForMail]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT TOP 1 B.AccountNo AS GlobalAccountNumber
			,B.TotalAmountEffected AS AdjustedAmount
			,CD.OutStandingAmount AS OutStandingAmount
			,BillAdjustmentId AS TransactionId
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,NULL,CD.LastName) AS Name
			,ISNULL(B.BillAdjustmentType,0) AS BillAdjustmentTypeId
			,1 AS IsSuccess
			,dbo.fn_GetCustomerMobileNo(CD.GlobalAccountNumber) AS MobileNo
			,dbo.fn_GetCustomerEmailId(CD.GlobalAccountNumber) AS EmailId
			,ISNULL(CD.MeterNumber,'--') AS MeterNumber
	FROM Tbl_BillAdjustments B
	JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber=B.AccountNo
	LEFT JOIN Tbl_BillAdjustmentType BT ON B.BillAdjustmentType=BT.BATID
	WHERE B.AccountNo=@GlobalAccountNumber
	AND B.BillAdjustmentType IN (2,4)
	ORDER BY BillAdjustmentId DESC
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAdjustmentDetForMail]    Script Date: 07/29/2015 11:08:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Adjustment Details of a customer
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAdjustmentDetForMail]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT TOP 1 B.AccountNo AS GlobalAccountNumber
			,B.TotalAmountEffected AS AdjustedAmount
			,CD.OutStandingAmount AS OutStandingAmount
			,BillAdjustmentId AS TransactionId
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,NULL,CD.LastName) AS Name
			,ISNULL(B.BillAdjustmentType,0) AS BillAdjustmentTypeId
			,1 AS IsSuccess
			,dbo.fn_GetCustomerMobileNo(CD.GlobalAccountNumber) AS MobileNo
			,dbo.fn_GetCustomerEmailId(CD.GlobalAccountNumber) AS EmailId
			,ISNULL(CD.MeterNumber,'--') AS MeterNumber
	FROM Tbl_BillAdjustments B
	JOIN UDV_CustomerDescription CD ON CD.GlobalAccountNumber=B.AccountNo
	LEFT JOIN Tbl_BillAdjustmentType BT ON B.BillAdjustmentType=BT.BATID
	WHERE B.AccountNo=@GlobalAccountNumber
	AND B.BillAdjustmentType IN (2,4)
	ORDER BY BillAdjustmentId DESC
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPaymnetDetForMail]    Script Date: 07/29/2015 11:08:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Payment Details of a customer
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerPaymnetDetForMail]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	SELECT   dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,NULL,CD.LastName) AS Name
		,dbo.fn_GetCustomerLastPaidAmount(CD.GlobalAccountNumber) AS PaidAmount
		,dbo.fn_GetCustomerLastPaymentDate(CD.GlobalAccountNumber) AS PaidDate
		,dbo.fn_GetCustomerLastPaymentTxnId(CD.GlobalAccountNumber) AS TransactionId
		,1 AS IsSuccess
		,dbo.fn_GetCustomerMobileNo(CD.GlobalAccountNumber) AS MobileNo
		,dbo.fn_GetCustomerEmailId(CD.GlobalAccountNumber) AS EmailId
		,ISNULL(CD.MeterNumber,'--') AS MeterNumber
		,CD.OutStandingAmount AS OutStandingAmount
	FROM UDV_CustomerDescription CD
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 07/29/2015 11:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT	  
			,@PresentRoleId INT = NULL
			,@NextRoleId INT = NULL
			,@CurrentLevel INT		
			,@IsFinalApproval BIT = 0
			,@AdjustmentLogId INT
			,@ApprovalStatusId INT
			,@FunctionId INT
			,@ModifiedBy VARCHAR(50)
			,@Details VARCHAR(200)
			,@BU_ID VARCHAR(50)
			,@GlobalAccountNumber VARCHAR(50)
	SELECT   
		 @ARID=C.value('(ARID)[1]','INT')
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C) 
     DECLARE @Xml XML 
     IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
											WHERE ARID = @ARID)
					 --(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
						--			RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerNameChangeLogs 
						--					WHERE NameChangeLogId = @NameChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
						IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE ARID = @ARID  
							SET @Xml=(SELECT 
										Title_Tenant AS TitleTanent
										,FirstName_Tenant AS FirstNameTanent
										,MiddleName_Tenant AS MiddleNameTanent
										,LastName_Tenant AS LastNameTanent
										,PhoneNumber AS PhoneNumberTanent
										,AlternatePhoneNumber AS AlternatePhoneNumberTanent
										,EmailID_Tenant AS EmailIdTanent
										,Title AS TitleLandlord
										,FirstName AS FirstNameLandlord
										,MiddleName AS MiddleNameLandlord
										,LastName AS LastNameLandlord
										,KnownAs AS KnownAs
										,HomeContactNumber AS HomeContactNumberLandlord
										,BusinessContactNumber AS BusinessPhoneNumberLandlord
										,OtherContactNumber AS OtherPhoneNumberLandlord
										,EmailId AS EmailIdLandlord
										,IsSameAsService AS IsSameAsService
										,IsSameAsTenent AS IsSameAsTenent
										,DocumentNo AS DocumentNo
										,ApplicationDate AS ApplicationDate
										,ConnectionDate AS ConnectionDate
										,SetupDate AS SetupDate
										,IsBEDCEmployee AS IsBEDCEmployee
										,IsVIPCustomer AS IsVIPCustomer
										,OldAccountNo AS OldAccountNo
										,CustomerTypeId AS CustomerTypeId
										,BookNo AS BookNo
										,PoleID AS PoleID
										,MeterNumber AS MeterNumber
										,TariffClassID AS TariffClassID
										,IsEmbassyCustomer AS IsEmbassyCustomer
										,EmbassyCode AS EmbassyCode
										,PhaseId AS PhaseId
										,ReadCodeID AS ReadCodeID
										,IdentityTypeId AS IdentityTypeId
										,IdentityNumber AS IdentityNumber
										,CertifiedBy AS CertifiedBy
										,Seal1 AS Seal1
										,Seal2 AS Seal2
										,ApplicationProcessedBy AS ApplicationProcessedBy
										,InstalledBy AS InstalledBy
										,AgencyId AS AgencyId
										,IsCAPMI AS IsCAPMI
										,MeterAmount AS MeterAmount
										,InitialBillingKWh AS InitialBillingKWh
										,InitialReading AS InitialReading
										,PresentReading AS PresentReading
										,HouseNo_Postal AS HouseNoPostal
										,StreetName_Postal AS StreetPostal
										,City_Postal AS CityPostaL
										,AreaCode_Postal AS AreaPostal
										,HouseNo_Service AS HouseNoService
										,StreetName_Service AS StreetService
										,City_Service AS CityService
										,AreaCode_Service AS AreaService
										,DocumentName AS DocumentName
										,[Path] AS [Path]
										,EmployeeCode AS EmployeeCode
										,ZipCode_Postal AS PZipCode
										,ZipCode_Service AS SZipCode
										,AccountTypeId AS AccountTypeId
										,ClusterCategoryId AS ClusterCategoryId
										,RouteSequenceNumber AS RouteSequenceNumber
										,MGActTypeID AS MGActTypeID
										,IsCommunication_Postal AS IsCommunicationPostal
										,IsCommunication_Service AS IsCommunicationService
										,IdentityTypeIdList AS IdentityTypeIdList
										,IdentityNumberList AS IdentityNumberList
										,UDFTypeIdList AS UDFTypeIdList
										,UDFValueList AS UDFValueList
										,CreatedBy AS CreatedBy
										,CreatedDate AS CreatedDate
										 FROM CUSTOMERS.Tbl_ApprovalRegistration
										 WHERE ARID = @ARID
										FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
					EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
					SET @GlobalAccountNumber=(SELECT TOP 1 CustomerId FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
						--Audit table
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
								,GlobalAccountNumber=@GlobalAccountNumber
							WHERE  ARID = @ARID
							
						END
				END
		ELSE
			BEGIN -- No Approval Levels are there					
						
					SET @Xml=(SELECT 
									Title_Tenant AS TitleTanent
									,FirstName_Tenant AS FirstNameTanent
									,MiddleName_Tenant AS MiddleNameTanent
									,LastName_Tenant AS LastNameTanent
									,PhoneNumber AS PhoneNumberTanent
									,AlternatePhoneNumber AS AlternatePhoneNumberTanent
									,EmailID_Tenant AS EmailIdTanent
									,Title AS TitleLandlord
									,FirstName AS FirstNameLandlord
									,MiddleName AS MiddleNameLandlord
									,LastName AS LastNameLandlord
									,KnownAs AS KnownAs
									,HomeContactNumber AS HomeContactNumberLandlord
									,BusinessContactNumber AS BusinessPhoneNumberLandlord
									,OtherContactNumber AS OtherPhoneNumberLandlord
									,EmailId AS EmailIdLandlord
									,IsSameAsService AS IsSameAsService
									,IsSameAsTenent AS IsSameAsTenent
									,DocumentNo AS DocumentNo
									,ApplicationDate AS ApplicationDate
									,ConnectionDate AS ConnectionDate
									,SetupDate AS SetupDate
									,IsBEDCEmployee AS IsBEDCEmployee
									,IsVIPCustomer AS IsVIPCustomer
									,OldAccountNo AS OldAccountNo
									,CustomerTypeId AS CustomerTypeId
									,BookNo AS BookNo
									,PoleID AS PoleID
									,MeterNumber AS MeterNumber
									,TariffClassID AS TariffClassID
									,IsEmbassyCustomer AS IsEmbassyCustomer
									,EmbassyCode AS EmbassyCode
									,PhaseId AS PhaseId
									,ReadCodeID AS ReadCodeID
									,IdentityTypeId AS IdentityTypeId
									,IdentityNumber AS IdentityNumber
									,CertifiedBy AS CertifiedBy
									,Seal1 AS Seal1
									,Seal2 AS Seal2
									,ApplicationProcessedBy AS ApplicationProcessedBy
									,InstalledBy AS InstalledBy
									,AgencyId AS AgencyId
									,IsCAPMI AS IsCAPMI
									,MeterAmount AS MeterAmount
									,InitialBillingKWh AS InitialBillingKWh
									,InitialReading AS InitialReading
									,PresentReading AS PresentReading
									,HouseNo_Postal AS HouseNoPostal
									,StreetName_Postal AS StreetPostal
									,City_Postal AS CityPostaL
									,AreaCode_Postal AS AreaPostal
									,HouseNo_Service AS HouseNoService
									,StreetName_Service AS StreetService
									,City_Service AS CityService
									,AreaCode_Service AS AreaService
									,DocumentName AS DocumentName
									,[Path] AS [Path]
									,EmployeeCode AS EmployeeCode
									,ZipCode_Postal AS PZipCode
									,ZipCode_Service AS SZipCode
									,AccountTypeId AS AccountTypeId
									,ClusterCategoryId AS ClusterCategoryId
									,RouteSequenceNumber AS RouteSequenceNumber
									,MGActTypeID AS MGActTypeID
									,IsCommunication_Postal AS IsCommunicationPostal
									,IsCommunication_Service AS IsCommunicationService
									,IdentityTypeIdList AS IdentityTypeIdList
									,IdentityNumberList AS IdentityNumberList
									,UDFTypeIdList AS UDFTypeIdList
									,UDFValueList AS UDFValueList
									,CreatedBy AS CreatedBy
									,CreatedDate AS CreatedDate
									 FROM CUSTOMERS.Tbl_ApprovalRegistration
									 WHERE ARID = @ARID
									FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
						EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml						
					
					SET @GlobalAccountNumber=(SELECT TOP 1 CustomerId FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
					UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
						,GlobalAccountNumber=@GlobalAccountNumber
					WHERE  ARID = @ARID
					--Audit Ray
				END
					SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
		END
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
																WHERE ARID = @ARID)
							
							
							--(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
							--									WHERE NameChangeLogId = @NameChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM CUSTOMERS.Tbl_ApprovalRegistration 
						WHERE ARID = @ARID
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE  ARID = @ARID

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END  	
END
--------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 07/29/2015 11:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN     
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	,@IsFinalApproval BIT = 0
	,@FunctionId INT
	,@ApprovalStatusId INT 
	,@BU_ID VARCHAR(50) 
	,@CurrentApprovalLevel INT
	,@RoleId INT
	,@CurrentLevel INT
	,@PresentRoleId INT
	,@NextRoleId INT
	,@NameChangeRequestId INT
	,@ModifiedBy varchar(50)
	,@RouteSequenceNumber_Value varchar(50)
	,@PhaseId_Value varchar(50)
	,@ReadCodeID_Value varchar(50)
	,@ApplicationProcessedBy_Value VARCHAR(50)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT') 
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
    ,@FunctionId=C.value('(FunctionId)[1]','VARCHAR(50)')  
    ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT')  
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	SET @RouteSequenceNumber_Value=(SELECT RouteName FROM Tbl_MRoutes WHERE RouteId=@RouteSequenceNumber)
	SET @PhaseId_Value=(SELECT Phase FROM Tbl_MPhases WHERE PhaseId=@PhaseId)
	SET @ReadCodeID_Value=(SELECT ReadCode FROM Tbl_MReadCodes WHERE ReadCodeId=@ReadCodeID)
	SET @ApplicationProcessedBy_Value =@ApplicationProcessedBy
	
	
	SET @StatusText='Approval Registration Starts'
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,	PresentApprovalRole
		,	NextApprovalRole
		,	ApproveStatusId
		,	CurrentApprovalLevel
		,   RouteSequenceNumber_value
		,	PhaseId_Value
		,	ReadCodeID_Value
		,	ApplicationProcessedBy_Value
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
			, @RouteSequenceNumber_value
			, @PhaseId_Value
			, @ReadCodeID_Value
			, @ApplicationProcessedBy_Value
		)
			
			IF(@IsFinalApproval = 1)
				BEGIN
				DECLAre @xml xml=null;
				select @xml EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @XmlDoc
				SET @GlobalAccountNumber=(SELECT TOP 1 CustomerId FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
				UPDATE CUSTOMERS.Tbl_ApprovalRegistration SET GlobalAccountNumber=@GlobalAccountNumber 
				WHERE ARID =(SELECT TOP 1 ARID FROM CUSTOMERS.Tbl_ApprovalRegistration ORDER BY ARID DESC )
				END
			ELSE
			BEGIN
				SELECT 1 AS IsSuccessful  
				 ,@StatusText AS StatusText   
				 FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
			END
END
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrationsByARID]    Script Date: 07/29/2015 11:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetApprovalRegistrationsByARID]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
	   ,(CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 AND CD.ARID=@ARID 
	 ORDER BY CD.CreatedDate desc 
	 FOR XML PATH('CustomerRegistrationBE'),TYPE  
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrations]    Script Date: 07/29/2015 11:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrations]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	   ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS PhaseId   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
	   ,(CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 ORDER BY CD.CreatedDate desc  
END
-------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 07/29/2015 11:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
   ,@Month INT          
   ,@Year INT           
   ,@Date DATETIME           
   ,@MonthStartDate DATE       
   ,@PreviousReading VARCHAR(50)          
   ,@CurrentReading VARCHAR(50)          
   ,@Usage DECIMAL(20)          
   ,@RemaningBalanceUsage INT          
   ,@TotalAmount DECIMAL(18,2)= 0          
   ,@TaxValue DECIMAL(18,2)          
   ,@BillingQueuescheduleId INT          
   ,@PresentCharge INT          
   ,@FromUnit INT          
   ,@ToUnit INT          
   ,@Amount DECIMAL(18,2)      
   ,@TaxId INT          
   ,@CustomerBillId INT = 0          
   ,@BillGeneratedBY VARCHAR(50)          
   ,@LastDateOfBill DATETIME          
   ,@IsEstimatedRead BIT = 0          
   ,@ReadCodeId INT          
   ,@BillType INT          
   ,@LastBillGenerated DATETIME        
   ,@FeederId VARCHAR(50)        
   ,@CycleId VARCHAR(MAX)     -- CycleId   
   ,@BillNo VARCHAR(50)      
   ,@PrevCustomerBillId INT      
   ,@AdjustmentAmount DECIMAL(18,2)      
   ,@PreviousDueAmount DECIMAL(18,2)      
   ,@CustomerTariffId VARCHAR(50)      
   ,@BalanceUnits INT      
   ,@RemainingBalanceUnits INT      
   ,@IsHaveLatest BIT = 0      
   ,@ActiveStatusId INT      
   ,@IsDisabled BIT      
   ,@BookDisableType INT      
   ,@IsPartialBill BIT      
   ,@OutStandingAmount DECIMAL(18,2)      
   ,@IsActive BIT      
   ,@NoFixed BIT = 0      
   ,@NoBill BIT = 0       
   ,@ClusterCategoryId INT=NULL    
   ,@StatusText VARCHAR(50)      
   ,@BU_ID VARCHAR(50)    
   ,@BillingQueueScheduleIdList VARCHAR(MAX)    
   ,@RowsEffected INT  
   ,@IsFromReading BIT  
   ,@TariffId INT  
   ,@EnergyCharges DECIMAL(18,2)   
   ,@FixedCharges  DECIMAL(18,2)  
   ,@CustomerTypeID INT  
   ,@ReadType Int  
   ,@TaxPercentage DECIMAL(18,2)=5    
   ,@TotalBillAmountWithTax  DECIMAL(18,2)  
   ,@AverageUsageForNewBill  DECIMAL(18,2)  
   ,@IsEmbassyCustomer INT  
   ,@TotalBillAmountWithArrears  DECIMAL(18,2)   
   ,@CusotmerNewBillID INT  
   ,@InititalkWh INT  
   ,@NetArrears DECIMAL(18,2)  
   ,@BookNo VARCHAR(30)  
   ,@GetPaidMeterBalance DECIMAL(18,2)  
   ,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
   ,@MeterNumber VARCHAR(50)  
   ,@ActualUsage DECIMAL(18,2)  
   ,@RegenCustomerBillId INT  
   ,@PaidAmount  DECIMAL(18,2)  
   ,@PrevBillTotalPaidAmount Decimal(18,2)  
   ,@PreviousBalance Decimal(18,2)  
   ,@OpeningBalance Decimal(18,2)  
   ,@CustomerFullName VARCHAR(MAX)  
   ,@BusinessUnitName VARCHAR(100)  
   ,@TariffName VARCHAR(50)  
   ,@ReadDate DateTime  
   ,@Multiplier INT  
   ,@Service_HouseNo VARCHAR(100)  
   ,@Service_Street VARCHAR(100)  
   ,@Service_City VARCHAR(100)  
   ,@ServiceZipCode VARCHAR(100)  
   ,@Postal_HouseNo VARCHAR(100)  
   ,@Postal_Street VARCHAR(100)  
   ,@Postal_City VARCHAR(100)  
   ,@Postal_ZipCode VARCHAR(100)  
   ,@Postal_LandMark VARCHAR(100)  
   ,@Service_LandMark VARCHAR(100)  
   ,@OldAccountNumber VARCHAR(100)  
   ,@ServiceUnitId VARCHAR(50)  
   ,@PoleId Varchar(50)  
   ,@ServiceCenterId VARCHAR(50)  
   ,@IspartialClose INT  
   ,@TariffCharges varchar(max)  
   ,@CusotmerPreviousBillNo varchar(50)  
   ,@DisableDate DATETIME 
   ,@BillingComments Varchar(max) 
   ,@TotalBillAmount decimal(18,2)
   ,@TotalPaidAmount DECIMAL(18,2)
   ,@PaidMeterDeductedAmount DECIMAL(18,2)
   ,@AdjustmentAmountEffected DECIMAL(18,2)
   ,@IsFirstmonthBill Bit
    ,@SetupDate DATETIME
   ,@ConnectionDate DATETIME
 DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
 DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
 SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
   ,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
   ,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
   FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	 SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) FROM Tbl_CustomerBills CB 
	 JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
	 WHERE BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	 SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	 FROM Tbl_CustomerBills AS CB
	 JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
	 JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
	 WHERE CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
       
 IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
 BEGIN     
      SELECT   
      @ActiveStatusId = ActiveStatusId,  
      @OutStandingAmount = ISNULL(OutStandingAmount,0)  
      ,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,  
      @IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InitialBillingKWh  
      ,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber  
      ,@OpeningBalance=isnull(OpeningBalance,0)  
      ,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
      ,@BusinessUnitName =BusinessUnitName  
      ,@TariffName =ClassName  
      ,@Service_HouseNo =Service_HouseNo  
      ,@Service_Street =Service_StreetName  
      ,@Service_City =Service_City  
      ,@ServiceZipCode  =Service_ZipCode  
      ,@Postal_HouseNo =Postal_HouseNo  
      ,@Postal_Street =Postal_StreetName  
      ,@Postal_City  =Postal_City  
      ,@Postal_ZipCode  =Postal_ZipCode  
      ,@Postal_LandMark =Postal_LandMark  
      ,@Service_LandMark =Service_LandMark  
      ,@OldAccountNumber=OldAccountNo  
      ,@BU_ID=BU_ID  
      ,@ServiceUnitId=SU_ID  
      ,@PoleId=PoleID  
      ,@TariffId=ClassID  
      ,@ServiceCenterId=ServiceCenterId  
      ,@CycleId=CycleId
      ,@SetupDate=SetupDate
      ,@ConnectionDate=ConnectionDate  
      FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   
         
       ----------------------------------------COPY START --------------------------------------------------------   
           --------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
	 
						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings SET IsBilled =0 
							,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							-- Insert Paid Meter Payments
							
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END

							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						    Return;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								  
						    Return;
							

							IF @ReadCodeId=2 -- 
							BEGIN
								SELECT  @PreviousReading=PreviousReading,
										@CurrentReading = PresentReading,@Usage=Usage,
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
										IF @Usage<@BalanceUnits
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
											SET @RemaningBalanceUsage=@BalanceUnits-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
											SET @Usage=@Usage-@BalanceUnits
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
							ELSE  
							BEGIN

								set @IsEstimatedRead =1
								 
								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
								SET @ReadType=1 -- Direct
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 
						 
						DECLARE @tblFixedCharges AS TABLE(ClassID	int,Amount Decimal(18,2))
						------------------------------------------------------------------------------------ 
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN

										IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
										END		
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)	
										 INSERT INTO @tblFixedCharges(ClassID ,Amount)
										 SELECT ClassID,Amount FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
						SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber)
						IF @GetPaidMeterBalance>0
						BEGIN
							
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
								
							END
						END
						------------------------
						-- Caluclate tax here
						
						 SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											*	((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
						  SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
							
							IF	 @PrevCustomerBillId IS NULL 
							BEGIN
							 
							 
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
										WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
								
   ----------------------------------------------------------COPY END-------------------------------------------------------------------  
   --- Need to verify all fields before insert  
   INSERT INTO Tbl_CustomerBills  
   (  
    [AccountNo]   --@GlobalAccountNumber       
    ,[TotalBillAmount] --@TotalBillAmountWithTax        
    ,[ServiceAddress] --@EnergyCharges   
    ,[MeterNo]   -- @MeterNumber      
    ,[Dials]     --     
    ,[NetArrears] --   @NetArrears      
    ,[NetEnergyCharges] --  @EnergyCharges       
    ,[NetFixedCharges]   --@FixedCharges       
    ,[VAT]  --     @TaxValue   
    ,[VATPercentage]  --  @TaxPercentage      
    ,[Messages]  --        
    ,[BU_ID]  --        
    ,[SU_ID]  --      
    ,[ServiceCenterId]    
    ,[PoleId] --         
    ,[BillGeneratedBy] --         
    ,[BillGeneratedDate]          
    ,PaymentLastDate        --  
    ,[TariffId]  -- @TariffId       
    ,[BillYear]    --@Year      
    ,[BillMonth]   --@Month       
    ,[CycleId]   -- @CycleId  
    ,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
    ,[ActiveStatusId]--          
    ,[CreatedDate]--GETDATE()          
    ,[CreatedBy]          
    ,[ModifedBy]          
    ,[ModifiedDate]          
    ,[BillNo]  --        
    ,PaymentStatusID          
    ,[PreviousReading]  --@PreviousReading        
    ,[PresentReading]   --  @CurrentReading     
    ,[Usage]     --@Usage     
    ,[AverageReading] -- @AverageUsageForNewBill        
    ,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
    ,[EstimatedUsage] --@Usage         
    ,[ReadCodeId]  --  @ReadType      
    ,[ReadType]  --  @ReadType  
    ,AdjustmentAmmount -- @AdjustmentAmount    
    ,BalanceUsage    --@RemaningBalanceUsage  
    ,BillingTypeId --   
    ,ActualUsage  
    ,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
    ,PreviousBalance--@PreviousBalance
    ,TariffRates  
   )          
    Values( @GlobalAccountNumber  
    ,@TotalBillAmount     
    ,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
    ,@MeterNumber      
    ,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
    ,@NetArrears         
    ,@EnergyCharges   
    ,@FixedCharges  
    ,@TaxValue   
    ,@TaxPercentage          
    ,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
    ,@BU_ID  
    ,@ServiceUnitId  
    ,@ServiceCenterId  
    ,@PoleId          
    ,@BillGeneratedBY          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
     ORDER BY RecievedDate DESC) --@LastDateOfBill          
    ,@TariffId  
    ,@Year  
    ,@Month  
    ,@CycleId  
    ,@TotalBillAmountWithArrears   
    ,1 --ActiveStatusId          
    ,(SELECT dbo.fn_GetCurrentDateTime())          
    ,@BillGeneratedBY          
    ,NULL --ModifedBy  
    ,NULL --ModifedDate  
    ,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
    ,2 -- PaymentStatusID     
    ,@PreviousReading          
    ,@CurrentReading          
    ,@Usage          
    ,@AverageUsageForNewBill  
    ,@TotalBillAmountWithTax               
    ,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
    ,@ReadType  
    ,@ReadType         
    ,@AdjustmentAmount      
    ,@RemaningBalanceUsage     
    ,2 -- BillingTypeId   
    ,@ActualUsage  
    ,@PrevBillTotalPaidAmount  
    ,@PreviousBalance
    ,@TariffCharges  
            
  )  
   set @CusotmerNewBillID = SCOPE_IDENTITY()   
   ----------------------- Update Customer Outstanding ------------------------------  
  
   -- Insert Paid Meter Payments  
   IF isnull(@PaidMeterDeductedAmount,0) >0
   BEGIN
   
   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
   
   update Tbl_PaidMeterDetails
   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
   where AccountNo=@GlobalAccountNumber
   
   END
         
   update CUSTOMERS.Tbl_CustomerActiveDetails  
   SET OutStandingAmount=@TotalBillAmountWithArrears  
   where GlobalAccountNumber=@GlobalAccountNumber  
   ----------------------------------------------------------------------------------  
   ----------------------Update Readings as is billed =1 ----------------------------  
     
   update Tbl_CustomerReadings set IsBilled =1 
	,BillNo=@CusotmerNewBillID
   where GlobalAccountNumber=@GlobalAccountNumber  
   
   --------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  
     
   insert into TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
   select @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID from  @tblFixedCharges  
     
   delete from @tblFixedCharges  
      
   ------------------------------------------------------------------------------------  
     
   --------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
   --Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
   --WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   
     
   ---------------------------------------------------------------------------------------  
   Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      
     
   --------------Save Bill Deails of customer name,BU-----------------------------------------------------  
   INSERT INTO Tbl_BillDetails  
      (CustomerBillID,  
       BusinessUnitName,  
       CustomerFullName,  
       Multiplier,  
       Postal_City,  
       Postal_HouseNo,  
       Postal_Street,  
       Postal_ZipCode,  
       ReadDate,  
       ServiceZipCode,  
       Service_City,  
       Service_HouseNo,  
       Service_Street,  
       TariffName,  
       Postal_LandMark,  
       Service_Landmark,  
       OldAccountNumber)  
     VALUES  
      (@CusotmerNewBillID,  
      @BusinessUnitName,  
      @CustomerFullName,  
      @Multiplier,  
      @Postal_City,  
      @Postal_HouseNo,  
      @Postal_Street,  
      @Postal_ZipCode,  
      @ReadDate,  
      @ServiceZipCode,  
      @Service_City,  
      @Service_HouseNo,  
      @Service_Street,  
      @TariffName,  
      @Postal_LandMark,  
      @Service_LandMark,  
      @OldAccountNumber)  
     
   ---------------------------------------------------Set Variables to NULL-  
     
   SET @TotalAmount = NULL  
   SET @EnergyCharges = NULL  
   SET @FixedCharges = NULL  
   SET @TaxValue = NULL  
      
   SET @PreviousReading  = NULL        
   SET @CurrentReading   = NULL       
   SET @Usage   = NULL  
   SET @ReadType =NULL  
   SET @BillType   = NULL       
   SET @AdjustmentAmount    = NULL  
   SET @RemainingBalanceUnits   = NULL   
   SET @TotalBillAmountWithArrears=NULL  
   SET @BookNo=NULL  
   SET @AverageUsageForNewBill=NULL   
   SET @IsHaveLatest=0  
   SET @ActualUsage=NULL  
   SET @RegenCustomerBillId =NULL  
   SET @PaidAmount  =NULL  
   SET @PrevBillTotalPaidAmount =NULL  
   SET @PreviousBalance =NULL  
   SET @OpeningBalance =NULL  
   SET @CustomerFullName  =NULL  
   SET @BusinessUnitName  =NULL  
   SET @TariffName  =NULL  
   SET @ReadDate  =NULL  
   SET @Multiplier  =NULL  
   SET @Service_HouseNo  =NULL  
   SET @Service_Street  =NULL  
   SET @Service_City  =NULL  
   SET @ServiceZipCode =NULL  
   SET @Postal_HouseNo  =NULL  
   SET @Postal_Street =NULL  
   SET @Postal_City  =NULL  
   SET @Postal_ZipCode  =NULL  
   SET @Postal_LandMark =NULL  
   SET @Service_LandMark =NULL  
   SET @OldAccountNumber=NULL   
   SET @DisableDate=NULL  
   SET @BillingComments=NULL
   SET @TotalBillAmount=NULL
   SET @PaidMeterDeductedAmount=NULL
   SET @IsEmbassyCustomer=NULL
   SET @TaxPercentage=NULL
   SET @PaidMeterDeductedAmount=NULL
	SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
 END  
 ELSE
	BEGIN
	 SELECT 0 AS NoRows   
	 SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
	END
END  
				 










GO


