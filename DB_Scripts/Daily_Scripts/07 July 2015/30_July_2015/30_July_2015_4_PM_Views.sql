
GO

/****** Object:  View [dbo].[UDV_CustomerDetailsForBillPDF]    Script Date: 07/30/2015 16:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UDV_CustomerDetailsForBillPDF] AS

	SELECT	
		 CD.GlobalAccountNumber
		,CD.AccountNo
		,CD.OldAccountNo
		,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS FullName
		,ISNULL(CD.HomeContactNumber,ISNULL(CD.BusinessContactNumber,ISNULL(CD.OtherContactNumber,'--'))) AS ContactNo
		,CD.ActiveStatusId
		,PD.PoleID
		,[dbo].[fn_GetCustomerPoleNo_ByPoleId](PD.PoleID) AS PoleNo
		,PD.TariffClassID AS TariffId
		,TC.ClassName AS TariffName
		,TCL.ClassName AS TariffCategory
		,PD.ReadCodeID
		,CAD.OutStandingAmount
		,BN.BookNo
		,BU.BU_ID
		,BU.BusinessUnitName
		,SU.SU_ID
		,SU.ServiceUnitName
		,SC.ServiceCenterId
		,SC.ServiceCenterName
		,C.CycleId
		,C.CycleName
		,CD.EmailId
		,PD.MeterNumber
		,ISNULL(MI.IsCAPMIMeter,0) AS IsCAPMIMeter
		,ISNULL(MI.CAPMIAmount,0) AS CAPMIAmount
		,PD.PhaseId
		,CAD.InitialBillingKWh 
		,PD.RouteSequenceNumber
		,R.RouteName  
		,PD.CustomerTypeId
		,CT.CustomerType
		,CAD.OpeningBalance
		,[dbo].[fn_GetCustomerServiceAddress_New](CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,'',CD.Service_ZipCode) AS ServiceAddress
	FROM CUSTOMERS.Tbl_CustomerSDetail AS CD 
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
	LEFT JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber
	LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId
	INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
	LEFT JOIN dbo.Tbl_MTariffClasses AS TCL ON TCL.ClassID = TC.RefClassID 
	LEFT JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId 
	LEFT JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
	LEFT JOIN dbo.Tbl_MCustomerTypes AS CT ON CT.CustomerTypeId = PD.CustomerTypeId
	LEFT JOIN dbo.Tbl_MRoutes AS R ON R.RouteId = PD.RouteSequenceNumber


GO


