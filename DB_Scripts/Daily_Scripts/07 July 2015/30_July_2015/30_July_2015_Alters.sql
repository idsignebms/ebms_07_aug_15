INSERT INTO TBL_FunctionalAccessPermission ( [Function] ,AccessLevels ,IsActive ,CreatedBy ,CreatedDate ) 
VALUES ( 'CustomerRegistration' ,2 ,1 ,'Admin' ,DBO.fn_GetCurrentDateTime() )

GO
INSERT INTO TBL_FunctionalAccessPermission([Function],AccessLevels,IsActive,CreatedBy,CreatedDate)
VALUES('Present Reading Adjustment',2,1,'Admin',DBO.fn_GetCurrentDateTime())

GO

CREATE TABLE Tbl_PresentReadingAdjustmentApprovalLog
(
	 PresentReadingAdjustmentLogId INT IDENTITY(1,1) PRIMARY KEY
	,GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,NewPresentReading VARCHAR(50)
	,PreviousReadDate DATETIME
	,PresentReadDate DATETIME
	,MeterNumber VARCHAR(50)
	,Remarks VARCHAR(MAX)
	,ApprovalStatusId INT
	,PresentApprovalRole INT
	,NextApprovalRole INT
	,CurrentAppovalLevel INT
	,IsLocked BIT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
	,ModifiedBy VARCHAR(50)
	,ModifiedDate DATETIME
)