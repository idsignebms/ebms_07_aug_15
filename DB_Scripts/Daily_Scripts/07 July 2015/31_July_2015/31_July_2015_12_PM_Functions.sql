
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerPoleNo_ByPoleId]    Script Date: 07/31/2015 12:22:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek.P
-- Create date: 30-07-2015
-- Description:	To Get the pole no
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerPoleNo_ByPoleId]
(
	@PoleId INT
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PoleNo VARCHAR(50)

	SELECT @PoleNo = ''
 
	RETURN @PoleNo
END
----------------------------------------------------------------------------------------------------------

GO


