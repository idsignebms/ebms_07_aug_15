
GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrationsAuditRay]    Script Date: 08/01/2015 13:43:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrationsAuditRay]
(
	@XmlDoc xml
)
AS
BEGIN 

--------------------------------------------
--              Declaration
--------------------------------------------

	DECLARE 
	@BU_ID VARCHAR(MAX) --='BEDC_BU_0012' 
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT -- = 15
   ,@ApprovalRoleId INT 
   ,@ARID INT 
   ,@SU_ID VARCHAR(MAX)--='BEDC_SU_0036'  
   ,@SC_ID VARCHAR(MAX)--='BEDC_SC_0043,BEDC_SC_0044'  
   ,@CycleId VARCHAR(MAX)--='BEDC_C_0176,BEDC_C_0179,BEDC_C_0180,BEDC_C_0177,BEDC_C_0178,BEDC_C_0192'  
   ,@TariffId VARCHAR(MAX)--='2,3,4,5,7,8,9,11,12,13,15,16,19,21,22,39,44,45,47,49,50,51,54,55'  
   ,@BookNo VARCHAR(MAX)--='BEDC_B_1009,BEDC_B_1010,BEDC_B_1011,BEDC_B_1012,BEDC_B_1013,BEDC_B_1014,BEDC_B_1015,BEDC_B_1016,BEDC_B_1017,BEDC_B_1018'  
   ,@FromDate VARCHAR(20)--='07/04/2015'  
   ,@ToDate VARCHAR(20)--='08/01/2015' 
   ,@PageNo INT--=1
   ,@PageSize INT--=5 
        
--------------------------------------------
--              Deserialization
--------------------------------------------
        
	 SELECT       
	 
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
	    
	  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
	 
	 
--------------------------------------------
--              Initialization on variables
-------------------------------------------- 
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 

--------------------------------------------
--              Requirement
-------------------------------------------- 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
   ;WITH PageResult AS
   (		
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.PoleID    
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal        
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading 
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Remarks WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Remarks END AS Remarks  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS PhaseId  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  ,CD.OutStandingAmount_ActiveDetails
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	  ,CONVERT(VARCHAR(19),CD.CreatedDate) AS CreatedDate
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 --WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
			AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
			AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
			AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
			AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,','))
	 )
 	 SELECT * FROM PageResult
	 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
		ORDER BY CreatedDate DESC 
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/01/2015 13:43:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50)
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		
	SELECT
		 @AccounNo = U.AccountNo + ' - ' + U.GlobalAccountNumber
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookNo
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.DisplayCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerBillPayments
			WHERE CustomerBillId = @CustomerBillId
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CustomerBillId = @CustomerBillId
	
		END
	
	SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.DisplayCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		,ISNULL((SELECT TOP 1 
				REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
			FROM Tbl_CustomerReadings CR3
			WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
				AND CR3.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR3.CustomerReadingId DESC),ISNULL(@InitialBillingKWh,0))
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,@NetArrears AS NetArrears
			,@Payments AS Payments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,@LastPaidAmount AS LastPaidAmount
			,@LastPaidDate AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrationsAuditRay]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrationsAuditRay]
(
	@XmlDoc xml
)
AS
BEGIN 

--------------------------------------------
--              Declaration
--------------------------------------------

	DECLARE 
	@BU_ID VARCHAR(MAX) --='BEDC_BU_0012' 
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT -- = 15
   ,@ApprovalRoleId INT 
   ,@ARID INT 
   ,@SU_ID VARCHAR(MAX)--='BEDC_SU_0036'  
   ,@SC_ID VARCHAR(MAX)--='BEDC_SC_0043,BEDC_SC_0044'  
   ,@CycleId VARCHAR(MAX)--='BEDC_C_0176,BEDC_C_0179,BEDC_C_0180,BEDC_C_0177,BEDC_C_0178,BEDC_C_0192'  
   ,@TariffId VARCHAR(MAX)--='2,3,4,5,7,8,9,11,12,13,15,16,19,21,22,39,44,45,47,49,50,51,54,55'  
   ,@BookNo VARCHAR(MAX)--='BEDC_B_1009,BEDC_B_1010,BEDC_B_1011,BEDC_B_1012,BEDC_B_1013,BEDC_B_1014,BEDC_B_1015,BEDC_B_1016,BEDC_B_1017,BEDC_B_1018'  
   ,@FromDate VARCHAR(20)--='07/04/2015'  
   ,@ToDate VARCHAR(20)--='08/01/2015' 
   ,@PageNo INT--=1
   ,@PageSize INT--=5 
        
--------------------------------------------
--              Deserialization
--------------------------------------------
        
	 SELECT       
	 
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
	    
	  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
	 
	 
--------------------------------------------
--              Initialization on variables
-------------------------------------------- 
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 

--------------------------------------------
--              Requirement
-------------------------------------------- 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
   ;WITH PageResult AS
   (		
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.PoleID    
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal        
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading 
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Remarks WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Remarks END AS Remarks  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS PhaseId  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  ,CD.OutStandingAmount_ActiveDetails
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	  ,CONVERT(VARCHAR(19),CD.CreatedDate) AS CreatedDate
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 --WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
			AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
			AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
			AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
			AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,','))
	 )
 	 SELECT * FROM PageResult
	 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
		ORDER BY CreatedDate DESC 
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50)
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		
	SELECT
		 @AccounNo = U.AccountNo + ' - ' + U.GlobalAccountNumber
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookNo
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.DisplayCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerBillPayments
			WHERE CustomerBillId = @CustomerBillId
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CustomerBillId = @CustomerBillId
	
		END
	
	SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.DisplayCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		,ISNULL((SELECT TOP 1 
				REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
			FROM Tbl_CustomerReadings CR3
			WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
				AND CR3.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR3.CustomerReadingId DESC),ISNULL(@InitialBillingKWh,0))
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,@NetArrears AS NetArrears
			,@Payments AS Payments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,@LastPaidAmount AS LastPaidAmount
			,@LastPaidDate AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_MasterSearch]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  Bhimaraju Vanka            
-- Create date: 15-Nov-2014            
-- Description: This is the proc about search masters using flag            
-- MODIFIED BY : NEERAJ KANOJIYA            
-- MODIFIED DATE: 17-NOV-2014            
-- Modified By: Padmini            
-- Modified Date:17-Feb-2015            
-- Description: Getting it from Bookno,Cycle,SC,SU & BU order            
-- Modified By: Faiz            
-- Modified Date:27-Mar-2015          
-- Description: Modified BookNo and Id for flag-5 for book no search          
-- Modified By: Faiz            
-- Modified Date:03-Apr-2015        
-- Description: Added Meter Type to search with meter type in Meter Information Search        
-- Modified By :kalyan  
-- modified Date :30-07-2015  
-- Modfied Description : Added IsCapmi and Capmi amount to @Flag=9 and modified some fields for @Flag=7    
-- Midified By :Kalyan
-- Modfied Description : Added Search Needed Fileds for User of @Flag=8    

-- =============================================            
ALTER PROCEDURE [dbo].[USP_MasterSearch]            
 (@XmlDoc Xml=null)            
AS            
BEGIN            
 DECLARE  @BusinessUnit VARCHAR(50)            
   ,@BusinessUnitCode VARCHAR(50)            
   ,@ServiceUnit VARCHAR(50)            
   ,@ServiceUnitCode VARCHAR(50)            
   ,@ServiceCenter VARCHAR(50)            
   ,@ServiceCenterCode VARCHAR(50)            
   ,@CycleName VARCHAR(50)            
   ,@CycleCode VARCHAR(50)            
   ,@BookNo VARCHAR(50)            
   ,@BookCode VARCHAR(50)            
   ,@RouteName VARCHAR(50)            
   ,@RouteCode VARCHAR(50)            
   ,@MeterNo VARCHAR(50)            
   ,@MeterSerialNo VARCHAR(50)            
   ,@Brand VARCHAR(50)          
   ,@MeterType VARCHAR(50)  --Faiz-ID103        
   ,@Name VARCHAR(50)            
   ,@ContactNo VARCHAR(50)            
   ,@RoleId INT            
   ,@DesignationId INT            
   ,@PageNo INT            
   ,@PageSize INT            
   ,@Flag INT            
   ,@BU_ID  VARCHAR(50)            
               
 SELECT   @BusinessUnit=C.value('(BusinessUnit)[1]','VARCHAR(50)')            
   ,@BusinessUnitCode=C.value('(BusinessUnitCode)[1]','VARCHAR(50)')            
   ,@ServiceUnit=C.value('(ServiceUnit)[1]','VARCHAR(50)')            
   ,@ServiceUnitCode=C.value('(ServiceUnitCode)[1]','VARCHAR(50)')            
   ,@ServiceCenter=C.value('(ServiceCenter)[1]','VARCHAR(50)')            
   ,@ServiceCenterCode=C.value('(ServiceCenterCode)[1]','VARCHAR(50)')            
   ,@CycleName=C.value('(CycleName)[1]','VARCHAR(50)')            
   ,@CycleCode=C.value('(CycleCode)[1]','VARCHAR(50)')            
   ,@BookNo=C.value('(BookNo)[1]','VARCHAR(50)')            
   ,@BookCode=C.value('(BookCode)[1]','VARCHAR(50)')            
   ,@RouteName=C.value('(RouteName)[1]','VARCHAR(50)')            
   ,@RouteCode=C.value('(RouteCode)[1]','VARCHAR(50)')            
   ,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(50)')            
   ,@MeterSerialNo=C.value('(MeterSerialNo)[1]','VARCHAR(50)')            
   ,@Brand=C.value('(Brand)[1]','VARCHAR(50)')             
   ,@MeterType=C.value('(MeterType)[1]','VARCHAR(50)')    --Faiz-ID103        
   ,@Name=C.value('(Name)[1]','VARCHAR(50)')            
   ,@ContactNo=C.value('(ContactNo)[1]','VARCHAR(50)')            
   ,@RoleId=C.value('(RoleId)[1]','INT')            
   ,@DesignationId=C.value('(DesignationId)[1]','INT')            
   ,@PageNo=C.value('(PageNo)[1]','INT')            
   ,@PageSize=C.value('(PageSize)[1]','INT')            
   ,@Flag=C.value('(Flag)[1]','INT')               
   ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')               
  FROM @XmlDoc.nodes('MasterSearchBE') as T(C)            
              
  --============================== ** Note ** ==========================            
  --@Flag = 1 --Bussiness Unit Search            
  --@Flag = 2 --Service Unit Search            
  --@Flag = 3 --Service Center Search            
  --@Flag = 4 --Cycle Search            
  --@Flag = 5 --Book Search            
  --@Flag = 6 --Route Management Search            
  --@Flag = 7 --Meter Information Search            
  --@Flag = 8 --User Search            
  --@Flag = 9 --Change Customer BU            
  --============================== ** Note ** ==========================            
              
  IF (@Flag = 1)            
  BEGIN            
   ;WITH PagedResults AS        
  (            
    SELECT B.BU_ID            
      ,B.BusinessUnitName            
      ,ROW_NUMBER() OVER(ORDER BY B.BU_ID ASC) AS RowNumber            
      ,ISNULL(B.BUCode,'-') AS BusinessUnitCode            
      ,B.Notes            
      ,(S.StateName +' ( '+ S.StateCode +' )')  AS StateName            
      ,S.StateCode            
    FROM Tbl_BussinessUnits B            
    LEFT JOIN Tbl_States S ON S.StateCode=B.StateCode            
    WHERE             
    (ISNULL(BusinessUnitName,'') like '%'+@BusinessUnit+'%' OR @BusinessUnit='')            
    AND (ISNULL(BUCode,'') like '%'+@BusinessUnitCode+'%' OR @BusinessUnitCode='')            
    --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
    AND (B.BU_ID=@BU_ID OR @BU_ID='')            
  )              
   SELECT              
    *                
    ,(Select COUNT(0) from PagedResults) as TotalRecords            
    FROM PagedResults p            
    WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
  END            
  ELSE IF(@Flag = 2)            
  BEGIN            
  ;WITH PagedResults AS            
  (            
   --SELECT  --S.SU_ID            
   --  S.ServiceUnitName            
   --  ,ROW_NUMBER() OVER(ORDER BY S.SU_ID ASC) AS RowNumber            
   --  ,ISNULL(S.SUCode,'--') AS ServiceUnitCode            
   --  ,S.Notes            
   --  ,B.BusinessUnitName            
   --  ,B.StateCode            
   --  ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName            
   --FROM Tbl_ServiceUnits S            
   --LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID            
   --LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode            
   --WHERE (ISNULL(ServiceUnitName,'') like '%'+@ServiceUnit+'%' OR @ServiceUnit='')            
   --AND (ISNULL(SUCode,'') like '%'+@ServiceUnitCode+'%' OR @ServiceUnitCode='')            
   ----AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
   --AND (B.BU_ID=@BU_ID OR @BU_ID='') 
   SELECT
   ROW_NUMBER() OVER(ORDER BY SU.ActiveStatusId ASC , SU.CREATEDDATE DESC ) AS RowNumber  
      ,BU.BusinessUnitName  
     ,SU_ID  
     ,ServiceUnitName  
     ,isnull(SU.Notes ,'---') as  Notes
     ,SU.CreatedDate       
     ,SU.BU_ID  
     ,SU.ActiveStatusId  
     ,SUCode  
     ,SU.Address1
     ,SU.Address2
     ,SU.City
     ,SU.ZIP
     ,S.StateName
   FROM Tbl_ServiceUnits SU  
   JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  
   JOIN Tbl_States S ON S.StateCode = BU.StateCode  
   WHERE (ISNULL(ServiceUnitName,'') like '%'+@ServiceUnit+'%' OR @ServiceUnit='')            
   AND (ISNULL(SUCode,'') like '%'+@ServiceUnitCode+'%' OR @ServiceUnitCode='')            
   AND (Bu.BU_ID=@BU_ID OR @BU_ID='') 
  )            
              
  SELECT              
   *                
   ,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
               
  END            
  ELSE IF(@Flag = 3)            
  BEGIN            
  ;WITH PagedResults AS            
  (            
   --SELECT  --C.ServiceCenterId            
   --  C.ServiceCenterName            
   --  ,ROW_NUMBER() OVER(ORDER BY C.ServiceCenterId ASC) AS RowNumber            
   --  ,C.SCCode            
   --  ,C.Notes            
   --  ,S.ServiceUnitName            
   --  ,B.StateCode            
   --  ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName            
   --FROM Tbl_ServiceCenter C            
   --LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=C.SU_ID            
   --LEFT JOIN Tbl_BussinessUnits B ON S.BU_ID=B.BU_ID            
   --LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode            
   --WHERE (ISNULL(ServiceCenterName,'') like '%'+@ServiceCenter+'%' OR @ServiceCenter='')            
   --AND (ISNULL(SCCode,'') like '%'+@ServiceCenterCode+'%' OR @ServiceCenterCode='')            
   ----AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
   --AND (B.BU_ID=@BU_ID OR @BU_ID='')  
   SELECT    
      ROW_NUMBER() OVER(ORDER BY SC.ActiveStatusId ASC , SC.CREATEDDATE DESC ) AS RowNumber    
     ,ServiceCenterId    
     ,ServiceCenterName    
     ,SC.Notes    
     ,SC.CreatedDate         
     ,SC.SU_ID    
     ,SC.ActiveStatusId    
     ,SCCode    
     ,SC.Address1  
     ,SC.Address2  
     ,SU.ServiceUnitName
     ,SC.City  
     ,SC.ZIP  
     ,S.StateName  
   FROM Tbl_ServiceCenter SC    
   JOIN Tbl_ServiceUnits SU ON SC.SU_ID=SU.SU_ID    
   JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID    
   JOIN Tbl_States S ON S.StateCode=BU.StateCode   
   WHERE (ISNULL(ServiceCenterName,'') like '%'+@ServiceCenter+'%' OR @ServiceCenter='')            
   AND (ISNULL(SCCode,'') like '%'+@ServiceCenterCode+'%' OR @ServiceCenterCode='')            
   --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
   AND (BU.BU_ID=@BU_ID OR @BU_ID='') 
  )            
              
  SELECT              
   *                
   ,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
               
  END            
  ELSE IF(@Flag = 4)            
  BEGIN            
   ;WITH PagedResults AS            
  (            
   --SELECT  --C.CycleId            
   --  ROW_NUMBER() OVER(ORDER BY C.CycleId ASC) AS RowNumber            
   --  ,C.CycleCode            
   --  ,C.CycleName            
   --  ,C.ContactName            
   --  ,C.ContactNo            
   --  ,C.DetailsOfCycle            
   --  ,B.BusinessUnitName AS BusinessUnit            
   --  ,S.ServiceUnitName AS ServiceUnit            
   --  ,SC.ServiceCenterName AS ServiceCenter            
   --  ,B.StateCode            
   --  ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName            
   --FROM Tbl_Cycles C            
   --LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId            
   --LEFT JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID            
   --LEFT JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID            
   --LEFT JOIN Tbl_States ST ON ST.StateCode=B.StateCode            
   --WHERE (ISNULL(CycleCode,'') like '%'+@CycleCode+'%' OR @CycleCode='')            
   --AND (ISNULL(CycleName,'') like '%'+@CycleName+'%' OR @CycleName='')            
   ----AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
   --AND (B.BU_ID=@BU_ID OR @BU_ID='')     
        
    Select
    ROW_NUMBER() OVER(ORDER BY C.ActiveStatusId ASC , C.CreatedDate DESC ) AS RowNumber        
    ,C.CycleId        
    ,C.CycleName        
    --,CycleNo        
    ,ISNULL(C.DetailsOfCycle,'---') AS Details        
    ,C.ActiveStatusId        
    ,C.CreatedBy      
    --,C.BU_ID        
    --,C.SU_ID        
    ,B.BU_ID        
    ,S.SU_ID      
    ,C.ServiceCenterId        
    ,ISNULL(C.CycleCode,'--') AS CycleCode      
    ,B.BusinessUnitName      
    ,ST.StateName      
    ,S.ServiceUnitName      
    ,SC.ServiceCenterName      
	 FROM Tbl_Cycles AS C      
	 JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1    
	 JOIN Tbl_ServiceUnits S ON S.SU_ID=SC.SU_ID  AND S.ActiveStatusId = 1    
	 JOIN Tbl_BussinessUnits B ON B.BU_ID=S.BU_ID  AND B.ActiveStatusId = 1    
	 JOIN Tbl_States ST ON ST.StateCode=B.StateCode  AND ST.IsActive = 1    
	 AND C.ActiveStatusId in (1,2)  
	 WHERE (ISNULL(CycleCode,'') like '%'+@CycleCode+'%' OR @CycleCode='')            
	 AND (ISNULL(CycleName,'') like '%'+@CycleName+'%' OR @CycleName='')            
	 --AND B.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
	 AND (B.BU_ID=@BU_ID OR @BU_ID='')         
   )            
              
  SELECT              
   *                
,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
               
  END            
  ELSE IF(@Flag = 5)            
  BEGIN            
  ;WITH PagedResults AS            
  (            
   --SELECT  --B.BookNo --Faiz-ID103          
   --B.ID AS BookNo           
   --  ,B.BookCode            
   --  ,B.Details            
   --  ,B.NoOfAccounts            
   --  ,ROW_NUMBER() OVER(ORDER BY B.BookNo ASC) AS RowNumber            
   --  ,C.CycleName            
   --  ,BU.BusinessUnitName AS BusinessUnit            
   --  ,SU.ServiceUnitName AS ServiceUnitName            
   --  ,SC.ServiceCenterName AS ServiceCenterName            
   --  ,BU.StateCode            
   --  ,BU.BusinessUnitName            
   --  ,(ST.StateName +' ( '+ ST.StateCode +' )')  AS StateName            
   --FROM Tbl_BookNumbers B            
   --LEFT JOIN Tbl_Cycles C ON C.CycleId=B.CycleId            
   --LEFT JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId            
   --LEFT JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID            
   --LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID            
   --LEFT JOIN Tbl_States ST ON ST.StateCode=BU.StateCode            
   --WHERE (ISNULL(BookCode,'') like '%'+@BookCode+'%' OR @BookCode='')            
   --AND (ISNULL(B.ID,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103          
   ----AND (ISNULL(BookNo,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103          
   ----AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
   --AND (BU.BU_ID=@BU_ID OR @BU_ID='')          
   
    SELECT        
     ROW_NUMBER() OVER(ORDER BY BN.ActiveStatusId ASC , BN.CreatedDate DESC ) AS RowNumber        
    ,BN.ID AS BookNo      
    ,(SELECT dbo.fn_GetCycleIdByBook(BN.BookNo)) As CycleId      
    ,(SELECT CycleName From Tbl_Cycles WHERE CycleId=(SELECT dbo.fn_GetCycleIdByBook(BookNo))) CycleName      
    ,BN.ActiveStatusId        
    ,BN.CreatedBy        
    ,BN.NoOfAccounts        
    --,Details        
    ,ISNULL(BN.Details,'---')AS Details        
    ,BU.BU_ID        
    ,SU.SU_ID        
    ,SC.ServiceCenterId        
    ,ISNULL(BN.BookCode,'---')AS BookCode        
    ,BU.BusinessUnitName AS BusinessUnitName        
    ,SU.ServiceUnitName  AS ServiceUnitName        
    ,SC.ServiceCenterName AS ServiceCenterName      
	,MarketerId      
	,ISNULL(dbo.fn_GetMarketersFullName(MarketerId),'--') AS Marketer      
	,ST.StateName         
	,BN.BookNo AS BNO  
	FROM Tbl_BookNumbers AS BN      
	JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId  AND C.ActiveStatusId = 1    
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  AND SC.ActiveStatusId = 1    
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  AND SU.ActiveStatusId = 1    
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID  AND BU.ActiveStatusId = 1    
	JOIN Tbl_States ST ON ST.StateCode=BU.StateCode  AND ST.IsActive = 1  
	WHERE (ISNULL(BookCode,'') like '%'+@BookCode+'%' OR @BookCode='')            
	AND (ISNULL(BN.ID,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103          
	--AND (ISNULL(BookNo,'') like '%'+@BookNo+'%' OR @BookNo='')  --Faiz-ID103          
	--AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
	AND (BU.BU_ID=@BU_ID OR @BU_ID='')  
   )            
              
  SELECT              
   *                
   ,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
  END            
  ELSE IF(@Flag = 6)            
  BEGIN            
   ;WITH PagedResults AS            
  (            
   SELECT  R.RouteName            
     ,R.RouteCode            
     ,R.Details            
     ,ROW_NUMBER() OVER(ORDER BY R.RouteName ASC) AS RowNumber            
     ,BU.BusinessUnitName AS BusinessUnit            
     ,BU.StateCode            
   FROM Tbl_MRoutes R            
   LEFT JOIN Tbl_BussinessUnits BU ON BU.BU_ID=R.BU_ID            
   WHERE (ISNULL(RouteName,'') like '%'+@RouteName+'%' OR @RouteName='')            
   AND (ISNULL(RouteCode,'') like '%'+@RouteCode+'%' OR @RouteCode='')            
   --AND BU.StateCode=(SELECT BUE.StateCode FROM TBL_BussinessUnits AS BUE WHERE BU_ID=@BU_ID)            
   AND (BU.BU_ID=@BU_ID OR @BU_ID='')            
   )            
              
  SELECT              
   *                
   ,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
  END            
  ELSE IF(@Flag = 7)            
  BEGIN            
   ;WITH PagedResults AS            
  (            
   SELECT  M.MeterNo            
     ,M.MeterSerialNo            
     ,MT.MeterType            
     ,M.MeterSize            
     ,M.MeterBrand            
     ,M.MeterRating            
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber            
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate            
     ,ISNULL(MeterDetails,'---') AS MeterDetails     
     ,MeterDials  
     ,ISNULL(CPD.GlobalAccountNumber,'--') AS AccountNo         
    -- ,M.MeterDetails            
     ,M.MeterMultiplier            
     ,M.MeterId            
     ,M.BU_ID           
     ,M.ActiveStatusId  
     ,M.MeterType AS MeterTypeId  
      ,ISNULL(ServiceHoursBeforeCalibration,'--') AS ServiceHoursBeforeCalibration   
     --,M.IsCAPMIMeter      
     --,M.CAPMIAmount      
     ,(case m.IsCAPMIMeter when 0 then 'No' else 'Yes' end) as IsCAPMIMeter    
     ,(case m.CAPMIAmount when 0 then '--' else CONVERT(varchar(20),m.CAPMIAmount) End) as CAPMIAmount    
    ,B.BusinessUnitName          
    ,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs AM WHERE AM.MeterNo = M.MeterNo AND ApprovalStatusId IN(1,4)) > 0) THEN 1    
   WHEN ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs AM WHERE AM.NewMeterNo = M.MeterNo AND ApproveStatusId IN(1,4)) > 0) THEN 1    
   ELSE 0 END) AS ActiveStatus      
   FROM Tbl_MeterInformation M            
   JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType AND MT.ActiveStatus=1            
   LEFT JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID             
   LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.MeterNumber=M.MeterNo      
   WHERE (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')            
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')            
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')            
   AND (ISNULL(MT.MeterType,'')=@MeterType OR @MeterType='')    --Faiz-ID103        
   AND (M.BU_ID=@BU_ID OR @BU_ID='')            
   )            
              
  SELECT              
   *                
   ,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
  END            
  ELSE IF(@Flag = 8)            
  BEGIN            
   ;WITH PagedResults AS            
  (            
   SELECT  U.UserId AS EmployeeId          
     ,U.Name            
     ,ROW_NUMBER() OVER(ORDER BY U.UserId ASC) AS RowNumber            
     ,U.SurName            
     ,U.PrimaryContact AS ContactNo1
     ,ISNULL(U.SecondaryContact,'---') AS ContactNo2              
     ,U.PrimaryEmailId            
     ,ISNULL(U.SecondaryEmailId,'---') AS SecondaryEmailId
     ,U.[Address]
     ,U.GenderId
	 ,(SELECT GenderName From Tbl_MGender Where GenderId=U.GenderId) As GenderName  
     ,ISNULL(U.Details,'---') AS Details  
     ,U.RoleId  
     ,(SELECT RoleName From Tbl_MRoles Where RoleId=U.RoleId) As RoleName
     ,(SELECT DesignationName From Tbl_MDesignations Where DesignationId=D.DesignationId) As DesignationName  
     ,U.UserDetailsId  
     ,ISNULL(U.Photo,'--') AS Photo  
     ,ISNULL(U.FilePath,'--') AS FilePath  
     ,L.ActiveStatusId  
     ,U.IsMobileAccess    
     ,U.DesignationId 
     --,D.DesignationName AS Designation            
     --,R.RoleName            
     ,A.Status             
   FROM Tbl_UserDetails U            
   JOIN Tbl_MDesignations D ON D.DesignationId=U.DesignationId            
   JOIN Tbl_MRoles R ON R.RoleId=U.RoleId            
   JOIN Tbl_UserLoginDetails L ON L.UserId=U.UserId            
   JOIN Tbl_MActiveStatusDetails A ON L.ActiveStatusId=A.ActiveStatusId            
   AND L.ActiveStatusId IN (1,2)            
   AND ((ISNULL(Name,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(SurName,'') like '%'+@Name+'%' OR @Name='') OR (ISNULL(PrimaryEmailId,'') like '%'+@Name+'%' OR @Name=''))            
   AND (ISNULL(PrimaryContact,'') like '%'+@ContactNo+'%' OR @ContactNo='')            
   AND (ISNULL(R.RoleId,0)=@RoleId OR @RoleId=0)            
   AND (ISNULL(D.DesignationId,0)=@DesignationId OR @DesignationId=0)               
   )            
              
  SELECT *,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
  END            
  ELSE IF(@Flag = 9)            
  BEGIN            
   ;WITH PagedResults AS            
  (            
   SELECT  M.MeterNo            
     ,M.MeterSerialNo            
     ,MT.MeterType            
     ,M.MeterSize            
     ,M.MeterBrand            
     ,M.MeterRating            
     ,ROW_NUMBER() OVER(ORDER BY M.MeterNo ASC) AS RowNumber            
     ,CONVERT(VARCHAR(50),M.NextCalibrationDate,106) AS NextCalibrationDate            
     ,M.MeterDials            
     ,M.MeterDetails            
     ,M.MeterMultiplier            
     ,M.MeterId            
     ,M.BU_ID            
    ,B.BusinessUnitName      
    ,(case m.IsCAPMIMeter when 0 then 'No' else 'Yes' end) as IsCAPMIMeter    
    ,(case m.CAPMIAmount when 0 then '--' else CONVERT(varchar(20),m.CAPMIAmount) End) as CAPMIAmount    
   FROM Tbl_MeterInformation M            
   LEFT JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId=M.MeterType            
   JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID            
   AND M.MeterNo NOT IN (SELECT CPD.MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD WHERE CPD.MeterNumber IS Not NULL)            
   AND M.ActiveStatusId IN(1,2)            
   AND (ISNULL(MeterNo,'') like '%'+@MeterNo+'%' OR @MeterNo='')            
   AND (ISNULL(MeterSerialNo,'') like '%'+@MeterSerialNo+'%' OR @MeterSerialNo='')            
   AND (ISNULL(MeterBrand,'') like '%'+@Brand+'%' OR @Brand='')          
   AND (M.BU_ID=@BU_ID OR @BU_ID='')            
   )            
              
  SELECT           
   *                
   ,(Select COUNT(0) from PagedResults) as TotalRecords            
   FROM PagedResults p            
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize            
  END            
 END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetails]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author  :  NEERAJ KANOJIYA  
-- Create date :  27-MARCH-2015  
-- Description :  GET CUSTOMERS FULL DETAILS  
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetCustomerDetails]    
 (    
 @XmlDoc xml
 )    
AS    
BEGIN    
  
    DECLARE  @GlobalAccountNumber VARCHAR(50)    
    SELECT              
    @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')   
  FROM @XmlDoc.nodes('CustomerRegistrationBE') AS T(C)       
  SELECT top 1  
    
  CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
  ,CASE CD.HomeContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNo END AS HomeContactNumberLandlord  
  ,CASE CD.BusinessContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNo END AS BusinessPhoneNumberLandlord  
  ,CASE CD.OtherContactNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNo END AS OtherPhoneNumberLandlord  
  ,CD.DocumentNo   
  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
  ,CD.OldAccountNo   
  ,CT.CustomerType  
  ,(CD.BookId+' ( '+CD.BookCode+' )') AS BookCode
  ,CD.PoleID   
  ,CD.MeterNumber   
  ,TC.ClassName as Tariff   
  ,CPD.IsEmbassyCustomer   
  ,CPD.EmbassyCode   
  ,CD.PhaseId   
  ,RC.ReadCode as ReadType  
  ,CD.IsVIPCustomer  
  ,CD.IsBEDCEmployee  
  ,CASE PAD.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.HouseNo END AS HouseNoService   
  ,CASE PAD.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.StreetName END AS StreetService   
  ,CASE PAD.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.City END AS CityService  
  ,PAD.AreaCode AS AreaService   
  ,CASE PAD.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD.ZipCode END AS SZipCode     
  ,PAD.IsCommunication AS IsCommunicationService     
  ,CASE PAD1.HouseNo WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.HouseNo END AS HouseNoPostal   
  ,CASE PAD1.StreetName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.StreetName END AS StreetPostal   
  ,CASE PAD1.City WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.City END AS  CityPostaL   
  ,ISNULL(CONVERT(VARCHAR(10),PAD1.AreaCode),'--') AS  AreaPostal    --Faiz-ID103
  ,CASE PAD1.ZipCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE PAD1.ZipCode END AS  PZipCode  
  ,PAD1.IsCommunication AS IsCommunicationPostal      
  ,PAD.AddressID AS ServiceAddressID    
  ,CAD.IsCAPMI  
  ,CAD.InitialBillingKWh   
  ,CAD.InitialReading   
  ,CAD.PresentReading --Faiz-ID103  
  ,CD.AvgReading as AverageReading   
  ,CD.Highestconsumption   
  ,CD.OutStandingAmount   
  ,OpeningBalance  
  ,CASE APD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal1 END AS Seal1  
  ,CASE APD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.Seal2 END AS Seal2  
  ,CASE MAT.AccountType WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MAT.AccountType END AS AccountType  
  ,CASE CD.ClassName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClassName END AS ClassName  
  ,CASE MCC.CategoryName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MCC.CategoryName END AS ClusterCategoryName  
  ,CASE MPH.Phase WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MPH.Phase END AS Phase  
  ,CASE MRT.RouteName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE MRT.RouteName END AS RouteName  
  ,CTD.TenentId  
  ,CASE CTD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.Title END AS TitleTanent  
  ,CASE CTD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.FirstName END AS FirstNameTanent  
  ,CASE CTD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.MiddleName END AS MiddleNameTanent  
  ,CASE CTD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.LastName END AS LastNameTanent  
  ,CASE CTD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.PhoneNumber END AS PhoneNumberTanent  
  ,CASE CTD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
  ,CASE CTD.EmailID WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CTD.EmailID END AS EmailIdTanent  
  ,CASE EMP.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS EmployeeName  
  ,CASE APD.ApplicationProcessedBy WHEN '' THEN '--' WHEN NULL THEN '--' ELSE APD.ApplicationProcessedBy END AS ApplicationProcessedBy  
  ,CASE EMP1.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP1.EmployeeName END AS EmployeeNameProcessBy  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE AGN.AgencyName END AS AgencyName  
  ,CASE AGN.AgencyName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
  ,CASE EMP.EmployeeName WHEN ''THEN '--' WHEN NULL THEN '--' ELSE EMP.EmployeeName END AS CertifiedBy1  
  ,CASE EMP2.EmployeeName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE EMP2.EmployeeName END AS CertifiedBy   
  ,CD.GlobalAccountNumber  
  ,CD.IsSameAsService  
  ,CS.StatusName AS [Status]--Faiz-ID103
  ,CD.OutStandingAmount
  ,CD.AccountNo --Faiz-ID103
  ,PMD.OutStandingAmount AS CapmiOutstandingAmount--Faiz-ID103
  ,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
, BU.BusinessUnitName AS BusinessUnit
, SU.ServiceUnitName AS ServiceUnitName
, SC.ServiceCenterName AS ServiceCenterName
, C.CycleName AS CycleName
  from UDV_CustomerDescription CD  
  left join Tbl_MCustomerTypes CT on CT.CustomerTypeId = CD.CustomerTypeId  
  left join Tbl_MTariffClasses TC on TC.ClassID  = CD.TariffId  
  left join CUSTOMERS.Tbl_CustomerProceduralDetails CPD on CPD.GlobalAccountNumber=CD.GlobalAccountNumber  
  left join Tbl_MReadCodes RC on RC.ReadCodeId = CD.ReadCodeID  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD on PAD.GlobalAccountNumber=CD.GlobalAccountNumber and PAD.IsServiceAddress=1 and PAD.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerPostalAddressDetails PAD1 on PAD1.GlobalAccountNumber=CD.GlobalAccountNumber and PAD1.IsServiceAddress=0  and PAD1.IsActive=1  
  left join CUSTOMERS.Tbl_CustomerActiveDetails CAD on CAD.GlobalAccountNumber=CD.GlobalAccountNumber   
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessDetails AS APD ON APD.GlobalAccountNumber=CD.GlobalAccountNumber  
  LEFT JOIN Tbl_MAccountTypes AS MAT ON CPD.AccountTypeId=MAT.AccountTypeId  
  LEFT JOIN MASTERS.Tbl_MClusterCategories AS MCC ON CD.ClusterCategoryId=MCC.ClusterCategoryId  
  LEFT JOIN Tbl_MPhases AS MPH ON MPH.PhaseId=CD.PhaseId  
  LEFT JOIN Tbl_MRoutes AS MRT ON MRT.RouteId=CD.RouteSequenceNo  
  LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS CTD ON CTD.TenentId=CD.TenentId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP ON CD.EmployeeCode=EMP.BEDCEmpId  
  LEFT JOIN CUSTOMERS.Tbl_ApplicationProcessPersonDetails APPD ON APD.Bedcinfoid=APPD.Bedcinfoid  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP1 ON APPD.InstalledBy=EMP1.BEDCEmpId  
  LEFT JOIN Tbl_Agencies AS AGN ON APPD.AgencyId=AGN.AgencyId  
  LEFT JOIN EMPLOYEE.Tbl_MBEDCEmployeeDetails AS EMP2 ON APD.CertifiedBy=EMP2.BEDCEmpId  
  JOIN Tbl_MCustomerStatus CS on CD.ActiveStatusId=CS.StatusId --Faiz-ID103  
  LEFT JOIN Tbl_PaidMeterDetails PMD ON PMD.AccountNo = CD.GlobalAccountNumber AND PMD.MeterNo = CD.MeterNumber--Faiz-ID103
  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
  Where CD.GlobalAccountNumber=@GlobalAccountNumber OR CD.OldAccountNo=@GlobalAccountNumber  
  FOR XML PATH('CustomerRegistrationBE'),TYPE  
END  
GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN     
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	,@IsFinalApproval BIT = 0
	,@FunctionId INT
	,@ApprovalStatusId INT 
	,@BU_ID VARCHAR(50) 
	,@CurrentApprovalLevel INT
	,@RoleId INT
	,@CurrentLevel INT
	,@PresentRoleId INT
	,@NextRoleId INT
	,@NameChangeRequestId INT
	,@ModifiedBy varchar(50)
	,@RouteSequenceNumber_Value varchar(50)
	,@PhaseId_Value varchar(50)
	,@ReadCodeID_Value varchar(50)
	,@ApplicationProcessedBy_Value VARCHAR(50)
	,@EmployeeCode_Value Varchar(50)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT') 
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
    ,@FunctionId=C.value('(FunctionId)[1]','VARCHAR(50)')  
    ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT') 
    ,@IsFinalApproval =C.value('(IsFinalApproval)[1]','BIT')    
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	SET @RouteSequenceNumber_Value=(SELECT RouteName FROM Tbl_MRoutes WHERE RouteId=@RouteSequenceNumber)
	SET @PhaseId_Value=(SELECT Phase FROM Tbl_MPhases WHERE PhaseId=@PhaseId)
	SET @ReadCodeID_Value=(SELECT ReadCode FROM Tbl_MReadCodes WHERE ReadCodeId=@ReadCodeID)
	SET @ApplicationProcessedBy_Value =@ApplicationProcessedBy
	SET @EmployeeCode_Value=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId =@EmployeeCode)
	
	SET @StatusText='Approval Registration Starts'
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level		
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,	PresentApprovalRole
		,	NextApprovalRole
		,	ApproveStatusId
		,	CurrentApprovalLevel
		,   RouteSequenceNumber_value
		,	PhaseId_Value
		,	ReadCodeID_Value
		,	ApplicationProcessedBy_Value
		,	EmployeeCode
		,	EmployeeCode_Value
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
			, @RouteSequenceNumber_value
			, @PhaseId_Value
			, @ReadCodeID_Value
			, @ApplicationProcessedBy_Value
			, @EmployeeCode
			, @EmployeeCode_Value
		)
			
			IF(@IsFinalApproval = 1)
				BEGIN
				DECLAre @xml xml=null;
				EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @XmlDoc
				SET @GlobalAccountNumber=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
				UPDATE CUSTOMERS.Tbl_ApprovalRegistration SET GlobalAccountNumber=@GlobalAccountNumber 
				WHERE ARID =(SELECT TOP 1 ARID FROM CUSTOMERS.Tbl_ApprovalRegistration ORDER BY ARID DESC ) 
				END
			ELSE
			BEGIN
				SELECT 1 AS IsSuccessful  
				 ,@StatusText AS StatusText   
				 FOR XML PATH('CustomerRegistrationBE'),TYPE  
			END
END
GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT	  
			,@PresentRoleId INT = NULL
			,@NextRoleId INT = NULL
			,@CurrentLevel INT		
			,@IsFinalApproval BIT = 0
			,@AdjustmentLogId INT
			,@ApprovalStatusId INT
			,@FunctionId INT
			,@ModifiedBy VARCHAR(50)
			,@Details VARCHAR(200)
			,@BU_ID VARCHAR(50)
			,@GlobalAccountNumber VARCHAR(50)
			,@IsSuccess BIT =0
	SELECT   
		 @ARID=C.value('(ARID)[1]','INT')
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C) 
     DECLARE @Xml XML 
     IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
											WHERE ARID = @ARID)
					 --(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
						--			RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerNameChangeLogs 
						--					WHERE NameChangeLogId = @NameChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval
						
							
							SET @Xml=(SELECT 
										Title_Tenant AS TitleTanent
										,FirstName_Tenant AS FirstNameTanent
										,MiddleName_Tenant AS MiddleNameTanent
										,LastName_Tenant AS LastNameTanent
										,PhoneNumber AS PhoneNumberTanent
										,AlternatePhoneNumber AS AlternatePhoneNumberTanent
										,EmailID_Tenant AS EmailIdTanent
										,Title AS TitleLandlord
										,FirstName AS FirstNameLandlord
										,MiddleName AS MiddleNameLandlord
										,LastName AS LastNameLandlord
										,KnownAs AS KnownAs
										,HomeContactNumber AS HomeContactNumberLandlord
										,BusinessContactNumber AS BusinessPhoneNumberLandlord
										,OtherContactNumber AS OtherPhoneNumberLandlord
										,EmailId AS EmailIdLandlord
										,IsSameAsService AS IsSameAsService
										,IsSameAsTenent AS IsSameAsTenent
										,DocumentNo AS DocumentNo
										,ApplicationDate AS ApplicationDate
										,ConnectionDate AS ConnectionDate
										,SetupDate AS SetupDate
										,IsBEDCEmployee AS IsBEDCEmployee
										,IsVIPCustomer AS IsVIPCustomer
										,OldAccountNo AS OldAccountNo
										,CustomerTypeId AS CustomerTypeId
										,BookNo AS BookNo
										,PoleID AS PoleID
										,MeterNumber AS MeterNumber
										,TariffClassID AS TariffClassID
										,IsEmbassyCustomer AS IsEmbassyCustomer
										,EmbassyCode AS EmbassyCode
										,PhaseId AS PhaseId
										,ReadCodeID AS ReadCodeID
										,IdentityTypeId AS IdentityTypeId
										,IdentityNumber AS IdentityNumber
										,CertifiedBy AS CertifiedBy
										,Seal1 AS Seal1
										,Seal2 AS Seal2
										,ApplicationProcessedBy AS ApplicationProcessedBy
										,InstalledBy AS InstalledBy
										,AgencyId AS AgencyId
										,IsCAPMI AS IsCAPMI
										,MeterAmount AS MeterAmount
										,InitialBillingKWh AS InitialBillingKWh
										,InitialReading AS InitialReading
										,PresentReading AS PresentReading
										,HouseNo_Postal AS HouseNoPostal
										,StreetName_Postal AS StreetPostal
										,City_Postal AS CityPostaL
										,AreaCode_Postal AS AreaPostal
										,HouseNo_Service AS HouseNoService
										,StreetName_Service AS StreetService
										,City_Service AS CityService
										,AreaCode_Service AS AreaService
										,DocumentName AS DocumentName
										,[Path] AS [Path]
										,EmployeeCode AS EmployeeCode
										,ZipCode_Postal AS PZipCode
										,ZipCode_Service AS SZipCode
										,AccountTypeId AS AccountTypeId
										,ClusterCategoryId AS ClusterCategoryId
										,RouteSequenceNumber AS RouteSequenceNumber
										,MGActTypeID AS MGActTypeID
										,IsCommunication_Postal AS IsCommunicationPostal
										,IsCommunication_Service AS IsCommunicationService
										,IdentityTypeIdList AS IdentityTypeIdList
										,IdentityNumberList AS IdentityNumberList
										,UDFTypeIdList AS UDFTypeIdList
										,UDFValueList AS UDFValueList
										,CreatedBy AS CreatedBy
										,CreatedDate AS CreatedDate
										 FROM CUSTOMERS.Tbl_ApprovalRegistration
										 WHERE ARID = @ARID
										FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
							EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
					
							SET @GlobalAccountNumber=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
						
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
								,GlobalAccountNumber =@GlobalAccountNumber
							WHERE ARID = @ARID  
						
						--Audit table
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
								,GlobalAccountNumber=@GlobalAccountNumber
							WHERE  ARID = @ARID
							
						END
				SET @IsSuccess=1
				END
		ELSE
			BEGIN -- No Approval Levels are there					
						
					SET @Xml=(SELECT 
									Title_Tenant AS TitleTanent
									,FirstName_Tenant AS FirstNameTanent
									,MiddleName_Tenant AS MiddleNameTanent
									,LastName_Tenant AS LastNameTanent
									,PhoneNumber AS PhoneNumberTanent
									,AlternatePhoneNumber AS AlternatePhoneNumberTanent
									,EmailID_Tenant AS EmailIdTanent
									,Title AS TitleLandlord
									,FirstName AS FirstNameLandlord
									,MiddleName AS MiddleNameLandlord
									,LastName AS LastNameLandlord
									,KnownAs AS KnownAs
									,HomeContactNumber AS HomeContactNumberLandlord
									,BusinessContactNumber AS BusinessPhoneNumberLandlord
									,OtherContactNumber AS OtherPhoneNumberLandlord
									,EmailId AS EmailIdLandlord
									,IsSameAsService AS IsSameAsService
									,IsSameAsTenent AS IsSameAsTenent
									,DocumentNo AS DocumentNo
									,ApplicationDate AS ApplicationDate
									,ConnectionDate AS ConnectionDate
									,SetupDate AS SetupDate
									,IsBEDCEmployee AS IsBEDCEmployee
									,IsVIPCustomer AS IsVIPCustomer
									,OldAccountNo AS OldAccountNo
									,CustomerTypeId AS CustomerTypeId
									,BookNo AS BookNo
									,PoleID AS PoleID
									,MeterNumber AS MeterNumber
									,TariffClassID AS TariffClassID
									,IsEmbassyCustomer AS IsEmbassyCustomer
									,EmbassyCode AS EmbassyCode
									,PhaseId AS PhaseId
									,ReadCodeID AS ReadCodeID
									,IdentityTypeId AS IdentityTypeId
									,IdentityNumber AS IdentityNumber
									,CertifiedBy AS CertifiedBy
									,Seal1 AS Seal1
									,Seal2 AS Seal2
									,ApplicationProcessedBy AS ApplicationProcessedBy
									,InstalledBy AS InstalledBy
									,AgencyId AS AgencyId
									,IsCAPMI AS IsCAPMI
									,MeterAmount AS MeterAmount
									,InitialBillingKWh AS InitialBillingKWh
									,InitialReading AS InitialReading
									,PresentReading AS PresentReading
									,HouseNo_Postal AS HouseNoPostal
									,StreetName_Postal AS StreetPostal
									,City_Postal AS CityPostaL
									,AreaCode_Postal AS AreaPostal
									,HouseNo_Service AS HouseNoService
									,StreetName_Service AS StreetService
									,City_Service AS CityService
									,AreaCode_Service AS AreaService
									,DocumentName AS DocumentName
									,[Path] AS [Path]
									,EmployeeCode AS EmployeeCode
									,ZipCode_Postal AS PZipCode
									,ZipCode_Service AS SZipCode
									,AccountTypeId AS AccountTypeId
									,ClusterCategoryId AS ClusterCategoryId
									,RouteSequenceNumber AS RouteSequenceNumber
									,MGActTypeID AS MGActTypeID
									,IsCommunication_Postal AS IsCommunicationPostal
									,IsCommunication_Service AS IsCommunicationService
									,IdentityTypeIdList AS IdentityTypeIdList
									,IdentityNumberList AS IdentityNumberList
									,UDFTypeIdList AS UDFTypeIdList
									,UDFValueList AS UDFValueList
									,CreatedBy AS CreatedBy
									,CreatedDate AS CreatedDate
									 FROM CUSTOMERS.Tbl_ApprovalRegistration
									 WHERE ARID = @ARID
									FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
						EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml						
					
					SET @GlobalAccountNumber=(SELECT TOP 1 CustomerId FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
					UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
						,GlobalAccountNumber=@GlobalAccountNumber
					WHERE  ARID = @ARID
					--Audit Ray
					SET @IsSuccess=1
				END					
		END
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
																WHERE ARID = @ARID)
							
							
							--(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
							--									WHERE NameChangeLogId = @NameChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM CUSTOMERS.Tbl_ApprovalRegistration 
						WHERE ARID = @ARID
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE  ARID = @ARID	
			SET @IsSuccess=1	
		END  	
		SELECT @IsSuccess As IsSuccess, @GlobalAccountNumber as GlobalAccountNumber
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
END
--------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=5)
	BEGIN
		SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')		
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=15)
	BEGIN
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=16)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrationsByARID]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrationsByARID]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  --,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		--, BN.BookCode
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,(CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 AND CD.ARID=@ARID 
	 ORDER BY CD.CreatedDate desc 
	 FOR XML PATH('CustomerRegistrationBE'),TYPE  
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrations]    Script Date: 08/01/2015 13:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrations]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber  
	  ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  --,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS PhaseId   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		--, BN.BookCode
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,(CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId AND CD.ApproveStatusId IN (1,4)
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 ORDER BY CD.CreatedDate desc  
END
-------------------------------------------------------------------------

GO
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerPDFBillDetails]    Script Date: 08/01/2015 19:00:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 13-FEB-2014
-- Description: Insert Customer PDF Bill Details
-- =============================================
CREATE PROCEDURE [USP_InsertCustomerPDFBillDetails]
	(
	@XmlDoc XML=null
	)
AS
BEGIN
	DECLARE
	     @FilePath VARCHAR(MAX) 
	    ,@BillNo VARCHAR(50)
	    ,@BillMonthId INT
	    ,@BillYearId INT
	    ,@CustomerBillId INT
		,@CreatedBy VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)
				
	SELECT 
			@GlobalAccountNo=C.value('(GlobalAccountNo)[1]','VARCHAR(50)')
			,@FilePath=C.value('(FilePath)[1]','VARCHAR(50)')
			,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
			,@BillMonthId=C.value('(BillMonth)[1]','INT')
			,@BillYearId=C.value('(BillYear)[1]','INT')
			,@CustomerBillId=C.value('(CustomerBillId)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('CustomerBillPDFDetailsBE') as T(C)	      
	
	
	
		INSERT INTO Tbl_CustomerBillPDFFiles
							(
								 FilePath
								,BillNo
								,BillMonthId
								,BillYearId
								,CustomerBillId
								,CreatedBy
								,CreatedDate
								,GlobalAccountNo
							)
				VALUES		(
								 @FilePath
								,@BillNo
								,@BillMonthId
								,@BillYearId
								,@CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								,@GlobalAccountNo
							)	
	
			SELECT 1 As IsSuccess
			FOR XML PATH('CustomerBillPDFDetailsBE'),TYPE
END


--====================================================================================================
GO

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
CREATE PROCEDURE [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]
(
	@XmlDoc xml
)
AS
BEGIN 

--------------------------------------------
--              Declaration
--------------------------------------------

	DECLARE 
	@BU_ID VARCHAR(MAX) --='BEDC_BU_0012' 
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  = 15
   ,@ApprovalRoleId INT 
   ,@ARID INT 
   ,@SU_ID VARCHAR(MAX)--='BEDC_SU_0036'  
   ,@SC_ID VARCHAR(MAX)--='BEDC_SC_0043,BEDC_SC_0044'  
   ,@CycleId VARCHAR(MAX)--='BEDC_C_0176,BEDC_C_0179,BEDC_C_0180,BEDC_C_0177,BEDC_C_0178,BEDC_C_0192'  
   ,@TariffId VARCHAR(MAX)--='2,3,4,5,7,8,9,11,12,13,15,16,19,21,22,39,44,45,47,49,50,51,54,55'  
   ,@BookNo VARCHAR(MAX)--='BEDC_B_1009,BEDC_B_1010,BEDC_B_1011,BEDC_B_1012,BEDC_B_1013,BEDC_B_1014,BEDC_B_1015,BEDC_B_1016,BEDC_B_1017,BEDC_B_1018'  
   ,@FromDate VARCHAR(20)--='07/04/2015'  
   ,@ToDate VARCHAR(20)--='08/01/2015' 
   ,@PageNo INT--=1
   ,@PageSize INT--=5 
   ,@Count INT
        
--------------------------------------------
--              Deserialization
--------------------------------------------
        
	 SELECT       
	 
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
	    
	  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
	 
	 
--------------------------------------------
--              Initialization on variables
-------------------------------------------- 
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 

--------------------------------------------
--              Requirement
-------------------------------------------- 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
   ;WITH PageResult AS
   (		
		SELECT    
		ARID AS ARID
	   ,ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber 
	   ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.PoleID    
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal        
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading 
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Remarks WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Remarks END AS Remarks  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS PhaseId  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  ,CD.OutStandingAmount_ActiveDetails
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
		,BN.BookNo AS BookNumber
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,CONVERT(VARCHAR(19),CD.CreatedDate) AS CreatedDate
	  ,BN.SortOrder AS BookSortOrder
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 --WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
			AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
			AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
			AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
			AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,','))
	 )
 	 SELECT * FROM PageResult
	 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
		ORDER BY ARID DESC 
		
	SET @Count =(select COUNT(0) from CUSTOMERS.Tbl_ApprovalRegistration as cd
				INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
				INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
				INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
				INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
				INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
				AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
				AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
				AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
				AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
				AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
				AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
				AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')))
	 SELECT @Count AS [Count]
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrationsByARID]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrationsByARID]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  --,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		--, BN.BookCode
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
						WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
	AND CD.ARID=@ARID 
	 ORDER BY CD.CreatedDate desc 
	 FOR XML PATH('CustomerRegistrationBE'),TYPE  
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrations]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrations]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber  
	  ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  --,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS PhaseId   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		--, BN.BookCode
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,(CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId AND CD.ApproveStatusId IN (1,4)
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 ORDER BY CD.CreatedDate desc  
END
-------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=15)
	BEGIN
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=16)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END
-------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT	  
			,@PresentRoleId INT = NULL
			,@NextRoleId INT = NULL
			,@CurrentLevel INT		
			,@IsFinalApproval BIT = 0
			,@AdjustmentLogId INT
			,@ApprovalStatusId INT
			,@FunctionId INT
			,@ModifiedBy VARCHAR(50)
			,@Details VARCHAR(200)
			,@BU_ID VARCHAR(50)
			,@GlobalAccountNumber VARCHAR(50)
			,@IsSuccess BIT =0
	SELECT   
		 @ARID=C.value('(ARID)[1]','INT')
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C) 
     DECLARE @Xml XML 
     IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
											WHERE ARID = @ARID)
					 --(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
						--			RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerNameChangeLogs 
						--					WHERE NameChangeLogId = @NameChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval
						
							
							SET @Xml=(SELECT 
										Title_Tenant AS TitleTanent
										,FirstName_Tenant AS FirstNameTanent
										,MiddleName_Tenant AS MiddleNameTanent
										,LastName_Tenant AS LastNameTanent
										,PhoneNumber AS PhoneNumberTanent
										,AlternatePhoneNumber AS AlternatePhoneNumberTanent
										,EmailID_Tenant AS EmailIdTanent
										,Title AS TitleLandlord
										,FirstName AS FirstNameLandlord
										,MiddleName AS MiddleNameLandlord
										,LastName AS LastNameLandlord
										,KnownAs AS KnownAs
										,HomeContactNumber AS HomeContactNumberLandlord
										,BusinessContactNumber AS BusinessPhoneNumberLandlord
										,OtherContactNumber AS OtherPhoneNumberLandlord
										,EmailId AS EmailIdLandlord
										,IsSameAsService AS IsSameAsService
										,IsSameAsTenent AS IsSameAsTenent
										,DocumentNo AS DocumentNo
										,ApplicationDate AS ApplicationDate
										,ConnectionDate AS ConnectionDate
										,SetupDate AS SetupDate
										,IsBEDCEmployee AS IsBEDCEmployee
										,IsVIPCustomer AS IsVIPCustomer
										,OldAccountNo AS OldAccountNo
										,CustomerTypeId AS CustomerTypeId
										,BookNo AS BookNo
										,PoleID AS PoleID
										,MeterNumber AS MeterNumber
										,TariffClassID AS TariffClassID
										,IsEmbassyCustomer AS IsEmbassyCustomer
										,EmbassyCode AS EmbassyCode
										,PhaseId AS PhaseId
										,ReadCodeID AS ReadCodeID
										,IdentityTypeId AS IdentityTypeId
										,IdentityNumber AS IdentityNumber
										,CertifiedBy AS CertifiedBy
										,Seal1 AS Seal1
										,Seal2 AS Seal2
										,ApplicationProcessedBy AS ApplicationProcessedBy
										,InstalledBy AS InstalledBy
										,AgencyId AS AgencyId
										,IsCAPMI AS IsCAPMI
										,MeterAmount AS MeterAmount
										,InitialBillingKWh AS InitialBillingKWh
										,InitialReading AS InitialReading
										,PresentReading AS PresentReading
										,HouseNo_Postal AS HouseNoPostal
										,StreetName_Postal AS StreetPostal
										,City_Postal AS CityPostaL
										,AreaCode_Postal AS AreaPostal
										,HouseNo_Service AS HouseNoService
										,StreetName_Service AS StreetService
										,City_Service AS CityService
										,AreaCode_Service AS AreaService
										,DocumentName AS DocumentName
										,[Path] AS [Path]
										,EmployeeCode AS EmployeeCode
										,ZipCode_Postal AS PZipCode
										,ZipCode_Service AS SZipCode
										,AccountTypeId AS AccountTypeId
										,ClusterCategoryId AS ClusterCategoryId
										,RouteSequenceNumber AS RouteSequenceNumber
										,MGActTypeID AS MGActTypeID
										,IsCommunication_Postal AS IsCommunicationPostal
										,IsCommunication_Service AS IsCommunicationService
										,IdentityTypeIdList AS IdentityTypeIdList
										,IdentityNumberList AS IdentityNumberList
										,UDFTypeIdList AS UDFTypeIdList
										,UDFValueList AS UDFValueList
										,CreatedBy AS CreatedBy
										,CreatedDate AS CreatedDate
										 FROM CUSTOMERS.Tbl_ApprovalRegistration
										 WHERE ARID = @ARID
										FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
							EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
					
							SET @GlobalAccountNumber=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
						
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
								,GlobalAccountNumber =@GlobalAccountNumber
							WHERE ARID = @ARID  
						
						--Audit table
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
								,GlobalAccountNumber=@GlobalAccountNumber
							WHERE  ARID = @ARID
							
						END
				SET @IsSuccess=1
				END
		ELSE
			BEGIN -- No Approval Levels are there					
						
					SET @Xml=(SELECT 
									Title_Tenant AS TitleTanent
									,FirstName_Tenant AS FirstNameTanent
									,MiddleName_Tenant AS MiddleNameTanent
									,LastName_Tenant AS LastNameTanent
									,PhoneNumber AS PhoneNumberTanent
									,AlternatePhoneNumber AS AlternatePhoneNumberTanent
									,EmailID_Tenant AS EmailIdTanent
									,Title AS TitleLandlord
									,FirstName AS FirstNameLandlord
									,MiddleName AS MiddleNameLandlord
									,LastName AS LastNameLandlord
									,KnownAs AS KnownAs
									,HomeContactNumber AS HomeContactNumberLandlord
									,BusinessContactNumber AS BusinessPhoneNumberLandlord
									,OtherContactNumber AS OtherPhoneNumberLandlord
									,EmailId AS EmailIdLandlord
									,IsSameAsService AS IsSameAsService
									,IsSameAsTenent AS IsSameAsTenent
									,DocumentNo AS DocumentNo
									,ApplicationDate AS ApplicationDate
									,ConnectionDate AS ConnectionDate
									,SetupDate AS SetupDate
									,IsBEDCEmployee AS IsBEDCEmployee
									,IsVIPCustomer AS IsVIPCustomer
									,OldAccountNo AS OldAccountNo
									,CustomerTypeId AS CustomerTypeId
									,BookNo AS BookNo
									,PoleID AS PoleID
									,MeterNumber AS MeterNumber
									,TariffClassID AS TariffClassID
									,IsEmbassyCustomer AS IsEmbassyCustomer
									,EmbassyCode AS EmbassyCode
									,PhaseId AS PhaseId
									,ReadCodeID AS ReadCodeID
									,IdentityTypeId AS IdentityTypeId
									,IdentityNumber AS IdentityNumber
									,CertifiedBy AS CertifiedBy
									,Seal1 AS Seal1
									,Seal2 AS Seal2
									,ApplicationProcessedBy AS ApplicationProcessedBy
									,InstalledBy AS InstalledBy
									,AgencyId AS AgencyId
									,IsCAPMI AS IsCAPMI
									,MeterAmount AS MeterAmount
									,InitialBillingKWh AS InitialBillingKWh
									,InitialReading AS InitialReading
									,PresentReading AS PresentReading
									,HouseNo_Postal AS HouseNoPostal
									,StreetName_Postal AS StreetPostal
									,City_Postal AS CityPostaL
									,AreaCode_Postal AS AreaPostal
									,HouseNo_Service AS HouseNoService
									,StreetName_Service AS StreetService
									,City_Service AS CityService
									,AreaCode_Service AS AreaService
									,DocumentName AS DocumentName
									,[Path] AS [Path]
									,EmployeeCode AS EmployeeCode
									,ZipCode_Postal AS PZipCode
									,ZipCode_Service AS SZipCode
									,AccountTypeId AS AccountTypeId
									,ClusterCategoryId AS ClusterCategoryId
									,RouteSequenceNumber AS RouteSequenceNumber
									,MGActTypeID AS MGActTypeID
									,IsCommunication_Postal AS IsCommunicationPostal
									,IsCommunication_Service AS IsCommunicationService
									,IdentityTypeIdList AS IdentityTypeIdList
									,IdentityNumberList AS IdentityNumberList
									,UDFTypeIdList AS UDFTypeIdList
									,UDFValueList AS UDFValueList
									,CreatedBy AS CreatedBy
									,CreatedDate AS CreatedDate
									 FROM CUSTOMERS.Tbl_ApprovalRegistration
									 WHERE ARID = @ARID
									FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
						EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml						
					
					SET @GlobalAccountNumber=(SELECT TOP 1 CustomerId FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
					UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
						,GlobalAccountNumber=@GlobalAccountNumber
					WHERE  ARID = @ARID
					--Audit Ray
					SET @IsSuccess=1
				END					
		END
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
																WHERE ARID = @ARID)
							
							
							--(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
							--									WHERE NameChangeLogId = @NameChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM CUSTOMERS.Tbl_ApprovalRegistration 
						WHERE ARID = @ARID
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE  ARID = @ARID	
			SET @IsSuccess=1	
		END  	
		SELECT @IsSuccess As IsSuccess, @GlobalAccountNumber as GlobalAccountNumber
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
END
--------------------------------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN     
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	,@IsFinalApproval BIT = 0
	,@FunctionId INT
	,@ApprovalStatusId INT 
	,@BU_ID VARCHAR(50) 
	,@CurrentApprovalLevel INT
	,@RoleId INT
	,@CurrentLevel INT
	,@PresentRoleId INT
	,@NextRoleId INT
	,@NameChangeRequestId INT
	,@ModifiedBy varchar(50)
	,@RouteSequenceNumber_Value varchar(50)
	,@PhaseId_Value varchar(50)
	,@ReadCodeID_Value varchar(50)
	,@ApplicationProcessedBy_Value VARCHAR(50)
	,@EmployeeCode_Value Varchar(50)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT') 
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
    ,@FunctionId=C.value('(FunctionId)[1]','VARCHAR(50)')  
    ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT') 
    ,@IsFinalApproval =C.value('(IsFinalApproval)[1]','BIT')    
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	SET @RouteSequenceNumber_Value=(SELECT RouteName FROM Tbl_MRoutes WHERE RouteId=@RouteSequenceNumber)
	SET @PhaseId_Value=(SELECT Phase FROM Tbl_MPhases WHERE PhaseId=@PhaseId)
	SET @ReadCodeID_Value=(SELECT ReadCode FROM Tbl_MReadCodes WHERE ReadCodeId=@ReadCodeID)
	SET @ApplicationProcessedBy_Value =@ApplicationProcessedBy
	SET @EmployeeCode_Value=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId =@EmployeeCode)
	
	SET @StatusText='Approval Registration Starts'
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level		
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,	PresentApprovalRole
		,	NextApprovalRole
		,	ApproveStatusId
		,	CurrentApprovalLevel
		,   RouteSequenceNumber_value
		,	PhaseId_Value
		,	ReadCodeID_Value
		,	ApplicationProcessedBy_Value
		,	EmployeeCode
		,	EmployeeCode_Value
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
			, @RouteSequenceNumber_value
			, @PhaseId_Value
			, @ReadCodeID_Value
			, @ApplicationProcessedBy_Value
			, @EmployeeCode
			, @EmployeeCode_Value
		)
			
			IF(@IsFinalApproval = 1)
				BEGIN
				DECLAre @xml xml=null;
				EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @XmlDoc
				SET @GlobalAccountNumber=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
				UPDATE CUSTOMERS.Tbl_ApprovalRegistration SET GlobalAccountNumber=@GlobalAccountNumber 
				WHERE ARID =(SELECT TOP 1 ARID FROM CUSTOMERS.Tbl_ApprovalRegistration ORDER BY ARID DESC ) 
				END
			ELSE
			BEGIN
				SELECT 1 AS IsSuccessful  
				 ,@StatusText AS StatusText   
				 FOR XML PATH('CustomerRegistrationBE'),TYPE  
			END
END
------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterInformation]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  V.Bhimaraju    
-- Create date: 08-04-2014    
-- Description: The purpose of this procedure is to get list of MeterInformation    
-- Author:  V.Bhimaraju    
-- Modified date: 04-02-2015    
-- Description: Added GlobalAccount No in this proc    
-- Author:  Faiz-ID103  
-- Modified date: 20-APR-2015  
-- Description: Modified the SP for active meter Type filter  
-- Modified By :kalyan
-- Modified Date :01-08-2015
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetMeterInformation]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT    
   ,@BU_ID VARCHAR(20) 
   ,@StartIndex int   
   ,@EndIndex int      
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')   
   ,@StartIndex= C.value('(startIndex)[1]','INT')    
   ,@EndIndex= C.value('(endIndex)[1]','INT')   
    
   ,@BU_ID= C.value('(BU_ID)[1]','VARCHAR(20)')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY M.ActiveStatusId ASC , M.CreatedDate DESC ) AS RowNumber    
    ,MeterId    
    ,MeterNo    
    ,M.MeterType AS MeterTypeId    
    --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType    
    ,MT.MeterType AS MeterType --Faiz-ID103  
    ,MeterSize    
    ,ISNULL(MeterDetails,'---') AS Details    
    ,M.ActiveStatusId    
    ,M.CreatedBy    
    ,MeterMultiplier    
    ,MeterBrand    
    ,MeterSerialNo    
    ,MeterRating    
    ,CONVERT(VARCHAR(50),NextCalibrationDate,103) AS BillDate    
    ,ISNULL((CONVERT(VARCHAR(50),NextCalibrationDate,106)),'--') AS NextCalibrationDate    
    ,ISNULL(ServiceHoursBeforeCalibration,'--') AS ServiceHoursBeforeCalibration    
    ,MeterDials    
    ,ISNULL(Decimals,0) AS Decimals    
    ,M.BU_ID    
    ,B.BusinessUnitName    
    ,M.IsCAPMIMeter  
    ,M.CAPMIAmount  
    ,ISNULL(CPD.GlobalAccountNumber,'--') AS AccountNo    
    ,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs AM WHERE AM.MeterNo = M.MeterNo AND ApprovalStatusId IN(1,4)) > 0) THEN 1  
   WHEN ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs AM WHERE AM.NewMeterNo = M.MeterNo AND ApproveStatusId IN(1,4)) > 0) THEN 1  
   ELSE 0 END) AS ActiveStatus  
    FROM Tbl_MeterInformation AS M    
    JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID     
    LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.MeterNumber=M.MeterNo    
    JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 --Faiz-ID103  
    WHERE M.ActiveStatusId IN(1,2)    
    AND M.BU_ID=@BU_ID OR @BU_ID=''    
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE (RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize) and (RowNumber>=@StartIndex  and RowNumber<=@EndIndex)
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END    
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerPDFBillDetails]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 13-FEB-2014
-- Description: Insert Customer PDF Bill Details
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerPDFBillDetails]
	(
	@XmlDoc XML=null
	)
AS
BEGIN
	DECLARE
	     @FilePath VARCHAR(MAX) 
	    ,@BillNo VARCHAR(50)
	    ,@BillMonthId INT
	    ,@BillYearId INT
	    ,@CustomerBillId INT
		,@CreatedBy VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)
				
	SELECT 
			@GlobalAccountNo=C.value('(GlobalAccountNo)[1]','VARCHAR(50)')
			,@FilePath=C.value('(FilePath)[1]','VARCHAR(50)')
			,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
			,@BillMonthId=C.value('(BillMonth)[1]','INT')
			,@BillYearId=C.value('(BillYear)[1]','INT')
			,@CustomerBillId=C.value('(CustomerBillId)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('CustomerBillPDFDetailsBE') as T(C)	      
	
	
	
		INSERT INTO Tbl_CustomerBillPDFFiles
							(
								 FilePath
								,BillNo
								,BillMonthId
								,BillYearId
								,CustomerBillId
								,CreatedBy
								,CreatedDate
								,GlobalAccountNo
							)
				VALUES		(
								 @FilePath
								,@BillNo
								,@BillMonthId
								,@BillYearId
								,@CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								,@GlobalAccountNo
							)	
	
			SELECT 1 As IsSuccess
			FOR XML PATH('CustomerBillPDFDetailsBE'),TYPE
END


--====================================================================================================

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
	@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
	,@Month INT          
	,@Year INT           
	,@Date DATETIME           
	,@MonthStartDate DATE       
	,@PreviousReading VARCHAR(50)          
	,@CurrentReading VARCHAR(50)          
	,@Usage DECIMAL(20)          
	,@RemaningBalanceUsage INT          
	,@TotalAmount DECIMAL(18,2)= 0          
	,@TaxValue DECIMAL(18,2)          
	,@BillingQueuescheduleId INT          
	,@PresentCharge INT          
	,@FromUnit INT          
	,@ToUnit INT          
	,@Amount DECIMAL(18,2)      
	,@TaxId INT          
	,@CustomerBillId INT = 0          
	,@BillGeneratedBY VARCHAR(50)          
	,@LastDateOfBill DATETIME          
	,@IsEstimatedRead BIT = 0          
	,@ReadCodeId INT          
	,@BillType INT          
	,@LastBillGenerated DATETIME        
	,@FeederId VARCHAR(50)        
	,@CycleId VARCHAR(MAX)     -- CycleId   
	,@BillNo VARCHAR(50)      
	,@PrevCustomerBillId INT      
	,@AdjustmentAmount DECIMAL(18,2)      
	,@PreviousDueAmount DECIMAL(18,2)      
	,@CustomerTariffId VARCHAR(50)      
	,@BalanceUnits INT      
	,@RemainingBalanceUnits INT      
	,@IsHaveLatest BIT = 0      
	,@ActiveStatusId INT      
	,@IsDisabled BIT      
	,@BookDisableType INT      
	,@IsPartialBill BIT      
	,@OutStandingAmount DECIMAL(18,2)      
	,@IsActive BIT      
	,@NoFixed BIT = 0      
	,@NoBill BIT = 0       
	,@ClusterCategoryId INT=NULL    
	,@StatusText VARCHAR(50)      
	,@BU_ID VARCHAR(50)    
	,@BillingQueueScheduleIdList VARCHAR(MAX)    
	,@RowsEffected INT  
	,@IsFromReading BIT  
	,@TariffId INT  
	,@EnergyCharges DECIMAL(18,2)   
	,@FixedCharges  DECIMAL(18,2)  
	,@CustomerTypeID INT  
	,@ReadType Int  
	,@TaxPercentage DECIMAL(18,2)=5    
	,@TotalBillAmountWithTax  DECIMAL(18,2)  
	,@AverageUsageForNewBill  DECIMAL(18,2)  
	,@IsEmbassyCustomer INT  
	,@TotalBillAmountWithArrears  DECIMAL(18,2)   
	,@CusotmerNewBillID INT  
	,@InititalkWh INT  
	,@NetArrears DECIMAL(18,2)  
	,@BookNo VARCHAR(30)  
	,@GetPaidMeterBalance DECIMAL(18,2)  
	,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
	,@MeterNumber VARCHAR(50)  
	,@ActualUsage DECIMAL(18,2)  
	,@RegenCustomerBillId INT  
	,@PaidAmount  DECIMAL(18,2)  
	,@PrevBillTotalPaidAmount Decimal(18,2)  
	,@PreviousBalance Decimal(18,2)  
	,@OpeningBalance Decimal(18,2)  
	,@CustomerFullName VARCHAR(MAX)  
	,@BusinessUnitName VARCHAR(100)  
	,@TariffName VARCHAR(50)  
	,@ReadDate DateTime  
	,@Multiplier INT  
	,@Service_HouseNo VARCHAR(100)  
	,@Service_Street VARCHAR(100)  
	,@Service_City VARCHAR(100)  
	,@ServiceZipCode VARCHAR(100)  
	,@Postal_HouseNo VARCHAR(100)  
	,@Postal_Street VARCHAR(100)  
	,@Postal_City VARCHAR(100)  
	,@Postal_ZipCode VARCHAR(100)  
	,@Postal_LandMark VARCHAR(100)  
	,@Service_LandMark VARCHAR(100)  
	,@OldAccountNumber VARCHAR(100)  
	,@ServiceUnitId VARCHAR(50)  
	,@PoleId Varchar(50)  
	,@ServiceCenterId VARCHAR(50)  
	,@IspartialClose INT  
	,@TariffCharges varchar(max)  
	,@CusotmerPreviousBillNo varchar(50)  
	,@DisableDate DATETIME 
	,@BillingComments Varchar(max) 
	,@TotalBillAmount decimal(18,2)
	,@TotalPaidAmount DECIMAL(18,2)
	,@PaidMeterDeductedAmount DECIMAL(18,2)
	,@AdjustmentAmountEffected DECIMAL(18,2)
	,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
	,@ConnectionDate DATETIME
	,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
	SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
		,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
		,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) 
	FROM Tbl_CustomerBills CB 
	JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
		AND BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	FROM Tbl_CustomerBills AS CB
	JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
		AND CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
	JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
       
	IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
		BEGIN     
			SELECT   
				 @ActiveStatusId = ActiveStatusId 
				,@OutStandingAmount = ISNULL(OutStandingAmount,0)  
				,@TariffId=TariffId
				,@CustomerTypeID=CustomerTypeID
				,@ClusterCategoryId=ClusterCategoryId 
				,@IsEmbassyCustomer=IsEmbassyCustomer
				,@InititalkWh=InitialBillingKWh  
				,@ReadCodeId=ReadCodeID
				,@BookNo=BookNo
				,@MeterNumber=MeterNumber  
				,@OpeningBalance=isnull(OpeningBalance,0)  
				,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
				,@BusinessUnitName =BusinessUnitName  
				,@TariffName =ClassName  
				,@Service_HouseNo =Service_HouseNo  
				,@Service_Street =Service_StreetName  
				,@Service_City =Service_City  
				,@ServiceZipCode  =Service_ZipCode  
				,@Postal_HouseNo =Postal_HouseNo  
				,@Postal_Street =Postal_StreetName  
				,@Postal_City  =Postal_City  
				,@Postal_ZipCode  =Postal_ZipCode  
				,@Postal_LandMark =Postal_LandMark  
				,@Service_LandMark =Service_LandMark  
				,@OldAccountNumber=OldAccountNo  
				,@BU_ID=BU_ID  
				,@ServiceUnitId=SU_ID  
				,@PoleId=PoleID  
				,@TariffId=ClassID  
				,@ServiceCenterId=ServiceCenterId  
				,@CycleId=CycleId
				,@SetupDate=SetupDate
				,@ConnectionDate=ConnectionDate  
			FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   

			----------------------------------------COPY START --------------------------------------------------------   
			--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------

			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
				BEGIN
					SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
						,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
					FROM Tbl_CustomerBills(NOLOCK)  
					WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
					
					DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId

					DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
					
					SELECT @PaidAmount=SUM(PaidAmount)
					FROM Tbl_CustomerBillPayments(NOLOCK)  
					WHERE BillNo=@CusotmerPreviousBillNo

					IF @PaidAmount IS NOT NULL
						BEGIN
							SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
							
							UPDATE Tbl_CustomerBillPayments
							SET BillNo=NULL
							WHERE BillNo=@RegenCustomerBillId
						END

					----------------------Update Readings as is billed =0 ----------------------------
					UPDATE Tbl_CustomerReadings 
					SET IsBilled =0 
						,BillNo=NULL
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
					-- Insert Paid Meter Payments

					Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
					
					Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) 
					from Tbl_PaidMeterPaymentDetails 
					WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 

					update	Tbl_PaidMeterDetails
					SET OutStandingAmount=isnull(OutStandingAmount,0) +	ISNULL(@PreviousPaidMeterDeductedAmount,0)
					WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1

					DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

					SET @PreviousPaidMeterDeductedAmount=NULL

					update CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=@OutStandingAmount
					where GlobalAccountNumber=@GlobalAccountNumber
					----------------------------------------------------------------------------------

					--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
					DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
					-------------------------------------------------------------------------------------
				END

			select  @IspartialClose=IsPartialBill,
				@BookDisableType=DisableTypeId,
				@DisableDate=DisableDate
			from Tbl_BillingDisabledBooks	  
			where BookNo=@BookNo and IsActive=1

			IF	isnull(@ActiveStatusId,0) =3 or	isnull(@ActiveStatusId,0) =4 or isnull(@CustomerTypeID,0) = 3  
				BEGIN
					Return;
				END    

			IF isnull(@BookDisableType,0)=1	 
				IF isnull(@IspartialClose,0)<>1
					Return;
					
			DECLARE @IsInActiveCustomer BIT = 0
			
			IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
				BEGIN
					SET @IsInActiveCustomer = 1
				END

			IF @ReadCodeId=2 
				BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
								@LastBillGenerated=LastBillGenerated,
								@PreviousBalance =Previousbalance,
								@BalanceUnits=BalanceUsage,
								@IsFromReading=IsFromReading,
								@PrevCustomerBillId=CustomerBillId,
								@PreviousBalance=PreviousBalance,
								@Multiplier=Multiplier,
								@ReadDate=ReadDate
							FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

					IF @IsFromReading=1
						BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
								BEGIN
									SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
														+convert(varchar(100),@BalanceUnits)+' , @Usage :'
														+convert(varchar(100),@Usage)
									SET @ActualUsage=@Usage
									SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
									SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
									SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								END
							ELSE
								BEGIN
									SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
									SET @RemaningBalanceUsage=0
									SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
														+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
														+ convert(varchar(100),@RemaningBalanceUsage)														
								END
						END
					ELSE
						BEGIN
							SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
							SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
						END
				END
			ELSE  
				BEGIN
					SET @IsEstimatedRead =1

					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
							,@PrevCustomerBillId=CustomerBillId, 
					@PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 

					IF @PreviousBalance IS NULL
						BEGIN
							SET @PreviousBalance=@OpeningBalance
						END
					
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
							IF(@IsInActiveCustomer = 1)
								BEGIN
									SET @Usage = 0
								END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
									SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
					SET @ActualUsage=@Usage
				END

			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0

			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

			DECLARE @tblFixedCharges AS TABLE(ClassID int,Amount Decimal(18,2))
			------------------------------------------------------------------------------------ 
			
			IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
				BEGIN
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
						BEGIN
							SET	@EnergyCharges=0
						END	
							
					SET @IsFirstmonthBill= (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ClassID ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
					SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
					
				END
			------------------------------------------------------------------------------------------------------------------------------------
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
				BEGIN
					IF @GetPaidMeterBalance<=@FixedCharges
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=0
							SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
						END
					ELSE
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							SET @PaidMeterDeductedAmount=@FixedCharges
							SET @FixedCharges=0
						END
				END
			------------------------
			-- Caluclate tax here

			SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											* ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 

			IF	@PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				END

			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0

			SET @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)

			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)

			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
			FROM Tbl_CustomerBills (NOLOCK)      
			WHERE AccountNo = @GlobalAccountNumber
			Group BY CustomerBillId       
			ORDER BY CustomerBillId DESC 

			IF @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage

			IF @RemainingBalanceUnits IS NULL
				SET @RemainingBalanceUnits=0

			-------------------------------------------------------------------------------------------------------
			SET @TariffCharges = (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))
									FROM Tbl_LEnergyClassCharges 
									WHERE ClassID =@TariffId
									AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
									FOR XML PATH(''), TYPE)
									.value('.','NVARCHAR(MAX)'),1,0,'')  ) 

			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------

			SELECT @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			
			-----------------------------------------------------------------------------------------------------------------------

			----------------------------------------------------------COPY END-------------------------------------------------------------------  
			--- Need to verify all fields before insert  
			INSERT INTO Tbl_CustomerBills  
			(  
				 [AccountNo]   --@GlobalAccountNumber       
				,[TotalBillAmount] --@TotalBillAmountWithTax        
				,[ServiceAddress] --@EnergyCharges   
				,[MeterNo]   -- @MeterNumber      
				,[Dials]     --     
				,[NetArrears] --   @NetArrears      
				,[NetEnergyCharges] --  @EnergyCharges       
				,[NetFixedCharges]   --@FixedCharges       
				,[VAT]  --     @TaxValue   
				,[VATPercentage]  --  @TaxPercentage      
				,[Messages]  --        
				,[BU_ID]  --        
				,[SU_ID]  --      
				,[ServiceCenterId]    
				,[PoleId] --         
				,[BillGeneratedBy] --         
				,[BillGeneratedDate]          
				,PaymentLastDate        --  
				,[TariffId]  -- @TariffId       
				,[BillYear]    --@Year      
				,[BillMonth]   --@Month       
				,[CycleId]   -- @CycleId  
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
				,[ActiveStatusId]--          
				,[CreatedDate]--GETDATE()          
				,[CreatedBy]          
				,[ModifedBy]          
				,[ModifiedDate]          
				,[BillNo]  --        
				,PaymentStatusID          
				,[PreviousReading]  --@PreviousReading        
				,[PresentReading]   --  @CurrentReading     
				,[Usage]     --@Usage     
				,[AverageReading] -- @AverageUsageForNewBill        
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
				,[EstimatedUsage] --@Usage         
				,[ReadCodeId]  --  @ReadType      
				,[ReadType]  --  @ReadType  
				,AdjustmentAmmount -- @AdjustmentAmount    
				,BalanceUsage    --@RemaningBalanceUsage  
				,BillingTypeId --   
				,ActualUsage  
				,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
				,PreviousBalance--@PreviousBalance
				,TariffRates  
			)          
			Values
			( 
				 @GlobalAccountNumber  
				,@TotalBillAmount     
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
				,@MeterNumber      
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
				,@NetArrears         
				,@EnergyCharges   
				,@FixedCharges  
				,@TaxValue   
				,@TaxPercentage          
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
				,@BU_ID  
				,@ServiceUnitId  
				,@ServiceCenterId  
				,@PoleId          
				,@BillGeneratedBY          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
				ORDER BY RecievedDate DESC) --@LastDateOfBill          
				,@TariffId  
				,@Year  
				,@Month  
				,@CycleId  
				,@TotalBillAmountWithArrears   
				,1 --ActiveStatusId          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,@BillGeneratedBY          
				,NULL --ModifedBy  
				,NULL --ModifedDate  
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
				,2 -- PaymentStatusID     
				,@PreviousReading          
				,@CurrentReading          
				,@Usage          
				,@AverageUsageForNewBill  
				,@TotalBillAmountWithTax               
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
				,@ReadType  
				,@ReadType         
				,@AdjustmentAmount      
				,@RemaningBalanceUsage     
				,2 -- BillingTypeId   
				,@ActualUsage  
				,@PrevBillTotalPaidAmount  
				,@PreviousBalance
				,@TariffCharges  
			)  
			
			SET @CusotmerNewBillID = SCOPE_IDENTITY()   
			----------------------- Update Customer Outstanding ------------------------------  

			-- Insert Paid Meter Payments  
			IF isnull(@PaidMeterDeductedAmount,0) >0
				BEGIN
					INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
					SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        

					UPDATE Tbl_PaidMeterDetails
					SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
					WHERE AccountNo=@GlobalAccountNumber
				END

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails  
			SET OutStandingAmount=@TotalBillAmountWithArrears  
			WHERE GlobalAccountNumber=@GlobalAccountNumber  
			----------------------------------------------------------------------------------  
			----------------------Update Readings as is billed =1 ----------------------------  
			
			IF(@IsInActiveCustomer = 0)
				BEGIN
					UPDATE Tbl_CustomerReadings 
					SET IsBilled = 1 
						,BillNo=@CusotmerNewBillID
					WHERE GlobalAccountNumber=@GlobalAccountNumber
				END

			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  

			INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
			SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID 
			FROM  @tblFixedCharges  
			
			INSERT INTO Tbl_CustomerBillFixedCharges
			(
				 CustomerBillId 
				,FixedChargeId 
				,Amount 
				,BillMonth 
				,BillYear 
				,CreatedDate 
			)
			SELECT
				 @CusotmerNewBillID
				,ClassID
				,Amount
				,@Month
				,@Year
				,(SELECT dbo.fn_GetCurrentDateTime())  
			FROM @tblFixedCharges  

			DELETE FROM @tblFixedCharges  

			------------------------------------------------------------------------------------  

			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   

			---------------------------------------------------------------------------------------  
			UPDATE Tbl_BillAdjustments 
			SET EffectedBillId=@CusotmerNewBillID  			
			WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      

			--------------Save Bill Deails of customer name,BU-----------------------------------------------------  
			INSERT INTO Tbl_BillDetails  
			(
				CustomerBillID,  
				BusinessUnitName,  
				CustomerFullName,  
				Multiplier,  
				Postal_City,  
				Postal_HouseNo,  
				Postal_Street,  
				Postal_ZipCode,  
				ReadDate,  
				ServiceZipCode,  
				Service_City,  
				Service_HouseNo,  
				Service_Street,  
				TariffName,  
				Postal_LandMark,  
				Service_Landmark,  
				OldAccountNumber
			)  
			VALUES  
			(
				@CusotmerNewBillID,  
				@BusinessUnitName,  
				@CustomerFullName,  
				@Multiplier,  
				@Postal_City,  
				@Postal_HouseNo,  
				@Postal_Street,  
				@Postal_ZipCode,  
				@ReadDate,  
				@ServiceZipCode,  
				@Service_City,  
				@Service_HouseNo,  
				@Service_Street,  
				@TariffName,  
				@Postal_LandMark,  
				@Service_LandMark,  
				@OldAccountNumber
			)  
			
			---------------------------------------------------Set Variables to NULL-  

			SET @TotalAmount = NULL  
			SET @EnergyCharges = NULL  
			SET @FixedCharges = NULL  
			SET @TaxValue = NULL  

			SET @PreviousReading  = NULL        
			SET @CurrentReading   = NULL       
			SET @Usage   = NULL  
			SET @ReadType =NULL  
			SET @BillType   = NULL       
			SET @AdjustmentAmount    = NULL  
			SET @RemainingBalanceUnits   = NULL   
			SET @TotalBillAmountWithArrears=NULL  
			SET @BookNo=NULL  
			SET @AverageUsageForNewBill=NULL   
			SET @IsHaveLatest=0  
			SET @ActualUsage=NULL  
			SET @RegenCustomerBillId =NULL  
			SET @PaidAmount  =NULL  
			SET @PrevBillTotalPaidAmount =NULL  
			SET @PreviousBalance =NULL  
			SET @OpeningBalance =NULL  
			SET @CustomerFullName  =NULL  
			SET @BusinessUnitName  =NULL  
			SET @TariffName  =NULL  
			SET @ReadDate  =NULL  
			SET @Multiplier  =NULL  
			SET @Service_HouseNo  =NULL  
			SET @Service_Street  =NULL  
			SET @Service_City  =NULL  
			SET @ServiceZipCode =NULL  
			SET @Postal_HouseNo  =NULL  
			SET @Postal_Street =NULL  
			SET @Postal_City  =NULL  
			SET @Postal_ZipCode  =NULL  
			SET @Postal_LandMark =NULL  
			SET @Service_LandMark =NULL  
			SET @OldAccountNumber=NULL   
			SET @DisableDate=NULL  
			SET @BillingComments=NULL
			SET @TotalBillAmount=NULL
			SET @PaidMeterDeductedAmount=NULL
			SET @IsEmbassyCustomer=NULL
			SET @TaxPercentage=NULL
			SET @PaidMeterDeductedAmount=NULL
			
			SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
		END  
	ELSE
		BEGIN
			SELECT 0 AS NoRows   
			SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
		END
END  
				 










GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/01/2015 19:02:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50)
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		
	SELECT
		 @AccounNo = U.AccountNo + ' - ' + U.GlobalAccountNumber
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookNo
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.DisplayCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerBillPayments
			WHERE CustomerBillId = @CustomerBillId
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CustomerBillId = @CustomerBillId
	
		END
	
	SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.DisplayCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		,ISNULL((SELECT TOP 1 
				REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
			FROM Tbl_CustomerReadings CR3
			WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
				AND CR3.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR3.CustomerReadingId DESC),ISNULL(@InitialBillingKWh,0))
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,@NetArrears AS NetArrears
			,@Payments AS Payments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,@LastPaidAmount AS LastPaidAmount
			,@LastPaidDate AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
			,@CustomerBillId AS CustomerBillId
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO


