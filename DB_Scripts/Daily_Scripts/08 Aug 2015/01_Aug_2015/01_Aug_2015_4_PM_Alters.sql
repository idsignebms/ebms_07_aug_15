
CREATE TABLE Tbl_CustomerBillPDFFiles
(
	 CustomerBillPDFId INT IDENTITY(1,1) PRIMARY KEY
	,GlobalAccountNo VARCHAR(50)
	,CustomerBillId INT
	,FilePath VARCHAR(MAX)
	,BillNo VARCHAR(200)
	,BillMonthId INT
	,BillYearId INt
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
)