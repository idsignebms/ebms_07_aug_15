CREATE TABLE Tbl_CustomerBillFixedCharges 
(
	 CustomerBillFixedChargeId INT IDENTITY(1,1) PRIMARY KEY
	,CustomerBillId INT
	,FixedChargeId INT
	,Amount DECIMAL(18,2)
	,BillNo VARCHAR(50)
	,BillMonth INT
	,BillYear INT
	,CreatedBy VARCHAR(50)
	,CreatedDate DATETIME
)