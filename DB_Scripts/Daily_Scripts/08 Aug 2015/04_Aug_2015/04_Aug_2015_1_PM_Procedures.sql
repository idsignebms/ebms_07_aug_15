
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]    Script Date: 08/04/2015 13:08:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj Kanojiya
-- Modified Date: 3-Aug-2015
-- =============================================  
CREATE PROCEDURE [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @BillMonthList VARCHAR(MAX)
		,@BillYearList VARCHAR(MAX)
		,@Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	--SELECT * FROM DBO.fn_splitTwo('6|2|2|','2|2|2|','|')
	
	CREATE TABLE #BillMonthList(RowNum INT identity(1,1), BillMonth varchar(10), BillYear varchar(10))
	
	INSERT INTO #BillMonthList(BillMonth,BillYear)
	
	SELECT * FROM DBO.fn_splitTwo(@BillMonthList,@BillYearList,'|')
	
	SELECT * FROM #BillMonthList
	
	
	SELECT  
		 @Month = C.value('(Month)[1]','INT')  
		,@Year = C.value('(Year)[1]','INT')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
		
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
	
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(RowNum) FROM #BillMonthList)  
	WHILE(@index<@rwCount) --loop
	BEGIN
		SET @index=@index+1
		SET @Month=(SELECT BillMonth FROM #BillMonthList WHERE RowNum=@index)
		SET @Year=(SELECT BillYear FROM #BillMonthList WHERE RowNum=@index)
		
		IF(@Month = 1)
			BEGIN
				SET @LastMonth = 12
				SET @LastYear = @Year - 1
			END
		ELSE
			BEGIN
				SET @LastMonth = @Month - 1
				SET @LastYear = @Year
			END
			
		--------------------------------------------------------------------------------------
		--CustomersList table for those customer who belongs to parameterised blll open month
		--------------------------------------------------------------------------------------
		
		SELECT 
			 CB.BU_ID
			,BU.BusinessUnitName
			,CB.SU_ID
			,SU.ServiceUnitName
			,CB.ServiceCenterId
			,SC.ServiceCenterName
			,CB.TariffId
			,TC.ClassName
			,CPD.CustomerTypeId
			,CT.CustomerType
			,CB.BillYear
			,CB.BillMonth
			,CB.AccountNo
			,CPD.ReadCodeID
			,CB.Usage
			,CB.NetFixedCharges
			,CB.NetEnergyCharges
			,CB.VATPercentage
			,CB.NetArrears
			,CB.VAT
			,CB.TotalBillAmount
			,CB.TotalBillAmountWithTax
			,CB.TotalBillAmountWithArrears
			,CB.ReadType
			,CB.PaidAmount
			,CAD.OutStandingAmount
			,CB.TotalBillAmountWithTax AS ClosingBalance
			,CB.BillNo
			,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
		INTO #CustomersList
		FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
		INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
		INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
		INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
		INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
		WHERE BillMonth = @Month AND BillYear = @Year

		--------------------------------------------------------------------------------------
		-- Total bill amount with tax after bill gen of last month.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  AccountNo
			 ,TotalBillAmountWithTax
		INTO #CustomerOpeningBalance
		FROM Tbl_CustomerBills
		WHERE BillMonth = @LastMonth AND BillYear = @LastYear

		--------------------------------------------------------------------------------------
		-- Total no of customer in those tariff and cust type.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  CPD.BookNo
			 ,BN.CycleId
			 ,BG.ServiceCenterId
			 ,SC.SU_ID
			 ,SU.BU_ID
			 ,CPD.TariffClassID
			 ,CPD.CustomerTypeId
			 ,GlobalAccountNumber
		INTO #CustomerPopulation
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
		INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
		INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

		INSERT INTO Tbl_ReportBillingStatisticsBySCId
		(
			 BU_ID
			,BusinessUnitName
			,SU_ID
			,ServiceUnitName
			,SC_ID
			,ServiceCenterName
			,CustomerTypeId
			,CustomerType
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,EnergyDelivered
			,FixedCharges
			,NoOfBilledCustomers
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
						AND CP.ServiceCenterId = CL.ServiceCenterId 
						AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			
		INSERT INTO Tbl_ReportBillingStatisticsByTariffId
		(
			 BU_ID
			,BusinessUnitName
			,TariffId
			,TariffName
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,NoOfBilledCustomers
			,EnergyDelivered
			,FixedCharges
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID  
						AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth

		DROP TABLE #CustomersList
		DROP TABLE #CustomerOpeningBalance
		DROP TABLE #CustomerPopulation	
		DROP TABLE #BillMonthList
	END
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerPDFBillDetails]    Script Date: 08/04/2015 13:08:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 13-FEB-2014
-- Description: Insert Customer PDF Bill Details
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerPDFBillDetails]
	(
	@XmlDoc XML=null
	)
AS
BEGIN
	DECLARE
	     @FilePath VARCHAR(MAX) 
	    ,@BillNo VARCHAR(50)
	    ,@BillMonthId INT
	    ,@BillYearId INT
	    ,@CustomerBillId INT
		,@CreatedBy VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)
		,@EmailId VARCHAR(MAX)
				
	SELECT 
			@GlobalAccountNo=C.value('(GlobalAccountNo)[1]','VARCHAR(50)')
			,@FilePath=C.value('(FilePath)[1]','VARCHAR(MAX)')
			,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
			,@BillMonthId=C.value('(BillMonth)[1]','INT')
			,@BillYearId=C.value('(BillYear)[1]','INT')
			,@CustomerBillId=C.value('(CustomerBillId)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
		    ,@EmailId = C.value('(EmailId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('CustomerBillPDFDetailsBE') as T(C)	      
	
	
	
		INSERT INTO Tbl_CustomerBillPDFFiles
							(
								 FilePath
								,BillNo
								,BillMonthId
								,BillYearId
								,CustomerBillId
								,CreatedBy
								,CreatedDate
								,GlobalAccountNo
								,EmailId
							)
				VALUES		(
								 @FilePath
								,@BillNo
								,@BillMonthId
								,@BillYearId
								,@CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								,@GlobalAccountNo
								,@EmailId
							)	
	
			SELECT 1 As IsSuccess
			FOR XML PATH('CustomerBillPDFDetailsBE'),TYPE
END


--====================================================================================================

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]    Script Date: 08/04/2015 13:08:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]
(
	@XmlDoc xml
)
AS
BEGIN 

--------------------------------------------
--              Declaration
--------------------------------------------

	DECLARE 
	@BU_ID VARCHAR(MAX) --='BEDC_BU_0012' 
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  = 15
   ,@ApprovalRoleId INT 
   ,@ARID INT 
   ,@SU_ID VARCHAR(MAX)--='BEDC_SU_0036'  
   ,@SC_ID VARCHAR(MAX)--='BEDC_SC_0043,BEDC_SC_0044'  
   ,@CycleId VARCHAR(MAX)--='BEDC_C_0176,BEDC_C_0179,BEDC_C_0180,BEDC_C_0177,BEDC_C_0178,BEDC_C_0192'  
   ,@TariffId VARCHAR(MAX)--='2,3,4,5,7,8,9,11,12,13,15,16,19,21,22,39,44,45,47,49,50,51,54,55'  
   ,@BookNo VARCHAR(MAX)--='BEDC_B_1009,BEDC_B_1010,BEDC_B_1011,BEDC_B_1012,BEDC_B_1013,BEDC_B_1014,BEDC_B_1015,BEDC_B_1016,BEDC_B_1017,BEDC_B_1018'  
   ,@FromDate VARCHAR(20)--='07/04/2015'  
   ,@ToDate VARCHAR(20)--='08/01/2015' 
   ,@PageNo INT--=1
   ,@PageSize INT--=5 
   ,@Count INT
        
--------------------------------------------
--              Deserialization
--------------------------------------------
        
	 SELECT       
	 
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
	    
	  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
	 
	 
--------------------------------------------
--              Initialization on variables
-------------------------------------------- 
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 

--------------------------------------------
--              Requirement
-------------------------------------------- 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
   ;WITH PageResult AS
   (		
		SELECT    
		ARID AS ARID
	   ,ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber 
	   ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.PoleID    
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal        
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading 
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Remarks WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Remarks END AS Remarks  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS PhaseId  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  ,CD.OutStandingAmount_ActiveDetails
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
		,BN.BookNo AS BookNumber
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,CONVERT(VARCHAR(19),CD.CreatedDate) AS CreatedDate
	  ,BN.SortOrder AS BookSortOrder
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 --WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
			AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
			AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
			AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
			AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,','))
	 )
 	 SELECT * FROM PageResult
	 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
		ORDER BY ARID DESC 
		
	SET @Count =(select COUNT(0) from CUSTOMERS.Tbl_ApprovalRegistration as cd
				INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
				INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
				INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
				INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
				INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
				AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
				AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
				AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
				AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
				AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
				AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
				AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')))
	 SELECT @Count AS [Count]
END
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50) = 0
		,@OutStanding VARCHAR(50) = 0
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		,@BulkEmails VARCHAR(MAX)
		
	SELECT
		 @AccounNo = U.GlobalAccountNumber
		,@OldAccountNo = U.OldAccountNo
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@BulkEmails=dbo.fn_GetCustomerEmailId(U.GlobalAccountNumber)
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookNo
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.ReadCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		--,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
		,@OutStanding = CONVERT(VARCHAR,(CAST(ISNULL(U.OutStandingAmount,0) AS MONEY)), 1)
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
				,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(TotalBillAmountWithArrears,0) AS MONEY)), 1)
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE CONVERT(DATETIME,RecievedDate) >= CONVERT(DATETIME,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			--SELECT
			--	@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			--FROM Tbl_CustomerBillPayments
			--WHERE CustomerBillId = @CustomerBillId
			
			--SELECT
			--	@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			--FROM Tbl_BillAdjustments
			--WHERE CustomerBillId = @CustomerBillId
	
		END
	
	SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.ReadCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		--,ISNULL((SELECT TOP 1 
		--		REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
		--	FROM Tbl_CustomerReadings CR3
		--	WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
		--		AND CR3.CustomerReadingId < CR.CustomerReadingId
		--	ORDER BY CR3.CustomerReadingId DESC),REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', ''))
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', '')
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@OldAccountNo AS OldAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@BulkEmails AS BulkEmails 
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,ISNULL(@NetArrears,@OutStanding) AS NetArrears
			,@Payments AS Payments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,@LastPaidAmount AS LastPaidAmount
			,@LastPaidDate AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
			,@CustomerBillId AS CustomerBillId
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	,
	(
		SELECT
			 CF.FixedChargeId AS FixedChargeId
			,C.ChargeName AS FixedChargeName
			,CONVERT(VARCHAR,(CAST(ISNULL(CF.Amount,0) AS MONEY)), 1) AS FixedChargeAmount
		FROM Tbl_CustomerBillFixedCharges CF
		INNER JOIN Tbl_MChargeIds C ON CF.FixedChargeId = C.ChargeId AND IsAcitve = 1
			AND CF.CustomerBillId = @CustomerBillId
		FOR XML PATH('CustomerBillAdditionalChargeDetails'),TYPE   
	)
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerPDFBillDetails]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 13-FEB-2014
-- Description: Insert Customer PDF Bill Details
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerPDFBillDetails]
	(
	@XmlDoc XML=null
	)
AS
BEGIN
	DECLARE
	     @FilePath VARCHAR(MAX) 
	    ,@BillNo VARCHAR(50)
	    ,@BillMonthId INT
	    ,@BillYearId INT
	    ,@CustomerBillId INT
		,@CreatedBy VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)
		,@EmailId VARCHAR(MAX)
				
	SELECT 
			@GlobalAccountNo=C.value('(GlobalAccountNo)[1]','VARCHAR(50)')
			,@FilePath=C.value('(FilePath)[1]','VARCHAR(MAX)')
			,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
			,@BillMonthId=C.value('(BillMonth)[1]','INT')
			,@BillYearId=C.value('(BillYear)[1]','INT')
			,@CustomerBillId=C.value('(CustomerBillId)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
		    ,@EmailId = C.value('(EmailId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('CustomerBillPDFDetailsBE') as T(C)	      
	
	
	
		INSERT INTO Tbl_CustomerBillPDFFiles
							(
								 FilePath
								,BillNo
								,BillMonthId
								,BillYearId
								,CustomerBillId
								,CreatedBy
								,CreatedDate
								,GlobalAccountNo
								,EmailId
							)
				VALUES		(
								 @FilePath
								,@BillNo
								,@BillMonthId
								,@BillYearId
								,@CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								,@GlobalAccountNo
								,@EmailId
							)	
	
			SELECT 1 As IsSuccess
			FOR XML PATH('CustomerBillPDFDetailsBE'),TYPE
END


--====================================================================================================

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
	@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
	,@Month INT          
	,@Year INT           
	,@Date DATETIME           
	,@MonthStartDate DATE       
	,@PreviousReading VARCHAR(50)          
	,@CurrentReading VARCHAR(50)          
	,@Usage DECIMAL(20)          
	,@RemaningBalanceUsage INT          
	,@TotalAmount DECIMAL(18,2)= 0          
	,@TaxValue DECIMAL(18,2)          
	,@BillingQueuescheduleId INT          
	,@PresentCharge INT          
	,@FromUnit INT          
	,@ToUnit INT          
	,@Amount DECIMAL(18,2)      
	,@TaxId INT          
	,@CustomerBillId INT = 0          
	,@BillGeneratedBY VARCHAR(50)          
	,@LastDateOfBill DATETIME          
	,@IsEstimatedRead BIT = 0          
	,@ReadCodeId INT          
	,@BillType INT          
	,@LastBillGenerated DATETIME        
	,@FeederId VARCHAR(50)        
	,@CycleId VARCHAR(MAX)     -- CycleId   
	,@BillNo VARCHAR(50)      
	,@PrevCustomerBillId INT      
	,@AdjustmentAmount DECIMAL(18,2)      
	,@PreviousDueAmount DECIMAL(18,2)      
	,@CustomerTariffId VARCHAR(50)      
	,@BalanceUnits INT      
	,@RemainingBalanceUnits INT      
	,@IsHaveLatest BIT = 0      
	,@ActiveStatusId INT      
	,@IsDisabled BIT      
	,@BookDisableType INT      
	,@IsPartialBill BIT      
	,@OutStandingAmount DECIMAL(18,2)      
	,@IsActive BIT      
	,@NoFixed BIT = 0      
	,@NoBill BIT = 0       
	,@ClusterCategoryId INT=NULL    
	,@StatusText VARCHAR(50)      
	,@BU_ID VARCHAR(50)    
	,@BillingQueueScheduleIdList VARCHAR(MAX)    
	,@RowsEffected INT  
	,@IsFromReading BIT  
	,@TariffId INT  
	,@EnergyCharges DECIMAL(18,2)   
	,@FixedCharges  DECIMAL(18,2)  
	,@CustomerTypeID INT  
	,@ReadType Int  
	,@TaxPercentage DECIMAL(18,2)=5    
	,@TotalBillAmountWithTax  DECIMAL(18,2)  
	,@AverageUsageForNewBill  DECIMAL(18,2)  
	,@IsEmbassyCustomer INT  
	,@TotalBillAmountWithArrears  DECIMAL(18,2)   
	,@CusotmerNewBillID INT  
	,@InititalkWh INT  
	,@NetArrears DECIMAL(18,2)  
	,@BookNo VARCHAR(30)  
	,@GetPaidMeterBalance DECIMAL(18,2)  
	,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
	,@MeterNumber VARCHAR(50)  
	,@ActualUsage DECIMAL(18,2)  
	,@RegenCustomerBillId INT  
	,@PaidAmount  DECIMAL(18,2)  
	,@PrevBillTotalPaidAmount Decimal(18,2)  
	,@PreviousBalance Decimal(18,2)  
	,@OpeningBalance Decimal(18,2)  
	,@CustomerFullName VARCHAR(MAX)  
	,@BusinessUnitName VARCHAR(100)  
	,@TariffName VARCHAR(50)  
	,@ReadDate DateTime  
	,@Multiplier INT  
	,@Service_HouseNo VARCHAR(100)  
	,@Service_Street VARCHAR(100)  
	,@Service_City VARCHAR(100)  
	,@ServiceZipCode VARCHAR(100)  
	,@Postal_HouseNo VARCHAR(100)  
	,@Postal_Street VARCHAR(100)  
	,@Postal_City VARCHAR(100)  
	,@Postal_ZipCode VARCHAR(100)  
	,@Postal_LandMark VARCHAR(100)  
	,@Service_LandMark VARCHAR(100)  
	,@OldAccountNumber VARCHAR(100)  
	,@ServiceUnitId VARCHAR(50)  
	,@PoleId Varchar(50)  
	,@ServiceCenterId VARCHAR(50)  
	,@IspartialClose INT  
	,@TariffCharges varchar(max)  
	,@CusotmerPreviousBillNo varchar(50)  
	,@DisableDate DATETIME 
	,@BillingComments Varchar(max) 
	,@TotalBillAmount decimal(18,2)
	,@TotalPaidAmount DECIMAL(18,2)
	,@PaidMeterDeductedAmount DECIMAL(18,2)
	,@AdjustmentAmountEffected DECIMAL(18,2)
	,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
	,@ConnectionDate DATETIME
	,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
	SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
		,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
		,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) 
	FROM Tbl_CustomerBills CB 
	JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
		AND BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	FROM Tbl_CustomerBills AS CB
	JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
		AND CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
	JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
       
	IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
		BEGIN     
			SELECT   
				 @ActiveStatusId = ActiveStatusId 
				,@OutStandingAmount = ISNULL(OutStandingAmount,0)  
				,@TariffId=TariffId
				,@CustomerTypeID=CustomerTypeID
				,@ClusterCategoryId=ClusterCategoryId 
				,@IsEmbassyCustomer=IsEmbassyCustomer
				,@InititalkWh=InitialBillingKWh  
				,@ReadCodeId=ReadCodeID
				,@BookNo=BookNo
				,@MeterNumber=MeterNumber  
				,@OpeningBalance=isnull(OpeningBalance,0)  
				,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
				,@BusinessUnitName =BusinessUnitName  
				,@TariffName =ClassName  
				,@Service_HouseNo =Service_HouseNo  
				,@Service_Street =Service_StreetName  
				,@Service_City =Service_City  
				,@ServiceZipCode  =Service_ZipCode  
				,@Postal_HouseNo =Postal_HouseNo  
				,@Postal_Street =Postal_StreetName  
				,@Postal_City  =Postal_City  
				,@Postal_ZipCode  =Postal_ZipCode  
				,@Postal_LandMark =Postal_LandMark  
				,@Service_LandMark =Service_LandMark  
				,@OldAccountNumber=OldAccountNo  
				,@BU_ID=BU_ID  
				,@ServiceUnitId=SU_ID  
				,@PoleId=PoleID  
				,@TariffId=ClassID  
				,@ServiceCenterId=ServiceCenterId  
				,@CycleId=CycleId
				,@SetupDate=SetupDate
				,@ConnectionDate=ConnectionDate  
			FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   

			----------------------------------------COPY START --------------------------------------------------------   
			--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------

			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
				BEGIN
					SELECT @OutStandingAmount=ISNULL(NetArrears,0)--+ISNULL(AdjustmentAmmount,0)
						,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
					FROM Tbl_CustomerBills(NOLOCK)  
					WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
					
					DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId

					DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
					
					--SELECT @PaidAmount=SUM(PaidAmount)
					--FROM Tbl_CustomerBillPayments(NOLOCK)  
					--WHERE BillNo=@CusotmerPreviousBillNo

					--IF @PaidAmount IS NOT NULL
					--	BEGIN
					--		SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
							
					--		UPDATE Tbl_CustomerBillPayments
					--		SET BillNo=NULL
					--		WHERE BillNo=@RegenCustomerBillId
					--	END

					----------------------Update Readings as is billed =0 ----------------------------
					UPDATE Tbl_CustomerReadings 
					SET IsBilled =0 
						,BillNo=NULL
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
					-- Insert Paid Meter Payments

					Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
					
					Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) 
					from Tbl_PaidMeterPaymentDetails 
					WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 

					update	Tbl_PaidMeterDetails
					SET OutStandingAmount=isnull(OutStandingAmount,0) +	ISNULL(@PreviousPaidMeterDeductedAmount,0)
					WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1

					DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

					SET @PreviousPaidMeterDeductedAmount=NULL

					update CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=@OutStandingAmount
					where GlobalAccountNumber=@GlobalAccountNumber
					----------------------------------------------------------------------------------

					--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
					DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
					-------------------------------------------------------------------------------------
				END

			select  @IspartialClose=IsPartialBill,
				@BookDisableType=DisableTypeId,
				@DisableDate=DisableDate
			from Tbl_BillingDisabledBooks	  
			where BookNo=@BookNo and IsActive=1

			IF	isnull(@ActiveStatusId,0) =3 or	isnull(@ActiveStatusId,0) =4 or isnull(@CustomerTypeID,0) = 3  
				BEGIN
					Return;
				END    

			IF isnull(@BookDisableType,0)=1	 
				IF isnull(@IspartialClose,0)<>1
					Return;
					
			DECLARE @IsInActiveCustomer BIT = 0
			
			IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
				BEGIN
					SET @IsInActiveCustomer = 1
				END

			IF @ReadCodeId=2 
				BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
								@LastBillGenerated=LastBillGenerated,
								@PreviousBalance =Previousbalance,
								@BalanceUnits=BalanceUsage,
								@IsFromReading=IsFromReading,
								@PrevCustomerBillId=CustomerBillId,
								@PreviousBalance=PreviousBalance,
								@Multiplier=Multiplier,
								@ReadDate=ReadDate
							FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

					IF @IsFromReading=1
						BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
								BEGIN
									SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
														+convert(varchar(100),@BalanceUnits)+' , @Usage :'
														+convert(varchar(100),@Usage)
									SET @ActualUsage=@Usage
									SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
									SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
									SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								END
							ELSE
								BEGIN
									SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
									SET @RemaningBalanceUsage=0
									SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
														+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
														+ convert(varchar(100),@RemaningBalanceUsage)														
								END
						END
					ELSE
						BEGIN
							SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
							SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
						END
				END
			ELSE  
				BEGIN
					SET @IsEstimatedRead =1

					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
							,@PrevCustomerBillId=CustomerBillId, 
					@PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 

					IF @PreviousBalance IS NULL
						BEGIN
							SET @PreviousBalance=@OpeningBalance
						END
					
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
							IF(@IsInActiveCustomer = 1)
								BEGIN
									SET @Usage = 0
								END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
									SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
					SET @ActualUsage=@Usage
				END

			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0

			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

			DECLARE @tblFixedCharges AS TABLE(ClassID int,Amount Decimal(18,2))
			------------------------------------------------------------------------------------ 
			
			IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
				BEGIN
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
						BEGIN
							SET	@EnergyCharges=0
						END	
							
					SET @IsFirstmonthBill= (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ClassID ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
					SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
					
				END
			------------------------------------------------------------------------------------------------------------------------------------
			SELECT @GetPaidMeterBalance=dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber)
			
			IF @GetPaidMeterBalance>0
				BEGIN
					IF @GetPaidMeterBalance<=@FixedCharges
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=0
							SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
						END
					ELSE
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							SET @PaidMeterDeductedAmount=@FixedCharges
							SET @FixedCharges=0
						END
				END
			------------------------
			-- Caluclate tax here

			SET @TaxValue = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 
											* ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
			SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE @TaxPercentage END) 

			IF	@PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				END

			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0

			SET @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)

			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)

			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
			FROM Tbl_CustomerBills (NOLOCK)      
			WHERE AccountNo = @GlobalAccountNumber
			Group BY CustomerBillId       
			ORDER BY CustomerBillId DESC 

			IF @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage

			IF @RemainingBalanceUnits IS NULL
				SET @RemainingBalanceUnits=0

			-------------------------------------------------------------------------------------------------------
			SET @TariffCharges = (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))
									FROM Tbl_LEnergyClassCharges 
									WHERE ClassID =@TariffId
									AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
									FOR XML PATH(''), TYPE)
									.value('.','NVARCHAR(MAX)'),1,0,'')  ) 

			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------

			SELECT @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			
			-----------------------------------------------------------------------------------------------------------------------

			----------------------------------------------------------COPY END-------------------------------------------------------------------  
			--- Need to verify all fields before insert  
			INSERT INTO Tbl_CustomerBills  
			(  
				 [AccountNo]   --@GlobalAccountNumber       
				,[TotalBillAmount] --@TotalBillAmountWithTax        
				,[ServiceAddress] --@EnergyCharges   
				,[MeterNo]   -- @MeterNumber      
				,[Dials]     --     
				,[NetArrears] --   @NetArrears      
				,[NetEnergyCharges] --  @EnergyCharges       
				,[NetFixedCharges]   --@FixedCharges       
				,[VAT]  --     @TaxValue   
				,[VATPercentage]  --  @TaxPercentage      
				,[Messages]  --        
				,[BU_ID]  --        
				,[SU_ID]  --      
				,[ServiceCenterId]    
				,[PoleId] --         
				,[BillGeneratedBy] --         
				,[BillGeneratedDate]          
				,PaymentLastDate        --  
				,[TariffId]  -- @TariffId       
				,[BillYear]    --@Year      
				,[BillMonth]   --@Month       
				,[CycleId]   -- @CycleId  
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
				,[ActiveStatusId]--          
				,[CreatedDate]--GETDATE()          
				,[CreatedBy]          
				,[ModifedBy]          
				,[ModifiedDate]          
				,[BillNo]  --        
				,PaymentStatusID          
				,[PreviousReading]  --@PreviousReading        
				,[PresentReading]   --  @CurrentReading     
				,[Usage]     --@Usage     
				,[AverageReading] -- @AverageUsageForNewBill        
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
				,[EstimatedUsage] --@Usage         
				,[ReadCodeId]  --  @ReadType      
				,[ReadType]  --  @ReadType  
				,AdjustmentAmmount -- @AdjustmentAmount    
				,BalanceUsage    --@RemaningBalanceUsage  
				,BillingTypeId --   
				,ActualUsage  
				,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
				,PreviousBalance--@PreviousBalance
				,TariffRates  
			)          
			Values
			( 
				 @GlobalAccountNumber  
				,@TotalBillAmount     
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
				,@MeterNumber      
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
				,@NetArrears         
				,@EnergyCharges   
				,@FixedCharges  
				,@TaxValue   
				,@TaxPercentage          
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
				,@BU_ID  
				,@ServiceUnitId  
				,@ServiceCenterId  
				,@PoleId          
				,@BillGeneratedBY          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
				ORDER BY RecievedDate DESC) --@LastDateOfBill          
				,@TariffId  
				,@Year  
				,@Month  
				,@CycleId  
				,@TotalBillAmountWithArrears   
				,1 --ActiveStatusId          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,@BillGeneratedBY          
				,NULL --ModifedBy  
				,NULL --ModifedDate  
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
				,2 -- PaymentStatusID     
				,@PreviousReading          
				,@CurrentReading          
				,@Usage          
				,@AverageUsageForNewBill  
				,@TotalBillAmountWithTax               
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
				,@ReadType  
				,@ReadType         
				,@AdjustmentAmount      
				,@RemaningBalanceUsage     
				,2 -- BillingTypeId   
				,@ActualUsage  
				,@PrevBillTotalPaidAmount  
				,@PreviousBalance
				,@TariffCharges  
			)  
			
			SET @CusotmerNewBillID = SCOPE_IDENTITY()   
			----------------------- Update Customer Outstanding ------------------------------  

			-- Insert Paid Meter Payments  
			IF isnull(@PaidMeterDeductedAmount,0) >0
				BEGIN
					INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
					SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        

					UPDATE Tbl_PaidMeterDetails
					SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
					WHERE AccountNo=@GlobalAccountNumber
				END

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails  
			SET OutStandingAmount=@TotalBillAmountWithArrears  
			WHERE GlobalAccountNumber=@GlobalAccountNumber  
			----------------------------------------------------------------------------------  
			----------------------Update Readings as is billed =1 ----------------------------  
			
			IF(@IsInActiveCustomer = 0)
				BEGIN
					UPDATE Tbl_CustomerReadings 
					SET IsBilled = 1 
						,BillNo=@CusotmerNewBillID
					WHERE GlobalAccountNumber=@GlobalAccountNumber
				END

			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  

			INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
			SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ClassID 
			FROM  @tblFixedCharges  
			
			INSERT INTO Tbl_CustomerBillFixedCharges
			(
				 CustomerBillId 
				,FixedChargeId 
				,Amount 
				,BillMonth 
				,BillYear 
				,CreatedDate 
			)
			SELECT
				 @CusotmerNewBillID
				,ClassID
				,Amount
				,@Month
				,@Year
				,(SELECT dbo.fn_GetCurrentDateTime())  
			FROM @tblFixedCharges  

			DELETE FROM @tblFixedCharges  

			------------------------------------------------------------------------------------  

			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   

			---------------------------------------------------------------------------------------  
			UPDATE Tbl_BillAdjustments 
			SET EffectedBillId=@CusotmerNewBillID  			
			WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      

			--------------Save Bill Deails of customer name,BU-----------------------------------------------------  
			INSERT INTO Tbl_BillDetails  
			(
				CustomerBillID,  
				BusinessUnitName,  
				CustomerFullName,  
				Multiplier,  
				Postal_City,  
				Postal_HouseNo,  
				Postal_Street,  
				Postal_ZipCode,  
				ReadDate,  
				ServiceZipCode,  
				Service_City,  
				Service_HouseNo,  
				Service_Street,  
				TariffName,  
				Postal_LandMark,  
				Service_Landmark,  
				OldAccountNumber
			)  
			VALUES  
			(
				@CusotmerNewBillID,  
				@BusinessUnitName,  
				@CustomerFullName,  
				@Multiplier,  
				@Postal_City,  
				@Postal_HouseNo,  
				@Postal_Street,  
				@Postal_ZipCode,  
				@ReadDate,  
				@ServiceZipCode,  
				@Service_City,  
				@Service_HouseNo,  
				@Service_Street,  
				@TariffName,  
				@Postal_LandMark,  
				@Service_LandMark,  
				@OldAccountNumber
			)  
			
			---------------------------------------------------Set Variables to NULL-  

			SET @TotalAmount = NULL  
			SET @EnergyCharges = NULL  
			SET @FixedCharges = NULL  
			SET @TaxValue = NULL  

			SET @PreviousReading  = NULL        
			SET @CurrentReading   = NULL       
			SET @Usage   = NULL  
			SET @ReadType =NULL  
			SET @BillType   = NULL       
			SET @AdjustmentAmount    = NULL  
			SET @RemainingBalanceUnits   = NULL   
			SET @TotalBillAmountWithArrears=NULL  
			SET @BookNo=NULL  
			SET @AverageUsageForNewBill=NULL   
			SET @IsHaveLatest=0  
			SET @ActualUsage=NULL  
			SET @RegenCustomerBillId =NULL  
			SET @PaidAmount  =NULL  
			SET @PrevBillTotalPaidAmount =NULL  
			SET @PreviousBalance =NULL  
			SET @OpeningBalance =NULL  
			SET @CustomerFullName  =NULL  
			SET @BusinessUnitName  =NULL  
			SET @TariffName  =NULL  
			SET @ReadDate  =NULL  
			SET @Multiplier  =NULL  
			SET @Service_HouseNo  =NULL  
			SET @Service_Street  =NULL  
			SET @Service_City  =NULL  
			SET @ServiceZipCode =NULL  
			SET @Postal_HouseNo  =NULL  
			SET @Postal_Street =NULL  
			SET @Postal_City  =NULL  
			SET @Postal_ZipCode  =NULL  
			SET @Postal_LandMark =NULL  
			SET @Service_LandMark =NULL  
			SET @OldAccountNumber=NULL   
			SET @DisableDate=NULL  
			SET @BillingComments=NULL
			SET @TotalBillAmount=NULL
			SET @PaidMeterDeductedAmount=NULL
			SET @IsEmbassyCustomer=NULL
			SET @TaxPercentage=NULL
			SET @PaidMeterDeductedAmount=NULL
			
			SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
		END  
	ELSE
		BEGIN
			SELECT 0 AS NoRows   
			SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
		END
END  
				 










GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj Kanojiya
-- Modified Date: 3-Aug-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @BillMonthList VARCHAR(MAX)
		,@BillYearList VARCHAR(MAX)
		,@Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	--SELECT * FROM DBO.fn_splitTwo('6|2|2|','2|2|2|','|')
	
	CREATE TABLE #BillMonthList(RowNum INT identity(1,1), BillMonth varchar(10), BillYear varchar(10))
	
	INSERT INTO #BillMonthList(BillMonth,BillYear)
	
	SELECT * FROM DBO.fn_splitTwo(@BillMonthList,@BillYearList,'|')
	
	SELECT * FROM #BillMonthList
	
	
	SELECT  
		 @Month = C.value('(Month)[1]','INT')  
		,@Year = C.value('(Year)[1]','INT')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
		
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
	
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(RowNum) FROM #BillMonthList)  
	WHILE(@index<@rwCount) --loop
	BEGIN
		SET @index=@index+1
		SET @Month=(SELECT BillMonth FROM #BillMonthList WHERE RowNum=@index)
		SET @Year=(SELECT BillYear FROM #BillMonthList WHERE RowNum=@index)
		
		IF(@Month = 1)
			BEGIN
				SET @LastMonth = 12
				SET @LastYear = @Year - 1
			END
		ELSE
			BEGIN
				SET @LastMonth = @Month - 1
				SET @LastYear = @Year
			END
			
		--------------------------------------------------------------------------------------
		--CustomersList table for those customer who belongs to parameterised blll open month
		--------------------------------------------------------------------------------------
		
		SELECT 
			 CB.BU_ID
			,BU.BusinessUnitName
			,CB.SU_ID
			,SU.ServiceUnitName
			,CB.ServiceCenterId
			,SC.ServiceCenterName
			,CB.TariffId
			,TC.ClassName
			,CPD.CustomerTypeId
			,CT.CustomerType
			,CB.BillYear
			,CB.BillMonth
			,CB.AccountNo
			,CPD.ReadCodeID
			,CB.Usage
			,CB.NetFixedCharges
			,CB.NetEnergyCharges
			,CB.VATPercentage
			,CB.NetArrears
			,CB.VAT
			,CB.TotalBillAmount
			,CB.TotalBillAmountWithTax
			,CB.TotalBillAmountWithArrears
			,CB.ReadType
			,CB.PaidAmount
			,CAD.OutStandingAmount
			,CB.TotalBillAmountWithTax AS ClosingBalance
			,CB.BillNo
			,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
		INTO #CustomersList
		FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
		INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
		INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
		INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
		INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
		WHERE BillMonth = @Month AND BillYear = @Year

		--------------------------------------------------------------------------------------
		-- Total bill amount with tax after bill gen of last month.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  AccountNo
			 ,TotalBillAmountWithTax
		INTO #CustomerOpeningBalance
		FROM Tbl_CustomerBills
		WHERE BillMonth = @LastMonth AND BillYear = @LastYear

		--------------------------------------------------------------------------------------
		-- Total no of customer in those tariff and cust type.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  CPD.BookNo
			 ,BN.CycleId
			 ,BG.ServiceCenterId
			 ,SC.SU_ID
			 ,SU.BU_ID
			 ,CPD.TariffClassID
			 ,CPD.CustomerTypeId
			 ,GlobalAccountNumber
		INTO #CustomerPopulation
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
		INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
		INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

		INSERT INTO Tbl_ReportBillingStatisticsBySCId
		(
			 BU_ID
			,BusinessUnitName
			,SU_ID
			,ServiceUnitName
			,SC_ID
			,ServiceCenterName
			,CustomerTypeId
			,CustomerType
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,EnergyDelivered
			,FixedCharges
			,NoOfBilledCustomers
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
						AND CP.ServiceCenterId = CL.ServiceCenterId 
						AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			
		INSERT INTO Tbl_ReportBillingStatisticsByTariffId
		(
			 BU_ID
			,BusinessUnitName
			,TariffId
			,TariffName
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,NoOfBilledCustomers
			,EnergyDelivered
			,FixedCharges
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID  
						AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth

		DROP TABLE #CustomersList
		DROP TABLE #CustomerOpeningBalance
		DROP TABLE #CustomerPopulation	
		DROP TABLE #BillMonthList
	END
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=5)
	BEGIN
		SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')		
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=15)
	BEGIN
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=16)
	BEGIN
		
			IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
END


GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=15)
	BEGIN
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	--ELSE IF(@FunctionId=16)
	--BEGIN
	--	IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog WHERE ApprovalStatusId IN(1,4))=0)
	--		SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
	--	ELSE
	--		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	--END
	ELSE IF(@FunctionId=16)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END
-------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPresentReadingChangeLogsToApprove]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 30-07-2015
-- Description: The purpose of this procedure is to get list of Present Meter Reading Requested persons for approval
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerPresentReadingChangeLogsToApprove]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX)
			,@RoleId INT
			,@UserIds VARCHAR(500)
			,@UserId VARCHAR(50) 
				,@FunctionId INT
			,@ApprovalRoleId INT
      
	SELECT     
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')
		,@UserId = C.value('(UserId)[1]','VARCHAR(50)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C)   
  
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	
		IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
				SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
				SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
  
	IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	
	SELECT
		  ROW_NUMBER() OVER (ORDER BY PRA.PresentReadingAdjustmentLogId ASC) AS RowNumber --Faiz-ID103
		 ,PRA.PresentReadingAdjustmentLogId
		 ,(CD.AccountNo+' - '+  PRA.GlobalAccountNumber) AS GlobalAccountNumber
		 ,CD.OldAccountNo AS OldAccountNumber 
		 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) As Name
		 ,PRA.PreviousReading
		 ,PRA.PresentReading
		 ,PRA.NewPresentReading
		 ,PRA.MeterNumber
		 ,CONVERT(VARCHAR(20),PRA.PreviousReadDate,107)AS PreviousReadDate
		 ,CONVERT(VARCHAR(20),PRA.PresentReadDate,107)AS PresentReadDate
		 ,ISNULL(PRA.Remarks,'--') As Details
		 ,(CASE WHEN PRA.NextApprovalRole IS NULL 
				THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
				ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
				END) AS [Status]
		,(CASE WHEN (PRA.PresentApprovalRole IS NULL OR (PRA.PresentApprovalRole = @RoleId 
		AND PRA.CurrentAppovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
		AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
				THEN 1
				ELSE 0
				END) AS IsPerformAction
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,PRA.PresentApprovalRole
		,PRA.NextApprovalRole
	FROM Tbl_PresentReadingAdjustmentApprovalLog PRA 
--	INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = PRA.GlobalAccountNumber 
--	INNER JOIN Tbl_MApprovalStatus AS APS ON PRA.ApprovalStatusId = APS.ApprovalStatusId AND PRA.ApprovalStatusId IN(1,4)
--	LEFT JOIN Tbl_MRoles AS NR ON PRA.NextApprovalRole = NR.RoleId
--	LEFT JOIN Tbl_MRoles AS CR ON PRA.PresentApprovalRole = CR.RoleId
--WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)
INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber = PRA.GlobalAccountNumber 
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber AND PRA.ApprovalStatusId IN(1,4)
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber 
	INNER JOIN UDV_BookNumberDetails BN ON BN.BookNo = CPD.BookNo AND BN.BU_ID IN(SELECT [com] FROM dbo.fn_Split(@BU_ID ,','))
	INNER JOIN Tbl_MApprovalStatus AS APS ON PRA.ApprovalStatusId = APS.ApprovalStatusId
	LEFT JOIN Tbl_MRoles AS NR ON PRA.NextApprovalRole = NR.RoleId
	LEFT JOIN Tbl_MRoles AS CR ON PRA.PresentApprovalRole = CR.RoleId
WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)

END
--------------------------------------------------------------------------------------------------
GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrationsByARID]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrationsByARID]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID ASC) AS RowNumber  
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  --,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		--, BN.BookCode
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 WHERE 
	 --@RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		--		AND 
	CD.ARID=@ARID 
	 ORDER BY CD.CreatedDate desc 
	 FOR XML PATH('CustomerRegistrationBE'),TYPE  
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetApprovalRegistrations]    Script Date: 08/04/2015 13:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetApprovalRegistrations]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @BU_ID VARCHAR(MAX)  
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  
      ,@ApprovalRoleId INT 
      ,@ARID INT 
        
	 SELECT       
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	 FROM @XmlDoc.nodes('ChangeCustomerTypeBE') AS T(C) 
	 
 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
		SELECT    
	   ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber  
	  ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  --,CD.BookNo AS BookCode
	  --,(CD.BookId+' ( '+BookCode+' )') AS BookCode
	  ,CD.PoleID   
	  ,CD.MeterNumber   
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS PhaseId   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal      
	  --,PAD.AddressID AS ServiceAddressID    
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading --Faiz-ID103  
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS Phase  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.EmployeeCode WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  --,CS.StatusName AS [Status]--Faiz-ID103
	  ,CD.OutStandingAmount_ActiveDetails
	  --,CD.AccountNo --Faiz-ID103
	  ,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId
				 AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
				 AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
						THEN 1
						ELSE 0
						END) AS IsPerformAction 
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		--, BN.BookCode
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
	   ,(CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END) AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,ARID
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId AND CD.ApproveStatusId IN (1,4)
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID)  
	 ORDER BY CD.CreatedDate desc  
END
-------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistrationInsert]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistrationInsert] 
(
 @XmlDoc xml 
)     
AS    
BEGIN
	DECLARE @ARID INT	  
			,@PresentRoleId INT = NULL
			,@NextRoleId INT = NULL
			,@CurrentLevel INT		
			,@IsFinalApproval BIT = 0
			,@AdjustmentLogId INT
			,@ApprovalStatusId INT
			,@FunctionId INT
			,@ModifiedBy VARCHAR(50)
			,@Details VARCHAR(200)
			,@BU_ID VARCHAR(50)
			,@GlobalAccountNumber VARCHAR(50)
			,@IsSuccess BIT =0
	SELECT   
		 @ARID=C.value('(ARID)[1]','INT')
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C) 
     DECLARE @Xml XML 
     IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND IsActive = 1) -- If Approval Levels Exists
				BEGIN
					SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
											WHERE ARID = @ARID)
					 --(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
						--			RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerNameChangeLogs 
						--					WHERE NameChangeLogId = @NameChangeLogId))

					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
								FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
					DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END
					IF(@FinalApproval =1)
						BEGIN -- If Approval levels are there and If it is final approval
						
							
							SET @Xml=(SELECT 
										Title_Tenant AS TitleTanent
										,FirstName_Tenant AS FirstNameTanent
										,MiddleName_Tenant AS MiddleNameTanent
										,LastName_Tenant AS LastNameTanent
										,PhoneNumber AS PhoneNumberTanent
										,AlternatePhoneNumber AS AlternatePhoneNumberTanent
										,EmailID_Tenant AS EmailIdTanent
										,Title AS TitleLandlord
										,FirstName AS FirstNameLandlord
										,MiddleName AS MiddleNameLandlord
										,LastName AS LastNameLandlord
										,KnownAs AS KnownAs
										,HomeContactNumber AS HomeContactNumberLandlord
										,BusinessContactNumber AS BusinessPhoneNumberLandlord
										,OtherContactNumber AS OtherPhoneNumberLandlord
										,EmailId AS EmailIdLandlord
										,IsSameAsService AS IsSameAsService
										,IsSameAsTenent AS IsSameAsTenent
										,DocumentNo AS DocumentNo
										,ApplicationDate AS ApplicationDate
										,ConnectionDate AS ConnectionDate
										,SetupDate AS SetupDate
										,IsBEDCEmployee AS IsBEDCEmployee
										,IsVIPCustomer AS IsVIPCustomer
										,OldAccountNo AS OldAccountNo
										,CustomerTypeId AS CustomerTypeId
										,BookNo AS BookNo
										,PoleID AS PoleID
										,MeterNumber AS MeterNumber
										,TariffClassID AS TariffClassID
										,IsEmbassyCustomer AS IsEmbassyCustomer
										,EmbassyCode AS EmbassyCode
										,PhaseId AS PhaseId
										,ReadCodeID AS ReadCodeID
										,IdentityTypeId AS IdentityTypeId
										,IdentityNumber AS IdentityNumber
										,CertifiedBy AS CertifiedBy
										,Seal1 AS Seal1
										,Seal2 AS Seal2
										,ApplicationProcessedBy AS ApplicationProcessedBy
										,InstalledBy AS InstalledBy
										,AgencyId AS AgencyId
										,IsCAPMI AS IsCAPMI
										,MeterAmount AS MeterAmount
										,InitialBillingKWh AS InitialBillingKWh
										,InitialReading AS InitialReading
										,PresentReading AS PresentReading
										,HouseNo_Postal AS HouseNoPostal
										,StreetName_Postal AS StreetPostal
										,City_Postal AS CityPostaL
										,AreaCode_Postal AS AreaPostal
										,HouseNo_Service AS HouseNoService
										,StreetName_Service AS StreetService
										,City_Service AS CityService
										,AreaCode_Service AS AreaService
										,DocumentName AS DocumentName
										,[Path] AS [Path]
										,EmployeeCode AS EmployeeCode
										,ZipCode_Postal AS PZipCode
										,ZipCode_Service AS SZipCode
										,AccountTypeId AS AccountTypeId
										,ClusterCategoryId AS ClusterCategoryId
										,RouteSequenceNumber AS RouteSequenceNumber
										,MGActTypeID AS MGActTypeID
										,IsCommunication_Postal AS IsCommunicationPostal
										,IsCommunication_Service AS IsCommunicationService
										,IdentityTypeIdList AS IdentityTypeIdList
										,IdentityNumberList AS IdentityNumberList
										,UDFTypeIdList AS UDFTypeIdList
										,UDFValueList AS UDFValueList
										,CreatedBy AS CreatedBy
										,CreatedDate AS CreatedDate
										 FROM CUSTOMERS.Tbl_ApprovalRegistration
										 WHERE ARID = @ARID
										FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
							EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml
					
							SET @GlobalAccountNumber=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
						
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
								,GlobalAccountNumber =@GlobalAccountNumber
							WHERE ARID = @ARID  
						
						--Audit table
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
							SET   -- Updating Request with Level Roles
								 ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
								,GlobalAccountNumber=@GlobalAccountNumber
							WHERE  ARID = @ARID
							
						END
				SET @IsSuccess=1
				END
		ELSE
			BEGIN -- No Approval Levels are there					
						
					SET @Xml=(SELECT 
									Title_Tenant AS TitleTanent
									,FirstName_Tenant AS FirstNameTanent
									,MiddleName_Tenant AS MiddleNameTanent
									,LastName_Tenant AS LastNameTanent
									,PhoneNumber AS PhoneNumberTanent
									,AlternatePhoneNumber AS AlternatePhoneNumberTanent
									,EmailID_Tenant AS EmailIdTanent
									,Title AS TitleLandlord
									,FirstName AS FirstNameLandlord
									,MiddleName AS MiddleNameLandlord
									,LastName AS LastNameLandlord
									,KnownAs AS KnownAs
									,HomeContactNumber AS HomeContactNumberLandlord
									,BusinessContactNumber AS BusinessPhoneNumberLandlord
									,OtherContactNumber AS OtherPhoneNumberLandlord
									,EmailId AS EmailIdLandlord
									,IsSameAsService AS IsSameAsService
									,IsSameAsTenent AS IsSameAsTenent
									,DocumentNo AS DocumentNo
									,ApplicationDate AS ApplicationDate
									,ConnectionDate AS ConnectionDate
									,SetupDate AS SetupDate
									,IsBEDCEmployee AS IsBEDCEmployee
									,IsVIPCustomer AS IsVIPCustomer
									,OldAccountNo AS OldAccountNo
									,CustomerTypeId AS CustomerTypeId
									,BookNo AS BookNo
									,PoleID AS PoleID
									,MeterNumber AS MeterNumber
									,TariffClassID AS TariffClassID
									,IsEmbassyCustomer AS IsEmbassyCustomer
									,EmbassyCode AS EmbassyCode
									,PhaseId AS PhaseId
									,ReadCodeID AS ReadCodeID
									,IdentityTypeId AS IdentityTypeId
									,IdentityNumber AS IdentityNumber
									,CertifiedBy AS CertifiedBy
									,Seal1 AS Seal1
									,Seal2 AS Seal2
									,ApplicationProcessedBy AS ApplicationProcessedBy
									,InstalledBy AS InstalledBy
									,AgencyId AS AgencyId
									,IsCAPMI AS IsCAPMI
									,MeterAmount AS MeterAmount
									,InitialBillingKWh AS InitialBillingKWh
									,InitialReading AS InitialReading
									,PresentReading AS PresentReading
									,HouseNo_Postal AS HouseNoPostal
									,StreetName_Postal AS StreetPostal
									,City_Postal AS CityPostaL
									,AreaCode_Postal AS AreaPostal
									,HouseNo_Service AS HouseNoService
									,StreetName_Service AS StreetService
									,City_Service AS CityService
									,AreaCode_Service AS AreaService
									,DocumentName AS DocumentName
									,[Path] AS [Path]
									,EmployeeCode AS EmployeeCode
									,ZipCode_Postal AS PZipCode
									,ZipCode_Service AS SZipCode
									,AccountTypeId AS AccountTypeId
									,ClusterCategoryId AS ClusterCategoryId
									,RouteSequenceNumber AS RouteSequenceNumber
									,MGActTypeID AS MGActTypeID
									,IsCommunication_Postal AS IsCommunicationPostal
									,IsCommunication_Service AS IsCommunicationService
									,IdentityTypeIdList AS IdentityTypeIdList
									,IdentityNumberList AS IdentityNumberList
									,UDFTypeIdList AS UDFTypeIdList
									,UDFValueList AS UDFValueList
									,CreatedBy AS CreatedBy
									,CreatedDate AS CreatedDate
									 FROM CUSTOMERS.Tbl_ApprovalRegistration
									 WHERE ARID = @ARID
									FOR XML PATH('CustomerRegistrationBE'),TYPE) 
				
						EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @Xml						
					
					SET @GlobalAccountNumber=(SELECT TOP 1 CustomerId FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
					UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked=1
						,GlobalAccountNumber=@GlobalAccountNumber
					WHERE  ARID = @ARID
					--Audit Ray
					SET @IsSuccess=1
				END					
		END
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM CUSTOMERS.Tbl_ApprovalRegistration 
																WHERE ARID = @ARID)
							
							
							--(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerNameChangeLogs 
							--									WHERE NameChangeLogId = @NameChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM CUSTOMERS.Tbl_ApprovalRegistration 
						WHERE ARID = @ARID
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE CUSTOMERS.Tbl_ApprovalRegistration 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE  ARID = @ARID	
			SET @IsSuccess=1	
		END  	
		SELECT @IsSuccess As IsSuccess, @GlobalAccountNumber as GlobalAccountNumber
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE
END
--------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_ApprovalRegistration]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [CUSTOMERS].[USP_ApprovalRegistration]  
(     
 @XmlDoc xml    
)    
AS    
BEGIN     
	DECLARE  
    @GlobalAccountNumber  Varchar(10)  
    ,@AccountNo  Varchar(50)  
    ,@TitleTanent varchar(10)  
    ,@FirstNameTanent varchar(100)  
    ,@MiddleNameTanent varchar(100)  
    ,@LastNameTanent varchar(100)  
    ,@PhoneNumberTanent varchar(20)  
    ,@AlternatePhoneNumberTanent  varchar(20)  
    ,@EmailIdTanent varchar(MAX)  
    ,@TitleLandlord varchar(10)  
    ,@FirstNameLandlord varchar(50)  
    ,@MiddleNameLandlord varchar(50)  
    ,@LastNameLandlord varchar(50)  
    ,@KnownAs varchar(150)  
    ,@HomeContactNumberLandlord varchar(20)  
    ,@BusinessPhoneNumberLandlord  varchar(20)  
    ,@OtherPhoneNumberLandlord varchar(20)  
    ,@EmailIdLandlord varchar(MAX)  
    ,@IsSameAsService BIT  
    ,@IsSameAsTenent BIT  
    ,@DocumentNo varchar(50)  
    ,@ApplicationDate Datetime  
    ,@ConnectionDate Datetime  
    ,@SetupDate Datetime  
    ,@IsBEDCEmployee BIT  
    ,@IsVIPCustomer BIT  
    ,@OldAccountNo Varchar(20)  
    ,@CreatedBy varchar(50)  
    ,@CustomerTypeId INT  
    ,@BookNo varchar(30)  
    ,@PoleID INT  
    ,@MeterNumber Varchar(50)  
    ,@TariffClassID INT  
    ,@IsEmbassyCustomer BIT  
    ,@EmbassyCode Varchar(50)  
    ,@PhaseId INT  
    ,@ReadCodeID INT  
    ,@IdentityTypeId INT  
    ,@IdentityNumber VARCHAR(100)  
    ,@UploadDocumentID varchar(MAX)  
    ,@CertifiedBy varchar(50)  
    ,@Seal1 varchar(50)  
    ,@Seal2 varchar(50)  
    ,@ApplicationProcessedBy varchar(50)  
    ,@EmployeeName varchar(100)  
    ,@InstalledBy  INT  
    ,@AgencyId INT  
    ,@IsCAPMI BIT  
    ,@MeterAmount Decimal(18,2)  
    ,@InitialBillingKWh BigInt  
    ,@InitialReading BigInt  
    ,@PresentReading BigInt  
    ,@StatusText NVARCHAR(max)  
    ,@CreatedDate DATETIME  
    ,@PostalAddressID INT  
    ,@Bedcinfoid INT  
    ,@ContactName varchar(100)  
    ,@HouseNoPostal varchar(50)  
    ,@StreetPostal Varchar(255)  
    ,@CityPostaL varchar(50)  
    ,@AreaPostal INT  
    ,@HouseNoService varchar(50)  
    ,@StreetService varchar(255)  
    ,@CityService varchar(50)  
    ,@AreaService INT  
    ,@TenentId INT  
    ,@ServiceAddressID INT  
    ,@IdentityTypeIdList varchar(MAX)  
    ,@IdentityNumberList varchar(MAX)  
    ,@DocumentName  varchar(MAX)  
    ,@Path  varchar(MAX)  
    ,@IsValid bit=1  
    ,@EmployeeCode INT  
    ,@PZipCode VARCHAR(50)  
    ,@SZipCode VARCHAR(50)  
    ,@AccountTypeId INT  
    ,@ClusterCategoryId INT  
    ,@RouteSequenceNumber INT  
    ,@Length INT  
    ,@UDFTypeIdList varchar(MAX)  
    ,@UDFValueList varchar(MAX)  
    ,@IsSuccessful BIT=1  
    ,@MGActTypeID INT  
    ,@IsCommunicationPostal BIT  
    ,@IsCommunicationService BIT  
    ,@IsServiceAddress_Postal BIT
    ,@IsServiceAddress_Service BIT
    ,@AgencyId_Value varchar(100)
    ,@AreaPostal_Name Varchar(100)
    ,@AreaService_Name Varchar(100)
    ,@MGActTypeID_Value VARCHAR(100)
    ,@MeterTypeId_Value VARCHAR(100)
	,@CustomerTypeId_Value VARCHAR(100)
	,@TariffClassID_Value VARCHAR(100)
	,@ClusterCategoryId_Value VARCHAR(100)
	,@AccountTypeId_Value VARCHAR(100)
	,@IsFinalApproval BIT = 0
	,@FunctionId INT
	,@ApprovalStatusId INT 
	,@BU_ID VARCHAR(50) 
	,@CurrentApprovalLevel INT
	,@RoleId INT
	,@CurrentLevel INT
	,@PresentRoleId INT
	,@NextRoleId INT
	,@NameChangeRequestId INT
	,@ModifiedBy varchar(50)
	,@RouteSequenceNumber_Value varchar(50)
	,@PhaseId_Value varchar(50)
	,@ReadCodeID_Value varchar(50)
	,@ApplicationProcessedBy_Value VARCHAR(50)
	,@EmployeeCode_Value Varchar(50)
	
    SET @StatusText='Dserialisation Starts'
	SELECT   
     @TitleTanent=C.value('(TitleTanent)[1]','varchar(10)')  
    ,@FirstNameTanent=C.value('(FirstNameTanent)[1]','varchar(100)')  
    ,@MiddleNameTanent=C.value('(MiddleNameTanent)[1]','varchar(100)')  
    ,@LastNameTanent=C.value('(LastNameTanent)[1]','varchar(100)')  
    ,@PhoneNumberTanent=C.value('(PhoneNumberTanent)[1]','varchar(20)')  
    ,@AlternatePhoneNumberTanent =C.value('(AlternatePhoneNumberTanent )[1]','varchar(20)')  
    ,@EmailIdTanent=C.value('(EmailIdTanent)[1]','varchar(MAX)')  
    ,@TitleLandlord=C.value('(TitleLandlord)[1]','varchar(10)')  
    ,@FirstNameLandlord=C.value('(FirstNameLandlord)[1]','varchar(50)')  
    ,@MiddleNameLandlord=C.value('(MiddleNameLandlord)[1]','varchar(50)')  
    ,@LastNameLandlord=C.value('(LastNameLandlord)[1]','varchar(50)')  
    ,@KnownAs=C.value('(KnownAs)[1]','varchar(150)')  
    ,@HomeContactNumberLandlord=C.value('(HomeContactNumberLandlord)[1]','varchar(20)')  
    ,@BusinessPhoneNumberLandlord =C.value('(BusinessPhoneNumberLandlord )[1]','varchar(20)')  
    ,@OtherPhoneNumberLandlord=C.value('(OtherPhoneNumberLandlord)[1]','varchar(20)')  
    ,@EmailIdLandlord=C.value('(EmailIdLandlord)[1]','varchar(MAX)')  
    ,@IsSameAsService=C.value('(IsSameAsService)[1]','BIT')  
    ,@IsSameAsTenent=C.value('(IsSameAsTenent)[1]','BIT')  
    ,@DocumentNo=C.value('(DocumentNo)[1]','varchar(50)')  
    ,@ApplicationDate=C.value('(ApplicationDate)[1]','Datetime')  
    ,@ConnectionDate=C.value('(ConnectionDate)[1]','Datetime')  
    ,@SetupDate=C.value('(SetupDate)[1]','Datetime')  
    ,@IsBEDCEmployee=C.value('(IsBEDCEmployee)[1]','BIT')  
    ,@IsVIPCustomer=C.value('(IsVIPCustomer)[1]','BIT')  
    ,@OldAccountNo=C.value('(OldAccountNo)[1]','Varchar(20)')  
    ,@CreatedBy=C.value('(CreatedBy)[1]','varchar(50)')  
    ,@CustomerTypeId=C.value('(CustomerTypeId)[1]','INT')  
    ,@BookNo=C.value('(BookNo)[1]','varchar(30)')  
    ,@PoleID=C.value('(PoleID)[1]','INT')  
    ,@MeterNumber=C.value('(MeterNumber)[1]','Varchar(50)')  
    ,@TariffClassID=C.value('(TariffClassID)[1]','INT')  
    ,@IsEmbassyCustomer=C.value('(IsEmbassyCustomer)[1]','BIT')  
    ,@EmbassyCode=C.value('(EmbassyCode)[1]','Varchar(50)')  
    ,@PhaseId=C.value('(PhaseId)[1]','INT')  
    ,@ReadCodeID=C.value('(ReadCodeID)[1]','INT')  
    ,@IdentityTypeId=C.value('(IdentityTypeId)[1]','INT')  
    ,@IdentityNumber=C.value('(IdentityNumber)[1]','VARCHAR(100)')  
    ,@UploadDocumentID=C.value('(UploadDocumentID)[1]','varchar(MAX)')  
    ,@CertifiedBy=C.value('(CertifiedBy)[1]','varchar(50)')  
    ,@Seal1=C.value('(Seal1)[1]','varchar(50)')  
    ,@Seal2=C.value('(Seal2)[1]','varchar(50)')  
    ,@ApplicationProcessedBy=C.value('(ApplicationProcessedBy)[1]','varchar(50)')  
    ,@EmployeeName=C.value('(EmployeeName)[1]','varchar(100)')  
    ,@InstalledBy =C.value('(InstalledBy )[1]','INT')  
    ,@AgencyId=C.value('(AgencyId)[1]','INT')  
    ,@IsCAPMI=C.value('(IsCAPMI)[1]','BIT')  
    ,@MeterAmount=C.value('(MeterAmount)[1]','Decimal(18,2)')  
    ,@InitialBillingKWh=C.value('(InitialBillingKWh)[1]','BigInt')  
    ,@InitialReading=C.value('(InitialReading)[1]','BigInt')  
    ,@PresentReading=C.value('(PresentReading)[1]','BigInt')  
    ,@HouseNoPostal =C.value('( HouseNoPostal )[1]',' varchar(50) ')  
    ,@StreetPostal=C.value('(StreetPostal)[1]','varchar(255)')  
    ,@CityPostaL=C.value('(CityPostaL)[1]','varchar(50)')  
    ,@AreaPostal=C.value('(AreaPostal)[1]','INT')  
    ,@HouseNoService=C.value('(HouseNoService)[1]','varchar(50)')  
    ,@StreetService=C.value('(StreetService)[1]','varchar(255)')  
    ,@CityService=C.value('(CityService)[1]','varchar(50)')  
    ,@AreaService=C.value('(AreaService)[1]','INT')  
    ,@IdentityTypeIdList=C.value('(IdentityTypeIdList)[1]','varchar(MAX)')  
    ,@IdentityNumberList=C.value('(IdentityNumberList)[1]','varchar(MAX)')  
    ,@DocumentName=C.value('(DocumentName)[1]','varchar(MAX)')  
    ,@Path=C.value('(Path)[1]','varchar(MAX)')  
    ,@EmployeeCode=C.value('(EmployeeCode)[1]','INT')  
    ,@PZipCode=C.value('(PZipCode)[1]','VARCHAR(50)')  
    ,@SZipCode=C.value('(SZipCode)[1]','VARCHAR(50)')  
    ,@AccountTypeId=C.value('(AccountTypeId)[1]','INT')  
    ,@UDFTypeIdList=C.value('(UDFTypeIdList)[1]','varchar(MAX)')  
    ,@UDFValueList=C.value('(UDFValueList)[1]','varchar(MAX)')  
    ,@ClusterCategoryId=C.value('(ClusterCategoryId)[1]','INT')  
    ,@RouteSequenceNumber=C.value('(RouteSequenceNumber)[1]','INT')  
    ,@MGActTypeID=C.value('(MGActTypeID)[1]','INT')  
    ,@IsCommunicationPostal=C.value('(IsCommunicationPostal)[1]','BIT')  
    ,@IsCommunicationService=C.value('(IsCommunicationService)[1]','BIT') 
    ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')  
    ,@FunctionId=C.value('(FunctionId)[1]','VARCHAR(50)')  
    ,@ApprovalStatusId=C.value('(ApprovalStatusId)[1]','INT') 
    ,@IsFinalApproval =C.value('(IsFinalApproval)[1]','BIT')    
FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C) 
	--Set values for Ids to display purpose
	SET @CreatedDate=dbo.fn_GetCurrentDateTime()
	SET @EmployeeName=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId = @CertifiedBy)
	SET @AgencyId_Value=(SELECT AgencyName FROM Tbl_Agencies WHERE AgencyId=@AgencyId)
	SET @AreaPostal_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaPostal)
	SET @AreaService_Name=(SELECT AreaDetails FROM MASTERS.Tbl_MAreaDetails WHERE AreaCode=@AreaService)
	SET @MGActTypeID_Value=(SELECT AccountType FROM Tbl_MGovtAccountTypes WHERE GActTypeID=@MGActTypeID)	
	SET @MeterTypeId_Value=(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber))
	SET @CustomerTypeId_Value=(SELECT CustomerType FROM Tbl_MCustomerTypeS WHERE CustomerTypeId =@CustomerTypeId)
	SET @TariffClassID_Value=(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=@TariffClassID)
	SET @ClusterCategoryId_Value=(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId=@ClusterCategoryId)
	SET @AccountTypeId_Value=(SELECT AccountType FROM Tbl_MAccountTypes WHERE AccountTypeId=@AccountTypeId)
	SET @RouteSequenceNumber_Value=(SELECT RouteName FROM Tbl_MRoutes WHERE RouteId=@RouteSequenceNumber)
	SET @PhaseId_Value=(SELECT Phase FROM Tbl_MPhases WHERE PhaseId=@PhaseId)
	SET @ReadCodeID_Value=(SELECT ReadCode FROM Tbl_MReadCodes WHERE ReadCodeId=@ReadCodeID)
	SET @ApplicationProcessedBy_Value =@ApplicationProcessedBy
	SET @EmployeeCode_Value=(SELECT EmployeeName FROM EMPLOYEE.Tbl_MBEDCEmployeeDetails WHERE BEDCEmpId =@EmployeeCode)
	
	SET @StatusText='Approval Registration Starts'
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level		
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO CUSTOMERS.Tbl_ApprovalRegistration
		(
			CertifiedBy
		,	CertifiedBy_Value
		,	Seal1
		,	Seal2
		,	ApplicationProcessedBy
		,	ContactName
		,	InstalledBy
		,	InstalledBy_Value
		,	AgencyId
		,	AgencyId_Value
		,	MeterAmount
		,	InitialBillingKWh
		,	InitialReading
		,	PresentReading
		,	HouseNo_Postal
		,	StreetName_Postal
		,	City_Postal
		,	AreaCode_Postal
		,	AreaCode_Postal_Value
		,	IsServiceAddress_Postal
		,	ZipCode_Postal
		,	IsCommunication_Postal
		,	HouseNo_Service
		,	StreetName_Service
		,	City_Service
		,	AreaCode_Service
		,	AreaCode_Service_Value
		,	IsServiceAddress_Service
		,	ZipCode_Service
		,	IsCommunication_Service
		,	Title
		,	FirstName
		,	MiddleName
		,	LastName
		,	KnownAs
		,	EmailId
		,	HomeContactNumber
		,	BusinessContactNumber
		,	OtherContactNumber
		,	IsSameAsService
		,	IsSameAsTenent
		,	DocumentNo
		,	ApplicationDate
		,	ConnectionDate
		,	SetupDate
		,	IsBEDCEmployee
		,	IsVIPCustomer
		,	OldAccountNo
		,	Service_HouseNo
		,	Service_StreetName
		,	Service_City
		,	Service_AreaCode
		,	Service_ZipCode
		,	Postal_HouseNo
		,	Postal_StreetName
		,	Postal_City
		,	Postal_AreaCode
		,	Postal_ZipCode
		,	FirstName_Tenant
		,	MiddleName_Tenant
		,	LastName_Tenant
		,	PhoneNumber
		,	AlternatePhoneNumber
		,	EmailID_Tenant
		,	Title_Tenant
		,	MeterNo_Tenant
		,	DocumentName
		,	[Path]
		,	MGActTypeID
		,	MGActTypeID_Value
		,	MeterNo
		,	MeterTypeId
		,	MeterTypeId_Value
		,	MeterCost
		,	MeterAssignedDate
		,	CustomerTypeId
		,	CustomerTypeId_Value
		,	TariffClassID
		,	TariffClassID_Value
		,	RouteSequenceNumber
		,	IsEmbassyCustomer
		,	EmbassyCode
		,	PhaseId
		,	ReadCodeID
		,	ClusterCategoryId
		,	ClusterCategoryId_Value
		,	BookNo
		,	PoleID
		,	MeterNumber
		,	AccountTypeId
		,	AccountTypeId_Value
		,   IdentityTypeIdList
		,	IdentityNumberList
		,	UDFTypeIdList
		,	UDFValueList
		,	CreatedBy
		,	CreatedDate
		,	PresentApprovalRole
		,	NextApprovalRole
		,	ApproveStatusId
		,	CurrentApprovalLevel
		,   RouteSequenceNumber_value
		,	PhaseId_Value
		,	ReadCodeID_Value
		,	ApplicationProcessedBy_Value
		,	EmployeeCode
		,	EmployeeCode_Value
		)
		VALUES
		(
			 @CertifiedBy  
			,@EmployeeName
			,@Seal1
			,@Seal2
			,@ApplicationProcessedBy
			,@ContactName
			,@InstalledBy
			,@EmployeeName
			,@AgencyId
			,@AgencyId_Value
			,@MeterAmount
			,@InitialBillingKWh
			,@InitialReading
			,@PresentReading
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@AreaPostal_Name
			,@IsServiceAddress_Postal
			,@PZipCode
			,@IsCommunicationPostal	
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@AreaService_Name
			,@IsServiceAddress_Service
			,@SZipCode
			,@IsCommunicationService
			,@TitleLandlord
			,@FirstNameLandlord
			,@MiddleNameLandlord
			,@LastNameLandlord
			,@KnownAs
			,@EmailIdLandlord
			,@HomeContactNumberLandlord
			,@BusinessPhoneNumberLandlord
			,@OtherPhoneNumberLandlord
			,@IsSameAsService
			,@IsSameAsTenent
			,@DocumentNo
			,@ApplicationDate
			,@ConnectionDate
			,@SetupDate
			,@IsBEDCEmployee
			,@IsVIPCustomer
			,@OldAccountNo
			,@HouseNoService
			,@StreetService
			,@CityService
			,@AreaService
			,@SZipCode
			,@HouseNoPostal
			,@StreetPostal
			,@CityPostaL
			,@AreaPostal
			,@PZipCode
			,@FirstNameTanent
			,@MiddleNameTanent
			,@LastNameTanent
			,@PhoneNumberTanent
			,@AlternatePhoneNumberTanent
			,@EmailIdTanent
			,@TitleTanent
			,@MeterNumber
			,@DocumentName
			,@Path
			,@MGActTypeID
			,@MGActTypeID_Value
			,@MeterNumber
			,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNumber)  
			,@MeterTypeId_Value
			,@MeterAmount
			,@ConnectionDate
			,@CustomerTypeId
			,@CustomerTypeId_Value
			,@TariffClassID
			,@TariffClassID_Value
			,@RouteSequenceNumber
			,@IsEmbassyCustomer
			,@EmployeeCode
			,@PhaseId
			,@ReadCodeID
			,@ClusterCategoryId
			,@ClusterCategoryId_Value
			,@BookNo
			,@PoleID
			,@MeterNumber
			,@AccountTypeId
			,@AccountTypeId_Value
			,@IdentityTypeIdList
			,@IdentityNumberList
			,@UDFTypeIdList
			,@UDFValueList
			,@CreatedBy
			,@CreatedDate
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
			, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
			, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
			, @RouteSequenceNumber_value
			, @PhaseId_Value
			, @ReadCodeID_Value
			, @ApplicationProcessedBy_Value
			, @EmployeeCode
			, @EmployeeCode_Value
		)
			
			IF(@IsFinalApproval = 1)
				BEGIN
				DECLAre @xml xml=null;
				EXECUTE [CUSTOMERS].[USP_CustomerRegistration] @XmlDoc
				SET @GlobalAccountNumber=(SELECT TOP 1 GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomersDetail ORDER BY CustomerId DESC)
				UPDATE CUSTOMERS.Tbl_ApprovalRegistration SET GlobalAccountNumber=@GlobalAccountNumber 
				WHERE ARID =(SELECT TOP 1 ARID FROM CUSTOMERS.Tbl_ApprovalRegistration ORDER BY ARID DESC ) 
				END
			ELSE
			BEGIN
				SELECT 1 AS IsSuccessful  
				 ,@StatusText AS StatusText   
				 FOR XML PATH('CustomerRegistrationBE'),TYPE  
			END
END
------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
			,@Count INT
			,@TotalReadingEnergies DECIMAL(18,2)
			,@ApprovalStatusId INT  
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@BU_ID VARCHAR(50)
			,@CurrentApprovalLevel INT
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	,@ApprovalStatusId=C.value('(ApproveStatusId)[1]','INT')
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')   
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	-- This is for Adjusting Initilal Billingkwh 
	IF((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)=0)
	BEGIN
		
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
		SET InitialReading=@ModifiedReading
			,PresentReading=@ModifiedReading
		WHERE GlobalAccountNumber=@GlobalAccountNumber
		
		SELECT 1 AS IsInitialBillingkWh
		FOR XML PATH('BillingBE'),TYPE
	END	
	
	-- This is for Adjusting Present Meter Reading
	
	ELSE IF((SELECT COUNT(0)FROM Tbl_PresentReadingAdjustmentApprovalLog 
				WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillingBE')
		END   
	ELSE
		BEGIN
	
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level	
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
				DECLARE @Forward INT
				SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					
				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END				
					
				IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
					BEGIN
						SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
					END
				ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
					BEGIN
						SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
					END
				ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
					BEGIN
						SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
					END
				ELSE
					BEGIN
					
					DECLARE @PreviousReadDate DATETIME
					SELECT TOP 1 @PreviousReadDate=ReadDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber
					ORDER BY CustomerReadingId DESC
					
					INSERT INTO Tbl_PresentReadingAdjustmentApprovalLog(
							 GlobalAccountNumber 
							,PreviousReading 
							,PresentReading 
							,NewPresentReading 
							,PreviousReadDate 
							,PresentReadDate 
							,MeterNumber 
							,Remarks 
							,ApprovalStatusId 
							,PresentApprovalRole 
							,NextApprovalRole 
							,CurrentAppovalLevel 
							,IsLocked
							,CreatedBy 
							,CreatedDate 
							,ModifiedBy 
							,ModifiedDate)
						SELECT 
							 @GlobalAccountNumber
							,@PreviousReading 
							,@PresentReading 
							,@ModifiedReading 
							,@PreviousReadDate 
							,dbo.fn_GetCurrentDateTime() 
							,@MeterNumber 
							,NULL 
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END 
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END 
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END 
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END 
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
							,@CreatedBy 
							,dbo.fn_GetCurrentDateTime()
							,@CreatedBy 
							,dbo.fn_GetCurrentDateTime()
					
					IF(@IsFinalApproval = 1)
						BEGIN
					
						SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)			
						
						SET @Count=(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
						
						IF (@Count=1)
							BEGIN
								SET @TotalReadingEnergies=@Usage
							END
						ELSE
							BEGIN
								
								DECLARE @Last2ndTotalReadingEnergies DECIMAL(18,2)
								
								SELECT @Last2ndTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
								WHERE TotalReadings =(SELECT MAX(TotalReadings)-1
																FROM Tbl_CustomerReadings
																WHERE GlobalAccountNumber=@GlobalAccountNumber)
								AND GlobalAccountNumber=@GlobalAccountNumber
								
								--SELECT @Last1stTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
								--WHERE TotalReadings =(SELECT MAX(TotalReadings)
								--								FROM Tbl_CustomerReadings
								--								WHERE GlobalAccountNumber=@GlobalAccountNumber)
								--AND GlobalAccountNumber=@GlobalAccountNumber
								
								UPDATE Tbl_CustomerReadings
								SET IsReadingWrong = 1
								WHERE TotalReadings =(SELECT MAX(TotalReadings)
																FROM Tbl_CustomerReadings
																WHERE GlobalAccountNumber=@GlobalAccountNumber)
								AND GlobalAccountNumber = @GlobalAccountNumber

								SET @TotalReadingEnergies= @Last2ndTotalReadingEnergies+@Usage
							END
						
						SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
						SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																						WHERE GlobalAccountNumber=@GlobalAccountNumber)
						
						--DECLARE @PreviousReadDate DATETIME
						--SELECT TOP 1 @PreviousReadDate=ReadDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber
						--ORDER BY CustomerReadingId DESC
						
						
						INSERT INTO Tbl_CustomerReadings(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								--,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								--,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								--,IsReadingWrong
								--,IsRollOver
								)
							VALUES(
								 @GlobalAccountNumber
								,GETDATE()
								,@ReadBy
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
								--,@AverageReading
								,0
								--,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
								,@TotalReadingEnergies
								,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
								,@Multipiler
								,2
								,@CreatedBy
								,GETDATE()
								--,@IsTamper
								,@MeterNumber
								,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
								--,@IsRollover
								--,1
								)
							
						SELECT @AverageReading = dbo.fn_GetAverageReading_Adjustment(@GlobalAccountNumber)
						
						UPDATE Tbl_CustomerReadings
						SET AverageReading = @AverageReading
						WHERE TotalReadings =(SELECT MAX(TotalReadings)
														FROM Tbl_CustomerReadings
														WHERE GlobalAccountNumber=@GlobalAccountNumber)
						AND GlobalAccountNumber = @GlobalAccountNumber
									
						INSERT INTO Tbl_PresentReadingAdjustmentLog
								(
									 GlobalAccountNumber
									,PreviousReading 
									,PresentReading 
									,NewPresentReading 
									,PreviousReadDate 
									,PresentReadDate 
									,MeterNumber 
									,CreatedBy 
									,CreatedDate
								)
						VALUES	(
									@GlobalAccountNumber
									,@PreviousReading
									,@PresentReading
									,@ModifiedReading
									,@PreviousReadDate
									,dbo.fn_GetCurrentDateTime()
									,@MeterNumber
									,@CreatedBy
									,dbo.fn_GetCurrentDateTime()
								)
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						SET InitialReading=@ModifiedReading
							,PresentReading=@ModifiedReading
						WHERE GlobalAccountNumber=@GlobalAccountNumber
						
					END
					
					SELECT 1 AS IsSuccess	
					FOR XML PATH('BillingBE'),TYPE 	
					END
	END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 08 May 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@OldAccountNo VARCHAR(50)
			,@AccountNo1 VARCHAR(50)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount 
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@OldAccountNo=OldAccountNo
			,@AccountNo1 = AccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY CustomerPaymentID DESC
    
    SELECT 
    (
		SELECT CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
			,@LastPaidAmount AS LastPaidAmount
			,@GlobalAccountNo AS AccountNo
			,(@AccountNo1+' - '+@GlobalAccountNo) AS AccNoAndGlobalAccNo
			,@Name AS Name
			,@ServiceAddress AS ServiceAddress
			,@OutstandingAmount AS OutStandingAmount
			,@BookGroup AS BookGroup 
			,@BookName AS BookName
			,@Tariff AS Tariff
			,@OldAccountNo AS OldAccountNo
		FOR XML PATH('Customerdetails'),TYPE 
    )
    ,
    (
		SELECT 
			 CB.BillNo
			,CustomerBillId AS CustomerBillID
			,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
			,CB.BillYear        
			,CB.BillMonth        
			,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
			,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
			,ISNULL(CB.PreviousReading,'0') AS PreviousReading
			,ISNULL(CB.PresentReading,'0') AS PresentReading
			,Usage AS Consumption
			,Dials AS MeterDials
			,NetEnergyCharges
			,NetFixedCharges
			,TotalBillAmount
			,VAT
			,TotalBillAmountWithTax
			,NetArrears
			,TotalBillAmountWithArrears 
			,CB.AccountNo AS AccountNo
			,CB.PaidAmount AS LastPaidAmount
		FROM Tbl_CustomerBills CB 
		WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
		AND (ISNULL(TotalBillAmountWithTax,0) > (ISNULL(PaidAmount,0) + ISNULL(AdjustmentAmmount,0)))
		ORDER BY CB.CustomerBillId DESC
		FOR XML PATH('CustomerBillDetails'),TYPE 
    )
    FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')            
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetailsByAccountNo]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 11 Apr 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@CustomerTypeId INT
			,@OldAccountNo VARCHAR(50)
			,@AccNoAndGlobalAccNo VARCHAR(100)
			,@ActiveStatusId INT
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount
			,@AccNoAndGlobalAccNo=(AccountNo+' - '+GlobalAccountNumber)
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@CustomerTypeId=CustomerTypeId
			,@OldAccountNo=OldAccountNo
			,@ActiveStatusId = ActiveStatusId
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC, CustomerPaymentID  DESC
	
	
    
	SELECT TOP(1)
		 CB.BillNo
		,CustomerBillId
		--,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
		,RC.DisplayCode AS ReadType
		,CB.BillYear        
		,CB.BillMonth        
		,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
		,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
		,ISNULL(CB.PreviousReading,'0') AS PreviousReading
		,ISNULL(CB.PresentReading,'0') AS PresentReading
		
		,Usage AS Consumption
		,Dials AS MeterDials
		,NetEnergyCharges
		,NetFixedCharges
		,TotalBillAmount
		,VAT 
		,TotalBillAmountWithTax
		,NetArrears
		,TotalBillAmountWithArrears 		
		,CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
		,@LastPaidAmount AS LastPaidAmount
		,CB.AccountNo AS AccountNo
		,@Name AS Name
		,@AccNoAndGlobalAccNo AS AccNoAndGlobalAccNo
		,@ServiceAddress AS ServiceAddress
		,@OutstandingAmount AS OutStandingAmount
		,@BookGroup AS BookGroup 
		,@BookName AS BookName
		,@Tariff AS Tariff
		,@OldAccountNo AS OldAccountNo
		,COUNT(0) OVER() AS TotalRecords
		,VAT AS Vat
		,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS ClassName
		,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=@CustomerTypeId) AS CustomerType
		,@ActiveStatusId AS ActiveStatusId
	FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
	WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
	AND (ISNULL(TotalBillAmountWithTax,0) > (ISNULL(PaidAmount,0) + ISNULL(AdjustmentAmmount,0)))
	ORDER BY CB.CustomerBillId DESC
	FOR XML PATH('CustomerDetailsBe'),TYPE                  
    
 END     


GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]
(
	@XmlDoc xml
)
AS
BEGIN 

--------------------------------------------
--              Declaration
--------------------------------------------

	DECLARE 
	@BU_ID VARCHAR(MAX) --='BEDC_BU_0012' 
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  = 15
   ,@ApprovalRoleId INT 
   ,@ARID INT 
   ,@SU_ID VARCHAR(MAX)--='BEDC_SU_0036'  
   ,@SC_ID VARCHAR(MAX)--='BEDC_SC_0043,BEDC_SC_0044'  
   ,@CycleId VARCHAR(MAX)--='BEDC_C_0176,BEDC_C_0179,BEDC_C_0180,BEDC_C_0177,BEDC_C_0178,BEDC_C_0192'  
   ,@TariffId VARCHAR(MAX)--='2,3,4,5,7,8,9,11,12,13,15,16,19,21,22,39,44,45,47,49,50,51,54,55'  
   ,@BookNo VARCHAR(MAX)--='BEDC_B_1009,BEDC_B_1010,BEDC_B_1011,BEDC_B_1012,BEDC_B_1013,BEDC_B_1014,BEDC_B_1015,BEDC_B_1016,BEDC_B_1017,BEDC_B_1018'  
   ,@FromDate VARCHAR(20)--='07/04/2015'  
   ,@ToDate VARCHAR(20)--='08/01/2015' 
   ,@PageNo INT--=1
   ,@PageSize INT--=5 
   ,@Count INT
        
--------------------------------------------
--              Deserialization
--------------------------------------------
        
	 SELECT       
	 
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
	    
	  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
	 
	 
--------------------------------------------
--              Initialization on variables
-------------------------------------------- 
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 

--------------------------------------------
--              Requirement
-------------------------------------------- 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
   ;WITH PageResult AS
   (		
		SELECT    
		ARID AS ARID
	   ,ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber 
	   ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.PoleID    
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal        
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading 
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Remarks WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Remarks END AS Remarks  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS PhaseId  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  ,CD.OutStandingAmount_ActiveDetails
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
		,BN.BookNo AS BookNumber
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,CONVERT(VARCHAR(19),CD.CreatedDate) AS CreatedDate
	  ,BN.SortOrder AS BookSortOrder
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 --WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
			AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
			AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
			AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
			AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,','))
	 )
 	 SELECT * FROM PageResult
	 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
		ORDER BY ARID DESC 
		
	SET @Count =(select COUNT(0) from CUSTOMERS.Tbl_ApprovalRegistration as cd
				INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
				INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
				INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
				INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
				INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
				AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
				AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
				AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
				AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
				AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
				AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
				AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')))
	 SELECT @Count AS [Count]
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetMeterInformation]    Script Date: 08/04/2015 13:09:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  V.Bhimaraju    
-- Create date: 08-04-2014    
-- Description: The purpose of this procedure is to get list of MeterInformation    
-- Author:  V.Bhimaraju    
-- Modified date: 04-02-2015    
-- Description: Added GlobalAccount No in this proc    
-- Author:  Faiz-ID103  
-- Modified date: 20-APR-2015  
-- Description: Modified the SP for active meter Type filter  
-- Modified By :kalyan
-- Modified Date :01-08-2015
-- =============================================    
ALTER PROCEDURE [dbo].[USP_GetMeterInformation]    
(    
 @XmlDoc xml    
)    
AS    
BEGIN    
 DECLARE  @PageNo INT    
   ,@PageSize INT    
   ,@BU_ID VARCHAR(20) 
   ,@StartIndex int   
   ,@EndIndex int      
        
  SELECT       
   @PageNo = C.value('(PageNo)[1]','INT')    
   ,@PageSize = C.value('(PageSize)[1]','INT')   
   ,@StartIndex= C.value('(startIndex)[1]','INT')    
   ,@EndIndex= C.value('(endIndex)[1]','INT')   
    
   ,@BU_ID= C.value('(BU_ID)[1]','VARCHAR(20)')    
  FROM @XmlDoc.nodes('MastersBE') AS T(C)     
      
  ;WITH PagedResults AS    
  (    
   SELECT    
     ROW_NUMBER() OVER(ORDER BY M.ActiveStatusId ASC , M.CreatedDate DESC ) AS RowNumber    
    ,MeterId    
    ,MeterNo    
    ,M.MeterType AS MeterTypeId    
    --,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=M.MeterType) AS MeterType    
    ,MT.MeterType AS MeterType --Faiz-ID103  
    ,MeterSize    
    ,ISNULL(MeterDetails,'---') AS Details    
    ,M.ActiveStatusId    
    ,M.CreatedBy    
    ,MeterMultiplier    
    ,MeterBrand    
    ,MeterSerialNo    
    ,MeterRating    
    ,CONVERT(VARCHAR(50),NextCalibrationDate,103) AS BillDate    
    ,ISNULL((CONVERT(VARCHAR(50),NextCalibrationDate,106)),'--') AS NextCalibrationDate    
    ,ISNULL(ServiceHoursBeforeCalibration,'--') AS ServiceHoursBeforeCalibration    
    ,MeterDials    
    ,ISNULL(Decimals,0) AS Decimals    
    ,M.BU_ID    
    ,B.BusinessUnitName    
    ,M.IsCAPMIMeter  
    ,M.CAPMIAmount  
    ,ISNULL(CPD.GlobalAccountNumber,'--') AS AccountNo    
    ,(CASE WHEN ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs AM WHERE AM.MeterNo = M.MeterNo AND ApprovalStatusId IN(1,4)) > 0) THEN 1  
   WHEN ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs AM WHERE AM.NewMeterNo = M.MeterNo AND ApproveStatusId IN(1,4)) > 0) THEN 1  
   ELSE 0 END) AS ActiveStatus  
    FROM Tbl_MeterInformation AS M    
    JOIN Tbl_BussinessUnits B ON M.BU_ID=B.BU_ID     
    LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.MeterNumber=M.MeterNo    
    JOIN Tbl_MMeterTypes MT ON MT.MeterTypeId = M.MeterType AND MT.ActiveStatus=1 --Faiz-ID103  
    WHERE M.ActiveStatusId IN(1,2)    
    AND M.BU_ID=@BU_ID OR @BU_ID=''    
  )    
      
  SELECT     
    (    
   SELECT *    
     ,(Select COUNT(0) from PagedResults) as TotalRecords         
   FROM PagedResults    
   WHERE (RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize) and (RowNumber>=@StartIndex  and RowNumber<=@EndIndex)
   FOR XML PATH('MastersBE'),TYPE    
  )    
  FOR XML PATH(''),ROOT('MastersBEInfoByXml')     
END    
GO


