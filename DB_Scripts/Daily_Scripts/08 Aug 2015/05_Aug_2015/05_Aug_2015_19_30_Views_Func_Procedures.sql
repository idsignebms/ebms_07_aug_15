GO

/****** Object:  View [dbo].[UDV_CustomerDetailsForBillPDF]    Script Date: 08/05/2015 19:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
ALTER VIEW [dbo].[UDV_CustomerDetailsForBillPDF] AS  
  
 SELECT   
   CD.GlobalAccountNumber  
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS FullName  
  ,ISNULL(CD.HomeContactNumber,ISNULL(CD.BusinessContactNumber,ISNULL(CD.OtherContactNumber,'--'))) AS ContactNo  
  ,CD.ActiveStatusId  
  ,PD.PoleID  
  ,[dbo].[fn_GetCustomerPoleNo_ByPoleId](PD.PoleID) AS PoleNo  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName AS TariffName  
  ,TCL.ClassName AS TariffCategory  
  ,PD.ReadCodeID  
  ,CAD.OutStandingAmount  
  ,BN.BookNo 
  ,BN.ID + '(' + BN.BookCode + ')' AS BookName
  ,BU.BU_ID  
  ,BU.BusinessUnitName  
  ,SU.SU_ID  
  ,SU.ServiceUnitName  
  ,SC.ServiceCenterId  
  ,SC.ServiceCenterName  
  ,C.CycleId  
  ,C.CycleName  
  ,CD.EmailId  
  ,PD.MeterNumber  
  ,ISNULL(MI.IsCAPMIMeter,0) AS IsCAPMIMeter  
  ,ISNULL(MI.CAPMIAmount,0) AS CAPMIAmount  
  ,PD.PhaseId  
  ,PH.Phase  
  ,CAD.InitialBillingKWh   
  ,PD.RouteSequenceNumber  
  ,R.RouteName    
  ,PD.CustomerTypeId  
  ,CT.CustomerType  
  ,CAD.OpeningBalance  
  ,[dbo].[fn_GetCustomerAddressFormat](PAD.HouseNo,PAD.StreetName,PAD.City,PAD.ZipCode) AS ServiceAddress  
  ,(CASE WHEN ISNULL(PAD.IsCommunication,0) = 1   
    THEN [dbo].[fn_GetCustomerAddressFormat](PAD.HouseNo,PAD.StreetName,PAD.City,PAD.ZipCode)  
    ELSE (SELECT [dbo].[fn_GetCustomerAddressFormat](PAD2.HouseNo,PAD2.StreetName,PAD2.City,PAD2.ZipCode)  
       FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails AS PAD2   
       WHERE PAD2.AddressID = CD.PostalAddressID)  
    END) AS BillingAddress  
 FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
 LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
 LEFT JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber  
 LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId  
 LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails AS PAD ON PAD.AddressID = CD.ServiceAddressID   
 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
 INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
 INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
 INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
 INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID   
 INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID   
 LEFT JOIN dbo.Tbl_MTariffClasses AS TCL ON TCL.ClassID = TC.RefClassID   
 LEFT JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId   
 LEFT JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo  
 LEFT JOIN dbo.Tbl_MCustomerTypes AS CT ON CT.CustomerTypeId = PD.CustomerTypeId  
 LEFT JOIN dbo.Tbl_MRoutes AS R ON R.RouteId = PD.RouteSequenceNumber  
 LEFT JOIN dbo.Tbl_MPhases AS PH ON PH.PhaseId = PD.PhaseId  
 WHERE PAD.IsActive = 1  
  
  
GO

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillFixedCharges]    Script Date: 08/05/2015 19:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 04 Aug 2015
-- Description:	To get the Fixed charges details
-- =============================================
CREATE FUNCTION [dbo].[fn_GetCustomerBillFixedCharges]
(
	 @FixedCharges BillFixedCharges READONLY
	,@MeterEffectedAmount DECIMAL(18,2)
)
RETURNS @TblCustomerFixedCharges TABLE
(
	 ChargeId INT
	,Amount DECIMAL(18,2)
)
AS
BEGIN

	DECLARE @MinChargeId INT, @MaxChargeId INT
	
	SELECT @MinChargeId = MIN(ChargeId), @MaxChargeId = MAX(ChargeId) FROM @FixedCharges
	
	WHILE(@MinChargeId <= @MaxChargeId)
		BEGIN				
			DECLARE @ChargeAmount DECIMAL(18,2) = 0
			
			SELECT @ChargeAmount = Amount FROM @FixedCharges WHERE ChargeId = @MinChargeId
			
			IF(@MeterEffectedAmount >= @ChargeAmount) 
				BEGIN
					SET @MeterEffectedAmount = @MeterEffectedAmount - @ChargeAmount
					SET @ChargeAmount = 0
				END
			ELSE
				BEGIN
					SET @ChargeAmount = @ChargeAmount - @MeterEffectedAmount
					SET @MeterEffectedAmount = 0
				END
			
			INSERT INTO @TblCustomerFixedCharges(ChargeId, Amount)
			VALUES(@MinChargeId, @ChargeAmount)
			
			SELECT @MinChargeId = MIN(ChargeId) FROM @FixedCharges WHERE ChargeId > @MinChargeId
		END
	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillsDetailsForPrint_ByServiceCenter]    Script Date: 08/05/2015 19:34:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 04 Aug 2015
-- Description:	To get the Last Bill Details for Bill text file to bulk bills
-- =============================================
CREATE FUNCTION [dbo].[fn_GetCustomerLastBillsDetailsForPrint_ByServiceCenter]
(
	 @ServiceCenter VARCHAR(50)
	,@BillMonth INT
	,@BillYear INT
)
RETURNS @TblCustomerLastBill TABLE
(
	 GlobalAccountNo VARCHAR(50)
	,PreviousBalance DECIMAL(18,2)
	,Payments DECIMAL(18,2)
	,Adjustments DECIMAL(18,2)
)
AS
BEGIN

	DECLARE @TempCustomerBills TABLE(SNo INT IDENTITY(1,1), GlobalAccountNo VARCHAR(50))
	
	INSERT INTO @TempCustomerBills(GlobalAccountNo)
	SELECT AccountNo FROM Tbl_CustomerBills
	WHERE ServiceCenterId = @ServiceCenter and BillMonth = @BillMonth and BillYear = @BillYear

	DECLARE @SNo INT = 1, @TotalCount INT = 0
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomerBills
	
	WHILE(@SNo <= @TotalCount)
	BEGIN			
		
		DECLARE @GlobalAccountNo VARCHAR(50)
			,@LastBilledDate DATETIME
			,@PreviousBalance DECIMAL(18,2) = 0 
			,@Payments DECIMAL(18,2) = 0 
			,@Adjustments DECIMAL(18,2) = 0 
			,@BillNo VARCHAR(50)		
		
		SELECT @GlobalAccountNo = GlobalAccountNo FROM @TempCustomerBills WHERE SNo = @SNo
	
		SELECT @BillNo = BillNo, @PreviousBalance = ISNULL(NetArrears,0) FROM Tbl_CustomerBills 
		WHERE AccountNo = @GlobalAccountNo and BillMonth = @BillMonth and BillYear = @BillYear

		IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
			BEGIN
				SELECT TOP 1
					 @LastBilledDate = BillGeneratedDate
					,@PreviousBalance = ISNULL(TotalBillAmountWithArrears,0)
				FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
					AND BillNo != @BillNo
				ORDER BY BillGeneratedDate DESC
				
				SELECT
					 @Payments = SUM(ISNULL(PaidAmount,0))
				FROM Tbl_CustomerPayments
				WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo	
				
				SELECT
					@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
				FROM Tbl_BillAdjustments
				WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo				
			END	
		ELSE
			BEGIN			
				SELECT
					 @Payments = SUM(ISNULL(PaidAmount,0))
				FROM Tbl_CustomerPayments
				WHERE AccountNo = @GlobalAccountNo	
				
				SELECT
					@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
				FROM Tbl_BillAdjustments
				WHERE AccountNo = @GlobalAccountNo	
			END
			
		INSERT INTO @TblCustomerLastBill(GlobalAccountNo, PreviousBalance, Payments, Adjustments)
		VALUES(@GlobalAccountNo, @PreviousBalance, @Payments, @Adjustments)	
		
		SET @SNo = @SNo + 1
	END
	
	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillsDetailsForPrint]    Script Date: 08/05/2015 19:34:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 04 Aug 2015
-- Description:	To get the Last Bill Details for Bill text file
-- =============================================
CREATE FUNCTION [dbo].[fn_GetCustomerLastBillsDetailsForPrint]
(
	 @GlobalAccountNo VARCHAR(50)
	,@BillMonth INT
	,@BillYear INT
)
RETURNS @TblCustomerLastBill TABLE
(
	 GlobalAccountNo VARCHAR(50)
	,PreviousBalance DECIMAL(18,2)
	,Payments DECIMAL(18,2)
	,Adjustments DECIMAL(18,2)
)
AS
BEGIN

	DECLARE @LastBilledDate DATETIME
		,@PreviousBalance DECIMAL(18,2) = 0 
		,@Payments DECIMAL(18,2) = 0 
		,@Adjustments DECIMAL(18,2) = 0 
		,@BillNo VARCHAR(50)
		
	SELECT @BillNo = BillNo, @PreviousBalance = ISNULL(NetArrears,0) FROM Tbl_CustomerBills 
	WHERE AccountNo = @GlobalAccountNo and BillMonth = @BillMonth and BillYear = @BillYear

	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = BillGeneratedDate
				,@PreviousBalance = ISNULL(TotalBillAmountWithArrears,0)
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				 @Payments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			SELECT
				@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo				
		END	
	ELSE
		BEGIN			
			SELECT
				 @Payments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE AccountNo = @GlobalAccountNo	
			
			SELECT
				@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE AccountNo = @GlobalAccountNo	
		END
		
	INSERT INTO @TblCustomerLastBill(GlobalAccountNo, PreviousBalance, Payments, Adjustments)
	VALUES(@GlobalAccountNo, @PreviousBalance, @Payments, @Adjustments)
	
	RETURN 
END

GO


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillFixedCharges]    Script Date: 08/05/2015 19:34:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 04 Aug 2015
-- Description:	To get the Fixed charges details
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerBillFixedCharges]
(
	 @FixedCharges BillFixedCharges READONLY
	,@MeterEffectedAmount DECIMAL(18,2)
)
RETURNS @TblCustomerFixedCharges TABLE
(
	 ChargeId INT
	,Amount DECIMAL(18,2)
)
AS
BEGIN

	DECLARE @MinChargeId INT, @MaxChargeId INT
	
	SELECT @MinChargeId = MIN(ChargeId), @MaxChargeId = MAX(ChargeId) FROM @FixedCharges
	
	WHILE(@MinChargeId <= @MaxChargeId)
		BEGIN				
			DECLARE @ChargeAmount DECIMAL(18,2) = 0
			
			SELECT @ChargeAmount = Amount FROM @FixedCharges WHERE ChargeId = @MinChargeId
			
			IF(@MeterEffectedAmount >= @ChargeAmount) 
				BEGIN
					SET @MeterEffectedAmount = @MeterEffectedAmount - @ChargeAmount
					SET @ChargeAmount = 0
				END
			ELSE
				BEGIN
					SET @ChargeAmount = @ChargeAmount - @MeterEffectedAmount
					SET @MeterEffectedAmount = 0
				END
			
			INSERT INTO @TblCustomerFixedCharges(ChargeId, Amount)
			VALUES(@MinChargeId, @ChargeAmount)
			
			SELECT @MinChargeId = MIN(ChargeId) FROM @FixedCharges WHERE ChargeId > @MinChargeId
		END
	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillsDetailsForPrint]    Script Date: 08/05/2015 19:34:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 04 Aug 2015
-- Description:	To get the Last Bill Details for Bill text file
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillsDetailsForPrint]
(
	 @GlobalAccountNo VARCHAR(50)
	,@BillMonth INT
	,@BillYear INT
)
RETURNS @TblCustomerLastBill TABLE
(
	 GlobalAccountNo VARCHAR(50)
	,PreviousBalance DECIMAL(18,2)
	,Payments DECIMAL(18,2)
	,Adjustments DECIMAL(18,2)
)
AS
BEGIN

	DECLARE @LastBilledDate DATETIME
		,@PreviousBalance DECIMAL(18,2) = 0 
		,@Payments DECIMAL(18,2) = 0 
		,@Adjustments DECIMAL(18,2) = 0 
		,@BillNo VARCHAR(50)
		
	SELECT @BillNo = BillNo, @PreviousBalance = ISNULL(NetArrears,0) FROM Tbl_CustomerBills 
	WHERE AccountNo = @GlobalAccountNo and BillMonth = @BillMonth and BillYear = @BillYear

	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = BillGeneratedDate
				,@PreviousBalance = ISNULL(TotalBillAmountWithArrears,0)
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				 @Payments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			SELECT
				@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo				
		END	
	ELSE
		BEGIN			
			SELECT
				 @Payments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE AccountNo = @GlobalAccountNo	
			
			SELECT
				@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE AccountNo = @GlobalAccountNo	
		END
		
	INSERT INTO @TblCustomerLastBill(GlobalAccountNo, PreviousBalance, Payments, Adjustments)
	VALUES(@GlobalAccountNo, @PreviousBalance, @Payments, @Adjustments)
	
	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillsDetailsForPrint_ByServiceCenter]    Script Date: 08/05/2015 19:34:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 04 Aug 2015
-- Description:	To get the Last Bill Details for Bill text file to bulk bills
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillsDetailsForPrint_ByServiceCenter]
(
	 @ServiceCenter VARCHAR(50)
	,@BillMonth INT
	,@BillYear INT
)
RETURNS @TblCustomerLastBill TABLE
(
	 GlobalAccountNo VARCHAR(50)
	,PreviousBalance DECIMAL(18,2)
	,Payments DECIMAL(18,2)
	,Adjustments DECIMAL(18,2)
)
AS
BEGIN

	DECLARE @TempCustomerBills TABLE(SNo INT IDENTITY(1,1), GlobalAccountNo VARCHAR(50))
	
	INSERT INTO @TempCustomerBills(GlobalAccountNo)
	SELECT AccountNo FROM Tbl_CustomerBills
	WHERE ServiceCenterId = @ServiceCenter and BillMonth = @BillMonth and BillYear = @BillYear

	DECLARE @SNo INT = 1, @TotalCount INT = 0
	
	SELECT @TotalCount = COUNT(0) FROM @TempCustomerBills
	
	WHILE(@SNo <= @TotalCount)
	BEGIN			
		
		DECLARE @GlobalAccountNo VARCHAR(50)
			,@LastBilledDate DATETIME
			,@PreviousBalance DECIMAL(18,2) = 0 
			,@Payments DECIMAL(18,2) = 0 
			,@Adjustments DECIMAL(18,2) = 0 
			,@BillNo VARCHAR(50)		
		
		SELECT @GlobalAccountNo = GlobalAccountNo FROM @TempCustomerBills WHERE SNo = @SNo
	
		SELECT @BillNo = BillNo, @PreviousBalance = ISNULL(NetArrears,0) FROM Tbl_CustomerBills 
		WHERE AccountNo = @GlobalAccountNo and BillMonth = @BillMonth and BillYear = @BillYear

		IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
			BEGIN
				SELECT TOP 1
					 @LastBilledDate = BillGeneratedDate
					,@PreviousBalance = ISNULL(TotalBillAmountWithArrears,0)
				FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
					AND BillNo != @BillNo
				ORDER BY BillGeneratedDate DESC
				
				SELECT
					 @Payments = SUM(ISNULL(PaidAmount,0))
				FROM Tbl_CustomerPayments
				WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo	
				
				SELECT
					@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
				FROM Tbl_BillAdjustments
				WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo				
			END	
		ELSE
			BEGIN			
				SELECT
					 @Payments = SUM(ISNULL(PaidAmount,0))
				FROM Tbl_CustomerPayments
				WHERE AccountNo = @GlobalAccountNo	
				
				SELECT
					@Adjustments = SUM(ISNULL(TotalAmountEffected,0))
				FROM Tbl_BillAdjustments
				WHERE AccountNo = @GlobalAccountNo	
			END
			
		INSERT INTO @TblCustomerLastBill(GlobalAccountNo, PreviousBalance, Payments, Adjustments)
		VALUES(@GlobalAccountNo, @PreviousBalance, @Payments, @Adjustments)	
		
		SET @SNo = @SNo + 1
	END
	
	RETURN 
END

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint_Customerwise]    Script Date: 08/05/2015 19:34:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint_Customerwise]
(  
 @GlobalAcountNumber VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (
  
   
  SELECT         
	CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  ISNULL(CB.VAT,0)  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]             
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
	 ,convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END),0) as Numeric) 
	   ) + ' ' + RC.DisplayCode AS Usage              
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     --,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,(case when  ISNULL(LP.Adjustments,0) < 0 
			then ISNULL(convert(varchar(50), ISNULL(LP.Adjustments,0)),'0.00') + ' CR ' 
			when  ISNULL(LP.Adjustments,0)  = 0 then '0.00' 
			ELSE  ISNULL(convert(varchar(50), ISNULL(LP.Adjustments,0)),'0.00') + ' DR ' END) as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate    
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     --,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance    
     ,ISNULL(convert(varchar(50),LP.PreviousBalance),'0.00') as PreviousBalance          
     ,CB.CycleId
     ,1 AS RowsEffected
      --,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
      ,ISNULL(convert(varchar(50),LP.Payments),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	,7 as [LastDueDays]
	,CONVERT(VARCHAR(11),BillGeneratedDate+7,106) as LastDueDate
	,(SELECT dbo.fn_GetCustomer_ADC(CB.Usage)) AS ADC
	,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID  
    LEFT JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
    LEFT JOIN (SELECT GlobalAccountNo 
				,PreviousBalance 
				,Payments 
				,Adjustments 
			FROM [dbo].[fn_GetCustomerLastBillsDetailsForPrint](@GlobalAcountNumber,@BillMonth,@BillYear)) AS LP ON LP.GlobalAccountNo=CB.AccountNo
     where AccountNo=@GlobalAcountNumber and BillMonth=@BillMonth and BillYear=@BillYear
  );

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 08/05/2015 19:34:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- MODIFIED BY	: NEERAJ KANOJIYA
-- DESC			: ADDED NEW FIELD FOR AVERAGE DAILY CONSUMPTION
-- DATE			: 29-MAY-15
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  ISNULL(CB.VAT,0)  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate()))) AS [MonthName] 
     ,(SELECT dbo.fn_GetMonthName(DATEPART(m, getdate())-1)) AS [BillingMonthName]           
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,  convert(varchar(50),CAST( ROUND((CASE WHEN CONVERT(BIGINT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(BIGINT,CB.Usage)END),0) as Numeric) 
	   ) + ' ' + RC.DisplayCode 
		AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     --,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
      ,case    when  ISNULL(LP.Adjustments,0)  < 0 
		then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money,  ISNULL(LP.Adjustments,0) ),1),'.00','') ) + ' CR ' 
		when  ISNULL(LP.Adjustments,0)  =0 then '0.00' 
		ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money,  ISNULL(LP.Adjustments,0) ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),CONVERT(VARCHAR(11),BD.ReadDate,106)),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00')  as LastPaidAmount
     --,ISNULL(convert(varchar(50),CB.PreviousBalance),'0.00') as PreviousBalance
     ,ISNULL(convert(varchar(50),LP.PreviousBalance),'0.00') as PreviousBalance                
     ,CB.CycleId
     ,1 AS RowsEffected
   
      --,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'0.00') AS TotalPayment
       ,ISNULL(convert(varchar(50),LP.Payments),'0.00') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
	  ,7 as [LastDueDays]
	  ,	CONVERT(VARCHAR(11),BillGeneratedDate+7,106)	 as LastDueDate
	  ,(SELECT dbo.fn_GetCustomer_ADC(CONVERT(BIGINT,CB.Usage))) AS ADC
	  ,TC1.ClassName
    FROM Tbl_CustomerBills(NoLOCK) CB    
    INNER JOIN Tbl_BillDetails(NoLOCK) BD ON CB.CustomerBillId = BD.CustomerBillID  
    LEFT JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadCodeId
    LEFT JOIN Tbl_MTariffClasses AS TC ON CB.TariffId=TC.ClassID
    LEFT JOIN Tbl_MTariffClasses AS TC1 ON TC.RefClassID=TC1.ClassID
    LEFT JOIN (SELECT GlobalAccountNo 
				,PreviousBalance 
				,Payments 
				,Adjustments 
			FROM [dbo].[fn_GetCustomerLastBillsDetailsForPrint_ByServiceCenter](@ServiceCenter,@BillMonth,@BillYear)) AS LP ON LP.GlobalAccountNo=CB.AccountNo
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );



GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges_New]    Script Date: 08/05/2015 19:34:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
ALTER FUNCTION  [dbo].[fn_GetFixedCharges_New]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
	,@GlobalAccountNumber   varchar(200)
	,@IsFirstMonthBill Bit 
	,@SetupDate DATETIME
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	DECLARE @Multiplier DECIMAL=1
	 
	 
	IF @IsFirstMonthBill =1
	BEGIN
		IF @SetupDate IS NOT NULL 
			BEGIN  
			   SET @Multiplier= DateDiff(mm,@SetupDate,DATEADD(day,-30,@MonthStartDate)  )
			   if(@Multiplier<=0)
				set @Multiplier=1
			END
	 		 
	END


	 

	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount*ISNULL(@Multiplier,0) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN 
	CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END

--------------------------------------------------------------------------------------------------


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetNormalCustomerConsumption]    Script Date: 08/05/2015 19:34:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,>
-- Description:	<Description,, Generate Normal customer consumption>
-- =============================================

--SELECT *	FROM fn_GetNormalCustomerConsumption('0001329260')

--select * from [fn_GetNormalCustomerConsumption]('0000057417',0)

   
			
			
ALTER FUNCTION [dbo].[fn_GetNormalCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50),
	@OpeningBalance decimal(18,2)	
)
RETURNS @CustomerConsumption TABLE 
(
	-- Add the column definitions for the TABLE variable here
	 GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage DECIMAL(18,2)
	,LastBillGenerated DATETIME
	,BalanceUsage INT
	,IsFromReading INT
	,CustomerBillId INT
	,PreviousBalance Decimal(18,2)
	,Multiplier INt
	,ReadDate datetime
)
AS
BEGIN
	DECLARE @ResultConsumption VARCHAR(1000),@LastBillGenerated DATETIME,@MeterMultiplier INT, @BalanceUsage INT,@CustomerBillId Int,@PreviousBalance decimal(18,2),@ReadDate Datetime,@LastBillCurrentReading varchar(50)
	
		declare @CustomerReadings TABLE (
			-- Add the column definitions for the TABLE variable here
			 GlobalAccountNumber VARCHAR(50)
			,PreviousReading VARCHAR(50)
			,PresentReading VARCHAR(50)
			,Usage DECIMAL(18,2)
			,LastBillGenerated DATETIME
			,BalanceUsage INT
			,IsFromReading INT
			,CustomerBillId INT
			,PreviousBalance Decimal(18,2)
			,Multiplier INt
			,ReadDate datetime
			,CustomerReadingId INT 
			,IsRollOver Bit
		)
	-- Add the T-SQL statements to compute the return value here
	-- If reading available
	IF EXISTS(SELECT GlobalAccountNumber FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0)
	BEGIN
		
		 
		SELECT @MeterMultiplier=(SELECT top (1)  MeterMultiplier FROM Tbl_MeterInformation(NOLOCK)    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber))    
		
		SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@BalanceUsage=BalanceUsage
		,@CustomerBillId=CustomerBillId,@PreviousBalance=PreviousBalance
		,@LastBillCurrentReading=PresentReading
		FROM Tbl_CustomerBills (NOLOCK)    
		WHERE AccountNo = @GlobalAccountNumber     
		ORDER BY CustomerBillId DESC   	
			
			IF @PreviousBalance IS NULL
				BEGIN
					select @PreviousBalance=@OpeningBalance
				END
			if @BalanceUsage IS NULL
				SET @BalanceUsage=0
			if @CustomerBillId IS NULL
				SET @CustomerBillId=0
		-- Fill the table variable with the rows for your result set
		INSERT INTO @CustomerReadings(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate
		,CustomerReadingId,IsRollOver)
		SELECT GlobalAccountNumber
			,convert(int,PreviousReading) AS PreviousReading
			,convert(int,PresentReading) AS PresentReading
			,Usage*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,ReadDate
			,CustomerReadingId
			,IsRollOver
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		
		INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate)
		SELECT GlobalAccountNumber
			,(select top 1 convert(int,PreviousReading) from @CustomerReadings order by CustomerReadingId asc)  AS PreviousReading
			,(select top 1 convert(int,PresentReading) from @CustomerReadings order by CustomerReadingId desc) AS PresentReading	   
			,sum(isnull(Usage,0))*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,Max(ReadDate)
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		group by GlobalAccountNumber
				
	
	END
	ELSE -- If reading not available
	BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 
		SELECT @IsFirstTimeCustomer=dbo.fn_IsFirstTimeCustomerForBilling(@GlobalAccountNumber)
		
		IF @IsFirstTimeCustomer=0
		BEGIN
				 
			DECLARE @AvgReading DECIMAL(18,2)
		
			SELECT @AvgReading = (case when isnull(AvgReading,0)=0 THen  ISNULL(InitialBillingKWh,0) else AvgReading end)
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
 		
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT TOP(1) AccountNo AS GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,@AvgReading AS Usage
				,BillGeneratedDate AS LastBillGenerated
				,isnull(BalanceUsage,0)
				,0 AS IsFromReading
				,CustomerBillId
				,PreviousBalance
				 
			FROM Tbl_CustomerBills (NOLOCK) 
			WHERE AccountNo = @GlobalAccountNumber 
			ORDER BY CustomerBillId DESC
		END
		ELSE
		BEGIN
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,
			PresentReading,Usage,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT GlobalAccountNumber
				,NULL AS PreviousReading
				,NULL AS PresentReading
				,case when isnull(AvgReading,0)=0 THen  InitialBillingKWh else AvgReading end AS Usage
				,0 AS  BalanceUsage 
				,0 AS IsFromReading
				,0
				,@OpeningBalance
				
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
		END		
	
	END	
	
	RETURN  
	
END



GO


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerEmailId]    Script Date: 08/05/2015 19:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
ALTER FUNCTION  [dbo].[fn_GetCustomerEmailId]  
(  
@GlobalAccountNumber VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @EmailId VARCHAR(MAX)  
  
DECLARE  @LandLordEmailId VARCHAR(MAX)
			,@TenentEmailId VARCHAR(MAX)
			,@TenentId INT=0


	SELECT @LandLordEmailId=(CASE EmailId WHEN '' THEN NULL ELSE EmailId END),@TenentId=ISNULL(TenentId,0)
	FROM CUSTOMERS.Tbl_CustomersDetail
	WHERE GlobalAccountNumber=@GlobalAccountNumber
	
	IF(@TenentId != 0)
		BEGIN
			SELECT @TenentEmailId=(CASE EmailId WHEN '' THEN NULL ELSE EmailId END)
			FROM CUSTOMERS.Tbl_CustomerTenentDetails
			WHERE TenentId=@TenentId	
		END
	
	
	SELECT @EmailId=(CASE WHEN @LandLordEmailId IS NOT NULL  aND @TenentEmailId IS NOT NULL
				THEN @LandLordEmailId +'; '+@TenentEmailId
				WHEN @LandLordEmailId IS NOT NULL
				THEN @LandLordEmailId
				WHEN @TenentEmailId IS NOT NULL
				THEN @TenentEmailId
				ELSE '0' END )
    
 RETURN @EmailId   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerMobileNo]    Script Date: 08/05/2015 19:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
ALTER FUNCTION  [dbo].[fn_GetCustomerMobileNo]  
(  
@GlobalAccountNumber VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @MobileNo VARCHAR(MAX)  
  
	--SELECT  --@MobileNo= REPLACE(CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0)))))),'-','')	 
	--	 @MobileNo= CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0))))))
	--FROM CUSTOMERS.Tbl_CustomersDetail CD
	--LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
	--WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
	
	SELECT  @MobileNo=(CONVERT(VARCHAR(50),
    (ISNULL((CASE PhoneNumber 
			WHEN '' THEN NULL 
			WHEN '-' THEN NULL 
		ELSE PhoneNumber END)
	  ,ISNULL((CASE AlternatePhoneNumber 
			WHEN '' THEN NULL 
			WHEN '-' THEN NULL 
		ELSE AlternatePhoneNumber END)
	  ,ISNULL((CASE HomeContactNumber 
			WHEN '' THEN NULL 
			WHEN '-' THEN NULL 
		ELSE HomeContactNumber END)
	  ,ISNULL((CASE BusinessContactNumber 
			WHEN '' THEN NULL 
			WHEN '-' THEN NULL 
		ELSE BusinessContactNumber END)
	  ,ISNULL((CASE OtherContactNumber 
			WHEN '' THEN NULL 
			WHEN '-' THEN NULL 
		ELSE OtherContactNumber END),0))))))))
FROM CUSTOMERS.Tbl_CustomersDetail CD
LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
WHERE CD.GlobalAccountNumber=@GlobalAccountNumber 
 
 DECLARE @pos INT
 SELECT @pos  = CHARINDEX('-', @MobileNo)
 SELECT @MobileNo = SUBSTRING(@MobileNo, @pos+1, LEN(@MobileNo)-@pos)
    
 RETURN @MobileNo   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaymentDate_Mail]    Script Date: 08/05/2015 19:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 12 Apr 2015  
-- Description: The purpose of this function is to get the Last Payment Date  
-- =============================================  
ALTER FUNCTION  [dbo].[fn_GetCustomerLastPaymentDate_Mail]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
  
 SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106) FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY CustomerPaymentID DESC
    
 RETURN @LastPaymentDate   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaidAmount_Mail]    Script Date: 08/05/2015 19:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 12 Apr 2015  
-- Description: The purpose of this function is to get the Last Payment amt   
-- =============================================  
ALTER FUNCTION  [dbo].[fn_GetCustomerLastPaidAmount_Mail]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
   ,@LastPaidAmount DECIMAL(18,2)  
  
 SELECT TOP(1)
 @LastPaidAmount = PaidAmount FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY CustomerPaymentID DESC
  
 SET @LastPaidAmount = ISNULL(@LastPaidAmount,0)  
      
 RETURN @LastPaidAmount   
  
END


GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaymentTxnId_Mail]    Script Date: 08/05/2015 19:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 28 Jul 2015  
-- Description: The purpose of this function is to get the Last Payment TxnId  
-- =============================================  
ALTER FUNCTION  [dbo].[fn_GetCustomerLastPaymentTxnId_Mail]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @TxnId VARCHAR(MAX)  
  
 SELECT TOP(1) 
 @TxnId=ISNULL(ReceiptNo,CustomerPaymentID) 
 FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY CustomerPaymentID DESC
    
 RETURN @TxnId   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------


GO


GO

/****** Object:  StoredProcedure [dbo].[USP_Update_PDFReportData]    Script Date: 08/05/2015 19:36:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Bhimaraju Vanka
-- Created Date: 05-08-2015
-- Description: Updating Report Data 
-- =============================================  
CREATE PROCEDURE [dbo].[USP_Update_PDFReportData]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
			DECLARE @Month INT 
					,@Year INT 
					,@LastMonth INT
					,@LastYear INT
					,@CreatedBy VARCHAR(50)
					
			SELECT  
				 @Month = C.value('(Month)[1]','INT')  
				,@Year = C.value('(Year)[1]','INT')   
				,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
			FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
	
	----------Customer Details-----------
			SELECT	 CB.BillYear
				,CB.BillMonth
				,CB.BillNo
				,CB.TariffId
				,CB.ServiceCenterId
				,CB.SU_ID
				,CB.BU_ID
				,CB.AccountNo
				,CPD.CustomerTypeId
				,CB.CustomerBillId
		INTO #CustomerBills
		FROM Tbl_CustomerBills CB 
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo
		WHERE CB.BillYear=@Year AND CB.BillMonth=@Month

	----------Payments Details-----------
		SELECT	 SUM(BP.PaidAmount) AS BillPaidAmount
				,CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,COUNT(0) AS NoOfStubs
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
		INTO #Payments
		FROM Tbl_CustomerBillPayments BP
		JOIN #CustomerBills CB ON CB.BillNo=BP.BillNo
		GROUP BY CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
				
		----------Adjustments Details-----------		
		SELECT	 SUM(BP.TotalAmountEffected) AS BillPaidAmount
				,CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,COUNT(0) AS NoOfStubs
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
		INTO #Adjustments
		FROM Tbl_BillAdjustments BP
		JOIN #CustomerBills CB ON BP.CustomerBillId=CB.CustomerBillId
		GROUP BY CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId

		----------Tbl_ReportBillingStatisticsBySCId Details-----------		
		UPDATE RBS SET   Payments = Payments+P.BillPaidAmount
						,RevenueCollected= RevenueCollected+P.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+P.BillPaidAmount
						,NoOfStubs= RBS.NoOfStubs+P.NoOfStubs
		FROM Tbl_ReportBillingStatisticsBySCId RBS
		JOIN #Payments P ON P.BU_ID=RBS.BU_ID
		AND P.SU_ID=RBS.SU_ID
		AND P.ServiceCenterId =RBS.SC_ID
		AND RBS.MonthId=P.BillMonth
		AND RBS.YearId=P.BillYear
		AND RBS.CustomerTypeId=P.CustomerTypeId
		
		UPDATE RBS SET   Payments = Payments+A.BillPaidAmount
						,RevenueCollected= RevenueCollected+A.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+A.BillPaidAmount
						,NoOfStubs= RBS.NoOfStubs+A.NoOfStubs
		FROM Tbl_ReportBillingStatisticsBySCId RBS
		JOIN #Adjustments A ON A.BU_ID=RBS.BU_ID
		AND A.SU_ID=RBS.SU_ID
		AND A.ServiceCenterId =RBS.SC_ID
		AND RBS.MonthId=A.BillMonth
		AND RBS.YearId=A.BillYear
		AND RBS.CustomerTypeId=A.CustomerTypeId
		
		----------Tbl_ReportBillingStatisticsByTariffId Updation-----------		
		UPDATE RBST SET   Payments = Payments+P.BillPaidAmount
						,RevenueCollected= RevenueCollected+P.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+P.BillPaidAmount
						,NoOfStubs= RBST.NoOfStubs+P.NoOfStubs
		FROM Tbl_ReportBillingStatisticsByTariffId RBST
		JOIN #Payments P ON P.BU_ID=RBST.BU_ID
		AND RBST.MonthId=P.BillMonth
		AND RBST.YearId=P.BillYear
		AND RBST.TariffId=P.TariffId
		
		UPDATE RBST SET   Payments = Payments+A.BillPaidAmount
						,RevenueCollected= RevenueCollected+A.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+A.BillPaidAmount
						,NoOfStubs= RBST.NoOfStubs+A.NoOfStubs
		FROM Tbl_ReportBillingStatisticsByTariffId RBST
		JOIN #Adjustments A ON A.BU_ID=RBST.BU_ID
		AND RBST.MonthId=A.BillMonth
		AND RBST.YearId=A.BillYear
		AND RBST.TariffId=A.TariffId		
		

		DROP TABLE #CustomerBills
		DROP TABLE #Payments
		DROP TABLE #Adjustments
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillDetailsPdfGen]    Script Date: 08/05/2015 19:36:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Neeraj Kanojiya
-- Modified Date: 3-Aug-2015
-- Description: Inserting Report Data 
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBillDetailsPdfGen]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
----------------------------------------------------------------------------------------
--				Variable Decleration
----------------------------------------------------------------------------------------
	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		,@index INT =0
		,@rwCount INT 
		,@AccounNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50) = 0
		,@OutStanding VARCHAR(50) = 0
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		,@BulkEmails VARCHAR(MAX) 
----------------------------------------------------------------------------------------
--				Deserializtion of input
----------------------------------------------------------------------------------------		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  		
----------------------------------------------------------------------------------------
--				Declaration of temp tables
----------------------------------------------------------------------------------------		
	CREATE TABLE #AccountNoList(RowNum INT identity(1,1)
								,GAN varchar(50))
								
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50)
										, BillMonth VARCHAR(20)
										, Consumption VARCHAR(20)
										, ADC VARCHAR(20)
										, CurrentDemand VARCHAR(20)
										, TotalBillPayable VARCHAR(24)
										, BillingType VARCHAR(10)
										,GAN VARCHAR(50))
										
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50)
									,CurrentReadingDate VARCHAR(20)
									,CurrentReading VARCHAR(20)
									,PreviousReadingDate VARCHAR(20)
									,PreviousReading VARCHAR(20)
									,Multiplier INT, Consumption VARCHAR(20)
									,GAN VARCHAR(50))
									
	CREATE TABLE #EmpDetails (   AccounNo VARCHAR(50)
								,OldAccountNo VARCHAR(50)
								,Name VARCHAR(200)
								,Phase VARCHAR(50)
								,BillingAddress VARCHAR(500)
								,ServiceAddress VARCHAR(500)
								,PhoneNo VARCHAR(20)
								,EmailId VARCHAR(50)
								,MeterNo VARCHAR(50)
								,BusinessUnit VARCHAR(50)
								,ServiceUnit VARCHAR(50)
								,ServiceCenter VARCHAR(50)
								,BookGroup VARCHAR(50)
								,BookNo VARCHAR(50)
								,PoleNo VARCHAR(50)
								,RouteSequense VARCHAR(50)
								,ConnectionType VARCHAR(50)
								,TariffCategory VARCHAR(50)
								,TariffType VARCHAR(50)
								,BillDate VARCHAR(20)
								,BillType VARCHAR(10)
								,BillNo VARCHAR(50)
								,BillRemarks VARCHAR(200)
								,NetArrears VARCHAR(50) 
								,OutStanding VARCHAR(50) 
								,Payments VARCHAR(50)
								,TotalBillAmountWithTax VARCHAR(50)
								,NetAmountPayble VARCHAR(50)
								,DueDate VARCHAR(20)
								,LastPaidAmount VARCHAR(20)
								,LastPaidDate VARCHAR(20)
								,PaidMeterAmount VARCHAR(20)
								,PaidMeterDeduction VARCHAR(20)
								,PaidMeterBalance VARCHAR(20)
								,Consumption VARCHAR(20)
								,TariffRate VARCHAR(20)
								,EnergyCharges VARCHAR(20)
								,FixedCharges VARCHAR(20)
								,TaxPercentage DECIMAL(18,2)
								,Tax VARCHAR(20)
								,TotalBillAmount VARCHAR(50)
								,BillPeriod VARCHAR(100)
								,IsCAPMIMeter BIT
								,LastBilledDate VARCHAR(50)
								,LastBilledNo VARCHAR(50)
								,LastBillPayments DECIMAL(18,2)
								,LastBillAdjustments DECIMAL(18,2)
								,CustomerBillId INT
								,PaidMeterPayments DECIMAL(18,2)
								,PaidMeterAdjustments DECIMAL(18,2)
								,InitialBillingKWh VARCHAR(20)
								,BulkEmails VARCHAR(MAX))		
	
	CREATE TABLE #BillCharges (FixedChargeId int
								,FixedChargeName varchar(max)
								,FixedChargeAmount DECIMAL(18,2)
								,GAN varchar(50))												
					
----------------------------------------------------------------------------------------
--				Account list adding for  bill month and year
----------------------------------------------------------------------------------------	
	INSERT INTO #AccountNoList(GAN)	
	select AccountNo from Tbl_CustomerBills where BillMonth=@Month and BillYear=@year	
----------------------------------------------------------------------------------------
--				Loop starts for each customer by GAN
----------------------------------------------------------------------------------------	
	
	SELECT COUNT(0) FROM #AccountNoList
	
	SET @rwCount=(SELECT MAX(RowNum) FROM #AccountNoList) 	
					
	WHILE(@index<@rwCount) --loop
		BEGIN
		SET @index=@index+1
		SET @GlobalAccountNo=(SELECT GAN FROM #AccountNoList WHERE RowNum=@index)
		SELECT
			 @AccounNo = U.GlobalAccountNumber
			,@OldAccountNo = U.OldAccountNo
			,@Name = U.FullName
			,@Phase = U.Phase
			,@ServiceAddress = U.ServiceAddress
			,@BillingAddress = U.BillingAddress
			,@PhoneNo = U.ContactNo
			,@EmailId = U.EmailId
			,@BulkEmails=dbo.fn_GetCustomerEmailId(U.GlobalAccountNumber)
			,@MeterNo = U.MeterNumber
			,@BusinessUnit = U.BusinessUnitName
			,@ServiceUnit = U.ServiceUnitName
			,@ServiceCenter = U.ServiceCenterName
			,@BookGroup = U.CycleName
			,@BookNo = U.BookNo
			,@PoleNo = U.PoleNo
			,@RouteSequense = U.RouteName
			,@ConnectionType = U.CustomerType
			,@TariffCategory = U.TariffCategory
			,@TariffType = U.TariffName
			,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
			,@BillType = RC.DisplayCode
			,@BillNo = CB.BillNo
			,@BillRemarks = CB.Remarks
			--,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
			,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
			,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
			,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
			,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
			,@IsCAPMIMeter = U.IsCAPMIMeter
			,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
			,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
			,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
			,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
			,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
			,@TaxPercentage = ISNULL(CB.VATPercentage,0)
			,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
			,@CustomerBillId = CB.CustomerBillId
			,@InitialBillingKWh = U.InitialBillingKWh
			,@OutStanding = CONVERT(VARCHAR,(CAST(ISNULL(U.OutStandingAmount,0) AS MONEY)), 1)
		FROM UDV_CustomerDetailsForBillPDF U
		INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
				AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		INSERT INTO #EmpDetails(
								 	AccounNo 
									,OldAccountNo 
									,Name 
									,Phase 
									,ServiceAddress
									,BillingAddress
									,PhoneNo
									,EmailId 
									,BulkEmails
									,MeterNo 
									,BusinessUnit 
									,ServiceUnit 
									,ServiceCenter 
									,BookGroup 
									,BookNo 
									,PoleNo 
									,RouteSequense 
									,ConnectionType 
									,TariffCategory 
									,TariffType 
									,BillDate 
									,BillType 
									,BillNo 
									,BillRemarks 
									,TotalBillAmount 
									,TotalBillAmountWithTax
									,NetAmountPayble 
									,DueDate 
									,IsCAPMIMeter 
									,PaidMeterBalance 
									,Consumption 
									,TariffRate 
									,EnergyCharges 
									,FixedCharges 
									,TaxPercentage 
									,Tax 
									,CustomerBillId 
									,InitialBillingKWh 
									,OutStanding 
								)
					VALUES
								(
									 @AccounNo 
									,@OldAccountNo 
									,@Name 
									,@Phase 
									,@ServiceAddress
									,@BillingAddress
									,@PhoneNo
									,@EmailId 
									,@BulkEmails
									,@MeterNo 
									,@BusinessUnit 
									,@ServiceUnit 
									,@ServiceCenter 
									,@BookGroup 
									,@BookNo 
									,@PoleNo 
									,@RouteSequense 
									,@ConnectionType 
									,@TariffCategory 
									,@TariffType 
									,@BillDate 
									,@BillType 
									,@BillNo 
									,@BillRemarks 
									,@TotalBillAmount 
									,@TotalBillAmountWithTax
									,@NetAmountPayble 
									,@DueDate 
									,@IsCAPMIMeter 
									,@PaidMeterBalance
									,@Consumption 
									,@TariffRate 
									,@EnergyCharges 
									,@FixedCharges 
									,@TaxPercentage 
									,@Tax 
									,@CustomerBillId 
									,@InitialBillingKWh
									,@OutStanding 
								
								)
		IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
			BEGIN
				SELECT TOP 1
					 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
					,@LastBilledNo = BillNo
					,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(TotalBillAmountWithArrears,0) AS MONEY)), 1)
				FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
					AND BillNo != @BillNo
				ORDER BY BillGeneratedDate DESC
				
				SELECT
					 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
				FROM Tbl_CustomerPayments
				WHERE CONVERT(DATETIME,RecievedDate) >= CONVERT(DATETIME,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo	
				
				SELECT
					@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
				FROM Tbl_BillAdjustments
				WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo	
				
				--SELECT
				--	@LastBillPayments = SUM(ISNULL(PaidAmount,0))
				--FROM Tbl_CustomerBillPayments
				--WHERE CustomerBillId = @CustomerBillId
				
				--SELECT
				--	@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
				--FROM Tbl_BillAdjustments
				--WHERE CustomerBillId = @CustomerBillId
		
			END
		
		SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
		
		SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
		
		SELECT 
			 TOP 1
			 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
			,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
		FROM Tbl_CustomerPayments 
		WHERE AccountNo = @GlobalAccountNo
		ORDER BY RecievedDate DESC
		
		IF(@IsCAPMIMeter = 1)
			BEGIN
				SELECT TOP 1 
					@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
				FROM Tbl_PaidMeterDetails
				WHERE AccountNo = @GlobalAccountNo
				AND MeterNo = @MeterNo AND ActiveStatusId = 1
				ORDER BY MeterId DESC
				
				SELECT
					@PaidMeterPayments = SUM(ISNULL(Amount,0))
				FROM Tbl_PaidMeterPaymentDetails
				WHERE AccountNo = @GlobalAccountNo
				AND MeterNo = @MeterNo
				
				SELECT
					@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
				FROM Tbl_PaidMeterAdjustmentDetails
				WHERE AccountNo = @GlobalAccountNo
				AND MeterNo = @MeterNo
				
				SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
				
			END
			
		
		
		INSERT INTO @TableLastConsumption
		(
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
			,GAN
		)
		SELECT TOP 5
			 CB.BillNo
			,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
			,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
			,''
			,''
			,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
			,RC.DisplayCode
			,CB.AccountNo
		FROM Tbl_CustomerBills CB
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
			AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
		ORDER BY CB.BillGeneratedDate DESC	
			
		INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
		,GAN
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		--,ISNULL((SELECT TOP 1 
		--		REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
		--	FROM Tbl_CustomerReadings CR3
		--	WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
		--		AND CR3.CustomerReadingId < CR.CustomerReadingId
		--	ORDER BY CR3.CustomerReadingId DESC),REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', ''))
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', '')
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
		,CR.GlobalAccountNumber
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC	
	
	INSERT INTO #BillCharges(	FixedChargeId
								,FixedChargeName
								,FixedChargeAmount
								,GAN
							)
	SELECT
			 CF.FixedChargeId AS FixedChargeId
			,C.ChargeName AS FixedChargeName
			,CONVERT(VARCHAR,(CAST(ISNULL(CF.Amount,0) AS MONEY)), 1) AS FixedChargeAmount
			,@GlobalAccountNo			
		FROM Tbl_CustomerBillFixedCharges CF
		INNER JOIN Tbl_MChargeIds C ON CF.FixedChargeId = C.ChargeId AND IsAcitve = 1
			AND CF.CustomerBillId = @CustomerBillId
	END
----------------------------------------------------------------------------------------
--				Loop ends and select details from temp tables
----------------------------------------------------------------------------------------		

		SELECT * FROM #EmpDetails
		ORDER BY AccounNo

		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
			,GAN
		FROM @TableLastConsumption
		ORDER BY GAN

		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
			,GAN
		FROM @TableLastReadings
		ORDER BY GAN

		SELECT * FROM #BillCharges
 
	DROP TABLE #AccountNoList
	DROP TABLE #EmpDetails
	DROP TABLE #BillCharges
END


GO


GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForFunctionAccessPermission]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 26-06-2015
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying Function Permissions
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForFunctionAccessPermission]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=5)
	BEGIN
		SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')		
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=15)
	BEGIN
		--IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApproveStatusId IN(1,4))=0)--Faiz-ID103
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=16)
	BEGIN
		
			IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
END


GO

/****** Object:  StoredProcedure [dbo].[USP_IsRequestExistForApprovalLevels]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		T.Karthik
-- Create date: 28-11-2014
-- Description:	The purpose of this procedure is to check any requests are pending before
--				modifying approval levels
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsRequestExistForApprovalLevels]
(
	@XmlDoc xml
)
AS
BEGIN 
	DECLARE @FunctionId INT, @BU_ID VARCHAR(50)
	SELECT
		@FunctionId=C.value('(FunctionId)[1]','INT')
		,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ApprovalBe') as T(C)
	
	IF(@FunctionId=1)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=2)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_PaymentsLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=3)
		BEGIN
			IF ((SELECT COUNT(0) FROM Tbl_LCustomerTariffChangeRequest A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
				SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
			ELSE
				SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
		END
	ELSE IF(@FunctionId=4)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_BookNoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=6)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerAddressChangeLog_New A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=7)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerMeterInfoChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=8)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=9)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerActiveStatusChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.AccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=10)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerTypeChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=11)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_AssignedMeterLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=12)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_ReadToDirectCustomerActivityLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNo=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END
	ELSE IF(@FunctionId=13)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_CustomerReadingApprovalLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=14)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_DirectCustomersAverageChangeLogs A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApproveStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE IF(@FunctionId=15)
	BEGIN
		--IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApprovalStatusId IN(1,4))=0)
		IF ((SELECT COUNT(0) FROM CUSTOMERS.Tbl_ApprovalRegistration WHERE ApproveStatusId IN(1,4))=0)--Faiz-ID103
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	--ELSE IF(@FunctionId=16)
	--BEGIN
	--	IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog WHERE ApprovalStatusId IN(1,4))=0)
	--		SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
	--	ELSE
	--		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	--END
	ELSE IF(@FunctionId=16)
	BEGIN
		IF ((SELECT COUNT(0) FROM Tbl_PresentReadingAdjustmentApprovalLog A
			INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON A.GlobalAccountNumber=CPD.GlobalAccountNumber
			INNER JOIN UDV_BookNumberDetails BND ON CPD.BookNo=BND.BookNo AND BND.BU_ID=@BU_ID WHERE A.ApprovalStatusId IN(1,4))=0)
			SELECT 1 AS IsSuccess FOR XML PATH('ApprovalBe')
		ELSE
			SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	END	
	ELSE
		SELECT 0 AS IsSuccess FOR XML PATH('ApprovalBe')
	
END
-------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustmentChangeApproval]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 02-05-2015
-- Description: Update type change Details of a customer after approval  
-- Modified By: NEERAJ KANOJIYA
-- Modified Date: 26-JUNE-2015
-- Description: @AccountNo was incorrect in update statement for outstanding, modified with @GlobalAccountNo.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_BillAdjustmentChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @PresentRoleId INT = NULL
		,@NextRoleId INT = NULL
		,@CurrentLevel INT		
		,@IsFinalApproval BIT = 0
		,@AdjustmentLogId INT
		,@ApprovalStatusId INT
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@Details VARCHAR(200)
		,@BU_ID VARCHAR(50)
DECLARE @OutStandingAmount DECIMAL(18,2)

	SELECT  
		 @AdjustmentLogId = C.value('(AdjustmentLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details = C.value('(Reason)[1]','VARCHAR(200)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('ChangeCustomerTypeBE') as T(C)
	
	
	DECLARE 	@GlobalAccountNo VARCHAR(50) ,
				@AccountNo VARCHAR(50) ,
				@BillNumber VARCHAR(50) ,
				@BillAdjustmentTypeId INT ,
				@MeterNumber VARCHAR(20) ,
				@PreviousReading VARCHAR(20) ,
				@CurrentReadingAfterAdjustment VARCHAR(50) ,
				@CurrentReadingBeforeAdjustment VARCHAR(50) ,
				@AdjustedUnits DECIMAL(18, 2) ,
				@TaxEffected DECIMAL(18, 2) ,
				@TotalAmountEffected DECIMAL(18, 2) ,
				@AmountEffected DECIMAL(18, 2) ,
				@EnergryCharges DECIMAL(18, 2) ,
				@AdditionalCharges DECIMAL(18, 2) ,
				@Remarks VARCHAR(MAX) ,
				@CreatedBy VARCHAR(50) ,
				@CreatedDate DATETIME ,
				@ModifedBy VARCHAR(50) ,
				@ModifiedDate DATETIME,
				@BillAdjustmentId INT,
				@BatchId INT
				
SELECT
	 @GlobalAccountNo				=GlobalAccountNumber				
	,@AccountNo							=AccountNo  
	,@BillNumber						=BillNumber  
	,@BillAdjustmentTypeId				=BillAdjustmentTypeId  
	,@MeterNumber						=MeterNumber  
	,@PreviousReading					=PreviousReading  
	,@CurrentReadingAfterAdjustment		=CurrentReadingAfterAdjustment 
	,@CurrentReadingBeforeAdjustment	=CurrentReadingBeforeAdjustment
	,@AdjustedUnits						=AdjustedUnits  
	,@TaxEffected						=TaxEffected  
	,@TotalAmountEffected				=TotalAmountEffected  
	,@EnergryCharges					=EnergryCharges  
	,@AdditionalCharges					=AdditionalCharges  
	,@Remarks							=Remarks  
	,@CreatedBy							=CreatedBy  
	,@CreatedDate						=CreatedDate  
	,@ModifedBy							=ModifedBy  
	,@ModifiedDate						=ModifiedDate
	,@BatchId							=BatchId
	,@AmountEffected					=(TotalAmountEffected - TaxEffected)
	FROM Tbl_AdjustmentsLogs
	WHERE AdjustmentLogId=@AdjustmentLogId
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			
			DECLARE @TotalAmountEffectedToCal DECIMAL(18,2) = 0, @TotalAdjustedUnits BIGINT = 0
			DECLARE @TotalAdditionalChanrges DECIMAL(18,2) = 0, @TotalConsumption BIGINT = 0
			
			SELECT 
				 @TotalAdditionalChanrges = ISNULL(NetFixedCharges,0)
				,@TotalConsumption = CONVERT(BIGINT,ISNULL(Usage,0))
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber
			
			SELECT 
				 @TotalAmountEffectedToCal = SUM(ISNULL(AmountEffected,0))
			FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber AND BillAdjustmentType = 2
			
			SELECT 
				 @TotalAdjustedUnits = SUM(ISNULL(AdjustedUnits,0))
			FROM Tbl_BillAdjustments WHERE AccountNo = @GlobalAccountNo 
			AND CustomerBillId = @BillNumber AND BillAdjustmentType = 3
			
			IF(ISNULL(@TotalAdditionalChanrges,0) < (ISNULL(@AmountEffected,0) + ISNULL(@TotalAmountEffectedToCal,0)) AND @BillAdjustmentTypeId = 2)
				BEGIN
					SELECT 0 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
			ELSE IF(ISNULL(@TotalConsumption,0) < (ISNULL(@AdjustedUnits,0) + ISNULL(@TotalAdjustedUnits,0)) AND @BillAdjustmentTypeId = 3)
				BEGIN
					SELECT 0 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
			ELSE 
				BEGIN
					DECLARE @CurrentRoleId INT
					SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
						BEGIN
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--				RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
							--						WHERE AdjustmentLogId = @AdjustmentLogId))

							--SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
							--FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
							
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
													WHERE AdjustmentLogId = @AdjustmentLogId)

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
										FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)
							DECLARE @FinalApproval BIT

							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
								BEGIN
										SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
								END
							ELSE
								BEGIN
									DECLARE @UserDetailedId INT
									SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
									SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
								END			
							
							
							IF(@FinalApproval =1)
								BEGIN -- If Approval levels are there and If it is final approval then update tariff
								
									UPDATE Tbl_AdjustmentsLogs 
									SET   -- Updating Request with Level Roles & Approval status 
										 ModifedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApproveStatusId = @ApprovalStatusId
										,CurrentApprovalLevel=0
										,IsLocked = 1
									WHERE GlobalAccountNumber = @GlobalAccountNo 
									AND AdjustmentLogId = @AdjustmentLogId 

									INSERT INTO Tbl_BillAdjustments(
										 AccountNo    
										 --,CustomerId    
										 ,AmountEffected   
										 ,BillAdjustmentType    
										 ,TotalAmountEffected
										 ,TaxEffected
										 ,AdjustedUnits
										 ,CustomerBillId
										 ,ApprovedBy
										 ,CreatedBy
										 ,CreatedDate 
										 ,BatchNo  
										 ,Remarks
										 )           
										VALUES(         
										  @GlobalAccountNo    
										 --,@CustomerId    
										 ,@AmountEffected
										 ,@BillAdjustmentTypeId   
										 ,@TotalAmountEffected
										 ,@TaxEffected
										 ,@AdjustedUnits
										 ,@BillNumber
										 ,@ModifedBy
										 ,@CreatedBy
										 ,@CreatedDate
										 ,@BatchId 
										 ,@Remarks
										 )      
									SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
									
									INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges,PreviousReading,CurrentReadingAfterAdjustment,AdditionalCharges,CurrentReadingBeforeAdjustment,AdditionalChargesID)
									SELECT @BillAdjustmentId,@EnergryCharges,@PreviousReading,@CurrentReadingAfterAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN @AmountEffected ELSE NULL END),@CurrentReadingBeforeAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN 1 ELSE 0 END)
									
									----OutStanding Amount is Updating Start
									
									--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
									--WHERE GlobalAccountNumber=@GlobalAccountNo
									
									--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
									
									--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
									--SET OutStandingAmount=@OutStandingAmount
									--WHERE GlobalAccountNumber=@GlobalAccountNo
									----OutStanding Amount is Updating End
									
									UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
										,ModifedBy=@ModifiedBy
										,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
									
									INSERT INTO Tbl_Audit_AdjustmentsLogs 
										(	 AdjustmentLogId
											,GlobalAccountNumber 
											,AccountNo 
											,BillNumber 
											,BillAdjustmentTypeId  
											,MeterNumber 
											,PreviousReading 
											,CurrentReadingAfterAdjustment 
											,CurrentReadingBeforeAdjustment 
											,AdjustedUnits 
											,TaxEffected 
											,TotalAmountEffected 
											,EnergryCharges 
											,AdditionalCharges 
											,PresentApprovalRole
											,NextApprovalRole  
											,ApproveStatusId  
											,Remarks
											,CreatedBy 
											,CreatedDate  
											,ModifedBy 
											,ModifiedDate
										)
									VALUES (@AdjustmentLogId
											,@GlobalAccountNo
											,@AccountNo 
											,@BillNumber 
											,@BillAdjustmentTypeId  
											,@MeterNumber 
											,@PreviousReading 
											,@CurrentReadingAfterAdjustment 
											,@CurrentReadingBeforeAdjustment 
											,@AdjustedUnits 
											,@TaxEffected 
											,@TotalAmountEffected 
											,@EnergryCharges 
											,@AdditionalCharges 
											,@PresentRoleId
											,@NextRoleId
											,@ApprovalStatusId
											,@Remarks
											,@CreatedBy 
											,@CreatedDate  
											,@ModifedBy 
											,dbo.fn_GetCurrentDateTime())
										
								END
							ELSE -- Not a final approval
								BEGIN
									
									UPDATE Tbl_AdjustmentsLogs 
									SET   -- Updating Request with Level Roles
										 ModifedBy = @ModifiedBy
										,ModifiedDate = dbo.fn_GetCurrentDateTime()
										,PresentApprovalRole = @PresentRoleId
										,NextApprovalRole = @NextRoleId 
										,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
										,CurrentApprovalLevel = CurrentApprovalLevel+1
										,IsLocked = 1
									WHERE GlobalAccountNumber = @GlobalAccountNo 
									AND AdjustmentLogId = @AdjustmentLogId
									
								END
						END
					ELSE 
						BEGIN -- No Approval Levels are there
							
								UPDATE Tbl_AdjustmentsLogs 
								SET   -- Updating Request with Level Roles & Approval status 
									 ModifedBy = @ModifiedBy
									,ModifiedDate = dbo.fn_GetCurrentDateTime()
									,PresentApprovalRole = @PresentRoleId
									,NextApprovalRole = @NextRoleId 
									,ApproveStatusId = @ApprovalStatusId
									,CurrentApprovalLevel= 0
									,IsLocked=1
								WHERE GlobalAccountNumber = @GlobalAccountNo 
								AND AdjustmentLogId = @AdjustmentLogId 

								INSERT INTO Tbl_BillAdjustments(
								 AccountNo    
								 --,CustomerId    
								 ,AmountEffected   
								 ,BillAdjustmentType    
								 ,TotalAmountEffected
								 ,TaxEffected
								 ,AdjustedUnits
								 ,CustomerBillId
								 ,ApprovedBy
								 ,CreatedBy
								 ,CreatedDate 
								 ,BatchNo  
								 ,Remarks
								 )           
								VALUES(         
								  @GlobalAccountNo    
								 --,@CustomerId    
								 ,@AmountEffected
								 ,@BillAdjustmentTypeId   
								 ,@TotalAmountEffected
								 ,@TaxEffected
								 ,@AdjustedUnits
								 ,@BillNumber
								 ,@ModifedBy
								 ,@CreatedBy
								 ,@CreatedDate
								 ,@BatchId 
								 ,@Remarks
								 )      
								SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
			   
								INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges,PreviousReading,CurrentReadingAfterAdjustment,AdditionalCharges,CurrentReadingBeforeAdjustment,AdditionalChargesID)
								SELECT @BillAdjustmentId,@EnergryCharges,@PreviousReading,@CurrentReadingAfterAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN @AmountEffected ELSE NULL END),@CurrentReadingBeforeAdjustment,(CASE WHEN @BillAdjustmentTypeId = 2 THEN 1 ELSE 0 END)
														
								----OutStanding Amount is Updating Start
									
								--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
								--	WHERE GlobalAccountNumber=@GlobalAccountNo
									
								--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
									
								--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
								--	SET OutStandingAmount=@OutStandingAmount
								--	WHERE GlobalAccountNumber=@GlobalAccountNo
								--	--OutStanding Amount is Updating End
								
								UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
									,ModifedBy=@ModifiedBy
									,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@GlobalAccountNo
								
								INSERT INTO Tbl_Audit_AdjustmentsLogs 
										(	 AdjustmentLogId
											,GlobalAccountNumber 
											,AccountNo 
											,BillNumber 
											,BillAdjustmentTypeId  
											,MeterNumber 
											,PreviousReading 
											,CurrentReadingAfterAdjustment 
											,CurrentReadingBeforeAdjustment 
											,AdjustedUnits 
											,TaxEffected 
											,TotalAmountEffected 
											,EnergryCharges 
											,AdditionalCharges 
											,PresentApprovalRole
											,NextApprovalRole  
											,ApproveStatusId  
											,Remarks
											,CreatedBy 
											,CreatedDate  
											,ModifedBy 
											,ModifiedDate
										)
									VALUES (@AdjustmentLogId
											,@GlobalAccountNo
											,@AccountNo 
											,@BillNumber 
											,@BillAdjustmentTypeId  
											,@MeterNumber 
											,@PreviousReading 
											,@CurrentReadingAfterAdjustment 
											,@CurrentReadingBeforeAdjustment 
											,@AdjustedUnits 
											,@TaxEffected 
											,@TotalAmountEffected 
											,@EnergryCharges 
											,@AdditionalCharges 
											,@PresentRoleId
											,@NextRoleId
											,@ApprovalStatusId
											,@Remarks
											,@CreatedBy 
											,@CreatedDate  
											,@ModifedBy 
											,dbo.fn_GetCurrentDateTime())					
					END

					SELECT 1 As IsSuccess  
					FOR XML PATH('ChangeCustomerTypeBE'),TYPE
				END
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN						
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
							--									WHERE AdjustmentLogId = @AdjustmentLogId))
							
								SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_AdjustmentsLogs 
																WHERE AdjustmentLogId = AdjustmentLogId )
								
							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
						WHERE AdjustmentLogId = @AdjustmentLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_AdjustmentsLogs 
			SET   
				 ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE GlobalAccountNumber = @GlobalAccountNo  
			AND AdjustmentLogId = @AdjustmentLogId
			
			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
		END 
		
	------------------------- Old One it was not changed ---------------------
	
	
	--IF(@ApprovalStatusId = 2)  -- Request Approved
	--	BEGIN  
	--		DECLARE @CurrentRoleId INT
	--		SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

	--		IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId And IsActive = 1) -- If Approval Levels Exists
	--			BEGIN
	--				SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--								RoleId = (SELECT PresentApprovalRole FROM Tbl_AdjustmentsLogs 
	--										WHERE AdjustmentLogId = @AdjustmentLogId))

	--				SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
	--				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

	--				IF((SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId) = 1)
	--					BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles & Approval status 
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = @ApprovalStatusId
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId 

	--						INSERT INTO Tbl_BillAdjustments(
	--							 AccountNo    
	--							 --,CustomerId    
	--							 ,AmountEffected   
	--							 ,BillAdjustmentType    
	--							 ,TotalAmountEffected
	--							 ,ApprovedBy
	--							 ,CreatedBy
	--							 ,CreatedDate   
	--							 )           
	--							VALUES(         
	--							  @GlobalAccountNo    
	--							 --,@CustomerId    
	--							 ,@TotalAmountEffected   
	--							 ,@BillAdjustmentTypeId   
	--							 ,@TotalAmountEffected
	--							 ,@ModifedBy
	--							 ,@ModifiedBy
	--							 ,GETDATE()  
	--							 )      
	--						SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
		   
	--						INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--						SELECT @BillAdjustmentId,@TotalAmountEffected
							
	--						----OutStanding Amount is Updating Start
							
	--						--SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--						--SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--						--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--						--SET OutStandingAmount=@OutStandingAmount
	--						--WHERE GlobalAccountNumber=@GlobalAccountNo
	--						----OutStanding Amount is Updating End
							
	--						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--							,ModifedBy=@ModifiedBy
	--							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
	--						INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())
								
	--					END
	--				ELSE -- Not a final approval
	--					BEGIN
							
	--						UPDATE Tbl_AdjustmentsLogs 
	--						SET   -- Updating Request with Level Roles
	--							 ModifedBy = @ModifiedBy
	--							,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--							,PresentApprovalRole = @PresentRoleId
	--							,NextApprovalRole = @NextRoleId 
	--							,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
	--						WHERE GlobalAccountNumber = @GlobalAccountNo 
	--						AND AdjustmentLogId = @AdjustmentLogId
							
	--					END
	--			END
	--		ELSE 
	--			BEGIN -- No Approval Levels are there
					
	--					UPDATE Tbl_AdjustmentsLogs 
	--					SET   -- Updating Request with Level Roles & Approval status 
	--						 ModifedBy = @ModifiedBy
	--						,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--						,PresentApprovalRole = @PresentRoleId
	--						,NextApprovalRole = @NextRoleId 
	--						,ApproveStatusId = @ApprovalStatusId
	--					WHERE GlobalAccountNumber = @GlobalAccountNo 
	--					AND AdjustmentLogId = @AdjustmentLogId 

	--					INSERT INTO Tbl_BillAdjustments(
	--						 AccountNo    
	--						 --,CustomerId    
	--						 ,AmountEffected   
	--						 ,BillAdjustmentType    
	--						 ,TotalAmountEffected 
	--						 ,CreatedBy
	--						 ,ApprovedBy
	--						 ,CreatedDate  
	--						 )           
	--						VALUES(         
	--						  @GlobalAccountNo    
	--						 --,@CustomerId    
	--						 ,@TotalAmountEffected   
	--						 ,@BillAdjustmentTypeId   
	--						 ,@TotalAmountEffected 
	--						 ,@ModifiedBy
	--						 ,@ModifiedBy
	--						 ,GETDATE() 
	--						 )      
	--					SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
	   
	--					INSERT INTO Tbl_BillAdjustmentDetails(BillAdjustmentId,EnergryCharges)
	--					SELECT @BillAdjustmentId,@TotalAmountEffected
						
	--					----OutStanding Amount is Updating Start
							
	--					--	SELECT @OutStandingAmount=OutStandingAmount FROM CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
							
	--					--	SET @OutStandingAmount=@OutStandingAmount+@TotalAmountEffected
							
	--					--	UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
	--					--	SET OutStandingAmount=@OutStandingAmount
	--					--	WHERE GlobalAccountNumber=@GlobalAccountNo
	--					--	--OutStanding Amount is Updating End
						
	--					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
	--						,ModifedBy=@ModifiedBy
	--						,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
						
	--					INSERT INTO Tbl_Audit_AdjustmentsLogs 
	--							(	 AdjustmentLogId
	--								,GlobalAccountNumber 
	--								,AccountNo 
	--								,BillNumber 
	--								,BillAdjustmentTypeId  
	--								,MeterNumber 
	--								,PreviousReading 
	--								,CurrentReadingAfterAdjustment 
	--								,CurrentReadingBeforeAdjustment 
	--								,AdjustedUnits 
	--								,TaxEffected 
	--								,TotalAmountEffected 
	--								,EnergryCharges 
	--								,AdditionalCharges 
	--								,PresentApprovalRole
	--								,NextApprovalRole  
	--								,ApproveStatusId  
	--								,Remarks
	--								,CreatedBy 
	--								,CreatedDate  
	--								,ModifedBy 
	--								,ModifiedDate
	--							)
	--						VALUES (@AdjustmentLogId
	--								,@GlobalAccountNo
	--								,@AccountNo 
	--								,@BillNumber 
	--								,@BillAdjustmentTypeId  
	--								,@MeterNumber 
	--								,@PreviousReading 
	--								,@CurrentReadingAfterAdjustment 
	--								,@CurrentReadingBeforeAdjustment 
	--								,@AdjustedUnits 
	--								,@TaxEffected 
	--								,@TotalAmountEffected 
	--								,@EnergryCharges 
	--								,@AdditionalCharges 
	--								,@PresentRoleId
	--								,@NextRoleId
	--								,@ApprovalStatusId
	--								,@Remarks
	--								,@CreatedBy 
	--								,@CreatedDate  
	--								,@ModifedBy 
	--								,dbo.fn_GetCurrentDateTime())					
	--		END

	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE
	--	END  
	--ELSE  
	--	BEGIN  -- Request Not Approved
	--		IF(@ApprovalStatusId = 3) -- Request is Rejected
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
	--					BEGIN -- Approval levels are there and status is rejected
	--						SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
	--												RoleId = (SELECT NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--															WHERE AdjustmentLogId = @AdjustmentLogId))

	--						SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
	--						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
	--					END
	--			END
	--		ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
	--					SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_AdjustmentsLogs 
	--					WHERE AdjustmentLogId = @AdjustmentLogId
	--			END

	--		-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
	--		UPDATE Tbl_AdjustmentsLogs 
	--		SET   
	--			 ModifedBy = @ModifiedBy
	--			,ModifiedDate = dbo.fn_GetCurrentDateTime()
	--			,PresentApprovalRole = @PresentRoleId
	--			,NextApprovalRole = @NextRoleId 
	--			,ApproveStatusId = @ApprovalStatusId
	--			,Remarks = @Details
	--		WHERE GlobalAccountNumber = @GlobalAccountNo  
	--		AND AdjustmentLogId = @AdjustmentLogId
			
	--		SELECT 1 As IsSuccess  
	--		FOR XML PATH('ChangeCustomerTypeBE'),TYPE  
	--	END 
	
	
	 
	----------------------Old one Not Changed -----------------------------

END

GO

/****** Object:  StoredProcedure [dbo].[USP_BillAdjustment]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--===================================    
--AUTHOR  : Neeraj Kanojiya    
--Created Date : 01-OCT-2014    
--Description : Update Bill adjustment    
--===================================    
ALTER PROCEDURE [dbo].[USP_BillAdjustment]    
(    
@XmlDoc XML    
)    
AS    
BEGIN     
  DECLARE    
			@BillAdjustmentId INT    
			,@CustomerBillId INT    
			,@AccountNo VARCHAR(50)    
			,@CustomerId VARCHAR(50)    
			,@AmountEffected DECIMAL(18,2)    
			,@TotalAmountEffected DECIMAL(18,2)    
			,@BillAdjustmentType INT    
			,@AdjustedUnits INT    
			,@ApprovalStatusId INT    
			,@BatchNo INT  
			,@EnergyCharges DECIMAL(18,2)   
			,@TaxEffected DECIMAL(18,2)  
			,@AdditionalCharges DECIMAL(18,2)   
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@Details VARCHAR(MAX)   
			,@BU_ID VARCHAR(50) 
			,@CurrentApprovalLevel INT
       
  SELECT    
		@CustomerBillId = C.value('(CustomerBillId)[1]','INT')    
		,@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')    
		,@BillAdjustmentType = C.value('(BillAdjustmentType)[1]','INT')   
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')    
		,@BatchNo = C.value('(BatchNo)[1]','INT')    
		,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)') 
		,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)') 
		,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)') 
		,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)') 
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
		,@FunctionId = C.value('(FunctionId)[1]','INT')  
		,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		
  FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C)        
       
      
      
IF((SELECT COUNT(0) FROM Tbl_CustomerNameChangeLogs   
					WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
					BEGIN
								DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
										
							DECLARE @Forward INT
						SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
							
							
						SELECT @PresentRoleId = PresentRoleId 
							,@NextRoleId = NextRoleId 
						FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
					
					END
				ELSE 
				BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TotalAmountEffected
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@AmountEffected
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details,
							@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				FROM CUSTOMERS.Tbl_CustomersDetail CD WHERE GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
			BEGIN
								   
				   INSERT INTO Tbl_BillAdjustments(    
					 CustomerBillId    
					 ,AccountNo    
					 ,CustomerId    
					 ,AmountEffected   
					 ,BillAdjustmentType    
					 ,TotalAmountEffected    
					 ,BatchNo  
					,CreatedBy
					,CreatedDate
					,ApprovedBy 
					,Remarks
					 )           
					VALUES(         
					 @CustomerBillId    
					 ,@AccountNo    
					 ,@CustomerId    
					 ,@AmountEffected   
					 ,@BillAdjustmentType   
					 ,@AmountEffected    
					 ,@BatchNo  
					,@ModifiedBy
					,dbo.fn_GetCurrentDateTime()
					,@ModifiedBy
					,@Details
					 )      
				  SELECT @BillAdjustmentId = SCOPE_IDENTITY()    
				       
				  INSERT INTO Tbl_BillAdjustmentDetails
									(BillAdjustmentId
									,EnergryCharges
									,CreatedBy
									,CreatedDate
									)    
									SELECT           
									@BillAdjustmentId     
									,@EnergyCharges    
									,@ModifiedBy
									,dbo.fn_GetCurrentDateTime() 
							
							--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
							,ModifedBy=@ModifiedBy
							,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
							
							--Updating the adjustment amount in Bill--commented by neeraj as per SATYA 
							
							--UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
							--WHERE CustomerBillId=@CustomerBillId
				
			END
		    SELECT     
   (CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess    
  FOR XML PATH('BillAdjustmentsBe'),TYPE      
		 		
 		END 
	ELSE  
		BEGIN  
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('ChangeBookNoBe')  
		END   

END
    

GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_BillAdjustment]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <22-JULY-2014>
-- Description:	<INSERT BILL ADJUSTMENT DETAILS>
-- Modified By : Padmini
-- Modified Date : 26-12-2014
-- =============================================
ALTER PROCEDURE [dbo].[USP_Insert_BillAdjustment]
(
	@XmlDoc xml=null
	,@MultiXmlDoc XML
)
AS
BEGIN	
		DECLARE
			@BillAdjustmentId INT
			,@CustomerBillId INT
			,@AccountNo VARCHAR(50)
			,@CustomerId VARCHAR(50)
			,@AmountEffected DECIMAL(18,2)
			,@TaxEffected DECIMAL(18,2)
			,@TotalAmountEffected DECIMAL(18,2)
			,@AdjustedUnits INT
			,@BillAdjustmentType INT
			,@BatchNo INT
			,@ModifiedBy VARCHAR(50) 
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@ApprovalStatusId INT 
			,@Details VARCHAR(MAX)  
			,@EnergyCharges DECIMAL(18,2)
			,@AdditionalCharges DECIMAL(18,2)
			,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
		
		SELECT
			@CustomerBillId = C.value('(CustomerBillId)[1]','INT')
			,@AccountNo	= C.value('(AccountNo)[1]','VARCHAR(50)')
			,@AmountEffected = C.value('(AmountEffected)[1]','DECIMAL(18,2)')
			,@TaxEffected = C.value('(TaxEffected)[1]','DECIMAL(18,2)')
			,@TotalAmountEffected = C.value('(TotalAmountEffected)[1]','DECIMAL(18,2)')
			,@BillAdjustmentType	= C.value('(BillAdjustmentType)[1]','INT')
			,@BatchNo = C.value('(BatchNo)[1]','INT')  
			,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')  
			,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
			,@FunctionId = C.value('(FunctionId)[1]','INT')  
			,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')  
			,@Details = C.value('(Remarks)[1]','VARCHAR(MAX)')  
			,@EnergyCharges = C.value('(EnergryCharges)[1]','DECIMAL(18,2)')
			,@AdditionalCharges = C.value('(AdditionalCharges)[1]','DECIMAL(18,2)')
				,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		FROM @XmlDoc.nodes('BillAdjustmentsBe') as T(C) 

		
 		--IF((SELECT COUNT(0) FROM Tbl_AdjustmentsLogs   
			--		WHERE GlobalAccountNumber = @AccountNo  AND ApproveStatusId IN (1,4)) = 0)
 		--BEGIN
 				DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
					,@NameChangeRequestId INT
			
				SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
				SET @CurrentLevel = 0 -- For Stating Level
				
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END

					DECLARE @Forward INT
					SET @Forward=1
						IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
							BEGIN
									SET @CurrentLevel=1	
									SET @CurrentApprovalLevel =1
									SET @Forward=0
							END
							ELSE
							BEGIN
								SET @CurrentApprovalLevel = @CurrentLevel+1
							END 
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
			
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
			
			INSERT INTO Tbl_AdjustmentsLogs 
				(GlobalAccountNumber
				,AccountNo
				,BillNumber
				,BillAdjustmentTypeId
				,TaxEffected
				,TotalAmountEffected
				,EnergryCharges
				,AdditionalCharges
				,PresentApprovalRole
				,NextApprovalRole
				,ApproveStatusId
				,Remarks
				,CreatedBy
				,CreatedDate
				,BatchId
				,CurrentApprovalLevel
				,IsLocked
				,MeterNumber
				)

				SELECT @AccountNo
							,CD.AccountNo
							,@CustomerBillId
							,@BillAdjustmentType
							,@TaxEffected
							,@TotalAmountEffected
							,@EnergyCharges
							,@AdditionalCharges
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
							, CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END
							,@Details
							,@ModifiedBy
							,dbo.fn_GetCurrentDateTime()
							,@BatchNo
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
							,CPD.MeterNumber
				FROM CUSTOMERS.Tbl_CustomersDetail CD 
				INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CD.GlobalAccountNumber
					AND CD.GlobalAccountNumber=@AccountNo
			
			SELECT @BillAdjustmentId = SCOPE_IDENTITY()
			
			IF(@IsFinalApproval = 1)
				BEGIN
					INSERT INTO Tbl_BillAdjustments(
						CustomerBillId
						,AccountNo
						,CustomerId
						,AmountEffected
						,BillAdjustmentType
						,TaxEffected
						,TotalAmountEffected
						,BatchNo
						,ApprovedBy
						,CreatedBy
						,CreatedDate
						,Remarks
						) 						
					VALUES(					
						@CustomerBillId
						,@AccountNo
						,@CustomerId
						,@AmountEffected
						,@BillAdjustmentType
						,@TaxEffected
						,@TotalAmountEffected
						,@BatchNo
						,@ModifiedBy
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,@Details
						)		
					SELECT @BillAdjustmentId = SCOPE_IDENTITY()
				
					INSERT INTO Tbl_BillAdjustmentDetails
											(
											BillAdjustmentId
											,AdditionalChargesID
											,AdditionalCharges
											,EnergryCharges
											,CreatedBy
											,CreatedDate
											)
					SELECT       
						@BillAdjustmentId 
						,C.value('(ChargeID)[1]','INT')       
						,C.value('(AmountEffected)[1]','DECIMAL(18,2)')      
						,@EnergyCharges
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
					 FROM @MultiXmlDoc.nodes('//NewDataSet/Table1') AS T(C)    	
					
					--UPDATING THE CUSTOMER OUTSTANDING AMOUNT
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails SET OutStandingAmount= ISNULL(OutStandingAmount,0) - @TotalAmountEffected
					,ModifedBy=@ModifiedBy
					,ModifiedDate=dbo.fn_GetCurrentDateTime() WHERE GlobalAccountNumber=@AccountNo
					 
					--Updating the adjustment amount in Bill
					UPDATE Tbl_CustomerBills SET AdjustmentAmmount= ISNULL(AdjustmentAmmount,0)+@TotalAmountEffected 
					WHERE CustomerBillId=@CustomerBillId
					 
				END
				
				SELECT 
					(CASE WHEN @BillAdjustmentId > 0 THEN 1 ELSE 0 END) As IsSuccess
				FOR XML PATH('BillAdjustmentsBe'),TYPE
		 		
 --		END 
	--ELSE  
	--	BEGIN  
	--		SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillAdjustmentsBe')  
	--	END  
 	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- Modified By : Bhimaraju Vanka
-- Desc: Here in Adjustment (No Bill) These details are not having in master table we need do left join 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	
	;WITH PageResult AS(
	SELECT TOP 5 ISNULL(BT.Name,'Adjustment(No Bill)') AS AdjustmentName
		--SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.TotalAmountEffected) AS TotalAmountEffected
			,CONVERT(DECIMAL(18,2),BA.AmountEffected) AS AmountEffected
			,ISNULL(BA.AdjustedUnits,0) AS AdjustedUnits
			,BAD.BatchNo AS BatchNo
			--,(CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
			,BA.Remarks
		FROM Tbl_BillAdjustments AS BA
		JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		JOIN Tbl_BATCH_ADJUSTMENT AS BAD ON BAD.BatchID=BA.BatchNo
		LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		--JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		)
		
		
	SELECT
	(	
		SELECT AdjustmentName
				--,(REPLACE(CONVERT(VARCHAR(25), CAST(AmountEffected AS MONEY), 1),'-','')+Format) AS NewAmountEffected
				,AmountEffected
				,TotalAmountEffected
				,AdjustedUnits
				,BatchNo
				,CreatedDate
				,Remarks
		FROM PageResult
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================  
--AUTHOR : NEERAJ KANOJIYA  
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS  
--CREATED ON: 4-OCT-14  
--=============================================================  
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]
(  
@XmlDoc XML  
)  
AS  
 BEGIN  
 DECLARE @BatchID INT, @BU_ID VARCHAR(50)
   
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')  
	, @BU_ID  = C.value('(BU_ID)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)  
   
   
SELECT (
		SELECT	BA.BatchID,
				BA.BatchNo,
				BA.BatchDate,
				BA.BatchTotal,
				(SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,
				RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode
				(BA.BatchTotal-
				(SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,
				convert(int, (BA.BatchTotal-
				(SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,
				(SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType
			FROM  Tbl_BATCH_ADJUSTMENT AS BA
			JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID AND BA.BatchStatusID = 1
				AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID
			 
			FOR XML PATH('BillAdjustments'),TYPE          
			),
			(
				SELECT	BA.BatchNO,
						BAT.Name AS AdjustmentName,
						CD.GlobalAccountNumber AS AccountNo,
						BLA.TotalAmountEffected,
						BLA.AmountEffected,
						ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,
						BLA.CreatedDate
						,BLA.Remarks
				from Tbl_BATCH_ADJUSTMENT		AS BA
				JOIN Tbl_BillAdjustments		AS BLA	ON BA.BatchID = BLA.BatchNo AND BA.BatchStatusID = 1
						AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID
				JOIN Tbl_BillAdjustmentDetails	AS BAD	ON BLA.BillAdjustmentId=BAD.BillAdjustmentId
				JOIN Tbl_BillAdjustmentType		AS BAT	ON BLA.BillAdjustmentType=BAT.BATID
				JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber
				 
				FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE      
			)
			FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')      
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)
			,@IsFirstmonthBill Bit
			,@SetupDate DATETIME
			,@ConnectionDate DATETIME

	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
			,SetupDate DATETIME
			,ConnectionDate DATETIME
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark
			,SetupDate  
			,ConnectionDate
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark 
			,CD.SetupDate 
			,CD.ConnectionDate
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   

		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN

			BEGIN TRY		    
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@SetupDate=SetupDate
						,@ConnectionDate=ConnectionDate 
						
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------


						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings 
							SET IsBilled =0 
								,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							-- Insert Paid Meter Payments
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
							DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END
					
					  ;WITH CTE (PreviousReading,PresentReading,GlobalAccountNumber,DuplicateCount)
						AS
						(
						SELECT PreviousReading,PresentReading,GlobalAccountNumber,
						ROW_NUMBER() OVER(PARTITION BY PreviousReading,PresentReading,GlobalAccountNumber 
						ORDER BY GlobalAccountNumber) AS DuplicateCount
						FROM Tbl_CustomerReadings
						where GlobalAccountNumber=@GlobalAccountNumber
						and IsBilled=0
						)
						DELETE 
						FROM CTE
						WHERE DuplicateCount > 1	 
						
						
							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
					
					DECLARE @IsInActiveCustomer BIT = 0
					
					IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
									BEGIN
							SET @IsInActiveCustomer = 1
						END

					IF @ReadCodeId=2 
						BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
						ELSE  
							BEGIN
								SET @IsEstimatedRead =1

								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
										,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
								IF(@IsInActiveCustomer = 1)
									BEGIN
										SET @Usage = 0
									END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
								SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

						DECLARE @tblFixedCharges AS BillFixedCharges 
						DECLARE @tblUpdatedFixedCharges AS BillFixedCharges 
						------------------------------------------------------------------------------------ 
			
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
						END	
											
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ChargeId ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
					SELECT @GetPaidMeterBalance=ISNULL(dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber),0)
			
						IF @GetPaidMeterBalance>0
						BEGIN
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
							END
						
						END
				
					INSERT INTO @tblUpdatedFixedCharges(ChargeId,Amount)
					SELECT ChargeId,Amount 
					FROM dbo.[fn_GetCustomerBillFixedCharges](@tblFixedCharges,@GetPaidMeterBalance)
						------------------------
						-- Caluclate tax here
					
					SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE 5 END) 
					SET @TaxValue = @TaxPercentage * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
 
							IF @PrevCustomerBillId IS NULL 
							BEGIN
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						
						SET  @BillGeneratedBY =( select  top 1 CreatedBy from Tbl_BillingQueueSchedule where BillingQueueScheduleId=@BillingQueueScheduleId)
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,
							(
								SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P 
								WHERE P.AccountNo = C.GlobalAccountNumber 
								ORDER BY RecievedDate DESC
							)         
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,case when (select COUNT(0) from Tbl_CustomerBills where AccountNo=@GlobalAccountNumber)>0  THEN @OutStandingAmount  else @OpeningBalance END
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						  IF isnull(@PaidMeterDeductedAmount,0) >0
						   BEGIN
						   
						   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
						   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
						   
						   update Tbl_PaidMeterDetails
						   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
						   where AccountNo=@GlobalAccountNumber
						   
						   END

						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------

					IF(@IsInActiveCustomer = 0)
						BEGIN
							UPDATE Tbl_CustomerReadings 
							SET IsBilled = 1 
								,BillNo=@CusotmerNewBillID
							WHERE GlobalAccountNumber=@GlobalAccountNumber
						END
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
						SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ChargeId 
						FROM  @tblUpdatedFixedCharges  
						
						INSERT INTO Tbl_CustomerBillFixedCharges
						(
							 CustomerBillId 
							,FixedChargeId 
							,Amount 
							,BillMonth 
							,BillYear 
							,CreatedDate 
						)
						SELECT
							 @CusotmerNewBillID
							,ChargeId
							,Amount
							,@Month
							,@Year
							,(SELECT dbo.fn_GetCurrentDateTime())  
						FROM @tblUpdatedFixedCharges  

						DELETE FROM @tblFixedCharges  
						DELETE FROM @tblUpdatedFixedCharges

						------------------------------------------------------------------------------------
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-
						SET @TotalAmount = NULL
						SET @CusotmerNewBillID=NULL
						SET @CusotmerNewBillID=NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL
						SET @IsEmbassyCustomer=NULL
						SET @TaxPercentage=NULL
						SET  @IsFirstmonthBill =NULL
						SET  @SetupDate =NULL
						SET @PaidMeterDeductedAmount=NULL
   			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue;        
						END
			END TRY
			BEGIN CATCH
			 
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue      

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
	@XmlDoc Xml
)  
AS  
BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
	,@Month INT          
	,@Year INT           
	,@Date DATETIME           
	,@MonthStartDate DATE       
	,@PreviousReading VARCHAR(50)          
	,@CurrentReading VARCHAR(50)          
	,@Usage DECIMAL(20)          
	,@RemaningBalanceUsage INT          
	,@TotalAmount DECIMAL(18,2)= 0          
	,@TaxValue DECIMAL(18,2)          
	,@BillingQueuescheduleId INT          
	,@PresentCharge INT          
	,@FromUnit INT          
	,@ToUnit INT          
	,@Amount DECIMAL(18,2)      
	,@TaxId INT          
	,@CustomerBillId INT = 0          
	,@BillGeneratedBY VARCHAR(50)          
	,@LastDateOfBill DATETIME          
	,@IsEstimatedRead BIT = 0          
	,@ReadCodeId INT          
	,@BillType INT          
	,@LastBillGenerated DATETIME        
	,@FeederId VARCHAR(50)        
	,@CycleId VARCHAR(MAX)     -- CycleId   
	,@BillNo VARCHAR(50)      
	,@PrevCustomerBillId INT      
	,@AdjustmentAmount DECIMAL(18,2)      
	,@PreviousDueAmount DECIMAL(18,2)      
	,@CustomerTariffId VARCHAR(50)      
	,@BalanceUnits INT      
	,@RemainingBalanceUnits INT      
	,@IsHaveLatest BIT = 0      
	,@ActiveStatusId INT      
	,@IsDisabled BIT      
	,@BookDisableType INT      
	,@IsPartialBill BIT      
	,@OutStandingAmount DECIMAL(18,2)      
	,@IsActive BIT      
	,@NoFixed BIT = 0      
	,@NoBill BIT = 0       
	,@ClusterCategoryId INT=NULL    
	,@StatusText VARCHAR(50)      
	,@BU_ID VARCHAR(50)    
	,@BillingQueueScheduleIdList VARCHAR(MAX)    
	,@RowsEffected INT  
	,@IsFromReading BIT  
	,@TariffId INT  
	,@EnergyCharges DECIMAL(18,2)   
	,@FixedCharges  DECIMAL(18,2)  
	,@CustomerTypeID INT  
	,@ReadType Int  
	,@TaxPercentage DECIMAL(18,2)=5    
	,@TotalBillAmountWithTax  DECIMAL(18,2)  
	,@AverageUsageForNewBill  DECIMAL(18,2)  
	,@IsEmbassyCustomer INT  
	,@TotalBillAmountWithArrears  DECIMAL(18,2)   
	,@CusotmerNewBillID INT  
	,@InititalkWh INT  
	,@NetArrears DECIMAL(18,2)  
	,@BookNo VARCHAR(30)  
	,@GetPaidMeterBalance DECIMAL(18,2)  
	,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
	,@MeterNumber VARCHAR(50)  
	,@ActualUsage DECIMAL(18,2)  
	,@RegenCustomerBillId INT  
	,@PaidAmount  DECIMAL(18,2)  
	,@PrevBillTotalPaidAmount Decimal(18,2)  
	,@PreviousBalance Decimal(18,2)  
	,@OpeningBalance Decimal(18,2)  
	,@CustomerFullName VARCHAR(MAX)  
	,@BusinessUnitName VARCHAR(100)  
	,@TariffName VARCHAR(50)  
	,@ReadDate DateTime  
	,@Multiplier INT  
	,@Service_HouseNo VARCHAR(100)  
	,@Service_Street VARCHAR(100)  
	,@Service_City VARCHAR(100)  
	,@ServiceZipCode VARCHAR(100)  
	,@Postal_HouseNo VARCHAR(100)  
	,@Postal_Street VARCHAR(100)  
	,@Postal_City VARCHAR(100)  
	,@Postal_ZipCode VARCHAR(100)  
	,@Postal_LandMark VARCHAR(100)  
	,@Service_LandMark VARCHAR(100)  
	,@OldAccountNumber VARCHAR(100)  
	,@ServiceUnitId VARCHAR(50)  
	,@PoleId Varchar(50)  
	,@ServiceCenterId VARCHAR(50)  
	,@IspartialClose INT  
	,@TariffCharges varchar(max)  
	,@CusotmerPreviousBillNo varchar(50)  
	,@DisableDate DATETIME 
	,@BillingComments Varchar(max) 
	,@TotalBillAmount decimal(18,2)
	,@TotalPaidAmount DECIMAL(18,2)
	,@PaidMeterDeductedAmount DECIMAL(18,2)
	,@AdjustmentAmountEffected DECIMAL(18,2)
	,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
	,@ConnectionDate DATETIME
	,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()       
  
	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
	SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
		,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
		,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) 
	FROM Tbl_CustomerBills CB 
	JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
		AND BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	FROM Tbl_CustomerBills AS CB
	JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
		AND CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
	JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
       
	IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
		BEGIN     
			SELECT   
				 @ActiveStatusId = ActiveStatusId 
				,@OutStandingAmount = ISNULL(OutStandingAmount,0)  
				,@TariffId=TariffId
				,@CustomerTypeID=CustomerTypeID
				,@ClusterCategoryId=ClusterCategoryId 
				,@IsEmbassyCustomer=IsEmbassyCustomer
				,@InititalkWh=InitialBillingKWh  
				,@ReadCodeId=ReadCodeID
				,@BookNo=BookNo
				,@MeterNumber=MeterNumber  
				,@OpeningBalance=isnull(OpeningBalance,0)  
				,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
				,@BusinessUnitName =BusinessUnitName  
				,@TariffName =ClassName  
				,@Service_HouseNo =Service_HouseNo  
				,@Service_Street =Service_StreetName  
				,@Service_City =Service_City  
				,@ServiceZipCode  =Service_ZipCode  
				,@Postal_HouseNo =Postal_HouseNo  
				,@Postal_Street =Postal_StreetName  
				,@Postal_City  =Postal_City  
				,@Postal_ZipCode  =Postal_ZipCode  
				,@Postal_LandMark =Postal_LandMark  
				,@Service_LandMark =Service_LandMark  
				,@OldAccountNumber=OldAccountNo  
				,@BU_ID=BU_ID  
				,@ServiceUnitId=SU_ID  
				,@PoleId=PoleID  
				,@TariffId=ClassID  
				,@ServiceCenterId=ServiceCenterId  
				,@CycleId=CycleId
				,@SetupDate=SetupDate
				,@ConnectionDate=ConnectionDate  
			FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   

			----------------------------------------COPY START --------------------------------------------------------   
			--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------

			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
				BEGIN
					SELECT @OutStandingAmount=ISNULL(NetArrears,0)--+ISNULL(AdjustmentAmmount,0)
						,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
					FROM Tbl_CustomerBills(NOLOCK)  
					WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
					
					DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId

					DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
					
					--SELECT @PaidAmount=SUM(PaidAmount)
					--FROM Tbl_CustomerBillPayments(NOLOCK)  
					--WHERE BillNo=@CusotmerPreviousBillNo

					--IF @PaidAmount IS NOT NULL
					--	BEGIN
					--		SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
							
					--		UPDATE Tbl_CustomerBillPayments
					--		SET BillNo=NULL
					--		WHERE BillNo=@RegenCustomerBillId
					--	END

					----------------------Update Readings as is billed =0 ----------------------------
					UPDATE Tbl_CustomerReadings 
					SET IsBilled =0 
						,BillNo=NULL
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
					-- Insert Paid Meter Payments

					Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
					
					Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) 
					from Tbl_PaidMeterPaymentDetails 
					WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 

					update	Tbl_PaidMeterDetails
					SET OutStandingAmount=isnull(OutStandingAmount,0) +	ISNULL(@PreviousPaidMeterDeductedAmount,0)
					WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1

					DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

					SET @PreviousPaidMeterDeductedAmount=NULL

					update CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=@OutStandingAmount
					where GlobalAccountNumber=@GlobalAccountNumber
					----------------------------------------------------------------------------------

					--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
					DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
					-------------------------------------------------------------------------------------
				END

			select  @IspartialClose=IsPartialBill,
				@BookDisableType=DisableTypeId,
				@DisableDate=DisableDate
			from Tbl_BillingDisabledBooks	  
			where BookNo=@BookNo and IsActive=1

			IF	isnull(@ActiveStatusId,0) =3 or	isnull(@ActiveStatusId,0) =4 or isnull(@CustomerTypeID,0) = 3  
				BEGIN
					Return;
				END    

			IF isnull(@BookDisableType,0)=1	 
				IF isnull(@IspartialClose,0)<>1
					Return;
					
			DECLARE @IsInActiveCustomer BIT = 0
			
			IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
				BEGIN
					SET @IsInActiveCustomer = 1
				END

			IF @ReadCodeId=2 
				BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
								@LastBillGenerated=LastBillGenerated,
								@PreviousBalance =Previousbalance,
								@BalanceUnits=BalanceUsage,
								@IsFromReading=IsFromReading,
								@PrevCustomerBillId=CustomerBillId,
								@PreviousBalance=PreviousBalance,
								@Multiplier=Multiplier,
								@ReadDate=ReadDate
							FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

					IF @IsFromReading=1
						BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
								BEGIN
									SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
														+convert(varchar(100),@BalanceUnits)+' , @Usage :'
														+convert(varchar(100),@Usage)
									SET @ActualUsage=@Usage
									SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
									SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
									SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								END
							ELSE
								BEGIN
									SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
									SET @RemaningBalanceUsage=0
									SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
														+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
														+ convert(varchar(100),@RemaningBalanceUsage)														
								END
						END
					ELSE
						BEGIN
							SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
							SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
						END
				END
			ELSE  
				BEGIN
					SET @IsEstimatedRead =1

					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
							,@PrevCustomerBillId=CustomerBillId, 
					@PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 

					IF @PreviousBalance IS NULL
						BEGIN
							SET @PreviousBalance=@OpeningBalance
						END
					
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
							IF(@IsInActiveCustomer = 1)
								BEGIN
									SET @Usage = 0
								END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
									SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
					SET @ActualUsage=@Usage
				END

			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0

			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

			DECLARE @tblFixedCharges AS BillFixedCharges 
			DECLARE @tblUpdatedFixedCharges AS BillFixedCharges 
			------------------------------------------------------------------------------------ 
			
			IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
				BEGIN
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
						BEGIN
							SET	@EnergyCharges=0
						END	
							
					SET @IsFirstmonthBill= (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ChargeId ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
					SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
					
				END
			------------------------------------------------------------------------------------------------------------------------------------
			SELECT @GetPaidMeterBalance=ISNULL(dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber),0)
			
			IF @GetPaidMeterBalance>0
				BEGIN
					IF @GetPaidMeterBalance<=@FixedCharges
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=0
							SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
						END
					ELSE
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							SET @PaidMeterDeductedAmount=@FixedCharges
							SET @FixedCharges=0
						END
						
				END
				
			INSERT INTO @tblUpdatedFixedCharges(ChargeId,Amount)
			SELECT ChargeId,Amount 
			FROM dbo.[fn_GetCustomerBillFixedCharges](@tblFixedCharges,@GetPaidMeterBalance)
			------------------------
			-- Caluclate tax here

			SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE 5 END) 
			SET @TaxValue = @TaxPercentage * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )

			IF	@PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				END

			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0

			SET @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)

			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)

			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
			FROM Tbl_CustomerBills (NOLOCK)      
			WHERE AccountNo = @GlobalAccountNumber
			Group BY CustomerBillId       
			ORDER BY CustomerBillId DESC 

			IF @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage

			IF @RemainingBalanceUnits IS NULL
				SET @RemainingBalanceUnits=0

			-------------------------------------------------------------------------------------------------------
			SET @TariffCharges = (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))
									FROM Tbl_LEnergyClassCharges 
									WHERE ClassID =@TariffId
									AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
									FOR XML PATH(''), TYPE)
									.value('.','NVARCHAR(MAX)'),1,0,'')  ) 

			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------

			SELECT @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			
			-----------------------------------------------------------------------------------------------------------------------

			----------------------------------------------------------COPY END-------------------------------------------------------------------  
			--- Need to verify all fields before insert  
			INSERT INTO Tbl_CustomerBills  
			(  
				 [AccountNo]   --@GlobalAccountNumber       
				,[TotalBillAmount] --@TotalBillAmountWithTax        
				,[ServiceAddress] --@EnergyCharges   
				,[MeterNo]   -- @MeterNumber      
				,[Dials]     --     
				,[NetArrears] --   @NetArrears      
				,[NetEnergyCharges] --  @EnergyCharges       
				,[NetFixedCharges]   --@FixedCharges       
				,[VAT]  --     @TaxValue   
				,[VATPercentage]  --  @TaxPercentage      
				,[Messages]  --        
				,[BU_ID]  --        
				,[SU_ID]  --      
				,[ServiceCenterId]    
				,[PoleId] --         
				,[BillGeneratedBy] --         
				,[BillGeneratedDate]          
				,PaymentLastDate        --  
				,[TariffId]  -- @TariffId       
				,[BillYear]    --@Year      
				,[BillMonth]   --@Month       
				,[CycleId]   -- @CycleId  
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
				,[ActiveStatusId]--          
				,[CreatedDate]--GETDATE()          
				,[CreatedBy]          
				,[ModifedBy]          
				,[ModifiedDate]          
				,[BillNo]  --        
				,PaymentStatusID          
				,[PreviousReading]  --@PreviousReading        
				,[PresentReading]   --  @CurrentReading     
				,[Usage]     --@Usage     
				,[AverageReading] -- @AverageUsageForNewBill        
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
				,[EstimatedUsage] --@Usage         
				,[ReadCodeId]  --  @ReadType      
				,[ReadType]  --  @ReadType  
				,AdjustmentAmmount -- @AdjustmentAmount    
				,BalanceUsage    --@RemaningBalanceUsage  
				,BillingTypeId --   
				,ActualUsage  
				,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
				,PreviousBalance--@PreviousBalance
				,TariffRates  
			)          
			Values
			( 
				 @GlobalAccountNumber  
				,@TotalBillAmount     
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
				,@MeterNumber      
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
				,@NetArrears         
				,@EnergyCharges   
				,@FixedCharges  
				,@TaxValue   
				,@TaxPercentage          
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
				,@BU_ID  
				,@ServiceUnitId  
				,@ServiceCenterId  
				,@PoleId          
				,@BillGeneratedBY          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
				ORDER BY RecievedDate DESC) --@LastDateOfBill          
				,@TariffId  
				,@Year  
				,@Month  
				,@CycleId  
				,@TotalBillAmountWithArrears   
				,1 --ActiveStatusId          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,@BillGeneratedBY          
				,NULL --ModifedBy  
				,NULL --ModifedDate  
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
				,2 -- PaymentStatusID     
				,@PreviousReading          
				,@CurrentReading          
				,@Usage          
				,@AverageUsageForNewBill  
				,@TotalBillAmountWithTax               
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
				,@ReadType  
				,@ReadType         
				,@AdjustmentAmount      
				,@RemaningBalanceUsage     
				,2 -- BillingTypeId   
				,@ActualUsage  
				,@PrevBillTotalPaidAmount  
				,@PreviousBalance
				,@TariffCharges  
			)  
			
			SET @CusotmerNewBillID = SCOPE_IDENTITY()   
			----------------------- Update Customer Outstanding ------------------------------  

			-- Insert Paid Meter Payments  
			IF isnull(@PaidMeterDeductedAmount,0) >0
				BEGIN
					INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
					SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        

					UPDATE Tbl_PaidMeterDetails
					SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
					WHERE AccountNo=@GlobalAccountNumber
				END

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails  
			SET OutStandingAmount=@TotalBillAmountWithArrears  
			WHERE GlobalAccountNumber=@GlobalAccountNumber  
			----------------------------------------------------------------------------------  
			----------------------Update Readings as is billed =1 ----------------------------  
			
			IF(@IsInActiveCustomer = 0)
				BEGIN
					UPDATE Tbl_CustomerReadings 
					SET IsBilled = 1 
						,BillNo=@CusotmerNewBillID
					WHERE GlobalAccountNumber=@GlobalAccountNumber
				END

			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  

			INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
			SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ChargeId 
			FROM  @tblUpdatedFixedCharges  
			
			INSERT INTO Tbl_CustomerBillFixedCharges
			(
				 CustomerBillId 
				,FixedChargeId 
				,Amount 
				,BillMonth 
				,BillYear 
				,CreatedDate 
			)
			SELECT
				 @CusotmerNewBillID
				,ChargeId
				,Amount
				,@Month
				,@Year
				,(SELECT dbo.fn_GetCurrentDateTime())  
			FROM @tblUpdatedFixedCharges  

			DELETE FROM @tblFixedCharges  
			DELETE FROM @tblUpdatedFixedCharges

			------------------------------------------------------------------------------------  

			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   

			---------------------------------------------------------------------------------------  
			UPDATE Tbl_BillAdjustments 
			SET EffectedBillId=@CusotmerNewBillID  			
			WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      

			--------------Save Bill Deails of customer name,BU-----------------------------------------------------  
			INSERT INTO Tbl_BillDetails  
			(
				CustomerBillID,  
				BusinessUnitName,  
				CustomerFullName,  
				Multiplier,  
				Postal_City,  
				Postal_HouseNo,  
				Postal_Street,  
				Postal_ZipCode,  
				ReadDate,  
				ServiceZipCode,  
				Service_City,  
				Service_HouseNo,  
				Service_Street,  
				TariffName,  
				Postal_LandMark,  
				Service_Landmark,  
				OldAccountNumber
			)  
			VALUES  
			(
				@CusotmerNewBillID,  
				@BusinessUnitName,  
				@CustomerFullName,  
				@Multiplier,  
				@Postal_City,  
				@Postal_HouseNo,  
				@Postal_Street,  
				@Postal_ZipCode,  
				@ReadDate,  
				@ServiceZipCode,  
				@Service_City,  
				@Service_HouseNo,  
				@Service_Street,  
				@TariffName,  
				@Postal_LandMark,  
				@Service_LandMark,  
				@OldAccountNumber
			)  
			
			---------------------------------------------------Set Variables to NULL-  

			SET @TotalAmount = NULL  
			SET @EnergyCharges = NULL  
			SET @FixedCharges = NULL  
			SET @TaxValue = NULL  

			SET @PreviousReading  = NULL        
			SET @CurrentReading   = NULL       
			SET @Usage   = NULL  
			SET @ReadType =NULL  
			SET @BillType   = NULL       
			SET @AdjustmentAmount    = NULL  
			SET @RemainingBalanceUnits   = NULL   
			SET @TotalBillAmountWithArrears=NULL  
			SET @BookNo=NULL  
			SET @AverageUsageForNewBill=NULL   
			SET @IsHaveLatest=0  
			SET @ActualUsage=NULL  
			SET @RegenCustomerBillId =NULL  
			SET @PaidAmount  =NULL  
			SET @PrevBillTotalPaidAmount =NULL  
			SET @PreviousBalance =NULL  
			SET @OpeningBalance =NULL  
			SET @CustomerFullName  =NULL  
			SET @BusinessUnitName  =NULL  
			SET @TariffName  =NULL  
			SET @ReadDate  =NULL  
			SET @Multiplier  =NULL  
			SET @Service_HouseNo  =NULL  
			SET @Service_Street  =NULL  
			SET @Service_City  =NULL  
			SET @ServiceZipCode =NULL  
			SET @Postal_HouseNo  =NULL  
			SET @Postal_Street =NULL  
			SET @Postal_City  =NULL  
			SET @Postal_ZipCode  =NULL  
			SET @Postal_LandMark =NULL  
			SET @Service_LandMark =NULL  
			SET @OldAccountNumber=NULL   
			SET @DisableDate=NULL  
			SET @BillingComments=NULL
			SET @TotalBillAmount=NULL
			SET @PaidMeterDeductedAmount=NULL
			SET @IsEmbassyCustomer=NULL
			SET @TaxPercentage=NULL
			SET @PaidMeterDeductedAmount=NULL
			
			SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
		END  
	ELSE
		BEGIN
			SELECT 0 AS NoRows   
			SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
		END
END  
				 










GO

/****** Object:  StoredProcedure [dbo].[USP_Update_PDFReportData]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Bhimaraju Vanka
-- Created Date: 05-08-2015
-- Description: Updating Report Data 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Update_PDFReportData]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
			DECLARE @Month INT 
					,@Year INT 
					,@LastMonth INT
					,@LastYear INT
					,@CreatedBy VARCHAR(50)
					
			SELECT  
				 @Month = C.value('(Month)[1]','INT')  
				,@Year = C.value('(Year)[1]','INT')   
				,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
			FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
	
	----------Customer Details-----------
			SELECT	 CB.BillYear
				,CB.BillMonth
				,CB.BillNo
				,CB.TariffId
				,CB.ServiceCenterId
				,CB.SU_ID
				,CB.BU_ID
				,CB.AccountNo
				,CPD.CustomerTypeId
				,CB.CustomerBillId
		INTO #CustomerBills
		FROM Tbl_CustomerBills CB 
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo
		WHERE CB.BillYear=@Year AND CB.BillMonth=@Month

	----------Payments Details-----------
		SELECT	 SUM(BP.PaidAmount) AS BillPaidAmount
				,CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,COUNT(0) AS NoOfStubs
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
		INTO #Payments
		FROM Tbl_CustomerBillPayments BP
		JOIN #CustomerBills CB ON CB.BillNo=BP.BillNo
		GROUP BY CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
				
		----------Adjustments Details-----------		
		SELECT	 SUM(BP.TotalAmountEffected) AS BillPaidAmount
				,CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,COUNT(0) AS NoOfStubs
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
		INTO #Adjustments
		FROM Tbl_BillAdjustments BP
		JOIN #CustomerBills CB ON BP.CustomerBillId=CB.CustomerBillId
		GROUP BY CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId

		----------Tbl_ReportBillingStatisticsBySCId Details-----------		
		UPDATE RBS SET   Payments = Payments+P.BillPaidAmount
						,RevenueCollected= RevenueCollected+P.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+P.BillPaidAmount
						,NoOfStubs= RBS.NoOfStubs+P.NoOfStubs
		FROM Tbl_ReportBillingStatisticsBySCId RBS
		JOIN #Payments P ON P.BU_ID=RBS.BU_ID
		AND P.SU_ID=RBS.SU_ID
		AND P.ServiceCenterId =RBS.SC_ID
		AND RBS.MonthId=P.BillMonth
		AND RBS.YearId=P.BillYear
		AND RBS.CustomerTypeId=P.CustomerTypeId
		
		UPDATE RBS SET   Payments = Payments+A.BillPaidAmount
						,RevenueCollected= RevenueCollected+A.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+A.BillPaidAmount
						,NoOfStubs= RBS.NoOfStubs+A.NoOfStubs
		FROM Tbl_ReportBillingStatisticsBySCId RBS
		JOIN #Adjustments A ON A.BU_ID=RBS.BU_ID
		AND A.SU_ID=RBS.SU_ID
		AND A.ServiceCenterId =RBS.SC_ID
		AND RBS.MonthId=A.BillMonth
		AND RBS.YearId=A.BillYear
		AND RBS.CustomerTypeId=A.CustomerTypeId
		
		----------Tbl_ReportBillingStatisticsByTariffId Updation-----------		
		UPDATE RBST SET   Payments = Payments+P.BillPaidAmount
						,RevenueCollected= RevenueCollected+P.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+P.BillPaidAmount
						,NoOfStubs= RBST.NoOfStubs+P.NoOfStubs
		FROM Tbl_ReportBillingStatisticsByTariffId RBST
		JOIN #Payments P ON P.BU_ID=RBST.BU_ID
		AND RBST.MonthId=P.BillMonth
		AND RBST.YearId=P.BillYear
		AND RBST.TariffId=P.TariffId
		
		UPDATE RBST SET   Payments = Payments+A.BillPaidAmount
						,RevenueCollected= RevenueCollected+A.BillPaidAmount
						,TotalAmountCollected= TotalAmountCollected+A.BillPaidAmount
						,NoOfStubs= RBST.NoOfStubs+A.NoOfStubs
		FROM Tbl_ReportBillingStatisticsByTariffId RBST
		JOIN #Adjustments A ON A.BU_ID=RBST.BU_ID
		AND RBST.MonthId=A.BillMonth
		AND RBST.YearId=A.BillYear
		AND RBST.TariffId=A.TariffId		
		

		DROP TABLE #CustomerBills
		DROP TABLE #Payments
		DROP TABLE #Adjustments
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPasswordStrength]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz - ID103>        
-- Create date: <06-Feb-2015>        
-- Description: <Get password strength>       
-- Modified By :kalyan
-- Modified Date :05-08-2015  
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetPasswordStrength]        
 AS      
BEGIN       
  
SELECT  
(  
 SELECT StrengthTypeId  
   ,StrengthName  
   ,case when MinLength=0 then 1 else  MinLength end as MinLength
   ,MaxLength 
   ,IsActive  
 From Tbl_PasswordStrength  
 FOR XML PATH('PasswordStrengthBE'),TYPE   
 )FOR XML PATH(''),ROOT('PasswordStrengthBEInfoByXml')    
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPagePermisions]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz-ID103>        
-- Create date: <09-JAN-2015>        
-- Description: <Retriving Master and Child page list for Role Permisions.>
-- Modified By :kalyan        
-- Modified Date :05-08-2015
--Description : added Page_Order in order by
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetPagePermisions]   
(  
@XmlDoc XML=null  
)  
AS        
 BEGIN       
 DECLARE @AccessLevelID INT  
   ,@RoleId INT   
  
 SELECT       
 @AccessLevelID = C.value('(AccessLevelID)[1]','INT')   
 ,@RoleId = C.value('(RoleId)[1]','INT')   
 FROM @XmlDoc.nodes('AdminBE') AS T(C)      
   
 Select (      
 --List for Master Menu Item      
       
       
   SELECT [MenuId]      
      ,[Name]      
      --,[Path]      
      ,[Menu_Order]      
      ,[ReferenceMenuId]      
      --,[Icon1]      
      --,[Icon2]      
      ,[Page_Order]      
      ,[IsActive]      
      --,[ActiveHeader]      
      --,[Menu_Image]      
   FROM Tbl_Menus WHERE Menu_Order IS NOT NULL AND IsActive= 1 ORDER BY Menu_Order      
   FOR XML PATH('AdminListBE'),TYPE      
    ),      
    (      
 --List for Child Menu Item      
    SELECT [MenuId]      
      ,[Name]      
      --,[Path]      
      ,[Menu_Order]      
      ,(CASE [ReferenceMenuId] WHEN 122 THEN 7 ELSE [ReferenceMenuId] END) AS [ReferenceMenuId]    
      --,[Icon1]      
      --,[Icon2]      
      ,[Page_Order]      
      ,[IsActive]      
      --,[ActiveHeader]      
      --,[Menu_Image]      
      ,ISNULL(Tbl_Menus.AccessLevelID,0) AS AccessId    
   FROM Tbl_Menus     
  LEFT JOIN Tbl_MMenuAccessLevels ON Tbl_Menus.AccessLevelID = Tbl_MMenuAccessLevels.AccessLevelID    
   WHERE Menu_Order IS NULL    
   AND IsActive= 1 AND Tbl_Menus.AccessLevelID >= @AccessLevelID  
 ORDER BY ReferenceMenuId,Page_Order      
   FOR XML PATH('AdminListBE_1'),TYPE      
   )      
   FOR XML PATH(''),ROOT('AdminBEInfoByXml')            
 END    
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50) = 0
		,@OutStanding VARCHAR(50) = 0
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		,@BulkEmails VARCHAR(MAX)
		
	SELECT
		 @AccounNo = U.GlobalAccountNumber
		,@OldAccountNo = U.OldAccountNo
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@BulkEmails=dbo.fn_GetCustomerEmailId(U.GlobalAccountNumber)
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookName
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.ReadCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		--,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
		,@OutStanding = CONVERT(VARCHAR,(CAST(ISNULL(U.OutStandingAmount,0) AS MONEY)), 1)
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
				,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(TotalBillAmountWithArrears,0) AS MONEY)), 1)
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			--SELECT
			--	@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			--FROM Tbl_CustomerBillPayments
			--WHERE CustomerBillId = @CustomerBillId
			
			--SELECT
			--	@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			--FROM Tbl_BillAdjustments
			--WHERE CustomerBillId = @CustomerBillId
	
		END
	ELSE
		BEGIN			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE AccountNo = @GlobalAccountNo	
		END
	
	SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.ReadCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		--,ISNULL((SELECT TOP 1 
		--		REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
		--	FROM Tbl_CustomerReadings CR3
		--	WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
		--		AND CR3.CustomerReadingId < CR.CustomerReadingId
		--	ORDER BY CR3.CustomerReadingId DESC),REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', ''))
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', '')
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@OldAccountNo AS OldAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@BulkEmails AS BulkEmails 
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,ISNULL(@NetArrears,@OutStanding) AS NetArrears
			,@Payments AS Payments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,ISNULL(@LastPaidAmount,'--') AS LastPaidAmount
			,ISNULL(@LastPaidDate,'--') AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
			,@CustomerBillId AS CustomerBillId
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	,
	(
		SELECT
			 CF.FixedChargeId AS FixedChargeId
			,C.ChargeName AS FixedChargeName
			,CONVERT(VARCHAR,(CAST(ISNULL(CF.Amount,0) AS MONEY)), 1) AS FixedChargeAmount
		FROM Tbl_CustomerBillFixedCharges CF
		INNER JOIN Tbl_MChargeIds C ON CF.FixedChargeId = C.ChargeId AND IsAcitve = 1
			AND CF.CustomerBillId = @CustomerBillId
		FOR XML PATH('CustomerBillAdditionalChargeDetails'),TYPE   
	)
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerPDFBillDetails]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 13-FEB-2014
-- Description: Insert Customer PDF Bill Details
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertCustomerPDFBillDetails]
	(
	@XmlDoc XML=null
	)
AS
BEGIN
	DECLARE
	     @FilePath VARCHAR(MAX) 
	    ,@BillNo VARCHAR(50)
	    ,@BillMonthId INT
	    ,@BillYearId INT
	    ,@CustomerBillId INT
		,@CreatedBy VARCHAR(50)
		,@GlobalAccountNo VARCHAR(50)
		,@EmailId VARCHAR(MAX)
				
	SELECT 
			@GlobalAccountNo=C.value('(GlobalAccountNo)[1]','VARCHAR(50)')
			,@FilePath=C.value('(FilePath)[1]','VARCHAR(MAX)')
			,@BillNo=C.value('(BillNo)[1]','VARCHAR(50)')
			,@BillMonthId=C.value('(BillMonth)[1]','INT')
			,@BillYearId=C.value('(BillYear)[1]','INT')
			,@CustomerBillId=C.value('(CustomerBillId)[1]','INT')
		    ,@CreatedBy = C.value('(CreatedBy)[1]','VARCHAR(50)')
		    ,@EmailId = C.value('(EmailId)[1]','VARCHAR(MAX)')
	FROM @XmlDoc.nodes('CustomerBillPDFDetailsBE') as T(C)	      
	
	
	
		INSERT INTO Tbl_CustomerBillPDFFiles
							(
								 FilePath
								,BillNo
								,BillMonthId
								,BillYearId
								,CustomerBillId
								,CreatedBy
								,CreatedDate
								,GlobalAccountNo
								,EmailId
							)
				VALUES		(
								 @FilePath
								,@BillNo
								,@BillMonthId
								,@BillYearId
								,@CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
								,@GlobalAccountNo
								,@EmailId
							)
			
			UPDATE Tbl_BillDetails SET BillFilePath=@FilePath
			WHERE CustomerBillID=@CustomerBillId
	
			SELECT 1 As IsSuccess
			FOR XML PATH('CustomerBillPDFDetailsBE'),TYPE
END


--====================================================================================================

GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj Kanojiya
-- Modified Date: 3-Aug-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
DECLARE @BillMonthList VARCHAR(MAX)
		,@BillYearList VARCHAR(MAX)
		,@Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	--SELECT * FROM DBO.fn_splitTwo('6|2|2|','2|2|2|','|')
	
		SELECT  
		 @BillMonthList = C.value('(BillMonthList)[1]','varchar(max)')  
		,@BillYearList = C.value('(BillYearList)[1]','varchar(max)')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)
	
	CREATE TABLE #BillMonthList(RowNum INT identity(1,1), BillMonth varchar(10), BillYear varchar(10))
	
	INSERT INTO #BillMonthList(BillMonth,BillYear)
	
	SELECT DISTINCT * FROM DBO.fn_splitTwo(@BillMonthList,@BillYearList,'|')
	
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(RowNum) FROM #BillMonthList)  
	WHILE(@index<@rwCount) --loop
	BEGIN		
		SET @index=@index+1
		SET @Month=(SELECT BillMonth FROM #BillMonthList WHERE RowNum=@index)
		SET @Year=(SELECT BillYear FROM #BillMonthList WHERE RowNum=@index)
		
		IF(@Month = 1)
			BEGIN
				SET @LastMonth = 12
				SET @LastYear = @Year - 1
			END
		ELSE
			BEGIN
				SET @LastMonth = @Month - 1
				SET @LastYear = @Year
			END
			
		DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
		DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
		--------------------------------------------------------------------------------------
		--CustomersList table for those customer who belongs to parameterised blll open month
		--------------------------------------------------------------------------------------
		
		SELECT 
			 CB.BU_ID
			,BU.BusinessUnitName
			,CB.SU_ID
			,SU.ServiceUnitName
			,CB.ServiceCenterId
			,SC.ServiceCenterName
			,CB.TariffId
			,TC.ClassName
			,CPD.CustomerTypeId
			,CT.CustomerType
			,CB.BillYear
			,CB.BillMonth
			,CB.AccountNo
			,CPD.ReadCodeID
			,CB.Usage
			,CB.NetFixedCharges
			,CB.NetEnergyCharges
			,CB.VATPercentage
			,CB.NetArrears
			,CB.VAT
			,CB.TotalBillAmount
			,CB.TotalBillAmountWithTax
			,CB.TotalBillAmountWithArrears
			,CB.ReadType
			,CB.PaidAmount
			,CAD.OutStandingAmount
			,CB.TotalBillAmountWithTax AS ClosingBalance
			,CB.BillNo
			,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
		INTO #CustomersList
		FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
		INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
		INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
		INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
		INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
		WHERE BillMonth = @Month AND BillYear = @Year

		--------------------------------------------------------------------------------------
		-- Total bill amount with tax after bill gen of last month.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  AccountNo
			 ,TotalBillAmountWithTax
		INTO #CustomerOpeningBalance
		FROM Tbl_CustomerBills
		WHERE BillMonth = @LastMonth AND BillYear = @LastYear

		--------------------------------------------------------------------------------------
		-- Total no of customer in those tariff and cust type.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  CPD.BookNo
			 ,BN.CycleId
			 ,BG.ServiceCenterId
			 ,SC.SU_ID
			 ,SU.BU_ID
			 ,CPD.TariffClassID
			 ,CPD.CustomerTypeId
			 ,GlobalAccountNumber
		INTO #CustomerPopulation
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
		INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
		INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

		INSERT INTO Tbl_ReportBillingStatisticsBySCId
		(
			 BU_ID
			,BusinessUnitName
			,SU_ID
			,ServiceUnitName
			,SC_ID
			,ServiceCenterName
			,CustomerTypeId
			,CustomerType
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,EnergyDelivered
			,FixedCharges
			,NoOfBilledCustomers
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
						AND CP.ServiceCenterId = CL.ServiceCenterId 
						AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			
		INSERT INTO Tbl_ReportBillingStatisticsByTariffId
		(
			 BU_ID
			,BusinessUnitName
			,TariffId
			,TariffName
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,NoOfBilledCustomers
			,EnergyDelivered
			,FixedCharges
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID  
						AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth		
	END			
		BEGIN TRY			
			DROP TABLE #CustomersList
			DROP TABLE #CustomerOpeningBalance
			DROP TABLE #CustomerPopulation	
			DROP TABLE #BillMonthList
		END try
		BEGIN CATCH
		END CATCH

	SELECT 1 AS IsSuccess
	FOR XML PATH('BillGenerationBe'),TYPE  
	
END



GO

/****** Object:  StoredProcedure [dbo].[USP_GetBillDetailsPdfGen]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Neeraj Kanojiya
-- Modified Date: 3-Aug-2015
-- Description: Inserting Report Data 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBillDetailsPdfGen]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
----------------------------------------------------------------------------------------
--				Variable Decleration
----------------------------------------------------------------------------------------
	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		,@index INT =0
		,@rwCount INT 
		,@AccounNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50) = 0
		,@OutStanding VARCHAR(50) = 0
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		,@BulkEmails VARCHAR(MAX) 
----------------------------------------------------------------------------------------
--				Deserializtion of input
----------------------------------------------------------------------------------------		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  		
----------------------------------------------------------------------------------------
--				Declaration of temp tables
----------------------------------------------------------------------------------------		
	CREATE TABLE #AccountNoList(RowNum INT identity(1,1)
								,GAN varchar(50))
								
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50)
										, BillMonth VARCHAR(20)
										, Consumption VARCHAR(20)
										, ADC VARCHAR(20)
										, CurrentDemand VARCHAR(20)
										, TotalBillPayable VARCHAR(24)
										, BillingType VARCHAR(10)
										,GAN VARCHAR(50))
										
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50)
									,CurrentReadingDate VARCHAR(20)
									,CurrentReading VARCHAR(20)
									,PreviousReadingDate VARCHAR(20)
									,PreviousReading VARCHAR(20)
									,Multiplier INT, Consumption VARCHAR(20)
									,GAN VARCHAR(50))
									
	CREATE TABLE #EmpDetails (   AccounNo VARCHAR(50)
								,OldAccountNo VARCHAR(50)
								,Name VARCHAR(200)
								,Phase VARCHAR(50)
								,BillingAddress VARCHAR(500)
								,ServiceAddress VARCHAR(500)
								,PhoneNo VARCHAR(20)
								,EmailId VARCHAR(50)
								,MeterNo VARCHAR(50)
								,BusinessUnit VARCHAR(50)
								,ServiceUnit VARCHAR(50)
								,ServiceCenter VARCHAR(50)
								,BookGroup VARCHAR(50)
								,BookNo VARCHAR(50)
								,PoleNo VARCHAR(50)
								,RouteSequense VARCHAR(50)
								,ConnectionType VARCHAR(50)
								,TariffCategory VARCHAR(50)
								,TariffType VARCHAR(50)
								,BillDate VARCHAR(20)
								,BillType VARCHAR(10)
								,BillNo VARCHAR(50)
								,BillRemarks VARCHAR(200)
								,NetArrears VARCHAR(50) 
								,OutStanding VARCHAR(50) 
								,Payments VARCHAR(50)
								,TotalBillAmountWithTax VARCHAR(50)
								,NetAmountPayble VARCHAR(50)
								,DueDate VARCHAR(20)
								,LastPaidAmount VARCHAR(20)
								,LastPaidDate VARCHAR(20)
								,PaidMeterAmount VARCHAR(20)
								,PaidMeterDeduction VARCHAR(20)
								,PaidMeterBalance VARCHAR(20)
								,Consumption VARCHAR(20)
								,TariffRate VARCHAR(20)
								,EnergyCharges VARCHAR(20)
								,FixedCharges VARCHAR(20)
								,TaxPercentage DECIMAL(18,2)
								,Tax VARCHAR(20)
								,TotalBillAmount VARCHAR(50)
								,BillPeriod VARCHAR(100)
								,IsCAPMIMeter BIT
								,LastBilledDate VARCHAR(50)
								,LastBilledNo VARCHAR(50)
								,LastBillPayments DECIMAL(18,2)
								,LastBillAdjustments DECIMAL(18,2)
								,CustomerBillId INT
								,PaidMeterPayments DECIMAL(18,2)
								,PaidMeterAdjustments DECIMAL(18,2)
								,InitialBillingKWh VARCHAR(20)
								,BulkEmails VARCHAR(MAX))		
	
	CREATE TABLE #BillCharges (FixedChargeId int
								,FixedChargeName varchar(max)
								,FixedChargeAmount DECIMAL(18,2)
								,GAN varchar(50))												
					
----------------------------------------------------------------------------------------
--				Account list adding for  bill month and year
----------------------------------------------------------------------------------------	
	INSERT INTO #AccountNoList(GAN)	
	select AccountNo from Tbl_CustomerBills where BillMonth=@Month and BillYear=@year	
----------------------------------------------------------------------------------------
--				Loop starts for each customer by GAN
----------------------------------------------------------------------------------------	
	
	SELECT COUNT(0) FROM #AccountNoList
	
	SET @rwCount=(SELECT MAX(RowNum) FROM #AccountNoList) 	
					
	WHILE(@index<@rwCount) --loop
		BEGIN
		SET @index=@index+1
		SET @GlobalAccountNo=(SELECT GAN FROM #AccountNoList WHERE RowNum=@index)
		SELECT
			 @AccounNo = U.GlobalAccountNumber
			,@OldAccountNo = U.OldAccountNo
			,@Name = U.FullName
			,@Phase = U.Phase
			,@ServiceAddress = U.ServiceAddress
			,@BillingAddress = U.BillingAddress
			,@PhoneNo = U.ContactNo
			,@EmailId = U.EmailId
			,@BulkEmails=dbo.fn_GetCustomerEmailId(U.GlobalAccountNumber)
			,@MeterNo = U.MeterNumber
			,@BusinessUnit = U.BusinessUnitName
			,@ServiceUnit = U.ServiceUnitName
			,@ServiceCenter = U.ServiceCenterName
			,@BookGroup = U.CycleName
			,@BookNo = U.BookNo
			,@PoleNo = U.PoleNo
			,@RouteSequense = U.RouteName
			,@ConnectionType = U.CustomerType
			,@TariffCategory = U.TariffCategory
			,@TariffType = U.TariffName
			,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
			,@BillType = RC.DisplayCode
			,@BillNo = CB.BillNo
			,@BillRemarks = CB.Remarks
			--,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
			,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
			,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
			,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
			,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
			,@IsCAPMIMeter = U.IsCAPMIMeter
			,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
			,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
			,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
			,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
			,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
			,@TaxPercentage = ISNULL(CB.VATPercentage,0)
			,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
			,@CustomerBillId = CB.CustomerBillId
			,@InitialBillingKWh = U.InitialBillingKWh
			,@OutStanding = CONVERT(VARCHAR,(CAST(ISNULL(U.OutStandingAmount,0) AS MONEY)), 1)
		FROM UDV_CustomerDetailsForBillPDF U
		INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
				AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		INSERT INTO #EmpDetails(
								 	AccounNo 
									,OldAccountNo 
									,Name 
									,Phase 
									,ServiceAddress
									,BillingAddress
									,PhoneNo
									,EmailId 
									,BulkEmails
									,MeterNo 
									,BusinessUnit 
									,ServiceUnit 
									,ServiceCenter 
									,BookGroup 
									,BookNo 
									,PoleNo 
									,RouteSequense 
									,ConnectionType 
									,TariffCategory 
									,TariffType 
									,BillDate 
									,BillType 
									,BillNo 
									,BillRemarks 
									,TotalBillAmount 
									,TotalBillAmountWithTax
									,NetAmountPayble 
									,DueDate 
									,IsCAPMIMeter 
									,PaidMeterBalance 
									,Consumption 
									,TariffRate 
									,EnergyCharges 
									,FixedCharges 
									,TaxPercentage 
									,Tax 
									,CustomerBillId 
									,InitialBillingKWh 
									,OutStanding 
								)
					VALUES
								(
									 @AccounNo 
									,@OldAccountNo 
									,@Name 
									,@Phase 
									,@ServiceAddress
									,@BillingAddress
									,@PhoneNo
									,@EmailId 
									,@BulkEmails
									,@MeterNo 
									,@BusinessUnit 
									,@ServiceUnit 
									,@ServiceCenter 
									,@BookGroup 
									,@BookNo 
									,@PoleNo 
									,@RouteSequense 
									,@ConnectionType 
									,@TariffCategory 
									,@TariffType 
									,@BillDate 
									,@BillType 
									,@BillNo 
									,@BillRemarks 
									,@TotalBillAmount 
									,@TotalBillAmountWithTax
									,@NetAmountPayble 
									,@DueDate 
									,@IsCAPMIMeter 
									,@PaidMeterBalance
									,@Consumption 
									,@TariffRate 
									,@EnergyCharges 
									,@FixedCharges 
									,@TaxPercentage 
									,@Tax 
									,@CustomerBillId 
									,@InitialBillingKWh
									,@OutStanding 
								
								)
		IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
			BEGIN
				SELECT TOP 1
					 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
					,@LastBilledNo = BillNo
					,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(TotalBillAmountWithArrears,0) AS MONEY)), 1)
				FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
					AND BillNo != @BillNo
				ORDER BY BillGeneratedDate DESC
				
				SELECT
					 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
				FROM Tbl_CustomerPayments
				WHERE CONVERT(DATETIME,RecievedDate) >= CONVERT(DATETIME,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo	
				
				SELECT
					@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
				FROM Tbl_BillAdjustments
				WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
				AND AccountNo = @GlobalAccountNo	
				
				--SELECT
				--	@LastBillPayments = SUM(ISNULL(PaidAmount,0))
				--FROM Tbl_CustomerBillPayments
				--WHERE CustomerBillId = @CustomerBillId
				
				--SELECT
				--	@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
				--FROM Tbl_BillAdjustments
				--WHERE CustomerBillId = @CustomerBillId
		
			END
		
		SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
		
		SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
		
		SELECT 
			 TOP 1
			 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
			,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
		FROM Tbl_CustomerPayments 
		WHERE AccountNo = @GlobalAccountNo
		ORDER BY RecievedDate DESC
		
		IF(@IsCAPMIMeter = 1)
			BEGIN
				SELECT TOP 1 
					@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
				FROM Tbl_PaidMeterDetails
				WHERE AccountNo = @GlobalAccountNo
				AND MeterNo = @MeterNo AND ActiveStatusId = 1
				ORDER BY MeterId DESC
				
				SELECT
					@PaidMeterPayments = SUM(ISNULL(Amount,0))
				FROM Tbl_PaidMeterPaymentDetails
				WHERE AccountNo = @GlobalAccountNo
				AND MeterNo = @MeterNo
				
				SELECT
					@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
				FROM Tbl_PaidMeterAdjustmentDetails
				WHERE AccountNo = @GlobalAccountNo
				AND MeterNo = @MeterNo
				
				SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
				
			END
			
		
		
		INSERT INTO @TableLastConsumption
		(
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
			,GAN
		)
		SELECT TOP 5
			 CB.BillNo
			,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
			,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
			,''
			,''
			,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
			,RC.DisplayCode
			,CB.AccountNo
		FROM Tbl_CustomerBills CB
		INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
			AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
		ORDER BY CB.BillGeneratedDate DESC	
			
		INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
		,GAN
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		--,ISNULL((SELECT TOP 1 
		--		REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
		--	FROM Tbl_CustomerReadings CR3
		--	WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
		--		AND CR3.CustomerReadingId < CR.CustomerReadingId
		--	ORDER BY CR3.CustomerReadingId DESC),REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', ''))
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', '')
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
		,CR.GlobalAccountNumber
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC	
	
	INSERT INTO #BillCharges(	FixedChargeId
								,FixedChargeName
								,FixedChargeAmount
								,GAN
							)
	SELECT
			 CF.FixedChargeId AS FixedChargeId
			,C.ChargeName AS FixedChargeName
			,CONVERT(VARCHAR,(CAST(ISNULL(CF.Amount,0) AS MONEY)), 1) AS FixedChargeAmount
			,@GlobalAccountNo			
		FROM Tbl_CustomerBillFixedCharges CF
		INNER JOIN Tbl_MChargeIds C ON CF.FixedChargeId = C.ChargeId AND IsAcitve = 1
			AND CF.CustomerBillId = @CustomerBillId
	END
----------------------------------------------------------------------------------------
--				Loop ends and select details from temp tables
----------------------------------------------------------------------------------------		

		SELECT * FROM #EmpDetails
		ORDER BY AccounNo

		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
			,GAN
		FROM @TableLastConsumption
		ORDER BY GAN

		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
			,GAN
		FROM @TableLastReadings
		ORDER BY GAN

		SELECT * FROM #BillCharges
 
	DROP TABLE #AccountNoList
	DROP TABLE #EmpDetails
	DROP TABLE #BillCharges
END


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMobileNo]    Script Date: 08/05/2015 19:36:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 27-July-2015
-- Description: The purpose of this procedure is to get the Mobile no of a customer
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerMobileNo]  
(  
@XmlDoc XML = NULL  
)  
AS  
BEGIN  
   
 DECLARE @GlobalAccountNumber VARCHAR(50)  
   
 SELECT   
  @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')  
 FROM @XmlDoc.nodes('CommunicationFeaturesBE') AS T(C)

	--SELECT   REPLACE(CONVERT(VARCHAR(20),ISNULL(PhoneNumber,ISNULL(AlternatePhoneNumber,ISNULL(HomeContactNumber,ISNULL(BusinessContactNumber,ISNULL(OtherContactNumber,0)))))),'-','') AS MobileNo		 
	--FROM CUSTOMERS.Tbl_CustomersDetail CD
	--LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
	--WHERE CD.GlobalAccountNumber=@GlobalAccountNumber
	SELECT dbo.fn_GetCustomerMobileNo(@GlobalAccountNumber) AS MobileNo
	FOR XML PATH('CommunicationFeaturesBE'),TYPE
    
END  

GO



