GO
CREATE TYPE BillFixedCharges AS TABLE
(
	 ChargeId INT
	,Amount DECIMAL(18,2)
)
GO
GO
Update Tbl_Menus set AccessLevelID=3 
where MenuId in (
146,147,148,155,138,209,56,156,210
)
GO
DELETE Tbl_NewPagePermissions where MenuId=115 and Role_Id!=12
GO
GO
ALTER TABLE Tbl_ReportBillingStatisticsBySCId
ADD AdjustmentAmount DECIMAL(18,2)
GO
ALTER TABLE Tbl_ReportBillingStatisticsByTariffId
ADD AdjustmentAmount DECIMAL(18,2)
GO
Update TBL_FunctionalAccessPermission set [Function]='Customer Registration' WHere FunctionId=15
GO