
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportData_BillGen]    Script Date: 08/07/2015 10:13:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj kanojya
-- Date		  : 6-Aug-2015
-- =============================================  
CREATE PROCEDURE [dbo].[USP_Insert_PDFReportData_BillGen]  
(  
	@Month INT 
	,@Year INT 
	,@CreatedBy VARCHAR(50)
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @LastMonth INT
		,@LastYear INT		
		
	
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
	IF(@Month = 1)
		BEGIN
			SET @LastMonth = 12
			SET @LastYear = @Year - 1
		END
	ELSE
		BEGIN
			SET @LastMonth = @Month - 1
			SET @LastYear = @Year
		END
		
	SELECT 
		 CB.BU_ID
		,BU.BusinessUnitName
		,CB.SU_ID
		,SU.ServiceUnitName
		,CB.ServiceCenterId
		,SC.ServiceCenterName
		,CB.TariffId
		,TC.ClassName
		,CPD.CustomerTypeId
		,CT.CustomerType
		,CB.BillYear
		,CB.BillMonth
		,CB.AccountNo
		,CPD.ReadCodeID
		,CB.Usage
		,CB.NetFixedCharges
		,CB.NetEnergyCharges
		,CB.VATPercentage
		,CB.NetArrears
		,CB.VAT
		,CB.TotalBillAmount
		,CB.TotalBillAmountWithTax
		,CB.TotalBillAmountWithArrears
		,CB.ReadType
		,CB.PaidAmount
		,CAD.OutStandingAmount
		,CB.TotalBillAmountWithTax AS ClosingBalance
		,CB.BillNo
		,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
	INTO #CustomersList
	FROM Tbl_CustomerBills CB 
	INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
	INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
	INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
	INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
	INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
	WHERE BillMonth = @Month AND BillYear = @Year

	SELECT 
		  AccountNo
		 ,TotalBillAmountWithTax
	INTO #CustomerOpeningBalance
	FROM Tbl_CustomerBills
	WHERE BillMonth = @LastMonth AND BillYear = @LastYear

	SELECT 
		  CPD.BookNo
		 ,BN.CycleId
		 ,BG.ServiceCenterId
		 ,SC.SU_ID
		 ,SU.BU_ID
		 ,CPD.TariffClassID
		 ,CPD.CustomerTypeId
		 ,GlobalAccountNumber
	INTO #CustomerPopulation
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
	INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
	INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

	INSERT INTO Tbl_ReportBillingStatisticsBySCId
	(
		 BU_ID
		,BusinessUnitName
		,SU_ID
		,ServiceUnitName
		,SC_ID
		,ServiceCenterName
		,CustomerTypeId
		,CustomerType
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,EnergyDelivered
		,FixedCharges
		,NoOfBilledCustomers
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
					AND CP.ServiceCenterId = CL.ServiceCenterId 
					AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		
	INSERT INTO Tbl_ReportBillingStatisticsByTariffId
	(
		 BU_ID
		,BusinessUnitName
		,TariffId
		,TariffName
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,NoOfBilledCustomers
		,EnergyDelivered
		,FixedCharges
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID  
					AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth

	DROP TABLE #CustomersList
	DROP TABLE #CustomerOpeningBalance
	DROP TABLE #CustomerPopulation	
	
END
GO



GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
	@XmlDoc Xml
)  
AS  
  BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
	,@Month INT          
	,@Year INT           
	,@Date DATETIME           
	,@MonthStartDate DATE       
	,@PreviousReading VARCHAR(50)          
	,@CurrentReading VARCHAR(50)          
	,@Usage DECIMAL(20)          
	,@RemaningBalanceUsage INT          
	,@TotalAmount DECIMAL(18,2)= 0          
	,@TaxValue DECIMAL(18,2)          
	,@BillingQueuescheduleId INT          
	,@PresentCharge INT          
	,@FromUnit INT          
	,@ToUnit INT          
	,@Amount DECIMAL(18,2)      
	,@TaxId INT          
	,@CustomerBillId INT = 0          
	,@BillGeneratedBY VARCHAR(50)          
	,@LastDateOfBill DATETIME          
	,@IsEstimatedRead BIT = 0          
	,@ReadCodeId INT          
	,@BillType INT          
	,@LastBillGenerated DATETIME        
	,@FeederId VARCHAR(50)        
	,@CycleId VARCHAR(MAX)     -- CycleId   
	,@BillNo VARCHAR(50)      
	,@PrevCustomerBillId INT      
	,@AdjustmentAmount DECIMAL(18,2)      
	,@PreviousDueAmount DECIMAL(18,2)      
	,@CustomerTariffId VARCHAR(50)      
	,@BalanceUnits INT      
	,@RemainingBalanceUnits INT      
	,@IsHaveLatest BIT = 0      
	,@ActiveStatusId INT      
	,@IsDisabled BIT      
	,@BookDisableType INT      
	,@IsPartialBill BIT      
	,@OutStandingAmount DECIMAL(18,2)      
	,@IsActive BIT      
	,@NoFixed BIT = 0      
	,@NoBill BIT = 0       
	,@ClusterCategoryId INT=NULL    
	,@StatusText VARCHAR(50)      
	,@BU_ID VARCHAR(50)    
	,@BillingQueueScheduleIdList VARCHAR(MAX)    
	,@RowsEffected INT  
	,@IsFromReading BIT  
	,@TariffId INT  
	,@EnergyCharges DECIMAL(18,2)   
	,@FixedCharges  DECIMAL(18,2)  
	,@CustomerTypeID INT  
	,@ReadType Int  
	,@TaxPercentage DECIMAL(18,2)=5    
	,@TotalBillAmountWithTax  DECIMAL(18,2)  
	,@AverageUsageForNewBill  DECIMAL(18,2)  
	,@IsEmbassyCustomer INT  
	,@TotalBillAmountWithArrears  DECIMAL(18,2)   
	,@CusotmerNewBillID INT  
	,@InititalkWh INT  
	,@NetArrears DECIMAL(18,2)  
	,@BookNo VARCHAR(30)  
	,@GetPaidMeterBalance DECIMAL(18,2)  
	,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
	,@MeterNumber VARCHAR(50)  
	,@ActualUsage DECIMAL(18,2)  
	,@RegenCustomerBillId INT  
	,@PaidAmount  DECIMAL(18,2)  
	,@PrevBillTotalPaidAmount Decimal(18,2)  
	,@PreviousBalance Decimal(18,2)  
	,@OpeningBalance Decimal(18,2)  
	,@CustomerFullName VARCHAR(MAX)  
	,@BusinessUnitName VARCHAR(100)  
	,@TariffName VARCHAR(50)  
	,@ReadDate DateTime  
	,@Multiplier INT  
	,@Service_HouseNo VARCHAR(100)  
	,@Service_Street VARCHAR(100)  
	,@Service_City VARCHAR(100)  
	,@ServiceZipCode VARCHAR(100)  
	,@Postal_HouseNo VARCHAR(100)  
	,@Postal_Street VARCHAR(100)  
	,@Postal_City VARCHAR(100)  
	,@Postal_ZipCode VARCHAR(100)  
	,@Postal_LandMark VARCHAR(100)  
	,@Service_LandMark VARCHAR(100)  
	,@OldAccountNumber VARCHAR(100)  
	,@ServiceUnitId VARCHAR(50)  
	,@PoleId Varchar(50)  
	,@ServiceCenterId VARCHAR(50)  
	,@IspartialClose INT  
	,@TariffCharges varchar(max)  
	,@CusotmerPreviousBillNo varchar(50)  
	,@DisableDate DATETIME 
	,@BillingComments Varchar(max) 
	,@TotalBillAmount decimal(18,2)
	,@TotalPaidAmount DECIMAL(18,2)
	,@PaidMeterDeductedAmount DECIMAL(18,2)
	,@AdjustmentAmountEffected DECIMAL(18,2)
	,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
	,@ConnectionDate DATETIME
	,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
	,@IsFirstRptTblData BIT=0   
  
	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
	SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
		,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
		,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) 
	FROM Tbl_CustomerBills CB 
	JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
		AND BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	FROM Tbl_CustomerBills AS CB
	JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
		AND CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
	JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
       
	IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
		BEGIN     
			SELECT   
				 @ActiveStatusId = ActiveStatusId 
				,@OutStandingAmount = ISNULL(OutStandingAmount,0)  
				,@TariffId=TariffId
				,@CustomerTypeID=CustomerTypeID
				,@ClusterCategoryId=ClusterCategoryId 
				,@IsEmbassyCustomer=IsEmbassyCustomer
				,@InititalkWh=InitialBillingKWh  
				,@ReadCodeId=ReadCodeID
				,@BookNo=BookNo
				,@MeterNumber=MeterNumber  
				,@OpeningBalance=isnull(OpeningBalance,0)  
				,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
				,@BusinessUnitName =BusinessUnitName  
				,@TariffName =ClassName  
				,@Service_HouseNo =Service_HouseNo  
				,@Service_Street =Service_StreetName  
				,@Service_City =Service_City  
				,@ServiceZipCode  =Service_ZipCode  
				,@Postal_HouseNo =Postal_HouseNo  
				,@Postal_Street =Postal_StreetName  
				,@Postal_City  =Postal_City  
				,@Postal_ZipCode  =Postal_ZipCode  
				,@Postal_LandMark =Postal_LandMark  
				,@Service_LandMark =Service_LandMark  
				,@OldAccountNumber=OldAccountNo  
				,@BU_ID=BU_ID  
				,@ServiceUnitId=SU_ID  
				,@PoleId=PoleID  
				,@TariffId=ClassID  
				,@ServiceCenterId=ServiceCenterId  
				,@CycleId=CycleId
				,@SetupDate=SetupDate
				,@ConnectionDate=ConnectionDate  
			FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   

			----------------------------------------COPY START --------------------------------------------------------   
			--SELECT @ServiceCenterId AS ServiceCenterId
			--SELECT TOP 1 SC_ID FROM Tbl_ReportBillingStatisticsBySCId WHERE SC_ID=@ServiceCenterId
				IF EXISTS(SELECT TOP 1 SC_ID FROM Tbl_ReportBillingStatisticsBySCId WHERE SC_ID=@ServiceCenterId)
					BEGIN
					--PRINT 'SC'
							IF EXISTS(SELECT TOP 1 CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
							BEGIN
								--PRINT 'RPT'
								------------------------------------------6-Aug-2015 (ID-077) starts--------------------------------------------
								--Assigning variable befor Bill gen
								--------------------------------------------------------------------------------------
								DECLARE  @BillMonthList VARCHAR(MAX)
								,@BillYearList VARCHAR(MAX)
								,@LastMonth INT
								,@LastYear INT
								,@CreatedBy VARCHAR(50)
								,@BU_ID_RPT VARCHAR(50)
								,@BusinessUnitName_RPT VARCHAR(50)
								,@SU_ID_RPT VARCHAR(50)
								,@ServiceUnitName_RPT VARCHAR(50)
								,@ServiceCenterId_RPT VARCHAR(50)
								,@ServiceCenterName_RPT VARCHAR(50)
								,@TariffId_RPT INT
								,@ClassName_RPT VARCHAR(50)
								,@CustomerTypeId_RPT INT
								,@CustomerType_RPT VARCHAR(50)
								,@BillYear_RPT INT
								,@BillMonth_RPT INT
								,@AccountNo_RPT VARCHAR(50)
								,@ReadCodeID_RPT INT
								,@Usage_RPT decimal(18,2)
								,@NetFixedCharges_RPT decimal(18,2)
								,@NetEnergyCharges_RPT decimal(18,2)
								,@VATPercentage_RPT decimal(18,2)
								,@NetArrears_RPT decimal(18,2)
								,@VAT_RPT decimal(18,2)
								,@TotalBillAmount_RPT decimal(18,2)
								,@TotalBillAmountWithTax_RPT decimal(18,2)
								,@TotalBillAmountWithArrears_RPT decimal(18,2)
								,@ReadType_RPT INT
								,@PaidAmount_RPT decimal(18,2)
								,@OutStandingAmount_RPT decimal(18,2)
								,@ClosingBalance_RPT decimal(18,2)
								,@BillNo_RPT VARCHAR(50)
								,@NoOfStubs_RPT VARCHAR(50)
								,@GlobalAccountNumber_RPT VARCHAR(50)
								SELECT TOP 1
									 @BU_ID_RPT = CB.BU_ID
									,@BusinessUnitName_RPT=BU.BusinessUnitName
									,@SU_ID_RPT=CB.SU_ID
									,@ServiceUnitName_RPT=SU.ServiceUnitName
									,@ServiceCenterId_RPT=CB.ServiceCenterId
									,@ServiceCenterName_RPT=SC.ServiceCenterName
									,@TariffId_RPT=CB.TariffId
									,@ClassName_RPT=TC.ClassName
									,@CustomerTypeId_RPT=CPD.CustomerTypeId
									,@CustomerType_RPT=CT.CustomerType
									,@BillYear_RPT=CB.BillYear
									,@BillMonth_RPT=CB.BillMonth
									,@AccountNo_RPT=CB.AccountNo
									,@ReadCodeID_RPT=CPD.ReadCodeID
									,@Usage_RPT=CB.Usage
									,@NetFixedCharges_RPT=CB.NetFixedCharges
									,@NetEnergyCharges_RPT=CB.NetEnergyCharges
									,@VATPercentage_RPT=CB.VATPercentage
									,@NetArrears_RPT=CB.NetArrears
									,@VAT_RPT=CB.VAT
									,@TotalBillAmount_RPT=CB.TotalBillAmount
									,@TotalBillAmountWithTax_RPT=CB.TotalBillAmountWithTax
									,@TotalBillAmountWithArrears_RPT=CB.TotalBillAmountWithArrears
									,@ReadType_RPT=CB.ReadType
									,@PaidAmount_RPT=CB.PaidAmount
									,@OutStandingAmount_RPT=CAD.OutStandingAmount
									,@TotalBillAmountWithTax_RPT =CB.TotalBillAmountWithTax 
									,@BillNo_RPT=CB.BillNo
									,@NoOfStubs_RPT=(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) 
									FROM Tbl_CustomerBills CB 
									INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
									INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
									INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
									INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
									INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
									INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
									INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
									INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
									INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
									WHERE	BillMonth = @Month 
									AND		BillYear = @Year
									AND		CB.AccountNo=@GlobalAccountNumber
								--select @ServiceCenterId_RPT as ServiceCenterId_RPT
								--select @ServiceCenterId as ServiceCenterId
								--------------------------------------------------------------------------------------
								--DEBIT AMOUNTS
								--------------------------------------------------------------------------------------
								--set @ServiceCenterId_RPT = @ServiceCenterId
								
								--SELECT @NetFixedCharges_RPT,@TotalBillAmount_RPT,@NetEnergyCharges_RPT,@TotalBillAmountWithTax_RPT
								--		,@TotalBillAmountWithTax_RPT, @ClosingBalance_RPT,@ServiceCenterId
										
								--PRINT '1'
									UPDATE Tbl_ReportBillingStatisticsBySCId
									SET	FixedCharges =FixedCharges-@NetFixedCharges_RPT
										,TotalAmountBilled =TotalAmountBilled-@TotalBillAmount_RPT			
										,EnergyBilled=EnergyBilled-@NetEnergyCharges_RPT
										,RevenueBilled=RevenueBilled-@TotalBillAmountWithTax_RPT
										,OpeningBalance=OpeningBalance-@TotalBillAmountWithTax_RPT
										,ClosingBalance=ClosingBalance-@ClosingBalance_RPT
									WHERE SC_ID=@ServiceCenterId	
							
									--PRINT '2'
									UPDATE Tbl_ReportBillingStatisticsByTariffId
									SET	FixedCharges =FixedCharges-@NetFixedCharges_RPT
										,TotalAmountBilled =TotalAmountBilled-@TotalBillAmount_RPT					
										,EnergyBilled=EnergyBilled-@NetEnergyCharges_RPT
										,RevenueBilled=RevenueBilled-@TotalBillAmountWithTax_RPT
										,OpeningBalance=OpeningBalance-@TotalBillAmountWithTax_RPT
										,ClosingBalance=ClosingBalance-@ClosingBalance_RPT
								WHERE TariffId=@TariffId_RPT	
								------------------------------------------6-Aug-2015 (ID-077) ends--------------------------------------------
						END
					END
				ELSE
					SET @IsFirstRptTblData=1
		
			--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------

			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
				BEGIN

					SELECT @OutStandingAmount=ISNULL(NetArrears,0)--+ISNULL(AdjustmentAmmount,0)
						,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
					FROM Tbl_CustomerBills(NOLOCK)  
					WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
					
					DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId

					DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
					
					--SELECT @PaidAmount=SUM(PaidAmount)
					--FROM Tbl_CustomerBillPayments(NOLOCK)  
					--WHERE BillNo=@CusotmerPreviousBillNo

					--IF @PaidAmount IS NOT NULL
					--	BEGIN
					--		SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
							
					--		UPDATE Tbl_CustomerBillPayments
					--		SET BillNo=NULL
					--		WHERE BillNo=@RegenCustomerBillId
					--	END

					----------------------Update Readings as is billed =0 ----------------------------
					UPDATE Tbl_CustomerReadings 
					SET IsBilled =0 
						,BillNo=NULL
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
					-- Insert Paid Meter Payments

					Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
					
					Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) 
					from Tbl_PaidMeterPaymentDetails 
					WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 

					update	Tbl_PaidMeterDetails
					SET OutStandingAmount=isnull(OutStandingAmount,0) +	ISNULL(@PreviousPaidMeterDeductedAmount,0)
					WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1

					DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

					SET @PreviousPaidMeterDeductedAmount=NULL

					update CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=@OutStandingAmount
					where GlobalAccountNumber=@GlobalAccountNumber
					----------------------------------------------------------------------------------

					
					--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
					DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
					-------------------------------------------------------------------------------------
				END
			

			select  @IspartialClose=IsPartialBill,
				@BookDisableType=DisableTypeId,
				@DisableDate=DisableDate
			from Tbl_BillingDisabledBooks	  
			where BookNo=@BookNo and IsActive=1

			IF	isnull(@ActiveStatusId,0) =3 or	isnull(@ActiveStatusId,0) =4 or isnull(@CustomerTypeID,0) = 3  
				BEGIN
					Return;
				END    

			IF isnull(@BookDisableType,0)=1	 
				IF isnull(@IspartialClose,0)<>1
					Return;
					
			DECLARE @IsInActiveCustomer BIT = 0
			
			IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
				BEGIN
					SET @IsInActiveCustomer = 1
				END

			IF @ReadCodeId=2 
				BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
								@LastBillGenerated=LastBillGenerated,
								@PreviousBalance =Previousbalance,
								@BalanceUnits=BalanceUsage,
								@IsFromReading=IsFromReading,
								@PrevCustomerBillId=CustomerBillId,
								@PreviousBalance=PreviousBalance,
								@Multiplier=Multiplier,
								@ReadDate=ReadDate
							FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

					IF @IsFromReading=1
						BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
								BEGIN
									SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
														+convert(varchar(100),@BalanceUnits)+' , @Usage :'
														+convert(varchar(100),@Usage)
									SET @ActualUsage=@Usage
									SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
									SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
									SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								END
							ELSE
								BEGIN
									SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
									SET @RemaningBalanceUsage=0
									SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
														+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
														+ convert(varchar(100),@RemaningBalanceUsage)														
								END
						END
					ELSE
						BEGIN
							SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
							SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
						END
				END
			ELSE  
				BEGIN
					SET @IsEstimatedRead =1

					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
							,@PrevCustomerBillId=CustomerBillId, 
					@PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 

					IF @PreviousBalance IS NULL
						BEGIN
							SET @PreviousBalance=@OpeningBalance
						END
					
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
							IF(@IsInActiveCustomer = 1)
								BEGIN
									SET @Usage = 0
								END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
									SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
					SET @ActualUsage=@Usage
				END

			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0

			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

			DECLARE @tblFixedCharges AS BillFixedCharges 
			DECLARE @tblUpdatedFixedCharges AS BillFixedCharges 
			------------------------------------------------------------------------------------ 
			
			IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
				BEGIN
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
						BEGIN
							SET	@EnergyCharges=0
						END	
							
					SET @IsFirstmonthBill= (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ChargeId ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
					SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
					
				END
			------------------------------------------------------------------------------------------------------------------------------------
			SELECT @GetPaidMeterBalance=ISNULL(dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber),0)
			
			IF @GetPaidMeterBalance>0
				BEGIN
					IF @GetPaidMeterBalance<=@FixedCharges
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=0
							SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
						END
					ELSE
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							SET @PaidMeterDeductedAmount=@FixedCharges
							SET @FixedCharges=0
						END
						
				END
				
			INSERT INTO @tblUpdatedFixedCharges(ChargeId,Amount)
			SELECT ChargeId,Amount 
			FROM dbo.[fn_GetCustomerBillFixedCharges](@tblFixedCharges,@GetPaidMeterBalance)
			------------------------
			-- Caluclate tax here

			SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE 5 END) 
			SET @TaxValue = @TaxPercentage * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )

			IF	@PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				END

			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0

			SET @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)

			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)

			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
			FROM Tbl_CustomerBills (NOLOCK)      
			WHERE AccountNo = @GlobalAccountNumber
			Group BY CustomerBillId       
			ORDER BY CustomerBillId DESC 

			IF @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage

			IF @RemainingBalanceUnits IS NULL
				SET @RemainingBalanceUnits=0

			-------------------------------------------------------------------------------------------------------
			SET @TariffCharges = (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))
									FROM Tbl_LEnergyClassCharges 
									WHERE ClassID =@TariffId
									AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
									FOR XML PATH(''), TYPE)
									.value('.','NVARCHAR(MAX)'),1,0,'')  ) 

			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------

			SELECT @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			
			-----------------------------------------------------------------------------------------------------------------------

			----------------------------------------------------------COPY END-------------------------------------------------------------------  
			--- Need to verify all fields before insert  
			INSERT INTO Tbl_CustomerBills  
			(  
				 [AccountNo]   --@GlobalAccountNumber       
				,[TotalBillAmount] --@TotalBillAmountWithTax        
				,[ServiceAddress] --@EnergyCharges   
				,[MeterNo]   -- @MeterNumber      
				,[Dials]     --     
				,[NetArrears] --   @NetArrears      
				,[NetEnergyCharges] --  @EnergyCharges       
				,[NetFixedCharges]   --@FixedCharges       
				,[VAT]  --     @TaxValue   
				,[VATPercentage]  --  @TaxPercentage      
				,[Messages]  --        
				,[BU_ID]  --        
				,[SU_ID]  --      
				,[ServiceCenterId]    
				,[PoleId] --         
				,[BillGeneratedBy] --         
				,[BillGeneratedDate]          
				,PaymentLastDate        --  
				,[TariffId]  -- @TariffId       
				,[BillYear]    --@Year      
				,[BillMonth]   --@Month       
				,[CycleId]   -- @CycleId  
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
				,[ActiveStatusId]--          
				,[CreatedDate]--GETDATE()          
				,[CreatedBy]          
				,[ModifedBy]          
				,[ModifiedDate]          
				,[BillNo]  --        
				,PaymentStatusID          
				,[PreviousReading]  --@PreviousReading        
				,[PresentReading]   --  @CurrentReading     
				,[Usage]     --@Usage     
				,[AverageReading] -- @AverageUsageForNewBill        
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
				,[EstimatedUsage] --@Usage         
				,[ReadCodeId]  --  @ReadType      
				,[ReadType]  --  @ReadType  
				,AdjustmentAmmount -- @AdjustmentAmount    
				,BalanceUsage    --@RemaningBalanceUsage  
				,BillingTypeId --   
				,ActualUsage  
				,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
				,PreviousBalance--@PreviousBalance
				,TariffRates  
			)          
			Values
			( 
				 @GlobalAccountNumber  
				,@TotalBillAmount     
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
				,@MeterNumber      
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
				,@NetArrears         
				,@EnergyCharges   
				,@FixedCharges  
				,@TaxValue   
				,@TaxPercentage          
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
				,@BU_ID  
				,@ServiceUnitId  
				,@ServiceCenterId  
				,@PoleId          
				,@BillGeneratedBY          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
				ORDER BY RecievedDate DESC) --@LastDateOfBill          
				,@TariffId  
				,@Year  
				,@Month  
				,@CycleId  
				,@TotalBillAmountWithArrears   
				,1 --ActiveStatusId          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,@BillGeneratedBY          
				,NULL --ModifedBy  
				,NULL --ModifedDate  
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
				,2 -- PaymentStatusID     
				,@PreviousReading          
				,@CurrentReading          
				,@Usage          
				,@AverageUsageForNewBill  
				,@TotalBillAmountWithTax               
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
				,@ReadType  
				,@ReadType         
				,@AdjustmentAmount      
				,@RemaningBalanceUsage     
				,2 -- BillingTypeId   
				,@ActualUsage  
				,@PrevBillTotalPaidAmount  
				,@PreviousBalance
				,@TariffCharges  
			)  
			
			SET @CusotmerNewBillID = SCOPE_IDENTITY()   
			----------------------- Update Customer Outstanding ------------------------------  

			-- Insert Paid Meter Payments  
			IF isnull(@PaidMeterDeductedAmount,0) >0
				BEGIN
					INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
					SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        

					UPDATE Tbl_PaidMeterDetails
					SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
					WHERE AccountNo=@GlobalAccountNumber
				END

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails  
			SET OutStandingAmount=@TotalBillAmountWithArrears  
			WHERE GlobalAccountNumber=@GlobalAccountNumber  
			----------------------------------------------------------------------------------  
			----------------------Update Readings as is billed =1 ----------------------------  
			
			IF(@IsInActiveCustomer = 0)
				BEGIN
					UPDATE Tbl_CustomerReadings 
					SET IsBilled = 1 
						,BillNo=@CusotmerNewBillID
					WHERE GlobalAccountNumber=@GlobalAccountNumber
				END

			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  

			INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
			SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ChargeId 
			FROM  @tblUpdatedFixedCharges  
			
			INSERT INTO Tbl_CustomerBillFixedCharges
			(
				 CustomerBillId 
				,FixedChargeId 
				,Amount 
				,BillMonth 
				,BillYear 
				,CreatedDate 
			)
			SELECT
				 @CusotmerNewBillID
				,ChargeId
				,Amount
				,@Month
				,@Year
				,(SELECT dbo.fn_GetCurrentDateTime())  
			FROM @tblUpdatedFixedCharges  

			DELETE FROM @tblFixedCharges  
			DELETE FROM @tblUpdatedFixedCharges

			------------------------------------------------------------------------------------  

			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   

			---------------------------------------------------------------------------------------  
			UPDATE Tbl_BillAdjustments 
			SET EffectedBillId=@CusotmerNewBillID  			
			WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      

			--------------Save Bill Deails of customer name,BU-----------------------------------------------------  
			INSERT INTO Tbl_BillDetails  
			(
				CustomerBillID,  
				BusinessUnitName,  
				CustomerFullName,  
				Multiplier,  
				Postal_City,  
				Postal_HouseNo,  
				Postal_Street,  
				Postal_ZipCode,  
				ReadDate,  
				ServiceZipCode,  
				Service_City,  
				Service_HouseNo,  
				Service_Street,  
				TariffName,  
				Postal_LandMark,  
				Service_Landmark,  
				OldAccountNumber
			)  
			VALUES  
			(
				@CusotmerNewBillID,  
				@BusinessUnitName,  
				@CustomerFullName,  
				@Multiplier,  
				@Postal_City,  
				@Postal_HouseNo,  
				@Postal_Street,  
				@Postal_ZipCode,  
				@ReadDate,  
				@ServiceZipCode,  
				@Service_City,  
				@Service_HouseNo,  
				@Service_Street,  
				@TariffName,  
				@Postal_LandMark,  
				@Service_LandMark,  
				@OldAccountNumber
			)  
			
			---------------------------------------------------Set Variables to NULL-  

			SET @TotalAmount = NULL  
			SET @EnergyCharges = NULL  
			SET @FixedCharges = NULL  
			SET @TaxValue = NULL  

			SET @PreviousReading  = NULL        
			SET @CurrentReading   = NULL       
			SET @Usage   = NULL  
			SET @ReadType =NULL  
			SET @BillType   = NULL       
			SET @AdjustmentAmount    = NULL  
			SET @RemainingBalanceUnits   = NULL   
			SET @TotalBillAmountWithArrears=NULL  
			SET @BookNo=NULL  
			SET @AverageUsageForNewBill=NULL   
			SET @IsHaveLatest=0  
			SET @ActualUsage=NULL  
			SET @RegenCustomerBillId =NULL  
			SET @PaidAmount  =NULL  
			SET @PrevBillTotalPaidAmount =NULL  
			SET @PreviousBalance =NULL  
			SET @OpeningBalance =NULL  
			SET @CustomerFullName  =NULL  
			SET @BusinessUnitName  =NULL  
			SET @TariffName  =NULL  
			SET @ReadDate  =NULL  
			SET @Multiplier  =NULL  
			SET @Service_HouseNo  =NULL  
			SET @Service_Street  =NULL  
			SET @Service_City  =NULL  
			SET @ServiceZipCode =NULL  
			SET @Postal_HouseNo  =NULL  
			SET @Postal_Street =NULL  
			SET @Postal_City  =NULL  
			SET @Postal_ZipCode  =NULL  
			SET @Postal_LandMark =NULL  
			SET @Service_LandMark =NULL  
			SET @OldAccountNumber=NULL   
			SET @DisableDate=NULL  
			SET @BillingComments=NULL
			SET @TotalBillAmount=NULL
			SET @PaidMeterDeductedAmount=NULL
			SET @IsEmbassyCustomer=NULL
			SET @TaxPercentage=NULL
			SET @PaidMeterDeductedAmount=NULL
			
			IF(@IsFirstRptTblData=0)
			BEGIN	
				--PRINT '3'
			------------------------------------------6-Aug-2015 (ID-077) starts--------------------------------------------
					--Assigning variable AFTER Bill gen
			-------------------------------------------------------------------------------------
				
			SELECT TOP 1
						 @BU_ID_RPT = CB.BU_ID
						,@BusinessUnitName_RPT=BU.BusinessUnitName
						,@SU_ID_RPT=CB.SU_ID
						,@ServiceUnitName_RPT=SU.ServiceUnitName
						,@ServiceCenterId_RPT=CB.ServiceCenterId
						,@ServiceCenterName_RPT=SC.ServiceCenterName
						,@TariffId_RPT=CB.TariffId
						,@ClassName_RPT=TC.ClassName
						,@CustomerTypeId_RPT=CPD.CustomerTypeId
						,@CustomerType_RPT=CT.CustomerType
						,@BillYear_RPT=CB.BillYear
						,@BillMonth_RPT=CB.BillMonth
						,@AccountNo_RPT=CB.AccountNo
						,@ReadCodeID_RPT=CPD.ReadCodeID
						,@Usage_RPT=CB.Usage
						,@NetFixedCharges_RPT=CB.NetFixedCharges
						,@NetEnergyCharges_RPT=CB.NetEnergyCharges
						,@VATPercentage_RPT=CB.VATPercentage
						,@NetArrears_RPT=CB.NetArrears
						,@VAT_RPT=CB.VAT
						,@TotalBillAmount_RPT=CB.TotalBillAmount
						,@TotalBillAmountWithTax_RPT=CB.TotalBillAmountWithTax
						,@TotalBillAmountWithArrears_RPT=CB.TotalBillAmountWithArrears
						,@ReadType_RPT=CB.ReadType
						,@PaidAmount_RPT=CB.PaidAmount
						,@OutStandingAmount_RPT=CAD.OutStandingAmount
						,@TotalBillAmountWithTax_RPT =CB.TotalBillAmountWithTax 
						,@BillNo_RPT=CB.BillNo
						,@NoOfStubs_RPT=(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) 
						FROM Tbl_CustomerBills CB 
						INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
						INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
						INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
						INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
						INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
						INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
						INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
						INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
						INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
						WHERE	BillMonth = @Month 
						AND		BillYear = @Year
						AND		CB.AccountNo=@GlobalAccountNumber
			--------------------------------------------------------------------------------------
					--DEBIT AMOUNTS
			--------------------------------------------------------------------------------------	

			UPDATE Tbl_ReportBillingStatisticsBySCId
				SET	FixedCharges =FixedCharges+@NetFixedCharges_RPT
					,TotalAmountBilled =TotalAmountBilled+@TotalBillAmount_RPT				
					,EnergyBilled=EnergyBilled+@NetEnergyCharges_RPT
					,RevenueBilled=RevenueBilled+@TotalBillAmountWithTax_RPT
					,OpeningBalance=OpeningBalance+@TotalBillAmountWithTax_RPT
					,ClosingBalance=ClosingBalance+@ClosingBalance_RPT
					
				WHERE SC_ID=@ServiceCenterId							

			UPDATE Tbl_ReportBillingStatisticsByTariffId
				SET	FixedCharges =FixedCharges+@NetFixedCharges_RPT
					,TotalAmountBilled =TotalAmountBilled+@TotalBillAmount_RPT					
					,EnergyBilled=EnergyBilled+@NetEnergyCharges_RPT
					,RevenueBilled=RevenueBilled+@TotalBillAmountWithTax_RPT
					,OpeningBalance=OpeningBalance+@TotalBillAmountWithTax_RPT
					,ClosingBalance=ClosingBalance+@ClosingBalance_RPT
				WHERE TariffId=@TariffId_RPT
					  AND MonthId=@Month AND YearId=@Year
					
			------------------------------------------6-Aug-2015 (ID-077) ends--------------------------------------------
			END
			ELSE 
			BEGIN
					--print '4'					
					exec dbo.USP_Insert_PDFReportData_BillGen @Month, @Year, @CreatedBy
			END
			SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
		END  
	ELSE
		BEGIN
			SELECT 0 AS NoRows   
			SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
		END
END  
				 










GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidPaymentUploads]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karteek
-- Create date: 28 Apr 2015
-- Description:	To validate the payments data and Insertint the payments into payment realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidPaymentUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT DISTINCT
		 IDENTITY(INT, 1,1) AS RowNumber    
		,ISNULL(CD.GlobalAccountNumber,'')  AS AccountNo  
		,CD.OldAccountNo AS OldAccountNo  
		,ISNULL(TempDetails.AmountPaid,0) AS PaidAmount
		,(CASE WHEN ISNULL(TempDetails.ReceiptNo,'') = '' THEN NULL ELSE TempDetails.ReceiptNo END) AS ReceiptNo
		,(CASE WHEN EXISTS (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) CPPP
									WHERE CPPP.ReceiptNo = TempDetails.ReceiptNo AND ISNULL(TempDetails.ReceiptNo,'') != '') 
				THEN 1 
				ELSE (CASE WHEN ((SELECT COUNT(0) FROM Tbl_PaymentTransactions PPPP
									WHERE PPPP.ReceiptNo = TempDetails.ReceiptNo AND PPPP.PUploadFileId = TempDetails.PUploadFileId
									 AND ISNULL(TempDetails.ReceiptNo,'') != '') > 1)
						THEN 1 
						ELSE 0
					END)
			END) AS IsDuplicateReceiptNo 
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN CONVERT(VARCHAR(50),TempDetails.ReceivedDate,101)
				ELSE NULL END) AS PaymentDate  
		,(CASE WHEN ISDATE(ReceivedDate) = 1 AND ISNULL(TempDetails.ReceiptNo,'') != '' AND ISNUMERIC(TempDetails.AmountPaid) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) CPPP WHERE CPPP.AccountNo = CD.GlobalAccountNumber 
					AND CONVERT(DATE,CPPP.RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(DECIMAL(18,2),CPPP.PaidAmount) = CONVERT(DECIMAL(18,2),TempDetails.AmountPaid) AND CPPP.ReceiptNo = TempDetails.ReceiptNo
					AND ISNULL(TempDetails.ReceiptNo,'') != '') 
						then 1 
						else (case when ((SELECT COUNT(0) FROM Tbl_PaymentTransactions PPPP WHERE AccountNo = CD.GlobalAccountNumber 
							AND CONVERT(DATE,PPPP.ReceivedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
							AND CONVERT(DECIMAL(18,2),PPPP.AmountPaid) = CONVERT(DECIMAL(18,2),TempDetails.AmountPaid)
							AND PPPP.ReceiptNo = TempDetails.ReceiptNo AND PPPP.PUploadFileId = TempDetails.PUploadFileId) > 1
							AND ISNULL(TempDetails.ReceiptNo,'') != '') then 1 else 0 end) end)
				ELSE 0
				END) as IsDuplicate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 AND ISNULL(TempDetails.ReceiptNo,'') = '' AND ISNUMERIC(TempDetails.AmountPaid) = 1
				THEN (case when Exists (SELECT 0 FROM Tbl_CustomerPayments(NOLOCK) CPPP WHERE CPPP.AccountNo = CD.GlobalAccountNumber 
					AND CONVERT(DATE,CPPP.RecievedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(DECIMAL(18,2),CPPP.PaidAmount) = CONVERT(DECIMAL(18,2),TempDetails.AmountPaid)
					AND ISNULL(CPPP.ReceiptNo,'') = ISNULL(TempDetails.ReceiptNo,'') AND ISNULL(TempDetails.ReceiptNo,'') = '') then 1 else 0 end)
				ELSE 0 END) as IsPaymentExist
		,(CASE WHEN ISDATE(ReceivedDate) = 1 AND ISNULL(TempDetails.ReceiptNo,'') = '' AND ISNUMERIC(TempDetails.AmountPaid) = 1
				THEN (case when ((SELECT COUNT(0) FROM Tbl_PaymentTransactions PPPP WHERE PPPP.AccountNo = CD.GlobalAccountNumber 
					AND CONVERT(DATE,PPPP.ReceivedDate) = CONVERT(DATE,TempDetails.ReceivedDate) 
					AND CONVERT(DECIMAL(18,2),PPPP.AmountPaid) = CONVERT(DECIMAL(18,2),TempDetails.AmountPaid)
					AND PPPP.PUploadFileId = TempDetails.PUploadFileId 
					AND ISNULL(PPPP.ReceiptNo,'') = ISNULL(TempDetails.ReceiptNo,'') AND ISNULL(TempDetails.ReceiptNo,'') = '') > 1) then 1 else 0 end)
				ELSE 0 END) as IsPaymentTransExist
		,(CASE WHEN PUB.BU_ID = BookDetails.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,TempDetails.PaymentMode
		,(CASE WHEN PM.PaymentModeId IS NULL THEN 0 ELSE PM.PaymentModeId END) AS PaymentModeId
		,CD.ActiveStatusId 
		,TempDetails.CreatedBy
		,TempDetails.CreatedDate
		,(CASE WHEN ISDATE(ReceivedDate) = 1 
				THEN (CASE WHEN CONVERT(DATE,TempDetails.ReceivedDate) > CONVERT(DATE,TempDetails.CreatedDate) THEN 0 ELSE 1 END)
				ELSE 0 END) AS IsGreaterDate
		,PUF.FilePath
		,BD.BatchID
		,TempDetails.PUploadFileId
		,TempDetails.SNO
		,TempDetails.PaymentTransactionId
		,ISDATE(TempDetails.ReceivedDate) AS IsValidDate
		,ISNUMERIC(TempDetails.AmountPaid) AS IsValidAmount
		,1 AS IsValid
		,CONVERT(VARCHAR(MAX),'') AS Comments
		,TempDetails.AccountNo AS TempTableAccountNo
		,CPD.CustomerTypeId
	INTO #PaymentValidation
	FROM Tbl_PaymentTransactions(NOLOCK) AS TempDetails
	INNER JOIN Tbl_PaymentUploadFiles(NOLOCK) PUF ON PUF.PUploadFileId = TempDetails.PUploadFileId  
	INNER JOIN Tbl_PaymentUploadBatches(NOLOCK) PUB ON PUB.PUploadBatchId = PUF.PUploadBatchId  
	INNER JOIN Tbl_BatchDetails(NOLOCK) BD ON BD.BatchNo = PUB.BatchNo AND BD.BatchDate = PUB.BatchDate
	LEFT JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON (CD.OldAccountNo = TempDetails.AccountNo OR CD.GlobalAccountNumber = TempDetails.AccountNo)
	--((CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.GlobalAccountNumber 
	--					OR (CASE WHEN ISNULL(TempDetails.AccountNo,'|') = ' ' THEN '|' ELSE TempDetails.AccountNo END) = CD.OldAccountNo)
	LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD On (ISNULL(CD.GlobalAccountNumber,0) = CPD.GlobalAccountNumber )
	LEFT JOIN UDV_BookNumberDetails BookDetails(NOLOCK) ON BookDetails.BookNo = ISNULL(CPD.BookNo,0)
	LEFT JOIN Tbl_MPaymentMode(NOLOCK) AS PM ON TempDetails.PaymentMode = PM.PaymentMode
	WHERE PUF.PUploadFileId IN (SELECT MAX(PUploadFileId) FROM Tbl_PaymentTransactions) 
	
	SELECT TOP(1) @FileUploadId = PUploadFileId FROM #PaymentValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE AccountNo = '')
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccountNo = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the Customer is closed customer or active customer
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the Customer is prepaid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE CustomerTypeId = 3)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Prepaid Customer'
					WHERE CustomerTypeId = 3
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
			
			-- TO check whether the given date is lesser than created date
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsGreaterDate = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment Date should be lessthan the current date'
					WHERE IsGreaterDate = 0
					
				END
			
			-- TO check whether the payment mode valid or not	
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE PaymentModeId = 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Payment Mode'
					WHERE PaymentModeId = 0
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicateReceiptNo = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate ReceiptNo'
					WHERE IsDuplicateReceiptNo = 1
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsPaymentExist = 1)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payment existed with empty ReceiptNo'
					WHERE IsPaymentExist = 1
					
				END
			
			-- TO check whether the payments duplicated or not
			IF EXISTS(SELECT 0 FROM #PaymentValidation WHERE IsPaymentTransExist = 1)
				BEGIN
				
					DECLARE @TempMaxIds TABLE(RowNumber INT, GlobalAccountNo VARCHAR(20))
					
					INSERT INTO @TempMaxIds(RowNumber,GlobalAccountNo)
					SELECT MAX(RowNumber),TempTableAccountNo FROM #PaymentValidation
					WHERE IsPaymentTransExist = 1 AND IsPaymentExist = 0
					GROUP BY TempTableAccountNo
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Payments duplicated'
					WHERE IsPaymentTransExist = 1
					
					UPDATE PT
					SET IsValid = 1
					FROM #PaymentValidation PT
					JOIN @TempMaxIds TP ON TP.RowNumber = PT.RowNumber
					
				END
			
			-- TO check whether the paid amount is valid or not
			IF((SELECT COUNT(0) FROM #PaymentValidation WHERE IsValidAmount = 0) > 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Amount'
					WHERE IsValidAmount = 0
					
				END
			
			-- TO check whether the paid amount is valid or not
			IF((SELECT COUNT(0) FROM #PaymentValidation WHERE IsValidAmount = 1 AND CONVERT(DECIMAL(18,2),PaidAmount) <= 0) > 0)
				BEGIN
					
					UPDATE #PaymentValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Amount'
					WHERE IsValidAmount = 1 AND CONVERT(DECIMAL(18,2),PaidAmount) <= 0
					
				END
						
			INSERT INTO Tbl_PaymentFailureTransactions
			(
				 PUploadFileId
				,SNO
				,AccountNo
				,AmountPaid
				,ReceiptNo
				,ReceivedDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 PUploadFileId
				,SNO
				,TempTableAccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentDate
				,PaymentMode
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #PaymentValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #PaymentValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Payments Transaction table	
			DELETE FROM Tbl_PaymentTransactions		
			WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #PaymentValidation WHERE IsValid = 0
						
			DECLARE @SNo INT, @TotalCount INT
					,@PresentAccountNo VARCHAR(50), @PaidAmount DECIMAL(18,2)
					,@CustomerPaymentId INT
					,@ReceiptNo VARCHAR(20)
					,@PaymentMode INT
					,@DocumentPath VARCHAR(MAX)
					,@RecievedDate DATETIME
					,@BatchNo INT
					,@CreatedBy VARCHAR(50)
					,@PaymentModeId INT
					,@IsBillExist BIT
					
			DECLARE @PaidBills TABLE(CustomerBillId INT, BillNo VARCHAR(50), PaidAmount DECIMAL(18,2), BillStatus INT) 
			
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccountNo
				,PaidAmount
				,ReceiptNo
				,PaymentModeId
				,FilePath
				,PaymentDate
				,BatchID
				,CreatedBy
			INTO #ValidPayments
			FROM #PaymentValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidPayments    
			SET @SuccessTransactions = @TotalCount
			PRINT @SuccessTransactions
			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @PresentAccountNo = AccountNo,
						@PaidAmount = PaidAmount,
						@ReceiptNo = ReceiptNo,
						@DocumentPath = FilePath,
						@RecievedDate = PaymentDate,
						@BatchNo = BatchID,
						@PaymentModeId = PaymentModeId,
						@CreatedBy = CreatedBy
					FROM #ValidPayments 
					WHERE RowNumber = @SNo
					
					INSERT INTO Tbl_CustomerPayments
					(
						 AccountNo
						,ReceiptNo
						,PaymentMode
						,DocumentPath
						,RecievedDate
						,PaymentType
						,PaidAmount
						,BatchNo
						,ActivestatusId
						,CreatedBy
						,CreatedDate
					)
					VALUES
					(
						 @PresentAccountNo
						,@ReceiptNo
						,@PaymentModeId
						,@DocumentPath
						,@RecievedDate
						,2 -- For Bulk Upload
						,@PaidAmount
						,@BatchNo
						,1
						,@CreatedBy
						,dbo.fn_GetCurrentDateTime()
					)					
					
					SET @CustomerPaymentId = SCOPE_IDENTITY()
					
					DELETE FROM @PaidBills
					
					SET @IsBillExist = 0
					
					IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE PaymentStatusID = 2 AND AccountNo = @PresentAccountNo)
						BEGIN
							SET @IsBillExist = 1
						END
					
					IF(@IsBillExist = 1)
						BEGIN
							
							INSERT INTO @PaidBills
							(
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							)
							SELECT 
								 CustomerBillId
								,BillNo
								,PaidAmount
								,BillStatus
							FROM dbo.fn_GetBillWiseBillAmountByAccountNo(@PresentAccountNo,@PaidAmount) 
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							SELECT 
								 @CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime()
							FROM @PaidBills
							
							UPDATE CB
							SET CB.ActiveStatusId = PB.BillStatus
								,CB.PaidAmount = (ISNULL(CB.PaidAmount,0) + ISNULL(PB.PaidAmount,0))
							FROM Tbl_CustomerBills CB
							INNER JOIN @PaidBills PB ON PB.BillNo = CB.BillNo AND PB.CustomerBillId = CB.CustomerBillId					
						END
					ELSE
						BEGIN
							
							INSERT INTO Tbl_CustomerBillPayments
							(
								 CustomerPaymentId
								,PaidAmount
								,BillNo
								,CustomerBillId
								,CreatedBy
								,CreatedDate
							)
							VALUES( 
								 @CustomerPaymentId
								,@PaidAmount
								,NULL
								,NULL
								,@CreatedBy
								,dbo.fn_GetCurrentDateTime())
						END
						
					UPDATE [CUSTOMERS].[Tbl_CustomerActiveDetails]			
					SET OutStandingAmount = ISNULL(OutStandingAmount,0) - ISNULL(@PaidAmount,0)
					WHERE GlobalAccountNumber = @PresentAccountNo
					
					SET @SNo = @SNo + 1
					
					SET @PresentAccountNo = NULL
					SET @PaidAmount = NULL
					SET @ReceiptNo = NULL
					SET @DocumentPath = NULL
					SET @RecievedDate = NULL
					SET @BatchNo = NULL
					SET @PaymentModeId = NULL
					SET @CreatedBy = NULL
								
				END
				
				INSERT INTO Tbl_PaymentSucessTransactions
				(
					 PUploadFileId
					,SNO
					,AccountNo
					,AmountPaid
					,ReceiptNo
					,ReceivedDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,Comments
				)
				SELECT 
					 PUploadFileId
					,SNO
					,TempTableAccountNo
					,PaidAmount
					,ReceiptNo
					,PaymentDate
					,PaymentMode
					,CreatedDate
					,CreatedBy
					,'Valid'
				FROM #PaymentValidation
				WHERE IsValid = 1
				
				--UPDATE B
				--SET B.BatchTotal = SUM(CAST(ISNULL(PaidAmount,0) AS DECIMAL(18,2)))
				--FROM Tbl_BatchDetails B
				--INNER JOIN #PaymentValidation P ON P.BatchID = B.BatchID AND IsValid = 1
				
				DELETE FROM Tbl_PaymentTransactions 
					WHERE PaymentTransactionId IN(SELECT PaymentTransactionId FROM #PaymentValidation)
				
				UPDATE Tbl_PaymentUploadFiles
				SET TotalSucessTransactions = @SuccessTransactions
					,TotalFailureTransactions = @FailureTransactions
				WHERE PUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END

GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBU_New]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Faiz-ID103
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================
ALTER PROCEDURE [dbo].[USP_IsCustExistsByBU_New]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)')  
		,@Flag=C.value('(Flag)[1]','INT')  
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	--SET	@AccountNo= '12500450'
	--SET @BUID='BEDC_BU_0003'   
	--SET	 @Flag=	 ''
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	DECLARE @CustomerTypeId INT 
	
	SELECT @CustomerBusinessUnitID=BU_ID,@CustomerActiveStatusID=ISNULL(ActiveStatusId,0)  
		,@CustomerTypeId = CustomerTypeId
	FROM  UDV_IsCustomerExists WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo 
	OR MeterNumber=@AccountNo
	  	
	--PRINT	 @CustomerBusinessUnitID 
	--PRINT	 @CustomerActiveStatusID
	
	IF @CustomerBusinessUnitID IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
			IF @CustomerBusinessUnitID =  @BUID OR @BUID =''    
			BEGIN
				IF @Flag=1
				BEGIN
					--PRINT 'Sucess and No Need to check the ActiveStatusID of the Customers'
					SELECT 1 AS IsSuccess,
						@CustomerActiveStatusID AS ActiveStatusId
						,@CustomerTypeId AS CustomerTypeId
					FOR XML PATH('RptCustomerLedgerBe')
				END
				ELSE
				BEGIN
					IF	 @CustomerActiveStatusID  = 1 OR @CustomerActiveStatusID=2 OR @CustomerActiveStatusID=3 
						BEGIN
							--PRINT 'Sucess and Active  Customer'
							SELECT 1 AS IsSuccess,@CustomerTypeId AS CustomerTypeId
							FOR XML PATH('RptCustomerLedgerBe') 
						END
					ELSE
						BEGIN							 
							--PRINT 'Failure In Active Status'
							SELECT 0 AS IsSuccess,
								@CustomerActiveStatusID AS ActiveStatusId
								,@CustomerTypeId AS CustomerTypeId
							FOR XML PATH('RptCustomerLedgerBe') 
						END
				END
			END
			ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU 
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerDetailsForPaymentEntry]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 14-04-2014  
-- Modified date : 11-JULY-2014  
-- Modified By: Neeraj Kanojiya  
-- Description: The purpose of this procedure is to get Customer details for Payment Entry  
-- Modified By: Suresh Kumar --- using fn_IsAccountNoExists_BU_Id  instead of prev function
-- Modified By: Bhimaraju v --- using outstanding amt bcz if old cust having outstand amt function will not work
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerDetailsForPaymentEntry]   
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @AccountNo VARCHAR(50)  
		,@BU_ID VARCHAR(50)
		,@Active INT = 1
		
	SELECT  
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('MastersBE') as T(C)  
        
  DECLARE @GlobalAccountNumber VARCHAR(50), @ActiveStatusId INT 
 
	SELECT @GlobalAccountNumber = GlobalAccountNumber, @ActiveStatusId = ActiveStatusId FROM CUSTOMERS.Tbl_CustomersDetail 
			WHERE GlobalAccountNumber=@AccountNo OR OldAccountNo = @AccountNo

	IF( @GlobalAccountNumber IS NOT NULL)
		BEGIN
			IF ((SELECT [dbo].[fn_IsAccountNoExists_BU_Id_Payments](@GlobalAccountNumber,@BU_ID)) = 1)
				BEGIN
						SELECT  
						 dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName) AS Name
						,CD.OldAccountNo--Faiz-ID103
						,CD.ClassName
						,(CD.AccountNo+' - '+CD.GlobalAccountNumber) AS AccNoAndGlobalAccNo
						,CD.OutStandingAmount AS DueAmount  --Commented By Raja-ID065
						,CD.GlobalAccountNumber AS AccountNo
						,'success' AS StatusText
						,CustomerTypeId
						,(SELECT CustomerType FROM Tbl_MCustomerTypes CT WHERE CT.CustomerTypeId=CD.CustomerTypeId) AS CustomerType
						,dbo.fn_GetCustomerServiceAddress_New(CD.Service_HouseNo,CD.Service_StreetName,CD.Service_Landmark,CD.Service_City,NULL,CD.Service_ZipCode) AS Address1
					FROM [UDV_CustomerDescription] CD
					WHERE GlobalAccountNumber = @GlobalAccountNumber AND (BU_ID = @BU_ID OR @BU_ID = '')
					FOR XML PATH('MastersBE')  
				END
			ELSE IF(@ActiveStatusId = 4)
				BEGIN
					SELECT 'Customer status is Closed' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
			ELSE
				BEGIN
					SELECT 'Customer doesn''t exist in selected Bussiness Unit' AS StatusText FOR XML PATH('MastersBE'),TYPE
				END
		END
	ELSE
		BEGIN
			SELECT 'Customer doesn''t exist' AS StatusText FOR XML PATH('MastersBE'),TYPE
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50) 
		,@OutStanding VARCHAR(50) 
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		,@BulkEmails VARCHAR(MAX)
		
	SELECT
		 @AccounNo = U.GlobalAccountNumber
		,@OldAccountNo = U.OldAccountNo
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@BulkEmails=dbo.fn_GetCustomerEmailId(U.GlobalAccountNumber)
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookName
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.ReadCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		--,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
		,@OutStanding = CONVERT(VARCHAR,(CAST(ISNULL(U.OpeningBalance,0) AS MONEY)), 1)
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
				,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(TotalBillAmountWithArrears,0) AS MONEY)), 1)
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			--SELECT
			--	@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			--FROM Tbl_CustomerBillPayments
			--WHERE CustomerBillId = @CustomerBillId
			
			--SELECT
			--	@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			--FROM Tbl_BillAdjustments
			--WHERE CustomerBillId = @CustomerBillId
	
		END
	ELSE
		BEGIN			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE AccountNo = @GlobalAccountNo	
		END
	
	--SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.ReadCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		--,ISNULL((SELECT TOP 1 
		--		REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
		--	FROM Tbl_CustomerReadings CR3
		--	WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
		--		AND CR3.CustomerReadingId < CR.CustomerReadingId
		--	ORDER BY CR3.CustomerReadingId DESC),REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', ''))
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', '')
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@OldAccountNo AS OldAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@BulkEmails AS BulkEmails 
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,ISNULL(@NetArrears,@OutStanding) AS NetArrears
			--,@Payments AS Payments
			,ISNULL(@LastBillPayments,'0.00') AS Payments
			,ISNULL(@LastBillAdjustments,'0.00') AS Adjustments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,ISNULL(@LastPaidAmount,'--') AS LastPaidAmount
			,ISNULL(@LastPaidDate,'--') AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
			,@CustomerBillId AS CustomerBillId
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	,
	(
		SELECT
			 CF.FixedChargeId AS FixedChargeId
			,C.ChargeName AS FixedChargeName
			,CONVERT(VARCHAR,(CAST(ISNULL(CF.Amount,0) AS MONEY)), 1) AS FixedChargeAmount
		FROM Tbl_CustomerBillFixedCharges CF
		INNER JOIN Tbl_MChargeIds C ON CF.FixedChargeId = C.ChargeId AND IsAcitve = 1
			AND CF.CustomerBillId = @CustomerBillId
		FOR XML PATH('CustomerBillAdditionalChargeDetails'),TYPE   
	)
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillingMonthOpen]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 19-05-2014
-- Description:	The purpose of this procedure is to insert Billing Open Status
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertBillingMonthOpen]
	(
	@XmlDoc xml
	)
AS
BEGIN
	
	DECLARE  @Year INT
			,@Month INT
			,@Details VARCHAR(MAX)
			,@CreatedBy VARCHAR(50)
			
	SELECT   @Year=C.value('(Year)[1]','INT')
			,@Month=C.value('(Month)[1]','INT')
			,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
			,@CreatedBy=C.value ('(CreatedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') AS T(C)
	
	
	DECLARE @MaxYear INT=0,@MaxMonth INT=0
	
	SET @MaxYear = (SELECT ISNULL(MAX([Year]),0)FROM Tbl_BillingMonths)
	SET @MaxMonth = (SELECT ISNULL(MAX([MONTH]),0) FROM Tbl_BillingMonths WHERE [Year]=(SELECT ISNULL(MAX([Year]),0) FROM Tbl_BillingMonths))
	
	IF(@MaxMonth=12)
		BEGIN
			SET @MaxMonth=0
			SET @MaxYear=@MaxYear+1
		END
	
	IF EXISTS(SELECT 0 FROM Tbl_BillingMonths WHERE [Year]=@Year AND [Month]=@Month )
	BEGIN
		SELECT 1 AS IsBillingMonthExists FOR XML PATH('BillingMonthOpenBE')
	END
	ELSE
	BEGIN
		
		IF (@Month >= @MaxMonth AND @Year >= @MaxYear)
			BEGIN
				
				INSERT INTO Tbl_BillingMonths([Year]
											 ,[Month]
											 ,OpenstatusId
											 ,Details
											 ,CreatedBy
											 ,CreatedDate
											 )
									 VALUES( @Year
											,@Month
											,1
											,CASE @Details WHEN '' THEN NULL ELSE @Details END
											,@CreatedBy
											,GETDATE()
										   )
				SELECT 1 AS IsSuccess FOR XML PATH('BillingMonthOpenBE')
				
			END
		ELSE 
			BEGIN
				SELECT 1 AS IsLesserMonthYear FOR XML PATH('BillingMonthOpenBE')
			END
	END	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
		,CMI.MeterMultiplier
		,CMI.CustomerTypeId
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	,CustomerReadingInner.IsRollOver
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	
	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 
	
	
	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
			

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN IsValidReading = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN IsValidReading = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
				THEN (CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) AND ISDATE(CB.ReadingDate) = 1
							THEN (CASE WHEN CONVERT(DATE,CustomerReadingsLogs.ReadDate) >= CONVERT(DATE,CB.ReadingDate) 
										THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) 
										ELSE 0 END)
							ELSE (CASE WHEN CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CB.ReadingDate) 
										THEN ISNULL(CustomerReadings.IsRollOver,0) 
										ELSE 0 END)
					  END)
		  ELSE 0 END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,(CASE WHEN (ReadToDirectApprovals.ApprovalStatusId = 1 OR ReadToDirectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		--,(CASE WHEN BD.IsActive = 1 
		--		THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
		--		ELSE 0 END) AS IsDisabledBook
		,(SELECT TOP 1 (CASE WHEN BD.IsActive = 1 
				THEN (CASE WHEN ISNULL(BD.IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
				ELSE 0 END) 
			FROM Tbl_BillingDisabledBooks BD 
			WHERE BD.BookNo = CB.BookNo
			ORDER BY DisabledBookId DESC) AS IsDisabledBook
		,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 AND ISDATE(CB.ReadingDate) = 1
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
		,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') != '' AND ISDATE(CB.ReadingDate) = 1 
					THEN (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,CB.ReadingDate) THEN 1 ELSE 0 END)
					ELSE 0 
					END) AS IsMeterAssignedDateExists
		,(CASE WHEN CustomerReadings.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (Case when CONVERT(DATE,CB.ReadingDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)
			ELSE 0 END) as IsLatestBill
		,CB.MeterMultiplier
		,CB.CustomerTypeId
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerReadToDirectApprovaStatus As ReadToDirectApprovals ON CB.GlobalAccountNumber=ReadToDirectApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerAssignMeterApprovaStatus As AssignMeterApprovals	ON CB.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber
	LEFT JOIN #CustomerLatestBills As LatestBills ON CB.GlobalAccountNumber=LatestBills.GlobalAccountNumber
	--LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the customer is prepaid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE CustomerTypeId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Prepaid Customer'
					WHERE CustomerTypeId = 3
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0 OR ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0 OR ISNULL(PresentReading,'') = ''
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)-- OR PrvExist = 1
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 --OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1 AND IsLatestBill = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Bill Generated for Future Date'
					WHERE IsBilled = 1 AND IsLatestBill = 1
					
				END
				
			---- TO check whether the reading is valid or not
			--IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
			--	BEGIN
					
			--		UPDATE #ReadingsValidation
			--		SET IsValid = 0,
			--			Comments = Comments + ', Invalid Readings'
			--		WHERE ISNULL(PresentReading,'') = ''
					
			--	END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId IN (1,4))
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE ApproveStatusId IN (1,4)
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsReadToDirectApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Disconnect Change Approval Process is pending for this Customer'
					WHERE IsReadToDirectApproval = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Changed Date'
					WHERE IsMeterChangedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterAssignedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Assigned Date'
					WHERE IsMeterAssignedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
				,@MeterMultiplier INT
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
				,MeterMultiplier
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy,
						@MeterMultiplier = MeterMultiplier
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@MeterMultiplier
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END


GO

/****** Object:  StoredProcedure [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 29-Apr-2015
-- Description:	To get the customer by BU is exists 
-- =============================================

ALTER PROCEDURE [dbo].[USP_IsCustExistsByBUAndStatus_MeterReadings]
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE	@AccountNo VARCHAR(50)='',
			@BUID VARCHAR(50)='',
			@Flag INT
	
	SELECT
		@AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@BUID=C.value('(BUID)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('RptCustomerLedgerBe') AS T(C)
	
	DECLARE @CustomerBusinessUnitID VARCHAR(50)
	DECLARE @GlobalAccountNo VARCHAR(50)
	DECLARE @CustomerActiveStatusID INT 
	DECLARE @IsDisabledBook BIT 
	DECLARE @CustomerTypeId INT
	
	SELECT @CustomerBusinessUnitID = U.BU_ID,@CustomerActiveStatusID = ISNULL(U.ActiveStatusId,0)  
		,@GlobalAccountNo = U.GlobalAccountNumber
		,@CustomerTypeId = U.CustomerTypeId
		--,@IsDisabledBook = (CASE WHEN BD.IsActive = 1 
		--						THEN (CASE WHEN IsPartialBill IS NULL THEN 1 ELSE 0 END)
		--						ELSE 0 END)
		,@IsDisabledBook = (SELECT TOP 1 (CASE WHEN BD.IsActive = 1 
								THEN (CASE WHEN ISNULL(BD.IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
								ELSE 0 END) 
							FROM Tbl_BillingDisabledBooks BD 
							WHERE BD.BookNo = U.BookNo
							ORDER BY DisabledBookId DESC)
	FROM  UDV_IsCustomerExists U
	--LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = U.BookNo
	WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo OR MeterNumber=@AccountNo)
	  	
	IF @GlobalAccountNo IS NULL
		BEGIN
			--PRINT 'Customer Not Exist'
			SELECT 0 AS IsSuccess
			FOR XML PATH('RptCustomerLedgerBe')
		END
	ELSE
	BEGIN
		IF @CustomerBusinessUnitID = @BUID    
			BEGIN
				IF @CustomerActiveStatusID = 1 --OR @CustomerActiveStatusID=2 
					BEGIN
						--PRINT 'Sucess and Active  Customer'
						SELECT 1 AS IsSuccess,
							@GlobalAccountNo AS AccountNo,
							@IsDisabledBook AS IsDisabledBook,
							0 AS CustIsNotActive,
							@CustomerTypeId AS CustomerTypeId
						FOR XML PATH('RptCustomerLedgerBe') 
					END
				ELSE
					BEGIN							 
						--PRINT 'Failure In Active Status'
						SELECT 1 AS IsSuccess,
								1 AS CustIsNotActive,
								@CustomerActiveStatusID AS ActiveStatusId,
								@CustomerTypeId AS CustomerTypeId
						FOR XML PATH('RptCustomerLedgerBe') 
					END
			END
		ELSE
			BEGIN
				--PRINT 'Not Belogns to the Same BU'
				SELECT	1 AS IsSuccess,
						1 AS IsCustExistsInOtherBU ,
						@IsDisabledBook AS IsDisabledBook,
						0 AS CustIsNotActive,
						@CustomerTypeId AS CustomerTypeId
				FOR XML PATH('RptCustomerLedgerBe')
			END
		END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Bhimaraju Vanka/Karteek Panakani
-- Create date: 23-July-2015
-- Description: Description,,Get customer statistics and tariff Info
-- =============================================   
ALTER PROCEDURE [dbo].[USP_RptGetCustomersBillingStatisticsInfo_New]  
 (  
 @XmlDoc XML  
 )  
AS  
BEGIN    
 SET NOCOUNT ON;  
 DECLARE @Month INT,    
   @Year INT,    
   @BU_ID VARCHAR(100)      
 
SELECT   @Month = C.value('(Month)[1]','INT')      
  ,@Year = C.value('(Year)[1]','INT')      
  ,@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')         
FROM @XmlDoc.nodes('PDFReportsBe') AS T(C)      

   SELECT TariffName AS Tariff		 
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(EnergyDelivered) AS MONEY),-1),'.00','') AS KWHSold
		 ,CONVERT(VARCHAR(20),CAST(SUM(FixedCharges) AS MONEY),-1) AS FixedCharges
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfBilledCustomers) AS MONEY),-1),'.00','') AS NoBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountBilled) AS MONEY),-1) AS TotalAmountBilled
		 ,CONVERT(VARCHAR(20),CAST(SUM(TotalAmountCollected) AS MONEY),-1) AS TotalAmountCollected
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(NoOfStubs) AS MONEY),-1),'.00','') AS NoOfStubs
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(TotalPopulationCount) AS MONEY),-1),'.00','') AS TotalPopulation
		 ,REPLACE(CONVERT(VARCHAR(20),CAST(SUM(WeightedAvg) AS MONEY),-1),'.00','') AS WeightedAverage
   FROM Tbl_ReportBillingStatisticsByTariffId
   WHERE (BU_ID=@BU_ID OR @BU_ID='')
	AND (YearId=@Year OR @Year=0)
	AND (MonthId=@Month OR @Month=0)
   GROUP BY TariffName
   
END  



GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportData_BillGen]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj kanojya
-- Date		  : 6-Aug-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportData_BillGen]  
(  
	@Month INT 
	,@Year INT 
	,@CreatedBy VARCHAR(50)
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @LastMonth INT
		,@LastYear INT		
		
	
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
	IF(@Month = 1)
		BEGIN
			SET @LastMonth = 12
			SET @LastYear = @Year - 1
		END
	ELSE
		BEGIN
			SET @LastMonth = @Month - 1
			SET @LastYear = @Year
		END
		
	SELECT 
		 CB.BU_ID
		,BU.BusinessUnitName
		,CB.SU_ID
		,SU.ServiceUnitName
		,CB.ServiceCenterId
		,SC.ServiceCenterName
		,CB.TariffId
		,TC.ClassName
		,CPD.CustomerTypeId
		,CT.CustomerType
		,CB.BillYear
		,CB.BillMonth
		,CB.AccountNo
		,CPD.ReadCodeID
		,CB.Usage
		,CB.NetFixedCharges
		,CB.NetEnergyCharges
		,CB.VATPercentage
		,CB.NetArrears
		,CB.VAT
		,CB.TotalBillAmount
		,CB.TotalBillAmountWithTax
		,CB.TotalBillAmountWithArrears
		,CB.ReadType
		,CB.PaidAmount
		,CAD.OutStandingAmount
		,CB.TotalBillAmountWithTax AS ClosingBalance
		,CB.BillNo
		,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
	INTO #CustomersList
	FROM Tbl_CustomerBills CB 
	INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
	INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
	INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
	INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
	INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
	WHERE BillMonth = @Month AND BillYear = @Year

	SELECT 
		  AccountNo
		 ,TotalBillAmountWithTax
	INTO #CustomerOpeningBalance
	FROM Tbl_CustomerBills
	WHERE BillMonth = @LastMonth AND BillYear = @LastYear

	SELECT 
		  CPD.BookNo
		 ,BN.CycleId
		 ,BG.ServiceCenterId
		 ,SC.SU_ID
		 ,SU.BU_ID
		 ,CPD.TariffClassID
		 ,CPD.CustomerTypeId
		 ,GlobalAccountNumber
	INTO #CustomerPopulation
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
	INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
	INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

	INSERT INTO Tbl_ReportBillingStatisticsBySCId
	(
		 BU_ID
		,BusinessUnitName
		,SU_ID
		,ServiceUnitName
		,SC_ID
		,ServiceCenterName
		,CustomerTypeId
		,CustomerType
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,EnergyDelivered
		,FixedCharges
		,NoOfBilledCustomers
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
					AND CP.ServiceCenterId = CL.ServiceCenterId 
					AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		
	INSERT INTO Tbl_ReportBillingStatisticsByTariffId
	(
		 BU_ID
		,BusinessUnitName
		,TariffId
		,TariffName
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,NoOfBilledCustomers
		,EnergyDelivered
		,FixedCharges
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID  
					AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,SUM(ISNULL(CL.TotalBillAmount,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth

	DROP TABLE #CustomersList
	DROP TABLE #CustomerOpeningBalance
	DROP TABLE #CustomerPopulation	
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchAdjustmentStatus]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Faiz-ID103>  
-- Create date: <23-DEC-2014>  
-- Description: <Retriving Batch Adjustment Status (Open/Close)>  
-- Author:  <Faiz-ID103>  
-- Create date: <13-May-2015>  
-- Description: Added the BU_ID for filtering the records  
-- Modified By : Bhimaraju Vanka  
-- Desc : Added Batch Status Id Condition  
-- Modified By : kalyan
-- modified Date :06-08-2015
--Desc: change order by desc

-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBatchAdjustmentStatus]  
(  
 @XmlDoc XML  
)  
AS  
 BEGIN  
  DECLARE @BU_ID VARCHAR(50)  
    ,@PageNo INT  
    ,@PageSize INT  
    
  SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(50)')       
    ,@PageNo = C.value('(PageNo)[1]','INT')  
    ,@PageSize = C.value('(PageSize)[1]','INT')         
  FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)     
   
 IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
    
 ;WITH PagedResults AS  
  (  
   SELECT ROW_NUMBER() OVER(ORDER BY BA.BatchDate desc) AS RowNumber   
     ,BA.BatchID  
     ,BA.BatchNo  
     ,ISNULL(CONVERT(VARCHAR(50),BA.BatchDate,106),'--') AS BatchDate  
     ,BA.BatchTotal  
     ,ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0) AS PaidAmount  
     ,BU_ID AS BUID  
     ,BillAdjustmentTypeId  
     ,Name AS BillAdjustmentType  
   FROM Tbl_BATCH_ADJUSTMENT AS BA  
   LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentTypeId=BT.BATID  
   WHERE ISNULL(BatchStatusID,1) = 1  
     --BatchTotal>(ISNULL((SELECT SUM(ABS(TotalAmountEffected)) FROM Tbl_BillAdjustments WHERE BatchNo=BA.BatchID),0))  
     --AND BU_ID=@BU_ID  
     AND BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))  
  )  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults PR  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   ORDER BY  convert(datetime, PR.BatchDate, 103) desc  
 END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================    
--AUTHOR : NEERAJ KANOJIYA    
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS    
--CREATED ON: 4-OCT-14    
--=============================================================    
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]  
(    
@XmlDoc XML    
)    
AS    
 BEGIN    
 DECLARE @BatchID INT, @BU_ID VARCHAR(50)  
     
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')    
 , @BU_ID  = C.value('(BU_ID)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)    
     
     
SELECT (  
  SELECT BA.BatchID,  
    BA.BatchNo,  
    BA.BatchDate,  
    BA.BatchTotal,  
    (SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,  
    RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode  
    (BA.BatchTotal-  
    (SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,  
    convert(int, (BA.BatchTotal-  
    (SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,  
    (SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType  
   FROM  Tbl_BATCH_ADJUSTMENT AS BA  
   JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID AND BA.BatchStatusID = 1  
   AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID  
  
      
   FOR XML PATH('BillAdjustments'),TYPE            
   ),  
   (  
    SELECT BA.BatchNO,  
      BAT.Name AS AdjustmentName,  
      CD.GlobalAccountNumber AS AccountNo,  
      BLA.TotalAmountEffected,  
      BLA.AmountEffected,  
      ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,  
      BLA.CreatedDate  
      ,BLA.Remarks  
    from Tbl_BATCH_ADJUSTMENT  AS BA  
    JOIN Tbl_BillAdjustments  AS BLA ON BA.BatchID = BLA.BatchNo AND BA.BatchStatusID = 1  
      AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID  
    JOIN Tbl_BillAdjustmentDetails AS BAD ON BLA.BillAdjustmentId=BAD.BillAdjustmentId  
    JOIN Tbl_BillAdjustmentType  AS BAT ON BLA.BillAdjustmentType=BAT.BATID  
    JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber  
      
    FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE        
   )  
   FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')        
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetBatchPaymentStatus]    Script Date: 08/07/2015 10:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
-- =============================================  
-- Author:  <Faiz-ID103>  
-- Create date: <23-DEC-2014>  
-- Description: <Retriving Batch Payment Status (Open/Close)>  
-- Modified By : Bhimaraju Vanka  
-- Desc : Added Batch Status Id Condition  
-- Modified By :kalyan
-- Modified Date :06-08-2015
--Desc : change order by desc
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBatchPaymentStatus]  
(  
 @XmlDoc XML  
)  
AS  
 BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)  
    ,@PageNo INT  
    ,@PageSize INT  
    
  SELECT  @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')      
    ,@PageNo = C.value('(PageNo)[1]','INT')  
    ,@PageSize = C.value('(PageSize)[1]','INT')       
  FROM @XmlDoc.nodes('BatchStatusBE') AS T(C)     
    
    
  IF(@BU_ID = '')  
  BEGIN  
   SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50))   
      FROM Tbl_BussinessUnits   
      WHERE ActiveStatusId = 1  
      FOR XML PATH(''), TYPE)  
     .value('.','NVARCHAR(MAX)'),1,1,'')  
  END  
    
  ;WITH PagedResults AS  
  (  
  SELECT ROW_NUMBER() OVER(ORDER BY BD.BatchDate desc) AS RowNumber   
   ,BD.BatchNo  
   ,BD.BatchID   
   ,BD.BatchTotal  
   ,ISNULL(CONVERT(VARCHAR(50),BD.BatchDate,106),'--') AS BatchDate  
   --,bd.BatchDate  
   ,ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchID),0) AS PaidAmount  
   --,(CASE   
   -- WHEN BD.BatchTotal<>(ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0) )   
   -- THEN 'Open'   
   -- else 'Close'   
   --end) AS BatchStatus  
   ,BU_ID AS BUID  
  FROM Tbl_BatchDetails AS BD  
  WHERE ISNULL(BD.BatchStatus,1) = 1 -- Faiz  
  --BD.BatchTotal>(ISNULL((SELECT SUM(PaidAmount) FROM Tbl_CustomerPayments WHERE BatchNo=BD.BatchNo),0))  
  --AND BU_ID=@BU_ID  
  AND BU_ID=(SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))  
  
  )  
   SELECT *  
     ,(Select COUNT(0) from PagedResults) as TotalRecords       
   FROM PagedResults pr  
   WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize  
   ORDER BY  convert(datetime, PR.BatchDate, 103) desc    
    
 END  
  
GO


