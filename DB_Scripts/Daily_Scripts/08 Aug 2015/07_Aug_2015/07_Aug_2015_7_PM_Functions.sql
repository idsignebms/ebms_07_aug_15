
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges_New]    Script Date: 08/07/2015 19:02:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
ALTER FUNCTION  [dbo].[fn_GetFixedCharges_New]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
	,@GlobalAccountNumber   varchar(200)
	,@IsFirstMonthBill Bit 
	,@SetupDate DATETIME
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	DECLARE @Multiplier DECIMAL=1
	 
	 
	IF @IsFirstMonthBill =1
	BEGIN
		IF @SetupDate IS NOT NULL 
			BEGIN  
			   --SET @Multiplier= DateDiff(mm,@SetupDate,DATEADD(day,-30,@MonthStartDate)  )
			   SET @Multiplier= DateDiff(mm,@SetupDate,@MonthStartDate)
			   if(@Multiplier<=0)
				set @Multiplier=1
			END
	 		 
	END


	 

	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount*ISNULL(@Multiplier,0) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN 
	CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END

--------------------------------------------------------------------------------------------------

